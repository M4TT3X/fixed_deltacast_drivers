/*
   DELTA-hd/sdi/codec
   ------------
   ANCILLARY DATA RECEPTION SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK to receive ancillary data stream.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a reception 
   loop.

   In order to compile this application, path to VideoMasterHD_Core.h and 
   VideoMasterHD_Sdi.h inclusion file and to VideoMasterHD.lib library 
   file must be properly configured.

   In order to run this application, the DELTA-hd/sdi/codec board RX0 connector must 
   be fed with a valid SD or HD SDI signal containing ancillary data.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards,VideoStandard,Interface;
ULONG             ChnType,i;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
VHD_ANCPACKET    *pAncPacket = NULL;
int               NbAncPackets = 0;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
ULONG             ClockSystem;
ULONG             NbRxRequired = 1, NbTxRequired = 0;

   init_keyboard();

   printf("\nANC RECEPTION SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for ( i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {
                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Wait for channel locked */
                  WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                  /* Auto-detect clock system */
                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);
                  if(Result == VHDERR_NOERROR)               
                  {
                     printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                     /* Select the detected clock system */
                     VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,ClockSystem);

                     /* Create a logical stream to receive from RX0 connector */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        /* Get auto-detected video standard */
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);
                        if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                        {
                           PrintVideoStandardInfo(VideoStandard);

                           Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,&Interface); 

                           if((Result == VHDERR_NOERROR) && (Interface != NB_VHD_INTERFACE))
                           {
                              PrintInterfaceInfo((VHD_INTERFACE)Interface);

                              /* Configure stream */
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                              /* Start stream */
                              Result = VHD_StartStream(StreamHandle);
                              if (Result == VHDERR_NOERROR)
                              {

                                 printf("\nReception started, press ESC to stop...\n");

                                 /* Reception loop */
                                 while (1)
                                 {

                                    if (kbhit()) 
                                    {
                                       getch();
                                       break;
                                    }

                                    /* Try to lock nextslot */
                                    Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       /* Get ANC content on the first 10 lines */
                                       for (int LineNumber = 1; LineNumber < 11; LineNumber++)
                                       {
                                          /* First check Y samples stream */
                                          NbAncPackets = 0;
                                          Result = VHD_SlotAncGetNbPackets(SlotHandle,LineNumber,VHD_SAMPLE_Y,&NbAncPackets);
                                          if (Result == VHDERR_NOERROR)
                                          {
                                             if (NbAncPackets > 0)
                                             {
                                                printf("\nLine %d - Y ANC : %d ANC packets\n",LineNumber,NbAncPackets);

                                                /* Get all packets */
                                                for (int AncPacketIndex = 0; AncPacketIndex < NbAncPackets; AncPacketIndex++)
                                                {
                                                   pAncPacket = NULL;
                                                   Result = VHD_SlotAncGetPacket(SlotHandle,LineNumber,VHD_SAMPLE_Y,AncPacketIndex,&pAncPacket);
                                                   if (Result == VHDERR_NOERROR)
                                                   {
                                                      printf("  ANC Packet %d : Did=%02X - SDid=%02X - DC=%02X\n",
                                                         AncPacketIndex,
                                                         pAncPacket->DataID,
                                                         pAncPacket->SecondaryDataID,
                                                         pAncPacket->DataCount);
                                                      printf("    UDW : ");
                                                      for (int UDW = 0; UDW < pAncPacket->DataCount; UDW++)
                                                         printf("%04X.",pAncPacket->pUserDataWords[UDW]);
                                                      printf("\b \n");
                                                   }
                                                   else
                                                   {
                                                      printf("ERROR : Cannot get ANC packet %d on line %d - Y samples. Result = 0x%08X (%s)\n",AncPacketIndex,LineNumber,Result, GetErrorDescription(Result));
                                                      break;
                                                   }
                                                }
                                                fflush(stdout);

                                                if (Result != VHDERR_NOERROR)
                                                   break;
                                             }
                                          }
                                          else
                                          {
                                             printf("ERROR : Cannot get number of packets on line %d - Y samples. Result = 0x%08X (%s)\n",LineNumber,Result, GetErrorDescription(Result));
                                             break;
                                          }
                                       }

                                       /* Unlock slot */
                                       VHD_UnlockSlotHandle(SlotHandle);
                                    }
                                    else if (Result != VHDERR_TIMEOUT)
                                    {
                                       printf("ERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       break;
                                    }
                                    else
                                       printf("Timeout \r");
                                 }

                                 printf("\n");

                                 /* Stop stream */
                                 VHD_StopStream(StreamHandle);
                              }
                              else
                                 printf("ERROR : Cannot start RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                           }
                           else
                              printf("ERROR : Cannot detect incoming interface from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Re-establish RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);  
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



