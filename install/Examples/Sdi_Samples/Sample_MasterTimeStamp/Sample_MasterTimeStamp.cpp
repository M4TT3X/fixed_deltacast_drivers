/*
   DELTA-hd/sdi/codec
   -------------
   RECEPTION TIME STAMP SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec
   board and of the VideoMasterHD SDK in a master time stamp reading
   use case.

   The application opens a board handle, configures it, and enters in a 
   master timestamp read back loop.

   In order to compile this application, path to VideoMasterHD_Core.h and 
   VideoMasterHD_Sdi.h inclusion file and to VideoMasterHD.lib library 
   file must be properly configured.

   In order to run this application, use a DELTA-hd/sdi/codec board.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
HANDLE            BoardHandle = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;



   init_keyboard();

   printf("\nRECEPTION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }        

         /* Open a handle on first DELTA-hd/sdi/codec board */
         Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
         if (Result == VHDERR_NOERROR)
         {
               /* Re-establish RX0-TX0 by-pass relay loopthrough */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
               /* Back to local clock... */
               VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_GENLOCK_SOURCE,VHD_GENLOCK_LOCAL);
               /* Set genlock to NTSC timings */
               VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_GENLOCK_VIDEO_STANDARD,VHD_VIDEOSTD_S259M_NTSC);

               /* Reception loop */
               while (1)
               {

                  if (kbhit()) 
                  {
                     getch();
                     break;
                  }


                  ULONG MasterTimeStamp=0;
                  Result = VHD_WaitNextMasterTimestamp(BoardHandle,&MasterTimeStamp, 200);
                  if (Result == VHDERR_NOERROR)
                  {
                     /* Do processing here*/
                  }
                  else
                  {
                     printf("\nERROR : Cannot get next timestamp. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                     break;
                  }

                  printf("next master  time stamp  %d           \r", MasterTimeStamp);
                  fflush(stdout);
                           
               }

            /* Close board handle */
            VHD_CloseBoardHandle(BoardHandle);
         }
         else
            printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



