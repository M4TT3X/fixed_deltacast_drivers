/*
   DELTA-hd/sdi/codec
   ------------
   AC3 AUDIO TRANSMISSION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK in a AC3 (compressed) audio transmission stream use case.

   The application opens board and stream handles, configures them, and 
   enters in a transmission loop where audio embedding is performed.

   In order to compile this application, path to VideoMasterHD_Core.h, VideoMasterHD_Sdi.h 
   and VideoMasterHD_Sdi_Audio.h inclusion file and to VideoMasterHD.lib and 
   VideoMasterHD_Audio.lib library file must be properly configured.

   Please refer to SMPTE 340M standard for more information about AC3 data embedding in AES3 Non-PCM audio.

   audio.ac3 file must be located next to the executable. This file can be replaced by another AC3 audio file (AC3ENCODINGBITRATE must be adjusted).
*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>
#include <math.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Audio/VideoMasterHD_Sdi_Audio.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Audio.h"
#endif

#include "../../Tools.h"

#define CLOCK_SYSTEM    VHD_CLOCKDIV_1

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

/*
Structure of an AC-3 Data Burst for 16-bit mode - See SMPTE 340M

|<-- 1536 Audio Samples periods (32ms @ 48kHz) -->|<-- 1536 Audio Samples periods (32ms @ 48kHz) -->|
|<----------------- Data Burst ------------------>|<----------------- Data Burst ------------------>|
|           |<- Burst payload ->|                 |           |<- Burst payload ->|                 |
|Pa|Pb|Pc|Pd|  AC-3 Sync Frame  |PADDING (silence)|Pa|Pb|Pc|Pd|  AC-3 Sync Frame  |PADDING (silence)|

*/
#define BURSTPREAMBLE_PA 0xF872 //(16-bit mode)
#define BURSTPREAMBLE_PB 0x4E1F //(16-bit mode)
#define BURSTPREAMBLE_PC 0x0001 //(16-bit mode - AC3 type - Data valid - Stream 0)
#define BURSTPREAMBLE_PD (AC3ENCODINGBITRATE*1536/48000)
#define PADDING 0x00
#define AC3DATABURSTSIZE (1536*4) // 1536 x 16-bit samples subframe mode (2 channels)
#define AC3ENCODINGBITRATE 384000 //Related to the audio.ac3 file (bps)
#define AC3FILENAME "audio.ac3"

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             SlotsCount,VideoStandard, NbOfSamples=0, BlockSize=0, SampleCnt=0,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL; 
VHD_AUDIOCHANNEL *pAudioChn=NULL;
VHD_AUDIOINFO     AudioInfo;
SHORT            *pSample=NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
FILE             *pFile_X=NULL;
ULONG             FileSize_UL=0;
ULONG             WordsCount_UL = 0;
ULONG             NbRxRequired = 0, NbTxRequired = 1;


   init_keyboard();

   printf("\nAUDIO TRANSMISSION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   pFile_X = fopen("audio.ac3","rb");
   if(!pFile_X)
   {
      printf("\nError while opening audio.ac3 for reading.");
      return 1;
   }

   fseek (pFile_X, 0, SEEK_END);   
   FileSize_UL = ftell (pFile_X);

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }        

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_TX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {
                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Select a 1/1 clock system */
                  VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,CLOCK_SYSTEM);

                  /* Create a logical stream to transmit on TX0 connector */
                  Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_TX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
                  if (Result == VHDERR_NOERROR)
                  {
                     switch (ChnType)
                     {
                     case VHD_CHNTYPE_3GSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_60Hz; Interface=VHD_INTERFACE_3G_A_425_1; break;
                     case VHD_CHNTYPE_HDSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_30Hz; Interface=VHD_INTERFACE_HD_292_1; break;
                     case VHD_CHNTYPE_SDSDI: 
                     default: VideoStandard=VHD_VIDEOSTD_S259M_PAL; Interface=VHD_INTERFACE_SD_259; break;
                     }

                     /* Configure stream video standard */
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                     VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_PRELOAD,2);
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);


                     /* Configure audio info */
                     memset(&AudioInfo, 0, sizeof(VHD_AUDIOINFO));
                     pAudioChn=&AudioInfo.pAudioGroups[0].pAudioChannels[0];
                     pAudioChn->Mode=AudioInfo.pAudioGroups[0].pAudioChannels[1].Mode=VHD_AM_STEREO;
                     pAudioChn->BufferFormat=AudioInfo.pAudioGroups[0].pAudioChannels[1].BufferFormat=VHD_AF_16; 

                     /* Get the biggest audio frame size */
                     NbOfSamples = VHD_GetNbSamples((VHD_VIDEOSTANDARD)VideoStandard, CLOCK_SYSTEM, VHD_ASR_48000, 0);
                     BlockSize = VHD_GetBlockSize(pAudioChn->BufferFormat, pAudioChn->Mode);

                     //AES Channel Status must be manually set because VideoMasterHD Audio API only supports AES Channel Status completion for PCM data
                     pAudioChn->AesChannelStatusBlockValid = TRUE; //Indicate to the VideoMasterHD API to not generate AES channel status automatically
                     pAudioChn->AesChannelStatusBlock.Pro = TRUE;
                     pAudioChn->AesChannelStatusBlock.NotAudio = TRUE; //Indicate that the data are not PCM
                     pAudioChn->AesChannelStatusBlock.EncodedAudioSignalEmphasis = 0;
                     pAudioChn->AesChannelStatusBlock.NotLocked = 0;
                     pAudioChn->AesChannelStatusBlock.SampleFrequency = 0x2;
                     pAudioChn->AesChannelStatusBlock.ChannelMode = 0;
                     pAudioChn->AesChannelStatusBlock.UserBitMngmt = 8;
                     pAudioChn->AesChannelStatusBlock.Aux = 0;
                     pAudioChn->AesChannelStatusBlock.SrcWordLen = 1;
                     pAudioChn->AesChannelStatusBlock.Reserved1 = 0;
                     pAudioChn->AesChannelStatusBlock.Reserved2 = 0;
                     pAudioChn->AesChannelStatusBlock.Reference = 0;
                     pAudioChn->AesChannelStatusBlock.Reserved3 = 0;
                     pAudioChn->AesChannelStatusBlock.Reserved4 = 0;
                     pAudioChn->AesChannelStatusBlock.OrigData = 0;
                     pAudioChn->AesChannelStatusBlock.DestData = 0;
                     pAudioChn->AesChannelStatusBlock.LocalSampleAddressCode = 0;
                     pAudioChn->AesChannelStatusBlock.TimeOfDaySampleAddressCode = 0;
                     pAudioChn->AesChannelStatusBlock.Reserved5 = 0;
                     pAudioChn->AesChannelStatusBlock.CSB0to5Unrel = 0;
                     pAudioChn->AesChannelStatusBlock.CSB6to13Unrel = 0;
                     pAudioChn->AesChannelStatusBlock.CSB14to17Unrel = 0;
                     pAudioChn->AesChannelStatusBlock.CSB18to21Unrel = 0;
                     pAudioChn->AesChannelStatusBlock.CRC = 0xf7;

                     /* Create an audio buffer */
                     pAudioChn->pData = new BYTE[NbOfSamples*BlockSize];
                     if(pAudioChn->pData)
                     {
                        /* Start stream */
                        Result = VHD_StartStream(StreamHandle);
                        if (Result == VHDERR_NOERROR)
                        {
                           printf("\nTransmission started, press ESC to stop...\n");

                           /* Reception loop */
                           while (1)
                           {

                              if (kbhit()) 
                              {
                                 getch();
                                 break;
                              }

                              /* Try to lock next slot */
                              Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                              if (Result == VHDERR_NOERROR)
                              {    
                                 /* Retrieve the number of needed samples */
                                 pAudioChn->DataSize = 0;
                                 Result = VHD_SlotEmbedAudio(SlotHandle,&AudioInfo);
                                 if (Result != VHDERR_BUFFERTOOSMALL)
                                 {
                                    printf("\nERROR : Cannot embed audio on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    break;
                                 }

                                 /* Build audio samples */
                                 NbOfSamples = pAudioChn->DataSize / BlockSize;
                                 for (ULONG i=0; i<NbOfSamples*2; i++)
                                 {
                                    switch(WordsCount_UL)
                                    {
                                       //Burst Preamble
                                    case 0: pAudioChn->pData[2*i] = (BURSTPREAMBLE_PA&0xff);  pAudioChn->pData[2*i+1] = ((BURSTPREAMBLE_PA&0xff00)>>8); break;
                                    case 1: pAudioChn->pData[2*i] = (BURSTPREAMBLE_PB&0xff);  pAudioChn->pData[2*i+1] = ((BURSTPREAMBLE_PB&0xff00)>>8); break;
                                    case 2: pAudioChn->pData[2*i] = (BURSTPREAMBLE_PC&0xff);  pAudioChn->pData[2*i+1] = ((BURSTPREAMBLE_PC&0xff00)>>8); break;
                                    case 3: pAudioChn->pData[2*i] = (BURSTPREAMBLE_PD&0xff);  pAudioChn->pData[2*i+1] = ((BURSTPREAMBLE_PD&0xff00)>>8); break;
                                    default: if(WordsCount_UL<((BURSTPREAMBLE_PD/16)+4)) //BURSTPREAMBLE_PD equal to the number of data bits in the burst payload => /16 for words
                                             {
                                                //Burst payload from ac3 file
                                                fread(&pAudioChn->pData[2*i+1],1,1,pFile_X);
                                                fread(&pAudioChn->pData[2*i],1,1,pFile_X);
                                             }
                                             else
                                             {
                                                //Padding
                                                pAudioChn->pData[2*i] = 0x00;
                                                pAudioChn->pData[2*i+1] = 0x00;
                                             } 
                                             break;
                                    }

                                    WordsCount_UL++;
                                    WordsCount_UL %= (AC3DATABURSTSIZE/2);
                                    if(ftell(pFile_X) == FileSize_UL)
                                       fseek(pFile_X, 0, SEEK_SET);
                                 }

                                 /* Embed audio */
                                 Result = VHD_SlotEmbedAudio(SlotHandle,&AudioInfo);
                                 if (Result != VHDERR_NOERROR)
                                 {
                                    printf("\nERROR : Cannot embed audio on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    break;
                                 }

                                 /* Unlock slot */
                                 Result = VHD_UnlockSlotHandle(SlotHandle);
                                 if (Result != VHDERR_NOERROR)
                                 {
                                    printf("\nERROR : Cannot unlock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    break;
                                 }
                                 /* Print some statistics */
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                 printf("%u frames sent            \r",SlotsCount);
                                 fflush(stdout);
                              }
                              else if (Result != VHDERR_TIMEOUT)
                              {
                                 printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                 break;
                              }
                           }

                           printf("\n");

                           /* Stop stream */
                           VHD_StopStream(StreamHandle);
                        }
                        else
                           printf("ERROR : Cannot start TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                        /* Delete audio buffer */
                        delete[] pAudioChn->pData;
                     }
                     else
                        printf("ERROR : Cannot allocate memory for test pattern buffer.\n");

                     /* Close stream handle */
                     VHD_CloseStreamHandle(StreamHandle);

                  }
                  else
                     printf("ERROR : Cannot open TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Re-establish RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);

               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);  
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


   fclose(pFile_X);

   close_keyboard();


   return 0;
}



