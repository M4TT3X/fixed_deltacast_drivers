/*
   DELTA-hd/sdi/codec
   ------------
   UNCOMPRESSED 5.1 AUDIO TRANSMISSION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK in an uncompressed 5.1 audio transmission stream use case.


   The application opens board and stream handles, configures them, and 
   enters in a transmission loop where audio embedding is performed.

   In order to compile this application, path to VideoMasterHD_Core.h, VideoMasterHD_Sdi.h 
   and VideoMasterHD_Sdi_Audio.h inclusion file and to VideoMasterHD.lib and 
   VideoMasterHD_Audio.lib library file must be properly configured.
*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>
#include <math.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Audio/VideoMasterHD_Sdi_Audio.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Audio.h"
#endif

#include "../../Tools.h"

#define CLOCK_SYSTEM    VHD_CLOCKDIV_1

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

#define SAMPLING_RATE   48000
#define PI              3.1415926538
#define AMPL            3276               /* full level -20 dB */ 
#define NORMAL_FREQ     1000               /* 1 kHz tone is played on Left, Central, Right, Surround Left and Surround Right channels */
#define LOW_FREQ        60                 /* 60 Hz tone is played on LFE */
#define NBOF_SAMPLE     (SAMPLING_RATE*2)  /* Channel is active alternately for 2 second */
#define NBOF_CHN        6

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             SlotsCount,VideoStandard, NbOfSamples=0, BlockSize=0, SampleCnt=0, TotalSampleCnt=0, Interface, AudioBufferSize;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL; 
VHD_AUDIOCHANNEL *ppAudioChnPair[3]={NULL, NULL, NULL};
ULONG            *ppAudioChnPairData[3]={NULL, NULL, NULL};
VHD_AUDIOINFO     AudioInfo;
ULONG            *p51AudioBuffer=NULL;
SHORT            *pSample=NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
int               ActiveChnIdx=0;
int               ToneFrequency=NORMAL_FREQ;

   init_keyboard();

   printf("\nAUDIO TRANSMISSION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }          

         /* Open a handle on first DELTA board */
         Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
         if (Result == VHDERR_NOERROR)
         {
            VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_TX0_TYPE, &ChnType);
            if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
            {
               /* Disable RX0-TX0 by-pass relay loopthrough */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

               /* Select a 1/1 clock system */
               VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,CLOCK_SYSTEM);

               /* Create a logical stream to transmit on TX0 connector */
               Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_TX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
               if (Result == VHDERR_NOERROR)
               {
                  switch (ChnType)
                  {
                  case VHD_CHNTYPE_3GSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_60Hz; Interface=VHD_INTERFACE_3G_A_425_1; break;
                  case VHD_CHNTYPE_HDSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_30Hz; Interface=VHD_INTERFACE_HD_292_1; break;
                  case VHD_CHNTYPE_SDSDI: 
                  default: VideoStandard=VHD_VIDEOSTD_S259M_PAL; Interface=VHD_INTERFACE_SD_259; break;
                  }

                  /* Configure stream video standard */
                  VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                  VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_PRELOAD,2);
                  VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                  /* 5.1 uncompressed audio is not natively supported by VideoMasterHD.
                     User has to received the 6 first channel pairs in stereo mode and build 
                     the 5.1 audio buffer "manually" by interleaving the 3 stereo pairs.
                  */

                  /* Configure audio info structure to setup 3 16-bit stereo receptions of the 3 first audio pairs */
                  memset(&AudioInfo, 0, sizeof(VHD_AUDIOINFO));
                  AudioInfo.pAudioGroups[0].pAudioChannels[0].Mode=VHD_AM_STEREO;
                  AudioInfo.pAudioGroups[0].pAudioChannels[1].Mode=VHD_AM_STEREO;
                  AudioInfo.pAudioGroups[0].pAudioChannels[2].Mode=VHD_AM_STEREO;
                  AudioInfo.pAudioGroups[0].pAudioChannels[3].Mode=VHD_AM_STEREO;
                  AudioInfo.pAudioGroups[1].pAudioChannels[0].Mode=VHD_AM_STEREO;
                  AudioInfo.pAudioGroups[1].pAudioChannels[1].Mode=VHD_AM_STEREO;

                  AudioInfo.pAudioGroups[0].pAudioChannels[0].BufferFormat=VHD_AF_16;
                  AudioInfo.pAudioGroups[0].pAudioChannels[1].BufferFormat=VHD_AF_16;
                  AudioInfo.pAudioGroups[0].pAudioChannels[2].BufferFormat=VHD_AF_16;
                  AudioInfo.pAudioGroups[0].pAudioChannels[3].BufferFormat=VHD_AF_16;
                  AudioInfo.pAudioGroups[1].pAudioChannels[0].BufferFormat=VHD_AF_16;
                  AudioInfo.pAudioGroups[1].pAudioChannels[1].BufferFormat=VHD_AF_16;

                  /* Get the biggest audio frame size */
                  NbOfSamples = VHD_GetNbSamples((VHD_VIDEOSTANDARD)VideoStandard, CLOCK_SYSTEM, VHD_ASR_48000, 0);
                  BlockSize = VHD_GetBlockSize(VHD_AF_16, VHD_AM_STEREO);
                  AudioBufferSize = NbOfSamples*BlockSize;

                  /* Point to the first channel of each audio pair */
                  ppAudioChnPair[0]=&AudioInfo.pAudioGroups[0].pAudioChannels[0];    //Left and right channels
                  ppAudioChnPair[1]=&AudioInfo.pAudioGroups[0].pAudioChannels[2];    //Central and LFE channels
                  ppAudioChnPair[2]=&AudioInfo.pAudioGroups[1].pAudioChannels[0];    //Surround left and surround right channels

                  /* Create audio buffer for the 3 audio pairs */
                  for(int ChnPairIdx=0; ChnPairIdx<3; ChnPairIdx++)
                  {
                     ppAudioChnPair[ChnPairIdx]->pData = new BYTE[AudioBufferSize];
                     ppAudioChnPairData[ChnPairIdx] = (ULONG *)ppAudioChnPair[ChnPairIdx]->pData;
                  }

                  /* Create 5.1 audio buffer */
                  p51AudioBuffer = (ULONG *) new BYTE[AudioBufferSize*3];

                  /* Start stream */
                  if(ppAudioChnPair[0]->pData && ppAudioChnPair[1]->pData && ppAudioChnPair[2]->pData && p51AudioBuffer)
                  {
                     pSample=(SHORT *)p51AudioBuffer;

                     /* Start stream */
                     Result = VHD_StartStream(StreamHandle);
                     if (Result == VHDERR_NOERROR)
                     {
                        printf("\nTransmission started, press ESC to stop...\n");

                        /* Reception loop */
                        while (1)
                        {

                           if (kbhit()) 
                           {
                              getch();
                              break;
                           }

                           /* Try to lock next slot */
                           Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                           if (Result == VHDERR_NOERROR)
                           {    
                              /* Retrieve the number of needed samples */
                              for(int ChnPairIdx=0; ChnPairIdx<3; ChnPairIdx++)
                              {
                                 ppAudioChnPair[ChnPairIdx]->DataSize = 0;
                              }

                              Result = VHD_SlotEmbedAudio(SlotHandle,&AudioInfo);
                              if (Result != VHDERR_BUFFERTOOSMALL)
                              {
                                 printf("\nERROR : Cannot embed audio on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                 break;
                              }

                              /* Build audio samples */
                              NbOfSamples = ppAudioChnPair[0]->DataSize / BlockSize;

                              /* Silence on all channels */
                              memset(pSample, 0, ppAudioChnPair[0]->DataSize*3);

                              for(int SampleIdx=0; SampleIdx< NbOfSamples; SampleIdx++)
                              {
                                 pSample[NBOF_CHN*SampleIdx + ActiveChnIdx] = (SHORT)(AMPL * sin((double)2*PI*ToneFrequency*TotalSampleCnt++/SAMPLING_RATE)); /* 1kHz tone */
                                 if(++SampleCnt == NBOF_SAMPLE)
                                 {
                                    SampleCnt = 0;
                                    ActiveChnIdx = (ActiveChnIdx+1)%NBOF_CHN;
                                    if(ActiveChnIdx == 3)
                                       ToneFrequency = LOW_FREQ;
                                    else
                                       ToneFrequency = NORMAL_FREQ;
                                 }
                              }


                              /* Build the 3 stereo pairs by des-interleaving the 5.1 audio buffer */
                              for(unsigned int SampleIdx=0; SampleIdx < ppAudioChnPair[0]->DataSize/4; SampleIdx++)
                              {
                                 for(int ChnPairIdx=0; ChnPairIdx<3; ChnPairIdx++)
                                 {
                                    ppAudioChnPairData[ChnPairIdx][SampleIdx] = p51AudioBuffer[3*SampleIdx + ChnPairIdx];
                                 }
                              }

                              /* Embed audio */
                              Result = VHD_SlotEmbedAudio(SlotHandle,&AudioInfo);
                              if (Result != VHDERR_NOERROR)
                              {
                                 printf("\nERROR : Cannot embed audio on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                 break;
                              }

                              /* Unlock slot */
                              Result = VHD_UnlockSlotHandle(SlotHandle);
                              if (Result != VHDERR_NOERROR)
                              {
                                 printf("\nERROR : Cannot unlock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                 break;
                              }
                              /* Print some statistics */
                              VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                              printf("%u frames sent            \r",SlotsCount);
                              fflush(stdout);
                           }
                           else if (Result != VHDERR_TIMEOUT)
                           {
                              printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              break;
                           }
                        }

                        printf("\n");

                        /* Stop stream */
                        VHD_StopStream(StreamHandle);
                     }
                     else
                        printf("ERROR : Cannot start TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot allocate memory for test pattern buffer.\n");
                  
                  /* Delete 5.1 audio buffer */
                  if(p51AudioBuffer)
                     delete[] p51AudioBuffer;

                  /* Delete audio buffer */
                  for(int ChnPairIdx=0; ChnPairIdx<3; ChnPairIdx++)
                  {
                     if(ppAudioChnPair[ChnPairIdx]->pData)
                        delete[] ppAudioChnPair[ChnPairIdx]->pData;
                  }

                  /* Close stream handle */
                  VHD_CloseStreamHandle(StreamHandle);

               }
               else
                  printf("ERROR : Cannot open TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

               /* Re-establish RX0-TX0 by-pass relay loopthrough */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);

            }
            else
               printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
         }
         else
            printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));

      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


   close_keyboard();

   return 0;
}



