/*
   DELTA-hd/sdi/codec
   ------------
   UNCOMPRESSED 5.1 AUDIO RECEPTION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   -----------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK in an uncompressed 5.1 audio reception stream use case.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a reception 
   loop where audio extraction is performed.

   In order to compile this application, path to VideoMasterHD_Core.h, VideoMasterHD_Sdi.h 
   and VideoMasterHD_Sdi_Audio.h inclusion file and to VideoMasterHD.lib and 
   VideoMasterHD_Audio.lib library file must be properly configured.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>
#include <math.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Audio/VideoMasterHD_Sdi_Audio.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Audio.h"
#endif

#include "../../Tools.h"

#define CLOCK_SYSTEM    VHD_CLOCKDIV_1001

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             SlotsCount, SlotsDropped,VideoStandard,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
VHD_AUDIOINFO     AudioInfo;
VHD_AUDIOCHANNEL *ppAudioChnPair[3]={NULL, NULL, NULL};
ULONG            *ppAudioChnPairData[3]={NULL, NULL, NULL};
ULONG             NbOfSamples, AudioBufferSize;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
ULONG             ClockSystem;
ULONG             *p51AudioBuffer=NULL;

   init_keyboard();

   printf("\nAUDIO RECEPTION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }           

         /* Open a handle on first DELTA board */
         Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
         if (Result == VHDERR_NOERROR)
         {
            VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
            if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
            {
               /* Disable RX0-TX0 by-pass relay loopthrough */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

               /* Wait for channel locked */
               WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

               Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);
               if(Result == VHDERR_NOERROR)
               {
                  printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                  /* Create a logical stream to receive from RX0 connector */
                  Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
                  if (Result == VHDERR_NOERROR)
                  {
                     /* Get auto-detected video standard */
                     Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);
                     if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                     {
                        PrintVideoStandardInfo(VideoStandard);

                        Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,&Interface); 

                        if((Result == VHDERR_NOERROR) && (Interface != NB_VHD_INTERFACE))
                        {
                           PrintInterfaceInfo((VHD_INTERFACE)Interface);

                           /* Configure stream */
                           VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                           VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);
                           VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                           /* Configure audio info : 48kHz - 16 bits audio reception on channel 1 */
                           memset(&AudioInfo, 0, sizeof(VHD_AUDIOINFO));

                           /* 5.1 uncompressed audio is not natively supported by VideoMasterHD.
                              User has to received the 3 first channel pairs in stereo mode and build 
                              the 5.1 audio buffer "manually" by interleaving the 3 stereo pairs.
                           */

                           /* Configure audio info structure to setup 3 16-bit stereo receptions of the 3 first audio pairs */
                           AudioInfo.pAudioGroups[0].pAudioChannels[0].Mode=VHD_AM_STEREO;
                           AudioInfo.pAudioGroups[0].pAudioChannels[1].Mode=VHD_AM_STEREO;
                           AudioInfo.pAudioGroups[0].pAudioChannels[2].Mode=VHD_AM_STEREO;
                           AudioInfo.pAudioGroups[0].pAudioChannels[3].Mode=VHD_AM_STEREO;
                           AudioInfo.pAudioGroups[1].pAudioChannels[0].Mode=VHD_AM_STEREO;
                           AudioInfo.pAudioGroups[1].pAudioChannels[1].Mode=VHD_AM_STEREO;

                           AudioInfo.pAudioGroups[0].pAudioChannels[0].BufferFormat=VHD_AF_16;
                           AudioInfo.pAudioGroups[0].pAudioChannels[1].BufferFormat=VHD_AF_16;
                           AudioInfo.pAudioGroups[0].pAudioChannels[2].BufferFormat=VHD_AF_16;
                           AudioInfo.pAudioGroups[0].pAudioChannels[3].BufferFormat=VHD_AF_16;
                           AudioInfo.pAudioGroups[1].pAudioChannels[0].BufferFormat=VHD_AF_16;
                           AudioInfo.pAudioGroups[1].pAudioChannels[1].BufferFormat=VHD_AF_16;

                           /* Get the biggest audio frame size */
                           NbOfSamples = VHD_GetNbSamples((VHD_VIDEOSTANDARD)VideoStandard, CLOCK_SYSTEM, VHD_ASR_48000, 0);
                           AudioBufferSize = NbOfSamples*VHD_GetBlockSize(VHD_AF_16, VHD_AM_STEREO);

                           /* Point to the first channel of each audio pair */
                           ppAudioChnPair[0]=&AudioInfo.pAudioGroups[0].pAudioChannels[0];    //Left and right channels
                           ppAudioChnPair[1]=&AudioInfo.pAudioGroups[0].pAudioChannels[2];    //Central and LFE channels
                           ppAudioChnPair[2]=&AudioInfo.pAudioGroups[1].pAudioChannels[0];    //Surround left and surround right channels

                           /* Create audio buffer for the 3 audio pairs */
                           for(int ChnPairIdx=0; ChnPairIdx<3; ChnPairIdx++)
                           {
                              ppAudioChnPair[ChnPairIdx]->pData = new BYTE[AudioBufferSize];
                              ppAudioChnPairData[ChnPairIdx] = (ULONG *)ppAudioChnPair[ChnPairIdx]->pData;
                           }

                           /* Create 5.1 audio buffer */
                           p51AudioBuffer = (ULONG *) new BYTE[AudioBufferSize*3];

                           /* Start stream */
                           if(ppAudioChnPair[0]->pData && ppAudioChnPair[1]->pData && ppAudioChnPair[2]->pData && p51AudioBuffer)
                           {
                              Result = VHD_StartStream(StreamHandle);
                              if (Result == VHDERR_NOERROR)
                              {

                                 printf("\nReception started, press ESC to stop...\n");

                                 /* Reception loop */
                                 while (1)
                                 {
                                    if (kbhit()) 
                                    {
                                       getch();
                                       break;
                                    }

                                    /* Try to lock next slot */
                                    Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       /* Set the audio buffer size */
                                       for(int ChnPairIdx=0; ChnPairIdx<3; ChnPairIdx++)
                                       {
                                          ppAudioChnPair[ChnPairIdx]->DataSize = AudioBufferSize;
                                       }

                                       /* Extract audio */
                                       Result=VHD_SlotExtractAudio(SlotHandle, &AudioInfo);
                                       if(Result==VHDERR_NOERROR)
                                       {
                                          /* Check that the number of sample is the same for the 3 audio channels pairs */
                                          if(   (ppAudioChnPair[0]->DataSize == ppAudioChnPair[1]->DataSize)
                                             && (ppAudioChnPair[0]->DataSize == ppAudioChnPair[2]->DataSize))
                                          {
                                             /* Build the 5.1 audio buffer by interleaving the 3 stereo pairs */
                                             for(unsigned int SampleIdx=0; SampleIdx < ppAudioChnPair[0]->DataSize/4; SampleIdx++)
                                             {
                                                for(int ChnPairIdx=0; ChnPairIdx<3; ChnPairIdx++)
                                                {
                                                   p51AudioBuffer[3*SampleIdx + ChnPairIdx] = ppAudioChnPairData[ChnPairIdx][SampleIdx];
                                                }
                                             }

                                             /* Do audio processing here */
                                          }
                                          else
                                             printf("\nERROR : audio channels have different number of sample\n");
                                       }

                                       /* Unlock slot */
                                       VHD_UnlockSlotHandle(SlotHandle);

                                       /* Print some statistics */
                                       VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                       VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                       printf("%u frames received \r", SlotsCount,SlotsDropped);
                                       fflush(stdout);

                                    }
                                    else if (Result != VHDERR_TIMEOUT)
                                    {
                                       printf("\nERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       break;
                                    }
                                    else
                                       printf("Timeout \r");
                                 }

                                 printf("\n");

                                 /* Stop stream */
                                 VHD_StopStream(StreamHandle);
                              }
                              else
                                 printf("ERROR : Cannot start RX0 stream on DELTA board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                           }    
                           else
                              printf("ERROR : Cannot allocate memory for test pattern buffer.\n");

                           
                           /* Delete 5.1 audio buffer */
                           if(p51AudioBuffer)
                              delete[] p51AudioBuffer;

                           /* Delete audio buffer */
                           for(int ChnPairIdx=0; ChnPairIdx<3; ChnPairIdx++)
                           {
                              if(ppAudioChnPair[ChnPairIdx]->pData)
                                 delete[] ppAudioChnPair[ChnPairIdx]->pData;
                           }
                        }
                        else
                           printf("ERROR : Cannot detect incoming interface from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                     }
                     else
                        printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                     /* Close stream handle */
                     VHD_CloseStreamHandle(StreamHandle);

                  }
                  else
                     printf("ERROR : Cannot open RX0 stream on DELTA board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

               }
               else
                  printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

               /* Re-establish RX0-TX0 by-pass relay loopthrough */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
            }
            else
               printf("ERROR : The selected channel is not an SDI one\n");

            /* Close board handle */
            VHD_CloseBoardHandle(BoardHandle);
         }
         else
            printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



