/*
   DELTA-hd/sdi/codec
   ------------
   TRANSMISSION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-SFP board and of the 
   VideoMasterHD SDK in a transmission stream use case.

   The application opens board and stream handles, configures them, and 
   enters in a transmission loop.

   In order to compile this application, path to VideoMasterHD_Core.h, 
   VideoMasterHD_SFP.h and VideoMasterHD_Sdi.h inclusion file 
   and to VideoMasterHD.lib library file must be properly configured.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD/VideoMasterHD_Sfp.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sfp.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#define userQueryTxtDisplayON_keyboard()
#define userQueryTxtDisplayOFF_keyboard()
#endif
#define KEY_SIZE 16
#define FILE_PATH_SIZE 255

int main (int argc, char *argv[])
{

ULONG                Result,DllVersion,NbBoards,ChnType;
ULONG                SlotsCount, SlotsDropped,VideoStandard,BufferSize;
HANDLE               BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
BYTE                 *pPatternBuffer = NULL;
int                  Line = 0, x, y, Width, Height;
ULONG                BrdId=(argc>1)?atoi(argv[1]):0;
BOOL32               Interlaced=FALSE;
BYTE                 *pBuffer = NULL;
BOOL32               Valid=FALSE;
VHD_SFP_SERIAL_ID    SerialID,NewSerialID;
VHD_SFP_STATUS       Sts;
ULONG                Temperature_UL;
VHD_SFP_MODE         ModuleType=NB_VHD_SFP_MODE;
char                 pKey[KEY_SIZE],pReadValue[FILE_PATH_SIZE],cha,pFilePath[FILE_PATH_SIZE],pVendorID[KEY_SIZE+1];
float                Temperature_SF;
ULONG                NbRxRequired = 0, NbTxRequired = 1;

   memset(pVendorID,0,sizeof(pVendorID));
   
   init_keyboard();

   printf("\nTRANSMISSION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               Result = VHD_GetSFPModuleSerialID( BoardHandle, 0, &NewSerialID, &Sts);
               if(Result == VHDERR_SFPMODULELOCKED)
               {               
                  /* Module identification */               
                  memcpy(pVendorID,NewSerialID.pVendorPN,16);
                  printf("\nEnter the path to the module key file for SFP module 0, type %s : ",pVendorID);

                  userQueryTxtDisplayON_keyboard();                    
                  scanf("%s", pReadValue);
                  fflush(stdin);  
                  userQueryTxtDisplayOFF_keyboard();

                  memcpy(pFilePath,pReadValue,sizeof(pReadValue));
                  /* Open of key file */
                  FILE *pFile_c = fopen(pFilePath, "rb");
                  if(pFile_c)
                  {  /* Read of key value */
                     fread(pKey, 1, KEY_SIZE, pFile_c);
                     /* Authorise SFP module */
                     Result = VHD_AuthorizeSFPModule( BoardHandle, 0,pKey, &Sts);
                     if(Sts!=VHD_SFP_STS_UNLOCKED)
                     {
                        printf("\nError : Incorrect key value , module 0 not actived\n");
                     }
                     else 
                     {
                        printf("\nCorrect key file value, module 0 actived\n");
                     }
                     fclose(pFile_c);
                  }
                  else
                  {
                     printf("Error : Impossible to open %s file\n", pFilePath);
                  }                                                                                       
               }
               else if(Result ==VHDERR_NOSFPMODULE)
               { 
                  printf("Error : No SFP module\n");
               }

               Result = VHD_GetSFPModuleSerialID( BoardHandle, 0, &NewSerialID, &Sts);
               if(Result == VHDERR_NOERROR)
               {
                  VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_TX0_TYPE, &ChnType);
                  if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
                  {

                     /* Disable RX0-TX0 by-pass relay loopthrough */
                     VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                     /* Select a 1/1 clock system */
                     VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,VHD_CLOCKDIV_1);

                     /* Create a logical stream to transmit on TX0 connector */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_TX0,VHD_SDI_STPROC_DISJOINED_VIDEO,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        switch (ChnType)
                        {
                        case VHD_CHNTYPE_3GSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_60Hz; break;
                        case VHD_CHNTYPE_HDSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_30Hz; break;
                        case VHD_CHNTYPE_SDSDI: 
                        default: VideoStandard=VHD_VIDEOSTD_S259M_PAL; break;
                        }
                        /* Configure stream video standard */
                        VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                        VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_DEPTH,4);
                        VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_PRELOAD,2);

                        if(VideoStandard==VHD_VIDEOSTD_S259M_PAL)
                        {
                           /* Create a test pattern buffer based on this standard */
                           Width=720;
                           Height=576;
                           Interlaced=TRUE;

                           if ((pPatternBuffer = new BYTE[Width*Height*2]))
                           {
                              ULONG *pTemp = (ULONG*)pPatternBuffer;

                              /* Fill in buffer with 75% color bars */
                              for (y = 0; y < Height; y++)
                              {
                                 for (x = 0; x < 90; x+=2)  /*Y CrY Cb*/
                                    pTemp[((y*720)+x)>>1] = 0xB480B480;  /* 75% White */
                                 for (x = 90; x < 180; x+=2)
                                    pTemp[((y*720)+x)>>1] = 0xA18DA12C;  /* 75% Yellow */
                                 for (x = 180; x < 270; x+=2)
                                    pTemp[((y*720)+x)>>1] = 0x832C839C;  /* 75% Cyan */
                                 for (x = 270; x < 360; x+=2)
                                    pTemp[((y*720)+x)>>1] = 0x70397048;  /* 75% Green */
                                 for (x = 360; x < 450; x+=2)
                                    pTemp[((y*720)+x)>>1] = 0x53C653B7;  /* 75% Magenta */
                                 for (x = 450; x < 540; x+=2)
                                    pTemp[((y*720)+x)>>1] = 0x41D44163;  /* 75% Red */
                                 for (x = 540; x < 630; x+=2)
                                    pTemp[((y*720)+x)>>1] = 0x227222D4;  /* 75% Blue */
                                 for (x = 630; x < 720; x+=2)
                                    pTemp[((y*720)+x)>>1] = 0x10801080;  /* Black*/
                              }
                           }
                        }
                        else
                        {                     
                           /* Create a test pattern buffer based on this standard */
                           Width=1920;
                           Height=1080;
                           Interlaced=FALSE;

                           if ((pPatternBuffer = new BYTE[Width*Height*2]))
                           {
                              ULONG *pTemp = (ULONG*)pPatternBuffer;

                              /* Fill in buffer with 75% color bars */
                              for (y = 0; y < Height; y++)
                              {
                                 for (x = 0; x < 240; x+=2)
                                    pTemp[((y*1920)+x)>>1] = 0xB480B480;  /* 75% White */
                                 for (x = 240; x < 480; x+=2)
                                    pTemp[((y*1920)+x)>>1] = 0xA888A82C;  /* 75% Yellow */
                                 for (x = 480; x < 720; x+=2)
                                    pTemp[((y*1920)+x)>>1] = 0x912C9193;  /* 75% Cyan */
                                 for (x = 720; x < 960; x+=2)
                                    pTemp[((y*1920)+x)>>1] = 0x8534853F;  /* 75% Green */
                                 for (x = 960; x < 1200; x+=2)
                                    pTemp[((y*1920)+x)>>1] = 0x3FCC3FC1;  /* 75% Magenta */
                                 for (x = 1200; x < 1440; x+=2)
                                    pTemp[((y*1920)+x)>>1] = 0x33D4336D;  /* 75% Red */
                                 for (x = 1440; x < 1680; x+=2)
                                    pTemp[((y*1920)+x)>>1] = 0x1C781CD4;  /* 75% Blue */
                                 for (x = 1680; x < 1920; x+=2)
                                    pTemp[((y*1920)+x)>>1] = 0x10801080;  /* Black*/
                              }
                              x=1920;y=1080;
                           }
                        }

                        if (pPatternBuffer)
                        {
                           /* Start stream */
                           Result = VHD_StartStream(StreamHandle);
                           if (Result == VHDERR_NOERROR)
                           {                        
                              printf("\nTransmission started, press ESC to stop...\n");

                              /* Transmission loop */

                              while (1)
                              {                         
                                 if (kbhit()) 
                                 {
                                    cha=getch();
                                    /* VHD_SFPMODULE_CP_SOFT_TX_DISABLE =>TRUE */
                                    if((cha=='d')||(cha=='D'))
                                    {
                                       Result= VHD_SetSFPMoluleCtrl(BoardHandle,0,VHD_SFP_CP_SOFT_TX_DISABLE,1);
                                    }
                                    /* VHD_SFPMODULE_CP_SOFT_TX_DISABLE =>FALSE */
                                    else if((cha=='i')||(cha=='I'))
                                    {
                                       Result= VHD_SetSFPMoluleCtrl(BoardHandle,0,VHD_SFP_CP_SOFT_TX_DISABLE,0);
                                    }
                                    else
                                       break;
                                 }
                                 /* SFP monitoring */

                                 /* Get serial ID of module SFP */
                                 Result = VHD_GetSFPModuleSerialID( BoardHandle, 0, &NewSerialID, &Sts);
                                 if(Result == VHDERR_NOERROR)
                                 {  /* If Serial ID is valid */
                                    if(Sts=VHD_SFP_STS_UNLOCKED)
                                    {  /* If Vendor PN is not modified, else new module SFP */
                                       if(memcmp(SerialID.pVendorPN,NewSerialID.pVendorPN,16)==0)
                                       {  /* Get temperature of module SFP */
                                          Result= VHD_GetSFPMoluleStatus(BoardHandle,0,VHD_SFP_SP_TEMPERATURE,&Temperature_UL);
                                          if(Result == VHDERR_NOERROR)
                                          {
                                             Temperature_SF=Temperature_UL;
                                             Temperature_SF/=256;
                                             if(Temperature_SF>=128)
                                                Temperature_SF-=256;
                                             printf("Temperature module %d : %f deg\t",0,Temperature_SF);
                                          }
                                       }
                                       else
                                       {  /* Store new Vendor PN of module SFP */
                                          memcpy(SerialID.pVendorPN,NewSerialID.pVendorPN,16);
                                          /* Get module type */
                                          Result = VHD_GetSFPModuleType( BoardHandle, 0, &ModuleType);
                                          if(Result == VHDERR_NOERROR)
                                          {  /*Set table*/                                                                                   
                                             Result= VHD_SetMonitoredSFPModuleDefaultRegistersTable(BoardHandle,0,ModuleType,NULL,0);                                                                                                                                                                  
                                          }
                                       }                                
                                    }
                                 }
                                 else
                                    memset(SerialID.pVendorPN,0,16);


                                 /* Try to lock next slot */
                                 Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);

                                 if (Result == VHDERR_NOERROR)
                                 {
                                    Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO,&pBuffer,&BufferSize);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       /* Do TX data processing here on pBuffer */

                                       /* Copy test pattern buffer to slot */

                                       memcpy(pBuffer, pPatternBuffer, BufferSize);

                                       /* Draw white line */

                                       ULONG *pTemp;
                                       if(Interlaced)
                                       {
                                          if(Line%2 == 0)
                                             pTemp = (ULONG*)(pBuffer+(Line/2)*Width*2);
                                          else
                                             pTemp = (ULONG*)(pBuffer+(((Height+1)/2)+(Line/2))*Width*2);
                                       }
                                       else
                                          pTemp = (ULONG*)(pBuffer+Line*Width*2);

                                       for (x = 0; x < Width; x+=2)
                                          *pTemp++ = 0xEB80EB80;  /* 100% White */

                                       /* Increment line position */
                                       Line ++;
                                       if (Line > Height-1) Line = 0;

                                       /* Unlock slot */
                                       VHD_UnlockSlotHandle(SlotHandle);

                                       /* Print some statistics */
                                       VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                       VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                       printf("%u frames sent (%u dropped)                                          \r",SlotsCount,SlotsDropped);
                                       fflush(stdout);
                                    }
                                    else
                                    {
                                       printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       break;
                                    }
                                 }
                                 else
                                 {
                                    printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));                           
                                    break;
                                 }
                              }                       
                              printf("\n");

                              /* Stop stream */
                              VHD_StopStream(StreamHandle);
                           }
                           else
                              printf("ERROR : Cannot start TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                           /* Delete test pattern buffer */
                           delete[] pPatternBuffer;

                        }
                        else
                           printf("ERROR : Cannot allocate memory for test pattern buffer.\n");


                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


                     /* Re-establish RX0-TX0 by-pass relay loopthrough */
                     VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
                  }
                  else
                     printf("ERROR : The selected channel is not an SDI one\n");

               }

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);      
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


   close_keyboard();

   return 0;
}



