/*
   DELTA-hd/sdi/codec
   ------------
   TIMECODE TRANSMISSION SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK to transmit ATC (Ancillary data TimeCode).

   The application opens board and stream handles, configures them, and 
   enters in a transmission loop.

   In order to compile this application, path to VideoMasterHD_Core.h, 
   VideoMasterHD_Sdi.h and VideoMasterHD_Sdi_Timecode.h inclusion 
   file and to VideoMasterHD.lib library file must be properly configured.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD/VideoMasterHD_Sdi_Timecode.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Timecode.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             VideoStandard,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
VHD_TIMECODESLOT  Timecode;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
ULONG             NbRxRequired = 0, NbTxRequired = 1;
ULONG             FrameRate_UL, FrameCount_UL=0, MaxFrameCode_UL;
BOOL32            Evenparity_B;



   init_keyboard();

   printf("\nTIMECODE TRANSMISSION SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {

               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_TX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {

                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);


                  /* Select a 1/1 clock system */
                  VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,VHD_CLOCKDIV_1);

               /* Create a logical stream to transmit on TX0 connector */
               Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_TX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
               if (Result == VHDERR_NOERROR)
               {
                  switch (ChnType)
                  {
                  case VHD_CHNTYPE_3GSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_60Hz; Interface=VHD_INTERFACE_3G_A_425_1; FrameRate_UL = 60; break;
                  case VHD_CHNTYPE_HDSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_30Hz; Interface=VHD_INTERFACE_HD_292_1; FrameRate_UL = 30; break;
                  case VHD_CHNTYPE_SDSDI: 
                  default: VideoStandard=VHD_VIDEOSTD_S259M_PAL; Interface=VHD_INTERFACE_SD_259; FrameRate_UL = 25; break;
                  }

                     /* Configure stream video standard */
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                     /* Start stream */
                     Result = VHD_StartStream(StreamHandle);
                     if (Result == VHDERR_NOERROR)
                     {

                        /* Reset the structure */
                        memset(&Timecode,0,sizeof(Timecode));
                        Timecode.TcInfo.pTcLine[0] = 0;                    /* Let VideoMasterHD decide */
                        Timecode.TcInfo.SampleStream = VHD_SAMPLE_Y_AND_C; /* Let VideoMasterHD decide */
                        Timecode.TcInfo.TcType = VHD_TIMECODE_RP188_LTC;   /* Output LTC */

                        printf("\nTransmission started, press ESC to stop...\n");

                        /* Reception loop */
                        while (1)
                        {

                           if (kbhit()) 
                           {
                              getch();
                              break;
                           }

                           /* Try to lock next slot */
                           Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);

                        if (Result == VHDERR_NOERROR)
                        {
                           if(Interface == VHD_INTERFACE_HD_DUAL_372
                              || Interface == VHD_INTERFACE_3G_B_425_1_DL
                              || Interface == VHD_INTERFACE_4X3G_B_DL)
                           {
                              Result = VHD_GetSlotParity(SlotHandle,&Evenparity_B);
                              if (Result == VHDERR_NOERROR)
                              {
                                 if(!Evenparity_B)
                                 {
                                    /* Insert timecode  */
                                    Result = VHD_SlotEmbedTimecode(SlotHandle,Timecode);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       printf("%02d:%02d:%02d:%02d \r",
                                          Timecode.TcData.Hour,
                                          Timecode.TcData.Minute,
                                          Timecode.TcData.Second,
                                          Timecode.TcData.Frame);
                                       fflush(stdout);

                                       /* Increment timecode - Frame rate in dual link is > 30Hz and must be incremented for each odd frame*/
                                       if (++Timecode.TcData.Frame == (FrameRate_UL/2))
                                       {
                                          Timecode.TcData.Frame = 0;
                                          if (++Timecode.TcData.Second == 60)
                                          {
                                             Timecode.TcData.Second = 0;
                                             if (++Timecode.TcData.Minute == 60)
                                             {
                                                Timecode.TcData.Minute = 0;
                                                if (++Timecode.TcData.Hour == 24)
                                                {
                                                   Timecode.TcData.Hour = 0;
                                                }
                                             }
                                          }
                                       }
                                    }
                                    else
                                    {
                                       printf("\nERROR : Cannot insert timecode. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    }
                                 }
                              }
                              else
                                 printf("ERROR : Cannot get slot parity. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                           }
                           else
                           {
                              /* Insert timecode  */
                              Result = VHD_SlotEmbedTimecode(SlotHandle,Timecode);
                              if (Result == VHDERR_NOERROR)
                              {
                                 printf("%02d:%02d:%02d:%02d \r",
                                    Timecode.TcData.Hour,
                                    Timecode.TcData.Minute,
                                    Timecode.TcData.Second,
                                    Timecode.TcData.Frame);
                                 fflush(stdout);

                                 /* Increment timecode - frame rate > 30Hz are incremented on each pair of frames while frame rate <= 30Hz are incremented on each frame */
                                 MaxFrameCode_UL = (FrameRate_UL > 30)?(FrameRate_UL/2):FrameRate_UL;
                                 if((FrameRate_UL <= 30)
                                    || (FrameCount_UL%2 == 0))
                                 {
                                    if (++Timecode.TcData.Frame == MaxFrameCode_UL)
                                    {
                                       Timecode.TcData.Frame = 0;
                                       if (++Timecode.TcData.Second == 60)
                                       {
                                          Timecode.TcData.Second = 0;
                                          if (++Timecode.TcData.Minute == 60)
                                          {
                                             Timecode.TcData.Minute = 0;
                                             if (++Timecode.TcData.Hour == 24)
                                             {
                                                Timecode.TcData.Hour = 0;
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                              else
                              {
                                 printf("\nERROR : Cannot insert timecode. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              }
                           }

                           FrameCount_UL++;

                              /* Unlock slot */
                              VHD_UnlockSlotHandle(SlotHandle);

                           }
                           else if (Result != VHDERR_TIMEOUT)
                           {
                              printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              break;
                           }
                        }

                        printf("\n");

                        /* Stop stream */
                        VHD_StopStream(StreamHandle);
                     }
                     else
                        printf("ERROR : Cannot start TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


                     /* Close stream handle */
                     VHD_CloseStreamHandle(StreamHandle);

                  }
                  else
                     printf("ERROR : Cannot open TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");


               /* Re-establish RX0-TX0 by-pass relay loopthrough */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);           
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


   close_keyboard();

   return 0;
}



