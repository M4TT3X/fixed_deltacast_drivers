/*
   DELTA-hd/sdi/codec
   ------------
   RX-MODIFY-TX STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK in a RX-modify-TX stream use case.
 
   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a processing 
   loop wherein the incoming picture is modified and sent back to the 
   board.

   In order to compile this application, path to VideoMasterHD_Core.h, VideoMasterHD_Sdi.h 
   and VideoMasterHD_Sdi_Audio.h inclusion file and to VideoMasterHD.lib and 
   VideoMasterHD_Audio.lib library file must be properly configured.

   In order to run this application, the DELTA-hd/sdi/codec board RX0 connector must 
   be fed with a valid SD or HD SDI signal.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>
#include <math.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Audio/VideoMasterHD_Sdi_Audio.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Audio.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif


#define DELAY        (2*4800)   //Delay in number of sample (200 ms)
#define STRENGTH     40         //40%

BYTE *pPreviousSample;
int   n=0;

void EchoFilter(BYTE *pBuffer, ULONG NbOfSamples)
{
   SHORT *pPreviousSample_S = (SHORT*)pPreviousSample;
   SHORT *pBuffer_S = (SHORT*)pBuffer;
   SHORT Temp_S;

   for(ULONG i=0; i<NbOfSamples; i++)
   {
      Temp_S = pBuffer_S[2*i];
      pBuffer_S[2*i] = (SHORT)((STRENGTH * (int)pPreviousSample_S[2*n] + (100-STRENGTH) * (int)pBuffer_S[2*i])/100);
      pPreviousSample_S[2*n] = Temp_S;

      Temp_S = pBuffer_S[2*i+1];
      pBuffer_S[2*i+1] = (SHORT)((STRENGTH * (int)pPreviousSample_S[2*n+1] + (100-STRENGTH) * (int)pBuffer_S[2*i+1])/100);
      pPreviousSample_S[2*n+1] = Temp_S;

      n = (n+1)%DELAY;
   }
}

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards,NbFramesProcessed, NbFramesDropped;
ULONG             ChnType;
ULONG             VideoStandard,Interface;
ULONG             ClockSystem;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
VHD_AUDIOINFO     AudioInfo;
VHD_AUDIOCHANNEL  *pAudioChn;
ULONG             NbOfSamples = 0,LastNbOfSamples = 0,BlockSize,MaxNbOfSamples;
int               RawFrameHeight = 0;
BYTE              *pBuffer=NULL;
ULONG             BufferSize;
ULONG             ArtificialAudioFrameNb = 1;
bool              ArtificialGroupCtrlValid = FALSE, StartAudio = FALSE;
ULONG             NbRxRequired = 1, NbTxRequired = 1;

   init_keyboard();

   printf("\nRX-MODIFY-TX STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {

               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {

                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Select RX0 as the genlock source for clock system detection */
                  VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_GENLOCK_SOURCE,VHD_GENLOCK_RX0);

                  /* Wait for channel locked */
                  WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                  /* Auto-detect clock system */
                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);
                  if(Result == VHDERR_NOERROR)
                  {
                     printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                     /* Select the detected clock system */
                     VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,ClockSystem);

                     /* Create a logical stream to perform RX0-TX0 online processing */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0_MODIFY_TX0,VHD_SDI_STPROC_JOINED,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        /* Get auto-detected video standard */                     
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);
                        if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                        {
                           PrintVideoStandardInfo(VideoStandard);
								   RawFrameHeight = GetRawFrameHeight(VideoStandard);

                           Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,&Interface); 

                           if((Result == VHDERR_NOERROR) && (Interface != NB_VHD_INTERFACE))
                           {
                              PrintInterfaceInfo((VHD_INTERFACE)Interface);

                              /* Configure stream */
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_DEPTH,8);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_TX_GENLOCK,TRUE);
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);


                              /* Configure audio info */
                              memset(&AudioInfo, 0, sizeof(VHD_AUDIOINFO));
                              pAudioChn=&AudioInfo.pAudioGroups[0].pAudioChannels[0];
                              pAudioChn->Mode=AudioInfo.pAudioGroups[0].pAudioChannels[1].Mode=VHD_AM_STEREO;
                              pAudioChn->BufferFormat=AudioInfo.pAudioGroups[0].pAudioChannels[1].BufferFormat=VHD_AF_16;

                              /* Get the biggest audio frame size */
                              MaxNbOfSamples = VHD_GetNbSamples((VHD_VIDEOSTANDARD)VideoStandard, (VHD_CLOCKDIVISOR)ClockSystem, VHD_ASR_48000, 0);
                              BlockSize = VHD_GetBlockSize(pAudioChn->BufferFormat,pAudioChn->Mode);

                              /* Create an audio buffer and delay buffer */
                              pAudioChn->pData = new BYTE[MaxNbOfSamples*BlockSize];
                              pPreviousSample = new BYTE[DELAY*BlockSize];
                              memset(pPreviousSample,0,DELAY*BlockSize);
                              if(pAudioChn->pData && pPreviousSample)
                              {
                                 /* Start stream */
                                 Result = VHD_StartStream(StreamHandle);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    printf("\nProcessing started, press ESC to stop...\n");

                                    /* Reception loop */
                                    while (1)
                                    {

                                       if (kbhit()) 
                                       {
                                          getch();
                                          break;
                                       }

                                       /* Try to lock next slot */
                                       Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                       if (Result == VHDERR_NOERROR)
                                       {
                                          Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO,&pBuffer,&BufferSize);
                                          if (Result == VHDERR_NOERROR)
                                          {
                                             /* Do some processing on picture : put chroma samples to 0x80 */
                                             for (ULONG i = 0; i < BufferSize; i+=2)
                                             {
                                                pBuffer[i] = 0x80;
                                             }
                                          }
                                          else
                                          {
                                             printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                             break;
                                          }

                                          /* Retrieve the original samples */
                                          pAudioChn->DataSize = MaxNbOfSamples*BlockSize;
                                          Result = VHD_SlotExtractAudio(SlotHandle,&AudioInfo);
                                          if (Result != VHDERR_NOERROR)
                                          {
                                             printf("\nERROR : Cannot extract audio on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                             break;
                                          }

                                          /* Delete all ANC  */
                                          for (int LineNumber = 1; LineNumber <= RawFrameHeight; LineNumber++)
                                          {
                                             VHD_SlotAncRemovePacket(SlotHandle,LineNumber,VHD_SAMPLE_Y,-1,TRUE);
                                             VHD_SlotAncRemovePacket(SlotHandle,LineNumber,VHD_SAMPLE_C,-1,TRUE);
                                          }

                                          /* Do some processing on audio : add echo to first channel pair */
                                          LastNbOfSamples = NbOfSamples;
                                          NbOfSamples = pAudioChn->DataSize / BlockSize;   

                                          /* If no audio control packet is present for SD signals, the following code build one
                                          to get correct synchronisation. */                                       
                                          if (VideoStandard == VHD_VIDEOSTD_S259M_NTSC)
                                          {                                       
                                             if (LastNbOfSamples == NbOfSamples)
                                             {
                                                ArtificialGroupCtrlValid = TRUE;
                                                ArtificialAudioFrameNb = 1;
                                                StartAudio = TRUE;                                      

                                                AudioInfo.pAudioGroups[0].GroupCtrlValid = TRUE;
                                                AudioInfo.pAudioGroups[0].GroupCtrl.AudioFrameNb = ArtificialAudioFrameNb;
                                                AudioInfo.pAudioGroups[0].GroupCtrl.ActiveChannels = 15;
                                                ArtificialAudioFrameNb++;                                       
                                             }
                                             else if (ArtificialGroupCtrlValid)
                                             {
                                                AudioInfo.pAudioGroups[0].GroupCtrlValid = TRUE;
                                                AudioInfo.pAudioGroups[0].GroupCtrl.AudioFrameNb = ArtificialAudioFrameNb;
                                                AudioInfo.pAudioGroups[0].GroupCtrl.ActiveChannels = 15;
                                                ArtificialAudioFrameNb++;
                                             }
                                          }   
                                          else if (VideoStandard == VHD_VIDEOSTD_S259M_PAL)
                                          {
                                             StartAudio = TRUE;
                                             AudioInfo.pAudioGroups[0].GroupCtrlValid = TRUE;
                                             ArtificialGroupCtrlValid = TRUE;
                                             AudioInfo.pAudioGroups[0].GroupCtrl.AudioFrameNb = ArtificialAudioFrameNb;
                                             AudioInfo.pAudioGroups[0].GroupCtrl.ActiveChannels = 15;
                                             if (ArtificialAudioFrameNb == 5)
                                             {
                                                ArtificialAudioFrameNb = 1;
                                             }
                                             else
                                             {
                                                ArtificialAudioFrameNb++;
                                             }
                                          }
                                          else
                                          {
                                             StartAudio = TRUE;
                                          }

                                          if(NbOfSamples && StartAudio)
                                          {
                                             EchoFilter(pAudioChn->pData, NbOfSamples);

                                             /* Embed audio */
                                             Result = VHD_SlotEmbedAudio(SlotHandle,&AudioInfo);
                                             if (Result != VHDERR_NOERROR)
                                             {
                                                printf("\nERROR : Cannot embed audio on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                                break;
                                             }
                                          }

                                          /* Unlock slot */
                                          VHD_UnlockSlotHandle(SlotHandle);

                                          /* Print some statistics */
                                          VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&NbFramesProcessed);
                                          VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&NbFramesDropped);
                                          printf("%u frames processed (%u dropped)            \r",NbFramesProcessed,NbFramesDropped);
                                          fflush(stdout);
                                       }
                                       else if (Result != VHDERR_TIMEOUT)
                                       {
                                          printf("\nERROR : Cannot lock slot on RX0-TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                          break;
                                       }
                                       else
                                          printf("Timeout \r");
                                    }

                                    printf("\n");

                                    /* Stop stream */
                                    VHD_StopStream(StreamHandle);
                                 }
                                 else
                                    printf("ERROR : Cannot start RX0-TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              }
                              else
                                 printf("ERROR : Cannot allocate memory for test pattern buffer.\n");
                           }
                           else
                              printf("ERROR : Cannot detect incoming interface from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open RX0-TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Re-establish RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);

               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");
               
               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);        
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


   close_keyboard();


   return 0;
}



