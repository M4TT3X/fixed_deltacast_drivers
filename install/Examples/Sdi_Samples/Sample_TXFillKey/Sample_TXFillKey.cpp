/*
   DELTA-3G
   ------------
   FILL + KEY TRANSMISSION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-3G-elp-key-d 4K/2K, 
   DELTA-3G-elp-d 4c/8c boards and of the VideoMasterHD SDK 
   in a fill + key transmission stream use case.

   The application opens board and stream handles, configures them, and 
   enters in a transmission loop.

   In order to compile this application, path to VideoMasterHD_Core.h and 
   VideoMasterHD_Sdi.h inclusion file and to VideoMasterHD.lib library 
   file must be properly configured.

   On the DELTA-3G-elp-key-d 4K/2K the connectors RX0(TX Key) and TX0(TX Fill)
   will be used, on the DELTA-3G-elp-d 4c/8c the connectors will be the TX1(TX Key) 
   and TX0(TX Fill).

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

#define BITMAP_PATH     "DELTACAST_450x129_RGBA.raw"
#define BITMAP_WIDTH    450
#define BITMAP_HEIGHT   129

#define INTERFACE_VHD   VHD_INTERFACE_3G_A_DUAL
#define VIDEO_STANDARD  VHD_VIDEOSTD_S274M_1080p_60Hz

BOOL32 LoadBitmap (BYTE *pBitmap);
void CopyBitmap (BYTE *pSourceBitmap, BYTE *pDestBuffer, int DestPosX, int DestPosY, int BufferWidth, int BufferHeight, BOOL32 Interlaced=FALSE, BOOL32 Odd=TRUE);


int main (int argc, char *argv[])
{
ULONG             Result, DllVersion, NbBoards, ChnType;
ULONG             SlotsCount, SlotsDropped, BufferSize;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
BYTE             *pPatternBuffer = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
BOOL32            Interlaced=FALSE;

BYTE             *pBitmap = new BYTE[BITMAP_WIDTH*BITMAP_HEIGHT*4];
int              yPos=1, xPos=1, ySpeed=1, xSpeed=1;
int               FrameWidth = 0, FrameHeight = 0, Odd = TRUE;
BYTE             *pBuffer = NULL;
ULONG             NbRxRequired = 0, NbTxRequired = 2;

   init_keyboard();

   printf("\nFILL + KEY TRANSMISSION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Load resource bitmap from a raw RGBA file */
   if (LoadBitmap(pBitmap))
   {
      /* Query VideoMasterHD information */
      Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
      if (Result == VHDERR_NOERROR)
      {
         printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
            DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
            NbBoards,(NbBoards>1)?"s":"");

         if (NbBoards > 0)
         {
            /* Query DELTA boards information */
            for (ULONG i = 0; i < NbBoards; i++)
            {
               PrintBoardInfo(i);
            }

            /* Check the number of required channels */
            if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
            {
               /* Open a handle on first DELTA-hd/sdi/codec board */
               Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
               if (Result == VHDERR_NOERROR)
               {
                  VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_TX0_TYPE, &ChnType);
                  if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
                  {
                     /* Disable RX0-TX0 by-pass relay loopthrough */
                     VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                     /* Select a 1/1 clock system */
                     VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,VHD_CLOCKDIV_1);

                     /* Create a logical stream to transmit on TX0 connector */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_TX0,VHD_SDI_STPROC_DISJOINED_VIDEO,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        GetVideoCharacteristics(VIDEO_STANDARD, &FrameWidth, &FrameHeight, NULL, NULL);

                        VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,INTERFACE_VHD);
                        VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VIDEO_STANDARD);
                        VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_DEPTH,4);
                        VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_PRELOAD,2);
                        VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFER_PACKING,VHD_BUFPACK_VIDEO_RGBA_32);
                        
                        /* Start stream */
                        Result = VHD_StartStream(StreamHandle);
                        if (Result == VHDERR_NOERROR)
                        {                        
                           printf("\nTransmission started, press ESC to stop...\n");

                           /* Transmission loop */

                           while (1)
                           {
                              if (kbhit()) 
                              {
                                 getch();
                                 break;
                              }

                              /* Try to lock next slot */
                              Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                              if (Result == VHDERR_NOERROR)
                              {
                                 Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO,&pBuffer,&BufferSize);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    /* Fill in the video buffer */
                                    memset(pBuffer,0,BufferSize);

                                    for(int i=0; i<(Interlaced?2:1); i++)
                                    {
                                       CopyBitmap(pBitmap,pBuffer,
                                          xPos,yPos,
                                          FrameWidth,FrameHeight,Interlaced,Odd);

                                       if((xPos==FrameWidth-BITMAP_WIDTH-1) || (xPos==0))
                                          xSpeed= -xSpeed;

                                       if((yPos==FrameHeight-BITMAP_HEIGHT-1) || (yPos==0))
                                          ySpeed= -ySpeed;

                                       xPos += xSpeed;
                                       yPos += ySpeed;
                                       Odd=!Odd;
                                    }  
                                 }
                                 else
                                 {
                                    printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    break;
                                 }

                                 /* Unlock slot */
                                 VHD_UnlockSlotHandle(SlotHandle);

                                 /* Print some statistics */
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                 printf("%u frames sent (%u dropped)            \r",SlotsCount,SlotsDropped);
                                 fflush(stdout);
                              }
                              else
                              {
                                 printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                 break;
                              }
                           }

                           printf("\n");

                           /* Stop stream */
                           VHD_StopStream(StreamHandle);
                        }
                        else
                           printf("ERROR : Cannot start TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                     
                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


                     /* Re-establish RX0-TX0 by-pass relay loopthrough */
                     VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
                  }
                  else
                     printf("ERROR : The selected channel is not an SDI one\n");

                  /* Close board handle */
                  VHD_CloseBoardHandle(BoardHandle);
               }
               else
                  printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));

            }
            else
               printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);       
         }
         else
            printf("No DELTA board detected, exiting...\n");
      }
      else
         printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
   }
   else
      printf("ERROR : Cannot load resource bitmap.\n");

   close_keyboard();

   return 0;
}

BOOL32 LoadBitmap (BYTE *pBitmap)
{
   BOOL32     Result = FALSE;
   FILE    *pFile = fopen(BITMAP_PATH,"rb");
   size_t   NbBytesRead = 0;

   if (pFile)
   {
      NbBytesRead = fread(pBitmap,1,BITMAP_WIDTH*BITMAP_HEIGHT*4,pFile);
      if (NbBytesRead == BITMAP_WIDTH*BITMAP_HEIGHT*4)
         Result = TRUE;

      fclose(pFile);
   }

   return Result;
}

void CopyBitmap (BYTE *pSourceBitmap, BYTE *pDestBuffer, int DestPosX, int DestPosY, int BufferWidth, int BufferHeight, BOOL32 Interlaced, BOOL32 Odd)
{
   int   y;

   if (Interlaced)
   {
      if(Odd)
      {
         if(DestPosY&1)
            y = DestPosY+1;
         else
            y = DestPosY;
      }
      else
      {
         if(DestPosY&1)
            y = DestPosY;
         else
            y = DestPosY+1;
      }

      for (; ((y < DestPosY+BITMAP_HEIGHT) && (y < BufferHeight)); y+=2)
      {
         if (!Odd)
         {
            if (DestPosX+BITMAP_WIDTH < BufferWidth)
               memcpy(pDestBuffer+((DestPosX+((((BufferHeight+1)/2)+(y/2))*BufferWidth))*4),
               pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
               BITMAP_WIDTH*4);
            else
               memcpy(pDestBuffer+((DestPosX+((((BufferHeight+1)/2)+(y/2))*BufferWidth))*4),
               pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
               (BufferWidth-DestPosX)*4);
         }
         else
         {
            if (DestPosX+BITMAP_WIDTH < BufferWidth)
               memcpy(pDestBuffer+((DestPosX+((y/2)*BufferWidth))*4),
               pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
               BITMAP_WIDTH*4);
            else
               memcpy(pDestBuffer+((DestPosX+((y/2)*BufferWidth))*4),
               pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
               (BufferWidth-DestPosX)*4);
         }
      }
   }
   else
   {
      for (y = DestPosY; ((y < DestPosY+BITMAP_HEIGHT) && (y < BufferHeight)); y++)
      {
         if (DestPosX+BITMAP_WIDTH < BufferWidth)
            memcpy(pDestBuffer+((DestPosX+(y*BufferWidth))*4),
            pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
            BITMAP_WIDTH*4);
         else
            memcpy(pDestBuffer+((DestPosX+(y*BufferWidth))*4),
            pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
            (BufferWidth-DestPosX)*4);
      }
   }
}



