/*
   DELTA-hd/sdi/codec
   ------------
   VBI DATA TRANSMISSION SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK to transmit vbi data stream.

   The application opens board and stream handles, configures them, and 
   enters in a transmission loop where it generates an OP42 slot to be embedded 
   within the outgoing SDI signal. 

   In order to compile this application, path to VideoMasterHD_Core.h and 
   VideoMasterHD_Sdi.h inclusion file and to VideoMasterHD.lib library 
   file must be properly configured
*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_VbiData/VideoMasterHD_Sdi_VbiData.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_VbiData.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             SlotsCount=0, VideoStandard,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
int               LineLength=0;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
VHD_OP42_SLOT     OP42Slot;
ULONG             NbRxRequired = 0, NbTxRequired = 1;

BYTE              pCCTable[13] =   {0xD,//Double Height
                                    0xB,//Start Box
                                    0xB,//Start Box
                                    'D',
                                    'E',
                                    'L',
                                    'T',
                                    'A',
                                    'C',
                                    'A',
                                    'S',
                                    'T',
                                    0xA //End Box
                                    };

   init_keyboard();

   printf("\nVBI TRANSMISSION SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }    

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_TX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {

                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Select a 1 clock system */
                  VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,VHD_CLOCKDIV_1);

                  /* Create a logical stream to transmit on TX0 connector */
                  Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_TX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
                  if (Result == VHDERR_NOERROR)
                  {
                     /* Set PAL for OP42 */
                     VideoStandard=VHD_VIDEOSTD_S259M_PAL;
                     Interface=VHD_INTERFACE_SD_259;

                     /* Configure stream video standard */
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);   
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);               

                     /* Start stream */
                     Result = VHD_StartStream(StreamHandle);
                     if (Result == VHDERR_NOERROR)
                     {

                        printf("\nTransmission started, press ESC to stop...\n");

                        /* Transmission loop */
                        while (1)
                        {

                           if (kbhit()) 
                           {
                              getch();
                              break;
                           }

                           /* Try to lock next slot */
                           Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);

                           if (Result == VHDERR_NOERROR)
                           {
                              /* Send caption "DELTACAST" every 100 frames */
                              memset(&OP42Slot, 0, sizeof(VHD_OP42_SLOT));                           
                              if (SlotsCount%100 == 0)
                              {
                                 /* Odd field */
                                 /* Send header : page 801 */
                                 OP42Slot.pTTPacket[0].MagazineNumber = 0;
                                 OP42Slot.pTTPacket[0].PacketNumber = 0;

                                 OP42Slot.pTTPacket[0].TTHeader.TTPageNumber.PageTens = 0;
                                 OP42Slot.pTTPacket[0].TTHeader.TTPageNumber.PageUnits = 1;
                                 OP42Slot.pTTPacket[0].TTHeader.TTControlBits.C4_ErasePage = TRUE;
                                 OP42Slot.pTTPacket[0].TTHeader.TTControlBits.C6_Subtitle = TRUE;
                                 OP42Slot.pTTPacket[0].TTHeader.TTControlBits.C8_UpdateIndicator = TRUE;

                                 /* Even field */
                                 /* Send text : packet number 22 */
                                 OP42Slot.pTTPacket[1].MagazineNumber = 0;
                                 OP42Slot.pTTPacket[1].PacketNumber = 22;
                                 memcpy(OP42Slot.pTTPacket[1].pTTData, pCCTable, 13);
                              }
                              else if (SlotsCount%100 == 1)
                              {
                                 /* Odd field */
                                 /* Repeat the text */
                                 OP42Slot.pTTPacket[0].MagazineNumber = 0;
                                 OP42Slot.pTTPacket[0].PacketNumber = 22;
                                 memcpy(OP42Slot.pTTPacket[0].pTTData, pCCTable, 13);

                                 /* Even field */
                                 /* Send dummy headers */
                                 OP42Slot.pTTPacket[1].MagazineNumber = 0;
                                 OP42Slot.pTTPacket[1].PacketNumber = 0;

                                 OP42Slot.pTTPacket[1].TTHeader.TTPageNumber.PageTens = 15;
                                 OP42Slot.pTTPacket[1].TTHeader.TTPageNumber.PageUnits = 15;
                              }
                              else
                              {
                                 /* Odd field */
                                 /* Send dummy headers */
                                 OP42Slot.pTTPacket[0].MagazineNumber = 0;
                                 OP42Slot.pTTPacket[0].PacketNumber = 0;

                                 OP42Slot.pTTPacket[0].TTHeader.TTPageNumber.PageTens = 15;
                                 OP42Slot.pTTPacket[0].TTHeader.TTPageNumber.PageUnits = 15;

                                 /* Even field */
                                 /* Send dummy headers */
                                 OP42Slot.pTTPacket[1].MagazineNumber = 0;
                                 OP42Slot.pTTPacket[1].PacketNumber = 0;

                                 OP42Slot.pTTPacket[1].TTHeader.TTPageNumber.PageTens = 15;
                                 OP42Slot.pTTPacket[1].TTHeader.TTPageNumber.PageUnits = 15;
                              }

                              Result = VHD_SlotEmbedOP42Subtitles(SlotHandle, &OP42Slot); 
                              if (Result != VHDERR_NOERROR)
                              {
                                 printf("ERROR : Cannot embed OP42. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              }                              

                              /* Unlock slot */
                              VHD_UnlockSlotHandle(SlotHandle);

                              /* Print some statistics */
                              VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                              printf("%u frames sent            \r",SlotsCount);
                              fflush(stdout);
                           }
                           else if (Result != VHDERR_TIMEOUT)
                           {
                              printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              break;
                           }
                        }

                        printf("\n");

                        /* Stop stream */
                        VHD_StopStream(StreamHandle);
                     }
                     else
                        printf("ERROR : Cannot start TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                     /* Close stream handle */
                     VHD_CloseStreamHandle(StreamHandle);

                  }
                  else
                     printf("ERROR : Cannot open TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Re-establish RX0-TX0 by-pass relay loopthrough */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);           
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


   close_keyboard();

   return 0;
}



