/*
   DELTA-hd
   --------
   3D RECEPTION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd board and of the 
   VideoMasterHD SDK in a stereoscopic reception stream use case.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a reception 
   loop.

   In order to compile this application, path to VideoMasterHD_Core.h and 
   VideoMasterHD_Sdi.h inclusion file and to VideoMasterHD.lib library 
   file must be properly configured.

   In order to run this application, the DELTA-hd board RX0 and RX1 connectors
   must be fed with valid and genlocked SD or HD SDI signal, typically 
   conveying left and right view of some 3D content.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             SlotsCount, SlotsDropped,VideoStandard,BufferSize,BufferSize2,Status,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
BYTE             *pBuffer=NULL,*pBuffer2=NULL;
int               NbAncPackets,NbAncPackets2;
VHD_ANCPACKET    *pAncPacket = NULL,*pAncPacket2 = NULL;
ULONG             ClockSystem;
ULONG             NbRxRequired = 2, NbTxRequired = 0;

   init_keyboard();

   printf("\n3D RECEPTION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }           

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {
                  /* Disable RX0-TX0 and RX1-TX1 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,FALSE);

                  /* Wait for channel locked */
                  WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                  /* Auto-detect clock system */
                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);

                  if(Result == VHDERR_NOERROR)
                  {
                     printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                     /* Create a logical stream to receive from RX0 and RX1 (coupled) connector */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_COUPLED_RX01,VHD_SDI_STPROC_JOINED,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        /* Get auto-detected video standard */
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);
                        if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                        {
                           PrintVideoStandardInfo(VideoStandard);

                           Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,&Interface); 

                           if((Result == VHDERR_NOERROR) && (Interface != NB_VHD_INTERFACE))
                           {
                              PrintInterfaceInfo((VHD_INTERFACE)Interface);

                              /* Configure stream */
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                              /* Start stream */
                              Result = VHD_StartStream(StreamHandle);
                              if (Result == VHDERR_NOERROR)
                              {

                                 printf("\nStereoscopic reception started, press ESC to stop...\n");

                                 /* Reception loop */
                                 while (1)
                                 {

                                    if (kbhit()) 
                                    {
                                       getch();
                                       break;
                                    }

                                    /* Try to lock next slot */
                                    Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO,&pBuffer,&BufferSize);
                                       if (Result == VHDERR_NOERROR)
                                       {
                                          /* Do left RX data processing here on pBuffer */
                                       }
                                       else
                                       {
                                          printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                          break;
                                       }
                                       Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO_2,&pBuffer2,&BufferSize2);
                                       if (Result == VHDERR_NOERROR)
                                       {
                                          /* Do right RX data processing here on pBuffer */
                                       }
                                       else
                                       {
                                          printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                          break;
                                       }

                                       Result = VHD_SlotAncGetNbPackets(SlotHandle,1,VHD_SAMPLE_Y,&NbAncPackets);
                                       if (Result == VHDERR_NOERROR)
                                       {
                                          if (NbAncPackets > 0)
                                          {
                                             pAncPacket = NULL;
                                             Result = VHD_SlotAncGetPacket(SlotHandle,1,VHD_SAMPLE_Y,0,&pAncPacket);
                                             if (Result == VHDERR_NOERROR)
                                             {
                                                /* Do left ANC data processing here on pAncPacketLeft */
                                             }
                                             else
                                             {
                                                printf("ERROR : Cannot get left ANC packet 0 on line 1 - Y samples. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                             }
                                          }
                                          fflush(stdout);
                                       }
                                       else
                                       {
                                          printf("ERROR : Cannot get number of left packets on line 1 - Y samples. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       }

                                       Result = VHD_SlotAncGetNbPackets(SlotHandle,1,VHD_SAMPLE_Y_2,&NbAncPackets2);
                                       if (Result == VHDERR_NOERROR)
                                       {
                                          if (NbAncPackets2 > 0)
                                          {
                                             pAncPacket = NULL;
                                             Result = VHD_SlotAncGetPacket(SlotHandle,1,VHD_SAMPLE_Y_2,0,&pAncPacket2);
                                             if (Result == VHDERR_NOERROR)
                                             {
                                                /* Do left ANC data processing here on pAncPacketLeft */
                                             }
                                             else
                                             {
                                                printf("ERROR : Cannot get right ANC packet 0 on line 1 - Y samples. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                             }
                                          }
                                          fflush(stdout);
                                       }
                                       else
                                       {
                                          printf("ERROR : Cannot get number of tight packets on line 1 - Y samples. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       }


                                       /* Unlock slot */
                                       VHD_UnlockSlotHandle(SlotHandle);                              
                                       fflush(stdout);

                                       /* Print some statistics */
                                       VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                       VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                       VHD_GetBoardProperty(BoardHandle,VHD_CORE_BP_RX0_STATUS,&Status);
                                       printf("%u frames received (%u dropped - ",SlotsCount,SlotsDropped);
                                       if(Status&VHD_SDI_RXSTS_ALIGNED_COUPLE)
                                          printf("Aligned)    \r");
                                       else
                                          printf("Not Aligned)    \r");
                                    }
                                    else if (Result != VHDERR_TIMEOUT)
                                    {
                                       printf("\nERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       break;
                                    }
                                    else
                                       printf("Timeout \r");
                                 }

                                 printf("\n");

                                 /* Stop stream */
                                 VHD_StopStream(StreamHandle);
                              }
                              else
                                 printf("ERROR : Cannot start RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                           }
                           else
                              printf("ERROR : Cannot detect incoming interface from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Re-establish RX0-TX0 and RX1-TX1 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,TRUE);
               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);         
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



