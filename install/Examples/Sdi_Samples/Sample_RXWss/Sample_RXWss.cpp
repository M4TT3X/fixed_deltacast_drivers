/*
   DELTA-hd/sdi/codec
   ------------
   VBI DATA RECEPTION SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK to receive vbi data stream.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a reception 
   loop. In the reception loop, the application use VideoMasterHD to look for and 
   to extract WSS carried on in VBI line.

   In order to compile this application, path to VideoMasterHD_Core.h and 
   VideoMasterHD_Sdi.h inclusion file and to VideoMasterHD.lib library 
   file must be properly configured.

   In order to run this application, the DELTA-hd/sdi/codec board RX0 connector must 
   be fed with a valid SD or HD SDI signal containing VBI data.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_VbiData/VideoMasterHD_Sdi_VbiData.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_VbiData.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

#define WSS_LINE     23

#define GETBIT(pBitBuffer,BitNumber) (((pBitBuffer)[(BitNumber)/8]>>(7-((BitNumber)%8)))&1)

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             VideoStandard,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
VHD_VBILINE       pCaptureLines[VHD_MAXNB_VBICAPTURELINE];
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
ULONG             ClockSystem;
VHD_WSS_SLOT      WssSlot;
ULONG             NbRxRequired = 1, NbTxRequired = 0;

   init_keyboard();

   printf("\nVBI RECEPTION SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {

                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Wait for channel locked */
                  WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                  /* Auto-detect clock system */
                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);
                  if(Result == VHDERR_NOERROR)
                  {
                     printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                     /* Create a logical stream to receive from RX0 connector */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        /* Get auto-detected video standard */
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);
                        if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                        {
                           PrintVideoStandardInfo(VideoStandard);

                           Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,&Interface); 

                           if((Result == VHDERR_NOERROR) && (Interface != NB_VHD_INTERFACE))
                           {
                              PrintInterfaceInfo((VHD_INTERFACE)Interface);

                              /* Configure stream */
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                              /* Set WSS line */
                              memset(&WssSlot, 0, sizeof(VHD_WSS_SLOT));
                              WssSlot.LineNumber = WSS_LINE;

                              /* Set line to capture */
                              memset(pCaptureLines,0,VHD_MAXNB_VBICAPTURELINE * sizeof(VHD_VBILINE));
                              pCaptureLines[0].CaptureFromC = TRUE;
                              pCaptureLines[0].CaptureFromY = FALSE;
                              pCaptureLines[0].LineNumber = WSS_LINE;

                              Result = VHD_VbiSetCaptureLines(StreamHandle, pCaptureLines);
                              if (Result == VHDERR_NOERROR)
                              {
                                 /* Start stream */
                                 Result = VHD_StartStream(StreamHandle);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    printf("\nReception started, press ESC to stop...\n");

                                    /* Reception loop */
                                    while (1)
                                    {

                                       if (kbhit()) 
                                       {
                                          getch();
                                          break;
                                       }

                                       /* Try to lock next slot */
                                       Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                       if (Result == VHDERR_NOERROR) 
                                       {                                
                                          /* Extract WSS Slot */
                                          Result = VHD_SlotExtractWSS(SlotHandle, &WssSlot);
                                          if (Result == VHDERR_NOERROR)
                                          {
                                             /* Group 1 */
                                             if ((WssSlot.WSSData&0xF)==0x8)
                                             {
                                                printf("Aspect ratio label : 4:3\n"  
                                                   "Format : Full format\n"
                                                   "Position : Not applicable\n"
                                                   "Active lines : 576\n");
                                             }
                                             else if ((WssSlot.WSSData&0xF)==0x1)
                                             {
                                                printf("Aspect ratio label : 14:9\n"  
                                                   "Format : Letterbox\n"
                                                   "Position : Center\n"
                                                   "Active lines : 504\n");
                                             }
                                             else if ((WssSlot.WSSData&0xF)==0x2)
                                             {
                                                printf("Aspect ratio label : 14:9\n"  
                                                   "Format : Letterbox\n"
                                                   "Position : Top\n"
                                                   "Active lines : 504\n");
                                             }
                                             else if ((WssSlot.WSSData&0xF)==0xB)
                                             {
                                                printf("Aspect ratio label : 16:9\n"  
                                                   "Format : Letterbox\n"
                                                   "Position : Center\n"
                                                   "Active lines : 430\n");
                                             }
                                             else if ((WssSlot.WSSData&0xF)==0x4)
                                             {
                                                printf("Aspect ratio label : 16:9\n"  
                                                   "Format : Letterbox\n"
                                                   "Position : Top\n"
                                                   "Active lines : 430\n");
                                             }
                                             else if ((WssSlot.WSSData&0xF)==0xD)
                                             {
                                                printf("Aspect ratio label : > 16:9\n"  
                                                   "Format : Letterbox\n"
                                                   "Position : Center\n"
                                                   "Active lines : Not applicable\n");
                                             }
                                             else if ((WssSlot.WSSData&0xF)==0xE)
                                             {
                                                printf("Aspect ratio label : > 14:9\n"  
                                                   "Format : Full format\n"
                                                   "Position : Center\n"
                                                   "Active lines : 576\n");
                                             }
                                             else if ((WssSlot.WSSData&0xF)==0x7)
                                             {
                                                printf("Aspect ratio label : 16:9\n"  
                                                   "Format : Full format (anamorphic)\n"
                                                   "Position : Not applicable\n"
                                                   "Active lines : 576\n");
                                             }

                                             /* Group 2 */
                                             if (GETBIT((BYTE*)&(WssSlot.WSSData), 4) == 1)
                                                printf("Film mode\n"); 
                                             else
                                                printf("Camera mode\n"); 

                                             if (GETBIT((BYTE*)&(WssSlot.WSSData), 5) == 1)
                                                printf("Motion Adaptive Colour Plus\n"); 
                                             else
                                                printf("Standard coding\n"); 

                                             if (GETBIT((BYTE*)&(WssSlot.WSSData), 6) == 1)
                                                printf("Modulated helper\n"); 
                                             else
                                                printf("No Helper\n"); 

                                             /* Group 3 */
                                             if (GETBIT((BYTE*)&(WssSlot.WSSData), 8) == 1)
                                                printf("Subtitles within teletext\n"); 
                                             else
                                                printf("No subtitles within teletext\n"); 

                                             if ((GETBIT((BYTE*)&(WssSlot.WSSData), 10) == 0)&&(GETBIT((BYTE*)&(WssSlot.WSSData), 9) == 0))
                                                printf("No open subtitles \n"); 
                                             else if ((GETBIT((BYTE*)&(WssSlot.WSSData), 9) == 0)&&(GETBIT((BYTE*)&(WssSlot.WSSData), 10) == 1))
                                                printf("Subtitles inside active image \n"); 
                                             else if ((GETBIT((BYTE*)&(WssSlot.WSSData), 9) == 1)&&(GETBIT((BYTE*)&(WssSlot.WSSData), 10) == 0))
                                                printf("Subtitles outside active image \n"); 

                                             /* Group 4 */
                                             if (GETBIT((BYTE*)&(WssSlot.WSSData), 11) == 1)
                                                printf("Film mode\n"); 
                                             else
                                                printf("Camera mode\n"); 

                                             if (GETBIT((BYTE*)&(WssSlot.WSSData), 12) == 1)
                                                printf("Copyright asserted\n"); 
                                             else
                                                printf("No copyright asserted or status unknown\n"); 

                                             if (GETBIT((BYTE*)&(WssSlot.WSSData), 13) == 1)
                                                printf("Copying restricted\n"); 
                                             else
                                                printf("Copying not restricted\n"); 

                                             /* Quit if valid WSS found */
                                             break;                                          
                                          }

                                          /* Unlock slot */ 
                                          VHD_UnlockSlotHandle(SlotHandle); 
                                       }
                                       else if (Result != VHDERR_TIMEOUT)
                                       {
                                          printf("ERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                          break;
                                       }
                                       else
                                          printf("Timeout \r");
                                    }

                                    printf("\r\n");

                                    /* Stop stream */
                                    VHD_StopStream(StreamHandle);
                                 }
                                 else
                                    printf("ERROR : Cannot start RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              }
                              else
                                 printf("ERROR : Cannot set vbi line to capture. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                           }
                           else
                              printf("ERROR : Cannot detect incoming interface from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Re-establish RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);           
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



