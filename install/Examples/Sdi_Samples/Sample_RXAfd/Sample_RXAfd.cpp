/*
   DELTA-hd/sdi/codec
   ------------
   AFD DATA RECEPTION SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK to receive Afd data.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a reception 
   loop. In the reception loop, the application use VideoMasterHD to look for and 
   to extract AFD carried on in VBI line or ANC packet.

   In order to compile this application, path to VideoMasterHD_Core.h, 
   VideoMasterHD_Sdi.h and VideoMasterHD_Sdi_VbiData.h inclusion file and to VideoMasterHD.lib library 
   file must be properly configured.

   In order to run this application, the DELTA-hd/sdi/codec board RX0 connector must 
   be fed with a valid SD-SDI signal containing VBI data or HD-SDI signal containing ANC data.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_VbiData/VideoMasterHD_Sdi_VbiData.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_VbiData.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

#define AFD_LINE     11

const char * GetAfdArCodeDescription(ULONG AfdArCode);

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             VideoStandard,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
VHD_VBILINE       pCaptureLines[VHD_MAXNB_VBICAPTURELINE];
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
ULONG             ClockSystem;
VHD_AFD_AR_SLOT   AfdArSlot;
ULONG             NbRxRequired = 1, NbTxRequired = 0;

   init_keyboard();

   printf("\nAFD RECEPTION SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {

                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Wait for channel locked */
                  WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                  /* Auto-detect clock system */
                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);
                  if(Result == VHDERR_NOERROR)
                  {
                     printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                     /* Create a logical stream to receive from RX0 connector */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        /* Get auto-detected video standard */
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);
                        if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                        {
                           PrintVideoStandardInfo(VideoStandard);

                           Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,&Interface); 

                           if((Result == VHDERR_NOERROR) && (Interface != NB_VHD_INTERFACE))
                           {
                              PrintInterfaceInfo((VHD_INTERFACE)Interface);

                              /* Configure stream */
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                              /* Set Afd line */
                              memset(&AfdArSlot, 0, sizeof(VHD_AFD_AR_SLOT));
                              AfdArSlot.LineNumber = 0;

                              if(Interface==VHD_INTERFACE_SD_259)
                              {
                                 /* Set line to capture */
                                 memset(pCaptureLines,0,VHD_MAXNB_VBICAPTURELINE * sizeof(VHD_VBILINE));
                                 pCaptureLines[0].CaptureFromC = TRUE;
                                 pCaptureLines[0].CaptureFromY = FALSE;
                                 pCaptureLines[0].LineNumber = AFD_LINE;

                                 Result = VHD_VbiSetCaptureLines(StreamHandle, pCaptureLines);
                              }


                              if (Result == VHDERR_NOERROR)
                              {
                                 /* Start stream */
                                 Result = VHD_StartStream(StreamHandle);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    printf("\nReception started, press ESC to stop...\n");

                                    /* Reception loop */
                                    while (1)
                                    {

                                       if (kbhit()) 
                                       {
                                          getch();
                                          break;
                                       }

                                       /* Try to lock next slot */
                                       Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                       if (Result == VHDERR_NOERROR) 
                                       {                                
                                          /* Extract Afd Slot */
                                          Result = VHD_SlotExtractAFD(SlotHandle, &AfdArSlot); 
                                          if(Result == VHDERR_NOERROR)
                                             printf("Afd and AR code received : 0x%02X (%s) \r", AfdArSlot.AFD_ARCode,GetAfdArCodeDescription(AfdArSlot.AFD_ARCode));               

                                          /* Unlock slot */ 
                                          VHD_UnlockSlotHandle(SlotHandle); 
                                       }  
                                       else if (Result != VHDERR_TIMEOUT)
                                       {
                                          printf("ERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                          break;
                                       }
                                       else
                                          printf("Timeout                                                                                                                               \r");
                                    }

                                    printf("\r\n");

                                    /* Stop stream */
                                    VHD_StopStream(StreamHandle);
                                 }
                                 else
                                    printf("ERROR : Cannot start RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              }
                              else
                                 printf("ERROR : Cannot set vbi line to capture. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                           }
                           else
                              printf("ERROR : Cannot detect incoming interface from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Re-establish RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);           
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}

const char * GetAfdArCodeDescription(ULONG AfdArCode)
{
   switch (AfdArCode)
   {
   case VHD_AFD_AR_U4_3 :                 return "Undefined 4:3";
   case VHD_AFD_AR_U16_9 :                return "Undefined 16:9";
   case VHD_AFD_AR_TLB16_9 :              return "Letterbox 16:9 image, at top of the coded frame";
   case VHD_AFD_AR_FF16_9 :               return "Full frame 16:9 image, the same as the coded frame";
   case VHD_AFD_AR_TLB14_9 :              return "Letterbox 14:9 image, at top of the coded frame";
   case VHD_AFD_AR_HCPB14_9 :             return "Pillarbox 14:9 image, horizontally centered in the coded frame";
   case VHD_AFD_AR_LBARG4_3 :             return "Letterbox image with an aspect ratio greater than 16:9, vertically centered in the coded frame";
   case VHD_AFD_AR_LBARG16_9 :            return "Letterbox image with an aspect ratio greater than 16:9, vertically centered in the coded frame";
   case VHD_AFD_AR_FF4_3 :                return "Full frame 4:3 image, the same as the coded frame";
   case VHD_AFD_AR_FF16_9P :              return "Full frame 16:9 image, the same as the coded frame";
   case VHD_AFD_AR_FF4_3P :               return "Full frame 4:3 image, the same as the coded frame";
   case VHD_AFD_AR_HCPB4_3 :              return "Pillarbox 4:3 image, horizontally centered in the coded frame";
   case VHD_AFD_AR_VCLB16_9 :             return "Letterbox 16:9 image, vertically centered in the coded frame with all image areas protected";
   case VHD_AFD_AR_AIAPFF16_9 :           return "Full frame 16:9 image, with all image areas protected";
   case VHD_AFD_AR_VCLB14_9 :             return "Letterbox 14:9 image, vertically centered in the coded frame";
   case VHD_AFD_AR_HCPB14_9P :            return "Pillarbox 14:9 image, horizontally centered in the coded frame";
   case VHD_AFD_AR_FF4_3A14_9C :          return "Full frame 4:3 image, with alternative 14:9 center";
   case VHD_AFD_AR_PB4_3A14_9C :          return "Pillarbox 4:3 image, with alternative 14:9 center";
   case VHD_AFD_AR_LB16_9A14_9C :         return "Letterbox 16:9 image, with alternative 14:9 center";
   case VHD_AFD_AR_FF16_9A14_9C :         return "Full frame 16:9 image, with alternative 14:9 center";
   case VHD_AFD_AR_LB16_9A4_3C :          return "Letterbox 16:9 image, with alternative 4:3 center";
   case VHD_AFD_AR_FF16_9A4_3C :          return "Full frame 16:9 image, with alternative 4:3 center";
   case NB_VHD_AFD_AR_CODE :              return "Undefined AFD and AR code";
   }
}



