/*
   DELTA-hd/sdi/codec
   ------------
   VBI DATA DIGITALIZATION SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK to receive and digitalize vbi data stream.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a reception 
   loop.

   In order to compile this application, path to VideoMasterHD_Core.h, VideoMasterHD_Sdi.h and 
   VideoMasterHD_Sdi_Vbi.h inclusion files and to VideoMasterHD.lib and 
   VideoMasterHD_VBI.lib library files must be properly configured.

   In order to run this application, the DELTA-hd/sdi/codec board RX0 connector must 
   be fed with a valid SD or HD SDI signal containing D-VITC signal (as per 
   SMPTE 266M).

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Vbi/VideoMasterHD_Sdi_Vbi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Vbi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

#define GETBIT(pBitBuffer,BitNumber) ((pBitBuffer[BitNumber/8]>>(7-(BitNumber%8)))&1)


int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             VideoStandard,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
BYTE             *pLineBuffer = NULL;
ULONG             LineBufferSize = 0;
BYTE              pVbiBuffer[12];
char              pTimeCodeString[12];
ULONG             VbiBufferSizeInBits = 90;
VHD_VBILINE       pCaptureLines[VHD_MAXNB_VBICAPTURELINE];
VHD_SAMPLETYPE    SourceSample;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
ULONG             ClockSystem;
ULONG             NbRxRequired = 1, NbTxRequired = 0;


   init_keyboard();

   printf("\nVBI DIGITALIZATION SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }  

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {

               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {

                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Wait for channel locked */
                  WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                  /* Auto-detect clock system */
                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);

                  if(Result == VHDERR_NOERROR)
                  {
                     printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                     /* Create a logical stream to receive from RX0 connector */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        /* Get auto-detected video standard */
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);
                        if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                        {
                           PrintVideoStandardInfo(VideoStandard);

                           Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,&Interface); 

                           if((Result == VHDERR_NOERROR) && (Interface != NB_VHD_INTERFACE))
                           {
                              PrintInterfaceInfo((VHD_INTERFACE)Interface);

                              /* Configure stream */
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                              /* Configure VBI capture on line 14 */
                              memset(pCaptureLines,0,VHD_MAXNB_VBICAPTURELINE * sizeof(VHD_VBILINE));
                              pCaptureLines[0].LineNumber = 14;
                              if ((VideoStandard == VHD_VIDEOSTD_S259M_PAL) || (VideoStandard == VHD_VIDEOSTD_S259M_NTSC))
                              {
                                 /* If SD signal, interleaved Y and C VBI data are stored on C samples buffer */
                                 SourceSample = VHD_SAMPLE_C;
                                 pCaptureLines[0].CaptureFromC = TRUE;
                                 pCaptureLines[0].CaptureFromY = FALSE;
                              }
                              else
                              {
                                 /* If HD signal, capture VBI from Y samples */
                                 SourceSample = VHD_SAMPLE_Y;
                                 pCaptureLines[0].CaptureFromC = FALSE;
                                 pCaptureLines[0].CaptureFromY = TRUE;
                              }

                              Result = VHD_VbiSetCaptureLines(StreamHandle, pCaptureLines);
                              if (Result == VHDERR_NOERROR)
                              {
                                 /* Start stream */
                                 Result = VHD_StartStream(StreamHandle);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    printf("\nReception started, press ESC to stop...\n");

                                    /* Reset timecode string */
                                    pTimeCodeString[0] = 'n';
                                    pTimeCodeString[1] = '/';
                                    pTimeCodeString[2] = 'a';
                                    pTimeCodeString[3] = 0;

                                    /* Reception loop */
                                    while (1)
                                    {

                                       if (kbhit()) 
                                       {
                                          getch();
                                          break;
                                       }

                                       /* Try to lock next slot */
                                       Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);

                                       if (Result == VHDERR_NOERROR)
                                       {
                                          /* Get VBI content */
                                          Result = VHD_SlotVbiGetLine(SlotHandle, pCaptureLines[0].LineNumber, SourceSample, &pLineBuffer, &LineBufferSize);
                                          if (Result == VHDERR_NOERROR)
                                          {
                                             memset(pVbiBuffer,0,12);
                                             VbiBufferSizeInBits = 90;
                                             /* Try digitalizing line 14 at 1.8MHz to obtain 90 data bits (see SMPTE266M - Annex C) */
                                             Result = VHD_VbiDigitalize(pLineBuffer,
                                                LineBufferSize,
                                                ((VideoStandard != VHD_VIDEOSTD_S259M_PAL) && (VideoStandard != VHD_VIDEOSTD_S259M_NTSC)),
                                                1800000.0,
                                                pVbiBuffer,
                                                &VbiBufferSizeInBits);
                                             if (Result == VHDERR_NOERROR)
                                             {
                                                /* Check synchronization bits (see SMPTE266M - Figure 1) */
                                                if ((GETBIT(pVbiBuffer,0)  == 1) && (GETBIT(pVbiBuffer,1)  == 0) && 
                                                   (GETBIT(pVbiBuffer,10) == 1) && (GETBIT(pVbiBuffer,11) == 0) && 
                                                   (GETBIT(pVbiBuffer,20) == 1) && (GETBIT(pVbiBuffer,21) == 0) && 
                                                   (GETBIT(pVbiBuffer,30) == 1) && (GETBIT(pVbiBuffer,31) == 0) && 
                                                   (GETBIT(pVbiBuffer,40) == 1) && (GETBIT(pVbiBuffer,41) == 0) && 
                                                   (GETBIT(pVbiBuffer,50) == 1) && (GETBIT(pVbiBuffer,51) == 0) && 
                                                   (GETBIT(pVbiBuffer,60) == 1) && (GETBIT(pVbiBuffer,61) == 0) && 
                                                   (GETBIT(pVbiBuffer,70) == 1) && (GETBIT(pVbiBuffer,71) == 0) && 
                                                   (GETBIT(pVbiBuffer,80) == 1) && (GETBIT(pVbiBuffer,81) == 0))
                                                {
                                                   /* Build timecode string */
                                                   /* Tens of hours */
                                                   pTimeCodeString[0] = '0' + ((GETBIT(pVbiBuffer,73)<<1) + GETBIT(pVbiBuffer,72));
                                                   /* Units of hours */
                                                   pTimeCodeString[1] = '0' + ((GETBIT(pVbiBuffer,65)<<3) + (GETBIT(pVbiBuffer,64)<<2) + (GETBIT(pVbiBuffer,63)<<1) + GETBIT(pVbiBuffer,62));
                                                   /* Separator */
                                                   pTimeCodeString[2] = ':';
                                                   /* Tens of minutes */
                                                   pTimeCodeString[3] = '0' + ((GETBIT(pVbiBuffer,54)<<2) + (GETBIT(pVbiBuffer,53)<<1) + GETBIT(pVbiBuffer,52));
                                                   /* Units of minutes */
                                                   pTimeCodeString[4] = '0' + ((GETBIT(pVbiBuffer,45)<<3) + (GETBIT(pVbiBuffer,44)<<2) + (GETBIT(pVbiBuffer,43)<<1) + GETBIT(pVbiBuffer,42));
                                                   /* Separator */
                                                   pTimeCodeString[5] = ':';
                                                   /* Tens of seconds */
                                                   pTimeCodeString[6] = '0' + ((GETBIT(pVbiBuffer,34)<<2) + (GETBIT(pVbiBuffer,33)<<1) + GETBIT(pVbiBuffer,32));
                                                   /* Units of seconds */
                                                   pTimeCodeString[7] = '0' + ((GETBIT(pVbiBuffer,25)<<3) + (GETBIT(pVbiBuffer,24)<<2) + (GETBIT(pVbiBuffer,23)<<1) + GETBIT(pVbiBuffer,22));
                                                   /* Separator */
                                                   pTimeCodeString[8] = ':';
                                                   /* Tens of frames */
                                                   pTimeCodeString[9] = '0' + ((GETBIT(pVbiBuffer,13)<<1) + GETBIT(pVbiBuffer,12));
                                                   /* Units of frames */
                                                   pTimeCodeString[10] = '0' + ((GETBIT(pVbiBuffer,5)<<3) + (GETBIT(pVbiBuffer,4)<<2) + (GETBIT(pVbiBuffer,3)<<1) + GETBIT(pVbiBuffer,2));
                                                   /* Terminator */
                                                   pTimeCodeString[11] = 0;
                                                }
                                             }
                                          }
                                          else
                                             printf("ERROR : Cannot get VBI data. Result = 0x%08X (%s)\r\n",Result, GetErrorDescription(Result));

                                          /* Print timecode */
                                          printf("D-VITC : %s            \r",pTimeCodeString);
                                          fflush(stdout);

                                          /* Unlock slot */
                                          VHD_UnlockSlotHandle(SlotHandle);
                                       }
                                       else if (Result != VHDERR_TIMEOUT)
                                       {
                                          printf("ERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                          break;
                                       }
                                       else
                                          printf("Timeout \r");
                                    }

                                    printf("\r\n");

                                    /* Stop stream */
                                    VHD_StopStream(StreamHandle);
                                 }
                                 else
                                    printf("ERROR : Cannot start RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              }
                              else
                                 printf("ERROR : Cannot set vbi line to capture. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                           }
                           else
                              printf("ERROR : Cannot detect incoming interface from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Re-establish RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);           
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



