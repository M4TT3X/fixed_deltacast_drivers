/*
   DELTA-hd/sdi/codec
   ------------
   AC3 AUDIO RECEPTION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd/sdi/codec board and of the 
   VideoMasterHD SDK in a AC3 (compressed) audio reception stream use case.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a reception 
   loop where audio extraction is performed.

   In order to compile this application, path to VideoMasterHD_Core.h, VideoMasterHD_Sdi.h 
   and VideoMasterHD_Sdi_Audio.h inclusion file and to VideoMasterHD.lib and 
   VideoMasterHD_Audio.lib library file must be properly configured.

   Please refer to SMPTE 340M standard for more information about AC3 data embedding in AES3 Non-PCM audio.
*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>
#include <math.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Audio/VideoMasterHD_Sdi_Audio.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Audio.h"
#endif

#include "../../Tools.h"

#define CLOCK_SYSTEM    VHD_CLOCKDIV_1001

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

/*
Structure of an AC-3 Data Burst for 16-bit mode - See SMPTE 340M

|<-- 1536 Audio Samples periods (32ms @ 48kHz) -->|<-- 1536 Audio Samples periods (32ms @ 48kHz) -->|
|<----------------- Data Burst ------------------>|<----------------- Data Burst ------------------>|
|           |<- Burst payload ->|                 |           |<- Burst payload ->|                 |
|Pa|Pb|Pc|Pd|  AC-3 Sync Frame  |PADDING (silence)|Pa|Pb|Pc|Pd|  AC-3 Sync Frame  |PADDING (silence)|

*/
#define BURSTPREAMBLE_PA 0xF872 //(16-bit mode)
#define BURSTPREAMBLE_PB 0x4E1F //(16-bit mode)
#define AC3FILENAME "RXaudio.ac3"

void RecordAC3(VHD_AUDIOCHANNEL *pAudioChn, FILE *pFile_X, ULONG *pEncodingBitRate_UL);

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             SlotsCount, SlotsDropped,VideoStandard,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
VHD_AUDIOINFO     AudioInfo;
VHD_AUDIOCHANNEL *pAudioChn=NULL;
ULONG             NbOfSamples,AudioBufferSize;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
ULONG             ClockSystem;
ULONG             NbRxRequired = 1, NbTxRequired = 0;
FILE              *pFile_X = NULL;
ULONG             EncodingBitRate_UL = 0;

   init_keyboard();

   printf("\nAUDIO RECEPTION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      pFile_X = fopen(AC3FILENAME,"wb");
      if(!pFile_X)
      {
         printf("Error: file %s cannot be opened for writing",AC3FILENAME);
         return 0;
      }

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }       

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {
                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Wait for channel locked */
                  WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);
                  if(Result == VHDERR_NOERROR)
                  {
                     printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                     /* Create a logical stream to receive from RX0 connector */
                     Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0,VHD_SDI_STPROC_DISJOINED_ANC,NULL,&StreamHandle,NULL);
                     if (Result == VHDERR_NOERROR)
                     {
                        /* Get auto-detected video standard */
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);
                        if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                        {
                           PrintVideoStandardInfo(VideoStandard);

                           Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,&Interface); 

                           if((Result == VHDERR_NOERROR) && (Interface != NB_VHD_INTERFACE))
                           {
                              PrintInterfaceInfo((VHD_INTERFACE)Interface);

                              /* Configure stream */
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);

                              /* Configure audio info : 48kHz - 16 bits audio reception on channel 1 */
                              memset(&AudioInfo, 0, sizeof(VHD_AUDIOINFO));
                              pAudioChn=&AudioInfo.pAudioGroups[0].pAudioChannels[0];
                              pAudioChn->Mode=AudioInfo.pAudioGroups[0].pAudioChannels[1].Mode=VHD_AM_STEREO;
                              pAudioChn->BufferFormat=AudioInfo.pAudioGroups[0].pAudioChannels[1].BufferFormat=VHD_AF_16;

                              /* Get the biggest audio frame size */
                              NbOfSamples = VHD_GetNbSamples((VHD_VIDEOSTANDARD)VideoStandard, CLOCK_SYSTEM, VHD_ASR_48000, 0);
                              AudioBufferSize = NbOfSamples*VHD_GetBlockSize(pAudioChn->BufferFormat, pAudioChn->Mode);

                              /* Create audio buffer */
                              pAudioChn->pData = new BYTE[AudioBufferSize];
                              if(pAudioChn->pData)
                              {
                                 /* Start stream */
                                 Result = VHD_StartStream(StreamHandle);
                                 if (Result == VHDERR_NOERROR)
                                 {

                                    printf("\nReception started, press ESC to stop...\n");

                                    /* Reception loop */
                                    while (1)
                                    {
                                       if (kbhit()) 
                                       {
                                          getch();
                                          break;
                                       }

                                       /* Try to lock next slot */
                                       Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                       if (Result == VHDERR_NOERROR)
                                       {
                                          /* Set the audio buffer size */
                                          pAudioChn->DataSize = AudioBufferSize;

                                          /* Extract audio */
                                          Result=VHD_SlotExtractAudio(SlotHandle, &AudioInfo);
                                          if(Result==VHDERR_NOERROR)
                                          {
                                             /* Do audio processing here */
                                             RecordAC3(pAudioChn, pFile_X,&EncodingBitRate_UL);
                                          }

                                          /* Unlock slot */
                                          VHD_UnlockSlotHandle(SlotHandle);

                                          /* Print some statistics */
                                          VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                          VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                          printf("%u frames received (%u dropped) Audio%s detected on channel 1 - %u bps                  \r",
                                             SlotsCount,SlotsDropped,pAudioChn->DataSize?"":" not",EncodingBitRate_UL);
                                          fflush(stdout);

                                       }
                                       else if (Result != VHDERR_TIMEOUT)
                                       {
                                          printf("\nERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                          break;
                                       }
                                       else
                                          printf("Timeout \r");
                                    }

                                    printf("\n");

                                    /* Stop stream */
                                    VHD_StopStream(StreamHandle);
                                 }
                                 else
                                    printf("ERROR : Cannot start RX0 stream on DELTA board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                                 /* Delete audio buffer */
                                 delete[] pAudioChn->pData;
                              }
                              else
                                 printf("ERROR : Cannot alloc bytes of memory\n");
                           }
                           else
                              printf("ERROR : Cannot detect incoming interface from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);

                     }
                     else
                        printf("ERROR : Cannot open RX0 stream on DELTA board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  }
                  else
                     printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Re-establish RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);  
      }
      else
         printf("No DELTA board detected, exiting...\n");

         fclose(pFile_X);
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}

void RecordAC3(VHD_AUDIOCHANNEL *pAudioChn, FILE *pFile_X, ULONG *pEncodingBitRate_UL)
{
   static BOOL32  BurstPreamble_PA_Found_B = FALSE;
   static BOOL32  BurstPreamble_PB_Found_B = FALSE;
   static BOOL32  BurstPreamble_PC_Found_B = FALSE;
   static BOOL32  BurstPreamble_PD_Found_B = FALSE;
   static WORD    BurstPreamble_PC_UW = 0;
   static WORD    BurstPreamble_PD_UW = 0;
   static WORD    BurstDataCount_UW = 0;

   for(ULONG i=0; i< (pAudioChn->DataSize/2); i++)
   {
      if(!BurstPreamble_PA_Found_B)
      {
         //Find Burst Preamble PA
         if(pAudioChn->pData[2*i] == (BURSTPREAMBLE_PA&0xff)
            && pAudioChn->pData[2*i+1] == ((BURSTPREAMBLE_PA&0xff00)>>8))
         {
            BurstPreamble_PA_Found_B = TRUE;
         }      
      }
      else if(BurstPreamble_PA_Found_B 
         && !BurstPreamble_PB_Found_B)
      {
         //Find Burst Preamble PB
         if(pAudioChn->pData[2*i] == (BURSTPREAMBLE_PB&0xff)
            && pAudioChn->pData[2*i+1] == ((BURSTPREAMBLE_PB&0xff00)>>8))
         {
            BurstPreamble_PB_Found_B = TRUE;
         }
         else
         {
            BurstPreamble_PA_Found_B = FALSE;    
         }
      }
      else if(BurstPreamble_PA_Found_B 
         && BurstPreamble_PB_Found_B 
         && !BurstPreamble_PC_Found_B)
      {
         //Read Burst Preamble PC
         BurstPreamble_PC_UW = pAudioChn->pData[2*i] + (pAudioChn->pData[2*i+1]<<8);
         BurstPreamble_PC_Found_B = TRUE;
      }
      else if(BurstPreamble_PA_Found_B 
         && BurstPreamble_PB_Found_B 
         && BurstPreamble_PC_Found_B
         && !BurstPreamble_PD_Found_B)
      {
         //Read Burst Preamble PD
         BurstPreamble_PD_UW = pAudioChn->pData[2*i] + (pAudioChn->pData[2*i+1]<<8);
         BurstPreamble_PD_Found_B = TRUE;
      }
      else if(BurstPreamble_PA_Found_B 
         && BurstPreamble_PB_Found_B 
         && BurstPreamble_PC_Found_B 
         && BurstPreamble_PD_Found_B)
      {
         //Read Burst data
         if(BurstDataCount_UW<(BurstPreamble_PD_UW/16))
         {
            if(pFile_X)
            {
               fwrite(&pAudioChn->pData[2*i+1],1,1,pFile_X);
               fwrite(&pAudioChn->pData[2*i],1,1,pFile_X);
            }
            BurstDataCount_UW++;
         }
         else
         {
            BurstDataCount_UW = 0;
            BurstPreamble_PA_Found_B = FALSE;
            BurstPreamble_PB_Found_B = FALSE;
            BurstPreamble_PC_Found_B = FALSE;
            BurstPreamble_PD_Found_B = FALSE;
         }
      }
   }
   if(pEncodingBitRate_UL)
      *pEncodingBitRate_UL = (ULONG)(BurstPreamble_PD_UW*48000/1536);
}



