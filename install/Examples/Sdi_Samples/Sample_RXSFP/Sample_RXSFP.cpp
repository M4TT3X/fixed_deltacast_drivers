 /*
   DELTA-hd/sdi/codec
   -------------
   RECEPTION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-SFP
   board and of the VideoMasterHD SDK in a reception stream use case.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, and enters in a reception 
   loop.

   In order to compile this application, path to VideoMasterHD_Core.h, 
   VideoMasterHD_SFP.h and VideoMasterHD_Sdi.h inclusion file 
   and to VideoMasterHD.lib library file must be properly configured.

   In order to run this application, the DELTA-hd/sdi/codec board RX0 connector
   must be fed with a valid SD or HD SDI signal.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD/VideoMasterHD_Sfp.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sfp.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#define userQueryTxtDisplayON_keyboard()
#define userQueryTxtDisplayOFF_keyboard()
#endif

#define KEY_SIZE 16
#define FILE_PATH_SIZE 255


int main (int argc, char *argv[])
{
ULONG                Result,DllVersion,NbBoards,ChnType;
ULONG                SlotsCount, SlotsDropped,VideoStandard,BufferSize;
HANDLE               BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
ULONG                BrdId=(argc>1)?atoi(argv[1]):0;
BYTE                 *pBuffer=NULL;
ULONG                ClockSystem;
VHD_SFP_SERIAL_ID    SerialID,NewSerialID;
VHD_SFP_STATUS       Sts;
ULONG                Temperature_UL;
VHD_SFP_MODE         ModuleType=NB_VHD_SFP_MODE;
char                 pFilePath[FILE_PATH_SIZE],pReadValue[FILE_PATH_SIZE],pKey[KEY_SIZE],pVendorID[KEY_SIZE+1];
float                Temperature_SF;
ULONG                NbRxRequired = 1, NbTxRequired = 0;

   memset(pVendorID,0,sizeof(pVendorID));

   init_keyboard();

   printf("\nRECEPTION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
      DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
      NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               Result = VHD_GetSFPModuleSerialID( BoardHandle, 0, &NewSerialID, &Sts);
               if(Result == VHDERR_SFPMODULELOCKED)
               {              
                  /* Module identification */

                  memcpy(pVendorID,NewSerialID.pVendorPN,16);
                  printf("\nEnter the path to the module key file for SFP module 0, type %s : ",pVendorID);

                  userQueryTxtDisplayON_keyboard();
                  scanf("%s", pReadValue);
                  fflush(stdin);
                  userQueryTxtDisplayOFF_keyboard();

                  memcpy(pFilePath,pReadValue,sizeof(pReadValue)); 
                  /* Open of key file */
                  FILE *pFile_c = fopen(pFilePath, "rb");
                  if(pFile_c)
                  {  /* Read of key value */
                     fread(pKey, 1, KEY_SIZE, pFile_c);
                     /* Authorise SFP module */
                     Result = VHD_AuthorizeSFPModule( BoardHandle, 0,pKey, &Sts);
                     if(Sts!=VHD_SFP_STS_UNLOCKED)
                     {
                        printf("\nError : Incorrect key value , module 0 not actived\n");
                     }
                     else 
                     {
                        printf("\nCorrect key file value, module 0 actived\n");
                     }
                     fclose(pFile_c);
                  }
                  else
                  {
                     printf("Error : Impossible to open %s file\n", pFilePath);
                  }                                                                                       
               }
               else if(Result ==VHDERR_NOSFPMODULE)
               { 
                  printf("Error : No SFP module\n");
               } 

               Result = VHD_GetSFPModuleSerialID( BoardHandle, 0, &NewSerialID, &Sts);
               if(Result == VHDERR_NOERROR)
               {
                  VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_RX0_TYPE, &ChnType);
                  if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
                  {
                     /* Disable RX0-TX0 by-pass relay loopthrough */
                     VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                     /* Wait for channel locked */

                     WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                     /* Auto-detect clock system */
                     Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_CLOCK_DIV,&ClockSystem);

                     if(Result == VHDERR_NOERROR)
                     {
                        printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                        /* Create a logical stream to receive from RX0 connector */
                        Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0,VHD_SDI_STPROC_DISJOINED_VIDEO,NULL,&StreamHandle,NULL);
                        if (Result == VHDERR_NOERROR)
                        {
                           /* Get auto-detected video standard */                     
                           Result = VHD_GetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,&VideoStandard);

                           if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                           {
                              PrintVideoStandardInfo((VHD_VIDEOSTANDARD)VideoStandard);

                              /* Configure stream */
                              VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                              VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);

                              /* Start stream */
                              Result = VHD_StartStream(StreamHandle);
                              if (Result == VHDERR_NOERROR)
                              {

                                 printf("\nReception started, press ESC to stop...\n");

                                 /* Reception loop */
                                 while (1)
                                 {

                                    if (kbhit()) 
                                    {
                                       getch();
                                       break;
                                    }

                                    /* SFP monitoring */

                                    /* Get serial ID of module SFP */
                                    Result = VHD_GetSFPModuleSerialID( BoardHandle, 0, &NewSerialID, &Sts);
                                    if(Result == VHDERR_NOERROR)
                                    {  /* If Serial ID is valid */
                                       if(Sts==VHD_SFP_STS_UNLOCKED)
                                       {  /* If Vendor PN is not modified, else new module SFP */
                                          if(memcmp(SerialID.pVendorPN,NewSerialID.pVendorPN,16)==0)
                                          {  /*Get temperature of module SFP*/
                                             Result= VHD_GetSFPMoluleStatus(BoardHandle,0,VHD_SFP_SP_TEMPERATURE,&Temperature_UL);
                                             if(Result == VHDERR_NOERROR)
                                             {
                                                Temperature_SF=Temperature_UL;
                                                Temperature_SF/=256;
                                                if(Temperature_SF>=128)
                                                   Temperature_SF-=256;
                                                printf("Temperature module %d : %f deg\t",0,Temperature_SF);
                                             }
                                          }
                                          else
                                          {  /* Store new Vendor PN of module SFP */ 
                                             memcpy(SerialID.pVendorPN,NewSerialID.pVendorPN,16);
                                             /* Get module type */
                                             Result = VHD_GetSFPModuleType( BoardHandle, 0, &ModuleType);
                                             if(Result == VHDERR_NOERROR)
                                             {
                                                Result= VHD_SetMonitoredSFPModuleDefaultRegistersTable(BoardHandle,0,ModuleType,NULL,0);                                                                                                                                                                     
                                             }
                                          }                                
                                       }
                                    }
                                    else
                                       memset(SerialID.pVendorPN,0,16);


                                    /* Try to lock next slot */
                                    Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO,&pBuffer,&BufferSize);
                                       if (Result == VHDERR_NOERROR)
                                       {
                                          /* Do RX data processing here on pBuffer */
                                       }
                                       else
                                       {
                                          printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                          break;
                                       }


                                       /* Unlock slot */
                                       VHD_UnlockSlotHandle(SlotHandle);

                                       /* Print some statistics */
                                       VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                       VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                       printf("%u frames received (%u dropped)                                         \r",SlotsCount,SlotsDropped);
                                       fflush(stdout);
                                    }
                                    else if (Result != VHDERR_TIMEOUT)
                                    {
                                       printf("\nERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       break;
                                    }
                                    else
                                       printf("Timeout                                                   \r");
                                 }

                                 printf("\n");

                                 /* Stop stream */
                                 VHD_StopStream(StreamHandle);
                              }
                              else
                                 printf("ERROR : Cannot start RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                           }
                           else
                              printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                           /* Close stream handle */
                           VHD_CloseStreamHandle(StreamHandle);

                        }
                        else
                           printf("ERROR : Cannot open RX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                     }
                     else
                        printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                     /* Re-establish RX0-TX0 by-pass relay loopthrough */
                     VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
                  }
                  else
                     printf("ERROR : The selected channel is not an SDI one\n");
               }          
               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);           
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



