/*
   DELTA-hd
   --------
   3D TRANSMISSION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-hd board and of the 
   VideoMasterHD SDK in a stereoscopic transmission stream use case.

   The application opens board and stream handles, configures them, and 
   enters in a transmission loop.

   In order to compile this application, path to VideoMasterHD_Core.h and 
   VideoMasterHD_Sdi.h inclusion file and to VideoMasterHD.lib library 
   file must be properly configured.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

BOOL32 CreatePattern(ULONG VideoStandard, BYTE *pPatternBuffer, BYTE *pPatternBuffer2)
{
   BOOL32 Result = TRUE, Interlaced = FALSE;
   FILE *pFile_X = NULL;
   BYTE *pTempBuffer = NULL; 
   ULONG Width=0, Height=0;
   char pFileName_c[32];
   
   switch(VideoStandard)
   {
   case VHD_VIDEOSTD_S274M_1080p_24Hz : 
   case VHD_VIDEOSTD_S274M_1080p_60Hz :
   case VHD_VIDEOSTD_S274M_1080p_50Hz :
   case VHD_VIDEOSTD_S274M_1080p_25Hz :
   case VHD_VIDEOSTD_S274M_1080p_30Hz : Width = 1920; Height = 1080; Interlaced = FALSE; sprintf(pFileName_c,"DELTACAST_Stereo_1080_Full.bin"); break;
   case VHD_VIDEOSTD_S274M_1080i_50Hz :
   case VHD_VIDEOSTD_S274M_1080i_60Hz : Width = 1920; Height = 1080; Interlaced = TRUE; sprintf(pFileName_c,"DELTACAST_Stereo_1080_Full.bin"); break;
   case VHD_VIDEOSTD_S296M_720p_50Hz :
   case VHD_VIDEOSTD_S296M_720p_60Hz : Width = 1280; Height = 720; Interlaced = FALSE; sprintf(pFileName_c,"DELTACAST_Stereo_720_Full.bin"); break;
   case VHD_VIDEOSTD_S259M_PAL : Width = 720; Height = 576; Interlaced = TRUE; sprintf(pFileName_c,"DELTACAST_Stereo_PAL_Full.bin"); break;
   case VHD_VIDEOSTD_S259M_NTSC : Width = 720; Height = 487; Interlaced = TRUE; sprintf(pFileName_c,"DELTACAST_Stereo_NTSC_Full.bin"); break;
   default : return FALSE;
   }
      
   if(Interlaced)
      pTempBuffer = new BYTE[Width*2];
   if (pPatternBuffer)
   {
      pFile_X = fopen(pFileName_c,"rb");
      if(pFile_X)
      {
         if(Interlaced)
         {
            for(ULONG i=0;i<(ULONG)(Height/2);i++)
            {
               if(fread(pTempBuffer,1,Width*2,pFile_X)>0)
                  memcpy(&(pPatternBuffer[i*Width*2]),pTempBuffer,Width*2);
               if(fread(pTempBuffer,1,Width*2,pFile_X)>0)
                  memcpy(&(pPatternBuffer[(i*Width*2) + (((Height+1)/2)*Width*2)]),pTempBuffer,Width*2);
               if((i==((Height/2)-1)) && ((Height%2) != 0))
               {
                  if(fread(pTempBuffer,1,Width*2,pFile_X)>0)
                     memcpy(&(pPatternBuffer[(i+1)*Width*2]),pTempBuffer,Width*2);
               }
            }
         }
         else
            if(fread(pPatternBuffer,Width*Height*2,1,pFile_X)==0)
               Result = FALSE;
      }
      else
         Result = FALSE;
   }
   else
      Result = FALSE;
   if (pPatternBuffer2)
   {
      if(pFile_X)
      {
         if(Interlaced)
         {
            for(ULONG i=0;i<(ULONG)(Height/2);i++)
            {
               if(fread(pTempBuffer,Width*2,1,pFile_X)>0)
                  memcpy(&(pPatternBuffer2[i*Width*2]),pTempBuffer,Width*2);
               if(fread(pTempBuffer,Width*2,1,pFile_X)>0)
                  memcpy(&(pPatternBuffer2[(i*Width*2) + (((Height+1)/2)*Width*2)]),pTempBuffer,Width*2);
               if((i==((Height/2)-1)) && ((Height%2) != 0))
               {
                  if(fread(pTempBuffer,Width*2,1,pFile_X)>0)
                     memcpy(&(pPatternBuffer2[(i+1)*Width*2]),pTempBuffer,Width*2);
               }
            }
         }
         else
            if(fread(pPatternBuffer2,Width*Height*2,1,pFile_X)==0)
               Result = FALSE;
      }
      else
         Result = FALSE;
   }
   else
      Result = FALSE;
      
   if(pFile_X)
      fclose(pFile_X);
   
   if(pTempBuffer)
      delete[] pTempBuffer;
      
   return Result;
}

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             ChnType;
ULONG             SlotsCount, SlotsDropped,VideoStandard,BufferSize,BufferSize2, Width=0, Height=0,Interface;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
BYTE             *pPatternBuffer = NULL,*pPatternBuffer2 = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
BYTE             *pBuffer = NULL,*pBuffer2 = NULL;
VHD_ANCPACKET    *pAncPacket = NULL,*pAncPacket2 = NULL;
ULONG             NbRxRequired = 0, NbTxRequired = 2;


   VideoStandard=VHD_VIDEOSTD_S259M_PAL;

   init_keyboard();

   printf("\n3D TRANSMISSION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");

   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (ULONG i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Check the number of required channels */
         if (SetNbChannels(BrdId, NbRxRequired, NbTxRequired))
         {
            /* Open a handle on first DELTA-hd/sdi/codec board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_TX0_TYPE, &ChnType);
               if((ChnType==VHD_CHNTYPE_SDSDI)||(ChnType==VHD_CHNTYPE_HDSDI)||(ChnType==VHD_CHNTYPE_3GSDI))
               {
                  /* Disable RX0-TX0 and RX1-TX1 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,FALSE);

                  /* Select a 1/1 clock system */
                  VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,VHD_CLOCKDIV_1);


                  /* Create a logical stream to transmit on TX0 connector */
                  Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_COUPLED_TX01,VHD_SDI_STPROC_JOINED,NULL,&StreamHandle,NULL);
                  if (Result == VHDERR_NOERROR)
                  {
                     /* Video Standard definition */
                     switch (ChnType)
                     {
                     case VHD_CHNTYPE_3GSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_60Hz; Interface=VHD_INTERFACE_3G_A_425_1; break;
                     case VHD_CHNTYPE_HDSDI: VideoStandard=VHD_VIDEOSTD_S274M_1080p_30Hz; Interface=VHD_INTERFACE_HD_292_1; break;
                     case VHD_CHNTYPE_SDSDI: 
                     default: VideoStandard=VHD_VIDEOSTD_S259M_PAL; Interface=VHD_INTERFACE_SD_259; break;
                     }

                     /* Configure stream video standard */
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                     VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_DEPTH,4);
                     VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_PRELOAD,2);
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_TX_GENLOCK,TRUE);
                     VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_INTERFACE,Interface);


                     switch(VideoStandard)
                     {
                     case VHD_VIDEOSTD_S274M_1080p_24Hz : 
                     case VHD_VIDEOSTD_S274M_1080p_60Hz :
                     case VHD_VIDEOSTD_S274M_1080p_50Hz :
                     case VHD_VIDEOSTD_S274M_1080p_25Hz :
                     case VHD_VIDEOSTD_S274M_1080p_30Hz :
                     case VHD_VIDEOSTD_S274M_1080i_50Hz :
                     case VHD_VIDEOSTD_S274M_1080i_60Hz : Width = 1920; Height = 1080; break;
                     case VHD_VIDEOSTD_S296M_720p_50Hz :
                     case VHD_VIDEOSTD_S296M_720p_60Hz : Width = 1280; Height = 720; break;
                     case VHD_VIDEOSTD_S259M_PAL : Width = 720; Height = 576; break;
                     case VHD_VIDEOSTD_S259M_NTSC : Width = 720; Height = 487; break;
                     }

                     pPatternBuffer = new BYTE[Width*Height*2];
                     pPatternBuffer2 = new BYTE[Width*Height*2];

                     if (CreatePattern(VideoStandard,pPatternBuffer,pPatternBuffer2))
                     {
                        /* Start stream */
                        Result = VHD_StartStream(StreamHandle);
                        if (Result == VHDERR_NOERROR)
                        {                        
                           printf("\nTransmission started, press ESC to stop...\n");

                           /* Reception loop */
                           while (1)
                           {

                              if (kbhit()) 
                              {
                                 getch();
                                 break;
                              }

                              /* Try to lock next slot */
                              Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);

                              if (Result == VHDERR_NOERROR)
                              {
                                 Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO,&pBuffer,&BufferSize);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    /* Do left TX data processing here on pBuffer */
                                    memcpy(pBuffer,
                                       pPatternBuffer,
                                       BufferSize);
                                 }
                                 else
                                 {
                                    printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    break;
                                 }

                                 Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO_2,&pBuffer2,&BufferSize2);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    /* Do right TX data processing here on pBuffer */
                                    memcpy(pBuffer2,
                                       pPatternBuffer2,
                                       BufferSize2);
                                 }
                                 else
                                 {
                                    printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    break;
                                 }

                                 /* Generate a temporary ancillary data packet (marked for deletion) */
                                 Result = VHD_SlotAncAllocatePacket(SlotHandle,10,&pAncPacket);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    pAncPacket->DataID          = 0x80;
                                    pAncPacket->SecondaryDataID = 0x00;
                                    pAncPacket->InHANC          = TRUE;

                                    /* Fill in the packet UDW with some data */
                                    for (int UDW = 0; UDW < 10; UDW++)
                                       pAncPacket->pUserDataWords[UDW] = (WORD)(UDW+80);


                                    /* Insert the packet in left Y sample stream of video line 5 */
                                    Result = VHD_SlotAncInsertPacket(SlotHandle,5,VHD_SAMPLE_Y,-1,pAncPacket);
                                    if (Result != VHDERR_NOERROR)
                                    {
                                       printf("\nERROR : Cannot insert ANC packet. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    }
                                 }

                                 /* Generate a temporary ancillary data packet (marked for deletion) */
                                 Result = VHD_SlotAncAllocatePacket(SlotHandle,10,&pAncPacket2);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    pAncPacket2->DataID          = 0x80;
                                    pAncPacket2->SecondaryDataID = 0x00;
                                    pAncPacket2->InHANC          = TRUE;

                                    /* Fill in the packet UDW with some data */
                                    for (int UDW = 0; UDW < 10; UDW++)
                                       pAncPacket2->pUserDataWords[UDW] = (WORD)(UDW+80);


                                    /* Insert the packet in right Y sample stream of video line 5 */
                                    Result = VHD_SlotAncInsertPacket(SlotHandle,5,VHD_SAMPLE_Y_2,-1,pAncPacket2);
                                    if (Result != VHDERR_NOERROR)
                                    {
                                       printf("\nERROR : Cannot insert ANC packet. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    }
                                 }

                                 /* Unlock slot */
                                 VHD_UnlockSlotHandle(SlotHandle);

                                 /* Print some statistics */
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                 printf("%u frames sent (%u dropped)            \r",SlotsCount,SlotsDropped);
                                 fflush(stdout);
                              }
                              else if (Result != VHDERR_TIMEOUT)
                              {
                                 printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                 break;
                              }
                           }

                           printf("\n");

                           /* Stop stream */
                           VHD_StopStream(StreamHandle);
                        }
                        else
                           printf("ERROR : Cannot start TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


                     }
                     else
                        printf("ERROR : Cannot create pattern.\n");

                     /* Delete test pattern buffer */
                     if(pPatternBuffer)
                        delete[] pPatternBuffer;
                     if(pPatternBuffer2)
                        delete[] pPatternBuffer2;

                     /* Close stream handle */
                     VHD_CloseStreamHandle(StreamHandle);

                  }
                  else
                     printf("ERROR : Cannot open TX0 stream on DELTA-hd/sdi/codec board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


                  /* Re-establish RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
               }
               else
                  printf("ERROR : The selected channel is not an SDI one\n");

               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
         }
         else
            printf("ERROR : Cannot set number of channel (%d RX and %d TX required).\n", NbRxRequired, NbTxRequired);           
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));


   close_keyboard();

   return 0;
}



