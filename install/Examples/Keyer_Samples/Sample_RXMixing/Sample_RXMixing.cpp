/*
   DELTA-key
   ------------
   RX CHANNELS MIXING SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-key board and of the 
   VideoMasterHD SDK Keyer Extension in a RX channels mixing use case.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, genlocks the board on the 
   RX0 input, configures the onboard keyer, and enters in a transmission loop.

   In order to compile this application, path to VideoMasterHD_Core.h, VideoMasterHD_Sdi 
   and VideoMasterHD_Keyer.h inclusion files, and to VideoMasterHD.lib library 
   file must be properly configured.

   In order to run this application, the DELTA-key board RX0 and RX1 connectors
   must be fed with valid SD or HD SDI signal. Both signal should be of the 
   same video standard, and should be genlocked together with a phase 
   difference not exceeding 4000 pixels

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>
#include <math.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD/VideoMasterHD_Sdi_Keyer.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Keyer.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             i;
ULONG             BufferSize = 0;

ULONG             RX0VideoStandard, RX1VideoStandard, ClockSystem ;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;

int               FrameWidth = 0, FrameHeight = 0;
BOOL32              Interlaced = FALSE, Invisible = TRUE;
double            a = 0, start = 0;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
BYTE             *pBuffer=NULL;


   init_keyboard();

   printf("\nRX MIXING SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for ( i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

         /* Open a handle on first DELTA board */
         Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
         if (Result == VHDERR_NOERROR)
         {
            /* Check the board type of the selected board */
            if(IsKeyerAvailable(BoardHandle))
            {

               /* Disable RX-TX by-pass relays loopthrough */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,FALSE);

               /* Select RX0 as the genlock source */
               VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_GENLOCK_SOURCE,VHD_GENLOCK_RX0);

               /* Wait for genlock reference */
               WaitForGenlockRef(BoardHandle);
               
               /* Auto-detect clock system */
               Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,&ClockSystem);

               if((Result == VHDERR_NOERROR))
               {
                  printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                  /* Auto-detected incoming video standard on RX0 */
                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_STANDARD,&RX0VideoStandard);

                  if ((Result == VHDERR_NOERROR) && (RX0VideoStandard != NB_VHD_VIDEOSTANDARDS))
                  {
                     PrintVideoStandardInfo(RX0VideoStandard);

                     GetVideoCharacteristics(RX0VideoStandard, &FrameWidth, &FrameHeight, &Interlaced, NULL);

                     /* Auto-detected incoming video standard on RX1 */
                     Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX1_STANDARD,&RX1VideoStandard);
                     if ((Result == VHDERR_NOERROR) && (RX1VideoStandard == RX0VideoStandard))
                     {
                        /* Select the detected clock system */
                        VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,ClockSystem);

                        /* Pre-configure the onboard keyer */

                        /* Keyer input A is fed from RX0 connector */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_INPUT_A,VHD_KINPUT_RX0);
                        /* Keyer input B is fed from RX1 connector */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_INPUT_B,VHD_KINPUT_RX1);
                        /* Keyer input K is fed from PC stream (Y component of a YUV stream) */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_INPUT_K,VHD_KINPUT_TX0);
                        /* Output graphics blended over video on TX0 */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_VIDEOOUTPUT_TX0,VHD_KOUTPUT_KEYER);
                        /* Configure alpha blending key dynamic to range from 0 to 1020 (use the full 8-bit range)*/
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ALPHACLIP_MIN,0);
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ALPHACLIP_MAX,1020);
                        /* Reset global alpha blending factor to zero */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ALPHABLEND_FACTOR,0);

                        /* Create a logical stream to send graphics to the onboard keyer */
                        Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_TX0,VHD_SDI_STPROC_DISJOINED_VIDEO,NULL,&StreamHandle,NULL);
                        if (Result == VHDERR_NOERROR)
                        {
                           /* Instruct the transmitter to genlock itself on the board reference */
                           VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_TX_GENLOCK,TRUE);

                           /* Configure the stream to be fed with 8-bit YUV graphics */
                           VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFER_PACKING,VHD_BUFPACK_VIDEO_YUV422_8);

                           /* Configure TX stream to output same standard as RX0 input */
                           VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,RX0VideoStandard);

                           /* Enable the onboard keyer */
                           VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ENABLE,TRUE);

                           /* Start stream */
                           Result = VHD_StartStream(StreamHandle);
                           if (Result == VHDERR_NOERROR)
                           {
                              printf("\nKeying started, press ESC to stop...\n");

                              /* Transmission loop */
                              while (1)
                              {
                                 if (kbhit()) 
                                 {
                                    getch();
                                    break;
                                 }

                                 /* Try to lock next slot */
                                 Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);

                                 if (Result == VHDERR_NOERROR)
                                 {

                                    a      = start;
                                    start += 0.05;

                                    Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO,&pBuffer,&BufferSize);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       /* Fill in the video buffer */
                                       memset(pBuffer,0,BufferSize);

                                       for (int y = 0; y < FrameHeight; y++)
                                       {
                                          if (Interlaced)
                                          {
                                             if (y & 1)
                                             {
                                                memset(pBuffer+(FrameWidth*FrameHeight)+((y/2)*FrameWidth*2),
                                                   255,
                                                   (size_t)(FrameWidth+(FrameWidth*sin(a)/2)));
                                             }
                                             else
                                             {
                                                memset(pBuffer+((y/2)*FrameWidth*2),
                                                   255,
                                                   (size_t)(FrameWidth+(FrameWidth*sin(a)/2)));
                                             }
                                             a += 0.005;

                                          }
                                          else
                                          {
                                             memset(pBuffer+(y*FrameWidth*2),
                                                255,
                                                (size_t)(FrameWidth+(FrameWidth*sin(a)/2)));
                                             a += 0.010;
                                          }
                                       }

                                    }
                                    else
                                    {
                                       printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
                                       break;
                                    }

                                    /* Unlock slot */
                                    VHD_UnlockSlotHandle(SlotHandle);

                                    if (Invisible)
                                    {
                                       VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ALPHABLEND_FACTOR,1020);
                                       Invisible = FALSE;
                                    }
                                 }
                                 else if (Result != VHDERR_TIMEOUT)
                                 {
                                    printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
                                    break;
                                 }

                              }

                              printf("\n");

                              /* Stop stream */
                              VHD_StopStream(StreamHandle);
                           }
                           else
                              printf("ERROR : Cannot start TX0 stream on DELTA-key board handle. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));


                           /* Close stream handle */
                           VHD_CloseStreamHandle(StreamHandle);

                        }
                        else
                           printf("ERROR : Cannot open TX0 stream on DELTA-key board handle. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
                     }
                     else
                        printf("ERROR : RX0 and RX1 connectors are not fed with similar video signals. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
               }
               else
                  printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));

            }
            else
               printf("ERROR : The selected board is not a DELTA-key board\n");

            /* Re-establish RX0-TX0 by-pass relay loopthrough */
            VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
            VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,TRUE);
            
            /* Close board handle */
            VHD_CloseBoardHandle(BoardHandle);
         }
         else
            printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n", BrdId, Result, GetErrorDescription(Result));
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



