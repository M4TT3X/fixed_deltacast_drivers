/*
   DELTA-key
   ------------
   PC GRAPHICS KEYING OVER LIVE VIDEO SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-key board and of the 
   VideoMasterHD SDK Keyer Extension in a PC graphics internal keying use case.

   The application opens board and stream handles, configures them, performs 
   auto-detection of the incoming video standard, genlocks the board on the 
   RX0 input, configures the onboard keyer, and enters in a transmission loop.

   In order to compile this application, path to VideoMasterHD_Core.h, VideoMasterHD_Sdi and  
   VideoMasterHD_Keyer.h inclusion files, and to VideoMasterHD.lib library 
   file must be properly configured.

   In order to run this application, the DELTA-key board RX0 connector must 
   be fed with a valid SD or HD SDI signal.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Sdi.h"
#include "VideoMasterHD/VideoMasterHD_Sdi_Keyer.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"
#include "VideoMasterHD_Sdi_Keyer.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif


#define BITMAP_PATH     "DELTACAST_450x129_RGBA.raw"
#define BITMAP_WIDTH    450
#define BITMAP_HEIGHT   129


BOOL32 LoadBitmap (BYTE *pBitmap);
void CopyBitmap (BYTE *pSourceBitmap, BYTE *pDestBuffer, int DestPosX, int DestPosY, int BufferWidth, int BufferHeight, BOOL32 Interlaced=FALSE, BOOL32 Odd=TRUE);

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards;
ULONG             i;
ULONG             BufferSize = 0;
ULONG             VideoStandard, GenlockSts=0, ClockSystem ;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;

BYTE             *pBitmap = new BYTE[BITMAP_WIDTH*BITMAP_HEIGHT*4];
int               FrameWidth = 0, FrameHeight = 0;
BOOL32              Interlaced = FALSE, Invisible = TRUE, Odd = TRUE;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
int               yPos=1, xPos=1, ySpeed=1, xSpeed=1;

BYTE             *pBuffer=NULL;


   init_keyboard();

   printf("\nPC GRAPHICS SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Load resource bitmap from a raw RGBA file */
   if (LoadBitmap(pBitmap))
   {
      /* Query VideoMasterHD information */
      Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
      if (Result == VHDERR_NOERROR)
      {

         printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
            DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
            NbBoards,(NbBoards>1)?"s":"");


         if (NbBoards > 0)
         {
            /* Query DELTA boards information */
            for ( i = 0; i < NbBoards; i++)
            {
               PrintBoardInfo(i);
            }

            /* Open a handle on first DELTA board */
            Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
            if (Result == VHDERR_NOERROR)
            {
               /* Check the board type of the selected board */
               if(IsKeyerAvailable(BoardHandle))
               {

                  /* Disable RX0-TX0 by-pass relay loopthrough */
                  VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);

                  /* Select RX0 as the genlock source */
                  VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_GENLOCK_SOURCE,VHD_GENLOCK_RX0);

                  /* Wait for genlock reference */
                  WaitForGenlockRef(BoardHandle);

                  /* Auto-detect clock system */
                  Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,&ClockSystem);

                  if((Result == VHDERR_NOERROR))
                  {
                     printf("\nIncoming clock system : %s\n",(ClockSystem==VHD_CLOCKDIV_1)?"European":"American");

                     /* Auto-detected incoming video standard */
                     Result = VHD_GetBoardProperty(BoardHandle,VHD_SDI_BP_RX0_STANDARD,&VideoStandard);
                     if ((Result == VHDERR_NOERROR) && (VideoStandard != NB_VHD_VIDEOSTANDARDS))
                     {                        
                        PrintVideoStandardInfo(VideoStandard);

                        GetVideoCharacteristics(VideoStandard, &FrameWidth, &FrameHeight, &Interlaced, NULL);
                        
                        /* Select the detected clock system */
                        VHD_SetBoardProperty(BoardHandle,VHD_SDI_BP_CLOCK_SYSTEM,ClockSystem);

                        /* Pre-configure the onboard keyer */

                        /* Keyer input A is fed from RX0 connector */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_INPUT_A,VHD_KINPUT_RX0);
                        /* Keyer input B is fed from PC stream (RGB components will be used) */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_INPUT_B,VHD_KINPUT_TX0);
                        /* Keyer input K is fed from PC stream (Alpha component will be used) */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_INPUT_K,VHD_KINPUT_TX0);
                        /* Output graphics blended over video on TX0 */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_VIDEOOUTPUT_TX0,VHD_KOUTPUT_KEYER);
                        /* Configure alpha blending key dynamic to range from 0 to 1020 (use the full 8-bit range)*/
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ALPHACLIP_MIN,0);
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ALPHACLIP_MAX,1020);
                        /* Reset global alpha blending factor to zero */
                        VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ALPHABLEND_FACTOR,0);

                        /* Create a logical stream to send graphics to the onboard keyer */
                        Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_TX0,VHD_SDI_STPROC_DISJOINED_VIDEO,NULL,&StreamHandle,NULL);
                        if (Result == VHDERR_NOERROR)
                        {
                           /* Instruct the transmitter to genlock itself on the board reference */
                           VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_TX_GENLOCK,TRUE);

                           /* Configure the stream to be fed with 8-bit RGBA graphics */
                           VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFER_PACKING,VHD_BUFPACK_VIDEO_RGBA_32);

                           /* Configure TX stream to output same standard as input */
                           VHD_SetStreamProperty(StreamHandle,VHD_SDI_SP_VIDEO_STANDARD,VideoStandard);
                           
                           /* Enable the onboard keyer */
                           VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ENABLE,TRUE);
                           
                           /* Start stream */
                           Result = VHD_StartStream(StreamHandle);
                           if (Result == VHDERR_NOERROR)
                           {
                              printf("\nKeying started, press ESC to stop...\n");

                              /* Transmission loop */
                              while (1)
                              {
                                 if (kbhit()) 
                                 {
                                    getch();
                                    break;
                                 }

                                 /* Try to lock next slot */
                                 Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);

                                 if (Result == VHDERR_NOERROR)
                                 {
                                    /* Fill in the video buffer */
                                    Result = VHD_GetSlotBuffer(SlotHandle,VHD_SDI_BT_VIDEO,&pBuffer,&BufferSize);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       /* Fill in the video buffer */
                                       memset(pBuffer,0,BufferSize);

                                       for(int i=0; i<(Interlaced?2:1); i++)
                                       {
                                          CopyBitmap(pBitmap,pBuffer,
                                             xPos,yPos,
                                             FrameWidth,FrameHeight,Interlaced,Odd);

                                          if((xPos==FrameWidth-BITMAP_WIDTH-1) || (xPos==0))
                                             xSpeed= -xSpeed;

                                          if((yPos==FrameHeight-BITMAP_HEIGHT-1) || (yPos==0))
                                             ySpeed= -ySpeed;

                                          xPos += xSpeed;
                                          yPos += ySpeed;
                                          Odd=!Odd;
                                       }                            
                                    }
                                    else
                                    {
                                       printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
                                       break;
                                    }
                                                                                                                
                                    /* Unlock slot */
                                    VHD_UnlockSlotHandle(SlotHandle);

                                    if (Invisible)
                                    {
                                       VHD_SetBoardProperty(BoardHandle,VHD_KEYER_BP_ALPHABLEND_FACTOR,1023);
                                       Invisible = FALSE;
                                    }

                                    /* Print genlock status */
                                    VHD_GetBoardProperty(BoardHandle, VHD_SDI_BP_GENLOCK_STATUS, &GenlockSts);
                                    printf("Reference %s !                    \r", (GenlockSts&VHD_SDI_GNLKSTS_UNLOCKED)?"unlocked":"locked");
                                    fflush(stdout);
                                 }
                                 else if (Result != VHDERR_TIMEOUT)
                                 {
                                    printf("\nERROR : Cannot lock slot on TX0 stream. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
                                    break;
                                 }
                                 else
                                    printf("Timeout !!\r");

                              }

                              printf("\n");

                              /* Stop stream */
                              VHD_StopStream(StreamHandle);
                           }
                           else
                              printf("ERROR : Cannot start TX0 stream on DELTA-key board handle. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));

                           /* Close stream handle */
                           VHD_CloseStreamHandle(StreamHandle);

                        }
                        else
                           printf("ERROR : Cannot open TX0 stream on DELTA-key board handle. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
                     }
                     else
                        printf("ERROR : Cannot detect incoming video standard from RX0. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot detect incoming clock system from RX0. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
               }
               else
                  printf("ERROR : No keyer available on the selected board\n");

               /* Re-establish RX0-TX0 by-pass relay loop-through */
               VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
               
               /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
            }
            else
               printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n", BrdId, Result, GetErrorDescription(Result));
         }
         else
            printf("No DELTA board detected, exiting...\n");
      }
      else
         printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n", Result, GetErrorDescription(Result));
   }
   else
      printf("ERROR : Cannot load resource bitmap.\n");

   close_keyboard();

   delete[] pBitmap;

   return 0;
}




BOOL32 LoadBitmap (BYTE *pBitmap)
{
BOOL32     Result = FALSE;
FILE    *pFile = fopen(BITMAP_PATH,"rb");
size_t   NbBytesRead = 0;
   
   if (pFile)
   {
      NbBytesRead = fread(pBitmap,1,BITMAP_WIDTH*BITMAP_HEIGHT*4,pFile);
      if (NbBytesRead == BITMAP_WIDTH*BITMAP_HEIGHT*4)
         Result = TRUE;

      fclose(pFile);
   }

   return Result;
}


void CopyBitmap (BYTE *pSourceBitmap, BYTE *pDestBuffer, int DestPosX, int DestPosY, int BufferWidth, int BufferHeight, BOOL32 Interlaced, BOOL32 Odd)
{
int   y;

   if (Interlaced)
   {
      if(Odd)
      {
         if(DestPosY&1)
            y = DestPosY+1;
         else
            y = DestPosY;
      }
      else
      {
         if(DestPosY&1)
            y = DestPosY;
         else
            y = DestPosY+1;
      }

      for (; ((y < DestPosY+BITMAP_HEIGHT) && (y < BufferHeight)); y+=2)
      {
         if (!Odd)
         {
            if (DestPosX+BITMAP_WIDTH < BufferWidth)
               memcpy(pDestBuffer+((DestPosX+((((BufferHeight+1)/2)+(y/2))*BufferWidth))*4),
                     pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
                     BITMAP_WIDTH*4);
            else
               memcpy(pDestBuffer+((DestPosX+((((BufferHeight+1)/2)+(y/2))*BufferWidth))*4),
                     pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
                     (BufferWidth-DestPosX)*4);
         }
         else
         {
            if (DestPosX+BITMAP_WIDTH < BufferWidth)
               memcpy(pDestBuffer+((DestPosX+((y/2)*BufferWidth))*4),
                     pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
                     BITMAP_WIDTH*4);
            else
               memcpy(pDestBuffer+((DestPosX+((y/2)*BufferWidth))*4),
                     pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
                     (BufferWidth-DestPosX)*4);
         }
      }
   }
   else
   {
      for (y = DestPosY; ((y < DestPosY+BITMAP_HEIGHT) && (y < BufferHeight)); y++)
      {
         if (DestPosX+BITMAP_WIDTH < BufferWidth)
            memcpy(pDestBuffer+((DestPosX+(y*BufferWidth))*4),
                   pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
                   BITMAP_WIDTH*4);
         else
            memcpy(pDestBuffer+((DestPosX+(y*BufferWidth))*4),
                   pSourceBitmap+((y-DestPosY)*BITMAP_WIDTH*4),
                   (BufferWidth-DestPosX)*4);
      }
   }
}




