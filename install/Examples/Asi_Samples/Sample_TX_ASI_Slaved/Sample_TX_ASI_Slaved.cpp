/*
   DELTA-codec
   -------------
   SLAVED ASI TRANSMISSION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-codec/asi board and 
   of the VideoMasterHD SDK in a slaved ASI transmission stream use case.

   The application opens board and the first ASI stream handles,
   configures them and enters in a slaved transmission loop.

   In order to compile this application, path to VideoMasterHD_Core.h
   and VideoMasterHD_Asi.h inclusion files and to VideoMasterHD.lib 
   library file must be properly configured.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Asi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Asi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

#define BUFSIZE_IN_MS   40          //40 ms of buffer

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards,BoardType,LowProfile;
ULONG             NbRxChannels,NbTxChannels,ChnType,i;
ULONG             SlotsCount, SlotsDropped,BufferSize,TxIdx,RxIdx,Bitrate,TxStatus,BufferSizeConstraint;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
BYTE             *pBuffer=NULL;
BYTE              pPattern_UB[188];

   /* Pattern initialization */
   pPattern_UB[0]=0x47;
   pPattern_UB[1]=0x1F;          /* PID corresponding to */
   pPattern_UB[2]=0xFF;          /* stuffing (0x1FFF) */
   pPattern_UB[3]=0x0;                 
   for(ULONG i=4; i<188; i++)
      pPattern_UB[i]=0;


   init_keyboard();

   printf("\nASI TRANSMISSION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");

      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }      
         
         /* Open a handle on selected DELTA board */
         Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
         if (Result == VHDERR_NOERROR)
         {
				VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_BOARD_TYPE, &BoardType);
				VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_LOWPROFILE, &LowProfile);

            /* Check the board type of the selected board */
            if(BoardType==VHD_BOARDTYPE_CODEC || (BoardType==VHD_BOARDTYPE_ASI && LowProfile))
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_NB_TXCHANNELS, &NbTxChannels);
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_NB_RXCHANNELS, &NbRxChannels);

               for(TxIdx=0; TxIdx<NbTxChannels; TxIdx++)
               {
                  VHD_GetBoardProperty(BoardHandle,ChnIdx2BpChnType(FALSE,TxIdx),&ChnType);
                  if(ChnType==VHD_CHNTYPE_ASI)
                     break;
               }
               for(RxIdx=0; RxIdx<NbRxChannels; RxIdx++)
               {
                  VHD_GetBoardProperty(BoardHandle,ChnIdx2BpChnType(TRUE,RxIdx),&ChnType);
                  if(ChnType==VHD_CHNTYPE_ASI)
                     break;
               }

               if((TxIdx!=NbTxChannels) && (RxIdx!=NbRxChannels))
               {
                  /* Disable RXi-TXi by-pass relay loopthrough */
                  if(TxIdx<2)
                  {
                     if(TxIdx==0)
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);
                     else
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,FALSE);
                  }
                  
                  if(RxIdx<2)
                  {
                     if(RxIdx==0)
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);
                     else
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,FALSE);
                  }

                  /* Create a logical stream to transmit on TXi connector */
                  Result = VHD_OpenStreamHandle(BoardHandle,GetTxStreamType(TxIdx),VHD_ASI_STPROC_DEFAULT,NULL,&StreamHandle,NULL);
                  if (Result == VHDERR_NOERROR)
                  {
                     VHD_SetStreamProperty(StreamHandle,VHD_ASI_SP_TX_BITRATE_SRC,GetBitRateSrc(RxIdx));

                     /* Wait for channel locked */
                     WaitForChannelLocked(BoardHandle, VHD_CORE_BP_RX0_STATUS);

                     /* Auto-detect clock system */
                     Result = VHD_GetStreamProperty(StreamHandle,VHD_ASI_SP_BITRATE,&Bitrate);

                     if (Result == VHDERR_NOERROR)
                     {
                        printf("Incoming bitrate on RX%d : %u bps\n", RxIdx,Bitrate);


                        VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFERQUEUE_PRELOAD,1);

                        /* Set packet type */
                        VHD_SetStreamProperty(StreamHandle,VHD_ASI_SP_PACKET_TYPE,VHD_ASI_PKT_188);
                        /* Set output bitrate */
                        VHD_SetStreamProperty(StreamHandle,VHD_ASI_SP_BITRATE,Bitrate);
                        
                        /* Compute and set buffer size */
                        BufferSize=(ULONG)((LONGLONG)Bitrate*BUFSIZE_IN_MS/8000);
                        if(BufferSize<0x800)
                           BufferSize=0x800;                 /* The buffer size must be greater or equal than 2KB */
                        else if(BufferSize>0x800000)
                           BufferSize=0x800000;              /* The buffer size must be less or equal than 8MB */

                        BufferSizeConstraint = 188;          /* The buffer size must be a multiple of the packet size */
                        if(BoardType == VHD_BOARDTYPE_ASI)
                           BufferSizeConstraint *= 16;       /* The size must be a multiple of 64 bytes on the DELTA-asi */

                        if(BufferSize < BufferSizeConstraint)
                           BufferSize = BufferSizeConstraint;
                        else
                           BufferSize -= BufferSize%BufferSizeConstraint; 

                        VHD_SetStreamProperty(StreamHandle,VHD_ASI_SP_BUFFER_SIZE,BufferSize);
                        
                        /* Enable backup bitrate if the input bitrate deviates more than 2kbps from the original one */
                        VHD_SetStreamProperty(StreamHandle,VHD_ASI_SP_BACKUP_BITRATE,TRUE);
                        VHD_SetStreamProperty(StreamHandle,VHD_ASI_SP_ALLOWED_BITRATE_DEV,2000);


                         /* Start stream */
                        Result = VHD_StartStream(StreamHandle);
                        if (Result == VHDERR_NOERROR)
                        {
                           printf("\nTransmission on TX%d slaved on RX%d started, press ESC to stop...\n", TxIdx,RxIdx);

                           /* Reception loop */
                           while (1)
                           {
                              if (kbhit()) 
                              {
                                 getch();
                                 break;
                              }

                              /* Try to lock next slot */
                              Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                              if (Result == VHDERR_NOERROR)
                              {
                                 Result = VHD_GetSlotBuffer(SlotHandle,VHD_ASI_BT_TS,&pBuffer,&BufferSize);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    for(ULONG i=0; i<BufferSize/188; i++)
                                    {
                                       /* Copy the pattern */
                                       memcpy(pBuffer, pPattern_UB, 188);
                                       pBuffer += 188;

                                       /* Increment continuity counter */
                                       pPattern_UB[3]++;
                                       pPattern_UB[3] &= 0x0F;
                                    }
                                 }
                                 else
                                 {
                                    printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                    break;
                                 }

                                 /* Unlock slot */
                                 VHD_UnlockSlotHandle(SlotHandle);

                                 /* Print some statistics */
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                 VHD_GetStreamProperty(StreamHandle,VHD_ASI_SP_BITRATE,&Bitrate);
                                 VHD_GetBoardProperty(BoardHandle,ChnIdx2BpStatus(FALSE,TxIdx),&TxStatus);
                                 printf("%u slots sent (%u underrun) - RX%d_BR=%9d - %s                 \r",SlotsCount,SlotsDropped,RxIdx,Bitrate, ((TxStatus&VHD_ASI_TXSTS_BITRATE_FAIL)?"Backup":"Slaved"));
                                 fflush(stdout);
                              }
                              else if (Result != VHDERR_TIMEOUT)
                              {
                                 printf("\nERROR : Cannot lock slot on TX%d stream. Result = 0x%08X (%s)\n",TxIdx,Result, GetErrorDescription(Result));
                                 break;
                              }
                           }

                           printf("\n");

                           /* Stop stream */
                           VHD_StopStream(StreamHandle);
                        }
                        else
                           printf("ERROR : Cannot start TX%d stream on board handle. Result = 0x%08X (%s)\n",TxIdx,Result, GetErrorDescription(Result));


                        /* Close stream handle */
                        VHD_CloseStreamHandle(StreamHandle);
                     }                       
                     else
                        printf("ERROR : Cannot detect incoming RX%d bitrate. Result = 0x%08X (%s)\n",RxIdx,Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot open TX%d stream on board handle. Result = 0x%08X (%s)\n",TxIdx,Result, GetErrorDescription(Result));

                  /* Re-establish RXi-TXi by-pass relay loopthrough */
                  if(TxIdx<2)
                  {
                     if(TxIdx==0)
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
                     else
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,TRUE);
                  }

                  if(RxIdx<2)
                  {
                     if(RxIdx==0)
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
                     else
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,TRUE);
                  }
               }
               else
                  printf("ERROR : No ASI TX or RX channel on this board\n");
            }
            else
               printf("ERROR : The selected board is not a DELTA-codec/asi board\n");

              /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
         }
         else
            printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



