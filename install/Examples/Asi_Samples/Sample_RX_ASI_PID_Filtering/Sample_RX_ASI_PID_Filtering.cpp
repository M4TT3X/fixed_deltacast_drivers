/*
   DELTA-codec
   -------------
   ASI RECEPTION STREAM WITH PID FILTERING SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-codec/asi board and 
   of the VideoMasterHD SDK in a ASI reception stream with PID
   filtering use case.

   The application opens board and the first ASI stream handles, 
   configures them and enters in a reception loop.

   In order to compile this application, path to VideoMasterHD_Core.h
   and VideoMasterHD_Asi.h inclusion files and to VideoMasterHD.lib 
   library file must be properly configured.

   In order to run this application, the DELTA-codec/asi board first ASI RX
   connector must be fed with a ASI signal containing packet with PID 
   equal to zero (PAT).
*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Asi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Asi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

#define BITRATE         10000000    //10Mbps
#define BUFSIZE_IN_MS   40          //40 ms of buffer

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards,BoardType;
ULONG             NbRxChannels,ChnType,i;
ULONG             Sts,SlotsCount, SlotsDropped,BufferSize, PktSize,BufferSizeConstraint;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
BYTE             *pBuffer=NULL;
VHD_ASI_SLOTSTATUS *pSlotStatus=NULL;
VHD_ASI_TSPACKETTYPE PktType;
int               pPIDFilter_i[VHD_MAXNB_PIDFILTER];



   init_keyboard();

   printf("\nASI RECEPTION STREAM WITH PID FILTERING SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");

      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }            
         
         /* Open a handle on selected DELTA board */
         Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
         if (Result == VHDERR_NOERROR)
         {
            VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_BOARD_TYPE, &BoardType);

            /* Check the board type of the selected board */
            if(BoardType==VHD_BOARDTYPE_CODEC || BoardType==VHD_BOARDTYPE_ASI)
            {
               VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_NB_RXCHANNELS, &NbRxChannels);

               for(i=0; i<NbRxChannels; i++)
               {
                  VHD_GetBoardProperty(BoardHandle,ChnIdx2BpChnType(TRUE,i),&ChnType);
                  if(ChnType==VHD_CHNTYPE_ASI)
                     break;
               }

               if(i!=NbRxChannels)
               {
                  /* Disable RXi-TXi by-pass relay loopthrough */
                  if(i<2)
                  {
                     if(i==0)
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,FALSE);
                     else
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,FALSE);
                  }

                  /* Create a logical stream to receive on RXi connector */
                  Result = VHD_OpenStreamHandle(BoardHandle,GetRxStreamType(i),VHD_ASI_STPROC_DEFAULT,NULL,&StreamHandle,NULL);
                  if (Result == VHDERR_NOERROR)
                  {
                     /* Wait for channel locked */
                     WaitForChannelLocked(BoardHandle, (VHD_CORE_BOARDPROPERTY)(ChnIdx2BpStatus(TRUE,i)));

                     /* Auto-detect clock system */
                     Result = VHD_GetStreamProperty(StreamHandle,VHD_ASI_SP_PACKET_TYPE,(ULONG*)&PktType);
                     
                     if ((Result == VHDERR_NOERROR) && ((PktType==VHD_ASI_PKT_188)||(PktType==VHD_ASI_PKT_204)))
                     {
                        if(PktType==VHD_ASI_PKT_188)
                        {
                           printf("188 packet type detected\n");
                           PktSize=188;
                        }
                        else
                        {
                           printf("204 packet type detected\n");
                           PktSize=204;
                        }

                        VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);

                        /* Set packet type */
                        VHD_SetStreamProperty(StreamHandle,VHD_ASI_SP_PACKET_TYPE,PktType);

                        /* Set buffer size */
                        BufferSize=(ULONG)((LONGLONG)BITRATE*BUFSIZE_IN_MS/8000);
                        if(BufferSize<0x800)
                           BufferSize=0x800;                 /* The buffer size must be greater or equal than 2KB */
                        else if(BufferSize>0x800000)
                           BufferSize=0x800000;              /* The buffer size must be less or equal than 8MB */

                        BufferSizeConstraint = PktSize;          /* The buffer size must be a multiple of the packet size */
                        if(BoardType == VHD_BOARDTYPE_ASI)
                           BufferSizeConstraint *= 16;       /* The size must be a multiple of 64 bytes on the DELTA-asi */

                        if(BufferSize < BufferSizeConstraint)
                           BufferSize = BufferSizeConstraint;
                        else
                           BufferSize -= BufferSize%BufferSizeConstraint; 
                        VHD_SetStreamProperty(StreamHandle,VHD_ASI_SP_BUFFER_SIZE,BufferSize);

                        pPIDFilter_i[0]=0;
                        for(int k=1; k<VHD_MAXNB_PIDFILTER; k++)
                           pPIDFilter_i[k]=-1;
                        
                        Result=VHD_SetPIDFilter(StreamHandle, pPIDFilter_i);
                        if (Result == VHDERR_NOERROR)
                        {
                           /* Start stream */
                           Result = VHD_StartStream(StreamHandle);
                           if (Result == VHDERR_NOERROR)
                           {
                              printf("\nReception started on RX%d, press ESC to stop...\n",i);

                              /* Reception loop */
                              while (1)
                              {
                                 if (kbhit()) 
                                 {
                                    getch();
                                    break;
                                 }


                                 /* Try to lock next slot */
                                 Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);
                                 if (Result == VHDERR_NOERROR)
                                 {
                                    Result = VHD_GetSlotBuffer(SlotHandle,VHD_ASI_BT_TS,&pBuffer,&BufferSize);
                                    if (Result == VHDERR_NOERROR)
                                    {
                                       /* Do RX data processing here on pBuffer */
                                    }
                                    else
                                    {
                                       printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       break;
                                    }

                                    Result = VHD_GetSlotStatus(SlotHandle,(void**)&pSlotStatus);
                                    if(Result == VHDERR_NOERROR)
                                    {
                                       if(pSlotStatus->FramingError)
                                          printf("\nINFO : This slot contains some data corrupted by a framing error.\n");
                                    }
                                    else
                                    {
                                       printf("\nERROR : Cannot get slot status. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                       break;
                                    }

                                    /* Unlock slot */
                                    VHD_UnlockSlotHandle(SlotHandle);
                                 }
                                 else if (Result != VHDERR_TIMEOUT)
                                 {
                                    printf("\nERROR : Cannot lock slot on RX%d stream. Result = 0x%08X (%s)\n",i,Result, GetErrorDescription(Result));
                                    break;
                                 }

                                 /* Print some statistics */
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&SlotsCount);
                                 VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&SlotsDropped);
                                 VHD_GetBoardProperty(BoardHandle,ChnIdx2BpStatus(TRUE,i),&Sts);
                                 printf("%u slots received (%u overrun) %s                            \r",SlotsCount,SlotsDropped,(Sts&VHD_CORE_RXSTS_UNLOCKED)?"!! Unlocked !!":(Sts&VHD_ASI_RXSTS_FRAMING_ERR)?"!! Framing Error !!":"");
                                 fflush(stdout);
                              }

                              printf("\n");

                              /* Stop stream */
                              VHD_StopStream(StreamHandle);
                           }
                           else
                              printf("ERROR : Cannot start RX%d stream on board handle. Result = 0x%08X (%s)\n",i,Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot set PID filter. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                     }
                     else
                        printf("ERROR : Cannot detect incoming RX%d bitrate. Result = 0x%08X (%s)\n",i,Result, GetErrorDescription(Result));

                     /* Close stream handle */
                     VHD_CloseStreamHandle(StreamHandle);

                  }
                  else
                     printf("ERROR : Cannot open RX%d stream on board handle. Result = 0x%08X (%s)\n",i,Result, GetErrorDescription(Result));

                  /* Re-establish RXi-TXi by-pass relay loopthrough */
                  if(i<2)
                  {
                     if(i==0)
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_0,TRUE);
                     else
                        VHD_SetBoardProperty(BoardHandle,VHD_CORE_BP_BYPASS_RELAY_1,TRUE);
                  }
               }
               else
                  printf("ERROR : No ASI RX channel on this board\n");
            }
            else
               printf("ERROR : The selected board is not a DELTA-codec/asi board\n");

              /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
         }
         else
            printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



