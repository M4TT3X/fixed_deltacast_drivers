/*
   DELTA-codec
   -------------
   ASI GET STC SAMPLE APPLICATION
   (c) DELTACAST
   --------------------------------------------------------

   This application demonstrates the use of the DELTA-codec/asi board and 
   of the VideoMasterHD SDK to get the STC (System Time Clock) from
   the DETLA-codec/asi.

   The application opens board and enters in a get STC loop.

   In order to compile this application, path to VideoMasterHD_Core.h
   and VideoMasterHD_Asi.h inclusion files and to VideoMasterHD.lib 
   library file must be properly configured.
*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Asi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Asi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

#define BITRATE         10000000    //10Mbps
#define BUFSIZE_IN_MS   40          //40 ms of buffer

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards,BoardType;
ULONG             i;
HANDLE            BoardHandle = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;


   init_keyboard();

   printf("\nASI RECEPTION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");

      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for (i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }          
         
         /* Open a handle on selected DELTA board */
         Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
         if (Result == VHDERR_NOERROR)
         {
            VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_BOARD_TYPE, &BoardType);

            /* Check the board type of the selected board */
            if(BoardType==VHD_BOARDTYPE_CODEC || BoardType==VHD_BOARDTYPE_ASI)
            {
               /* Loop */
               while (1)
               {
                  if (kbhit()) 
                  {
                     getch();
                     break;
                  }               
                  
                  /* Get the STC */
                  VHD_ASI_TIMESTAMP TimeStamp_X;
                  Result = VHD_GetSTC(BoardHandle,&TimeStamp_X);
                  if (Result == VHDERR_NOERROR)
                  {
                     printf("STC = %d ticks @90kHz + %d ticks @27MHz\n",TimeStamp_X.Base0_31, TimeStamp_X.Ext0_8);
                  }
                  else
                  {
                     printf("ERROR : Impossible to get the System Time Clock. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                     break;
                  }

                  PAUSE(1000);
               }
            }
            else
               printf("ERROR : The selected board is not a DELTA-codec/asi board\n");

              /* Close board handle */
               VHD_CloseBoardHandle(BoardHandle);
         }
         else
            printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}



