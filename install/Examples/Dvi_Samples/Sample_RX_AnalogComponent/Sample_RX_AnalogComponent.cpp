/*
   DELTA-dvi
   -------------
   ANALOG COMPONENT VIDEO RECEPTION STREAM SAMPLE APPLICATION
   (c) DELTACAST
   ----------------------------------------------------------

   This application demonstrates the use of the DELTA-dvi board and 
   of the VideoMasterHD SDK in an analog component video reception stream 
   use case.

   The application opens board and stream handles, configures them and enters
   in a reception loop.

   In order to compile this application, path to VideoMasterHD_Core.h inclusion 
   file and to VideoMasterHD.lib library file must be properly configured.

   In order to run this application, the DELTA-dvi board RX0 connector
   must be fed with an analog component video signal.

*/


/*** HEADER INCLUSION ********************************************************/

#if !defined(__linux__) && !defined(__APPLE__)
#include <Windows.h>
#endif

#include <stdio.h>

#if defined(__APPLE__)
#include "VideoMasterHD/VideoMasterHD_Core.h"
#include "VideoMasterHD/VideoMasterHD_Dvi.h"
#else
#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Dvi.h"
#endif

#include "../../Tools.h"

#if defined (__linux__) || defined (__APPLE__)
#define KEY_VIRTUAL  0x1b
#define KEY_VIRTUAL2 0x5b
#define KEY_LEFT     0x44
#define KEY_RIGHT    0x43
#define KEY_UP       0x41
#define KEY_DOWN     0x42
#include "../../Keyboard.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define PAUSE(param) usleep(param*1000)
#else
#define KEY_VIRTUAL  0xe0
#define KEY_LEFT     0x4b
#define KEY_RIGHT    0x4d
#define KEY_UP       0x48
#define KEY_DOWN     0x50
#define PAUSE(param) Sleep(param)
#include <conio.h>
#define init_keyboard() 
#define close_keyboard()
#endif

int main (int argc, char *argv[])
{
ULONG             Result,DllVersion,NbBoards,BoardType;
ULONG             i;
ULONG             NbFramesRecv = 0, NbFramesDropped = 0, BufferSize = 0;
HANDLE            BoardHandle = NULL, StreamHandle = NULL, SlotHandle = NULL;
ULONG             BrdId=(argc>1)?atoi(argv[1]):0;
ULONG             Height=0,Width=0,RefreshRate=0;
VHD_DVI_MODE      DviMode;
int               PxlShift=0, LineShift=0;
BYTE             *pBuffer=NULL;
BOOL32              Interlaced_B=FALSE;


   init_keyboard();

   printf("\nANALOG COMPONENT VIDEO RECEPTION STREAM SAMPLE APPLICATION\n(c) DELTACAST\n--------------------------------------------------------\n\n");
   
   /* Query VideoMasterHD information */
   Result = VHD_GetApiInfo(&DllVersion,&NbBoards);
   if (Result == VHDERR_NOERROR)
   {

      printf("VideoMasterHD DLL v%02d.%02d.%04d\n%u board%s detected\n\n",
         DllVersion>>24,(DllVersion>>16)&0xFF,DllVersion&0xFFFF,
         NbBoards,(NbBoards>1)?"s":"");


      if (NbBoards > 0)
      {
         /* Query DELTA boards information */
         for ( i = 0; i < NbBoards; i++)
         {
            PrintBoardInfo(i);
         }

 
        /* Open a handle on selected DELTA board */
         Result = VHD_OpenBoardHandle(BrdId,&BoardHandle,NULL,0);
         if (Result == VHDERR_NOERROR)
         {
            VHD_GetBoardProperty(BoardHandle, VHD_CORE_BP_BOARD_TYPE, &BoardType);

            /* Check the board type of the selected board */
            if(BoardType==VHD_BOARDTYPE_DVI)
            {
               /* Create a logical stream to receive from RX0 connector */
               Result = VHD_OpenStreamHandle(BoardHandle,VHD_ST_RX0,VHD_DVI_STPROC_DEFAULT,NULL,&StreamHandle,NULL);
               if (Result == VHDERR_NOERROR)
               {

                  /* Set the primary mode of this channel to analog component video */
                  Result = VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_MODE, VHD_DVI_MODE_ANALOG_COMPONENT_VIDEO);
                  if (Result == VHDERR_NOERROR)
                  {
                     printf("Waiting for incoming DVI-Component signal... Press any key to stop...\n");
                     do
                     {
                        /* Auto-detect Dvi mode */
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_DVI_SP_MODE,(ULONG*)&DviMode);
                        if(kbhit()) break;
                     }
                     while(DviMode != VHD_DVI_MODE_ANALOG_COMPONENT_VIDEO && Result != VHDERR_NOERROR);
                     
                     if(Result == VHDERR_NOERROR && DviMode == VHD_DVI_MODE_ANALOG_COMPONENT_VIDEO)
                     {                  
                        /* Configure RGBA reception */
                        VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_BUFFER_PACKING, VHD_BUFPACK_VIDEO_RGBA_32);
                        
                        /* Get auto-detected resolution */
                        Result = VHD_GetStreamProperty(StreamHandle,VHD_DVI_SP_ACTIVE_WIDTH,&Width);
                        if (Result == VHDERR_NOERROR)
                        {
                           Result = VHD_GetStreamProperty(StreamHandle,VHD_DVI_SP_ACTIVE_HEIGHT,&Height);
                           if (Result == VHDERR_NOERROR)
                           {
                              Result = VHD_GetStreamProperty(StreamHandle,VHD_DVI_SP_INTERLACED,(ULONG*)&Interlaced_B);
                              if (Result == VHDERR_NOERROR)
                              {
                                 Result = VHD_GetStreamProperty(StreamHandle,VHD_DVI_SP_REFRESH_RATE,&RefreshRate);
                                 if (Result == VHDERR_NOERROR)                              
                                 {
                                    printf("\nIncoming graphic resolution : %ux%u (%s)\n", Width, Height, Interlaced_B?"Interlaced":"Progressive");

                                    /* Configure stream. Only VHD_DVI_SP_ACTIVE_WIDTH, VHD_DVI_SP_ACTIVE_HEIGHT and 
                                    VHD_DVI_SP_INTERLACED, VHD_DVI_SP_REFRESH_RATE properties are required for DVI-D.
                                    VHD_DVI_SP_PIXEL_CLOCK, VHD_DVI_SP_TOTAL_WIDTH, VHD_DVI_SP_TOTAL_HEIGHT, 
                                    VHD_DVI_SP_H_SYNC, VHD_DVI_SP_H_FRONT_PORCH, VHD_DVI_SP_V_SYNC and 
                                    VHD_DVI_SP_V_FRONT_PORCH properties are not applicable for DVI-Component */ 
                                    
                                    VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_ACTIVE_WIDTH, Width);
                                    VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_ACTIVE_HEIGHT, Height);
                                    VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_INTERLACED, Interlaced_B);
                                    VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_REFRESH_RATE, RefreshRate);
                                    VHD_SetStreamProperty(StreamHandle,VHD_CORE_SP_TRANSFER_SCHEME,VHD_TRANSFER_SLAVED);

                                    /* Start stream */
                                    Result = VHD_StartStream(StreamHandle);
                                    if (Result == VHDERR_NOERROR)
                                    {

                                       printf("\nReception started, press ESC to stop...\n");

                                       /* Reception loop */
                                       while (1)
                                       {

                                          if (kbhit()) 
                                          {
                                             int c=getch();
                                             if(c==KEY_VIRTUAL)
                                             {
                                                c=getch();

         #ifdef __linux__
                                                if(c==KEY_VIRTUAL2)
                                                {
                                                   c=getch();
         #else
                                                if(1)
                                                {
         #endif
                                                   /* Adjust centering */
                                                   switch(c)
                                                   {
                                                   case KEY_LEFT : 
                                                      if(PxlShift<512)
                                                      {
                                                         PxlShift++; 
                                                         VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_H_SHIFT, (ULONG)PxlShift);  
                                                      }
                                                      break;
                                                   case KEY_RIGHT :
                                                      if(PxlShift>=-512)
                                                      {
                                                         PxlShift--; 
                                                         VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_H_SHIFT, (ULONG)PxlShift);  
                                                      }
                                                      break;
                                                   case KEY_UP : 
                                                      if(LineShift<8)
                                                      {
                                                         LineShift++; 
                                                         VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_V_SHIFT, (ULONG)LineShift);  
                                                      }
                                                      break;
                                                   case KEY_DOWN :
                                                      if(LineShift>=-8)
                                                      {
                                                         LineShift--; 
                                                         VHD_SetStreamProperty(StreamHandle,VHD_DVI_SP_V_SHIFT, (ULONG)LineShift);  
                                                      }
                                                      break;
                                                   default:
                                                      break;
                                                   }                                   
                                                }
                                                else
                                                   break;
                                             }
                                             else
                                                break;
                                          }

                                          /* Try to lock next slot */
                                          Result = VHD_LockSlotHandle(StreamHandle,&SlotHandle);

                                          if (Result == VHDERR_NOERROR)
                                          {

                                             Result = VHD_GetSlotBuffer(SlotHandle,VHD_DVI_BT_VIDEO,&pBuffer,&BufferSize);
                                             if (Result == VHDERR_NOERROR)
                                             {
                                                /* Do RX data processing here on pBuffer */
                                             }
                                             else
                                             {
                                                printf("\nERROR : Cannot get slot buffer. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                                break;
                                             }


                                             /* Unlock slot */
                                             VHD_UnlockSlotHandle(SlotHandle);

                                             /* Print some statistics */
                                             VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_COUNT,&NbFramesRecv);
                                             VHD_GetStreamProperty(StreamHandle,VHD_CORE_SP_SLOTS_DROPPED,&NbFramesDropped);
                                             printf("%u frames received (%u dropped)            \r",NbFramesRecv,NbFramesDropped);
                                             fflush(stdout);
                                          }
                                          else if (Result != VHDERR_TIMEOUT)
                                          {
                                             printf("\nERROR : Cannot lock slot on RX0 stream. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                             break;
                                          }
                                       }

                                       printf("\n");

                                       /* Stop stream */
                                       VHD_StopStream(StreamHandle);
                                    }
                                    else
                                       printf("ERROR : Cannot start RX0 stream on DELTA-dvi board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                                 }
                                 else
                                    printf("ERROR : Cannot detect incoming refresh rate from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                              }
                              else
                                 printf("ERROR : Cannot detect if incoming stream from RX0 is interlaced or progressive. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                           }
                           else
                              printf("ERROR : Cannot detect incoming active height from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                        }
                        else
                           printf("ERROR : Cannot detect incoming active width from RX0. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                     
                     }
                     else
                        printf("ERROR : Cannot detect DVI-Componen incoming signal. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
                  }
                  else
                     printf("ERROR : Cannot configure RX0 stream primary mode. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

                  /* Close stream handle */
                  VHD_CloseStreamHandle(StreamHandle);
               }
               else
                  printf("ERROR : Cannot open RX0 stream on DELTA-dvi board handle. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));
            }
            else
               printf("ERROR : The selected board is not a DELTA-dvi board\n");

           /* Close board handle */
            VHD_CloseBoardHandle(BoardHandle);
         }
         else
            printf("ERROR : Cannot open DELTA board %u handle. Result = 0x%08X (%s)\n",BrdId,Result, GetErrorDescription(Result));
      }
      else
         printf("No DELTA board detected, exiting...\n");
   }
   else
      printf("ERROR : Cannot query VideoMasterHD information. Result = 0x%08X (%s)\n",Result, GetErrorDescription(Result));

   close_keyboard();

   return 0;
}




