/*! VideomasterHD_Sdi.h

    Copyright (c) 2009, DELTACAST. All rights reserved.

    THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
    KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
    PURPOSE.

  Project : VideomasterHD

  Package : 

  Company : DELTACAST

  Author  : gt                             Date: 2009/06/26

  Purpose : 

*/


#ifndef _VIDEOMASTERHD_SDI_H_
#define _VIDEOMASTERHD_SDI_H_



/*_ CONSTANTS ________________________________________________________________
//
// This section defines the different constants used by VideomasterHD_Sdi
*/


/*_ VideomasterHD_Sdi STATUS INFORMATION BIT MASKS ___________________________________*/

#define VHD_MAXNB_VBICAPTURELINE 64       /*!VHD_MAXNB_VBICAPTURELINE
                                             Maximum number of video lines that may be configured for VBI
                                             capture using VHD_VbiSetCaptureLines                         */

   /*! RX channel status bit masks. These constants define status bit masks for the VHD_CORE_BP_RXx_STATUS board properties */
#define VHD_SDI_RXSTS_BADVIDEOSTD           0x00000004  /*! RX channel bad video standard indicator. This bit is set to '1' when the channel cannot recognize incoming video standard */
#define VHD_SDI_RXSTS_BADDATA               0x00000008  /*! RX channel bad data indicator. This bit is set to '1' when the channel detected errors in incoming video signal */
#define VHD_SDI_RXSTS_ALIGNED_COUPLE        0x00000020  /*! RX channel couple aligned indicator. This bit is set to '1' when the two started channels coupled in stereoscopy mode are aligned (<512 pxl) or four started channels coupled in 4k mode are aligned (<512 pxl)*/
#define VHD_SDI_RXSTS_DEPRECATED            0x00000040  /*! Deprecated */
#define VHD_SDI_RXSTS_DUAL_LINK_PHYSICAL    0x00004000  /*! RX channel Dual Link indicator */
#define VHD_SDI_RXSTS_QUAD_LINK_PHYSICAL    0x00000400  /*! RX channel Quad Link indicator */
#define VHD_SDI_RXSTS_LINK_A                0x00000080  /*! RX channel first link (A) indicator in a Dual/Quad Link stream */
#define VHD_SDI_RXSTS_LINK_B                0x00000800  /*! RX channel second link (B) indicator in a Dual/Quad Link stream */
#define VHD_SDI_RXSTS_LINK_C                0x00001000  /*! RX channel third link (C) indicator in a Quad Link stream */
#define VHD_SDI_RXSTS_LINK_D                0x00002000  /*! RX channel fourth link (D) indicator in a Quad Link stream */
#define VHD_SDI_RXSTS_LEVELB_3G             0x00000100  /*! RX channel 3G level B indicator */
#define VHD_SDI_RXSTS_CARRIER_UNDETECTED    0x00000200  /*! RX channel carrier undetected indicator. This bit is set to '1' when the carrier is not detected on incoming signal */

#define VHD_SDI_RXSTS_DL_LINK_A VHD_SDI_RXSTS_LINK_A 
#define VHD_SDI_RXSTS_DUAL_LINK VHD_SDI_RXSTS_DEPRECATED  /*! RX channel Dual Link virtual indicator */

   /*! Genlock status bit masks. These constants define status bit masks for the VHD_SDI_BP_GENLOCK_STATUS board properties */
#define VHD_SDI_GNLKSTS_NOREF     0x00000001  /*!VHD_SDI_GNLKSTS_NOREF
                                                 Genlocking circuitry <i>no reference</i> indicator. This bit
                                                 is set to '1' when no valid input reference is detected by
                                                 the genlocking circuitry                                     */
#define VHD_SDI_GNLKSTS_UNLOCKED  0x00000002  /*!VHD_SDI_GNLKSTS_UNLOCKED
                                                 Unlocked genlocking circuitry indicator. This bit is set to
                                                 '1' when the genlocking circuitry cannot lock itself onto the
                                                 incoming signal                                               */

   /*! HDMI status bit masks. These constants define status bit masks for the VHD_SDI_BP_HDMIx_STATUS board properties */
#define VHD_SDI_HDMISTS_BADVIDEOSTD 0x00000001  /*!VHD_SDI_HDMISTS_BADVIDEOSTD
                                                   HDMI source bad video standard indicator.This bit is set to
                                                   '1' when the HDMI source video standard is not supported by the monitor  */

/*_ VideomasterHD_Sdi EVENT SOURCES __________________________________________________*/

   /*! Event sources bit masks. These constants define event source bit masks for the StateChangeMask parameter of VHD_OpenBoardHandle */
#define VHD_SDI_EVTSRC_GENLOCK    0x04000000  /*! Genlock state changes event source */

/*_ VideomasterHD_Sdi DATA TYPE __________________________________________________*/
/*! Data types bit masks. These constants define data type bit masks for the VHD_CORE_SP_MUTED_DATA_MASK stream property */
#define VHD_SDI_DATA_VIDEO        0x00000001  /*! Video data */
#define VHD_SDI_DATA_ANC          0x00000002  /*! ANC data */
#define VHD_SDI_DATA_THUMBNAIL    0x00000004  /*! Thumbnail data */
#define VHD_SDI_DATA_VIDEO2       0x00000008  /*! Video data of the second channel */
#define VHD_SDI_DATA_ANC2         0x00000010  /*! ANC data of the second channel */
#define VHD_SDI_DATA_THUMBNAIL2   0x00000020  /*! Thumbnail data of the second channel */
#define VHD_SDI_DATA_VIDEO3       0x00000040  /*! Video data of the third channel */
#define VHD_SDI_DATA_VIDEO4       0x00000080  /*! Video data of the fourth channel */
#define VHD_SDI_DATA_ANC3         0x00000100  /*! ANC data of the third channel */
#define VHD_SDI_DATA_ANC4         0x00000200  /*! ANC data of the fourth channel */
#define VHD_SDI_DATA_THUMBNAIL3   0x00000400  /*! Thumbnail data of the third channel */
#define VHD_SDI_DATA_THUMBNAIL4   0x00000800  /*! Thumbnail data of the fourth channel */
#define VHD_SDI_DATA_RAW          0x00001000  /*! RAW data */
#define VHD_SDI_DATA_RAW2         0x00002000  /*! RAW data of the second channel */
#define VHD_SDI_DATA_RAW3         0x00004000  /*! RAW data of the third channel */
#define VHD_SDI_DATA_RAW4         0x00008000  /*! RAW data of the fourth channel */


/*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideomasterHD_Sdi
*/

/*_ VHD_SDI_BOARDPROPERTY _______________________________________________*/
/*!
   Summary
   VideomasterHD SDI board properties
   Description
   The VHD_SDI_BOARDPROPERTY enumeration lists all the board
   properties available on SDI cards.
   
   These values are used as indexes for VHD_GetBoardProperty and
   VHD_SetBoardProperty functions calls.
   See Also
   VHD_GetBoardProperty VHD_SetBoardProperty                     */
typedef enum _VHD_SDI_BOARDPROPERTY
{
   VHD_SDI_BP_RX0_STANDARD=ENUMBASE_SDI,  /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX0_STANDARD
                                             Incoming video standard on RX0 connector (see VHD_VIDEOSTANDARD)        */
   VHD_SDI_BP_RX1_STANDARD,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX1_STANDARD
                                             Incoming video standard on RX1 connector (see VHD_VIDEOSTANDARD)        */
   VHD_SDI_BP_RX2_STANDARD,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX2_STANDARD
                                             Incoming video standard on RX2 connector (see VHD_VIDEOSTANDARD)        */
   VHD_SDI_BP_RX3_STANDARD,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX3_STANDARD
                                             Incoming video standard on RX3 connector (see VHD_VIDEOSTANDARD)        */
   VHD_SDI_BP_CLOCK_SYSTEM,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_CLOCK_SYSTEM
                                             On read, read back clock system if genlock source is local.
                                             Otherwise, detected clock system on selected genlock input if locked.
                                             On write, board clock system selection and genlock system restart
                                             that could disturb RX/TX streams.
                                             (see VHD_CLOCKDIVISOR), default is VHD_CLOCKDIV_1.                      */
   VHD_SDI_BP_GENLOCK_SOURCE,             /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK_SOURCE
                                             Board genlock source selection (see VHD_GENLOCKSOURCE)
                                             , default is VHD_GENLOCK_LOCAL                                          */
   VHD_SDI_BP_GENLOCK_OFFSET,             /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK_OFFSET
                                             Phase offset (in pixels) to apply to genlocked TX streams
                                             towards their reference (default is 0, max is pixel number per frame)   */
   VHD_SDI_BP_GENLOCK_STATUS,             /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK_STATUS
                                             Genlock status (see VHD_SDI_GNLKSTS_xxx)                                */
   VHD_SDI_BP_GENLOCK_VIDEO_STANDARD,     /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK_VIDEO_STANDARD
                                             On read, detected video standard on selected genlock input
                                             if locked (see VHD_VIDEOSTANDARD). Because of a genlock reset,
                                             a detection following a clock divisor setting could failed.
                                             On write, configured genlock video standard
                                             (VHD_VIDEOSTD_* = manual setting, VHD_VIDEOSTD_AUTO_DETECT_GENLOCK = auto)
                                             , default is VHD_VIDEOSTD_AUTO_DETECT_GENLOCK                           */
   VHD_SDI_BP_GENLOCK_CIRCUITRY,          /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK_CIRCUITRY
                                             TRUE if the board own genlock circuitry, FALSE if no genlock
                                             circuitry is present. As an example, RX-only boards do not
                                             need genlock circuitry                                                  */
   VHD_SDI_BP_RX0_CLOCK_DIV,            /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX0_CLOCK_DIV
                                          Incoming clock divisor on RX0 connector (see VHD_CLOCKDIVISOR)             */
   VHD_SDI_BP_RX1_CLOCK_DIV,            /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX1_CLOCK_DIV
                                          Incoming clock divisor on RX1 connector (see VHD_CLOCKDIVISOR)             */
   VHD_SDI_BP_RX2_CLOCK_DIV,            /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX2_CLOCK_DIV
                                          Incoming clock divisor on RX2 connector (see VHD_CLOCKDIVISOR)             */
   VHD_SDI_BP_RX3_CLOCK_DIV,            /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX3_CLOCK_DIV
                                          Incoming clock divisor on RX3 connector (see VHD_CLOCKDIVISOR)             */
   VHD_SDI_BP_MASTERTIMESTAMP,          /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_MASTERTIMESTAMP
                                           Board master timestamp (global frame counter)                             */
   VHD_SDI_BP_HDMI0SOURCE,              /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_HDMI0SOURCE
                                          HDMI0 source (see VHD_HDMISOURCE)                                          */
   VHD_SDI_BP_HDMI1SOURCE,              /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_HDMI1SOURCE
                                          HDMI1 source (see VHD_HDMISOURCE)                                          */
   VHD_SDI_BP_HDMI0_STATUS,             /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_HDMI0_STATUS
                                          HDMI0 status (see VHD_SDI_HDMISTS_xxx)                                     */
   VHD_SDI_BP_HDMI1_STATUS,             /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_HDMI1_STATUS
                                          HDMI1 status (see VHD_SDI_HDMISTS_xxx)                                     */
   VHD_SDI_BP_HDMI0PACKING,              /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_HDMI0PACKING
                                          HDMI0 packing (see VHD_BUFFERPACKING)                                      */
   VHD_SDI_BP_HDMI1PACKING,              /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_HDMI1PACKING
                                          HDMI1 packing (see VHD_BUFFERPACKING)                                      */
   VHD_SDI_BP_GENLOCK_WAITING_FRAMES,   /*! reserved */
   VHD_SDI_BP_GENLOCK_JITTER_WINDOW,    /*! reserved */
   VHD_SDI_BP_GENLOCK_JITTER,           /*! reserved */
   VHD_SDI_BP_RX4_STANDARD,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX4_STANDARD
                                             Incoming video standard on RX3 connector (see VHD_VIDEOSTANDARD)        */
   VHD_SDI_BP_RX5_STANDARD,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX5_STANDARD
                                             Incoming video standard on RX3 connector (see VHD_VIDEOSTANDARD)        */
   VHD_SDI_BP_RX6_STANDARD,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX6_STANDARD
                                             Incoming video standard on RX3 connector (see VHD_VIDEOSTANDARD)        */
   VHD_SDI_BP_RX7_STANDARD,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX7_STANDARD
                                             Incoming video standard on RX3 connector (see VHD_VIDEOSTANDARD)        */
   VHD_SDI_BP_RX4_CLOCK_DIV,            /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX4_CLOCK_DIV
                                          Incoming clock divisor on RX0 connector (see VHD_CLOCKDIVISOR)             */
   VHD_SDI_BP_RX5_CLOCK_DIV,            /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX5_CLOCK_DIV
                                          Incoming clock divisor on RX1 connector (see VHD_CLOCKDIVISOR)             */
   VHD_SDI_BP_RX6_CLOCK_DIV,            /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX6_CLOCK_DIV
                                          Incoming clock divisor on RX2 connector (see VHD_CLOCKDIVISOR)             */
   VHD_SDI_BP_RX7_CLOCK_DIV,            /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX7_CLOCK_DIV
                                          Incoming clock divisor on RX3 connector (see VHD_CLOCKDIVISOR)             */
   VHD_SDI_BP_CLOCK_SYSTEM2,             /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_CLOCK_SYSTEM2
                                            On read, read back clock system2 if genlock2 source is local.
                                            Otherwise, detected clock system on selected genlock2 input
                                            if locked. On write, board clock system2 selection and
                                            genlock2 system restart that could disturb RX/TX streams.
                                            (see VHD_CLOCKDIVISOR), default is VHD_CLOCKDIV_1. This
                                            feature is only available on <b>DELTA-hd-elp-d 44</b>.        */
   VHD_SDI_BP_GENLOCK2_CIRCUITRY,       /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK2_CIRCUITRY
                                          TRUE if the board own genlock2 circuitry, FALSE if no genlock2
                                          circuitry is present.                                                      */
   VHD_SDI_BP_GENLOCK2_SOURCE,          /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK2_SOURCE
                                           Board genlock2 source selection (see VHD_GENLOCKSOURCE) ,
                                           default is VHD_GENLOCK_LOCAL. This feature is only available
                                           on <b>DELTA-hd-elp-d 44</b>.                                 */
   VHD_SDI_BP_GENLOCK2_OFFSET,             /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK2_OFFSET
                                              Phase offset (in pixels) to apply to genlocked TX streams
                                              towards their reference (default is 0, max is pixel number
                                              per frame). This feature is only available on <b>DELTA-hd-elp-d
                                              44</b>.                                                         */
   VHD_SDI_BP_GENLOCK2_STATUS,             /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK2_STATUS
                                              Genlock2 status (see VHD_SDI_GNLKSTS_xxx). This feature is
                                              only available on <b>DELTA-hd-elp-d 44</b>.                */
   VHD_SDI_BP_GENLOCK2_VIDEO_STANDARD,  /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_GENLOCK2_VIDEO_STANDARD
                                           On read, detected video standard on selected genlock2 input
                                           if locked (see VHD_VIDEOSTANDARD). Because of a genlock2
                                           reset, a detection following a clock divisor setting could
                                           failed. On write, configured genlock2 video standard
                                           (VHD_VIDEOSTD_* = manual setting,
                                           VHD_VIDEOSTD_AUTO_DETECT_GENLOCK = auto) , default is
                                           VHD_VIDEOSTD_AUTO_DETECT_GENLOCK. This feature is only
                                           available on <b>DELTA-hd-elp-d 44</b>.                      */
   VHD_SDI_BP_CLOCK_ADJUST,               /*!__VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_CLOCK_ADJUST
                                          Control the first onboard VCXO that is used to adjust the onboard reference clock frequency.
                                          please contact support@deltacast.tv if you need to use this function */
   VHD_SDI_BP_CLOCK2_ADJUST,              /*!__VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_CLOCK2_ADJUST
                                          Control the second onboard VCXO that is used to adjust the onboard reference clock2 frequency.
                                          please contact support@deltacast.tv if you need to use this function */
   VHD_SDI_BP_RX0_INTERFACE,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX0_INTERFACE
                                             Incoming interface on RX0 connector (see VHD_INTERFACE)        */
   VHD_SDI_BP_RX1_INTERFACE,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX1_INTERFACE
                                             Incoming interface on RX1 connector (see VHD_INTERFACE)        */
   VHD_SDI_BP_RX2_INTERFACE,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX2_INTERFACE
                                             Incoming interface on RX2 connector (see VHD_INTERFACE)        */
   VHD_SDI_BP_RX3_INTERFACE,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX3_INTERFACE
                                             Incoming interface on RX3 connector (see VHD_INTERFACE)        */
   VHD_SDI_BP_RX4_INTERFACE,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX4_INTERFACE
                                             Incoming interface on RX4 connector (see VHD_INTERFACE)        */
   VHD_SDI_BP_RX5_INTERFACE,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX5_INTERFACE
                                             Incoming interface on RX5 connector (see VHD_INTERFACE)        */
   VHD_SDI_BP_RX6_INTERFACE,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX6_INTERFACE
                                             Incoming interface on RX6 connector (see VHD_INTERFACE)        */
   VHD_SDI_BP_RX7_INTERFACE,               /*!_VHD_SDI_BOARDPROPERTY::VHD_SDI_BP_RX7_INTERFACE
                                             Incoming interface on RX7 connector (see VHD_INTERFACE)        */
   NB_VHD_SDI_BOARDPROPERTIES
} VHD_SDI_BOARDPROPERTY;


/*_ VHD_SDI_STREAMPROPERTY ______________________________________________*/
/*!
Summary
VideoMasterHD SDI streams properties

Description
The VHD_SDI_STREAMPROPERTY enumeration lists all the available SDI
stream properties.

These values are used as indexes for VHD_GetStreamProperty and 
VHD_SetStreamProperty functions calls.

See Also
VHD_GetStreamProperty
VHD_SetStreamProperty
*/
typedef enum _VHD_SDI_STREAMPROPERTY
{
   VHD_SDI_SP_TX_GENLOCK=ENUMBASE_SDI,          /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_TX_GENLOCK
                                                   TX logical stream genlocking control (default is FALSE)  */
   VHD_SDI_SP_VIDEO_STANDARD,                   /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_VIDEO_STANDARD
                                                   Logical stream video standard selection (see VHD_VIDEOSTANDARD)
                                                   ,default is VHD_VIDEOSTD_S259M_PAL for SD boards
                                                  and VHD_VIDEOSTD_S274M_1080p_30Hz for HD boards           */
   VHD_SDI_SP_COMPUTE_CRC,					         /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_COMPUTE_CRC
                          					            Only available with transmission raw stream.
                                                   If this property is set to TRUE, the board computes
                          					            and overrides the CRC of each HD line (EDH packet of each SD
                          					            field). Otherwise, the board transmits the provided buffer
                                                   without modifying CRC and EDH (default is FALSE)         */
   VHD_SDI_SP_VBI_10BITS,                       /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_VBI_10BITS
                                                   VBI 10-Bits or 8-bits packing selection
                                                   ,default is FALSE (8-Bits)                               */
   VHD_SDI_SP_TX_GENLOCK_SELECTION,             /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_TX_GENLOCK_SELECTION
                                                   specify genlock circuitry to use if several are present
                                                   , default is Gnlk 0                                      */
   VHD_SDI_SP_INTERFACE,                        /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_INTERFACE
                                                   specify how the stream must be managed (see VHD_INTERFACE)
                                                   , default is VHD_INTERFACE_AUTO */
   VHD_SDI_SP_YUVK_NO_CHROMA_ON_KEY,            /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_YUVK_NO_CHROMA_ON_KEY
                                                   if false (default), chroma values of Key stream are equal to Fill ones
                                                   if true, chroma values of Key stream are forced to Neutral value (512 on 10-bit) */
   VHD_SDI_SP_ENABLE_VBI_PARSING,               /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_ENABLE_VBI_PARSING
                                                   if false (default). VANC, on VBI line set for capture, are not detected
                                                   if true, parsing of 10 bits VBI for searching VANC data when set for capture*/
   VHD_SDI_SP_CRC_LINE_ERRORS,                  /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_CRC_LINE_ERRORS
                                                   Counts the number of slots with errors in CRC line since the stream has been
                                                   started                                                    */
   VHD_SDI_SP_CHECK_CRC_SWITCHING_LINE,         /*!_VHD_SDI_STREAMPROPERTY::VHD_SDI_SP_CHECK_CRC_SWITCHING_LINE
                                                   If this property is set to TRUE, the CRC check on the switching line 
                                                   is disabled (default is TRUE)                             */
   NB_VHD_SDI_STREAMPROPERTIES
} VHD_SDI_STREAMPROPERTY;


/*_ VHD_CLOCKDIVISOR __________________________________________*/
/*!
Summary
VideomasterHD SDI clock systems

Description
The VHD_CLOCKDIVISOR enumeration lists all supported sample
rate divisors used in VHD_SDI_BP_CLOCK_SYSTEM board property.

See Also
<LINK VHD_SDI_BOARDPROPERTY, VHD_SDI_BP_CLOCK_SYSTEM>              
*/
typedef enum _VHD_CLOCKDIVISOR
{
   VHD_CLOCKDIV_1 = 0,           /*!_VHD_CLOCKDIVISOR::VHD_CLOCKDIV_1
                                    Clock rate divisor to produce a frame rate of 24,25,50,30 or
                                    60 fps (default)                                             */
   VHD_CLOCKDIV_1001,            /*!_VHD_CLOCKDIVISOR::VHD_CLOCKDIV_1001
                                    Clock rate divisor to produce a frame rate of 23.98, 29.97 or
                                    59.94 fps                                                     */
   NB_VHD_CLOCKDIVISORS
} VHD_CLOCKDIVISOR;


/*_ VHD_GENLOCKSOURCE ________________________________________________*/
/*!
Summary
VideomasterHD SDI board genlock sources

Description
The VHD_GENLOCKSOURCE enumeration lists all supported genlock
sources used in VHD_SDI_BP_GENLOCK_SOURCE board property.

See Also
<LINK VHD_SDI_BOARDPROPERTY, VHD_SDI_BP_GENLOCK_SOURCE>              
*/
typedef enum _VHD_GENLOCKSOURCE
{
   VHD_GENLOCK_LOCAL = 0,     /*! Local clock (default) */
   VHD_GENLOCK_RX0,           /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_RX0
                                 Genlock source is RX0 SDI input */
   VHD_GENLOCK_RX1,           /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_RX1
                                 Genlock source is RX1 SDI input */
   VHD_GENLOCK_BB,            /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_BB
                                 Genlock source is analog BlackBurst input */
   VHD_GENLOCK_RX2,           /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_RX2
                              Genlock source is RX0 SDI input */
   VHD_GENLOCK_RX3,           /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_RX3
                              Genlock source is RX1 SDI input */
   VHD_GENLOCK_RX4,           /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_RX4
                              Genlock source is RX1 SDI input */
   VHD_GENLOCK_RX5,           /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_RX5
                              Genlock source is RX1 SDI input */
   VHD_GENLOCK_RX6,           /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_RX6
                              Genlock source is RX1 SDI input */
   VHD_GENLOCK_RX7,           /*!_VHD_GENLOCKSOURCE::VHD_GENLOCK_RX7
                              Genlock source is RX1 SDI input */

   NB_VHD_GENLOCKSOURCES
} VHD_GENLOCKSOURCE;


/*_ VHD_VIDEOSTANDARD ________________________________________________*/
/*!
   Summary
   VideomasterHD supported SDI video standards
   Description
   The VHD_VIDEOSTANDARD enumeration lists all the supported SDI
   video standards
   
   These values are used in VHD_SDI_SP_VIDEO_STANDARD stream
   property.
   See Also
   <link VHD_SDI_STREAMPROPERTY, VHD_SDI_SP_VIDEO_STANDARD>      */
  

typedef enum _VHD_VIDEOSTANDARD
{
   VHD_VIDEOSTD_S274M_1080p_25Hz = 0,  /*! SMPTE 274M - HD 1080p @ 25Hz standard */
   VHD_VIDEOSTD_S274M_1080p_30Hz,      /*! SMPTE 274M - HD 1080p @ 30Hz standard (default) */
   VHD_VIDEOSTD_S274M_1080i_50Hz,      /*! SMPTE 274M - HD 1080i @ 50Hz standard */
   VHD_VIDEOSTD_S274M_1080i_60Hz,      /*! SMPTE 274M - HD 1080i @ 60Hz standard */
   VHD_VIDEOSTD_S296M_720p_50Hz,       /*! SMPTE 296M - HD 720p @ 50Hz standard */
   VHD_VIDEOSTD_S296M_720p_60Hz,       /*! SMPTE 296M - HD 720p @ 60Hz standard */
   VHD_VIDEOSTD_S259M_PAL,             /*! SMPTE 259M - SD PAL standard */
   VHD_VIDEOSTD_S259M_NTSC,            /*! SMPTE 259M - SD NTSC standard */
   VHD_VIDEOSTD_S274M_1080p_24Hz,      /*! SMPTE 274M - HD 1080p @ 24Hz standard */
   VHD_VIDEOSTD_S274M_1080p_60Hz,      /*! SMPTE 274M - 3G 1080p @ 60Hz standard */
   VHD_VIDEOSTD_S274M_1080p_50Hz,      /*! SMPTE 274M - 3G 1080p @ 50Hz standard */
   VHD_VIDEOSTD_S274M_1080psf_24Hz,    /*! SMPTE 274M - HD 1080psf @ 24Hz standard */
   VHD_VIDEOSTD_S274M_1080psf_25Hz,    /*! SMPTE 274M - HD 1080psf @ 25Hz standard */
   VHD_VIDEOSTD_S274M_1080psf_30Hz,    /*! SMPTE 274M - HD 1080psf @ 30Hz standard */
   VHD_VIDEOSTD_S296M_720p_24Hz,       /*! SMPTE 296M - HD 720p @ 24Hz standard */
   VHD_VIDEOSTD_S296M_720p_25Hz,       /*! SMPTE 296M - HD 720p @ 25Hz standard */
   VHD_VIDEOSTD_S296M_720p_30Hz,       /*! SMPTE 296M - HD 720p @ 30Hz standard */
   VHD_VIDEOSTD_S2048M_2048p_24Hz,     /*! SMPTE 2048M - HD 2048p @ 24 Hz standard */
   VHD_VIDEOSTD_S2048M_2048p_25Hz,     /*! SMPTE 2048M - HD 2048p @ 25 Hz standard */
   VHD_VIDEOSTD_S2048M_2048p_30Hz,     /*! SMPTE 2048M - HD 2048p @ 30 Hz standard */
   VHD_VIDEOSTD_S2048M_2048psf_24Hz,   /*! SMPTE 2048M - HD 2048psf @ 24 Hz standard */
   VHD_VIDEOSTD_S2048M_2048psf_25Hz,   /*! SMPTE 2048M - HD 2048psf @ 25 Hz standard */
   VHD_VIDEOSTD_S2048M_2048psf_30Hz,   /*! SMPTE 2048M - HD 2048psf @ 30 Hz standard */
   VHD_VIDEOSTD_S2048M_2048p_60Hz,     /*! SMPTE 2048M - 3G 2048p @ 60Hz standard */
   VHD_VIDEOSTD_S2048M_2048p_50Hz,     /*! SMPTE 2048M - 3G 2048p @ 50Hz standard */
   VHD_VIDEOSTD_S2048M_2048p_48Hz,     /*! SMPTE 2048M - 3G 2048p @ 50Hz standard */
   VHD_VIDEOSTD_3840x2160p_24Hz,       /*! 3840x2160 - 4x HD 1080p @ 24Hz merged */
   VHD_VIDEOSTD_3840x2160p_25Hz,       /*! 3840x2160 - 4x HD 1080p @ 25Hz merged */
   VHD_VIDEOSTD_3840x2160p_30Hz,       /*! 3840x2160 - 4x HD 1080p @ 30Hz merged */
   VHD_VIDEOSTD_3840x2160p_50Hz,       /*! 3840x2160 - 4x 3G 1080p @ 50Hz merged */
   VHD_VIDEOSTD_3840x2160p_60Hz,       /*! 3840x2160 - 4x 3G 1080p @ 60Hz merged */
   VHD_VIDEOSTD_4096x2160p_24Hz,       /*! 4096x2160 - 4x HD 2048p @ 24Hz merged */
   VHD_VIDEOSTD_4096x2160p_25Hz,       /*! 4096x2160 - 4x HD 2048p @ 25Hz merged */
   VHD_VIDEOSTD_4096x2160p_30Hz,       /*! 4096x2160 - 4x HD 2048p @ 30Hz merged */
   VHD_VIDEOSTD_4096x2160p_48Hz,       /*! 4096x2160 - 4x 3G 2048p @ 48Hz merged */
   VHD_VIDEOSTD_4096x2160p_50Hz,       /*! 4096x2160 - 4x 3G 2048p @ 50Hz merged */
   VHD_VIDEOSTD_4096x2160p_60Hz,       /*! 4096x2160 - 4x 3G 2048p @ 60Hz merged */
   NB_VHD_VIDEOSTANDARDS               
} VHD_VIDEOSTANDARD;

/*!VHD_VIDEOSTD_AUTO_DETECT_GENLOCK
   Auto detection of genlock video standard (used with <link VHD_SDI_BOARDPROPERTY, VHD_SDI_BP_GENLOCK_VIDEO_STANDARD>) */
#define VHD_VIDEOSTD_AUTO_DETECT_GENLOCK   (VHD_VIDEOSTANDARD)(-1)



/*_ VHD_SDI_STREAMPROCMODE ________________________________________*/
/*!
Summary
VideoMasterHD SDI streams processing modes

Description
The VHD_SDI_STREAMPROCMODE enumeration lists all the SDI stream 
processing modes.

These values are used during VHD_OpenStreamHandle function calls.

See Also
VHD_OpenStreamHandle
*/

typedef enum _VHD_SDI_STREAMPROCMODE
{
   VHD_SDI_STPROC_RAW=ENUMBASE_SDI,           /*!_VHD_SDI_STREAMPROCMODE::VHD_SDI_STPROC_RAW
                                        Raw data mode (full bitstream handling) */
   VHD_SDI_STPROC_JOINED,            /*! Joined processed mode (synchronous video/anc handling) */
   VHD_SDI_STPROC_DISJOINED_VIDEO,   /*!_VHD_SDI_STREAMPROCMODE::VHD_SDI_STPROC_DISJOINED_VIDEO
                                        Disjoined processed mode (video handling only) */
   VHD_SDI_STPROC_DISJOINED_ANC,     /*!_VHD_SDI_STREAMPROCMODE::VHD_SDI_STPROC_DISJOINED_ANC
                                        Disjoined processed mode (ANC/VBI handling only) */
   VHD_SDI_STPROC_DISJOINED_THUMBNAIL,/*!_VHD_SDI_STREAMPROCMODE::VHD_SDI_STPROC_DISJOINED_THUMBNAIL
                                        Disjoined processed mode (thumbnail handling only) */
   NB_VHD_SDI_STREAMPROCMODE
} VHD_SDI_STREAMPROCMODE;

/*_ VHD_SDI_BUFFERTYPE ________________________________________*/
/*!
   Summary
   VideoMasterHD SDI buffer type
   Description
   The VHD_SDI_BUFFERTYPE enumeration lists all the SDI buffer
   types.
   
   These values are used during VHD_GetSlotBuffer function calls
   See Also
   VHD_GetSlotBuffer, VHD_STREAMTYPE
   Remarks
   Currently, only one buffer type is available                  */

typedef enum _VHD_SDI_BUFFERTYPE
{
   VHD_SDI_BT_VIDEO = 0,               /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_VIDEO
                                        SDI video frames buffer type */
   VHD_SDI_BT_VIDEO_2 = 1,             /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_VIDEO_2
                                        Second SDI video frames buffer type (only for VHD_ST_COUPLED_* stream types)*/
   VHD_SDI_BT_ANC_C = 2,               /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_C
                                        ANC C playlist buffer type */
   VHD_SDI_BT_ANC_Y = 3,               /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_Y
                                        ANC Y playlist buffer type */
   VHD_SDI_BT_ANC_C_2 = 4,             /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_C_2
                                        Second ANC C playlist buffer type (only for VHD_ST_COUPLED_* stream types)*/
   VHD_SDI_BT_ANC_Y_2 = 5,             /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_Y_2
                                        Second ANC Y playlist buffer type (only for VHD_ST_COUPLED_* stream types)*/
   VHD_SDI_BT_VIDEO_3 = 6,             /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_VIDEO_3
                                        Third SDI video frames buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_ANC_C_3 = 7,             /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_C_3
                                        Third ANC C playlist buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_ANC_Y_3 = 8,             /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_Y_3
                                        Third ANC Y playlist buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_VIDEO_4 = 9,             /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_VIDEO_4
                                        Fourth SDI video frames buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_ANC_C_4 = 10,            /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_C_4
                                        Fourth ANC C playlist buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_ANC_Y_4 = 11,            /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_Y_4
                                        Fourth ANC Y playlist buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_THUMBNAIL = 12,          /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_THUMBNAIL
                                        SDI thumbnail frames buffer type */
   VHD_SDI_BT_THUMBNAIL_2 = 13,        /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_THUMBNAIL_2
                                        Second SDI video thumbnail buffer type (only for VHD_ST_COUPLED_* stream types)*/
   VHD_SDI_BT_ANC_C_B = 14,            /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_C_B
                                        ANC C playlist buffer type (dual)*/
   VHD_SDI_BT_ANC_Y_B = 15,            /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_Y_B
                                        ANC Y playlist buffer type (dual)*/
   VHD_SDI_BT_ANC_C_2_B = 16,          /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_C_2_B
                                        Second ANC C playlist buffer type (only for dual VHD_ST_COUPLED_* stream types)*/
   VHD_SDI_BT_ANC_Y_2_B = 17,          /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_Y_2_B
                                        Second ANC Y playlist buffer type (only for dual VHD_ST_COUPLED_* stream types)*/
   VHD_SDI_BT_ANC_C_3_B = 18,          /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_C_3_B
                                        Third ANC C playlist buffer type (only for dual VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_ANC_Y_3_B = 19,          /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_Y_3_B
                                        Third ANC Y playlist buffer type (only for dual VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_ANC_C_4_B = 20,          /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_C_4_B
                                        Fourth ANC C playlist buffer type (only for dual VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_ANC_Y_4_B = 21,          /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_ANC_Y_4_B
                                        Fourth ANC Y playlist buffer type (only for dual VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_THUMBNAIL_3 = 22,        /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_THUMBNAIL_3
                                        Third SDI video thumbnail buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_THUMBNAIL_4 = 23,        /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_THUMBNAIL_4
                                        Fourth SDI video thumbnail buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_RAW = 24,                /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_RAW
                                        RAW buffer type */
   VHD_SDI_BT_RAW_2 = 25,              /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_RAW_2
                                        Second RAW buffer type (only for VHD_ST_COUPLED_* stream types)*/
   VHD_SDI_BT_RAW_3 = 26,              /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_RAW
                                        Third RAW buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   VHD_SDI_BT_RAW_4 = 27,              /*!_VHD_SDI_BUFFERTYPE::VHD_SDI_BT_RAW_2
                                        Fourth RAW buffer type (only for VHD_ST_COUPLED_RX0123 or VHD_ST_COUPLED_TX0123 stream types)*/
   NB_VHD_SDI_BUFFERTYPE
} VHD_SDI_BUFFERTYPE;


/*_ VHD_SAMPLETYPE ____________________________________________*/
/*!
   Summary
   VideoMasterHD SDI sample stream types
   Description
   The VHD_SAMPLETYPE enumeration lists the two available video
   \sample types (Y and C).
   
   It is used to define streams to look for ANC and VBI data in.
   
   
   
   These values are used during VHD_SlotAnc* and VHD_SlotVbi*
   function calls.
   See Also
   VHD_SlotAncGetNbPackets VHD_SlotAncGetPacket VHD_SlotAncInsertPacket
   VHD_SlotVbiGetLine VHD_SlotVbiInsertLine                             */

typedef enum _VHD_SAMPLETYPE
{
   VHD_SAMPLE_Y = 0,                /*! Luma ancillary data stream */
   VHD_SAMPLE_C,                    /*! Chroma ancillary data stream, or SD ANC/VBI stream */
   VHD_SAMPLE_Y_2,                  /*! Second luma ancillary data stream (only in stereoscopic and 4K modes)*/
   VHD_SAMPLE_C_2,                  /*! Second chroma ancillary data stream, or SD ANC/VBI stream (only in stereoscopic and 4K modes)*/
   VHD_SAMPLE_Y_3,                  /*! Third luma ancillary data stream (only in 4k mode)*/
   VHD_SAMPLE_C_3,                  /*! Third chroma ancillary data stream, or SD ANC/VBI stream (only in 4k mode)*/
   VHD_SAMPLE_Y_4,                  /*! Fourth luma ancillary data stream (only in 4k mode)*/
   VHD_SAMPLE_C_4,                  /*! Fourth chroma ancillary data stream, or SD ANC/VBI stream (only in 4k mode)*/
   VHD_SAMPLE_Y_B,                  /*! Additional luna ancillary data stream (only in dual-link modes) */
   VHD_SAMPLE_C_B,                  /*! Additional chroma ancillary data streamm, or SD ANC/VBI stream (only in dual-link modes) */
   VHD_SAMPLE_Y_2_B,                /*! Additional second luna ancillary data stream (only in stereoscopic and 4K modes + dual-link modes ) */
   VHD_SAMPLE_C_2_B,                /*! Additional second chroma ancillary data streamm, or SD ANC/VBI stream (only in stereoscopic and 4K modes + dual-link modes ) */
   VHD_SAMPLE_Y_3_B,                /*! Additional third luna ancillary data stream (only in 4K modes + dual-link modes ) */
   VHD_SAMPLE_C_3_B,                /*! Additional third chroma ancillary data streamm, or SD ANC/VBI stream (only in 4K modes + dual-link modes ) */
   VHD_SAMPLE_Y_4_B,                /*! Additional fourth luna ancillary data stream (only in 4K modes + dual-link modes ) */
   VHD_SAMPLE_C_4_B,                /*! Additional fourth chroma ancillary data streamm, or SD ANC/VBI stream (only in 4K modes + dual-link modes ) */
   NB_VHD_SAMPLETYPES
} VHD_SAMPLETYPE;

/*!VHD_SAMPLE_Y_AND_C
Setting the SampleStream value in VHD_TIMECODEINFO structure with the VHD_SlotExtractTimecode function to VHD_SAMPLE_Y_AND_C on reception streams result in looking for timecode in both Y and C streams */
#define VHD_SAMPLE_Y_AND_C  (VHD_SAMPLETYPE)(-1)


/*_ VHD_HDMISOURCE _____________________________________________*/
/*!
   Summary
   VideomasterHD HDMI source
   Description
   The VHD_HDMISOURCE enumeration lists all the available sources
   for the HDMI monitoring
   
   These values are used in VHD_SDI_BP_HDMI0SOURCE and 
   VHD_SDI_BP_HDMI1SOURCE board property and only apply
   to 3G boards.
   See Also
   <link VHD_SDI_BOARDPROPERTY, VHD_SDI_BP_HDMI0SOURCE>
   <link VHD_SDI_BOARDPROPERTY, VHD_SDI_BP_HDMI1SOURCE>
*/
typedef enum _VHD_HDMISOURCE
{
   VHD_HDMISRC_NONE,                      /*! No signal */
   VHD_HDMISRC_PHYSICAL_RX0,              /*! Physical RX0 signal*/
   VHD_HDMISRC_PHYSICAL_RX1,              /*! Physical RX1 signal*/
   VHD_HDMISRC_PHYSICAL_TX0,              /*! Physical TX0 signal*/
   VHD_HDMISRC_PHYSICAL_TX1,              /*! Physical TX1 signal*/
   VHD_HDMISRC_PHYSICAL_TX2,              /*! Physical TX2 signal*/
   VHD_HDMISRC_PHYSICAL_TX3,              /*! Physical TX3 signal*/
   VHD_HDMISRC_PHYSICAL_TX0_SBS_TX1,      /*! Physical TX0 and TX1 signals side by side*/
   VHD_HDMISRC_PHYSICAL_TX0_SBS_TX2,      /*! Physical TX0 and TX2 signals side by side*/
   VHD_HDMISRC_PHYSICAL_TX2_SBS_TX3,      /*! Physical TX2 and TX3 signals side by side*/
   VHD_HDMISRC_PHYSICAL_TX1_SBS_TX3,      /*! Physical TX1 and TX3 signals side by side*/
   NB_VHD_HDMISOURCE
}VHD_HDMISOURCE;

/*_ VHD_INTERFACE _____________________________________________*/
/*!
   Summary
   VideomasterHD interface
   Description
   The VHD_INTERFACE enumeration lists all the available interface
   
   These values are used in VHD_SDI_SP_INTERFACE
   See Also
   <link VHD_SDI_BOARDPROPERTY, VHD_SDI_SP_INTERFACE>
*/
typedef enum _VHD_INTERFACE
{
   VHD_INTERFACE_DEPRECATED,                    /*! */
   VHD_INTERFACE_SD_259,                        /*! SD SMPTE 259 interface*/
   VHD_INTERFACE_HD_292_1,                      /*! HD SMPTE 291-1 interface*/
   VHD_INTERFACE_HD_DUAL_372,                   /*! HD Dual Link SMPTE 372 interface*/
   VHD_INTERFACE_3G_A_425_1,                    /*! 3G-A SMPTE 425-1 interface*/
   VHD_INTERFACE_4XHD_QUADRANT,                 /*! 4xHD interface (4K image is split into 4 quadrants for transport)*/
   VHD_INTERFACE_4X3G_A_QUADRANT,               /*! 4x3G-A interface (4K image is split into 4 quadrants for transport)*/
   VHD_INTERFACE_SD_DUAL,                       /*! SD Dual Link (application of SMPTE 372 to SD) */
   VHD_INTERFACE_3G_A_DUAL,                     /*! 3G-A Dual interface (application of SMPTE 372 to 3GA)*/
   VHD_INTERFACE_3G_B_DL_425_1,                 /*! 3G-B SMPTE 425-1 interface for mapping of SMPTE ST 372 dual-link*/
   VHD_INTERFACE_4X3G_B_DL_QUADRANT,            /*! 4x3G-B SMPTE 425-1 interface for mapping of SMPTE ST 372 dual-link (4K image is split into 4 quadrants for transport)*/
   VHD_INTERFACE_2X3G_B_DS_425_3,               /*! 4x3G-B SMPTE 425-3 interface  (4K image is split into 4 images with the 2-sample interleave division rule for transport)*/
   VHD_INTERFACE_4X3G_A_425_5,                  /*! 4x3G-A SMPTE 425-5 interface  (4K image is split into 4 images with the 2-sample interleave division rule for transport)*/
   VHD_INTERFACE_4X3G_B_DL_425_5,               /*! 4x3G-B SMPTE 425-5 interface  (4K image is split into 4 images with the 2-sample interleave division rule for transport)*/
   VHD_INTERFACE_3G_B_DL_425_1_DUAL,            /*! 3G-B SMPTE 425-1 interface for mapping of SMPTE ST 372 dual-link, dual interface*/
   VHD_INTERFACE_2X3G_B_DS_425_3_DUAL,          /*! 4x3G-B SMPTE 425-3 interface  (4K image is split into 4 images with the 2-sample interleave division rule for transport), dual interface*/
   VHD_INTERFACE_4XHD_QUADRANT_DUAL,            /*! 4xHD interface (4K image is split into 4 quadrants for transport), dual interface*/
   VHD_INTERFACE_4X3G_A_QUADRANT_DUAL,          /*! 4x3G-A interface (4K image is split into 4 quadrants for transport), dual interface*/
   VHD_INTERFACE_4X3G_A_425_5_DUAL,             /*! 4x3G-A SMPTE 425-5 interface  (4K image is split into 4 images with the 2-sample interleave division rule for transport), dual interface*/
   VHD_INTERFACE_4X3G_B_DL_QUADRANT_DUAL,       /*! 4x3G-B SMPTE 425-1 interface for mapping of SMPTE ST 372 dual-link (4K image is split into 4 quadrants for transport), dual interface*/
   VHD_INTERFACE_4X3G_B_DL_425_5_DUAL,          /*! 4x3G-B SMPTE 425-5 interface  (4K image is split into 4 images with the 2-sample interleave division rule for transport), dual interface*/
   NB_VHD_INTERFACE
}VHD_INTERFACE;

#define VHD_INTERFACE_AUTO                   VHD_INTERFACE_DEPRECATED                     /*! Backward compatibility */
#define VHD_INTERFACE_3G_B_425_1_DL          VHD_INTERFACE_3G_B_DL_425_1                  /*! Backward compatibility */
#define VHD_INTERFACE_4XHD                   VHD_INTERFACE_4XHD_QUADRANT                  /*! Backward compatibility */
#define VHD_INTERFACE_4X3G_A                 VHD_INTERFACE_4X3G_A_QUADRANT                /*! Backward compatibility */
#define VHD_INTERFACE_4X3G_B_DL              VHD_INTERFACE_4X3G_B_DL_QUADRANT             /*! Backward compatibility */
#define VHD_INTERFACE_3G_B_425_1_DL_DUAL     VHD_INTERFACE_3G_B_DL_425_1_DUAL             /*! Backward compatibility */
#define VHD_INTERFACE_4XHD_DUAL              VHD_INTERFACE_4XHD_QUADRANT_DUAL             /*! Backward compatibility */
#define VHD_INTERFACE_4X3G_A_DUAL            VHD_INTERFACE_4X3G_A_QUADRANT_DUAL           /*! Backward compatibility */
#define VHD_INTERFACE_4X3G_B_DL_DUAL         VHD_INTERFACE_4X3G_B_DL_QUADRANT_DUAL        /*! Backward compatibility */

/*_ STRUCTURES _______________________________________________________________
//
// This section defines the different structures used by VideomasterHD_Sdi
*/

/*_ VHD_ANCPACKET ___________________________________________________*/
/*!
   Summary
   Ancillary data packet
   Description
   The VHD_ANCPACKET structure abstracts an ancillary data
   packet. This structure is used by the VHD_SlotAncGetPacket
   and VHD_SlotAncInsertPacket API functions.
   See Also
   VHD_SlotAncGetPacket VHD_SlotAncInsertPacket               */

typedef struct _VHD_ANCPACKET
{
   BYTE  DataID;              /*! 8 LSB of ANC packet Data ID field */
   union
   {
      BYTE  DataBlockNumber;  /*! 8 LSB of ANC packet Data Block Number field (Type 1 packets) */
      BYTE  SecondaryDataID;  /*! 8 LSB of ANC packet Secondary Data ID field (Type 2 packets) */
   };									/*! */
   BYTE  DataCount;           /*! Number of User Data words in this packet */
   WORD *pUserDataWords;      /*! Pointer to ANC packet User Data Words */
   BOOL32  InHANC;              /*! TRUE if this packet concerns horizontal blanking space, FALSE if located in horizontal active space. Please note that this only concerns packets of VANC space since during active lines, ANC are only allowed during horizontal blanking */
} VHD_ANCPACKET;



/*_ VHD_VBILINE __________________________________________________*/
/*!
   Summary
   VBI line capture configuration
   Description
   The VHD_VBILINE structure defines a capture parameter for VBI
   data. This structure is used in an array in
   VHD_VbiSetCapturedLines in order to configure up to
   VHD_MAXNB_VBICAPTURELINE lines to capture VBI from
   See Also
   VHD_VbiSetCaptureLines                                        */

typedef struct _VHD_VBILINE
{
   int   LineNumber;    /*! Specifies the line number (1-based) to capture VBI from */
   BOOL32  CaptureFromY;  /*! Specifies if Y samples must be captured on this line */
   BOOL32  CaptureFromC;  /*! Specifies if C samples must be captured on this line */
} VHD_VBILINE;





#ifndef EXCLUDE_API_FUNCTIONS

#ifdef __cplusplus
extern "C" {
#endif

/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideomasterHD_Sdi
*/

/*** VHD_SlotGetNbCrcLineErrors ****************************************************/
/*!VHD_SlotGetNbCrcLineErrors@HANDLE@ULONG *
   Summary
   Slot number of CRC line errors query 
   Description
   This function retrieves number the number of CRC line errors
   associated to the given slot.
   Parameters
   SlotHandle :      Handle of the slot to operate on
   pNbCrcLineErrors : Pointer to caller\-allocated variable
                     receiving the number of CRC line errors
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)      */
ULONG VIDEOMASTER_HD_API VHD_SlotGetNbCrcLineErrors (HANDLE SlotHandle, ULONG *pNbCrcLineErrors);

/*_ Ancillary data buffers navigation functions _____________________________*/


/*** VHD_SlotAncGetNbPackets *****************************************************/
/*!VHD_SlotAncGetNbPackets@HANDLE@int@VHD_SAMPLETYPE@int *
   Summary
   Number of ancillary data packets on a line query
   Description
   This function gives the number of ancillary data packets
   found on the specified line number of the provided SDI slot.
   
   The concerned stream must be of processing mode including
   ancillary data handling
   Parameters
   SlotHandle :     Handle of the slot to work on
   LineNumber :     Video line number to get count from (1..N,
                    with N depending on the standard)
   AncStream :      Specifies if working on Y or C ancillary data
                    stream (in SD, only C stream is used)
   pNbAncPackets :  Pointer to caller\-allocated variable
                    receiving the ancillary data packets count
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the AncStream argument value must be
   VHD_SAMPLE_C, since Y and C samples are multiplexed together
   to carry on ancillary data packets
   See Also
   VHD_LockSlotHandle VHD_SlotAncGetPacket                        */
ULONG VIDEOMASTER_HD_API VHD_SlotAncGetNbPackets (HANDLE SlotHandle, int LineNumber, VHD_SAMPLETYPE AncStream, int *pNbAncPackets);


/*** VHD_SlotAncGetPacket ********************************************************/
/*!VHD_SlotAncGetPacket@HANDLE@int@VHD_SAMPLETYPE@int@VHD_ANCPACKET **
   Summary
   Ancillary data packet retrieval
   Description
   This function provides the specified ancillary data packet
   regarding the provided SDI slot.
   
   The concerned stream must be of processing mode including
   ancillary data handling
   Parameters
   SlotHandle :   Handle of the slot to work on
   LineNumber :   Video line number to get packet from (1..N,
                  with N depending on the standard)
   AncStream :    Specifies if working on Y or C ancillary data
                  stream (in SD, only C stream is used)
   PacketIndex :  Index (zero\-based) of the packet to retrieve
   ppAncPacket :  Pointer receiving a pointer to allocated ANC
                  packet structure
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the AncStream argument value must be
   VHD_SAMPLE_C, since Y and C samples are multiplexed together
   to carry on ancillary data packets.
   
   In RX-modify-TX streams, you can freely update the content of
   the packet, but you cannot change its size. To replace this
   packet with a packet of different size, remove this one and
   allocate a new one
   See Also
   VHD_LockSlotHandle VHD_SlotAncGetNbPackets VHD_ANCPACKET      */
ULONG VIDEOMASTER_HD_API VHD_SlotAncGetPacket (HANDLE SlotHandle, int LineNumber, VHD_SAMPLETYPE AncStream, int PacketIndex, VHD_ANCPACKET **ppAncPacket);


/*** VHD_SlotAncAllocatePacket ***************************************************/
/*!VHD_SlotAncAllocatePacket@HANDLE@BYTE@VHD_ANCPACKET **
   Summary
   Ancillary data packet allocation
   Description
   This function allocates memory for a new ancillary data
   packet. The allocated memory is attached to a SDI slot
   
   The concerned stream must be of processing mode including
   ancillary data handling
   Parameters
   SlotHandle :   Handle of the slot to work on
   DataCount :    Number of User Data Words to allocate
   ppAncPacket :  Pointer receiving a pointer to allocated ANC
                  packet structure
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Once a new packet has been allocated and properly filled in,
   it may be inserted on a specific video line using the
   VHD_SlotAncInsertPacket function.
   
   Allocated memory is automatically released by VideoMasterHD
   once the ancillary data slot is unlocked.
   
   User data words of allocated packets are all reset to zero by
   this function.
   
   
   
   Although the allocated packet will contain DataCount user
   data words, the caller may freely set the actual DataCount
   field of the packet structure to a value less or equal to it
   in before subsequent call to VHD_SlotAncInsertPacket
   See Also
   VHD_SlotAncInsertPacket VHD_ANCPACKET                         */
ULONG VIDEOMASTER_HD_API VHD_SlotAncAllocatePacket (HANDLE SlotHandle, BYTE DataCount, VHD_ANCPACKET **ppAncPacket);


/*** VHD_SlotAncInsertPacket *****************************************************/
/*!VHD_SlotAncInsertPacket@HANDLE@int@VHD_SAMPLETYPE@int@VHD_ANCPACKET *
   Summary
   Ancillary data packet insertion
   Description
   This function inserts the given ancillary data packet at the
   specified line within the provided SDI slot.
   
   The concerned stream must be of processing mode including
   ancillary data handling
   Parameters
   SlotHandle :   Handle of the slot to work on
   LineNumber :   Video line number to insert packet on (1..N,
                  with N depending on the standard)
   AncStream :    Specifies if working on Y or C ancillary data
                  stream (in SD, only C stream is used)
   PacketIndex :  Position (zero\-based) to put this packet at
                  (\-1 to insert at last location)
   pAncPacket :   Pointer to ancillary data packet to insert
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the AncStream argument is ignored, since
   Y and C samples are multiplexed together to carry on
   ancillary data packets.
   
   Packet insertion automatically release allocated packet, so
   that the pAncPacket pointer cannot be re-used without any
   call to VHD_SlotAncAllocatePacket.
   
   
   
   When inserting a packet on a line already containing
   ancillary data, the PacketIndex argument let you decide where
   to place the new packet.
   
   It is related to other packets already present in the same
   zone as the packet to insert, that is in HANC space or in
   active part of the line.
   
   
   
   For example, if a line already contains packets A,B and C in
   HANC space, inserting a new HANC packet N at location 1 will
   give the following result : A,N,B,C
   See Also
   VHD_SlotAncAllocatePacket VHD_ANCPACKET                       */
ULONG VIDEOMASTER_HD_API VHD_SlotAncInsertPacket (HANDLE SlotHandle, int LineNumber, VHD_SAMPLETYPE AncStream, int PacketIndex, VHD_ANCPACKET *pAncPacket);


/*** VHD_SlotAncRemovePacket *****************************************************/
/*!VHD_SlotAncRemovePacket@HANDLE@int@VHD_SAMPLETYPE@int@BOOL32
   Summary
   Ancillary data packet removal
   Description
   This function remove the specified ancillary data packet from
   the specified line on the provided SDI slot.
   
   The concerned stream must be of processing mode including
   ancillary data handling
   Parameters
   SlotHandle :   Handle of the slot to work on
   LineNumber :   Video line number to remove packet from (1..N,
                  with N depending on the standard)
   AncStream :    Specifies if working on Y or C ancillary data
                  stream (in SD, only C stream is used)
   PacketIndex :  Position (zero\-based) of the packet to remove
                  (\-1 to remove all packets from line)
   InHANC :       Specifies if PacketIndex is related to HANC
                  zone or to active line zone. If PacketIndex is
                  \-1, then this argument is ignored and the
                  whole ANC line is erased
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the AncStream argument is ignored, since
   Y and C samples are multiplexed together to carry on
   ancillary data packets
   See Also
   VHD_LockSlotHandle VHD_SlotAncGetNbPackets                    */
ULONG VIDEOMASTER_HD_API VHD_SlotAncRemovePacket (HANDLE SlotHandle, int LineNumber, VHD_SAMPLETYPE AncStream, int PacketIndex, BOOL32 InHANC);





/*_ VBI data buffers navigation functions ___________________________________*/


/*** VHD_VbiSetCaptureLines **************************************************/
/*!VHD_VbiSetCaptureLines@HANDLE@VHD_VBILINE
   Summary
   VBI capture lines configuration
   Description
   This function configures the video lines from which VBI data
   must be captured on a SDI reception stream.
   
   The concerned stream must be of processing mode including
   ANC/VBI handling
   
   The function must be called before starting the capture using
   VHD_StartStream. The VBI capture configuration remains active
   until the stream handle is closed using
   VHD_CloseStreamHandle, or until it is re-configured by
   calling this function again.
   Parameters
   StrmHandle :     Handle of the stream to work on
   pCaptureLines :  Array of structure defining the video lines
                    to capture VBI from. To capture less than
                    VHD_MAXNB_VBICAPTURELINE lines, simply reset
                    all other entries to zero
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Please note that no ancillary data packets will be captured
   in active video part of the lines configured for VBI capture.
   
   When working in SD, the CaptureFromY and CaptureFromC fields
   of the pCaptureLines array are meaningless. In this case, Y
   and C samples are always captured and multiplexed together in
   the VBI buffer
   See Also
   VHD_VBILINE VHD_MAXNB_VBICAPTURELINE                          */
ULONG VIDEOMASTER_HD_API VHD_VbiSetCaptureLines (HANDLE StrmHandle, VHD_VBILINE pCaptureLines[VHD_MAXNB_VBICAPTURELINE]);

/*** VHD_VbiSetCaptureLines2 **************************************************/
/*!VHD_VbiSetCaptureLines2@HANDLE@VHD_VBILINE
   Summary
   VBI capture lines configuration on the second coupled stream
   Description
   This function configures the video lines from which VBI data
   must be captured on the second coupled SDI reception stream.
   
   The concerned stream must be of type VHD_ST_COUPLED_RX01 or
   VHD_ST_COUPLED_RX23, and of processing mode including
   ANC/VBI handling.
   
   The function must be called before starting the capture using
   VHD_StartStream. The VBI capture configuration remains active
   until the stream handle is closed using
   VHD_CloseStreamHandle, or until it is re-configured by
   calling this function again.
   Parameters
   StrmHandle :     Handle of the stream to work on
   pCaptureLines :  Array of structure defining the video lines
                    to capture VBI from. To capture less than
                    VHD_MAXNB_VBICAPTURELINE lines, simply reset
                    all other entries to zero
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Please note that no ancillary data packets will be captured
   in active video part of the lines configured for VBI capture.
   
   When working in SD, the CaptureFromY and CaptureFromC fields
   of the pCaptureLines array are meaningless. In this case, Y
   and C samples are always captured and multiplexed together in
   the VBI buffer
   See Also
   VHD_VBILINE VHD_MAXNB_VBICAPTURELINE                          */
ULONG VIDEOMASTER_HD_API VHD_VbiSetCaptureLines2 (HANDLE StrmHandle, VHD_VBILINE pCaptureLines[VHD_MAXNB_VBICAPTURELINE]);

/*** VHD_VbiSetCaptureLinesEx **************************************************/
/*!VHD_VbiSetCaptureLinesEx
   Summary
   VBI capture lines configuration on the specified stream
   Description
   This function configures the video lines from which VBI data
   must be captured on the specified SDI reception stream. If ChannelIndex = -1
   the function configures the video lines on each available SDI reception stream.
   
   The concerned stream must be of processing mode including
   ANC/VBI handling.
   
   The function must be called before starting the capture using
   VHD_StartStream. The VBI capture configuration remains active
   until the stream handle is closed using
   VHD_CloseStreamHandle, or until it is re-configured by
   calling this function again.
   Parameters
   StrmHandle :     Handle of the stream to work on
   pCaptureLines :  Array of structure defining the video lines
                    to capture VBI from. To capture less than
                    VHD_MAXNB_VBICAPTURELINE lines, simply reset
                    all other entries to zero
   ChannelIndex :   Specify the SDI reception stream on which the VBI
                    data must be captured
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Please note that no ancillary data packets will be captured
   in active video part of the lines configured for VBI capture.
   
   When working in SD, the CaptureFromY and CaptureFromC fields
   of the pCaptureLines array are meaningless. In this case, Y
   and C samples are always captured and multiplexed together in
   the VBI buffer
   See Also
   VHD_VBILINE VHD_MAXNB_VBICAPTURELINE                          */
ULONG VIDEOMASTER_HD_API VHD_VbiSetCaptureLinesEx (HANDLE StrmHandle, VHD_VBILINE pCaptureLines[VHD_MAXNB_VBICAPTURELINE], ULONG ChannelIndex);

/*** VHD_SlotVbiGetLine **********************************************************/
/*!VHD_SlotVbiGetLine
   Summary
   VBI lines retrieval
   Description
   This function retrieves the specified VBI line raw content
   from the provided SDI slot.
   
   The concerned stream must be of processing mode including
   ANC/VBI handling, and the requested video line must be
   included in the list passed to VHD_VbiSetCaptureLines
   Parameters
   SlotHandle :    Handle of the slot to work on
   LineNumber :    Video line number to get VBI from (1..N, with
                   N depending on the standard)
   VbiStream :     Specifies if working on Y or C samples stream
                   (in SD, Y and C samples are multiplexed
                   together in C stream)
   ppLineBuffer :  Pointer receiving a pointer to raw VBI line
                   buffer
   pBufferSize :   Pointer receiving the size of the buffer
                   provided to ppLineBuffer
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the VbiStream argument value must be
   VHD_SAMPLE_C, since Y and C samples are always captured and
   multiplexed together in the VBI buffer
   See Also
   VHD_VBILINE VHD_MAXNB_VBICAPTURELINE VHD_VbiSetCaptureLines   */
ULONG VIDEOMASTER_HD_API VHD_SlotVbiGetLine (HANDLE SlotHandle, int LineNumber, VHD_SAMPLETYPE VbiStream, BYTE **ppLineBuffer, ULONG *pBufferSize);

/*** VHD_SlotVbiRemoveLine **********************************************************/
/*!VHD_SlotVbiRemoveLine
   Summary
   VBI lines remove from SDI slot
   Description
   This function remove the specified VBI line
   from the provided SDI slot.
   Parameters
   SlotHandle :    Handle of the slot to work on
   LineNumber :    Video line number to get VBI from (1..N, with
                   N depending on the standard)
   VbiStream :     Specifies if working on Y or C samples stream
                   (in SD, Y and C samples are multiplexed
                   together in C stream)
   
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
    */
ULONG VIDEOMASTER_HD_API VHD_SlotVbiRemoveLine (HANDLE SlotHandle, int LineNumber, VHD_SAMPLETYPE VbiStream);


/*** VHD_SlotVbiInsertLine *******************************************************/
/*!VHD_SlotVbiInsertLine@HANDLE@int@VHD_SAMPLETYPE@BYTE *@ULONG
   Summary
   VBI lines insertion
   Description
   This function inserts the specified VBI line raw content on
   the provided SDI slot.
   
   The concerned stream must be of processing mode including
   ANC/VBI handling
   Parameters
   SlotHandle :   Handle of the slot to work on
   LineNumber :   Video line number to insert VBI on (1..N, with
                  N depending on the standard)
   VbiStream :    Specifies if working on Y or C samples stream
                  (in SD, Y and C samples are multiplexed
                  together in C stream)
   pLineBuffer :  Pointer to raw VBI content to insert to line
   BufferSize :   Size (in bytes) of the pLineBuffer
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Please note that no ancillary data packets will be inserted
   in active video part of the lines configured for VBI
   insertion.
   
   When working in SD, the VbiStream argument is ignored, since
   Y and C samples are always captured and multiplexed together
   in the VBI buffer
   
   
   
   The provided buffer size cannot exceed the active line size,
   depending on the video standard used. If the buffer is
   smaller than line size, then it will be automatically
   completed with idle level samples
   See Also
   VHD_VBILINE VHD_MAXNB_VBICAPTURELINE                          */
ULONG VIDEOMASTER_HD_API VHD_SlotVbiInsertLine (HANDLE SlotHandle, int LineNumber, VHD_SAMPLETYPE VbiStream, BYTE *pLineBuffer, ULONG BufferSize);

#ifdef __cplusplus
}
#endif

#endif

#endif // _VIDEOMASTERHD_SDI_H_
