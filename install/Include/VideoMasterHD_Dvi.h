/*! VideomasterHD_Dvi.h

    Copyright (c) 2009, DELTACAST. All rights reserved.

    THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
    KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
    PURPOSE.

  Project : VideomasterHD

  Package : 

  Company : DELTACAST

  Author  : gt                             Date: 2009/06/26

  Purpose : 

*/


#ifndef _VIDEOMASTERHD_DVI_H_
#define _VIDEOMASTERHD_DVI_H_


/*_ CONSTANTS ________________________________________________________________
//
// This section defines the different constants used by VideomasterHD_Dvi
*/

/*_ VideomasterHD_Dvi DATA TYPE __________________________________________________*/
/*! Data types bit masks. These constants define data type bit masks for the VHD_CORE_SP_MUTED_DATA_MASK stream property */
#define VHD_DVI_DATA_VIDEO        0x00000001  /*! Video data */

#define VHD_DVI_STPROC_DEFAULT      VHD_DVI_STPROC_DISJOINED_VIDEO /*! For compatibility */

/*_ HDMI audio InfoFrame definition __________________________________________________*/
/*! Define VHD_HDMI_AUDIO_INFOFRAME fields */
/*! ChannelCount */
#define VHD_HDMI_AUDIO_INFOFRAME_CHANNEL_COUNT_REF_STREAM_HEADER  0
#define VHD_HDMI_AUDIO_INFOFRAME_CHANNEL_COUNT_2                  1
#define VHD_HDMI_AUDIO_INFOFRAME_CHANNEL_COUNT_3                  2
#define VHD_HDMI_AUDIO_INFOFRAME_CHANNEL_COUNT_4                  3
#define VHD_HDMI_AUDIO_INFOFRAME_CHANNEL_COUNT_5                  4
#define VHD_HDMI_AUDIO_INFOFRAME_CHANNEL_COUNT_6                  5
#define VHD_HDMI_AUDIO_INFOFRAME_CHANNEL_COUNT_7                  6
#define VHD_HDMI_AUDIO_INFOFRAME_CHANNEL_COUNT_8                  7

/*! CodingType */
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_REF_STREAM_HEADER    0
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_PCM                  1
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_AC3                  2
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_MPEG1                3
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_MP3                  4
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_MPEG2                5
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_AACLC                6
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_DTS                  7
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_ATRAC                8
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_DSD                  9
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_EAC3                 10
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_DTSHD                11
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_MLP                  12
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_DST                  13
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_WMAPRO               14
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_REF_AUDIO_CXT        15

/*! SampleSize */
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLE_SIZE_REF_STREAM_HEADER   0
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLE_SIZE_16_BITS             1
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLE_SIZE_20_BITS             2
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLE_SIZE_24_BITS             3

/*! SamplingFrequency */
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLING_FREQ_REF_STREAM_HEADER   0
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLING_FREQ_32000HZ             1
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLING_FREQ_44100HZ             2
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLING_FREQ_48000HZ             3
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLING_FREQ_88200HZ             4
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLING_FREQ_96000HZ             5
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLING_FREQ_176400HZ            6
#define VHD_HDMI_AUDIO_INFOFRAME_SAMPLING_FREQ_192000HZ            7

/*! CodingTypeExt */
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_EXT_REF_CT        0
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_EXT_HEAAC         1
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_EXT_HEAACV2       2
#define VHD_HDMI_AUDIO_INFOFRAME_CODING_TYPE_EXT_MPEGSURROUND  3

/*! SpeakerPlacement */
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR                      0
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE                  1
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC                   2
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC               3
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_RC                   4
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_RC               5
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RC                6
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RC            7
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_RL_RR                8
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_RL_RR            9
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR             10
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR         11
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_RL_RR_RC             12
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_RL_RR_RC         13
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_RC          14
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_RC      15
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_RL_RR_RLC_RRC        16
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_RL_RR_RLC_RRC    17
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_RLC_RRC     18
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_RLC_RRC 19
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FLC_FRC              20
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FLC_FRC          21
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_FLC_FRC           22
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_FLC_FRC       23
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_RC_FLC_FRC           24
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_RC_FLC_FRC       25
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RC_FLC_FRC        26
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RC_FLC_FRC    27
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_RL_RR_FLC_FRC        28
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_RL_RR_FLC_FRC    29
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_FLC_FRC     30
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_FLC_FRC 31
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_FCH         32
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_FCH     33
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_TC          34
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_TC      35
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_RL_RR_FLH_FRH        36
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_RL_RR_FLH_FRH    37
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_RL_RR_FLW_FRW        38
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_RL_RR_FLW_FRW    39
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_RC_TC       40
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_RC_TC   41
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_RC_FCH      42
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_RC_FCH  43
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_FCH_TC      44
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_FCH_TC  45
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_FLH_FRH     46
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_FLH_FRH 47
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_FC_RL_RR_FLW_FRW     48
#define VHD_HDMI_AUDIO_INFOFRAME_SPEAKER_PLACEMENT_FL_FR_LFE_FC_RL_RR_FLW_FRW 49

/*! LFEPlaybackLevel */
#define VHD_HDMI_AUDIO_INFOFRAME_LFE_PLAYBACK_LEVEL_UNDEFINED  0
#define VHD_HDMI_AUDIO_INFOFRAME_LFE_PLAYBACK_LEVEL_0DB        1
#define VHD_HDMI_AUDIO_INFOFRAME_LFE_PLAYBACK_LEVEL_10DB       2

/*! DownMixedInhibit */
#define VHD_HDMI_AUDIO_INFOFRAME_DOWNMIX_PERMITTED    0
#define VHD_HDMI_AUDIO_INFOFRAME_DOWNMIX_PROHIBITED   1

/*_ HDMI audio AES channel status definition __________________________________________________*/
/*! Define _VHD_HDMI_AUDIO_AES_STS fields */
/*! Professional */
#define VHD_HDMI_AUDIO_AES_STS_CONSUMER      0
#define VHD_HDMI_AUDIO_AES_STS_PROFESSIONAL  1

/*! LinearPCM */
#define VHD_HDMI_AUDIO_AES_SAMPLE_STS_LINEAR_PCM_SAMPLE 0
#define VHD_HDMI_AUDIO_AES_SAMPLE_STS_OTHER_SAMPLE      1

/*! Copyright */
#define VHD_HDMI_AUDIO_AES_STS_COPYRIGHT_ASSERTED  0
#define VHD_HDMI_AUDIO_AES_STS_COPYRIGHT_NONE      1

/*! Emphasis */
#define VHD_HDMI_AUDIO_AES_STS_EMPHASIS_NONE    0
#define VHD_HDMI_AUDIO_AES_STS_EMPHASIS_50_15   1

/*! SamplingFrequency */
#define VHD_HDMI_AUDIO_AES_STS_SAMPLING_FREQ_44100HZ  0
#define VHD_HDMI_AUDIO_AES_STS_SAMPLING_FREQ_48000HZ  2
#define VHD_HDMI_AUDIO_AES_STS_SAMPLING_FREQ_32000HZ  3
#define VHD_HDMI_AUDIO_AES_STS_SAMPLING_FREQ_88200HZ  8
#define VHD_HDMI_AUDIO_AES_STS_SAMPLING_FREQ_768000HZ 9
#define VHD_HDMI_AUDIO_AES_STS_SAMPLING_FREQ_96000HZ  10
#define VHD_HDMI_AUDIO_AES_STS_SAMPLING_FREQ_176000HZ 12
#define VHD_HDMI_AUDIO_AES_STS_SAMPLING_FREQ_192000HZ 14

/*! ClockAccuracy */
#define VHD_HDMI_AUDIO_AES_STS_CLOCK_ACCURACY_LEVEL2_1000PPM   0
#define VHD_HDMI_AUDIO_AES_STS_CLOCK_ACCURACY_LEVEL1_50PPM     1
#define VHD_HDMI_AUDIO_AES_STS_CLOCK_ACCURACY_LEVEL3_VARIABLE  2

/*! MaxWordLengthSize */
#define VHD_HDMI_AUDIO_AES_STS_MAX_WORD_LENGTH_20BITS 0
#define VHD_HDMI_AUDIO_AES_STS_MAX_WORD_LENGTH_24BITS 1

/*! SampleWordLength */
#define VHD_HDMI_AUDIO_AES_STS_SAMPLE_WORD_LENGTH_UNDEFINED       0
#define VHD_HDMI_AUDIO_AES_STS_SAMPLE_WORD_LENGTH_20_OR_16_BITS   1
#define VHD_HDMI_AUDIO_AES_STS_SAMPLE_WORD_LENGTH_22_OR_18_BITS   2
#define VHD_HDMI_AUDIO_AES_STS_SAMPLE_WORD_LENGTH_23_OR_19_BITS   4
#define VHD_HDMI_AUDIO_AES_STS_SAMPLE_WORD_LENGTH_24_OR_20_BITS   5
#define VHD_HDMI_AUDIO_AES_STS_SAMPLE_WORD_LENGTH_21_BITS         6

/*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideomasterHD_Dvi
*/


/*_ VHD_DVI_STREAMPROPERTY______________________________________________*/
/*!
   Summary
   VideoMasterHD DVI streams properties
   Description
   The VHD_DVI_STREAMPROPERTY enumeration lists all the
   available DVI stream properties.
   
   These values are used as indexes for VHD_GetStreamProperty
   and VHD_SetStreamProperty functions calls.
   See Also
   VHD_GetStreamProperty VHD_SetStreamProperty                */
typedef enum _VHD_DVI_STREAMPROPERTY
{
   VHD_DVI_SP_MODE = ENUMBASE_DVI,  /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_MODE
                                       DVI mode (see VHD_DVI_MODE)
                                       ,default is VHD_DVI_MODE_DVI_D                                 */
   VHD_DVI_SP_ACTIVE_WIDTH,         /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_ACTIVE_WIDTH
                                       Active picture width, in pixels. Must be a multiple of 8
                                       (default is 0)                                                 */
   VHD_DVI_SP_ACTIVE_HEIGHT,        /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_ACTIVE_HEIGHT
                                       Active picture height, in lines (default is 0)                 */
   VHD_DVI_SP_INTERLACED,           /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_INTERLACED
                                       TRUE if interlaced, FALSE if progressive
                                       (default is FALSE)                                             */
   VHD_DVI_SP_REFRESH_RATE,            /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_REFRESH_RATE
                                          Refresh rate, in frame per second (default is 0)            */
   VHD_DVI_SP_PIXEL_CLOCK,             /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_PIXEL_CLOCK
                                          Pixel clock (kHz), default is 0                             */
   VHD_DVI_SP_TOTAL_WIDTH,             /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_TOTAL_WIDTH
                                          Total picture width, in pixels (default is 0)               */
   VHD_DVI_SP_TOTAL_HEIGHT,            /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_TOTAL_HEIGHT
                                          Total picture height, in lines (default is 0)               */
   VHD_DVI_SP_H_SYNC,                  /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_H_SYNC
                                          Horizontal sync pulse size, in pixels (default is 0)        */
   VHD_DVI_SP_H_FRONT_PORCH,           /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_H_FRONT_PORCH
                                          Horizontal front porch size, in pixels (default is 0)       */
   VHD_DVI_SP_V_SYNC,                  /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_V_SYNC
                                          Vertical sync pulse size, in lines (default is 0)           */
   VHD_DVI_SP_V_FRONT_PORCH,           /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_V_FRONT_PORCH
                                          Vertical front porch size, in lines (default is 0)          */
   VHD_DVI_SP_H_SHIFT,                 /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_H_SHIFT
                                          Picture horizontal shift, in pixels. Must be comprised between -512 and 511.
                                          (default is 0)                                              */
   VHD_DVI_SP_V_SHIFT,                 /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_V_SHIFT
                                          Picture vertical shift, in lines. Must be comprised between -8 and 7.
                                          (default is 0)                                              */
   VHD_DVI_SP_MEASURED_H_POL,          /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_MEASURED_H_POL
                                          Horizontal polarity. '1' = positive, '0' = negative         */
   VHD_DVI_SP_MEASURED_V_POL,          /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_MEASURED_V_POL
                                          Vertical polarity. '1' = positive, '0' = negative           */
   VHD_DVI_SP_MEASURED_LINE_DUR,       /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_MEASURED_LINE_DUR
                                          Duration of a line in ns                                    */
   VHD_DVI_SP_MEASURED_TOTAL_HEIGHT,   /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_MEASURED_TOTAL_HEIGHT
                                          Total picture height, in lines                              */
   VHD_DVI_SP_MEASURED_V_SYNC,         /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_MEASURED_V_SYNC
                                          Vertical sync pulse size, in lines                          */

   VHD_DVI_SP_DUAL_LINK,               /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_DUAL_LINK
                                          TRUE if dual link, FALSE if single link (default is FALSE)  */
   VHD_DVI_SP_DISABLE_EDID_AUTO_LOAD,  /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_DISABLE_EDID_AUTO_LOAD
                                          TRUE to disable automatic EDID loading while setting DVI mode.
                                          (default is FALSE)                                          */
   VHD_DVI_SP_INPUT_CS,                /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_INPUT_CS
                                          HDMI input color space (see VHD_HDMI_CS) */
   VHD_DVI_SP_3D_MODE,                 /*!_VHD_DVI_STREAMPROPERTY::VHD_DVI_SP_3D_MODE
                                          HDMI 3D mode */
   NB_VHD_DVI_STREAMPROPERTIES
} VHD_DVI_STREAMPROPERTY;


/*_ VHD_DVI_STREAMPROCMODE ________________________________________*/
/*!
Summary
VideoMasterHD DVI streams processing modes

Description
The VHD_DVI_STREAMPROCMODE enumeration lists all the DVI stream 
processing modes.

These values are used during VHD_OpenStreamHandle function calls.

See Also
VHD_OpenStreamHandle
*/
typedef enum _VHD_DVI_STREAMPROCMODE
{
   VHD_DVI_STPROC_DISJOINED_VIDEO=ENUMBASE_DVI,         /*! Single Video processing (default) */
   VHD_DVI_STPROC_DISJOINED_AUDIO,                      /*! Single audio processing */
   VHD_DVI_STPROC_JOINED,                               /*! Coupled processing (video and audio) */
   VHD_DVI_STPROC_DISJOINED_THUMBNAIL,                  /*! Single thumbnail processing */
   NB_VHD_DVI_STREAMPROCMODE
} VHD_DVI_STREAMPROCMODE;

/*_ VHD_DVI_BUFFERTYPE ________________________________________*/
/*!
   Summary
   VideoMasterHD DVI buffer type
   Description
   The VHD_DVI_BUFFERTYPE enumeration lists all the DVI buffer
   types.
   
   These values are used during VHD_GetSlotBuffer function
   calls.
   See Also
   VHD_GetSlotBuffer
   Remarks
   Currently, only one buffer type is available                */

typedef enum _VHD_DVI_BUFFERTYPE
{
   VHD_DVI_BT_VIDEO = 0,             /*!_VHD_DVI_BUFFERTYPE::VHD_DVI_BT_VIDEO
                                        DVI/HDMI frames buffer type */
   VHD_DVI_BT_AUDIO,                 /*!_VHD_DVI_BUFFERTYPE::VHD_DVI_BT_AUDIO
                                        Audio frames buffer type */
   VHD_DVI_BT_THUMBNAIL,             /*!_VHD_DVI_BUFFERTYPE::VHD_DVI_BT_THUMBNAIL
                                        Thumbnail frames buffer type */
   NB_VHD_DVI_BUFFERTYPE
} VHD_DVI_BUFFERTYPE;


/*_ VHD_DVI_MODE ____________________________________________________*/
/*!
   Summary
   DELTA-dvi DVI modes
   Description
   The VHD_DVI_MODE enumeration lists all supported DVI modes
   used in VHD_DVI_SP_MODE stream property.
   See Also
   <link VHD_DVI_STREAMPROPERTY, VHD_DVI_SP_MODE>             */
typedef enum _VHD_DVI_MODE
{
   VHD_DVI_MODE_DVI_D=0,                  /*! DVI-D mode */
   VHD_DVI_MODE_DVI_A,                    /*! DVI-A mode */
   VHD_DVI_MODE_HDMI,                     /*! HDMI mode */
   VHD_DVI_MODE_ANALOG_COMPONENT_VIDEO,   /*! Analog component video mode */
   NB_VHD_DVI_MODES
}VHD_DVI_MODE;


/*_ VHD_DVI_A_STANDARD ____________________________________________________*/
/*!
Summary
DELTA-dvi DVI-A standards

Description
The VHD_DVI_A_STANDARD enumeration lists all supported DVI-A
standard used by the VHD_PresetDviAStreamProperties function.

See Also
VHD_PresetDviAStreamProperties             
*/
typedef enum _VHD_DVI_A_STANDARD
{
   VHD_DVIA_STD_CVT=0,              /*! CVT standard */
   VHD_DVIA_STD_CVTRB,              /*! CVT-RB standard */
   VHD_DVIA_STD_DMT,                /*! DMT standard */
   VHD_DVIA_STD_SMPTE,              /*!_VHD_DVI_A_STANDARD::VHD_DVIA_STD_SMPTE
                                       SMPTE standard (CEA-861) */
   VHD_DVIA_STD_GTF,                /*! GTF standard */
   NB_VHD_DVI_A_STANDARD
} VHD_DVI_A_STANDARD;


/*_ VHD_DVI_EEDID_PRESET ____________________________________________________*/
/*!
Summary
DELTA-dvi E-EDID presets

Description
The VHD_DVI_EEDID_PRESET enumeration lists all predefined E-EDID
used by the VHD_PresetEEDID function.

See Also
VHD_PresetEEDID             
*/
typedef enum _VHD_DVI_EEDID_PRESET
{
   VHD_EEDID_EMPTY=0,               /*! Empty E-EDID. The host should force its output regardless of the DELTA-dvi E-EDID. Size=0 byte. */
   VHD_EEDID_DVIA,                  /*! DVI-A E-EDID (EDID version 1, revision 3). Size=128 bytes. */
   VHD_EEDID_DVID,                  /*! DVI-D E-EDID (EDID version 1, revision 3). Size=128 bytes. */
   VHD_EEDID_HDMI,                  /*! HDMI E-EDID (EDID version 1, revision 3). Size=256 bytes. */
   VHD_EEDID_DVID_DUAL,             /*! DVI-D E-EDID with dual-link formats (EDID version 1, revision3). Size=256 bytes. */
   VHD_EEDID_HDMI_H4K,              /*! HDMI 4K E-EDID (EDID version 1, revision 4). Size=256 bytes. */
   VHD_EEDID_DVID_H4K,              /*! DVI-D 4K E-EDID (EDID version 1). Size=256 bytes. */
   NB_VHD_EEDID
} VHD_DVI_EEDID_PRESET;


/*_ VHD_HDMI_CS ____________________________________________________*/
/*!
Summary
DELTA-h4k color space

Description
The VHD_HDMI_CS enumeration lists all supported HDMI color space.            
*/
typedef enum _VHD_HDMI_CS
{
   VHD_HDMI_CS_RGB_FULL=0,         /*! RGB full color space */
   VHD_HDMI_CS_RGB_LIMITED,      /*! RGB limited color space */
   VHD_HDMI_CS_YUV601,           /*! YUV 601 (SD) color space */
   VHD_HDMI_CS_YUV709,           /*! YUV 709 (HD) color space */
   VHD_HDMI_CS_XVYCC_601,        /*! CVYCC 601 (SD) color space */
   VHD_HDMI_CS_XVYCC_709,        /*! CVYCC 709 (HD) color space */
   VHD_HDMI_CS_YUV_601_FULL,     /*! YUV 601 (SD) full color space */
   VHD_HDMI_CS_YUV_709_FULL,     /*! YUV 709 (HD) full color space */
   VHD_HDMI_CS_SYCC_601,         /*! SYCC 601 color space */
   VHD_HDMI_CS_ADOBE_YCC_601,    /*! Adobe YCC 601 color space */
   VHD_HDMI_CS_ADOBE_RGB,        /*! Adobe RGB color space */
   NB_VHD_HDMI_CS
} VHD_HDMI_CS;


/*_ VHD_HDMI_VIDEOSTANDARD ________________________________________________*/
/*!
   Summary
   VideomasterHD supported HDMI video standards
   Description
   The VHD_HDMI_VIDEOSTANDARD enumeration lists all the supported HDMI
   video standards
*/
typedef enum _VHD_HDMI_VIDEOSTANDARD
{
   VHD_HDMI_VIDEOSTD_640x480p_60Hz=0,       /*! 640x480p @ 60Hz VIC=1*/
   VHD_HDMI_VIDEOSTD_720x480p_60Hz,       /*! 720x480p @ 60Hz VIC=3*/
   VHD_HDMI_VIDEOSTD_1280x720p_60Hz,      /*! 1280x720p @ 60 VIC=4*/
   VHD_HDMI_VIDEOSTD_1920x1080i_30Hz,     /*! 1920x1080i @ 30Hz VIC=5*/
   VHD_HDMI_VIDEOSTD_720x480i_30Hz,       /*! 720x480i @ 30Hz VIC=7*/
   VHD_HDMI_VIDEOSTD_1920x1080p_60Hz,     /*! 1920x1080p60Hz VIC=16*/
   VHD_HDMI_VIDEOSTD_720x576p_50Hz,       /*! 720x576p @ 50Hz VIC=18*/
   VHD_HDMI_VIDEOSTD_1280x720p_50Hz,      /*! 280x720p @ 50Hz VIC=19*/
   VHD_HDMI_VIDEOSTD_1920x1080i_25Hz,     /*! 1920x1080i @ 25Hz VIC=20*/
   VHD_HDMI_VIDEOSTD_720x576i_25Hz,       /*! 720x576i @ 25Hz VIC=22*/
   VHD_HDMI_VIDEOSTD_1920x1080p_50Hz,     /*! 1920x1080p @ 50Hz VIC=31*/
   VHD_HDMI_VIDEOSTD_1920x1080p_24Hz,     /*! 1920x1080p @ 24Hz VIC=32*/
   VHD_HDMI_VIDEOSTD_1920x1080p_25Hz,     /*! 1920x1080p @ 25Hz VIC=33*/
   VHD_HDMI_VIDEOSTD_1920x1080p_30Hz,     /*! 1920x1080p @ 30Hz VIC=34*/
   VHD_HDMI_VIDEOSTD_1920x1080i_50Hz,     /*! 1920x1080i @ 50Hz VIC=40*/
   VHD_HDMI_VIDEOSTD_1280x720p_100Hz,     /*! 1280x720p @ 100Hz VIC=41*/
   VHD_HDMI_VIDEOSTD_720x576p_100Hz,      /*! 720x576p @ 100Hz VIC=43*/
   VHD_HDMI_VIDEOSTD_720x576i_50Hz,       /*! 720x576i @ 50Hz VIC=45*/
   VHD_HDMI_VIDEOSTD_1920x1080i_60Hz,     /*! 1920x1080i @ 60Hz VIC=46*/
   VHD_HDMI_VIDEOSTD_1280x720p_120Hz,     /*! 1280x720p @ 120Hz VIC=47*/
   VHD_HDMI_VIDEOSTD_720x480p_120Hz,      /*! 720x480p @ 120Hz VIC=49*/
   VHD_HDMI_VIDEOSTD_720x480i_60Hz,       /*! 720x480i @ 60Hz VIC=51*/
   VHD_HDMI_VIDEOSTD_720x576p_200Hz,      /*! 720x576p @ 200Hz VIC=53*/
   VHD_HDMI_VIDEOSTD_720x576i_100Hz,      /*! 720x576i @ 100Hz VIC=55*/
   VHD_HDMI_VIDEOSTD_720x480p_240Hz,      /*! 720x480p @ 240Hz VIC=57*/
   VHD_HDMI_VIDEOSTD_720x480i_120Hz,      /*! 720x480i @ 120Hz VIC=59*/
   VHD_HDMI_VIDEOSTD_1280x720p_24Hz,      /*! 1280x720p @ 24Hz VIC=60*/
   VHD_HDMI_VIDEOSTD_1280x720p_25Hz,      /*! 1280x720p @ 25Hz VIC=61*/
   VHD_HDMI_VIDEOSTD_1280x720p_30Hz,      /*! 1280x720p @ 30Hz VIC=62*/
   VHD_HDMI_VIDEOSTD_1920x1080p_120Hz,    /*! 1920x1080p @ 120Hz VIC=63*/
   VHD_HDMI_VIDEOSTD_1920x1080p_100Hz,    /*! 1920x1080p @ 100Hz VIC=64*/
   VHD_HDMI_VIDEOSTD_3840x2160p_30Hz,     /*! 3840x2160p @ 30Hz HDMI_VIC=1*/
   VHD_HDMI_VIDEOSTD_3840x2160p_25Hz,     /*! 3840x2160p @ 25Hz HDMI_VIC=2*/
   VHD_HDMI_VIDEOSTD_3840x2160p_24Hz,     /*! 3840x2160p @ 24Hz HDMI_VIC=3*/
   VHD_HDMI_VIDEOSTD_4096x2160p_24Hz,     /*! 4096x2160p @ 24Hz HDMI_VIC=4*/
   NB_VHD_HDMI_VIDEOSTD
} VHD_HDMI_VIDEOSTANDARD;

/*_ VHD_HDMI_3D_MODE ________________________________________________*/
/*!
   Summary
   VideomasterHD supported HDMI 3D modes
   Description
   The VHD_HDMI_3D_MODE enumeration lists all the supported HDMI
   3D modes
*/
typedef enum _VHD_HDMI_3D_MODE
{
   VHD_HDMI_3D_MODE_NONE,           /*! 2D mode */
   VHD_HDMI_3D_MODE_FRAME,          /*! 3D frame mode */
   VHD_HDMI_3D_MODE_TOP_BOTTOM,     /*! 3D top-bottom mode */
   VHD_HDMI_3D_MODE_SIDE_BY_SIDE,   /*! 3D side-by-side mode */
   NB_VHD_HDMI_3D_MODE
} VHD_HDMI_3D_MODE;

/*_ VHD_HDMI_AUDIO_TYPE ________________________________________________*/
/*!
   Summary
   VideomasterHD supported HDMI audio types
   Description
   The VHD_HDMI_AUDIO_TYPE enumeration lists all the supported HDMI
   audio types
*/
typedef enum _VHD_HDMI_AUDIO_TYPE
{
   VHD_HDMI_AUDIO_TYPE_NONE,           /*! No audio */
   VHD_HDMI_AUDIO_TYPE_AUDIO_SAMPLES,  /*! Audio samples packets */
   VHD_HDMI_AUDIO_TYPE_HBR_PACKETS,    /*! High-Bitrate packets */
   VHD_HDMI_AUDIO_TYPE_DSD_PACKETS,    /*! DSD packets */
   VHD_HDMI_AUDIO_TYPE_DST_PACKETS,    /*! DST packets */
   NB_VHD_HDMI_AUDIO_TYPE
} VHD_HDMI_AUDIO_TYPE;


/*_ STRUCTURES _______________________________________________________________
//
// This section defines the different structures used by VideomasterHD_Dvi
*/

#pragma pack (push,1)
/*_ VHD_HDMI_AUDIO_INFOFRAME ____________________________________________________*/
/*!
   Summary
   HDMI audio InfoFrame
   Description
   This structure contains the received audio
   InfoFrame block.  */
typedef union _VHD_HDMI_AUDIO_INFOFRAME
{
   struct
   {
      //Byte 0
      UBYTE ChannelCount   : 3;     /*!*/
      UBYTE Reserved1   : 1;        /*!*/
      UBYTE CodingType  : 4;        /*!*/

      //Byte 1
      UBYTE SampleSize        : 2;  /*!*/
      UBYTE SamplingFrequency : 3;  /*!*/
      UBYTE Reserved2         : 3;  /*!*/

      //Byte 2
      UBYTE CodingTypeExt  : 5;     /*!*/
      UBYTE Reserved3      : 3;     /*!*/

      //Byte 3
      UBYTE SpeakerPlacement  : 8;  /*!*/

      //Byte 4
      UBYTE LFEPlaybackLevel  : 2;  /*!*/
      UBYTE Reserved4         : 1;  /*!*/
      UBYTE LevelShiftValue   : 4;  /*!*/
      UBYTE DownMixedInhibit  : 1;  /*!*/

      //Byte 5
      UBYTE Reserved5   : 8;        /*!*/

      //Byte 6
      UBYTE Reserved6   : 8;        /*!*/

      //Byte 7
      UBYTE Reserved7   : 8;        /*!*/

      //Byte 8
      UBYTE Reserved8   : 8;        /*!*/

      //Byte 9
      UBYTE Reserved9   : 8;        /*!*/

   };
   UBYTE pData[10];     /*!*/
} VHD_HDMI_AUDIO_INFOFRAME;

/*_ VHD_HDMI_AUDIO_AES_STS ____________________________________________________*/
/*!
   Summary
   HDMI audio AES channel status
   Description
   This structure contains the received audio
   AES channel status block.  */
typedef union _VHD_HDMI_AUDIO_AES_STS
{
   struct  
   {
      //Byte 0
      UBYTE Professional   : 1;     /*!*/
      UBYTE LinearPCM      : 1;     /*!*/
      UBYTE Copyright      : 1;     /*!*/
      UBYTE Emphasis       : 3;     /*!*/
      UBYTE Reserved1      : 2;     /*!*/

      //Byte 1
      UBYTE CategoryCode   : 8;     /*!*/

      //Byte 2
      UBYTE SourceNb    : 4;        /*!*/
      UBYTE ChannelNb   : 4;        /*!*/

      //Byte 3
      UBYTE SamplingFrequency : 4;  /*!*/
      UBYTE ClockAccuracy     : 2;  /*!*/
      UBYTE Reserved2         : 2;  /*!*/

      //Byte 4
      UBYTE MaxWordLengthSize : 1;  /*!*/
      UBYTE SampleWordLength  : 3;  /*!*/
      UBYTE Reserved3         : 4;  /*!*/

      //BYTE 5
      UBYTE Reserved4 : 8;          /*!*/

      //BYTE 6
      UBYTE Reserved5 : 8;          /*!*/

      //BYTE 7
      UBYTE Reserved6 : 8;          /*!*/

   };
   UBYTE pData[8];   /*!*/
} VHD_HDMI_AUDIO_AES_STS;

#pragma pack (pop)

#ifndef EXCLUDE_API_FUNCTIONS

#ifdef __cplusplus
extern "C" {
#endif


/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideomasterHD_Dvi
*/



/*** VHD_PresetDviAStreamProperties ******************************************/
/*!VHD_PresetDviAStreamProperties@HANDLE@VHD_DVI_A_STANDARD@ULONG@ULONG@ULONG@BOOL32
   Summary
   DELTA-dvi DVI-A stream properties preset
   Description
   This function presets DELTA-dvi DVI-A reception stream
   properties according to the specified video timing standard
   and main characteristics.
   Parameters
   StrmHandle :    Handle of the stream to operate on
   DviAStd :       Analog graphic standard (see
                   VHD_DVI_A_STANDARD)
   ActiveWidth :   Active width. Must be a multiple of 8.
   ActiveHeight :  Active height.
   RefreshRate :   Refresh rate in Hz.
   Interlaced :    TRUE if interlaced. FALSE if progressive.
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration).
   
   The function returns VHDERR_NOTFOUND if the specified format
   doesn't exists in the specified DVI-A standard.
   See Also
   VHD_DVI_STREAMPROPERTY VHD_DVI_A_STANDARD VHD_SetStreamProperty
   VHD_GetStreamProperty                                        */
ULONG VIDEOMASTER_HD_API VHD_PresetDviAStreamProperties (HANDLE StrmHandle, VHD_DVI_A_STANDARD DviAStd,  ULONG ActiveWidth, ULONG ActiveHeight, ULONG RefreshRate, BOOL32 Interlaced);


/*** VHD_PresetEEDID ****************************************************/
/*!
   Summary
   DELTA-dvi E-EDID information preset
   Description
   This function fills in a caller-allocated buffer with a
   preset E-EDID.
   Parameters
   Preset :           E\-EDID preset (see VHD_DVI_EEDID_PRESET)
   pEEDIDBuffer :     Pointer to a caller\-allocated buffer
                      receiving the preset E\-EDID
   EEDIDBufferSize :  E\-EDID buffer size. This size should be 0,
                      128 or 256 bytes depending on the E\-EDID
                      preset
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   The filled in buffer may be used with VHD_LoadEEDID function
   to actually load the E-EDID in the DVI PROM of the selected
   channel.
   See Also
   VHD_DVI_EEDID_PRESET VHD_LoadEEDID
*/
ULONG VIDEOMASTER_HD_API VHD_PresetEEDID (VHD_DVI_EEDID_PRESET Preset, BYTE *pEEDIDBuffer, ULONG EEDIDBufferSize);


/*** VHD_LoadEEDID ****************************************************/
/*!VHD_LoadEEDID@HANDLE@BYTE *@ULONG
   Summary
   DELTA-dvi E-EDID information reload
   Description
   This function reloads the DELTA-dvi E-EDID information with
   the provided buffer
   Parameters
   StrmHandle :       Handle of the stream to load E\-EDID
   pEEDIDBuffer :     Pointer to the buffer containing the E\-EDID
                      to load in the DVI PROM
   EEDIDBufferSize :  E\-EDID buffer size. This size must be 0,
                      128 or 256 bytes
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   This function loads the DVI PROM of the selected channel with
   the given E-EDID buffer.
   
   The user should set the VHD_DVI_SP_DISABLE_EDID_AUTO_LOAD to
   TRUE when using this function to disable default E-EDID
   loading on DVI mode setting.
   
   The VHD_PresetEEDID function can be used to preset the E-EDID
   buffer.
   See Also
   VHD_PresetEEDID                                                 */
ULONG VIDEOMASTER_HD_API VHD_LoadEEDID (HANDLE StrmHandle, BYTE *pEEDIDBuffer, ULONG EEDIDBufferSize);


/*** VHD_ReadEEDID ****************************************************/
/*!VHD_ReadEEDID@HANDLE@ULONG@BYTE *@ULONG *
   Summary
   Reads E-EDID information (DELTA Dvi V2) or reads monitor E-EDID 
   through HDMI output (DELTA 3G and DELTA-hd-e-04)
   Description
   On DELTA-dvi, this function reads the board E-EDID information and
   writes it to the provided buffer. On DELTA-3G and DELTA-hd-e-04,
   this function reads the monitor E-EDID information through HDMI
   output and writes it to the provided buffer.
   Parameters
   BrdHandle :         Handle of the board to load E\-EDID.
   ChannelIndex :      Index of the channel to load E\-EDID.
   pEEDIDBuffer :      Pointer to the user's allocated buffer
                       that will be filled in by the E\-EDID
                       (\>=256 bytes).
   pEEDIDBufferSize :  Pointer to the user's allocated buffer
                       size (\>= 256).
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration).
   Remarks
   Only available with DELTA Dvi V2 board (DELTA-dvi remark).
   
   This function reads the E-EDID DVI PROM of the selected
   channel (DELTA-dvi remark).
   
   pEEDIDBufferSize must specify the size of the user's
   allocated buffer. It will return the number of read byte (DELTA-dvi remark).

   pEEDIDBufferSize must be 256 (DELTA-3G and DELTA-hd-e-04 remark).
   
   PLease remind that the setting of DVI mode reloads a default
   E-EDID information if the VHD_DVI_SP_DISABLE_EDID_AUTO_LOAD
   property is not set (DELTA-dvi remark).
   See Also
   VHD_PresetEEDID, VHD_LoadEEDID,
   VHD_DVI_SP_DISABLE_EDID_AUTO_LOAD                            */
ULONG VIDEOMASTER_HD_API VHD_ReadEEDID(HANDLE BrdHandle,ULONG ChannelIndex,BYTE *pEEDIDBuffer, ULONG *pEEDIDBufferSize);

/*** VHD_DetectDviAFormat ****************************************************/
/*!VHD_DetectDviAFormat@HANDLE@VHD_DVI_A_STANDARD *@ULONG *@ULONG *@ULONG *@BOOL32 *
   Summary
   Detect DVI-A format
   Description
   This function auto detect the DVI-A standard, the active
   width and height, the refresh rate and if it's interlaced.
   Parameters
   StrmHandle :     Handle of the stream to detect.
   pDviAStd :       Pointer to caller\-allocated variable to
                    return DVI\-A Standard.
   pActiveWidth :   Pointer to caller\-allocated variable to
                    return Active Width.
   pActiveHeight :  Pointer to caller\-allocated variable to
                    return Active Height.
   pRefreshRate :   Pointer to caller\-allocated variable to
                    return Refresh Rate.
   pInterlaced :    Pointer to caller\-allocated variable to
                    return Interlaced status.
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration).
   Remarks
   This function detect the DVI-A format. The DVI-A standard is
   deduced from incoming signal timings.
   
   Call this function to auto detect parameters that will be
   used in the VHD_PresetDviAStreamProperties function.
   See Also
   VHD_PresetDviAStreamProperties                               */
ULONG VIDEOMASTER_HD_API VHD_DetectDviAFormat(HANDLE StrmHandle,VHD_DVI_A_STANDARD *pDviAStd,ULONG *pActiveWidth,ULONG *pActiveHeight,ULONG *pRefreshRate,BOOL32 *pInterlaced);


/*** VHD_PresetHDMIStreamProperties ******************************************/
/*!VHD_PresetHDMIStreamProperties
   Summary
   DELTA-h4k HDMI stream properties preset
   Description
   This function presets DELTA-h4k HDMI reception stream
   properties according to the specified video timing standard
   and main characteristics.
   Parameters
   SlotHandle :    Handle of the slot to operate on
   HDMIVideoStd :  HDMI video standard (see
                   VHD_HDMI_VIDEOSTANDARD)
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration).
   
   See Also
   VHD_DVI_STREAMPROPERTY VHD_HDMI_VIDEOSTANDARD VHD_SetStreamProperty
   VHD_GetStreamProperty                                        */
ULONG VIDEOMASTER_HD_API VHD_PresetHDMIStreamProperties (HANDLE StrmHandle, VHD_HDMI_VIDEOSTANDARD HDMIVideoStd);


/*** VHD_GetSlotHDMIAudioInfo ******************************************/
/*!VHD_GetSlotHDMIAudioInfo
   Summary
   DELTA-h4k HDMI detect audio information
   Description
   This function detect HDMI audio type, InfoFrame and AES channel status   
   Parameters
   SlotHandle :            Handle of the slot to operate on
   pHDMIAudioType :        Pointer to caller\-allocated variable to
                           return audio type (see VHD_HDMI_AUDIO_TYPE)
   pHDMIAudioInfoFrame :   Pointer to caller\-allocated variable to
                           return audio InfoFrame (see VHD_HDMI_AUDIO_INFOFRAME)
   pHDMIAudioAESSts :      Pointer to caller\-allocated variable to
                           return audio AES channel status (see VHD_HDMI_AUDIO_AES_STS)
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration).*/
ULONG VIDEOMASTER_HD_API VHD_GetSlotHDMIAudioInfo(HANDLE SlotHandle, VHD_HDMI_AUDIO_TYPE *pHDMIAudioType, VHD_HDMI_AUDIO_INFOFRAME *pHDMIAudioInfoFrame, VHD_HDMI_AUDIO_AES_STS *pHDMIAudioAESSts);

#ifdef __cplusplus
}
#endif

#endif

#endif // _VIDEOMASTERHD_DVI_H_
