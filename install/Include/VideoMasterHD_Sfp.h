/*! VideomasterHD_Sfp.h

    Copyright (c) 2009, DELTACAST. All rights reserved.

    THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
    KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
    PURPOSE.

  Project : VideomasterHD

  Package : 

  Company : DELTACAST

  Author  : ms                             Date: 2014/04/03

  Purpose : 

*/


#ifndef _VIDEOMASTERHD_SFP_H_
#define _VIDEOMASTERHD_SFP_H_

typedef unsigned char UCHAR;

/*_ CONSTANTS ________________________________________________________________
//
// This section defines the different constants used by VideomasterHD_Sfp
*/


/*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideomasterHD_Sfp
*/

/*_ VHD_SFP_MODE ____________________________________________*/
/*!
   Summary
   VideoMasterHD SFP mode types
   Description
   The VHD_SFP_MODE enumeration lists the available mode types 
   corresponding on each SFP module supported by default 
   plus a standard mode for other SFP module.
   
   
   
   These values are used during VHD_SetMonitoredSFPModuleDefaultRegistersTable
   function call.                             */

typedef enum _VHD_SFP_MODE
{
      VHD_SFP_MODE_STANDARD,                /*! */

      //Coaxial
      VHD_SFP_MODE_EB30CSRT_LN,             /*! */
      VHD_SFP_MODE_EB30CSRT_LNR,            /*! */

      //Optical
      VHD_SFP_MODE_GO2929_31CM,             /*! */
      VHD_SFP_MODE_GO2949_85CL,             /*! */

      //Converters
      VHD_SFP_MODE_EB30CSRT_AN,             /*! */
      VHD_SFP_MODE_EB34TD1T_SN,             /*! */
      VHD_SFP_MODE_EB34TD1R_SN,             /*! */
      
      //HD-BNC Coaxial
      VHD_SFP_MODE_EB15HDRT_LN_PL,          /*! */

      NB_VHD_SFP_MODE

} VHD_SFP_MODE;

/*_ VHD_SFP_STATUSPROPERTY _______________________________________________*/
/*!
   Summary
   Hardware Sfp module status properties
   Description
   The VHD_SFP_STATUSPROPERTY enumeration lists all the available Sfp
   module status properties.

   These values are used as properties for VHD_GetSFPMoluleStatus functions calls

   See Also
   VHD_GetSFPMoluleStatus
   VHD_SetSFPMoluleCtrl
*/

typedef enum _VHD_SFP_STATUSPROPERTY
{
   //Diagnostics Registers
   VHD_SFP_SP_TEMPERATURE,                /*! */ 
   VHD_SFP_SP_TENSION,                    /*! */ 
   VHD_SFP_SP_LASERBIAS,                  /*! */ 
   VHD_SFP_SP_POWER_TX,                   /*! */ 
   VHD_SFP_SP_POWER_RX,                   /*! */ 

   //Register 110
   VHD_SFP_SP_DATA_READY_STATE,           /*! */ 
   VHD_SFP_SP_RX_LOS,                     /*! */ 
   VHD_SFP_SP_TX_FAULT,                   /*! */ 
   VHD_SFP_SP_SOFT_RATE_SELECT,           /*! */ 
   VHD_SFP_SP_SOFT_TX_DISABLE,            /*! */ 
   VHD_SFP_SP_TX_DIS_STATE,               /*! */ 

   //Register 111
   VHD_SFP_SP_TEMP_UPDATE,                /*! */ 
   VHD_SFP_SP_VCC_UPDATE,                 /*! */ 
   VHD_SFP_SP_TX_BIAS_UPDATE,             /*! */ 
   VHD_SFP_SP_TX_POWER_UPDATE,            /*! */ 
   VHD_SFP_SP_RX_POWER_UPDATE,            /*! */ 

   //Register 112
   VHD_SFP_SP_TEMP_HIGH_ALARM,            /*! */ 
   VHD_SFP_SP_TEMP_LOW_ALARM,             /*! */ 
   VHD_SFP_SP_VCC_HIGH_ALARM,             /*! */ 
   VHD_SFP_SP_VCC_LOW_ALARM,              /*! */ 
   VHD_SFP_SP_LASERBIAS_HIGH_ALARM,       /*! */ 
   VHD_SFP_SP_LASERBIAS_LOW_ALARM,        /*! */ 
   VHD_SFP_SP_TX_HIGH_ALARM,              /*! */ 
   VHD_SFP_SP_TX_LOW_ALARM,               /*! */ 

   //Register 113
   VHD_SFP_SP_RX_POWER_HIGH_ALARM,        /*! */ 
   VHD_SFP_SP_RX_POWER_LOW_ALARM,         /*! */ 

   //Register 116
   VHD_SFP_SP_TEMP_HIGH_WARNING,          /*! */ 
   VHD_SFP_SP_TEMP_LOW_WARNING,           /*! */ 
   VHD_SFP_SP_VCC_HIGH_WARNING,           /*! */ 
   VHD_SFP_SP_VCC_LOW_WARNING,            /*! */ 
   VHD_SFP_SP_LASERBIAS_HIGH_WARNING,     /*! */ 
   VHD_SFP_SP_LASERBIAS_LOW_WARNING,      /*! */ 
   VHD_SFP_SP_TX_HIGH_WARNING,            /*! */ 
   VHD_SFP_SP_TX_LOW_WARNING,             /*! */ 

   //Register 117
   VHD_SFP_SP_RX_POWER_HIGH_WARNING,      /*! */ 
   VHD_SFP_SP_RX_POWER_LOW_WARNING,       /*! */ 

   //Vendor Specific Registers
   VHD_SFP_SP_AUTOSLEEP,                  /*! */ 
   VHD_SFP_SP_RECLOCKER_RATE_TX,          /*! */ 
   VHD_SFP_SP_RECLOCKER_BYPASS_TX,        /*! */ 
   VHD_SFP_SP_RECLOCKER_LOCK_TX,          /*! */ 
   VHD_SFP_SP_RECLOCKER_BYPASS_RX,        /*! */ 
   VHD_SFP_SP_RECLOCKER_LOCK_RX,          /*! */ 
   VHD_SFP_SP_AUTOSLEEP_RX,               /*! */ 
   VHD_SFP_SP_PASSIVE_LOOPBACK,           /*! */ 

   NB_VHD_SFP_STATUSPROPERTIES
} VHD_SFP_STATUSPROPERTY;


/*_ VHD_SFP_CTRLPROPERTY _______________________________________________*/
/*!
   Summary
   Hardware Sfp module CTRL properties
   Description
   The VHD_SFP_CTRLPROPERTY enumeration lists all the available Sfp
   module CTRL properties.

   These values are used as properties for VHD_GetSFPMoluleCTRL functions calls

   See Also
   VHD_GetSFPMoluleStatus
   VHD_SetSFPMoluleCtrl
*/

typedef enum _VHD_SFP_CTRLPROPERTY
{

   //Register 110
   VHD_SFP_CP_SOFT_RATE=VHD_SFP_SP_SOFT_RATE_SELECT,                       /*! */ 
   VHD_SFP_CP_SOFT_TX_DISABLE=VHD_SFP_SP_SOFT_TX_DISABLE,                  /*! */ 

   //Register 120
   VHD_SFP_CP_RECLOCKER_BYPASS_TX=VHD_SFP_SP_RECLOCKER_BYPASS_TX,          /*! */ 

   //Register 124
   VHD_SFP_CP_RECLOCKER_BYPASS_RX=VHD_SFP_SP_RECLOCKER_BYPASS_RX,          /*! */ 
   VHD_SFP_CP_AUTOSLEEP=VHD_SFP_SP_AUTOSLEEP,                              /*! */ 
   VHD_SFP_CP_AUTOSLEEP_RX=VHD_SFP_SP_AUTOSLEEP_RX,                        /*! */ 
   VHD_SFP_CP_PASSIVE_LOOPBACK=VHD_SFP_SP_PASSIVE_LOOPBACK,                /*! */ 

   NB_VHD_SFPMODULE_CTRLPROPERTIES
} VHD_SFP_CTRLPROPERTY;

/*_ VHD_SFP_STATUS ___________________________________________________*/
/*!
   Summary
   SFP Status
   Description
   SFP status gets back by VHD_AuthorizeSFPModule,VHD_GetMonitoredSFPModuleRegisterValue,VHD_GetSFPModuleSerialID functions.
   This enum is used by the VHD_AuthorizeSFPModule, GetMonitoredSFPModuleRegisterValue, GetSFPModuleSerialID  API functions.
                  */

typedef enum _VHD_SFP_STATUS{
   VHD_SFP_STS_NO_MOD,        /*!_VHD_SFP_STATUS::VHD_SFP_STS_NO_MOD
                              No SFP module in corresponding SFP cage */
   VHD_SFP_STS_UNLOCKED,      /*!_VHD_SFP_STATUSSFP::VHD_SFP_STS_UNLOCKED
                              module unlocked */
   VHD_SFP_STS_LOCKED,        /*!_VHD_SFP_STATUSSFP::VHD_SFP_STS_LOCKED
                              module locked */
   VHD_SFP_STS_DELAY,         /*!_VHD_SFP_STATUS::VHD_SFP_STS_DELAY
                              Delay not respected between the used of VHD_AuthorizeSFPModule function */
   VHD_SFP_STS_NOT_READY,     /*!_VHD_SFP_STATUS::VHD_SFP_STS_NOT_READY
                              Monitored SFP module register value not ready */
   NB_VHD_SFP_STATUS
}VHD_SFP_STATUS;


/*_ STRUCTURES _______________________________________________________________
//
// This section defines the different structures used by VideomasterHD_Sfp
*/

/*_ VHD_SFP_SERIAL_ID ___________________________________________________*/
/*!
   Summary
   SFP Serial ID data
   Description
   The VHD_SERIAL_ID structure abstracts embrionix serial ID. 
   This structure is used by the VHD_GetSFPModuleSerialID API functions.
                  */
typedef union _VHD_SFP_SERIAL_ID
{   
   struct
   {
      UBYTE Identifier;          /*!Type of serial transceiver*/
      UBYTE ExtIdentifier;       /*!Extended type of serial transceiver*/
      UBYTE Connector;           /*!Connector type, Vendor specific */
      UBYTE Transceiver;         /*!Electronic or optical compatibility*/
      UBYTE pTransceiverCode[7]; /*!Code for electronic compatibility or optical compatibility*/
      UBYTE Encoding;            /*!Code for serial encoding algorithm*/
      UBYTE BRNominal;           /*!Nominal bit rate, units of 100Mbps*/
      UBYTE Reserved1;           /*!Reserved*/
      UBYTE Length9MicroM_km;    /*!Link length supported for standard SMF, units of km*/
      UBYTE Length9MicroM;       /*!Link length supported for standard SMF, units of 100m*/
      UBYTE Length50MicroM;      /*!Link length supported for 50/125 �m fiber, units of 10m*/
      UBYTE Length65MicroM;      /*!Link length supported for 62.5/125 �m fiber, units of 10 m*/
      UBYTE LengthCopper;        /*!Length supported (cooper) in m*/
      UBYTE Reserved2;           /*!Reserved*/
      UCHAR pVendorName[16];     /*!SFP transceiver vendor name (ASCII)*/
      UBYTE Reserved3;           /*!Reserved*/
      UBYTE pVendorOUI[3];       /*!SFP vendor IEEE company ID*/
      UCHAR pVendorPN[16];       /*!Part number(ASCII)*/
      UCHAR pVendorRev[4];       /*!Revision level for part number (ASCII)*/
      UBYTE WavelengthMSB;       /*!Reserved*/
      UBYTE WavelengthLSB;       /*!Reserved*/
      UBYTE Reserved4;           /*!Reserved*/
      UBYTE CCBASE;              /*!checksum (8lsb result) (add. 0-62)*/
      UBYTE pOptions[2];         /*!optional SFP signals are implemented*/
      UBYTE BRMax;               /*!Upper bit rate margin, unit of %*/
      UBYTE BRMin;               /*!Lower bit rate margin, unit of %*/
      UCHAR pVendorSN[16];       /*!Serial number (ASCII)*/
      UBYTE pDateCode[8];        /*!Vendor's manufacturing date code*/
      UBYTE DiagnosticMonitoring;/*!Diagnostic Monitoring*/
      UBYTE EnhancedOptions;     /*!Enhanced Options*/
      UBYTE SFF8472Compliance;   /*!SFF-8472 Compliance*/
      UBYTE CCEXT;               /*!Checksum (add. 64-94)*/
      UBYTE pVendorSpecific[32]; /*!Vendor Specific*/
   };
   UBYTE pWord[128];                               /*!*/
}VHD_SFP_SERIAL_ID;


/*_ VHD_SFP_REG_ADDR ___________________________________________________*/
/*!
   Summary
   SFP Address of register
   Description
   This structure is used by the VHD_SetMonitoredSFPModuleRegistersTable 
   and VHD_GetMonitoredSFPModuleRegistersTable API functions.
                  */
typedef struct _VHD_SFP_REG_ADDR
{
   UBYTE SlaveAddr;  /*!Memory block address*/
   UBYTE RegAddr;    /*!Register address*/
}VHD_SFP_REG_ADDR;

#ifndef EXCLUDE_API_FUNCTIONS


#ifdef __cplusplus
extern "C" {
#endif
/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideomasterHD_Sfp
*/

/*_ API FUNCTIONS GENERIC _______________________________________________________________
//
// This section defines the different API generic functions 
*/

/*** VHD_ReadSFPModuleRegister ****************************************************/
/*!VHD_ReadSFPModuleRegister
   Summary
   VideoMasterHD SFP module read register
   
   Description
   This function returns the value of the specified SFP module register
   
   Parameters
   BrdHandle :    Handle of the board to set or get SFP property
   ModIdx :       Index of SFP module cage 
   BlockAddr :    Memory block address
   Addr :         Register address
   pValue :       Value of register
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_ReadSFPModuleRegister(HANDLE BrdHandle, ULONG ModIdx, UBYTE BlockAddr,UBYTE Addr,UBYTE *pValue);

/*** VHD_WriteSFPModuleRegister ****************************************************/
/*!VHD_WriteSFPModuleRegister
   Summary
   VideoMasterHD SFP module write register
   
   Description
   This function sets the value of the specified SFP module register
   
   Parameters
   BrdHandle :    Handle of the board to set or get SFP property
   ModIdx :       Index of SFP module cage 
   BlockAddr :    Memory block address
   Addr :         Register address
   Value :        Value of register
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_WriteSFPModuleRegister(HANDLE BrdHandle, ULONG ModIdx, UBYTE BlockAddr, UBYTE Addr, UBYTE Value);

/*** VHD_GetSFPModuleSerialID ****************************************************/
/*!VHD_GetSFPModuleSerialID
   Summary
   VideoMasterHD SFP module get serial ID
   
   Description
   This function gets the serial ID of the specified SFP module
   
   Parameters
   BrdHandle :    Handle of the board to set or get SFP property
   ModIdx :       Index of the SFP module cage 
   pSerialID :    Serial ID of the SFP module
   pSts:          Data or module status
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_GetSFPModuleSerialID(HANDLE BrdHandle, ULONG ModIdx, VHD_SFP_SERIAL_ID *pSerialID, VHD_SFP_STATUS *pSts);

/*** VHD_GetSFPModuleType ****************************************************/
/*!VHD_GetSFPModuleType
   Summary
   VideoMasterHD SFP module get the module type
   
   Description
   This function gets the type of the specified SFP module
   
   Parameters
   BrdHandle :    Handle of the board to set or get SFP property
   ModIdx :       Index of the SFP module cage 
   pModuleType :  Type of the SFP module
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_GetSFPModuleType(HANDLE BrdHandle, ULONG ModIdx, VHD_SFP_MODE *pModuleType);

/*** VHD_AuthorizeSFPModule  ****************************************************/
/*!VHD_AuthorizeSFPModule
   Summary
   VideoMasterHD SFP module authorization function
   
   Description
   This function authorizes or not the specified SFP module in SFP cage
   
   Parameters
   BrdHandle :    Handle of the board to set or get SFP property
   ModIdx :       Index of SFP module cage 
   pKey�:         SFP module Key 
   pSts:          Data or module status
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_AuthorizeSFPModule(HANDLE BrdHandle, ULONG ModIdx, char *pKey, VHD_SFP_STATUS *pSts);

/*** VHD_SetMonitoredSFPModuleRegistersTable ****************************************************/
/*!VHD_SetMonitoredSFPModuleRegistersTable
   Summary
   VideoMasterHD Set of SFP module registers table 
   
   Description
   This function sets the registers table of a specified SFP module
   
   Parameters
   BrdHandle :    Handle of the board to set or get SFP property 
   ModIdx :       Index of SFP module cage 
   pTable :       Address of the table
   TableSize :    Table size
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_SetMonitoredSFPModuleRegistersTable(HANDLE BrdHandle,ULONG ModIdx, VHD_SFP_REG_ADDR *pTable, ULONG TableSize);

/*** VHD_GetMonitoredSFPModuleRegistersTable ****************************************************/
/*!VHD_GetMonitoredSFPModuleRegistersTable
   Summary
   VideoMasterHD Get of SFP module registers table 
   
   Description
   This function gets the registers table of a specified SFP module
   
   Parameters
   BrdHandle :    Handle of the board to set or get SFP property 
   ModIdx :       Index of SFP module cage 
   pTable :       Address of the table
   pTableSize :   Table size
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                            */
ULONG VIDEOMASTER_HD_API VHD_GetMonitoredSFPModuleRegistersTable(HANDLE BrdHandle,ULONG ModIdx, VHD_SFP_REG_ADDR *pTable, ULONG *pTableSize);

/*** VHD_GetMonitoredSFPModuleRegisterValue ****************************************************/
/*!VHD_GetMonitoredSFPModuleRegisterValue
   Summary
   VideoMasterHD Get of SFP Module register table value 
   
   Description
   This function gets the value of a specific register table index for the specified SFP module register
   
   Parameters
   BrdHandle :       Handle of the board to set or get SFP property 
   ModIdx :          Index of SFP module cage 
   TableEntryIdx :   Table entry index
   pValue :          Value of the entry table
   pSts:             Data or module status
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_GetMonitoredSFPModuleRegisterValue(HANDLE BrdHandle,ULONG ModIdx, ULONG TableEntryIdx, UBYTE *pValue, VHD_SFP_STATUS *pSts);











/*_ API FUNCTIONS MODULE _______________________________________________________________
//
// This section defines the different API module functions
*/

/*** VHD_SetMonitoredSFPModuleDefaultRegistersTable ****************************************************/
/*!VHD_SetMonitoredSFPModuleDefaultRegistersTable
   Summary
   VideoMasterHD SFP module set default value of registers table
   
   Description
   This function sets default value of registers table 
   for the specified SFP module register

   Parameters
   BrdHandle :    Handle of the board to set or get SFP property 
   ModIdx :       Index of SFP module cage 
   ModType :      mode type generic or Embrionix
   pTableAdd :       Address of the table with additional registers
   pTableSizeAdd :   Table size
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_SetMonitoredSFPModuleDefaultRegistersTable(HANDLE BrdHandle, ULONG ModIdx, VHD_SFP_MODE ModType, VHD_SFP_REG_ADDR *pTableAdd, ULONG TableSizeAdd);


/*** VHD_GetSFPMoluleStatus ****************************************************/
/*!VHD_GetSFPMoluleStatus
   Summary
   VideoMasterHD SFP module get status
   
   Description
   This function gets status of the specified SFP module
   
   Parameters
   BrdHandle :         Handle of the board to set or get SFP property 
   ModIdx :            Index of SFP module cage 
   SFPModuleProperty : SFP module property to get
   pValue :            Value to get
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_GetSFPMoluleStatus(HANDLE BrdHandle,ULONG ModIdx, VHD_SFP_STATUSPROPERTY SFPModuleProperty, ULONG *pValue);


/*** VHD_SetSFPMoluleCtrl ****************************************************/
/*!VHD_SetSFPMoluleCtrl
   Summary
   VideoMasterHD SFP module set control
   
   Description
   This function sets control of the specified SFP module
   
   Parameters
   BrdHandle :          Handle of the board to set or get SFP property 
   ModIdx :             Index of SFP module cage 
   SFPModuleProperty :  SFP module property to set
   Value :              value to set
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)                           */
ULONG VIDEOMASTER_HD_API VHD_SetSFPMoluleCtrl(HANDLE BrdHandle, ULONG ModIdx, VHD_SFP_CTRLPROPERTY SFPModuleProperty, ULONG Value);



#ifdef __cplusplus
}
#endif

#endif

#endif // _VIDEOMASTERHD_SFP_H_

