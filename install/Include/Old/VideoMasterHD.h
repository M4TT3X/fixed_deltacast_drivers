/*! VideomasterHD.h

    Copyright (c) 2009, DELTACAST. All rights reserved.

    THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
    KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
    PURPOSE.

  Project : VideomasterHD

  Package : 

  Company : DELTACAST

  Author  : gt                             Date: 2009/06/26

  Purpose : 

*/


#ifndef _VIDEOMASTERHD_H_
#define _VIDEOMASTERHD_H_



#include "VideoMasterHD_Core.h"
#include "VideoMasterHD_Sdi.h"


/*_ CONSTANTS ________________________________________________________________
//
// This section defines the different constants used by VideomasterHD
*/

#define VHD_RXSTS_OVERRUN                 VHD_CORE_RXSTS_OVERRUN    
#define VHD_RXSTS_UNLOCKED                VHD_CORE_RXSTS_UNLOCKED   
#define VHD_RXSTS_BADVIDEOSTD             VHD_SDI_RXSTS_BADVIDEOSTD 
#define VHD_RXSTS_BADDATA                 VHD_SDI_RXSTS_BADDATA     
#define VHD_TXSTS_UNDERRUN                VHD_CORE_TXSTS_UNDERRUN
#define VHD_GNLKSTS_NOREF                 VHD_SDI_GNLKSTS_NOREF
#define VHD_GNLKSTS_UNLOCKED              VHD_SDI_GNLKSTS_UNLOCKED   
#define VHD_EVTSRC_RX0                    VHD_CORE_EVTSRC_RX0      
#define VHD_EVTSRC_RX1                    VHD_CORE_EVTSRC_RX1      
#define VHD_EVTSRC_TX0                    VHD_CORE_EVTSRC_TX0      
#define VHD_EVTSRC_TX1                    VHD_CORE_EVTSRC_TX1      
#define VHD_EVTSRC_GENLOCK                VHD_SDI_EVTSRC_GENLOCK  
#define VHD_EVTSRC_WATCHDOG               VHD_CORE_EVTSRC_WATCHDOG 
#define VHD_RXSTS_MISALIGNED              VHD_KEYER_RXSTS_MISALIGNED
#define VHD_RXSTS_CARRIER_UNDETECTED      VHD_SDI_RXSTS_CARRIER_UNDETECTED  

#define VHD_ST_SINGLE_RX0              VHD_ST_RX0 
#define VHD_ST_SINGLE_RX1              VHD_ST_RX1      
#define VHD_ST_SINGLE_TX0              VHD_ST_TX0      
#define VHD_ST_SINGLE_TX1              VHD_ST_TX1      
#define VHD_ST_DUAL_RX                 (VHD_STREAMTYPE)4      
#define VHD_ST_DUAL_TX                 (VHD_STREAMTYPE)5      
#define VHD_ST_SINGLE_RX0_MODIFY_TX0   VHD_ST_RX0_MODIFY_TX0      
#define VHD_ST_SINGLE_RX1_MODIFY_TX1   VHD_ST_RX1_MODIFY_TX1  
#define VHD_ST_DUAL_RX_MODIFY_TX       (VHD_STREAMTYPE)8    
#define VHD_ST_SINGLE_RX2              VHD_ST_RX2
#define VHD_ST_SINGLE_RX3              VHD_ST_RX3
#define VHD_ST_SINGLE_TX2              VHD_ST_TX2
#define VHD_ST_SINGLE_TX3              VHD_ST_TX3


/*!VHD_VIDEOPACKING
   <color Red>This enumeration is now deprecated and replaced by
   VHD_BUFFERPACKING.</color>                                    */
typedef VHD_BUFFERPACKING  VHD_VIDEOPACKING;


/*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideomasterHD
*/

/*_ DELTA-hd BOARD PROPERTIES _______________________________________________*/
/*!VHD_BOARDPROPERTY
   Summary
   DELTA-hd board properties
   
   <color Red>This enumeration is now deprecated and replaced by
   VHD_CORE_BOARDPROPERTY.</color>
   Description
   The VHD_BOARDPROPERTY enumeration lists all the available
   board properties.
   
   These values are used as indexes for VHD_GetBoardProperty and
   VHD_SetBoardProperty functions calls.
   See Also
   VHD_GetBoardProperty VHD_SetBoardProperty                     */
typedef enum _VHD_BOARDPROPERTY
{
   VHD_BP_DRIVER_VERSION = 0,       /*! DELTA-x driver version */
   VHD_BP_FIRMWARE_VERSION,         /*! DELTA-x fpga firmware version */
   VHD_BP_SERIALNUMBER_LSW,         /*! DELTA-x board serial number (32 least significant bits) */
   VHD_BP_SERIALNUMBER_MSW,         /*! DELTA-x board serial number (32 most significant bits) */
   VHD_BP_BOARD_TYPE,               /*! DELTA-x board type */
   VHD_BP_REF_COUNT,                /*! DELTA-x board handles counter */
   VHD_BP_CHN_AVAILABILITY,         /*! Physical channel availability mask (bit0 = RX0, bit1 = RX1, bit2 = RX2, bit3 = RX3, bit4 = TX0, bit5 = TX1, bit6 = TX2, bit7 = TX3) */
   VHD_BP_SETUP_LOCK,               /*! Board setup lock */
   VHD_BP_BYPASS_RELAY_0,           /*! DELTA-x RXTX 0 by-pass relay control (TRUE = loopthrough enabled, FALSE = relay disabled) */
   VHD_BP_BYPASS_RELAY_1,           /*! DELTA-x RXTX 1 by-pass relay control (TRUE = loopthrough enabled, FALSE = relay disabled) */
   VHD_BP_CLOCK_SYSTEM,             /*! On read, read back clock system if genlock source is local. Otherwise, detected clock system on selected genlock input if locked. On write, board clock system selection (see VHD_CLOCKDIVISOR)*/
   VHD_BP_GENLOCK_SOURCE,           /*! DELTA-x board genlock source control (see VHD_GENLOCKSOURCE) */
   VHD_BP_GENLOCK_OFFSET,           /*! Phase offset (in pixels) to apply to genlocked TX streams */
   VHD_BP_RX0_STATUS,               /*! RX0 channel status (see VHD_RXSTS_xxx) */
   VHD_BP_RX1_STATUS,               /*! RX1 channel status (see VHD_RXSTS_xxx) */
   VHD_BP_TX0_STATUS,               /*! TX0 channel status (see VHD_TXSTS_xxx) */
   VHD_BP_TX1_STATUS,               /*! TX1 channel status (see VHD_TXSTS_xxx) */
   VHD_BP_GENLOCK_STATUS,           /*! Genlock status (see VHD_GNLKSTS_xxx) */
   VHD_BP_GENLOCK_VIDEO_STANDARD,   /*! On read, detected video standard on selected genlock input if locked (see VHD_VIDEOSTANDARD). On write, configured genlock video standard (VHD_VIDEOSTD_* = manual setting, VHD_VIDEOSTD_AUTO_DETECT_GENLOCK = auto) */
   VHD_BP_STATE_CHANGE_EVENT_MASK,  /*! Event types that will trigger the registered caller event, if any */
   VHD_BP_RX0_STANDARD,             /*! Incoming video standard on RX0 connector (see VHD_VIDEOSTANDARD) */
   VHD_BP_RX1_STANDARD,             /*! Incoming video standard on RX1 connector (see VHD_VIDEOSTANDARD) */
   VHD_BP_WATCHDOG_TIMER,           /*! By-pass relays watchdog timer value (in msec, max 60000 msec). Watchdog re-establish by-pass relays loopthrough if not re-armed using VHD_RearmWatchdog every configured time. Set value to 0 to disable watchdog (default)*/
   VHD_BP_RX2_STANDARD,             /*! Incoming video standard on RX2 connector (see VHD_VIDEOSTANDARD) */
   VHD_BP_RX3_STANDARD,             /*! Incoming video standard on RX3 connector (see VHD_VIDEOSTANDARD) */
   VHD_BP_RX2_STATUS,               /*! RX2 channel status (see VHD_RXSTS_xxx) */
   VHD_BP_RX3_STATUS,               /*! RX3 channel status (see VHD_RXSTS_xxx) */
   VHD_BP_TX2_STATUS,               /*! TX2 channel status (see VHD_TXSTS_xxx) */
   VHD_BP_TX3_STATUS,               /*! TX3 channel status (see VHD_TXSTS_xxx) */
   VHD_BP_SERIALNUMBER_EX,          /*! DELTA-x board serial number extension */
   VHD_BP_NBOF_LANE,                /*! Number of negotiated PCI express lane. This property is only available for PCI express board. */
   VHD_BP_FIRMWARE2_VERSION,        /*! DELTA-x cpld firmware version */
   VHD_BP_FIRMWARE3_VERSION,        /*! DELTA-x micro-controller firmware version */
   NB_VHD_BOARDPROPERTIES
} VHD_BOARDPROPERTY;


/*_ VHD_STREAMPROPERTY ______________________________________________*/
/*!VHD_STREAMPROPERTY
   Summary
   VideoMasterHD streams properties
   
   <color Red>This enumeration is now deprecated and replaced by
   VHD_CORE_STREAMPROPERTY.</color>
   Description
   The VHD_STREAMPROPERTY enumeration lists all the available
   stream properties.
   
   These values are used as indexes for VHD_GetStreamProperty
   and VHD_SetStreamProperty functions calls.
   See Also
   VHD_GetStreamProperty VHD_SetStreamProperty                   */

typedef enum _VHD_STREAMPROPERTY
{
   VHD_SP_REF_COUNT = 0,         /*! Logical stream handles counter */
   VHD_SP_FORCE_EXCLUSIVE,       /*! Force exclusive access to this stream : no other handle may be created on the same stream type on this board (default is FALSE) */
   VHD_SP_TRANSFER_SCHEME,       /*! Logical stream transfers handling scheme selection (see VHD_TRANSFERSCHEME) */
   VHD_SP_VIDEO_STANDARD,        /*! Logical stream video standard selection (see VHD_VIDEOSTANDARD) */
   VHD_SP_VIDEO_PACKING,         /*! Logical stream video packing selection (see VHD_VIDEOPACKING) */
   VHD_SP_BUFFER_DEPTH,          /*! Stream driver buffers queue depth, in number of frames (minimum is 2, default is 4) */
   VHD_SP_IO_TIMEOUT,            /*! Stream I/O operations time-out configuration (default is 100 msec) */
   VHD_SP_TX_GENLOCK,            /*! TX logical stream genlocking control (default is FALSE) */
   VHD_SP_TX_OUTPUT,             /*! TX output control (see VHD_OUTPUTMODE) */
   VHD_SP_FRAMES_COUNT,          /*! Count the number of frames transferred since the stream has been started */
   VHD_SP_FRAMES_DROPPED,        /*! Count the number of dropped frames since the stream has been started */
   VHD_SP_BUFFER_FILLING,        /*! Current filling level of stream driver buffer queue, in number of frames */
   VHD_SP_BUFFER_PRELOAD,        /*! Number of TX buffer to preload before actual channel start. Cannot be greater than VHD_SP_BUFFER_DEPTH. */
   NB_VHD_STREAMPROPERTIES
} VHD_STREAMPROPERTY;



/*_ VHD_KEYERPROPERTY _______________________________________________*/
/*!VHD_KEYERPROPERTY
   Summary
   VideoMasterHD keyer properties
   
   <color Red>This enumeration is now deprecated and replaced by
   VHD_KEYER_BOARDPROPERTY.</color>
   Description
   The VHD_KEYERPROPERTY enumeration lists all the available
   keyer properties.
   
   These values are used as indexes for VHD_GetKeyerProperty and
   VHD_SetKeyerProperty functions calls
   See Also
   VHD_GetKeyerProperty VHD_SetKeyerProperty                     */

typedef enum _VHD_KEYERPROPERTY
{
   VHD_KP_ENABLE = 0,               /*!_VHD_KEYERPROPERTY::VHD_KP_ENABLE
                                          DELTA-hd-key hardware keyer enable (default is FALSE) */
   VHD_KP_INPUT_A,                  /*!_VHD_KEYERPROPERTY::VHD_KP_INPUT_A
                                          DELTA-hd-key keyer first input selection (see VHD_KEYERINPUT), default is VHD_KINPUT_RX0 */
   VHD_KP_INPUT_B,                  /*!_VHD_KEYERPROPERTY::VHD_KP_INPUT_B
                                          DELTA-hd-key keyer second input selection (see VHD_KEYERINPUT), default is VHD_KINPUT_TX0 */
   VHD_KP_INPUT_K,                  /*!_VHD_KEYERPROPERTY::VHD_KP_INPUT_K
                                          DELTA-hd-key keyer key input selection (see VHD_KEYERINPUT), default is VHD_KINPUT_TX0 */
   VHD_KP_VIDEOOUTPUT_TX0,          /*!_VHD_KEYERPROPERTY::VHD_KP_VIDEOOUTPUT_TX0
                                          DELTA-hd-key TX0 connector video output source (see VHD_KEYEROUTPUT), default is VHD_KOUTPUT_KEYER */
   VHD_KP_VIDEOOUTPUT_TX1,          /*!_VHD_KEYERPROPERTY::VHD_KP_VIDEOOUTPUT_TX1
                                          DELTA-hd-key TX1 connector video output source (see VHD_KEYEROUTPUT), default is VHD_KOUTPUT_TEST */
   VHD_KP_ANCOUTPUT_TX0,            /*!_VHD_KEYERPROPERTY::VHD_KP_ANCOUTPUT_TX0
                                          DELTA-hd-key TX0 connector ancillary data output source (see VHD_KEYEROUTPUT), default is VHD_KOUTPUT_TEST */
   VHD_KP_ANCOUTPUT_TX1,            /*!_VHD_KEYERPROPERTY::VHD_KP_ANCOUTPUT_TX1
                                          DELTA-hd-key TX1 connector ancillary data output source (see VHD_KEYEROUTPUT), default is VHD_KOUTPUT_TEST */
   VHD_KP_BLENDING_TYPE,            /*!_VHD_KEYERPROPERTY::VHD_KP_BLENDING_TYPE
                                          DELTA-hd-key alpha blending type (see VHD_KEYERBLENDINGTYPE), default is VHD_BLENDING_MULTIPLICATIVE */
   VHD_KP_ALPHACLIP_MIN,            /*!_VHD_KEYERPROPERTY::VHD_KP_ALPHACLIP_MIN
                                          DELTA-hd-key minimum value for alpha clipping (default is 0) */
   VHD_KP_ALPHACLIP_MAX,            /*!_VHD_KEYERPROPERTY::VHD_KP_ALPHACLIP_MAX
                                          DELTA-hd-key maximum value for alpha clipping (default is 0) */
   VHD_KP_ALPHABLEND_FACTOR,        /*!_VHD_KEYERPROPERTY::VHD_KP_ALPHABLEND_FACTOR
                                          DELTA-hd-key global alpha blending factor (default is 0) */
   VHD_KP_INVERT_KEY,               /*!_VHD_KEYERPROPERTY::VHD_KP_INVERT_KEY
                                          DELTA-hd-key alpha blending inversion (default is FALSE) */
   VHD_KP_RX0_GENLOCK_OFFSET,       /*! RX0 signal phase offset towards reference clock */
   VHD_KP_RX1_GENLOCK_OFFSET,       /*! RX1 signal phase offset towards reference clock */
   VHD_KP_RX0_BLACK,                /*! RX0 signal is replaced by a black video signal */
   VHD_KP_RX1_BLACK,                /*! RX1 signal is replaced by a black video signal */
   NB_VHD_KEYERPROPERTIES
} VHD_KEYERPROPERTY;



/*_ VHD_BOARDRESET ___________________________________________________*/
/*!VHD_BOARDRESET
   Summary
   VideomasterHD board reset operations
   
   <color Red>This enumeration is now deprecated.</color>
   Description
   The VHD_BOARDRESET enumeration lists all the available driver
   and board reset stages.
   
   These values are used during VHD_ResetBoard function calls.
   See Also
   VHD_ResetBoard                                                */

typedef enum _VHD_BOARDRESET
{
   VHD_RESET_GENLOCK = 0,           /*! Obsolete. Genlock circuitry reset is completely handled in hardware */
   VHD_RESET_CLOSE_STREAMS,         /*! Resets streams handling state machine, closes physical channels, and resets streams properties */
   VHD_RESET_BOARD_RESET,           /*! Performs VHD_BR_CLOSE_STREAMS, and then resets board handling state machine, and resets board properties */
   VHD_RESET_FIRMWARE_RELOAD,       /*! Performs VHD_BR_BOARD_RESET, and then reloads board firmware */
   NB_VHD_BOARDRESETS
} VHD_BOARDRESET;



/*_ DELTA-hd STREAM PROCESSING MODES ________________________________________*/
/*!VHD_STREAMPROCMODE
   Summary
   VideoMasterHD streams processing modes
   
   <color Red>This enumeration is now deprecated and replaced by
   VHD_SDI_STREAMPROCMODE.</color>
   Description
   The VHD_STREAMPROCMODE enumeration lists all the stream
   processing modes.
   
   These values are used during VHD_OpenStreamHandle function
   calls.
   See Also
   VHD_OpenStreamHandle                                          */

typedef enum _VHD_STREAMPROCMODE
{
   VHD_STPROC_RAW = 0,           /*! Raw data mode (full bit stream handling) */
   VHD_STPROC_JOINED,            /*! Joined processed mode (synchronous video/anc handling) */
   VHD_STPROC_DISJOINED_VIDEO,   /*! Disjoined processed mode - (video handling) */
   VHD_STPROC_DISJOINED_ANC,     /*! Disjoined processed mode - (anc handling) */
   NB_VHD_STREAMPROCMODES
} VHD_STREAMPROCMODE;




/*_ STRUCTURES _______________________________________________________________
//
// This section defines the different structures used by VideomasterHD
*/




/*_ VHD_BOARDINFO ___________________________________________________*/
/*!VHD_BOARDINFO
   Summary
   Board information
   
   <color Red>This structure is now deprecated and replaced by
   board properties.</color>
   Description
   The VHD_BOARDINFO structure contains information about a
   particular board.
   
   The VHD_GetBoardInfo API function uses this structure to
   provide identification information about a board.
   Remarks
   The FirmwareVersion field of the VHD_BOARDINFO structure are
   BCD-encoded (Binary-coded decimal) as follows:
   
     * bits 31..24 : day
     * bits 23..16 : month
     * bits 15..8 : year
     * bits 7..4 : version
     * bits 3..0 : revision
   See Also
   VHD_GetBoardInfo VHD_BOARDTYPE                               */

typedef struct _VHD_BOARDINFO
{
   ULONG    DriverVersion;       /*!
                                 Driver version handling this board
                                 */
   ULONG    FirmwareVersion;     /*!
                                 Board firmware version
                                 */
   LONGLONG SerialNumber;        /*! Board serial number */
   ULONG    BoardType;           /*! DELTA-hd board type (see VHD_BOARDTYPE) */
} VHD_BOARDINFO;



/*_ VHD_BOARDINFOEX _____________________________________________*/
/*!VHD_BOARDINFOEX
   Summary
   Board extended information
   
   <color Red>This structure is now deprecated and replaced by
   board properties.</color>
   Description
   The VHD_BOARDINFOEX structure contains layout and version
   information about a particular board.
   
   The VHD_GetBoardInfoEx API function uses this structure to
   provide identification information about a board.
   Remarks
   See Also
   VHD_GetBoardInfoEx VHD_BOARDINFOEX                          */

typedef struct _VHD_BOARDINFOEX
{
   VHD_BOARDINFO  BaseInformation;     /*! Base information, as provided by VHD_GetBoardInfo */
   BOOL32           PciExpress;          /*! TRUE if the board is PCI-e, FALSE if it is PCI */
   BOOL32           HasGenlockCircuitry; /*! TRUE if the board own genlock circuitry, FALSE if no genlock circuitry is present. As an example, RX-only boards does not need genlock circuitry */
   BOOL32           LowProfile;          /*! TRUE if the board is of low-profile form factor, FALSE if it is of half-size PCI form factor */
   int            NbRxChannels_i;      /*! Number of RX channels */
   int            NbTxChannels_i;      /*! Number of TX channels */
   LPVOID         pReserved;           /*! Reserved for future use */
} VHD_BOARDINFOEX;



/*_ SLOT INFORMATION ________________________________________________________*/
/*!VHD_SLOTINFO
   Summary
   VideoMasterHD slot information
   
   <color Red>This structure is now deprecated and replaced by
   slot handles and associated functions (see
   VHD_LockSlotHandle).</color>
   Description
   The VHD_SLOTINFO structure contains information about a
   particular slot.
   
   
   
   It takes part of the VHD_SLOT structure used by the
   VHD_LockSlot and VHD_UnlockSlot API functions.
   See Also
   VHD_SLOT VHD_LockSlot VHD_UnlockSlot VHD_STREAMPROCMODE     */

typedef struct _VHD_SLOTINFO
{
   ULONG    SlotType;            /*! Slot type (taken from the VHD_STREAMPROCMODE enumeration) */
   ULONG    TimeStamp;           /*!_VHD_SLOTINFO::TimeStamp
                                    Slot time stamp (now replaced by VHD_GetSlotID) */
   ULONG    Reserved;            /*! Reserved */
} VHD_SLOTINFO;






/*_ VHD_RAWSLOTDATA ___________________________________________________________*/
/*!VHD_RAWSLOTDATA
   Summary
   Raw data slot data pointer
   
   <color Red>This structure is now deprecated and replaced by
   slot handles and associated functions (see
   VHD_LockSlotHandle).</color>
   Description
   The VHD_RAWSLOTDATA structure gives access to the data buffer
   of a raw slot.
   
   
   
   It takes part of the VHD_SLOT structure used by the
   VHD_LockSlot and VHD_UnlockSlot API functions.
   See Also
   VHD_SLOT VHD_LockSlot VHD_UnlockSlot                          */

typedef struct _VHD_RAWSLOTDATA
{
   ULONG    RawDataSize;         /*! Raw data size (in bytes) */
   BYTE    *pBuffer;             /*! Slot raw data buffer */
} VHD_RAWSLOTDATA;



/*_ VHD_JOINEDSLOTDATA ________________________________________________________*/
/*!VHD_JOINEDSLOTDATA
   Summary
   Joined mode slot data pointers
   
   <color Red>This structure is now deprecated and replaced by
   slot handles and associated functions (see
   VHD_LockSlotHandle).</color>
   Description
   The VHD_JOINEDSLOTDATA structure gives access to the data
   buffers of a joined slot.
   
   It contains pointers to both the video and the ancillary
   media types data.
   
   
   
   It takes part of the VHD_SLOT structure used by the
   VHD_LockSlot and VHD_UnlockSlot API functions.
   See Also
   VHD_SLOT VHD_LockSlot VHD_UnlockSlot                        */

typedef struct _VHD_JOINEDSLOTDATA
{
   ULONG    VideoDataSize;       /*! Video data size (in bytes) */
   BYTE    *pVideoBuffer;        /*! Video data buffer */
   ULONG    ANCDataSize;         /*!  ANC data size (in bytes). Do not access this field, use VHD_Anc* functions */
   BYTE    *pANCBuffer;          /*!  ANC data buffer. Do not access this field, use VHD_Anc* functions */
} VHD_JOINEDSLOTDATA;



/*_ VHD_DISJOINEDSLOTDATA _____________________________________________________*/
/*!VHD_DISJOINEDSLOTDATA
   Summary
   Disjoined mode slot data pointer
   
   <color Red>This structure is now deprecated and replaced by
   slot handles and associated functions (see
   VHD_LockSlotHandle).</color>
   Description
   The VHD_DISJOINEDSLOTDATA structure gives access to one data
   buffer of a disjoined slot.
   
   It contains pointer to either the video or the ancillary
   media type data.
   
   
   
   It takes part of the VHD_SLOT structure used by the
   VHD_LockSlot and VHD_UnlockSlot API functions.
   
   
   
   When working in disjoined ANC mode, do not access directly to
   the slot buffer, use VHD_Anc* functions instead
   See Also
   VHD_SLOT VHD_LockSlot VHD_UnlockSlot                          */

typedef struct _VHD_DISJOINEDSLOTDATA
{
   ULONG    DataSize;            /*! Data size (in bytes) */
   BYTE    *pBuffer;             /*! Data buffer */
} VHD_DISJOINEDSLOTDATA;





/*_ VHD_SLOT _______________________________________________________________*/
/*!VHD_SLOT
   Summary
   Slot data pointer
   
   <color Red>This structure is now deprecated and replaced by
   slot handles and associated functions (see
   VHD_LockSlotHandle).</color>
   Description
   The VHD_SLOT structure gives access to one slot data.
   
   It presents slot information, as well as a union containing
   pointers to the available media types data, depending on the
   selected stream processing mode.
   
   
   
   This structure is used by the VHD_LockSlot and VHD_UnlockSlot
   API functions.
   See Also
   VHD_SLOTINFO VHD_RAWSLOTDATA VHD_JOINEDSLOTDATA
   VHD_DISJOINEDSLOTDATA VHD_LockSlot VHD_UnlockSlot             */

typedef struct _VHD_SLOT
{
   VHD_SLOTINFO               SlotInfo;      /*! Slot information structure */
   union
   {
      VHD_RAWSLOTDATA         Raw;           /*! Raw data */
      VHD_JOINEDSLOTDATA      Joined;        /*! Joined data */
      VHD_DISJOINEDSLOTDATA   Disjoined;     /*! Disjoined data */
   } SlotData;                               /*! Slot data union */
} VHD_SLOT;







#ifndef EXCLUDE_API_FUNCTIONS

#ifdef __cplusplus
extern "C" {
#endif

/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideomasterHD
*/



/*_ API management functions ________________________________________________*/



/*** VHD_GetBoardInfo ********************************************************/
/*!VHD_GetBoardInfo@ULONG@VHD_BOARDINFO *
   Summary
   Board information query
   
   <color Red>This function is now deprecated and replaced by
   board properties.</color>
   Description
   This function provides information about the specified board
   Parameters
   BoardIndex :  Zero\-based index of the board to get
                 information for
   pInfo :       Pointer to a caller\-allocated structure wherein
                 to store board information
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_BOARDINFO                                                  */
ULONG VIDEOMASTER_HD_API VHD_GetBoardInfo (ULONG BoardIndex, VHD_BOARDINFO *pInfo);


/*** VHD_GetBoardInfoEx ********************************************************/
/*!VHD_GetBoardInfoEx@ULONG@VHD_BOARDINFOEX *
   Summary
   Board extended information query
   
   <color Red>This function is now deprecated and replaced by
   board properties.</color>
   Description
   This function provides extended information about the
   specified board
   Parameters
   BoardIndex :  Zero\-based index of the board to get
                 information for
   pInfo :       Pointer to a caller\-allocated structure wherein
                 to store board information
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_BOARDINFOEX                                                */
ULONG VIDEOMASTER_HD_API VHD_GetBoardInfoEx (ULONG BoardIndex, VHD_BOARDINFOEX *pInfoEx);




/*_ Boards handling functions _______________________________________________*/



/*** VHD_ResetBoard **********************************************************/
/*!VHD_ResetBoard@HANDLE@ULONG
   Summary
   Board reset
   
   <color Red>This function is now deprecated.</color>
   Description
   This function performs the requested reset operation on the
   specified board instance
   Parameters
   BrdHandle :  Handle of the board to reset
   ResetType :  Type of reset to perform, must be a value of the
                VHD_BOARDRESET enumeration
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_BOARDRESET                                                */
ULONG VIDEOMASTER_HD_API VHD_ResetBoard (HANDLE BrdHandle, ULONG ResetType);






/*_ Streams handling functions _______________________________________________*/


/*** VHD_OpenStreamHandle ****************************************************/
/*!VHD_OpenStreamHandle@HANDLE@VHD_STREAMTYPE@VHD_STREAMPROCMODE@BOOL32 *@HANDLE *@HANDLE
   <title VHD_OpenStreamHandle - old>
   <toctitle VHD_OpenStreamHandle - old>
   
   Summary
   VideoMasterHD stream handle opening
   
   <color Red>This function is now deprecated and replaced by
   VHD_OpenStreamHandle function located in
   VideoMasterHD_Core.h.</color>
   Description
   This function opens a handle to the specified logical stream
   Parameters
   BrdHandle :         Handle of the board to open a stream
                       handle on
   StrmType :          Type of logical stream to open, must be a
                       value of the VHD_CORE_STREAMTYPE
                       enumeration
   ProcessingMode :    Mode the stream is processed, must be a
                       value of the VHD_xxx_STREAMPROCMODE
                       enumeration
   pSetupLock :        Pointer to a boolean variable if must lock
                       configuration on that stream, or NULL
                       otherwise. If not NULL, then the boolean
                       is updated by VideoMasterHD to signal if
                       the handle owns the configuration lock or
                       not
   pStrmHandle :       Pointer to a caller\-allocated variable
                       receiving the stream handle
   OnDataReadyEvent :  Optional caller\-allocated event to be
                       triggered by VideoMasterHD each time a
                       slot is ready
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_OpenBoardHandle VHD_CloseStreamHandle VHD_STREAMTYPE
   VHD_STREAMPROCMODE                                             */

//ULONG VIDEOMASTER_HD_API VHD_OpenStreamHandle (HANDLE BrdHandle, VHD_STREAMTYPE StrmType, VHD_STREAMPROCMODE ProcessingMode, BOOL32 *pSetupLock, HANDLE *pStrmHandle, HANDLE OnDataReadyEvent);




/*_ Low-level transfers handling functions __________________________________*/


/*** VHD_LockSlot ************************************************************/
/*!VHD_LockSlot@HANDLE@VHD_SLOT *
   Summary
   VideoMasterHD slot query
   
   <color Red>This function is now deprecated and replaced by
   VHD_LockSlotHandle.</color>
   Description
   This function locks a stream slot.
   
   If none is ready, the caller is put asleep until such a slot
   is available, or until the time-out value specified by the
   VHD_SP_IO_TIMEOUT stream property is reached.
   
   Every locked slot must be unlocked using VHD_UnlockSlot
   Parameters
   StrmHandle :  Handle of the stream to operate on
   pSlot :       Pointer to caller\-allocated structure to return
                 locked slot
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_UnlockSlot VHD_SLOT                                        */
ULONG VIDEOMASTER_HD_API VHD_LockSlot (HANDLE StrmHandle, VHD_SLOT *pSlot);


/*** VHD_UnlockSlot **********************************************************/
/*!VHD_UnlockSlot@HANDLE@VHD_SLOT *
   Summary
   VideoMasterHD slot releasing
   
   <color Red>This function is now deprecated and replaced by
   VHD_UnlockSlotHandle.</color>
   Description
   This function unlocks a locked stream slot. Every locked slot
   must be unlocked using this function
   Parameters
   StrmHandle :  Handle of the stream to operate on
   pSlot :       Pointer to the slot to unlock
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_LockSlot VHD_SLOT                                         */

ULONG VIDEOMASTER_HD_API VHD_UnlockSlot (HANDLE StrmHandle, VHD_SLOT *pSlot);




/*_ Ancillary data buffers navigation functions _____________________________*/


/*** VHD_AncGetNbPackets *****************************************************/
/*!VHD_AncGetNbPackets@HANDLE@VHD_SLOT *@int@VHD_SAMPLETYPE@int *
   Summary
   Number of ancillary data packets on a line query
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotAncGetNbPackets.</color>
   Description
   This function gives the number of ancillary data packets
   found on the specified line number
   Parameters
   StrmHandle :     Handle of the stream to work on
   pSlot :          Pointer to slot to get ancillary data packets
                    count from
   LineNumber :     Video line number to get count from (1..N,
                    with N depending on the standard)
   AncStream :      Specifies if working on Y or C ancillary data
                    stream (in SD, only C stream is used)
   pNbAncPackets :  Pointer to caller\-allocated variable
                    receiving the ancillary data packets count
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the AncStream argument value must be
   VHD_SAMPLE_C, since Y and C samples are multiplexed together
   to carry on ancillary data packets
   See Also
   VHD_LockSlot VHD_AncGetPacket                                  */
ULONG VIDEOMASTER_HD_API VHD_AncGetNbPackets (HANDLE StrmHandle, VHD_SLOT *pSlot, int LineNumber, VHD_SAMPLETYPE AncStream, int *pNbAncPackets);


/*** VHD_AncGetPacket ********************************************************/
/*!VHD_AncGetPacket@HANDLE@VHD_SLOT *@int@VHD_SAMPLETYPE@int@VHD_ANCPACKET **
   Summary
   Ancillary data packet retrieval
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotAncGetPacket.</color>
   Description
   This function retrieves the specified ancillary data packet
   Parameters
   StrmHandle :   Handle of the stream to work on
   pSlot :        Pointer to slot to get ancillary data packet
                  from
   LineNumber :   Video line number to get packet from (1..N,
                  with N depending on the standard)
   AncStream :    Specifies if working on Y or C ancillary data
                  stream (in SD, only C stream is used)
   PacketIndex :  Index (zero\-based) of the packet to retrieve
   ppAncPacket :  Pointer receiving a pointer to allocated ANC
                  packet structure
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the AncStream argument value must be
   VHD_SAMPLE_C, since Y and C samples are multiplexed together
   to carry on ancillary data packets.
   
   In RX-modify-TX streams, you can freely update the content of
   the packet, but you cannot change its size. To replace this
   packet with a packet of different size, remove this one and
   allocate a new one
   See Also
   VHD_LockSlot VHD_AncGetNbPackets VHD_ANCPACKET                */
ULONG VIDEOMASTER_HD_API VHD_AncGetPacket (HANDLE StrmHandle, VHD_SLOT *pSlot, int LineNumber, VHD_SAMPLETYPE AncStream, int PacketIndex, VHD_ANCPACKET **ppAncPacket);


/*** VHD_AncAllocatePacket ***************************************************/
/*!VHD_AncAllocatePacket@HANDLE@BYTE@VHD_ANCPACKET **
   Summary
   Ancillary data packet allocation
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotAncAllocatePacket.</color>
   Description
   This function allocates memory for a new ancillary data
   packet. This function is less efficient than
   VHD_AncAllocatePacketEx function.
   Parameters
   StrmHandle :   Handle of the stream to work on
   DataCount :    Number of User Data Words to allocate
   ppAncPacket :  Pointer receiving a pointer to allocated ANC
                  packet structure
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Once a new packet has been allocated and properly filled in,
   it may be inserted on a specific video line using the
   VHD_AncInsertPacket function.
   
   Allocated memory is automatically released by VideoMasterHD
   once the ancillary data slot is unlocked or when the stream
   is stopped.
   
   User data words of allocated packets are all reset to zero by
   this function.
   
   
   
   Although the allocated packet will contain DataCount user
   data words, the caller may freely set the actual DataCount
   field of the packet structure to a value less or equal to it
   in before subsequent call to VHD_AncInsertPacket
   See Also
   VHD_AncInsertPacket VHD_ANCPACKET                             */
ULONG VIDEOMASTER_HD_API VHD_AncAllocatePacket (HANDLE StrmHandle, BYTE DataCount, VHD_ANCPACKET **ppAncPacket);


/*** VHD_AncAllocatePacketEx ***************************************************/
/*!VHD_AncAllocatePacketEx@HANDLE@VHD_SLOT *@BYTE@VHD_ANCPACKET **
   Summary
   Ancillary data packet allocation
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotAncAllocatePacket.</color>
   Description
   This function allocates memory for a new ancillary data
   packet. This function is more efficient than
   VHD_AncAllocatePacket function.
   Parameters
   StrmHandle :   Handle of the stream to work on
   pSlot :        Pointer to slot to insert ancillary data packet
                  to.
   DataCount :    Number of User Data Words to allocate
   ppAncPacket :  Pointer receiving a pointer to allocated ANC
                  packet structure
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Once a new packet has been allocated and properly filled in,
   it may be inserted on a specific video line using the
   VHD_AncInsertPacket function.
   
   Allocated memory is automatically released by VideoMasterHD
   once the ancillary data slot is unlocked or when the stream
   is stopped.
   
   User data words of allocated packets are all reset to zero by
   this function.
   
   
   
   Although the allocated packet will contain DataCount user
   data words, the caller may freely set the actual DataCount
   field of the packet structure to a value less or equal to it
   in before subsequent call to VHD_AncInsertPacket
   See Also
   VHD_AncInsertPacket VHD_ANCPACKET                              */
ULONG VIDEOMASTER_HD_API VHD_AncAllocatePacketEx (HANDLE StrmHandle, VHD_SLOT *pSlot, BYTE DataCount, VHD_ANCPACKET **ppAncPacket);


/*** VHD_AncInsertPacket *****************************************************/
/*!VHD_AncInsertPacket@HANDLE@VHD_SLOT *@int@VHD_SAMPLETYPE@int@VHD_ANCPACKET *
   Summary
   Ancillary data packet insertion
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotAncInsertPacket.</color>
   Description
   This function insert the given ancillary data packet at the
   specified line
   Parameters
   StrmHandle :   Handle of the stream to work on
   pSlot :        Pointer to slot to insert ancillary data packet
                  in
   LineNumber :   Video line number to insert packet on (1..N,
                  with N depending on the standard)
   AncStream :    Specifies if working on Y or C ancillary data
                  stream (in SD, only C stream is used)
   PacketIndex :  Position (zero\-based) to put this packet at
                  (\-1 to insert at last location)
   pAncPacket :   Pointer to ancillary data packet to insert
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the AncStream argument is ignored, since
   Y and C samples are multiplexed together to carry on
   ancillary data packets.
   
   Packet insertion automatically release allocated packet, so
   that the pAncPacket pointer cannot be re-used without any
   call to VHD_AncAllocatePacket.
   
   
   
   When inserting a packet on a line already containing
   ancillary data, the PacketIndex argument let you decide where
   to place the new packet.
   
   It is related to other packets already present in the same
   zone as the packet to insert, that is in HANC space or in
   active part of the line.
   
   
   
   For example, if a line already contains packets A,B and C in
   HANC space, inserting a new HANC packet N at location 1 will
   give the following result : A,N,B,C
   See Also
   VHD_AncAllocatePacket VHD_ANCPACKET                            */
ULONG VIDEOMASTER_HD_API VHD_AncInsertPacket (HANDLE StrmHandle, VHD_SLOT *pSlot, int LineNumber, VHD_SAMPLETYPE AncStream, int PacketIndex, VHD_ANCPACKET *pAncPacket);


/*** VHD_AncRemovePacket *****************************************************/
/*!VHD_AncRemovePacket@HANDLE@VHD_SLOT *@int@VHD_SAMPLETYPE@int@BOOL32
   Summary
   Ancillary data packet removal
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotAncRemovePacket.</color>
   Description
   This function remove the specified ancillary data packet from
   the specified line
   Parameters
   StrmHandle :   Handle of the stream to work on
   pSlot :        Pointer to slot to remove ancillary data packet
                  from
   LineNumber :   Video line number to remove packet from (1..N,
                  with N depending on the standard, or \-1 to
                  remove all packets from slot)
   AncStream :    Specifies if working on Y or C ancillary data
                  stream (in SD, only C stream is used)
   PacketIndex :  Position (zero\-based) of the packet to remove
                  (\-1 to remove all packets from line)
   InHANC :       Specifies if PacketIndex is related to HANC
                  zone or to active line zone. If PacketIndex is
                  \-1, then this argument is ignored and the
                  whole ANC line is erased
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the AncStream argument is ignored, since
   Y and C samples are multiplexed together to carry on
   ancillary data packets
   See Also
   VHD_LockSlot                                                   */

ULONG VIDEOMASTER_HD_API VHD_AncRemovePacket (HANDLE StrmHandle, VHD_SLOT *pSlot, int LineNumber, VHD_SAMPLETYPE AncStream, int PacketIndex, BOOL32 InHANC);




/*_ VBI data buffers navigation functions ___________________________________*/


/*** VHD_VbiGetLine **********************************************************/
/*!VHD_VbiGetLine@HANDLE@VHD_SLOT *@int@VHD_SAMPLETYPE@BYTE **@ULONG *
   Summary
   VBI lines retrieval
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotVbiGetLine.</color>
   Description
   This function retrieves the specified VBI line raw content
   Parameters
   StrmHandle :    Handle of the stream to work on
   pSlot :         Pointer to slot to get VBI line from
   LineNumber :    Video line number to get VBI from (1..N, with
                   N depending on the standard)
   VbiStream :     Specifies if working on Y or C samples stream
                   (in SD, Y and C samples are multiplexed
                   together in C stream)
   ppLineBuffer :  Pointer receiving a pointer to raw VBI line
                   buffer
   pBufferSize :   Pointer receiving the size of the buffer
                   provided to ppLineBuffer
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   When working in SD, the VbiStream argument value must be
   VHD_SAMPLE_C, since Y and C samples are always captured and
   multiplexed together in the VBI buffer
   See Also
   VHD_VBILINE VHD_MAXNB_VBICAPTURELINE                          */

ULONG VIDEOMASTER_HD_API VHD_VbiGetLine (HANDLE StrmHandle, VHD_SLOT *pSlot, int LineNumber, VHD_SAMPLETYPE VbiStream, BYTE **ppLineBuffer, ULONG *pBufferSize);


/*** VHD_VbiInsertLine *******************************************************/
/*!VHD_VbiInsertLine@HANDLE@VHD_SLOT *@int@VHD_SAMPLETYPE@BYTE *@ULONG
   Summary
   VBI lines insertion
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotVbiInsertLine.</color>
   Description
   This function insert the specified VBI line raw content
   Parameters
   StrmHandle :   Handle of the stream to work on
   pSlot :        Pointer to slot to insert VBI line to
   LineNumber :   Video line number to insert VBI on (1..N, with
                  N depending on the standard)
   VbiStream :    Specifies if working on Y or C samples stream
                  (in SD, Y and C samples are multiplexed
                  together in C stream)
   pLineBuffer :  Pointer to raw VBI content to insert to line
   BufferSize :   Size (in bytes) of the pLineBuffer
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Please note that no ancillary data packets will be inserted
   in active video part of the lines configured for VBI
   insertion.
   
   When working in SD, the VbiStream argument is ignored, since
   Y and C samples are always captured and multiplexed together
   in the VBI buffer
   
   
   
   The provided buffer size cannot exceed the active line size,
   depending on the video standard used. If the buffer is
   smaller than line size, then it will be automatically
   completed with idle level samples
   See Also
   VHD_VBILINE VHD_MAXNB_VBICAPTURELINE                          */
ULONG VIDEOMASTER_HD_API VHD_VbiInsertLine (HANDLE StrmHandle, VHD_SLOT *pSlot, int LineNumber, VHD_SAMPLETYPE VbiStream, BYTE *pLineBuffer, ULONG BufferSize);



#ifdef __cplusplus
}
#endif




#endif  //EXCLUDE_API_FUNCTIONS


#endif // _VIDEOMASTERHD_H_
