/*! VideoMasterHD.h

   Copyright (c) 2007, DELTACAST. All rights reserved.

   THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
   KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
   PURPOSE.

   Project : DELTA-hd 

   Package : VideoMasterHD_Audio API

   Company : DELTACAST

   Author  : Gerald THIELEN                              Date: 2007/09/28

   Purpose : Definition of the VideoMasterHD_Audio API

   Copyright 2007 by DELTACAST.  All rights reserved.
   All information and source code contained herein is proprietary and confidential to DELTACAST.
   Any use, reproduction, or disclosure without the written permission of DELTACAST is prohibited.

*/


#ifndef _AUDIOMASTERHD_H
#define _AUDIOMASTERHD_H


#ifdef __cplusplus
extern "C" {
#endif

#ifdef __linux__
#define AUDIOMASTER_HD_API 
#elif defined(__APPLE__)
#define AUDIOMASTER_HD_API
#else
#define AUDIOMASTER_HD_API WINAPI
#endif

#include "VideoMasterHD_Sdi_Audio.h"


/*_ CONSTANTS ________________________________________________________________
//
// This section defines the different constants used by VideoMasterHD_Audio
*/

/*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideoMasterHD_Audio
*/

/*_ STRUCTURES _______________________________________________________________
//
// This section defines the different structures used by VideoMasterHD
*/




/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by AudioMasterHD_Audio
*/


/*** VHD_ExtractAudio ********************************************************/
/*!VHD_ExtractAudio@HANDLE@VHD_SLOT *@VHD_AUDIOINFO *
   Summary
   Audio data extraction
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotExtractAudio.</color>
   Description
   This function extracts audio data.
   Parameters
   StrmHandle :  Handle of the stream to work on
   pSlot :       Pointer to slot to get audio data from
   pAudioInfo :  Pointer to a VHD_AUDIOINFO structure which
                 contains extraction audio configuration on call,
                 and extracted audio data on return.
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   A caller-allocated buffer is needed for each channel to
   capture. In stereo channel mode, channel samples of the same
   AES pair are interleaved in the same buffer (the buffer of
   the left (i.e. the first) channel of the audio pair). If the
   stereo channel mode is used for one channel of an audio pair,
   the ChannelMode and ChannelFormat fields of the two channels
   of this pair should be the same.
   
   
   
   The API computes the required size of this buffer based on
   the audio frame number of the embedded group control packet
   and the programmed video standard and clock divisor. If the
   buffer is smaller that this computed size, VHD_ExtractAudio
   doesn't extract audio and returns the required buffer size.
   Otherwise, VHD_ExtractAudio extracts audio in the buffer and
   returns the used buffer size.
   
   
   
   If the API cannot compute the required size because there is
   no group control packet or because of a "free run sampling
   rate", VHD_ExtractAudio extracts audio until the buffer is
   full.
   
   
   
   If the computed required buffer size is too small to extract
   all embedded samples, VHD_ExtractAudio extracts audio until
   the buffer is full and returns VHDERR_BUFFERTOOSMALL. This
   could happen if the programmed clock divisor doesn't match
   the incoming one, or if the incoming stream is not standard.
   See Also
   VHD_SLOT VHD_AUDIOINFO                                         */
ULONG AUDIOMASTER_HD_API VHD_ExtractAudio (HANDLE StrmHandle, VHD_SLOT *pSlot, VHD_AUDIOINFO *pAudioInfo);


/*** VHD_EmbedAudio ********************************************************/
/*!VHD_EmbedAudio@HANDLE@VHD_SLOT *@VHD_AUDIOINFO *
   Summary
   Audio data embedding
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotEmbedAudio.</color>
   Description
   This function embeds audio data.
   Parameters
   StrmHandle :  Handle of the stream to work on
   pSlot :       Pointer to slot to fill with audio data
   pAudioInfo :  Pointer to a VHD_AUDIOINFO structure which
                 contains embedding audio configuration and audio
                 data.
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   In stereo channel mode, channel samples of the same AES pair
   have to be interleaved in the same buffer (the buffer of the
   left (i.e. the first) channel of the audio pair). If the
   stereo channel mode is used for one channel of an audio pair,
   the ChannelMode and ChannelFormat fields of the two channels
   of this pair should be the same.
   
   
   
   If the audio sample rate of an audio group (SD: audio pair)
   is not an entire multiple of the video frame rate (ex: 48kHz
   audio sampling rate with 29.97Hz video frame rate), the API
   sets automatically the AudioFrameNb (SD: pAudioFrameNb) value
   of this group (SD: pair) on return for the next
   VHD_EmbedAudio call. So, the caller should pay attention if
   it uses an other VHD_AUDIOINFO structure on the next
   VHD_EmbedAudio call.
   
   
   
   48kHz, 44.1kHz and 32kHz are valid sampling rate for HD
   embedding. Only 48kHz is valid for SD embedding.
   
   Freerun is invalid for both HD and SD
   See Also
   VHD_SLOT VHD_AUDIOINFO                                         */
ULONG AUDIOMASTER_HD_API VHD_EmbedAudio (HANDLE StrmHandle, VHD_SLOT *pSlot,  VHD_AUDIOINFO *pAudioInfo);


#ifdef __cplusplus
}
#endif


#endif /* _AUDIOMASTERHD_H */

