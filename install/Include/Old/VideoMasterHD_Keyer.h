/*! VideomasterHD_Keyer.h

    Copyright (c) 2009, DELTACAST. All rights reserved.

    THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
    KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
    PURPOSE.

  Project : VideomasterHD

  Package : 

  Company : DELTACAST

  Author  : gt                             Date: 2009/07/09

  Purpose : 

*/


#ifndef _VIDEOMASTERHD_KEYER_H_
#define _VIDEOMASTERHD_KEYER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "VideoMasterHD_Sdi_Keyer.h"

/*_ CONSTANTS ________________________________________________________________
//
// This section defines the different constants used by VideomasterHD_Keyer
*/


/*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideomasterHD_Keyer
*/


/*_ STRUCTURES _______________________________________________________________
//
// This section defines the different structures used by VideomasterHD_Keyer
*/






#ifndef EXCLUDE_API_FUNCTIONS

/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideomasterHD_Keyer
*/


/*** VHD_SetKeyerProperty ****************************************************/
/*!VHD_SetKeyerProperty@HANDLE@ULONG@ULONG
   Summary
   VideoMasterHD keyer property configuration
   
   <color Red>This function is now deprecated and replaced by
   VHD_SetBoardProperty and dedicated keyer board properties.</color>
   Description
   This function configures the value of the specified keyer
   property
   Parameters
   BrdHandle :  Handle of the board to set keyer property on
   Property :   Property to set, must be a value of the
                VHD_KEYERPROPERTIES enumeration
   Value :      Property value to configure
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_KEYERPROPERTY VHD_GetKeyerProperty                             */
ULONG VIDEOMASTER_HD_API VHD_SetKeyerProperty (HANDLE BrdHandle, ULONG Property, ULONG Value);


/*** VHD_GetKeyerProperty ****************************************************/
/*!VHD_GetKeyerProperty@HANDLE@ULONG@ULONG *
   Summary
   VideoMasterHD keyer property query
   
   <color Red>This function is now deprecated and replaced by
   VHD_GetBoardProperty and dedicated keyer board properties.</color>
   Description
   This function retrieves value of the specified keyer property
   Parameters
   BrdHandle :  Handle of the board to get keyer property from
   Property :   Property to get, must be a value of the
                VHD_KEYERPROPERTIES enumeration
   pValue :     Pointer to caller\-allocated variable to return
                property value
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_KEYERPROPERTY VHD_SetKeyerProperty                             */
ULONG VIDEOMASTER_HD_API VHD_GetKeyerProperty (HANDLE BrdHandle, ULONG Property, ULONG *pValue);

#endif

#ifdef __cplusplus
}
#endif

#endif // _VIDEOMASTERHD_KEYER_H_
