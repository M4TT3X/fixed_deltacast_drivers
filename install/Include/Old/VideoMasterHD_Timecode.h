/*! VideomasterHD_Timecode.h

    Copyright (c) 2009, DELTACAST. All rights reserved.

    THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
    KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
    PURPOSE.

  Project : VideomasterHD

  Package : 

  Company : DELTACAST

  Author  : gt                             Date: 2009/07/09

  Purpose : 

*/


#ifndef _VIDEOMASTERHD_TIMECODE_H_
#define _VIDEOMASTERHD_TIMECODE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "VideoMasterHD_Sdi_Timecode.h"

/*_ CONSTANTS ________________________________________________________________
//
// This section defines the different constants used by VideomasterHD_Timecode
*/


/*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideomasterHD_Timecode
*/


/*_ STRUCTURES _______________________________________________________________
//
// This section defines the different structures used by VideomasterHD_Timecode
*/





#ifndef EXCLUDE_API_FUNCTIONS

/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideomasterHD_Timecode
*/


/*** VHD_ExtractTimecode *****************************************************/
/*!VHD_ExtractTimecode@HANDLE@VHD_SLOT *@VHD_TIMECODESLOT *
   Summary
   Timecode extraction
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotExtractTimecode.</color>
   Description
   This function looks for timecode in the provided slot.
   
   The slot must be of type VHD_STPROC_JOINED or
   VHD_STPROC_DISJOINED_ANC.
   
   
   
   The VHD_TIMECODESLOT structure exchanged with the caller is
   used both to specify the timecode extraction mode, and to
   return the extracted timecode value
   Parameters
   StrmHandle :  Handle of the stream to operate on
   pSlot :       Pointer to the slot to look for timecode in
   pTimecode :   Pointer to a caller\-allocated structure
                 specifying the timecode extraction mode, and
                 used to give back the extracted timecode to the
                 caller
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Timecode may be embedded in the SDI signal according to two
   standards :
   
     * SMPTE RP 188 defines a transmission format for conveyance
       of linear (LTC) or vertical interval (VITC) timecode in the
       ancillary data space of a SDI signal. It specifies ANC
       timecode (ATC) embedding for both HD and SD. When extracting
       in this mode, VideoMasterHD may be configured to look for
       specified ATC (LTC, VITC1 or VITC2) on one line, two lines,
       or on every line until found. This configuration is achieved
       using the pTcLine field of the timecode structure
     * SMPTE 266M describes the signal format of a digital
       vertical interval timecode (D&#45;VITC). It specifies VBI
       timecode embedding for SD signals only. When extracting in
       this mode, first use VHD_VbiSetCaptureLines to configure
       VideoMasterHD to capture relevant VBI lines. Then,
       VideoMasterHD may be configured to look for D&#45;VITC on one
       line, two lines, or on every line until found. This
       configuration is achieved using the pTcLine field of the
       timecode structure
   
   Once a slot is locked, this function may be called several
   times, in order to look for different timecode types as an
   \example
   See Also
   VHD_LockSlot VHD_VbiSetCaptureLines VHD_TIMECODESLOT              */
ULONG VIDEOMASTER_HD_API VHD_ExtractTimecode (HANDLE StrmHandle, VHD_SLOT *pSlot, VHD_TIMECODESLOT *pTimecode);


/*** VHD_EmbedTimecode *******************************************************/
/*!VHD_EmbedTimecode@HANDLE@VHD_SLOT *@VHD_TIMECODESLOT
   Summary
   Timecode embedding
   
   <color Red>This function is now deprecated and replaced by
   VHD_SlotEmbedTimecode.</color>
   Description
   This function embeds the provided timecode in the provided
   slot, according to the specified standard. The slot must be
   of type VHD_STPROC_JOINED or VHD_STPROC_DISJOINED_ANC.
   Parameters
   StrmHandle :  Handle of the stream to operate on
   pSlot :       Pointer to the slot to embed the timecode in
   Timecode :    Structure specifying the timecode to embed and the
                 embedding mode
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Timecode may be embedded in the SDI signal according to two
   standards :
   
     * SMPTE RP 188 defines a transmission format for conveyance
       of linear (LTC) or vertical interval (VITC) timecode in the
       ancillary data space of a SDI signal. It specifies ANC
       timecode (ATC) embedding for both HD and SD. When embedding
       in this mode, VideoMasterHD inserts ATC packet on the line
       specified by the pTcLine[0] field of the timecode structure.
       If this value is set to zero, then default location is
       choosen, according to the video standard used
     * SMPTE 266M describes the signal format of a digital
       vertical interval timecode (D&#45;VITC). It specifies VBI
       timecode embedding for SD signals only. When embedding in
       this mode, VideoMasterHD inserts D&#45;VITC packet on the two
       lines defined by the pTcLine field of the timecode structure.
       If this field is set to zero, then default location is
       choosen according to the video standard used
   
   Once a slot is locked, this function may be called several
   times, in order to insert different timecode types as an
   \example
   See Also
   VHD_LockSlot VHD_TIMECODESLOT                                     */
ULONG VIDEOMASTER_HD_API VHD_EmbedTimecode (HANDLE StrmHandle, VHD_SLOT *pSlot, VHD_TIMECODESLOT Timecode);


#endif

#ifdef __cplusplus
}
#endif

#endif // _VIDEOMASTERHD_TIMECODE_H_
