/*! VideoMasterHD_Vbi.h

   Copyright (c) 2007, DELTACAST. All rights reserved.

   THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
   KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
   PURPOSE.

   Project : VideoMasterHD

   Package : VBI Extension API

   Company : DELTACAST

   Author  : Olivier ANTOINE                                Date: 2009/01/27

   Purpose : Definition of the VideoMasterHD_Vbi API

   Copyright 2007 by DELTACAST.  All rights reserved.
   All information and source code contained herein is proprietary and confidential to DELTACAST.
   Any use, reproduction, or disclosure without the written permission of DELTACAST is prohibited.

*/


#ifndef _VIDEOMASTERHD_VBI_H
#define _VIDEOMASTERHD_VBI_H


#ifdef __cplusplus
extern "C" {
#endif

#ifndef WIN32
#define VIDEOMASTERHD_VBI_API 
#else
#define VIDEOMASTERHD_VBI_API WINAPI
#endif

#include "VideoMasterHD_Sdi_Vbi.h"


   

/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideoMasterHD_Vbi
*/



#ifdef __cplusplus
}
#endif


#endif /* _VIDEOMASTERHD_VBI_H */

