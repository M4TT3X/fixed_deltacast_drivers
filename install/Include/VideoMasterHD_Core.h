/*! VideomasterHD_Core.h

    Copyright (c) 2009, DELTACAST. All rights reserved.

    THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
    KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
    PURPOSE.

  Project : VideomasterHD

  Package : 

  Company : DELTACAST

  Author  : gt                             Date: 2009/06/26

  Purpose : 

*/


#ifndef _VIDEOMASTERHD_CORE_H_
#define _VIDEOMASTERHD_CORE_H_


#if defined (__linux__) || defined (__APPLE__)
# define VIDEOMASTER_HD_API 
# include <stdint.h>
#else
# define VIDEOMASTER_HD_API WINAPI
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef INFINITE
#define INFINITE 0xFFFFFFFF
#endif

typedef signed short int SHORT;
typedef unsigned short int USHORT;
typedef void* HANDLE;
typedef unsigned char BYTE;
typedef unsigned char UBYTE;
typedef void* LPVOID;

#if defined(__linux__)
typedef long long LONGLONG;
typedef int BOOL;
typedef BOOL BOOL32;
typedef unsigned short int WORD;
typedef uint32_t ULONG;
#elif defined(__APPLE__)
#include <libkern/OSTypes.h>
typedef SInt64 LONGLONG;
typedef SInt32 BOOL32;
typedef UInt16 WORD;
typedef UInt32 ULONG;
#else
typedef __int64 LONGLONG;
typedef int BOOL;
typedef BOOL BOOL32;
typedef unsigned short int WORD;
typedef unsigned long ULONG;
#endif

/*_ CONSTANTS ________________________________________________________________
//
// This section defines the different constants used by VideomasterHD_Core
*/
#define ENUMBASE_MASK   0xFF000000
#define ENUMBASE_CORE   0x01000000
#define ENUMBASE_SDI    0x02000000
#define ENUMBASE_KEYER  0x03000000
#define ENUMBASE_ASI    0x04000000
#define ENUMBASE_DVI    0x10000000
#define ENUMBASE_SFP    0x20000000



/*_ VideomasterHD_Core STATUS INFORMATION BIT MASKS ___________________________________*/

   /*! RX channel status bit masks. These constants define status bit masks for the VHD_CORE_BP_RXx_STATUS board properties */
#define VHD_CORE_RXSTS_OVERRUN     0x00000001  /*! RX channel overrun indicator. This bit is set to '1' when the software cannot sustain the incoming data rate */
#define VHD_CORE_RXSTS_UNLOCKED    0x00000002  /*! RX channel unlocked indicator. This bit is set to '1' when the channel cannot lock on incoming signal */

   /*! TX channel status bit masks. These constants define status bit masks for the VHD_CORE_BP_TXx_STATUS board properties */
#define VHD_CORE_TXSTS_UNDERRUN    0x00000001  /*! TX channel underrun indicator. This bit is set to '1' when the software cannot sustain the incoming data rate */


/*_ VideomasterHD_Core EVENT SOURCES __________________________________________________*/

   /*! Event sources bit masks. These constants define event source bit masks for the StateChangeMask parameter of VHD_OpenBoardHandle */
#define VHD_CORE_EVTSRC_RX0        0x00000001  /*! RX0 channel state changes event source */
#define VHD_CORE_EVTSRC_RX1        0x00000002  /*! RX1 channel state changes event source */
#define VHD_CORE_EVTSRC_RX2        0x00000004  /*! RX2 channel state changes event source */
#define VHD_CORE_EVTSRC_RX3        0x00000008  /*! RX3 channel state changes event source */
#define VHD_CORE_EVTSRC_RX4        0x00000010  /*! RX4 channel state changes event source */
#define VHD_CORE_EVTSRC_RX5        0x00000020  /*! RX5 channel state changes event source */
#define VHD_CORE_EVTSRC_RX6        0x00000040  /*! RX6 channel state changes event source */
#define VHD_CORE_EVTSRC_RX7        0x00000080  /*! RX7 channel state changes event source */
#define VHD_CORE_EVTSRC_TX0        0x00000100  /*! TX0 channel state changes event source */
#define VHD_CORE_EVTSRC_TX1        0x00000200  /*! TX1 channel state changes event source */
#define VHD_CORE_EVTSRC_TX2        0x00000400  /*! TX2 channel state changes event source */
#define VHD_CORE_EVTSRC_TX3        0x00000800  /*! TX3 channel state changes event source */
#define VHD_CORE_EVTSRC_WATCHDOG   0x40000000  /*! Watchdog timer expired (by-pass relays loop re-established) */


/*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideomasterHD_Core
*/

/*_ VHD_BOARDTYPE ____________________________________________________*/
/*!
   Summary
   VideomasterHD board types
   Description
   The VHD_BOARDTYPE enumeration lists all the supported
   DELTACAST board types.
   
   These values are used by the VHD_CORE_BP_BOARD_TYPE board
   property
   See Also
   VHD_GetBoardProperty                                      */

typedef enum _VHD_BOARDTYPE
{
   VHD_BOARDTYPE_HD = 0,         /*! DELTA-hd board */
   VHD_BOARDTYPE_HDKEY,          /*!_VHD_BOARDTYPE::VHD_BOARDTYPE_HDKEY
                                    DELTA-key board */
   VHD_BOARDTYPE_SD,             /*!_VHD_BOARDTYPE::VHD_BOARDTYPE_SD
                                    DELTA-sdi board */
   VHD_BOARDTYPE_SDKEY,          /*!_VHD_BOARDTYPE::VHD_BOARDTYPE_SDKEY
                                    Future use */
   VHD_BOARDTYPE_DVI,            /*! DELTA-dvi board */
   VHD_BOARDTYPE_CODEC,          /*! DELTA-codec board */
   VHD_BOARDTYPE_3G,             /*! DELTA-3G board */
   VHD_BOARDTYPE_3GKEY,          /*! DELTA-key 3G board */
   VHD_BOARDTYPE_HDMI,           /*! DELTA-h4k board */
   VHD_BOARDTYPE_FLEX,           /*! DELTA-flex board */
   VHD_BOARDTYPE_ASI,           /*! DELTA-asi board */
   NB_VHD_BOARDTYPES
} VHD_BOARDTYPE;



/*_ VHD_CORE_BOARDPROPERTY _______________________________________________*/
/*!
   Summary
   VideomasterHD core board properties
   Description
   The VHD_CORE_BOARDPROPERTY enumeration lists all the
   available board properties common to all supported hardware
   families.
   
   These values are used as indexes for VHD_GetBoardProperty and
   VHD_SetBoardProperty functions calls.
   See Also
   VHD_GetBoardProperty VHD_SetBoardProperty                     */
typedef enum _VHD_CORE_BOARDPROPERTY
{
   /* Software board handling */
   VHD_CORE_BP_DRIVER_VERSION = ENUMBASE_CORE,        /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_DRIVER_VERSION
                                                         Driver version, as a 32-bit unsigned integer of form
                                                         0xMMmmBBBB                                                  */
   VHD_CORE_BP_REF_COUNT,                             /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_REF_COUNT
                                                         Opened board handles counter                                */
   VHD_CORE_BP_SETUP_LOCK,                            /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_SETUP_LOCK
                                                         Board setup lock (default is FALSE)                         */
   VHD_CORE_BP_STATE_CHANGE_EVENT_MASK,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_STATE_CHANGE_EVENT_MASK
                                                         Future use                                                  */

   /* Board information */
   VHD_CORE_BP_BOARD_TYPE,                            /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_BOARD_TYPE
                                                         Deltacast board family type                                 */
   VHD_CORE_BP_FIRMWARE_VERSION,                      /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_FIRMWARE_VERSION
                                                         FPGA version, if any                                        */
   VHD_CORE_BP_FIRMWARE2_VERSION,                     /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_FIRMWARE2_VERSION
                                                         CPLD version, if any                                        */
   VHD_CORE_BP_FIRMWARE3_VERSION,                     /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_FIRMWARE3_VERSION
                                                         Micro-controller version, if any                            */
   VHD_CORE_BP_SERIALNUMBER_LSW,                      /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_SERIALNUMBER_LSW
                                                         Silicon serial number (32 least significant bits)           */
   VHD_CORE_BP_SERIALNUMBER_MSW,                      /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_SERIALNUMBER_MSW
                                                         Silicon serial number (32 most significant bits)            */
   VHD_CORE_BP_SERIALNUMBER_EX,                       /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_SERIALNUMBER_EX
                                                         Silicon serial number extension (DELTA-dvi only)            */
   VHD_CORE_BP_NBOF_LANE,                             /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_NBOF_LANE
                                                         Number of negotiated PCI express lanes. This property is only
                                                         available for PCI express cards.                            */
   VHD_CORE_BP_LOWPROFILE,                            /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_LOWPROFILE
                                                         TRUE if the board is of low-profile form factor, FALSE if it
                                                         is of full PCI height form factor                           */
   VHD_CORE_BP_NB_RXCHANNELS,                         /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_NB_RXCHANNELS
                                                         Number of reception channels */
   VHD_CORE_BP_NB_TXCHANNELS,                         /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_NB_TXCHANNELS
                                                         Number of transmission channels */

   /* Board configuration */
   VHD_CORE_BP_BYPASS_RELAY_0,                        /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_BYPASS_RELAY_0
                                                         RX0-TX0 by-pass relay control (TRUE = loopthrough enabled,
                                                         FALSE = relay disabled), default is TRUE.                    */
   VHD_CORE_BP_BYPASS_RELAY_1,                        /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_BYPASS_RELAY_1
                                                         RX1-TX1 by-pass relay control (TRUE = loopthrough enabled,
                                                         FALSE = relay disabled), default is TRUE.                    */
   VHD_CORE_BP_WATCHDOG_TIMER,                        /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_WATCHDOG_TIMER
                                                         By-pass relays watchdog timer value (in msec, max 30000
                                                         msec). Watchdog re-establish by-pass relays loop-through if
                                                         not re-armed using VHD_RearmWatchdog every period configured
                                                         by this value. Set value to 0 to disable watchdog (default)  */

   /* Channel status */
   VHD_CORE_BP_CHN_AVAILABILITY,          /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_CHN_AVAILABILITY
                                             Physical channel availability mask (bit0 = RX0, bit1 = RX1, bit2 = RX2, bit3 = RX3, bit4 = TX0, bit5 = TX1, bit6 = TX2, bit7 = TX3, bit8 = RX4, bit9 = RX5, bit10 = RX6, bit11 = RX7, bit12 = TX4, bit13 = TX5, bit14 = TX6, bit15 = TX7). A channel is available if the corresponding mask bit is set to 1 */  
   VHD_CORE_BP_RX0_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX0_STATUS
                                             RX0 channel status (see VHD_xx_RXSTS_xxx) */
   VHD_CORE_BP_RX1_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX1_STATUS
                                             RX1 channel status (see VHD_xx_RXSTS_xxx) */
   VHD_CORE_BP_RX2_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX2_STATUS
                                             RX2 channel status (see VHD_xx_RXSTS_xxx) */
   VHD_CORE_BP_RX3_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX3_STATUS
                                             RX3 channel status (see VHD_xx_RXSTS_xxx) */
   VHD_CORE_BP_TX0_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX0_STATUS
                                             TX0 channel status (see VHD_xx_TXSTS_xxx) */
   VHD_CORE_BP_TX1_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX1_STATUS
                                             TX1 channel status (see VHD_xx_TXSTS_xxx) */
   VHD_CORE_BP_TX2_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX2_STATUS
                                             TX2 channel status (see VHD_xx_TXSTS_xxx) */
   VHD_CORE_BP_TX3_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX3_STATUS
                                             TX3 channel status (see VHD_xx_TXSTS_xxx) */
   
   /* Channel type */
   VHD_CORE_BP_RX0_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX0_TYPE
                                          RX0 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_RX1_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX1_TYPE
                                          RX1 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_RX2_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX2_TYPE
                                          RX2 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_RX3_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX3_TYPE
                                          RX3 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_TX0_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX0_TYPE
                                          TX0 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_TX1_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX1_TYPE
                                          TX1 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_TX2_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX2_TYPE
                                          TX2 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_TX3_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX3_TYPE
                                          TX3 channel type (see VHD_CHANNELTYPE) */
 
   /* Preallocation size */
   VHD_CORE_BP_PREALLOCPOOL0SIZE,      /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_PREALLOCPOOL0SIZE
                                          Preallocated memory Pool 0 size, in bytes, or zero if the
                                          preallocated memory pool does not exist                   */
   VHD_CORE_BP_PREALLOCPOOL1SIZE,      /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_PREALLOCPOOL1SIZE
                                          Preallocated memory Pool 1 size, in bytes, or zero if the
                                          preallocated memory pool does not exist                   */
   VHD_CORE_BP_PREALLOCPOOL2SIZE,      /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_PREALLOCPOOL2SIZE
                                          Preallocated memory Pool 2 size, in bytes, or zero if the
                                          preallocated memory pool does not exist                   */
   VHD_CORE_BP_PREALLOCPOOL3SIZE,      /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_PREALLOCPOOL3SIZE
                                          Preallocated memory Pool 3 size, in bytes, or zero if the
                                          preallocated memory pool does not exist                   */
   VHD_CORE_BP_RX4_STATUS,             /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX4_STATUS
                                          RX0 channel status (see VHD_xx_RXSTS_xxx) */
   VHD_CORE_BP_RX5_STATUS,             /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX5_STATUS
                                          RX1 channel status (see VHD_xx_RXSTS_xxx) */
   VHD_CORE_BP_RX6_STATUS,             /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX6_STATUS
                                          RX2 channel status (see VHD_xx_RXSTS_xxx) */
   VHD_CORE_BP_RX7_STATUS,             /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX7_STATUS
                                          RX3 channel status (see VHD_xx_RXSTS_xxx) */
   VHD_CORE_BP_RX4_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX4_TYPE
                                          RX0 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_RX5_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX5_TYPE
                                          RX1 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_RX6_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX6_TYPE
                                          RX2 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_RX7_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_RX7_TYPE
                                          RX3 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_BUS_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_BUS_TYPE
                                          Bus type (see VHD_BUSTYPE)*/
   VHD_CORE_BP_TEMPERATURE,            /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TEMPERATURE
                                          Current FPGA temperature in Celcius degrees */
   VHD_CORE_BP_BYPASS_RELAY_2,         /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_BYPASS_RELAY_2
                                          RX2-TX2 by-pass relay control (TRUE = loopthrough enabled,
                                          FALSE = relay disabled), default is TRUE.                    */
   VHD_CORE_BP_BYPASS_RELAY_3,         /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_BYPASS_RELAY_3
                                          RX3-TX3 by-pass relay control (TRUE = loopthrough enabled,
                                          FALSE = relay disabled), default is TRUE.                    */
   VHD_CORE_BP_TX4_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX4_STATUS
                                             TX4 channel status (see VHD_xx_TXSTS_xxx) */
   VHD_CORE_BP_TX5_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX5_STATUS
                                             TX5 channel status (see VHD_xx_TXSTS_xxx) */
   VHD_CORE_BP_TX6_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX6_STATUS
                                             TX6 channel status (see VHD_xx_TXSTS_xxx) */
   VHD_CORE_BP_TX7_STATUS,                /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX7_STATUS
                                             TX7 channel status (see VHD_xx_TXSTS_xxx) */
   VHD_CORE_BP_TX4_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX4_TYPE
                                          TX4 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_TX5_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX5_TYPE
                                          TX5 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_TX6_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX6_TYPE
                                          TX6 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_TX7_TYPE,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_TX7_TYPE
                                          TX7 channel type (see VHD_CHANNELTYPE) */
   VHD_CORE_BP_IS_BIDIR,               /*!_VHD_CORE_BOARDPROPERTY::VHD_CORE_BP_IS_BIDIR
                                          TRUE if the card is bidirectionnel */
   NB_VHD_CORE_BOARDPROPERTIES
} VHD_CORE_BOARDPROPERTY;

/*_ VHD_CORE_STREAMPROPERTY ______________________________________________*/
/*!
   Summary
   VideoMasterHD core streams properties
   Description
   The VHD_CORE_STREAMPROPERTY enumeration lists all the
   available stream properties common to all supported hardware
   families.
   
   These values are used as indexes for VHD_GetStreamProperty
   and VHD_SetStreamProperty functions calls.
   See Also
   VHD_GetStreamProperty VHD_SetStreamProperty                  */
typedef enum _VHD_CORE_STREAMPROPERTY
{
   VHD_CORE_SP_REF_COUNT=ENUMBASE_CORE,   /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_REF_COUNT
                                             Opened stream handles counter                                        */
   VHD_CORE_SP_FORCE_EXCLUSIVE,           /*! _VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_FORCE_EXCLUSIVE
                                             Force exclusive access to this stream : no other handle may
                                             be created on the same stream type on this board (default is FALSE)  */
   VHD_CORE_SP_TRANSFER_SCHEME,           /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_TRANSFER_SCHEME
                                             Stream transfers handling scheme (see VHD_TRANSFERSCHEME).
                                             Affect reception stream only (default is VHD_TRANSFER_UNCONSTRAINED )        */
   VHD_CORE_SP_IO_TIMEOUT,                /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_IO_TIMEOUT
                                             Slot locking time-out value, in milliseconds (default is 100
                                             msec)                                                                */
   VHD_CORE_SP_TX_OUTPUT,                  /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_TX_OUTPUT
                                             Transmission stream output control (see VHD_OUTPUTMODE)
                                             , default is VHD_OUTPUT_STREAM                                       */
   VHD_CORE_SP_SLOTS_COUNT,               /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_SLOTS_COUNT
                                             Counts the number of slots transferred since the stream has
                                             been started                                                         */
   VHD_CORE_SP_SLOTS_DROPPED,             /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_SLOTS_DROPPED
                                             Counts the number of dropped slots since the stream has been
                                             started                                                              */
   VHD_CORE_SP_BUFFERQUEUE_DEPTH,         /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_BUFFERQUEUE_DEPTH
                                             Stream driver buffers queue depth, in number of slots
                                             (minimum is 2, maximum is 32, default is 4)                          */
   VHD_CORE_SP_BUFFERQUEUE_FILLING,       /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_BUFFERQUEUE_FILLING
                                             Current filling level of stream driver buffer queue, in
                                             number of slots                                                      */
   VHD_CORE_SP_BUFFERQUEUE_PRELOAD,       /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_BUFFERQUEUE_PRELOAD
                                             Number of TX buffer to preload before actual channel start.
                                             May be zero (default) to disable TX preload, or must be
                                             comprised between 1 and VHD_CORE_SP_BUFFERQUEUE_DEPTH + 1            */
   VHD_CORE_SP_BUFFER_PACKING,            /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_BUFFER_PACKING
                                             Stream buffers data packing (see VHD_BUFFERPACKING)
                                             , default is VHD_BUFPACK_VIDEO_YUV422_8                              */
   VHD_CORE_SP_DELAY,                     /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_DELAY
                                             Number of frames to be delayed (hardware delay line,
                                             DELTA-key only), minimum is 1, maximum is 7, default is 1            */
   VHD_CORE_SP_MUTED_DATA_MASK,           /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_MUTED_DATA_MASK
                                             On a RX stream, this property defines the data that shouldn't be 
                                             temporarily captured.                                                */
   VHD_CORE_SP_FIELD_MODE,                /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_FIELD_MODE
                                             Switch between frame and field mode for interlaced format
                                             , default is FALSE (frame mode)                                      */
   VHD_CORE_SP_FIELD_MERGE,               /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_FIELD_MERGE
                                             Activate field merging for interlaced stream (default is FALSE)       */
   VHD_CORE_SP_LINE_PADDING,              /*!_VHD_CORE_STREAMPROPERTY::VHD_CORE_SP_LINE_PADDING
                                             Activate 64 or 128 bytes line padding (default is 0)                 */
   NB_VHD_CORE_STREAMPROPERTIES   
} VHD_CORE_STREAMPROPERTY;


/*_ VHD_STREAMTYPE ___________________________________________________*/
/*!
   Summary
   VideoMasterHD stream types
   Description
   The VHD_STREAMTYPE enumeration lists all the stream types.
   
   These values are used during VHD_OpenStreamHandle function
   calls.
   See Also
   VHD_OpenStreamHandle                                       */

typedef enum _VHD_STREAMTYPE
{
   VHD_ST_RX0=0,                    /*!_VHD_STREAMTYPE::VHD_ST_RX0
                                       Reception stream on RX0 physical channel */
   VHD_ST_RX1=1,                    /*!_VHD_STREAMTYPE::VHD_ST_RX1
                                       Reception stream on RX1 physical channel */
   VHD_ST_RX2=9,                    /*!_VHD_STREAMTYPE::VHD_ST_RX2
                                       Reception stream on RX2 physical channel */
   VHD_ST_RX3=10,                   /*!_VHD_STREAMTYPE::VHD_ST_RX3
                                       Reception stream on RX3 physical channel */
   VHD_ST_TX0=2,                    /*!_VHD_STREAMTYPE::VHD_ST_TX0
                                       Transmission stream on TX0 physical channel, or TX0 PC stream
                                       feeding hardware keyer                                        */
   VHD_ST_TX1=3,                    /*!_VHD_STREAMTYPE::VHD_ST_TX1
                                       Transmission stream on TX1 physical channel, or TX1 PC stream
                                       feeding hardware keyer                                        */
   VHD_ST_TX2=11,                   /*!_VHD_STREAMTYPE::VHD_ST_TX2
                                       Transmission stream on TX2 physical channel */
   VHD_ST_TX3=12,                   /*!_VHD_STREAMTYPE::VHD_ST_TX3
                                       Transmission stream on TX3 physical channel */ 
   VHD_ST_RX0_MODIFY_TX0=6,         /*!_VHD_STREAMTYPE::VHD_ST_RX0_MODIFY_TX0
                                       RX-modify-TX stream coupling RX0 to TX0 physical channels */
   VHD_ST_RX1_MODIFY_TX1=7,         /*!_VHD_STREAMTYPE::VHD_ST_RX1_MODIFY_TX1
                                       RX-modify-TX stream coupling RX1 to TX1 physical channels */

   VHD_ST_RX0_HW_PROCESS_TX0=13,    /*!_VHD_STREAMTYPE::VHD_ST_RX0_HW_PROCESS_TX0
                                       Process on RX0/TX0, ex: Delay Line */
   VHD_ST_RX1_HW_PROCESS_TX1=14,    /*!_VHD_STREAMTYPE::VHD_ST_RX1_HW_PROCESS_TX1
                                          Process on RX1/TX1, ex: Delay Line */
   VHD_ST_COUPLED_RX01=15,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_RX01
                                       Coupled reception on RX0 and RX1 physical channel */
   VHD_ST_COUPLED_RX23=16,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_RX23
                                       Coupled reception on RX2 and RX3 physical channel */
   VHD_ST_COUPLED_TX01=17,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_TX01
                                       Coupled transmission on TX0 and TX1 physical channel */
   VHD_ST_COUPLED_TX23=18,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_TX23
                                       Coupled transmission on TX2 and TX3 physical channel */
   VHD_ST_COUPLED_TX02=19,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_TX02
                                       Coupled transmission on TX0 and TX2 physical channel */
   VHD_ST_RX4=20,                   /*!_VHD_STREAMTYPE::VHD_ST_RX4
                                       Reception stream on RX4 physical channel */
   VHD_ST_RX5=21,                   /*!_VHD_STREAMTYPE::VHD_ST_RX5
                                       Reception stream on RX5 physical channel */
   VHD_ST_RX6=22,                   /*!_VHD_STREAMTYPE::VHD_ST_RX6
                                       Reception stream on RX6 physical channel */
   VHD_ST_RX7=23,                   /*!_VHD_STREAMTYPE::VHD_ST_RX7
                                       Reception stream on RX7 physical channel */
   VHD_ST_COUPLED_RX45=24,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_RX45
                                       Coupled reception on RX4 and RX5 physical channel */
   VHD_ST_COUPLED_RX67=25,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_RX67
                                       Coupled reception on RX6 and RX7 physical channel */
   VHD_ST_COUPLED_RX0123=26,        /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_RX0123
                                       Coupled reception on RX0, RX1, RX2 and RX3 physical channel */
   VHD_ST_COUPLED_TX0123=27,        /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_TX0123
                                       Coupled reception on TX0, TX1, TX2 and TX3 physical channel */
   VHD_ST_COUPLED_RX4567=28,        /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_RX4567
                                       Coupled reception on RX4, RX5, RX6 and RX7 physical channel */
   VHD_ST_RX2_MODIFY_TX2=29,        /*!_VHD_STREAMTYPE::VHD_ST_RX2_MODIFY_TX2
                                       RX-modify-TX stream coupling RX2 to TX2 physical channels */
   VHD_ST_RX3_MODIFY_TX3=30,        /*!_VHD_STREAMTYPE::VHD_ST_RX3_MODIFY_TX3
                                       RX-modify-TX stream coupling RX3 to TX3 physical channels */
   VHD_ST_COUPLED_RX02=31,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_RX02
                                       Coupled reception on RX0 and RX2 physical channel */
   VHD_ST_COUPLED_RX46=32,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_RX46
                                       Coupled reception on RX4 and RX6 physical channel */
   VHD_ST_TX4=33,                   /*!_VHD_STREAMTYPE::VHD_ST_TX4
                                       Transmission stream on TX4 physical channel */ 
   VHD_ST_TX5=34,                   /*!_VHD_STREAMTYPE::VHD_ST_TX5
                                       Transmission stream on TX5 physical channel */ 
   VHD_ST_TX6=35,                   /*!_VHD_STREAMTYPE::VHD_ST_TX6
                                       Transmission stream on TX6 physical channel */ 
   VHD_ST_TX7=36,                   /*!_VHD_STREAMTYPE::VHD_ST_TX7
                                       Transmission stream on TX7 physical channel */ 
   VHD_ST_COUPLED_TX4567=37,        /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_TX4567
                                       Coupled reception on TX4, TX5, TX6 and TX7 physical channel */
   VHD_ST_COUPLED_TX45=38,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_TX45
                                       Coupled reception on TX4 and TX5 physical channel */
   VHD_ST_COUPLED_TX67=39,          /*!_VHD_STREAMTYPE::VHD_ST_COUPLED_TX67
                                       Coupled reception on TX6 and TX7 physical channel */
   NB_VHD_STREAMTYPES=40
} VHD_STREAMTYPE;

/* Compatibility define to match with old name */
#define VHD_ST_COUPLED_RX0RX1    VHD_ST_COUPLED_RX01
#define VHD_ST_COUPLED_RX2RX3    VHD_ST_COUPLED_RX23
#define VHD_ST_COUPLED_TX0TX1    VHD_ST_COUPLED_TX01
#define VHD_ST_COUPLED_TX2TX3    VHD_ST_COUPLED_TX23
#define VHD_ST_COUPLED_TX0TX2    VHD_ST_COUPLED_TX02
#define VHD_ST_COUPLED_RX4RX5    VHD_ST_COUPLED_RX45
#define VHD_ST_COUPLED_RX6RX7    VHD_ST_COUPLED_RX67


/*_ VHD_CHANNELTYPE _______________________________________________*/
/*!
   Summary
   Physical channel type
   Description
   The VHD_CHANNELTYPE enumeration lists all the available
   channel type
   
   These values are used in VHD_CORE_BP_RXx_TYPE and
   VHD_CORE_BP_TXx_TYPE board properties.
   See Also
   <link VHD_CORE_BOARDPROPERTY, VHD_CORE_BP_xxx_TYPE>     */

typedef enum _VHD_CHANNELTYPE
{
   VHD_CHNTYPE_DISABLE = 0,            /*!_VHD_CHANNELTYPE::VHD_CHNTYPE_DISABLE
                                          Channel not present in the board layout, corresponding
                                          functionalities are disabled                           */
   VHD_CHNTYPE_SDSDI,                  /*! SD-SDI channel */
   VHD_CHNTYPE_HDSDI,                  /*! HD-SDI channel */
   VHD_CHNTYPE_3GSDI,                  /*! 3G-SDI channel */
   VHD_CHNTYPE_DVI,                    /*! DVI channel */
   VHD_CHNTYPE_ASI,                    /*! ASI channel */
   VHD_CHNTYPE_HDMI,                   /*! HDMI channel */
   NB_VHD_CHANNELTYPE
} VHD_CHANNELTYPE;


/*_ VHD_BUSTYPE _______________________________________________*/
/*!
   Summary
   Bus type
   Description
   The VHD_BUSTYPE enumeration lists all the available
   bus type
   
   See Also
   <link VHD_CORE_BOARDPROPERTY, VHD_CORE_BP_BUS_TYPE>     */

typedef enum _VHD_BUSTYPE
{
   VHD_BUSTYPE_PCI = 0,               /*! PCI bus */
   VHD_BUSTYPE_PCIe,                  /*! PCIe bus */
   VHD_BUSTYPE_PCIe_gen2,             /*! PCIe gen2 bus */
   NB_VHD_BUSTYPE
} VHD_BUSTYPE;

/*_ VHD_TRANSFERSCHEME _______________________________________________*/
/*!
   Summary
   VideoMasterHD streams data transfers schemes
   Description
   The VHD_TRANSFERSCHEME enumeration lists all the available
   stream transfer schemes
   
   These values are used in VHD_CORE_SP_TRANSFER_SCHEME stream
   property.
   See Also
   <link VHD_CORE_STREAMPROPERTY, VHD_CORE_SP_TRANSFER_SCHEME> */

typedef enum _VHD_TRANSFERSCHEME
{
   VHD_TRANSFER_UNCONSTRAINED = 0,  /*!_VHD_TRANSFERSCHEME::VHD_TRANSFER_UNCONSTRAINED
                                       Unconstrained data transfers (default). Reception stream
                                       always provides last captured slot. No effect in transmission */
   VHD_TRANSFER_SLAVED,             /*!_VHD_TRANSFERSCHEME::VHD_TRANSFER_SLAVED
                                       Slaved data transfers. Reception stream always provides
                                       oldest captured slot. No effect in transmission         */
   VHD_TRANSFER_FRAMEACCURATE,      /*!_VHD_TRANSFERSCHEME::VHD_TRANSFER_FRAMEACCURATE
                                       Future use */
   NB_VHD_TRANSFERSCHEMES
} VHD_TRANSFERSCHEME;



/*_ VHD_BUFFERPACKING __________________________________________*/
/*!
Summary
VideoMasterHD streams buffer packing schemes
Description
The VHD_BUFFERPACKING enumeration lists all the available
buffer packing schemes

These values are used in VHD_CORE_SP_BUFFER_PACKING stream
property.
See Also
<link VHD_CORE_STREAMPROPERTY, VHD_CORE_SP_BUFFER_PACKING>
*/

typedef enum _VHD_BUFFERPACKING
{
   VHD_BUFPACK_VIDEO_YUV422_8 = 0,              /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_YUV422_8
                                                   4:2:2 8-bit YUV video packing (default, detailed <link YUV422_8 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_YUVK4224_8,                /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_YUVK4224_8
                                                   4:2:2:4 8-bit YUVK video packing (detailed <link YUVK4224_8 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_YUV422_10,                 /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_YUV422_10
                                                   4:2:2 10-bit YUV video packing (detailed <link YUV422_10 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_YUVK4224_10,               /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_YUVK4224_10
                                                   4:2:2:4 10-bit YUVK video packing (detailed <link YUVK4224_10 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_YUV4444_8,                 /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_YUV4444_8
                                                   4:4:4:4 8-bit YUV video packing (K forced to blank, detailed <link YUV4444_8/ YUVK4444_8 Video Packings, here>) */
   VHD_BUFPACK_VIDEO_YUVK4444_8,                /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_YUVK4444_8
                                                   4:4:4:4 8-bit YUVK video packing (detailed <link YUV4444_8/ YUVK4444_8 Video Packings, here>) */
   VHD_BUFPACK_VIDEO_YUV444_10,                 /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_YUV444_10
                                                   4:4:4 10-bit YUV video packing (detailed <link YUV444_10 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_YUVK4444_10,               /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_YUVK4444_10
                                                   4:4:4:4 10-bit YUVK video packing (detailed <link YUVK4444_10 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_RGB_32,                    /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_RGB_32
                                                   4:4:4 8-bit RGB video packing (detailed <link RGB_32 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_RGBA_32,                   /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_RGBA_32
                                                   4:4:4:4 8-bit RGBA video packing (detailed <link RGBA_32 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_RGB_24,                    /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_RGB_24
                                                   4:4:4 8-bit RGB video packing (24 bits) (detailed <link RGB_24 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_PLANAR_YVU420_8,	         /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_PLANAR_YVU420_8
                                                   4:2:0 8-bit YUV planar video packing (YV12)(detailed <link YV12 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_PLANAR_YUV420_8,	         /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_PLANAR_YUV420_8
                                                   4:2:0 8-bit YUV planar video packing (I420)(detailed <link I420 Video Packing, here>) */
   VHD_BUFPACK_VIDEO_PLANAR_YVU420_10_MSB_PAD,  /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_PLANAR_YVU420_10_MSB_PAD
                                                   4:2:0 10-bit MSB PAD YVU planar video packing (detailed <link YVU420_10_MSB_PAD, here>) */
   VHD_BUFPACK_VIDEO_PLANAR_YVU420_10_LSB_PAD,  /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_PLANAR_YVU420_10_LSB_PAD
                                                   4:2:0 10-bit LSB PAD YVU planar video packing (detailed <link YVU420_10_LSB_PAD, here>) */
   VHD_BUFPACK_VIDEO_PLANAR_YUV420_10_MSB_PAD,  /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_PLANAR_YUV420_10_MSB_PAD 
                                                   4:2:0 10-bit MSB PAD YUV planar video packing (detailed <link YUV420_10_MSB_PAD, here>) */
   VHD_BUFPACK_VIDEO_PLANAR_YUV420_10_LSB_PAD,  /*!_VHD_BUFFERPACKING::VHD_BUFPACK_VIDEO_PLANAR_YUV420_10_LSB_PAD
                                                   4:2:0 10-bit LSB PAD YUV planar video packing (detailed <link YUV420_10_LSB_PAD, here>) */
   NB_VHD_BUFPACKINGS
} VHD_BUFFERPACKING;

#define VHD_VIDEOPACK_V208               VHD_BUFPACK_VIDEO_YUV422_8              /*!VHD_VIDEOPACK_V208
                                                                                    Old V208 Video Packing is called now YUV422_8. */
#define VHD_VIDEOPACK_AV208              VHD_BUFPACK_VIDEO_YUVK4224_8            /*!VHD_VIDEOPACK_AV208
                                                                                    Old AV208 Video Packing is called now YUVK4224_8. */
#define VHD_VIDEOPACK_V210               VHD_BUFPACK_VIDEO_YUV422_10             /*!VHD_VIDEOPACK_V210
                                                                                    Old V210 Video Packing is called now YUV422_10. */
#define VHD_VIDEOPACK_AV210              VHD_BUFPACK_VIDEO_YUVK4224_10           /*!VHD_VIDEOPACK_AV210
                                                                                    Old AV210 Video Packing is called now YUVK4224_10. */
#define VHD_VIDEOPACK_V408               VHD_BUFPACK_VIDEO_YUV4444_8             /*!VHD_VIDEOPACK_V408
                                                                                    Old V408 Video Packing is called now YUV4444_8. */
#define VHD_VIDEOPACK_AV408              VHD_BUFPACK_VIDEO_YUVK4444_8            /*!VHD_VIDEOPACK_AV408
                                                                                    Old AV408 Video Packing is called now YUVK4444_8. */
#define VHD_VIDEOPACK_V410               VHD_BUFPACK_VIDEO_YUV444_10             /*!VHD_VIDEOPACK_V410
                                                                                    Old V410 Video Packing is called now YUV444_10. */
#define VHD_VIDEOPACK_AV410              VHD_BUFPACK_VIDEO_YUVK4444_10           /*!VHD_VIDEOPACK_AV410
                                                                                    Old AV410 Video Packing is called now YUVK4444_10. */
#define VHD_VIDEOPACK_C408               VHD_BUFPACK_VIDEO_RGB_32                /*!VHD_VIDEOPACK_C408
                                                                                    Old C408 Video Packing is called now RGB_32. */
#define VHD_VIDEOPACK_AC408              VHD_BUFPACK_VIDEO_RGBA_32               /*!VHD_VIDEOPACK_AC408
                                                                                    Old AC408 Video Packing becomes RGBA_32. */
#define VHD_BUFPACK_VIDEO_PLANAR_YV12    VHD_BUFPACK_VIDEO_PLANAR_YVU420_8	      /*!VHD_BUFPACK_VIDEO_PLANAR_YV12
                                                                                    Old YV12 Buffer Packing becomes PLANAR_YVU420_8. */
#define VHD_BUFPACK_VIDEO_PLANAR_I420    VHD_BUFPACK_VIDEO_PLANAR_YUV420_8       /*!VHD_BUFPACK_VIDEO_PLANAR_I420
                                                                                    Old I420 Buffer Packing becomes PLANAR_YUV420_8. */
#define NB_VHD_VIDEOPACKINGS  NB_VHD_BUFPACKINGS


/*_ VHD_OUTPUTMODE _____________________________________________*/
/*!
   Summary
   VideomasterHD TX streams output modes
   Description
   The VHD_OUTPUTMODE enumeration lists all the available output
   modes
   
   These values are used in VHD_CORE_SP_TX_OUTPUT stream
   property and only apply to SDI transmission streams.
   See Also
   <link VHD_CORE_STREAMPROPERTY, VHD_CORE_SP_TX_OUTPUT>         */

typedef enum _VHD_OUTPUTMODE
{
   VHD_OUTPUT_STREAM = 0,                 /*! Direct output from logical stream (default) */
   VHD_OUTPUT_TEST,                       /*!_VHD_OUTPUTMODE::VHD_OUTPUT_TEST
                                             Test (green) output (only available on DELTA-hd) */
   VHD_OUTPUT_NONE,                       /*! No output. TX channel is in tri-state until the VHD_CORE_SP_TX_OUTPUT is explicitly set to an other value. 
                                              Remark : On a DELTA-hd, setting VHD_CORE_BP_BYPASS_RELAY_x to FALSE reconnects the corresponding TX channel to the stream or the test pattern as a side effect. */
   NB_VHD_OUTPUTMODES
} VHD_OUTPUTMODE;



/*_ VHD_ERRORCODE ____________________________________________________*/
/*!
<keywords Error, Error Codes>

Summary
VideoMasterHD error codes

Description
The VHD_ERRORCODE enumeration lists all error codes that can
be generated by the API functions          
*/

typedef enum _VHD_ERRORCODE
{
   VHDERR_NOERROR,               /*! No error */
   VHDERR_FATALERROR,            /*! Fatal error occurred (should re-install) */
   VHDERR_OPERATIONFAILED,       /*! Operation failed (undefined error) */
   VHDERR_NOTENOUGHRESOURCE,     /*! Not enough resource to complete the operation */
   VHDERR_NOTIMPLEMENTED,        /*! Not implemented yet */
   VHDERR_NOTFOUND,              /*! Required element was not found */

   VHDERR_BADARG,                /*! Bad argument value */
   VHDERR_INVALIDPOINTER,        /*! Invalid pointer */
   VHDERR_INVALIDHANDLE,         /*! Invalid handle */
   VHDERR_INVALIDPROPERTY,       /*! Invalid property index */
   VHDERR_INVALIDSTREAM,         /*! Invalid stream or invalid stream type */

   VHDERR_RESOURCELOCKED,        /*! Resource is currently locked */

   VHDERR_BOARDNOTPRESENT,       /*! Board is not available */
   VHDERR_INCOHERENTBOARDSTATE,  /*! Incoherent board state or register value */
   VHDERR_INCOHERENTDRIVERSTATE, /*! Incoherent driver state */
   VHDERR_INCOHERENTLIBSTATE,    /*! Incoherent library state */
   VHDERR_SETUPLOCKED,           /*! Configuration is locked */
   VHDERR_CHANNELUSED,           /*! Requested channel is already used or doesn't exist */
   VHDERR_STREAMUSED,            /*! Requested stream is already used */
   VHDERR_READONLYPROPERTY,      /*! Property is read-only */
   VHDERR_OFFLINEPROPERTY,       /*! Property is off line-only */
   VHDERR_TXPROPERTY,            /*! Property is of TX streams */
   VHDERR_TIMEOUT,               /*! Time-out occurred */
   VHDERR_STREAMNOTRUNNING,      /*! Stream is not running */
   VHDERR_BADINPUTSIGNAL,        /*! Bad input signal, or unsupported standard */
   VHDERR_BADREFERENCESIGNAL,    /*! Bad genlock signal, or unsupported standard */
   VHDERR_FRAMELOCKED,           /*! Frame already locked */
   VHDERR_FRAMEUNLOCKED,         /*! Frame already unlocked */
   VHDERR_INCOMPATIBLESYSTEM,    /*! Selected video standard is incompatible with running clock system */

   VHDERR_ANCLINEISEMPTY,        /*! ANC line is empty */
   VHDERR_ANCLINEISFULL,         /*! 0. */

   VHDERR_BUFFERTOOSMALL,        /*! Buffer too small */
   VHDERR_BADANC,                /*! Received ANC aren't standard */
   VHDERR_BADCONFIG,             /*! Invalid configuration */

   VHDERR_FIRMWAREMISMATCH,      /*! The loaded firmware is not compatible with the installed driver */
   VHDERR_LIBRARYMISMATCH,       /*! The loaded VideomasterHD library is not compatible with the installed driver */   

   VHDERR_FAILSAFE,              /*! The fail safe firmware is loaded. You need to upgrade your firmware */   
   VHDERR_RXPROPERTY,            /*! Property is of RX streams */

   VHDERR_ALREADYINITIALIZED,    /*! The system is already initialized */
   VHDERR_NOTINITIALIZED,        /*! The system isn't initialized */
   VHDERR_CROSSTHREAD,           /*! Cross-thread operations are not supported */

   VHDERR_INCOHERENTDATA,        /*! Incoherent data */
   VHDERR_BADSIZE,               /*! Wrong buffer size */ 

   VHDERR_WAKEUP,                /*! The wakeup requires all boards to be closed */
   VHDERR_DEVICE_REMOVED,        /*! The device has been removed, the clients should stop referencing this driver instance so the OS can unload it */

   VHDERR_DATANOTREADY,          /*! Data is not ready */
   VHDERR_NOSFPMODULE,           /*! No SFP module */
   VHDERR_SFPMODULELOCKED,       /*! SFP module locked */
   VHDERR_INVALIDTABLE,          /*! SFP module table not set or invalid */
   VHDERR_TEMPERATURETOOHIGH,    /*! The board is in low power mode due to high temperature */

   VHDERR_LTCSOURCEUNLOCKED,     /*! LTC source unlocked */

   NB_VHD_ERRORCODES
} VHD_ERRORCODE;

/*_ VHD_BIDIRCFG_2C _____________________________________________*/
/*!
   Summary
   VideomasterHD bidirectional channel configuration
   Description
   The VHD_BIDIRCFG_2C enumeration lists all the available bidirectional 
   channel configuration for a DELTA 2c card.
   
   These values are used in VHD_SetBiDirCfg function
   See Also
   <link VHD_SetBiDirCfg>
*/
typedef enum _VHD_BIDIRCFG_2C
{
   VHD_BIDIR_11,  /*! RX0 and TX0 configuration*/
   VHD_BIDIR_20,  /*! RX0 and RX1 configuration*/
   VHD_BIDIR_02,  /*! TX0 and TX1 configuration*/
   NB_VHD_BIDIR_2C
}VHD_BIDIRCFG_2C;

#define VHD_BIDIRCFG VHD_BIDIRCFG_2C

/*_ VHD_BIDIRCFG_4C _____________________________________________*/
/*!
   Summary
   VideomasterHD bidirectional channel configuration
   Description
   The VHD_BIDIRCFG_4C enumeration lists all the available bidirectional 
   channel configuration for a DELTA 4c card.
   
   These values are used in VHD_SetBiDirCfg function
   See Also
   <link VHD_SetBiDirCfg>
*/
typedef enum _VHD_BIDIRCFG_4C
{
   VHD_BIDIR_40,  /*! RX0, RX1, RX2 and RX3 configuration*/
   VHD_BIDIR_31,  /*! RX0, RX1, RX2 and TX0 configuration*/
   VHD_BIDIR_22,  /*! RX0, RX1, TX0 and TX1 configuration*/
   VHD_BIDIR_13,  /*! RX0, TX0, TX1 and TX2 configuration*/
   VHD_BIDIR_04,  /*! TX0, TX1, TX2 and TX3 configuration*/
   NB_VHD_BIDIR_4C
}VHD_BIDIRCFG_4C;

/*_ VHD_BIDIRCFG_8C _____________________________________________*/
/*!
   Summary
   VideomasterHD bidirectional channel configuration
   Description
   The VHD_BIDIRCFG_8C enumeration lists all the available bidirectional 
   channel configuration for a DELTA 2c card.
   
   These values are used in VHD_SetBiDirCfg function
   See Also
   <link VHD_SetBiDirCfg>
*/
typedef enum _VHD_BIDIRCFG_8C
{
   VHD_BIDIR_80,  /*! RX0, RX1, RX2, RX3, RX4, RX5, RX6 and RX7 configuration*/
   VHD_BIDIR_71,  /*! RX0, RX1, RX2, RX3, RX4, RX5, RX6 and TX0 configuration*/
   VHD_BIDIR_62,  /*! RX0, RX1, RX2, RX3, RX4, RX5, TX0 and TX1 configuration*/
   VHD_BIDIR_53,  /*! RX0, RX1, RX2, RX3, RX4, TX0, TX1 and TX2 configuration*/
   VHD_BIDIR_44,  /*! RX0, RX1, RX2, RX3, TX0, TX1, TX2 and TX3 configuration*/
   VHD_BIDIR_35,  /*! RX0, RX1, RX2, TX0, TX1, TX2, TX3 and TX4 configuration*/
   VHD_BIDIR_26,  /*! RX0, RX1, TX0, TX1, TX2, TX3, TX4 and TX5 configuration*/
   VHD_BIDIR_17,  /*! RX0, TX0, TX1, TX2, TX3, TX4, TX5 and TX6 configuration*/
   VHD_BIDIR_08,  /*! TX0, TX1, TX2, TX3, TX4, TX5, TX6 and TX7 configuration*/
   NB_VHD_BIDIR_8C
}VHD_BIDIRCFG_8C;

/*_ VHD_COMPANION_CARD_TYPE _____________________________________________*/
/*!
   Summary
   VideomasterHD companion card type
   Description
   The VHD_COMPANION_CARD_TYPE enumeration lists all the available companion card types
   
   These values are used in VHD_DetectCompanionCard function
   See Also
   <link VHD_DetectCompanionCard>
*/

typedef enum _VHD_COMPANION_CARD_TYPE
{
   VHD_LTC_COMPANION_CARD=0,       /*! A-LTC companion card*/
   NB_VHD_COMPANION_CARD_TYPE
}VHD_COMPANION_CARD_TYPE;

/*_ VHD_TIMECODE_SOURCE _____________________________________________*/
/*!
   Summary
   VideomasterHD timecode source
   Description
   The VHD_TIMECODE_SOURCE enumeration lists all the available timecode sources
   
   These values are used in VHD_GetTimecode and VHD_GetSlotTimecode functions
   See Also
   <link VHD_GetTimecode, VHD_GetSlotTimecode>
*/
typedef enum _VHD_TIMECODE_SOURCE
{
   VHD_TC_SRC_LTC_COMPANION_CARD,     /*! A-LTC companion card timecode source*/
   NB_VHD_TC_SRC
}VHD_TIMECODE_SOURCE;

/*_ STRUCTURES _______________________________________________________________
//
// This section defines the different structures used by VideomasterHD_Core
*/

/*_ VHD_TIMECODE _______________________________________________________*/
/*!
   Summary
   SDI embedded timecode structure
   Description
   The VHD_TIMECODE structure abstracts a timecode value.
   See Also
   VHD_SlotExtractTimecode VHD_SlotEmbedTimecode
   VHD_TIMECODETYPE VHD_TIMECODESLOT                      */

typedef struct _VHD_TIMECODE
{
   BYTE  Hour;                /*! Timecode hour component */
   BYTE  Minute;              /*! Timecode minute component */
   BYTE  Second;              /*! Timecode second component */
   BYTE  Frame;               /*! Timecode frame component */
   ULONG BinaryGroups;        /*! Timecode binary groups component. This field comprises 8 groups of 4-bit, stored in a 32-bit variable with LSB being the LSB of BG1 until MSB being MSB of BG8 */
   BYTE  Flags;               /*! Timecode 6 flag bits, as specified by SMPTE 12M */
   BYTE  pDBB[2];             /*! Timecode distributed binary bit groups component (ATC only) */
} VHD_TIMECODE;

#ifndef EXCLUDE_API_FUNCTIONS


#ifdef __cplusplus
extern "C" {
#endif



/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideomasterHD_Core
*/



/*_ API management functions ________________________________________________*/



/*** VHD_GetApiInfo **********************************************************/
/*!VHD_GetApiInfo@ULONG *@ULONG *
   Summary
   VideoMasterHD API information query
   Description
   This function provides information about the VideoMasterHD
   library, and about the number of detected Deltacast cards
   Parameters
   pApiVersion :  Optional pointer to a variable wherein the
                  library stores its version number
   pNbBoards :    Optional pointer to a variable wherein the
                  library stores the number of detected Deltacast
                  cards
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)       */
ULONG VIDEOMASTER_HD_API VHD_GetApiInfo (ULONG *pApiVersion, ULONG *pNbBoards);




/*_ Boards handling functions _______________________________________________*/



/*** VHD_OpenBoardHandle *****************************************************/
/*!VHD_OpenBoardHandle@ULONG@HANDLE *@HANDLE@ULONG
   Summary
   Board handle opening
   Description
   This function opens a handle to the specified board
   Parameters
   BoardIndex :          Zero\-based index of the board to open a
                         handle on
   pBrdHandle :          Pointer to a caller\-allocated variable
                         receiving the board handle
   OnStateChangeEvent :  Future use. Must be NULL.
   StateChangeMask :     Future use. Must be zero.
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_CloseBoardHandle                                           */
ULONG VIDEOMASTER_HD_API VHD_OpenBoardHandle (ULONG BoardIndex, HANDLE *pBrdHandle, HANDLE OnStateChangeEvent, ULONG StateChangeMask);


/*** VHD_CloseBoardHandle ****************************************************/
/*!
   Summary
   Board handle closing
   Description
   This function closes a board handle previously opened with
   VHD_OpenBoardHandle
   Parameters
   BrdHandle :  Handle of the board to close
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   If there are still running logical streams in the context of
   this board handle, they are closed. This does not mean
   physical channels are stopped, since they could still be
   accessed in another context
   See Also
   VHD_OpenBoardHandle
*/
ULONG VIDEOMASTER_HD_API VHD_CloseBoardHandle (HANDLE BrdHandle);


/*** VHD_SetBoardProperty ****************************************************/
/*!VHD_SetBoardProperty@HANDLE@ULONG@ULONG
   Summary
   Board property configuration
   Description
   This function configures the value of the specified board
   property
   Parameters
   BrdHandle :  Handle of the board to set property on
   Property :   Property to set, must be a value of the various
                board properties enumerations
   Value :      Property value to configure
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_CORE_BOARDPROPERTY VHD_SDI_BOARDPROPERTY
   VHD_GetBoardProperty                                         */
ULONG VIDEOMASTER_HD_API VHD_SetBoardProperty (HANDLE BrdHandle, ULONG Property, ULONG Value);


/*** VHD_GetBoardProperty ****************************************************/
/*!VHD_GetBoardProperty@HANDLE@ULONG@ULONG *
   Summary
   Board property query
   Description
   This function retrieves value of the specified board property
   Parameters
   BrdHandle :  Handle of the board to get property from
   Property :   Property to get, must be a value of the various
                board properties enumerations
   pValue :     Pointer to caller\-allocated variable to return
                property value
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_CORE_BOARDPROPERTY VHD_SDI_BOARDPROPERTY
   VHD_SetBoardProperty                                          */
ULONG VIDEOMASTER_HD_API VHD_GetBoardProperty (HANDLE BrdHandle, ULONG Property, ULONG *pValue);


/*** VHD_RearmWatchdog *******************************************************/
/*!VHD_RearmWatchdog@HANDLE
   Summary
   Board watchdog re-arming
   Description
   This function re-arms the by-pass relays watchdog of the
   specified board
   Parameters
   BrdHandle :  Handle of the board to re\-arm watchdog on
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   The DELTA-hd, DELTA-key and DELTA-codec boards comprise
   by-pass relays that electrically connect each RX-TX pair in
   case of power failure.
   
   These by-pass relays may also be used to protect your video
   system against software crashes, by the way of the featured
   watchdog.
   
   
   
   Watchdog is an onboard timer that is programmed using the <link VHD_CORE_BOARDPROPERTY, VHD_CORE_BP_WATCHDOG_TIMER>
   board property. Once it is set, the board re-establish relays
   loopthrough if the watchdog has not been re-armed using the
   present function within the time specified by <link VHD_CORE_BOARDPROPERTY, VHD_CORE_BP_WATCHDOG_TIMER>,
   or disabled by setting this property to zero
   See Also
   <link VHD_CORE_BOARDPROPERTY, VHD_CORE_BP_WATCHDOG_TIMER>                                                           */
ULONG VIDEOMASTER_HD_API VHD_RearmWatchdog (HANDLE BrdHandle);

/*** VHD_GetBoard27MhzTime ****************************************************/
/*!VHD_GetBoard27MhzTime@HANDLE@LONGLONG *
   Summary
   Board 27Mhz time query
   Description
   This function retrieves the 27Mhz time sample
   associated to the given board.
   Parameters
   BoardHandle :     Handle of the board to operate on
   p27MHzTimeStamp : Pointer to caller\-allocated variable
                     receiving the 27Mhz time 
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)      */
ULONG VIDEOMASTER_HD_API VHD_GetBoard27MhzTime(HANDLE BoardHandle, LONGLONG *p27MHzTimeStamp);



/*_ Streams handling functions ______________________________________________*/



/*** VHD_OpenStreamHandle ****************************************************/
/*!VHD_OpenStreamHandle@HANDLE@ULONG@ULONG@BOOL32 *@HANDLE *@HANDLE
   Summary
   VideoMasterHD stream handle opening
   Description
   This function opens a handle to the specified logical stream
   Parameters
   BrdHandle :         Handle of the board to open a stream
                       handle on
   StrmType :          Type of logical stream to open, must be a
                       value of the VHD_STREAMTYPE enumeration
   ProcessingMode :    Mode the stream is processed, must be a
                       value of the VHD_xxx_STREAMPROCMODE
                       enumeration
   pSetupLock :        Pointer to a boolean variable if must lock
                       configuration on that stream, or NULL
                       otherwise. If not NULL, then the boolean
                       is updated by VideoMasterHD to signal if
                       the handle owns the configuration lock or
                       not
   pStrmHandle :       Pointer to a caller\-allocated variable
                       receiving the stream handle
   OnDataReadyEvent :  Handle of an caller\-created event. Must be NULL if not used.
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_OpenBoardHandle VHD_CloseStreamHandle VHD_STREAMTYPE
   VHD_SDI_STREAMPROCMODE VHD_DVI_STREAMPROCMODE
   VHD_ASI_STREAMPROCMODE                                         */
ULONG VIDEOMASTER_HD_API VHD_OpenStreamHandle (HANDLE BrdHandle, ULONG StrmType, ULONG ProcessingMode, BOOL32 *pSetupLock, HANDLE *pStrmHandle, HANDLE OnDataReadyEvent);


/*** VHD_CloseStreamHandle ***************************************************/
/*!
   Summary
   VideoMasterHD stream handle closing
   Description
   This function closes a logical stream handle previously
   opened with VHD_OpenStreamHandle
   Parameters
   StrmHandle :  Handle of the stream to close
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Closing a logical stream handle stops corresponding physical
   channels only if the stream is not accessed anymore (closing
   of last handle on that stream)
   See Also
   VHD_OpenStreamHandle
*/
ULONG VIDEOMASTER_HD_API VHD_CloseStreamHandle (HANDLE StrmHandle);


/*** VHD_SetStreamProperty ***************************************************/
/*!VHD_SetStreamProperty@HANDLE@ULONG@ULONG
   Summary
   VideoMasterHD stream property configuration
   Description
   This function configures the value of the specified stream
   property
   Parameters
   StrmHandle :  Handle of the stream to set property on
   Property :    Property to set, must be a value of the various
                 stream properties enumerations
   Value :       Property value to configure
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Setting stream properties is only allowed if the caller
   handle has the configuration lock on the stream.
   See Also
   VHD_CORE_STREAMPROPERTY VHD_SDI_STREAMPROPERTY
   VHD_DVI_STREAMPROPERTY VHD_ASI_STREAMPROPERTY
   VHD_GetStreamProperty                                         */
ULONG VIDEOMASTER_HD_API VHD_SetStreamProperty (HANDLE StrmHandle, ULONG Property, ULONG Value);


/*** VHD_GetStreamProperty ***************************************************/
/*!VHD_GetStreamProperty@HANDLE@ULONG@ULONG *
   Summary
   VideoMasterHD stream property query
   Description
   This function retrieves value of the specified stream
   property
   Parameters
   StrmHandle :  Handle of the stream to get property from
   Property :    Property to get, must be a value of the various
                 stream properties enumerations
   pValue :      Pointer to caller\-allocated variable to return
                 property value
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)

   
   In DVI, auto-detection properties are not available on RX1
   if RX0 is started in dual link


   See Also
   VHD_CORE_STREAMPROPERTY VHD_SDI_STREAMPROPERTY
   VHD_DVI_STREAMPROPERTY VHD_ASI_STREAMPROPERTY
   VHD_SetStreamProperty                                         */
ULONG VIDEOMASTER_HD_API VHD_GetStreamProperty (HANDLE StrmHandle, ULONG Property, ULONG *pValue);


/*** VHD_StartStream *********************************************************/
/*!VHD_StartStream@HANDLE
   Summary
   VideoMasterHD stream starting
   Description
   This function starts the specified stream
   Parameters
   StrmHandle :  Handle of the stream to start
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Once the stream is started, all offline properties become
   unavailable.
   
   
   
   If VHD_StartStream returns <link VHD_ERRORCODE, VHDERR_NOTENOUGHRESOURCE>,
   this most of the time means that the underlying driver was
   unable to allocate the buffer queue used to transfer data
   from and to the card. This can be verified by reading back
   the <link VHD_CORE_STREAMPROPERTY, VHD_CORE_SP_BUFFERQUEUE_DEPTH>
   stream property. In this case, try reducing the <link VHD_CORE_STREAMPROPERTY, VHD_CORE_SP_BUFFERQUEUE_DEPTH>
   property or increasing the amount of RAM in your PC. When
   using <link Memory preallocation, Memory Preallocation>, <link VHD_ERRORCODE, VHDERR_NOTENOUGHRESOURCE>
   is returned if the preallocated pool size is too small or if
   the preallocation failed (check <link VHD_CORE_BOARDPROPERTY, VHD_CORE_BP_PREALLOCPOOLxSIZE>
   before starting the stream).


   In DVI,
   if RX0 is started in dual link, RX1 can't be started (return VHDERR_CHANNELUSED)
   if RX1 is started, RX0 can't be started in dual link (return VHDERR_CHANNELUSED)
   
   
   
   Under Linux, this memory allocation issue is often due to
   MAX_ORDER being too small. Please consult the Installation
   Guide for further information on that subject
   See Also
   VHD_StopStream VHD_OpenStreamHandle                                                                           */
ULONG VIDEOMASTER_HD_API VHD_StartStream (HANDLE StrmHandle);


/*** VHD_StopStream **********************************************************/
/*!
	Summary
	VideoMasterHD stream stopping
	
	Description
   This function stops the specified stream if it is running

   Parameters
   StrmHandle : Handle of the stream to stop

   Returns
   The function returns the status of its execution as VideoMasterHD error code
   (see VHD_ERRORCODE enumeration)

   See Also
   VHD_StartStream
*/
ULONG VIDEOMASTER_HD_API VHD_StopStream (HANDLE StrmHandle);




/*_ Low-level transfers handling functions __________________________________*/


/*** VHD_LockSlotHandle ************************************************************/
/*!VHD_LockSlotHandle@HANDLE@HANDLE *
   Summary
   VideoMasterHD slot query
   Description
   This function locks a slot on the stream, and provides a
   handle to the locked slot.
   
   
   
   If no slot is ready, the caller is put asleep until such a
   slot is available, or until the time-out value specified by
   the <link VHD_CORE_STREAMPROPERTY, VHD_CORE_SP_IO_TIMEOUT>
   stream property is reached.
   
   Every locked slot must be unlocked using VHD_UnlockSlotHandle
   Parameters
   StrmHandle :   Handle of the stream to operate on
   pSlotHandle :  Pointer to a caller\-allocated variable
                  receiving the slot handle
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_UnlockSlotHandle                                          */
ULONG VIDEOMASTER_HD_API VHD_LockSlotHandle (HANDLE StrmHandle, HANDLE *pSlotHandle);


/*** VHD_UnlockSlotHandle **********************************************************/
/*!VHD_UnlockSlotHandle@HANDLE
   Summary
   VideoMasterHD slot releasing
   Description
   This function unlocks a locked slot. Every locked slot must
   be unlocked using this function
   Parameters
   SlotHandle :  Handle of the slot to operate on
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_LockSlotHandle                                          */
ULONG VIDEOMASTER_HD_API VHD_UnlockSlotHandle (HANDLE SlotHandle);




/*_ Slot management functions ________________________________________________*/


/*** VHD_GetSlotID *****************************************************/
/*!VHD_GetSlotID@HANDLE@ULONG *
   Summary
   VideoMasterHD slot ID query
   Description
   This function retrieves the ID of the given slot.
   
   In reception, the ID is a 32-bit counter incremented
   each time a new buffer is received by the hardware. In
   transmission, it is incremented each time the application
   locks a slot.
   Parameters
   SlotHandle :      Handle of the slot to operate on
   pSlotID :         Pointer to a caller\-allocated variable
                     receiving the ID value
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   
   
   See Also
   VHD_LockSlotHandle                                           */
ULONG VIDEOMASTER_HD_API VHD_GetSlotID (HANDLE SlotHandle, ULONG *pSlotID);


/*** VHD_GetSlotBuffer *****************************************************/
/*!VHD_GetSlotBuffer@HANDLE@ULONG@BYTE **@ULONG *
   Summary
   VideomasterHD slot buffer query
   Description
   This function provides pointer to the specified buffer from
   the given locked slot handle
   Parameters
   SlotHandle :   Handle of the slot to operate on
   BufferType :   Set to VHD_SDI_BUFFERTYPE
                  on SDI streams, to VHD_DVI_BUFFERTYPE
                  on DVI streams, or to VHD_ASI_BUFFERTYPE
                  on ASI streams
    ppBuffer :    Pointer to a caller\-allocated variable
                  receiving the buffer pointer
   pBufferSize :  Pointer to a caller\-allocated variable
                  receiving the buffer size
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_LockSlotHandle VHD_SDI_BUFFERTYPE VHD_ASI_BUFFERTYPE
   VHD_DVI_BUFFERTYPE                                                            */
ULONG VIDEOMASTER_HD_API VHD_GetSlotBuffer (HANDLE SlotHandle, ULONG BufferType, BYTE **ppBuffer, ULONG *pBufferSize);


/*** VHD_GetSlotStatus *****************************************************/
/*!VHD_GetSlotStatus@HANDLE@void **
   Summary
   VideomasterHD slot status query
   Description
   This function retrieves the status of the given slot
   Parameters
   SlotHandle :    Handle of the slot to operate on
   ppSlotStatus :  Pointer to a caller\-allocated variable
                   receiving the status pointer
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   The *ppSlotStatus pointer should be casted according to the
   actual stream type.
   
   Currently, VideoMasterHD implements ASI slots status only. On
   ASI steams the pointer should thus be casted to a
   VHD_ASI_SLOTSTATUS pointer.
   See Also
   VHD_ASI_SLOTSTATUS VHD_LockSlotHandle                         */
ULONG VIDEOMASTER_HD_API VHD_GetSlotStatus (HANDLE SlotHandle, void **ppSlotStatus);


/*** VHD_GetSlotTimestamp ****************************************************/
/*!VHD_GetSlotTimestamp@HANDLE@ULONG *
   Summary
   Slot timestamp query
   Description
   This function retrieves the master timestamp sample
   associated to the given slot.
   
   It is only available on SDI reception streams.
   Parameters
   SlotHandle :      Handle of the slot to operate on
   pSlotTimestamp :  Pointer to caller\-allocated variable
                     receiving the slot timestamp
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   See Also
   VHD_LockSlotHandle                                       */
ULONG VIDEOMASTER_HD_API VHD_GetSlotTimestamp (HANDLE SlotHandle, ULONG *pSlotTimestamp);

/*** VHD_WaitNextMasterTimestamp ****************************************************/
/*!VHD_WaitNextMasterTimestamp@HANDLE@ULONG *@ULONG
   Summary
   Next board master timestamp query
   Description
   This function waits until next increment of the board master
   timestamp and retrieves its value
   Parameters
   BrdHandle :   Handle of the board to get property from
   pTimeStamp :  Pointer to caller\-allocated variable receiving
                 next timestamp value
   TimeOut :     Time\-out value (in millisecond) of the wait
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)      */
#ifdef __cplusplus
ULONG VIDEOMASTER_HD_API VHD_WaitNextMasterTimestamp (HANDLE BrdHandle, ULONG *pTimeStamp, ULONG TimeOut=100);
#else
ULONG VIDEOMASTER_HD_API VHD_WaitNextMasterTimestamp (HANDLE BrdHandle, ULONG *pTimeStamp, ULONG TimeOut);
#endif

/*** VHD_GetSlotSystemTime ****************************************************/
/*!VHD_GetSlotSystemTime@HANDLE@LONGLONG *
   Summary
   Slot system time query
   Description
   This function retrieves the system time sample
   associated to the given slot.
   Parameters
   SlotHandle :      Handle of the slot to operate on
   pSlotSystemTime : Pointer to caller\-allocated variable
                     receiving the slot system time in us
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)      */
ULONG VIDEOMASTER_HD_API VHD_GetSlotSystemTime (HANDLE SlotHandle, LONGLONG *pSlotSystemTime);



/*** VHD_ManufacturerCheck ****************************************************/
/*!
   Summary
   Manufacturer check

   Description
   This function checks that the board belongs to the user supply chain.

   Parameters
   BrdHandle :      Handle of the board to operate on
   Challenge :      An random number on which the branding algorithm will be applied
   pResponse :      Pointer to caller\-allocated variable
                    receiving the response of the challenge

   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)  

   See also
   <link Branding>  
*/
ULONG VIDEOMASTER_HD_API VHD_ManufacturerCheck(HANDLE BrdHandle, ULONG Challenge, ULONG *pResponse);

/*** VHD_GetSlotParity ****************************************************/
/*!VHD_GetSlotParity@HANDLE@BOOL32 *
   Summary
   Slot parity query
   Description
   This function retrieves the parity
   associated to the given slot.
   Parameters
   SlotHandle :      Handle of the slot to operate on
   pEvenParity_B :   Pointer to caller\-allocated variable
                     receiving the slot parity (TRUE = even, FALSE = ODD)
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)      */
ULONG VIDEOMASTER_HD_API VHD_GetSlotParity (HANDLE SlotHandle, BOOL32 *pEvenParity_B);

/*** VHD_SetBiDirCfg ****************************************************/
/*!VHD_SetBiDirCfg@ULONG@ULONG
   Summary
   Bidirectional channel configuration
   Description
   This function defines the channel configuration on bidirectional card
   associated to the given slot.
   Parameters
   BoardIndex :    Zero\-based index of the board to open a
                   handle on
   BiDirCfg_UL :   See VHD_BIDIRCFG_2C, VHD_BIDIRCFG_4C and VHD_BIDIRCFG_8C enumerations

   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)      */
ULONG VIDEOMASTER_HD_API VHD_SetBiDirCfg(ULONG BoardIndex, ULONG BiDirCfg_UL);

/*** VHD_GetSlot27MhzTimeStamp ****************************************************/
/*!VHD_GetSlot27MhzTimeStamp@HANDLE@LONGLONG *
   Summary
   Slot 27Mhz time stamp query
   Description
   This function retrieves the 27Mhz time stamp sample
   associated to the given slot.
   Parameters
   SlotHandle :      Handle of the slot to operate on
   p27MHzTimeStamp : Pointer to caller\-allocated variable
                     receiving the 27Mhz time stamp
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)      */
ULONG VIDEOMASTER_HD_API VHD_GetSlot27MhzTimeStamp(HANDLE SlotHandle, LONGLONG *p27MHzTimeStamp);

/*** VHD_DetectCompanionCard ****************************************************/
/*!VHD_DetectCompanionCard@HANDLE@VHD_COMPANION_CARD_TYPE@BOOL32 *
   Summary
   Companion card detection
   Description
   This function detects the presence of the companion card
   associated to the given companion card type.
   Parameters
   BoardHandle :        Handle of the board to operate on
   CompanionCardType :  Companion card type
   pCompanionCard :     Pointer to caller\-allocated variable
                        receiving the companion card presence (TRUE = present, FALSE = not present)
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration) */ 
ULONG VIDEOMASTER_HD_API VHD_DetectCompanionCard( HANDLE BoardHandle, VHD_COMPANION_CARD_TYPE CompanionCardType, BOOL32 *pPresentCompanionCard );

/*** VHD_GetTimecode ****************************************************/
/*!VHD_GetTimecode@HANDLE@VHD_TIMECODE_SOURCE@BOOL32 *@float *@VHD_TIMECODE *
   Summary
   Get timecode
   Description
   This function gets the timecode value 
   associated to the given timecode source.
   Parameters
   BoardHandle :        Handle of the board to operate on
   TcSource :           Timecode source
   pLocked :            Locked state of the LTC signal (TRUE = locked, FALSE = unlocked) 
   pFrameRate :         Framerate of the LTC signal 
   pTimeCode :          Pointer to a caller\-allocated variable
                        Timecode packet structure

   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Timecode is get in the requested timecode source according to the SMPTE 12M-1-2008 standard */
ULONG VIDEOMASTER_HD_API VHD_GetTimecode(HANDLE BoardHandle, VHD_TIMECODE_SOURCE TcSource, BOOL32 *pLocked, float *pFrameRate, VHD_TIMECODE *pTimeCode);

/*** VHD_GetSlotTimecode ****************************************************/
/*!VHD_GetSlotTimecode@HANDLE@VHD_TIMECODE_SOURCE@VHD_TIMECODE *
   Summary
   Get slot timecode
   Description
   This function looks for timecode value in the requested timecode source synchronized with the
   provided SDI slot.
   Parameters
   SlotHandle :        Handle of the slot to operate on
   TcSource :          Timecode source
   pTimeCode :         Pointer to a caller\-allocated variable
                       Timecode packet structure

   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)
   Remarks
   Timecode is get in the requested timecode source according to the
   SMPTE 12M-1-2008 standard */
ULONG VIDEOMASTER_HD_API VHD_GetSlotTimecode(HANDLE SlotHandle, VHD_TIMECODE_SOURCE TcSource, VHD_TIMECODE *pTimeCode);

#ifdef __cplusplus
}
#endif

#endif //EXCLUDE_API_FUNCTIONS


#endif // _VIDEOMASTERHD_CORE_H_
