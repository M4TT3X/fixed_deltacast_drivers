#ifndef _VIDEOMASTERHD_HDMI_AUDIO_H_
#define _VIDEOMASTERHD_HDMI_AUDIO_H_

#ifdef __cplusplus
extern "C" {
#endif

   /*_ ENUMERATIONS _____________________________________________________________
//
// This section defines the different enumerations used by VideomasterHD_Audio
*/



/*_ VHD_HDMIAUDIOFORMAT  _________________________________________________________*/
/*!
   Summary
   HDMI audio formats
   Description
   The VHD_HDMIAUDIOFORMAT enumeration lists all the available HDMI
   audio sample formats                                         */
typedef enum _VHD_HDMIAUDIOFORMAT {
   VHD_HDMIAUDIOFORMAT_16,                   /*! PCM sample (16 bits) */
   VHD_HDMIAUDIOFORMAT_20,                   /*! PCM sample (20 bits) */
   VHD_HDMIAUDIOFORMAT_24,                   /*! PCM sample (24 bits) */
   NB_VHD_HDMIAUDIOFORMAT
} VHD_HDMIAUDIOFORMAT;

#ifndef EXCLUDE_API_FUNCTIONS

/*_ API FUNCTIONS ____________________________________________________________
//
// This section defines the different API functions exported by VideomasterHD_Audio
*/

/*** VHD_SlotExtractHDMIPCMAudio ****************************************************/
/*!
   Summary
   HDMI Audio data extraction
   Description
   This function extracts PCM audio data from an HDMI slot.
   
   The concerned stream must be of processing mode including
   audio data handling
   Parameters
   SlotHandle :  Handle of the slot to work on
   AudioFormat:  audio data bit depth (16, 20 or 24-bit)
   ChannelMask:  mask of the channels that must be extracted
   pData :       Pointer to a caller\-allocated array of byte
                 receiving the audio data
   pBufferSize : Pointer to a caller\-allocated variable
                 receiving the buffer size
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)

   Remarks
   The required size of this buffer can be calculated based on
   the audio format, the audio channel mask,
   the incoming audio sampling rate and the video frame rate.
   If the buffer is smaller than the required size,
   VHD_SlotExtractHDMIPCMAudio doesn't extract audio and returns the
   required buffer size. Otherwise, VHD_SlotExtractHDMIPCMAudio
   extracts audio in the buffer and returns the used buffer
   size.

   See Also
   VHD_LockSlotHandle VHD_HDMIAUDIOFORMAT                                                  */
   ULONG VIDEOMASTER_HD_API VHD_SlotExtractHDMIPCMAudio (HANDLE SlotHandle,VHD_HDMIAUDIOFORMAT AudioFormat, UBYTE ChannelMask, UBYTE *pData, ULONG *pBufferSize);

   /*** VHD_SlotExtractHDMINonPCMAudio ****************************************************/
/*!
   Summary
   HDMI Audio data extraction
   Description
   This function extracts non-PCM audio data from an HDMI slot.
   
   The concerned stream must be of processing mode including
   audio data handling
   Parameters
   SlotHandle :  Handle of the slot to work on
   pData :       Pointer to a caller\-allocated array of byte
                 receiving the audio data
   pBufferSize : Pointer to a caller\-allocated variable
                 receiving the buffer size
   Returns
   The function returns the status of its execution as
   VideoMasterHD error code (see VHD_ERRORCODE enumeration)

   Remarks
   The required size of this buffer can be calculated based on
   the audio format, the audio channel mask,
   the incoming audio sampling rate and the video frame rate.
   If the buffer is smaller than the required size,
   VHD_SlotExtractHDMINonPCMAudio doesn't extract audio and returns the
   required buffer size. Otherwise, VHD_SlotExtractHDMINonPCMAudio
   extracts audio in the buffer and returns the used buffer
   size.

   See Also
   VHD_LockSlotHandle                                                  */
   ULONG VIDEOMASTER_HD_API VHD_SlotExtractHDMINonPCMAudio (HANDLE SlotHandle, UBYTE *pData, ULONG *pBufferSize);

#endif

#ifdef __cplusplus
}
#endif

#endif //_VIDEOMASTERHD_HDMI_AUDIO_H_