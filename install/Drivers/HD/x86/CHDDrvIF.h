/********************************************************************************************************************//**
 * @internal
 * @file   	CHDDrvIF.h
 * @date   	2010/07/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	c     	Author   Description
   ====================================================================================================================
   2010/07/16  v00.01.0000    cs       Creation of this file
   2010/01/21  v00.01.0001    ja       New: Board_ArmCmd() and GetArmRevID()
   2010/01/25  v00.01.0002    ja       New: ARMStopHDMI() and ARMStartHDMI()
   2010/02/02  v00.01.0003    ja       New: Add AV208 support (FPGA_RXx_BIT_DUAL_LINK in Channel_SdiRx_Start())
   2011/04/08  v00.01.0004    cs/ja    Remove a Board_SetIntMask() on RX1 or RX3 in the Channel_SdiRX_Start() when
                                       the 3D feature is activated. These interrupts are not required in such cases.
   2011/04/08  v00.01.0005    cs       Add a CPLDVersion_UL member in the BOARD_INFO structure
                                       
 **********************************************************************************************************************/

#ifndef _CHDDRVIF_H_
#define _CHDDRVIF_H_

/***** INCLUDES SECTION ***********************************************************************************************/


#include "CStreamDrvIF.h"
#include "SDI.h"
#include "HDDrv_Properties.h"
#include "HDDrv_Types.h"
#include "HDDrv_Version.h"
#include "SPI.h"
#include "IGSPIF.h"
#include "IFwUpdate.h"
#include "IFabInfo.h"

namespace CHDDrvIF_NS
{



/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define RXCHANNEL_ADDITIONAL_FEATURES_DISABLE_IO_PROCESSING 0x000000001
#define RXCHANNEL_ADDITIONAL_FEATURES_ENABLE_3D             0x000000004
#define RXCHANNEL_ADDITIONAL_FEATURES_ENABLE_THUMBNAIL      0x000000008
#define RXCHANNEL_ADDITIONAL_FEATURES_VBI10BITS             0x000000010


#define TXCHANNEL_ADDITIONAL_FEATURES_DISABLE_IO_PROCESSING 0x000000001
#define TXCHANNEL_ADDITIONAL_FEATURES_ENABLE_SMPTE352M      0x000000002
#define TXCHANNEL_ADDITIONAL_FEATURES_ENABLE_3D             0x000000004
#define TXCHANNEL_ADDITIONAL_FEATURES_RXONTX_ENABLE         0x000000080 
#define TXCHANNEL_ADDITIONAL_FEATURES_RXONTX_DELAY_MSK      0x000000070 
#define TXCHANNEL_ADDITIONAL_FEATURES_DISABLE_CLAMPING      0x000000100 
#define TXCHANNEL_ADDITIONAL_FEATURES_NO_CHROMA_ON_KEY      0x000000200 

#define TXCHANNEL_ADDITIONAL_FEATURES_BIT_RXONTX_DELAY(delay)        ((delay<<4) & TXCHANNEL_ADDITIONAL_FEATURES_RXONTX_DELAY_MSK)
#define TXCHANNEL_ADDITIONAL_FEATURES_GET_BIT_RXONTX_DELAY(add_feat) ((add_feat&TXCHANNEL_ADDITIONAL_FEATURES_RXONTX_DELAY_MSK) >> 4)

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

   typedef struct  
   {
      HDDRV_BOARD_TYPE BoardType_e;
      HDDRV_EXTBOARD_TYPE ExtBoardType_e;
      ULONG FirmwareVersion_UL;
      HDDRV_FIRMWAREID FirmwareID_e;
      STREAM_BOARD_STATE FirmwareState_e;
      ULONG BoardFeatures_UL;
      UBYTE NbRxChannels_UB;
      UBYTE NbTxChannels_UB;
      ULONG CPLDVersion_UL;
      ULONG RqstFpgaFwVersion_UL;
      ULONG ArmVersion_UL;
      ULONG RqstArmFwVersion_UL;
      ULONG NbUpgradableStuff_UL;
      HDDRV_FIRMWAREID ArmFwId_E;
   } BOARD_INFO;

   enum CHANNEL
   {
      CHANNEL_RX,
      CHANNEL_TX
   };

   enum ONBOARD_BUFFER_MAPPING
   {
      ONBOARD_BUFFER_MAPPING_DEFAULT_22,
      ONBOARD_BUFFER_MAPPING_DEFAULT_40,
      ONBOARD_BUFFER_MAPPING_DEFAULT_40_THUMBNAIL,
      ONBOARD_BUFFER_MAPPING_DELAYLINE_22_EVEN,
      ONBOARD_BUFFER_MAPPING_DELAYLINE_22_ODD,
      ONBOARD_BUFFER_MAPPING_22_8_TX_BUF_CFG,
      ONBOARD_BUFFER_MAPPING_DEFAULT_04
   } ;


   /** @page HDSDI_ON_BOARD_MEMORY_MAPPING HDSDI On-board memory mapping
    *
    * @section ONBOARD_BUFFER_MAPPING_22_8_TX_BUF_CFG
    *
    * In this special configuration, the 8 on-board pointers are set to 8 different values. Using this feature, the board
    * is able to reduce the risk of under condition. Note that the early transfer must be use to be able to refill the empty slots.
    * Note that this configuration allows only using video "only mode": actually, there is no more room for extra data like ANC.
    *
    */


    

   /*********************************************************************************************************************//**
    * @enum SDI_REF_CLOCK_SRC
    * @brief This enumeration gives all available reference clock sources.
    * @remark The enumeration value are built with tag on the most significant byte to easily determine the source type for
    *         further parameters checking. Used tags are :
    *         - 0x00 : Local clock
    *         - 0x01 : Analog reference
    *         - 0x02 : External clock
    *         - 0x03 : Reference from input channel (RXx)
    *************************************************************************************************************************/

   enum SDI_REF_CLOCK_SRC
   {
      SDI_REF_CLOCK_SRC_LOCAL      =0x00000000,          ///< local 27Mhz oscillator
      SDI_REF_CLOCK_SRC_PROG_LOCAL =0x00000001,          ///< Programmable local oscillator
      SDI_REF_CLOCK_SRC_BB         =0x01000000,          ///< Black burst
      SDI_REF_CLOCK_SRC_EX0        =0x02000000,          ///< External connector 0
      SDI_REF_CLOCK_SRC_EX1        =0x02000001,          ///< External connector 1
      SDI_REF_CLOCK_SRC_RX0        =0x03000000,          ///< Clock from RX0
      SDI_REF_CLOCK_SRC_RX1        =0x03000001,          ///< Clock from RX1
      SDI_REF_CLOCK_SRC_RX2        =0x04000000,          ///< Clock from RX2
      SDI_REF_CLOCK_SRC_RX3        =0x04000001,          ///< Clock from RX3
      SDI_REF_CLOCK_SRC_UNDEFINED  =0xFFFFFFFF
   } ;
   
   /** @enum CLOCK_DIVIDER
    *  @addtogroup BOARD_LEVEL_ACCESS
    *  @brief This enumeration gives the available value for the clock divider parameter of the CLOCK_SETUP_PARAM structure. 
    **/
    enum CLOCK_DIVIDER
   {
      CLOCK_DIVIDER_1=0,      ///< Specifies a clock divider set to 1.0 to obtain frame rate like 50 or 60Hz
      CLOCK_DIVIDER_1001      ///< Specifies a clock divider set to 1.001 to obtain NTSC like frame rate (59.xx Hz)
   }   ;

    enum HDMI_SOURCE
    {
       HDMI_SOURCE_PHYSICAL_RX0,
       HDMI_SOURCE_PHYSICAL_RX1,
       HDMI_SOURCE_PHYSICAL_TX0,
       HDMI_SOURCE_PHYSICAL_TX1,
       HDMI_SOURCE_PHYSICAL_TX2,
       HDMI_SOURCE_PHYSICAL_TX3,
       HDMI_SOURCE_PHYSICAL_TX0_SBS_TX1,
       HDMI_SOURCE_PHYSICAL_TX0_SBS_TX2,
       HDMI_SOURCE_PHYSICAL_TX2_SBS_TX3,
       HDMI_SOURCE_PHYSICAL_TX1_SBS_TX3
    };

   /** @struct CLOCK_SETUP_PARAM
    *  @addtogroup BOARD_LEVEL_ACCESS
    *  @brief Clock setup related parameters.
    * This structure groups all needed parameters for board clocking setup. These parameters are uses with the
    * Board_ReconfigureClocks() method. 
    **/
   struct  CLOCK_SETUP_PARAM
   {
      CLOCK_DIVIDER ClockDivider_e;          
      SDI_REF_CLOCK_SRC ReferenceClockSource_e;
     // ULONG GenlockSource_UL;                /*!< Available values are GENLOCK_SOURCE_FREERUN, GENLOCK_SOURCE_RX0, GENLOCK_SOURCE_RX1, GENLOCK_SOURCE_BLACK_BURST */
      ULONG TimeoutForReference_UL;                             
      ULONG LockOnRefTimeout_UL;     
      SLONG GenlockOffset_SL;
      BOOL32  SetupForKeyer_B;                 /*!< TRUE if clocks must be configured for a keying use case, FALSE otherwise */
      BOOL32  EnableRefStdAutoDetect_B;        /*!< Specifies that the input video std for clocking is either auto detected or forced through the InputVidStd_e parameter */
      BOOL32  ForceMasterVTGStd_B;
      SDI_VIDEOSTD RefVidStd_e;         /*!< Specifies the default video standard in the case of forced incoming vid std (genlock), for freerun operation  */
      SDI_VIDEOSTD MasterVTGVidStd_e;   /*!< Specifies the default video standard in the case of forced incoming vid std (genlock), for freerun operation  */

      ULONG RXChannelsAlignment_UL;          /*!< Specifies, in the case of keyer, if it's needed to try to align RX channels to output */
      ULONG AllowedOffsetForRXAlignment_UL;  /*!< Specifies the maximum offset from the reference to attempt. TODO : not implemented */
   } ;  

   typedef struct
   {
      SDI_VIDEOSTD      VidStd_e;
      SDI_DATA_TYPE     DataType_e;
      SDI_DATA_FORMAT   DataFormat_e;
      BOOL32              GenlockEnable_B;
      ULONG             Genlock_VOffset_UL;
      ULONG             Genlock_HOffset_UL;
      ULONG             AdditionalFeatures_UL;
   } TX_CHANNEL_START_PARAM;

   typedef struct
   {
      BOOL32              AutoDetectInputVidStd_B;
      SDI_VIDEOSTD      VidStd_e;
      SDI_DATA_TYPE     DataType_e;
      SDI_DATA_FORMAT   DataFormat_e;
      ULONG             AdditionalFeatures_UL;
   } RX_CHANNEL_START_PARAM;


   typedef struct
   {
      BOOL32 Coupled_B;
      BOOL32 Dual_B;
   }CHANNEL_STOP_PARAM;

   typedef struct  
   {
      BOOL32 MVTGLocked_B;   // Internal Master VTG is locked
      BOOL32 RefPresent_B;   // Reference is present at the Genlock chip input -> a valid video standard is identified
      BOOL32 Locked_B;       // Genlock chip in locked on its input signal -> valid input pixel clock
   } GENLOCK_STATUS;


   enum ASI_FMT
   {
      ASI_FMT_188,
      ASI_FMT_188_TO_204,
      ASI_FMT_204,
      ASI_FMT_204_TO_188,
      ASI_FMT_RAW,
   };

   enum ASI_INTSEL
   {
      ASI_INTSEL_1,
      ASI_INTSEL_10,
      ASI_INTSEL_100,
      ASI_INTSEL_1000,
   };

   enum ASI_BRSRC
   {
      ASI_BRSRC_LOCAL,
      ASI_BRSRC_RX0,
      ASI_BRSRC_RX1,
   };

   struct ASI_TX_CHANNEL_SETUP_PARAM  
   {  
      ASI_FMT           Fmt_e;
      ULONG             BufferSize_UL;
      ULONG             BitRate_UL;
      ASI_INTSEL        IntPer_e;
      ULONG             MaxDev_UL;
      ASI_BRSRC         BitrateSrc_E;
      BOOL32            BkpBR_B;
      BOOL32            SynchronousSlaving_B;
   };

   struct ASI_RX_CHANNEL_SETUP_PARAM 
   {  
      ASI_FMT           Fmt_e;
      ULONG             BufferSize_UL;
      ULONG             BitRate_UL;
      ASI_INTSEL        IntPer_e;
      ULONG             MaxDev_UL;
      BOOL32              EnableTS_B;
      int               *pPIDFilters_i;
      int               NbOfPIDFilters_i;
      BOOL32              DontSetIMR_B;
   };


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

class CHDDrvIF : public CStreamDrvIF, public ISPIInterface, public IGSPIIF, public IFwUpdate, public IFabInfo
{
public:

   CHDDrvIF(ULONG BoardIdx_UL);
   static ULONG GetNbBoard();

   virtual CBufferQueue * CreateBufferQueue();
   virtual CDRVIF_STATUS Board_IsOK();


   const BOARD_INFO * Board_GetInfo() {return &m_BoardInfo_X;}

   CDRVIF_STATUS  Board_SetGenlockExpectedRefStd(SDI_VIDEOSTD VidStd_e);
   CDRVIF_STATUS  Board_SetGenlockClkSrc(SDI_REF_CLOCK_SRC GenlockClkSrc_e);
   CDRVIF_STATUS  Board_SetGenlockWaitingFrame(ULONG NbFrames_UL);
   CDRVIF_STATUS  Board_SetGenlockJitterWindow(ULONG JitterWindow_UL);
   CDRVIF_STATUS  Board_GetGenlockMTGJitter(int *pMtgJitter_i);


   CDRVIF_STATUS  Board_InitialClocksSetup();
   CDRVIF_STATUS  Board_SetupVTG (int VTGIndex_i, SDI_VIDEOSTD Standard_E, SDI_DATA_FORMAT DataFormat_e);
   CDRVIF_STATUS  Board_SetupMasterVTG (SDI_VIDEOSTD MVTGStandard_E, SDI_VIDEOSTD RefStandard_E);
   CDRVIF_STATUS  Board_SetIntMask( ULONG Mask_UL );
   CDRVIF_STATUS  Board_ClrIntMask( ULONG Mask_UL );
   CDRVIF_STATUS  Board_GetSSN(LONGLONG *pSerialNumber_LL);
   void           Board_TrigInterrupt(ULONG Interrupt_UL);
   CDRVIF_STATUS  Board_SetBypassRelay(int RelayID_i, BOOL32 Enable_B);
   CDRVIF_STATUS  Board_GetBypassRelay(int RelayID_i, BOOL32 *pEnable_B);
   CDRVIF_STATUS  Board_GetBoardTimeStamp( ULONG *pTimeCode_UL=NULL,BOOL32 WaitNextFrame_B=FALSE, ULONG TimeOut_UL=1000);
   CDRVIF_STATUS  Board_WatchdogEnable(ULONG TimeInMS_UL);  
   CDRVIF_STATUS  Board_WatchdogArm();    
   CDRVIF_STATUS  Board_WatchdogDisable();
   void           Board_SetOnBoardBufferBaseAddr(CHANNEL Channel_e, UBYTE ChannelIdx_UB, HDBRD_CHN_DATA_TYPE DataType_e, UBYTE BufferIdx_UB, ULONG BufferBaseAddr_UL );
   void           Board_SetOnBoardBufferBaseAddr(CHANNEL Channel_e, UBYTE ChannelIdx_UB, BUFFERQUEUE_PARAM *pBQParam_X, int ChannelCoupledIdx_i);
   CDRVIF_STATUS Board_FWPROMUpdate(UBYTE FirmwareID_UB, UBYTE *pBuffer_UB, ULONG BufferSize_UL,BOOL32 CheckFirmware_B=FALSE, INotifyProgressFunctor *pSignalProgress_fct=NULL);

   CDRVIF_STATUS  Genlock_StsDrvEvent_Create(DRVEVENT *pDrvEvent_X);
   CDRVIF_STATUS  Genlock_StsDrvEvent_Close(DRVEVENT *pDrvEvent_X);
   CDRVIF_STATUS  Genlock_StsDrvEvent_Wait(DRVEVENT *pDrvEvent_X,ULONG Timeout_UL, GENLOCK_STATUS *pGenlockStatus_X=NULL);
   CDRVIF_STATUS  Genlock_GetStatus(GENLOCK_STATUS *pGenlockStatus_X);
   CDRVIF_STATUS  Genlock_SetClockDivisor(CLOCK_DIVIDER ClockDivider_e);
   CDRVIF_STATUS  Genlock_SetInputVideoStandard(SDI_VIDEOSTD VidStd_E);
   CDRVIF_STATUS  Genlock_GetInputVideoStandard(SDI_VIDEOSTD *pVidStd_E);

   CDRVIF_STATUS  Channel_SdiTx_ArmPreload( UBYTE ChannelIdx_UL);
   CDRVIF_STATUS  Channel_SdiTx_Start(UBYTE ChannelIdx_UL, TX_CHANNEL_START_PARAM * pParam_X);
   CDRVIF_STATUS  Channel_SdiTx_Stop(UBYTE ChannelIdx_UL);
   CDRVIF_STATUS  Channel_SdiTx_Stop( UBYTE ChannelIdx_UB, CHANNEL_STOP_PARAM *pParam_X);

   CDRVIF_STATUS  Channel_SdiRx_Start(UBYTE ChannelIdx_UL, RX_CHANNEL_START_PARAM * pParam_X);
   CDRVIF_STATUS  Channel_SdiRx_Stop(UBYTE ChannelIdx_UL);
   CDRVIF_STATUS  Channel_SdiRx_Stop( UBYTE Channel_UB, CHANNEL_STOP_PARAM *pParam_X);
   CDRVIF_STATUS  Channel_SdiRx_GetStatus(UBYTE ChannelIdx_UL, BOOL32 *pLocked_B, SDI_VIDEOSTD *pVidStd_e, BOOL32 *pAligned_B=NULL, BOOL32 *pCarrierUndetected_B = NULL);

   static ULONG   Helper_GetOnBoardAddress( ONBOARD_BUFFER_MAPPING MapID_e, CHANNEL Channel_e,UBYTE ChnIdx_i, HDBRD_CHN_DATA_TYPE DataType_e, ULONG BufIdx_UL);
   static void Helper_FillOnBoardAddresses(ONBOARD_BUFFER_MAPPING MapID_e,CHANNEL Channel_e,UBYTE ChannelIdx_UB,BUFFERQUEUE_PARAM *pBQParam_X,int ChnCoupledIdx_i);

   CDRVIF_STATUS  GS4911_Init();
   CDRVIF_STATUS  GS4911_SetOutputVideoStandard(ULONG VideoStd_UL);
   CDRVIF_STATUS  GS4911_IsLocked(BOOL32 *pLocked_B);
   CDRVIF_STATUS  GS4911_GetGenlockSts(ULONG *pStatus_UL);
   CDRVIF_STATUS  GS4911_GetInputVideoStandard(ULONG *pInputVideoStd_UL);
   CDRVIF_STATUS  GS4911_GenlockEnable(BOOL32 Enable_B);

   CDRVIF_STATUS GS1559_GetInputVideoStandard(UBYTE RxChannelIdx_UB, ULONG *pInputVideoStd_UL);
   CDRVIF_STATUS GS1559_GetInputVideoStandard(UBYTE RxChannelIdx_UB, UBYTE *pInputVideoStd_UB, BOOL32 *pProgressive_B, BOOL32 *pStdLocked_B);
   CDRVIF_STATUS GS1559_GetErrorStatus(UBYTE RxChannelIdx_UB, ULONG *pErrorStatus_UL);

   CDRVIF_STATUS Board_Asi_GetSTC(ULONG *pMSW_UL, ULONG *pLSW_UL);
   CDRVIF_STATUS Channel_AsiTx_Start( UBYTE Channel_UB, ASI_TX_CHANNEL_SETUP_PARAM *pParam_X );
   CDRVIF_STATUS Channel_AsiTx_Stop( UBYTE Channel_UB );
   CDRVIF_STATUS Channel_AsiTx_GetStatus(UBYTE Channel_UB, BOOL32 *pFramingErr_B/*=NULL*/, BOOL32 *pBitrateErr_B/*=NULL*/);
   CDRVIF_STATUS Channel_AsiTx_GetBytesDiff(UBYTE Channel_UB, int *pBytesDiff_i);
   CDRVIF_STATUS Channel_AsiRx_Start( UBYTE Channel_UB, ASI_RX_CHANNEL_SETUP_PARAM *pParam_X );
   CDRVIF_STATUS Channel_AsiRx_Stop( UBYTE Channel_UB );
   CDRVIF_STATUS Channel_AsiRx_GetStatus( UBYTE Channel_UB, BOOL32 *pFramingErr_B/*=NULL*/, BOOL32 *pBitrateErr_B/*=NULL*/, BOOL32 *pUnlocked_B/*=NULL*/, BOOL32 *pBadFmt_B/*=NULL*/ );
   CDRVIF_STATUS Channel_AsiRx_GetBytesDiff( UBYTE Channel_UB, int *pBytesDiff_i );
   CDRVIF_STATUS Board_GetArmRevID(ULONG *pArmRevId_UL);
   CDRVIF_STATUS Board_GetArmSSN(ULONG *pArmSSNLS_UL,ULONG *pArmSSNIS_UL,ULONG *pArmSSNMS_UL);
   CDRVIF_STATUS Board_ARMStopHDMI(ULONG HDMIIndex_UL);
   CDRVIF_STATUS Board_ARMStartHDMI(ULONG HDMIIndex_UL,HDMI_SOURCE HDMISource_E,SDI_DATA_FORMAT HDMIPacking_E);
   CDRVIF_STATUS Board_ARMUpgrade( UBYTE *pBuffer_UB, ULONG BufferSize_UL );
   CDRVIF_STATUS Board_ReadHDMIEDID( ULONG HDMIIndex_UL,BYTE *pEEDIDBuffer);
   CDRVIF_STATUS Board_GetHDMISts( ULONG HDMIIndex_UL,ULONG *pHDMISts_UL);
   CDRVIF_STATUS Board_ReadFabInfo(ULONG *pFabInfo_UL, ULONG *pSize_UL);
   CDRVIF_STATUS Board_WriteFabInfo(ULONG *pFabInfo_UL, ULONG *pSize_UL);

   CDRVIF_STATUS Board_LoadDriverProperties();
   CDRVIF_STATUS FillTXDriverProperties(UBYTE ChannelIdx_UB, BOOL32 SWModifyStream_B, ULONG Preload_UL);
   CDRVIF_STATUS Board_WatchdogTimerRegister(ULONG *pRegister_UL);


public:
   virtual BOOL32 SPI_CheckErasing( ULONG StartAddr_UL,ULONG Nb_UL, INotifyProgressFunctor *pSignalProgress_fct );
   virtual BOOL32 SPI_CheckWriting( UBYTE *pBuffer_UB, ULONG StartAddr_UL, ULONG Nb_UL, INotifyProgressFunctor *pSignalProgress_fct );
   virtual void SPI_ReadStatus( UBYTE *pStatus_UB );
   virtual void SPI_SectorErase( ULONG StartAddr_UL, ULONG NbSector_UL, BOOL32 Sector64K_B, INotifyProgressFunctor *pSignalProgress_fct );
   virtual void SPI_WriteEnable();
   virtual void SPI_WriteDisable();
   virtual void SPI_Read( UBYTE *pBuffer_UB, ULONG StartAddr_UL, ULONG Nb_UL, INotifyProgressFunctor *pSignalProgress_fct );
   virtual void SPI_Write( UBYTE *pBuffer_UB, ULONG StartAddr_UL, ULONG Nb_UL, INotifyProgressFunctor *pSignalProgress_fct );
   virtual void SPI_WaitBusy();
   
   
   CDRVIF_STATUS GSPIRead(ULONG ChipID_UL, ULONG RegAddr_UL, ULONG *pValue_UL);
   CDRVIF_STATUS GSPIWrite(ULONG ChipID_UL, ULONG RegAddr_UL, ULONG Value_UL);

protected:
   virtual BOOL32 CheckValidateAccessCode(ULONG ValidateAccessCode_UL);
   BOOL32 Board_IsFWInPROM();
   BOOL32  Board_IsDDRAMOk();
   CDRVIF_STATUS Board_ArmCmd(ULONG Cmd_UL, ULONG *pStatus_UL=NULL);
   static void    MonitorSpiProgress(ULONG ProgressInPrc_UL, void *pUser );


   // GSPI Interface
   // The GSPI interface allows access to on-board SPI chips like GS4911 (genlock), ... at register level
   
   BOARD_INFO m_BoardInfo_X;
   KTHREADID  m_GnlkKThreadId;
   int         m_FwUpdateStepCnt_i;
   int         m_FwUpdateTotalStep_i;
   INotifyProgressFunctor * m_pUserSignalProgress_fct;
};

class CHDDrvBufferQueue : public CBufferQueue
{

   friend class CHDDrvIF;

protected:

   CHDDrvBufferQueue(CHDDrvIF * pOwner_O);

private:

   CHDDrvIF * m_pOwner_O;


};

};
#endif // _CHDDRVIF_H_
