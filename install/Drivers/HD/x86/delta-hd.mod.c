#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x58334a4a, "module_layout" },
	{ 0xadf42bd5, "__request_region" },
	{ 0x281c1af5, "cdev_del" },
	{ 0x45a30ebd, "kmalloc_caches" },
	{ 0x9ad71be8, "cdev_init" },
	{ 0xf9a482f9, "msleep" },
	{ 0xd6147ae2, "up_read" },
	{ 0x5a849319, "mem_map" },
	{ 0xd6ee688f, "vmalloc" },
	{ 0xb85f3bbe, "pv_lock_ops" },
	{ 0x788fe103, "iomem_resource" },
	{ 0xd0d8621b, "strlen" },
	{ 0xd65ba617, "dev_set_drvdata" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0x4aabc7c4, "__tracepoint_kmalloc" },
	{ 0x670c0597, "down_interruptible" },
	{ 0x363e731f, "device_destroy" },
	{ 0x7485e15e, "unregister_chrdev_region" },
	{ 0x999e8297, "vfree" },
	{ 0x1139ffc, "max_mapnr" },
	{ 0x592b9cd7, "down_read" },
	{ 0xe174aa7, "__init_waitqueue_head" },
	{ 0xabc6e780, "pci_set_master" },
	{ 0x2bc95bd4, "memset" },
	{ 0x25957f3c, "kmem_cache_alloc_notrace" },
	{ 0x88941a06, "_raw_spin_unlock_irqrestore" },
	{ 0x4936a2ec, "current_task" },
	{ 0xb72397d5, "printk" },
	{ 0xffc4ddc6, "kthread_stop" },
	{ 0x65ed27b3, "kunmap" },
	{ 0xfaef0ed, "__tasklet_schedule" },
	{ 0x2f287f0d, "copy_to_user" },
	{ 0xb457a6fd, "device_create" },
	{ 0x9545af6d, "tasklet_init" },
	{ 0xfda85a7d, "request_threaded_irq" },
	{ 0x61651be, "strcat" },
	{ 0x658e0cf7, "cdev_add" },
	{ 0x3af98f9e, "ioremap_nocache" },
	{ 0x73876bc, "pci_bus_read_config_word" },
	{ 0x87a2498e, "kmap" },
	{ 0x93fca811, "__get_free_pages" },
	{ 0x6680e453, "get_user_pages" },
	{ 0x108e8985, "param_get_uint" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x4292364c, "schedule" },
	{ 0x3ee8b29e, "pv_cpu_ops" },
	{ 0x40e2ac6, "wake_up_process" },
	{ 0x9bce482f, "__release_region" },
	{ 0x54183804, "pci_unregister_driver" },
	{ 0x6443d74d, "_raw_spin_lock" },
	{ 0x587c70d8, "_raw_spin_lock_irqsave" },
	{ 0x4302d0eb, "free_pages" },
	{ 0xf09c7f68, "__wake_up" },
	{ 0xd2965f6f, "kthread_should_stop" },
	{ 0x506746b6, "getrawmonotonic" },
	{ 0x37a0cba, "kfree" },
	{ 0x19f5af4c, "kthread_create" },
	{ 0xaef59b43, "remap_pfn_range" },
	{ 0x2e60bace, "memcpy" },
	{ 0xe75663a, "prepare_to_wait" },
	{ 0x3285cc48, "param_set_uint" },
	{ 0xedc03953, "iounmap" },
	{ 0x57b09822, "up" },
	{ 0x3f22fd6a, "__pci_register_driver" },
	{ 0xcbfd593e, "put_page" },
	{ 0xcff6d41c, "class_destroy" },
	{ 0x6e9ebfe7, "request_firmware" },
	{ 0xb00ccc33, "finish_wait" },
	{ 0x48848356, "pci_enable_device" },
	{ 0x362ef408, "_copy_from_user" },
	{ 0x55ca06d9, "__class_create" },
	{ 0xe12a9232, "dev_get_drvdata" },
	{ 0x4a217cd0, "release_firmware" },
	{ 0x9e7d6bd0, "__udelay" },
	{ 0x29537c9e, "alloc_chrdev_region" },
	{ 0xf20dabd8, "free_irq" },
	{ 0xe914e41e, "strcpy" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v000011B0d0000880Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000100sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000002sv*sd*bc*sc*i*");
