#ifndef _CBOARDUPGRADE_H_
#define _CBOARDUPGRADE_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "IBoardUpgrade.h"
#include "CHDDrvIF.h"

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

using namespace CHDDrvIF_NS;

class CHDBoardUpgrade : public CHDDrvIF, public IBoardUpgrade
{
public:
   
   CHDBoardUpgrade(ULONG BoardIdx_UL);
   ~CHDBoardUpgrade(){};
   
   char mpBoardName_c[64];
   
   virtual ULONG GetNbUpgradableStuff();
   virtual CDRVIF_STATUS GetUpgradeInfo(ULONG StuffIndex_UL, ULONG* pCurrentFirmware_UL, ULONG* pRequestedFirmware_UL);
   virtual CDRVIF_STATUS UpgradeFirmware(ULONG StuffIndex_UL, INotifyProgressFunctor *pSignalProgress_fct);
   virtual char * GetBoardName();
};



#endif