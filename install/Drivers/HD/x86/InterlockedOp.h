/********************************************************************************************************************//**
 * @internal
 * @file   	InterlockedOp.h
 * @date   	2011/04/22
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2011/04/22  v00.01.0000    cs       Fix a bug in the implementation of InterlockedExchangeAdd
 **********************************************************************************************************************/

#ifndef _INTERLOCKEDOP_H_
#define _INTERLOCKEDOP_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define _InterlockedOr InterlockedOr
#define _InterlockedAnd InterlockedAnd

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

LONG InterlockedIncrement(volatile LONG* Addend_L );
LONG InterlockedDecrement(volatile LONG* Addend_L );
LONG InterlockedExchangeAdd(volatile LONG* Addend_L, LONG Increment_L );
LONG InterlockedOr(volatile LONG* pTarget_L, LONG Value_L );
LONG InterlockedAnd(volatile LONG* pTarget_L, LONG Value_L );
LONG InterlockedExchange(volatile LONG* pTarget_L, LONG Value_L);


/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _INTERLOCKEDOP_H_
