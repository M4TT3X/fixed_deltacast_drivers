/********************************************************************************************************************//**
 * @file   CSkelDrvIF.h
 * @date   2008/01/04
 * @Author Christophe Seyler
 * @brief  Driver abstraction class Windows definition
 *
 * Package : Low-level API
 * Company : DELTACAST
 *
 * @par Copyright notice 
 * Copyright 2007 by DELTACAST.  All rights reserved.
 * All information and source code contained herein is proprietary and confidential to DELTACAST.
 * Any use, reproduction, or disclosure without the written permission of DELTACAST is prohibited.
 *
 **********************************************************************************************************************//*

 ====================================================================================================================
 =  Version History                                                                                                 =
 ====================================================================================================================
   Date     	Version     	Author   Description
 ====================================================================================================================
   2006/11/09  v00.01.0000    cs       Creation of this file
   2007/07/11  v00.02.0000    cs/ot    Add a SAFE_CLOSE in CSkelDrvIF class destructor   
   2007/11/21  v00.03.0000    cs       Add 64-Bit compliance
   2008/01/07  v00.03.0001    cs       Documentation update
   2008/01/09  v00.04.0000    cs       Change the error constants to positive value to avoid conversion types warnings.
   2008/03/17  v00.05.0000    cs       Clear the memory area allocated by AllocateMemory() method
   2009/06/11  v00.06.0000    cs       add a #pragma comment (lib,"setupapi.lib") to avoid the need to manually add the library to
                                       use the class.
   2010/02/11  v00.07.0000    jm       new : Support of memory pool preallocation
                                       Add AllocateCommonBufferPool(), FreeCommonBufferPool() and GetMemMngrInfo()
   2011/02/07  v00.08.0000    cs       Add support for DMABuf link buffer in any cases, using the new DMABufMngr IOCTL.
   2011/03/07    v00.08.0001  cs       Fix a bug in the Windows implementation of CreateDMABuffer(). The function always increased
                                       the process working size whatever the allocation requires it or not.
   2011/03/11  v00.08.0002    cs       Fix a bug in the windows version of DestroyUSBuf() ... IN the call on VirtualFree, 
                                       the size was provided as parameter instead of flags. That was leading to a leak in the
                                       page count usage.
   2011/04/07  v00.08.0003    cs       Get rid off the Memory VirtualLock/Unlock out of the interface. These operation are now
                                       only done in the driver.
   2015/09/08  v00.08.0004    bc       Use AdjustBuffer for Mac
 ====================================================================================================================
 =  TODO List                                                                                                       =
 ====================================================================================================================
 - Split the CSkelDrvIF object into 2 objects because these is only a few number of method really OS-dependant. 
 Then, there are too much redoubling code shared between the Windows and the Linux CSkelDrvIF object.
 To do this, a CDrvInterfaceBaseObj symbol will be conditionally defined to either CDrvInterfaceWindows or 
 CDrvInterfaceLinux.
 By this way, all methods not OS-dependent will be highlighted.

 **********************************************************************************************************************/

#ifndef _CDRVINTERFACE_H_
#define _CDRVINTERFACE_H_


/***** INCLUDES SECTION ***********************************************************************************************/
#include "ISkelDrvIF.h"
#include "OS.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/* Define BAR_ACCESS_MODE to one of those in your toolchain options */
#define BAR_ACCESS_MODE_MIXED       0  /* BAR's are mapped for debugging purpose, but IOCTL's are used for register accesses */
#define BAR_ACCESS_MODE_MAP_ONLY    1  /* BAR's are mapped, this mapping is used for register accesses */
#define BAR_ACCESS_MODE_IOCTL_ONLY  2  /* BAR's are not mapped at all, only IOCTL accesses can be used */

#ifndef BAR_ACCESS_MODE
#define BAR_ACCESS_MODE             BAR_ACCESS_MODE_MIXED
#endif

/***** MACROS DEFINITION **********************************************************************************************/

#define IORead8(offset)          IORead(offset,IOCTL_IO_ACCESS_TYPE_8BIT)
#define IORead16(offset)         IORead(offset,IOCTL_IO_ACCESS_TYPE_16BIT)
#define IORead32(offset)         IORead(offset,IOCTL_IO_ACCESS_TYPE_32BIT)
#define IOWrite8(offset,value)   IOWrite(offset,value,IOCTL_IO_ACCESS_TYPE_8BIT)
#define IOWrite16(offset,value)  IOWrite(offset,value,IOCTL_IO_ACCESS_TYPE_16BIT)
#define IOWrite32(offset,value)  IOWrite(offset,value,IOCTL_IO_ACCESS_TYPE_32BIT)


#define SHREG32_GET(idx,pval)             ShadowReg32_Get(idx,pval)
#define SHREG32_SET(idx,val)              ShadowReg32_Set(idx,val)
#define SHREG32_SETBIT(idx,val)           ShadowReg32_SetBit(idx,val)
#define SHREG32_SETMSK(idx,val,msk)       ShadowReg32_SetMsk(idx,val,msk)
#define SHREG32_CLRBIT(idx,val)           ShadowReg32_ClrBit(idx,val)
#define SHREG32_SET_AUTOCLR_BIT(idx,val)  ShadowReg32_SetAutoClearBit(idx,val)
#define UNSAFE_SHREG32_SET(idx,val)       ShadowReg32_Set(idx,val)

/***** STRUCUTRES DEFINITION ******************************************************************************************/


/***** CLASSES DECLARATIONS *******************************************************************************************/


class CDMABuffer : public IDMABuffer
{
public:

   TYPE GetType() {return m_Type_e;}

   virtual ~CDMABuffer() {};
   DMABUFID GetDMABufID() {return DMABUFID_Create(GetDMABufType(), GetID());}
   

protected:

   virtual ULONG GetID()=0;
   virtual ULONG  GetDMABufType()=0;
   CDMABuffer(TYPE Type_e) {m_Type_e=Type_e;}


protected:

   TYPE m_Type_e;

};

class  CDMABuf_USBuf : public CDMABuffer
{
friend class CSkelDrvIF;
public:

   CDMABuf_USBuf(ULONG ID_UL, void *pBuf_v, ULONG Size_UL) : CDMABuffer(TYPE_US_BUFFER)
   {
      m_ID_UL=ID_UL;

      m_Size_UL=m_AllocatedSize_UL=Size_UL;
      m_pBuf_v=m_pAllocatedBuf_v = pBuf_v;
   }

   ULONG GetID() {return m_ID_UL;}
   ULONG GetSize() {return m_Size_UL;}
   UBYTE * GetBuffer() {return (UBYTE*)m_pBuf_v;};

   UBYTE * GetAllocatedBuffer() {return (UBYTE*)m_pAllocatedBuf_v;};
   ULONG   GetAllocatedSize() {return m_AllocatedSize_UL;}

protected:

   ULONG  GetDMABufType() {return DMABUF_TYPE_USBUF;}

   void AdjustBuffer(ULONG Offset_UL)
   {
#if defined(__linux__) || defined(__APPLE__)
      m_Size_UL=m_AllocatedSize_UL - 4096; // Skip the entire first page to keep a 'page-aligned' buffer
      m_pBuf_v=&((UBYTE*)m_pAllocatedBuf_v)[Offset_UL];
#endif
   }

   ULONG m_ID_UL;
   ULONG m_AllocatedSize_UL;
   void *m_pAllocatedBuf_v;

   ULONG m_Size_UL;
   void *m_pBuf_v;

};

class CDMABuf_CBuf : public CDMABuffer
{
protected:
   CDMABuf_CBuf(ALLOC_MEM_DESCR		*pCommonBufDescr_X, TYPE Type_e	) : CDMABuffer(Type_e)
   {
      m_CommonBufDescr_X=*pCommonBufDescr_X;
   }
public:
   CDMABuf_CBuf(ALLOC_MEM_DESCR		*pCommonBufDescr_X	) : CDMABuffer(TYPE_COMMON_BUFFER)
   {
      m_CommonBufDescr_X=*pCommonBufDescr_X;
   }

   ALLOC_MEM_DESCR * GetDescr() {return &m_CommonBufDescr_X;}

   ULONG GetID() {return m_CommonBufDescr_X.CommonBufID_UL;}
   ULONG GetSize() {return m_CommonBufDescr_X.Size_UL;}
   UBYTE * GetBuffer() {return m_CommonBufDescr_X.pAddr_UB;};

private:
   ULONG  GetDMABufType() {return DMABUF_TYPE_COMMONBUF;}


   ALLOC_MEM_DESCR		m_CommonBufDescr_X;	 	 
}; 

class CDMABuf_PreallocCBuf : public CDMABuf_CBuf
{
public: 
   CDMABuf_PreallocCBuf(ALLOC_MEM_DESCR *pCommonBufDescr_X, TYPE Type_e)
      : CDMABuf_CBuf(pCommonBufDescr_X, Type_e)
   {

   }
};



/*********************************************************************************************************************//**
* @class CSkelDrvIF
* @brief This is the main low level interface to the driver.
*
* It provides a set of methods to interface the low level behavior of the driver. Some OS dependent objects
* and OS functions calls are involved in this class and abstracted at this level.
*
* @remark This class is OS dependent.  
*************************************************************************************************************************/
class CSkelDrvIF : public ISkelDrvIF
{
protected:

   BOOL32 OpenBoard(DRIVER_ID_TYPE DrvGuid_X,ULONG BoardIdx_UL,ULONG ResMappingParameters_UL=0x3F, ULONG DrvSubType_UL=0);

public:  

   static ULONG GetVersion() {return CDRVIF_VERSION;}
   static ULONG GetNbBoard(DRIVER_ID_TYPE DrvGuid_X, ULONG DrvSubType_UL=0);
   static BOARD_HANDLE FindBoard(DRIVER_ID_TYPE DrvGuid_X,ULONG BrdIdx_UL, ULONG DrvSubType_UL=0);
#ifdef __APPLE__
   static ULONG GetBoardSubType(BOARD_HANDLE Board_H);
#endif
   CDRVIF_STATUS GetDriverVersion(ULONG *pVersion_UL);
   CDRVIF_STATUS GetBoardRefCount(ULONG *pRefCount_UL);  
   CDRVIF_STATUS GetBoardNoAwakeRefCount(ULONG *pRefCount_UL);
  
   static BOOL32 IsValidDrvEvent(DRVEVENT *pDrvEvent_X){if (!pDrvEvent_X) return FALSE; else return (pDrvEvent_X->EventID_e!=INVALID_EVENT_ID);}
   static EVENT_ID RetrieveEventID(DRVEVENT *pDrvEvent_X) {return pDrvEvent_X->EventID_e;}

#ifdef WIN32
   static HANDLE  RetrieveEventHandle(DRVEVENT *pDrvEvent_X) {return pDrvEvent_X->Event_H;}
#endif

   CSkelDrvIF(DRIVER_ID_TYPE DrvGuid_X,ULONG BoardIdx_UL,ULONG ResMappingParameters_UL=0x3F, ULONG DrvSubType_UL=0);
   virtual ~CSkelDrvIF();

   BOOL32     BoardIsOpened();

   CDRVIF_STATUS    CreateDrvEvent(DRVEVENT *pDrvEvent_X, BOOL32 ManualReset_B=FALSE,ULONG AssociatedIntSrc_UL=0, BOOL32 EnableIntSrcInDPC_B=TRUE, void * pParam_v=NULL);
   CDRVIF_STATUS    WaitOnDrvEvent(DRVEVENT *pDrvEvent_X,ULONG Timeout_UL=INFINITE);
   CDRVIF_STATUS    CloseDrvEvent(DRVEVENT *pDrvEvent_X);
   CDRVIF_STATUS    SetDrvEvent(DRVEVENT *pDrvEvent_X);
   BOOL32           ResetDrvEvent(DRVEVENT *pDrvEvent_X);
   CDRVIF_STATUS    SetDrvEventAssociatedIntSrc(DRVEVENT *pDrvEvent_X,ULONG AssociatedIntSrc_UL=0, BOOL32 EnableIntSrcInDPC_B=TRUE);

   CDRVIF_STATUS    GetMemMngrInfo(IOCTL_GETPREALLOCINFO_COMMON_BUFFER_PARAM *pMemMngrInfo_X);
   CDRVIF_STATUS    ReservePoolSide(ULONG PoolId_UL, ULONG PoolSide_UL, ULONG NbOfBuffers_UL, ULONG pBuffersSize_UL[COMMON_BUFFER_MAX_SLAVE+1]);
   CDRVIF_STATUS    FreePoolSide(ULONG PoolId_UL, ULONG PoolSide_UL);
   
   CDRVIF_STATUS    ReservePreallocatedMemory(ULONG Size_UL, ULONG PoolId_UL, ULONG PoolSide_UL, ALLOC_MEM_DESCR *pDescr_X);    //Reserve a preallocated buffer + map buffer
   CDRVIF_STATUS    AllocateMemory(ULONG Size_UL,ALLOC_MEM_DESCR *pDescr_X);     //Allocate a common buffer + map buffer. 
   CDRVIF_STATUS    FreeMemory(ALLOC_MEM_DESCR *pDescr_X);                       //Unmap buffer + free common buffer      

   CDRVIF_STATUS    ReservePreallocatedBuffer(ULONG Size_UL, ULONG PoolId_UL, ULONG PoolSide_UL, ULONG *pBufId_UL);
   CDRVIF_STATUS    AllocateCommonBuffer(ULONG Size_UL, ULONG *pBufId_UL);    
   CDRVIF_STATUS    FreeCommonBuffer( ULONG BufferID_UL );                    

   CDRVIF_STATUS    GetCommonBuffer(ULONG BufID_UL, COMMON_BUFFER_DESCR *pDescr_X);          //Map buffer   TODO : rename : MapCommonBuffer?
   CDRVIF_STATUS    ReleaseCommonBuffer( ULONG BufferID_UL,COMMON_BUFFER_DESCR *pDescr_X);   //Unmap buffer TODO : rename : UnmapCommonBuffer?
   CDRVIF_STATUS    LinkCommonBuffer( IOCTL_LINK_COMMON_BUFFER_PARAM *pLink_X );
      
   IDMABuffer*   CreateDMABuffer(ULONG Size_UL, IDMABuffer::TYPE Type_e = IDMABuffer::TYPE_AUTO);
   void		     DestroyDMABuffer(IDMABuffer * pBuffer_O);
   CDRVIF_STATUS LinkDMABuffer(IDMABuffer * pMaster_O, IDMABuffer * pSlave_O);


   CDRVIF_STATUS DMABuf_GetFlags(DMABUFID BufID, ULONG *pFlags_UL);
   CDRVIF_STATUS DMABuf_SetBitFlags(DMABUFID BufID, ULONG *pFlags_UL);
   CDRVIF_STATUS DMABuf_ClrBitFlags(DMABUFID BufID, ULONG *pFlags_UL);

   CDRVIF_STATUS SetDPCIntMask(ULONG Mask_UL);
   CDRVIF_STATUS ClrDPCIntMask(ULONG Mask_UL);
   CDRVIF_STATUS GetIntSrc(ULONG *pIntSrc_UL);

   BOARD*        GetBoardPtr();
   PCI_RESOURCE* GetPCIResourcesPtr();
   CDRVIF_STATUS GetRegisterInfo(PMM32_REGISTER pRegPtr, UBYTE *pPCIBar_UB, ULONG *pOffset_UL);

protected:
   MM32_REGISTER** mppRegisterCache_X;
   ULONG          mRegisterCacheBarCount_UL, mRegisterCacheBarSize_UL;

   void           Register_CreateCache(ULONG BarCount_UL, ULONG BarSize_UL);
   void           Register_DestroyCache();

   CDRVIF_STATUS  Register_Operation(IOCTL_REGISTER_ACCESS_TYPE Type_E, ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL);

public:
   PMM32_REGISTER Register_Factory(ULONG Bar_UL, ULONG Offset_UL);

   CDRVIF_STATUS Register_Get(ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL) { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_GET, Bar_UL, Offset_UL, pValue_UL); }
   CDRVIF_STATUS Register_Set(ULONG Bar_UL, ULONG Offset_UL, ULONG  Value_UL)  { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_SET, Bar_UL, Offset_UL, &Value_UL); }
   CDRVIF_STATUS Register_Set(ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL) { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_SET, Bar_UL, Offset_UL, pValue_UL); }
   CDRVIF_STATUS Register_And(ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL) { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_AND, Bar_UL, Offset_UL, pValue_UL); }
   CDRVIF_STATUS Register_And(ULONG Bar_UL, ULONG Offset_UL, ULONG Value_UL)   { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_AND, Bar_UL, Offset_UL, &Value_UL); }
   CDRVIF_STATUS Register_Or (ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL) { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_OR, Bar_UL, Offset_UL, pValue_UL); }
   CDRVIF_STATUS Register_Or (ULONG Bar_UL, ULONG Offset_UL, ULONG Value_UL)   { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_OR, Bar_UL, Offset_UL, &Value_UL); }
   CDRVIF_STATUS Register_Xor(ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL) { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_XOR, Bar_UL, Offset_UL, pValue_UL); }
   CDRVIF_STATUS Register_Xor(ULONG Bar_UL, ULONG Offset_UL, ULONG Value_UL)   { return Register_Operation(IOCTL_REGISTER_ACCESS_TYPE_XOR, Bar_UL, Offset_UL, &Value_UL); }

   CDRVIF_STATUS DriverProperty_GetValue(ULONG Property_UL, ULONG *pValue_UL);  
   CDRVIF_STATUS DriverProperty_SetValue(ULONG Property_UL, ULONG Value_UL);    
   CDRVIF_STATUS DriverProperty_SetBit(ULONG Property_UL, ULONG Bit_UL);  
   CDRVIF_STATUS DriverProperty_ClrBit(ULONG Property_UL, ULONG Bit_UL);  
 
   CDRVIF_STATUS ShadowReg32_SetBit(ULONG Idx_UL, ULONG Bit_UL, ULONG *pValue_UL=NULL);
   CDRVIF_STATUS ShadowReg32_SetMsk(ULONG Idx_UL, ULONG Bit_UL, ULONG Msk_UL, ULONG *pValue_UL=NULL);
   CDRVIF_STATUS ShadowReg32_SetAutoClearBit(ULONG Idx_UL, ULONG Bit_UL, ULONG *pValue_UL=NULL);
   CDRVIF_STATUS ShadowReg32_ClrBit(ULONG Idx_UL, ULONG Bit_UL, ULONG *pValue_UL=NULL);
   CDRVIF_STATUS ShadowReg32_Get(ULONG Idx_UL, ULONG *pValue_UL);
   CDRVIF_STATUS ShadowReg32_Set(ULONG Idx_UL, ULONG Value_UL);

   CDRVIF_STATUS KThread_Wakeup(KTHREADID KThreadID, ULONG SyncParam_UL);
   CDRVIF_STATUS KThread_Stop(KTHREADID KThreadID);
   CDRVIF_STATUS KThread_SetPeriod(KTHREADID KThreadID, ULONG Timeout_UL);

   CDRVIF_STATUS SetDbgMask(ULONG ModuleID_UL, ULONG Mask_UL);
   
   CDRVIF_STATUS GetCurrentProcessGuid(ULONG *pProcessGuid_UL);
   CDRVIF_STATUS GetProcessGuidList( ULONG pProcessGuidList_UL[MAX_PROCESSES] );

   CDRVIF_STATUS DeviceIOCtl( ULONG Cmd_UL,const void * pOutBuffer_UB, ULONG OutSize_UL, void * pInBuffer, ULONG InSize_UL );
   
   /* Returns TRUE if a wakeup event occured recently 
    *  => All resources need to be freed
    *  => Call AcknowledgeWakeup();
    *  => Recreate stuff if you want to */
   BOOL32 IsAwakening();
   void   AcknowledgeWakeup();
   BOOL32 IsRemoved();
   BOOL32 IsLowPowerMode();

protected:

   CDMABuf_USBuf * CreateUSBuffer(ULONG Size_UL, void *pUserSpaceBuffer_v);
   void DestroyUSBuffer(CDMABuf_USBuf * pUSBuf_O);

   
#ifdef WIN32
   EVENT_ID OpenEvent(HANDLE Event_H);
   CDRVIF_STATUS     CloseEvent(EVENT_ID EventID_e);
   CDRVIF_STATUS    RegisterEvent(EVENT_ID EventID_e, ULONG IntSource_UL);
#else
   EVENT_ID  OpenEvent(void);
   CDRVIF_STATUS      CloseEvent(EVENT_ID EventID_e);
   CDRVIF_STATUS     SetEvent(EVENT_ID  EventID_e);
   BOOL32            ResetEvent(EVENT_ID  EventID_e);
   CDRVIF_STATUS     RegisterEvent(EVENT_ID EventID_e, ULONG IntSrc_UL);
   CDRVIF_STATUS     WaitOnEvent(EVENT_ID  EventID_e,ULONG TimeOut_UL=INFINITE);
#endif    
  
   BOARD  m_Board_X;
   BOOL32 DrvFeatures_Check(ULONG Features_UL);
   inline ULONG DrvFeatures_Get() {return m_DriverFeatures_UL;}
   ULONG m_DriverFeatures_UL; // Local copy of the Driver features for faster access.
   BOOL32 mIsAwakening_B, mIsRemoved;
   DRV_POWER_STATE mPowerState_E;

};

#endif // _CDRVINTERFACE_H_
