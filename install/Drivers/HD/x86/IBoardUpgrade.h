/********************************************************************************************************************//**
 * @file   	IBoardUpgrade.h
 * @date   	2013/03/06
 * @author 	bc
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/16  v00.01.0000    bc       Creation of this file

 **********************************************************************************************************************/

#ifndef _IBOARDUPGRADE_H_
#define _IBOARDUPGRADE_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"
#include "ISkelDrvIF.h"
#include "INotifyProgressFunctor.h"

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
#if defined (__linux__)||(__APPLE__)
#define DRV_EXPORT 
#else
#define DRV_EXPORT WINAPI
#endif

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

class IBoardUpgrade 
{
public:
   virtual ~IBoardUpgrade(){};
   virtual ULONG GetNbUpgradableStuff()=0;
   virtual CDRVIF_STATUS GetUpgradeInfo(ULONG StuffIndex_UL, ULONG* pCurrentFirmware_UL, ULONG* pRequestedFirmware_UL)=0;
   virtual CDRVIF_STATUS UpgradeFirmware(ULONG StuffIndex_UL, INotifyProgressFunctor *pSignalProgress_fct)=0;
   virtual char * GetBoardName()=0;

};

#ifdef __cplusplus
extern "C" {
#endif
   
ULONG DRV_EXPORT GetNbBoards();
   
IBoardUpgrade * DRV_EXPORT GetIBoardUpgrade(ULONG BoardIdx_UL);
   
void DRV_EXPORT ReleaseIBoardUpgrade(IBoardUpgrade * pBoardUpgrade_I);
   
#ifdef __cplusplus
}
#endif

#endif
