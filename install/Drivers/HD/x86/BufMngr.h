/********************************************************************************************************************//**
 * @file   BufMngr.h
 * @date   2008/01/07
 * @Author cs
 * @brief  
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/02/06  v00.01.0000    cs       Creation of this file
   2008/01/02  v00.02.0000    cs       Add BufferQueue Lock/Unlock mechanism
   2008/01/07  v00.02.0001    cs       Documentation update
   2008/12/04  v00.02.0002    cs       Documentation update
   2009/07/01  v00.02.0003    gt       New buffer queue feature : early transfert
   2009/11/05  v00.03.0000    cs/gt    The early transfer feature induces a potential concurrent access to a call to
                                       BufMngr_BufQueueLockBySystem for the same buffer queue:
                                       - In ealry transfer mode, the 'user' BufMngr_BufQueueUnlock() implicitly calls
                                         the BufMngr_Transfer() function. A race condition could occurs between this user
                                         unlock and an interrupt. In this case, the interrupt could leads to a "Unable to
                                         lock buffer queue" (because the user unlock has taken the spin lock ownership).
                                         even though the user unlock doesn't trig a transfer and release the buffer queue
                                         after a while. In this case, the BufMnfr_BufQueueLockBySystem/UnlockBySystem
                                         should be atomic when it's caused by the user unlock.

                                      The way to reach a correct way and avoid spinlock reentrance is:
                                      - remove spinlock ownership attempts outside the BufMnfr_BufQueueLockBySystem/UnlockBySystem
                                      - Outside the BufMngr_Transfer() function, surrounds the BufMnfr_BufQueueLockBySystem/UnlockBySystem
                                        by the Spinlock_Acquire/relase to get the same behavior as before.
                                      - Inside the  BufMngr_Transfer() function, places the spinlock acquire and release to achieve
                                        an atomic behaviour when needed.

                                     Remark : It was not possible to directly remove the spinlock acquire/release from the 
                                              BufMnfr_BufQueueUnlockBySystem and just surrounds the call because, internally, the unlock
                                              function calls the BufQueueClose() outside the spinlocked area. A BOOL32 parameter has been 
                                              added the the BufQueueClose() function to specify if it must use spinlock or not (avoiding
                                              reentrance)

 **********************************************************************************************************************/

#ifndef _BUFMNGR_H_
#define _BUFMNGR_H_

/***** ADDITIONAL DOXUGEN DOCUMENTATION *******************************************************************************/


/** @page BUFMNGR_MAIN_PAGE Data buffer manager 

\section BUFMNGR_MAIN_PAGE_SECT_1 1. Data buffer management concept

All data transfers are handled by the the buffer manager module. It allows user to create list of buffers used
for DMAs, and setup these DMAs to be triggered by specified event (interrupt). These mechanisms involves several
kind of "objects" :
- Memory buffers
- Buffer queues

\subsection BUFMNGR_MAIN_PAGE_SECT_1_1 1.1 Memory buffers
*/

/***** INCLUDES SECTION ***********************************************************************************************/

#include "BufMngrTypes.h"



/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define BUFFER_LOCK_REQUESTOR_USER              0x010000
#define BUFFER_LOCK_REQUESTOR_USER_EXCLUSIVE    0x020000
#define BUFFER_LOCK_REQUESTOR_DMA_MANAGER       0x040000
#define BUFFER_LOCK_REQUESTOR_ID_MASK           0x00FFFF
#define BUFFER_LOCK_REQUESTOR_MASK              0xFF0000


/* Lock Status :

33222222 22221111 11111100 000000000 
10987654 32109876 54321098 764543210

--USER LOCK BITS--

*/

#define BUFFER_STS_LOCK_BY_USER_MASK            0x0000FFFF
#define BUFFER_STS_LOCK_BY_SYSTEM               0x00020000
#define BUFFER_STS_FILLED_WITH_RCV_DATA         0x00100000
#define BUFFER_STS_FILLED_WITH_XMT_DATA         0x00200000
#define BUFFER_STS_FILLED_WITH_XFER_DATA_MASK   0x00F00000
#define BUFFER_STS_DATA_MARKED_AS_INVALID       0x01000000
#define BUFFER_STS_DATA_MARKED_AS_OUTSIDE_TC    0x02000000
#define BUFFER_STS_INVALID_DATA                 0x04000000

#define BUFFER_QUEUE_FLAGS_OPENED            0x80000000 ///< This flag specifies that the buffer queue is opened
#define BUFFER_QUEUE_FLAGS_MARKED_TO_CLOSE   0x40000000 ///< This flag specifies that the buffer queue was attempts to be close when it was locked
#define BUFFER_QUEUE_FLAGS_LOCKED_BY_SYSTEM  0x20000000 ///< This flag specifies that the buffer queue is locked by the system
#define BUFFER_QUEUE_FLAGS_OPENING           0x10000000 ///< This flag is set during the BQ opening operation.


#define MAX_BUFFER_PER_QUEUE 32
#define MAX_BUFQUEUE_GROUPID 8
#define NB_BUFFER_QUEUE 24

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct _BUFFER
{
   ULONG Type_UL;                         // Buffer Type (NOT_USED by default)
//    COMMON_BUFFER_DESCR *pBufferDescr_X;   // Keeps the descriptor for the associated common buffer 
//    ULONG CommonBufIdx_UL;                 // Keeps the associated Common Buffer ID (for CommonBufMngr Locking/Unlocking)
   DMABUFID DMABufID_UL;
   ULONG BufferNumber_UL;                 // Keeps the buffer number for reception and transmission ordering 
   ULONG LockStatus_UL;                   // Buffer lock status
   BUFFER_HEADER *pBufHdr_X;              //
} BUFFER;


/**********************************************************************************************************************//**
 * @struct BUFFER_QUEUE
 * @brief  This structure contains all needed data to describe a buffer queue.
 *************************************************************************************************************************/
typedef struct _BUFFER_QUEUE
{
   ULONG          BufQueueFlags_UL;                         // Buffer queue flags
   BUFFER        *pBuf_X;                                   // This pointer points to the buffer list (either to the pLocalBuf_X or other in the case of linked buffer queue)
   BUFFER         pLocalBuf_X[MAX_BUFFER_PER_QUEUE];        // List of buffers attached to buffer queue.
   PID_TYPE       PID_UL;                                   // Owner's PID
 
   BUFFER_QUEUE_OPEN_PARAM Param_X;

   ULONG          BufCounter_UL;
   ULONG          IntHandledCount_UL;

   ULONG          MaxBufferSize_UL;                         // Keep the minimal size of all attached buffers.
 
   BUFFER_QUEUE_STATUS Status_X;

   BOOL32           EarlyInProgress_B;
#undef CNT_ONBRD_FILLED_BUF //TODO clean
#ifdef CNT_ONBRD_FILLED_BUF  
   ULONG          OnBoardFilledBuffers_UL;                  // Count the buffers transfered on board (increment on DMA Done, decremented on trigger interrupt */
#endif
   ULONG          LinkedQueueHandle_UL;
} BUFFER_QUEUE;

struct _DEVICE_EXTENSION;


/**********************************************************************************************************************//**
 * @struct BUFFER_MNGR_DATA
 * @brief This structure groups the needed data for Buffer Manager to add to the Device Extenstion. 
 *************************************************************************************************************************/


typedef struct _BUFFER_MNGR_DATA
{
   BUFFER_QUEUE pBufQueue_X[NB_BUFFER_QUEUE];   // List of buffer queues
   KOBJ_SPINLOCK SpinLock_KO;                   // Spinlock to protect shared data at buffer manager level.

   ULONG pGrpIDRefCount_UL[MAX_BUFQUEUE_GROUPID];

   BOOL32 (*CallBack_BufMngrTxIntHandler_fct)(PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL);
   BOOL32 (*CallBack_BufMngrRxIntHandler_fct)(PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL);
   BOOL32 (*CallBack_BufMngrIsTransferAllowed_fct)(PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL, BOOL32 Early_B);
   void (*CallBack_BufMngrOnGrpProcessing_fct)(PDEVICE_EXTENSION pdx,ULONG GrpID_UL, ULONG RefCount_UL, BOOL32 FromDmaDone_B, ULONG *pBQUserParam_UL);


} BUFFER_MNGR_DATA;

typedef struct
{
   ULONG LockType_UL;
   ULONG LockRequestMode_UL;
} BUFFER_LOCK_PARAM;

typedef struct
{
   ULONG UnlockRequestMode_UL;
} BUFFER_UNLOCK_PARAM;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void  BufMngr_DeviceAdd(PDEVICE_EXTENSION pdx);
void  BufMngr_CleanUp( PDEVICE_EXTENSION pdx,PID_TYPE PID_UL, BOOL32 DevicePresent_B);
IOCTL_STATUS  BufMngr_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,PID_TYPE PID_UL);

ULONG BufMngr_BufQueueOpen(PDEVICE_EXTENSION pdx,BUFFER_QUEUE_OPEN_PARAM* pParam_X,PID_TYPE PID_UL);
BOOL32  BufMngr_BufQueueClose(PDEVICE_EXTENSION pdx,ULONG FrmBufQueueHandle_UL, BOOL32 Safe_B);
BOOL32  BufMngr_AddBuf(PDEVICE_EXTENSION pdx,ULONG FrmBufQueueIdx_UL,ULONG CommonBufIdx_UL);
BOOL32  BufMngr_BufQueueReset(PDEVICE_EXTENSION pdx,ULONG FrmBufQueueHandle_UL,ULONG ResetParameter_UL);
BOOL32  BufMngr_BufQueueUpdate(PDEVICE_EXTENSION pdx,ULONG FrmBufQueueHandle_UL,BUFFER_QUEUE_UPDATE_PARAM* pParam_X,PID_TYPE PID_UL);
BOOL32  BufMngr_BufQueueLink(PDEVICE_EXTENSION pdx,BUFFER_QUEUE_LINK_PARAM* pParam_X);
BOOL32  BufMngr_BufQueueGetStatus(PDEVICE_EXTENSION pdx,ULONG BufQueueHandle_UL, BUFFER_QUEUE_STATUS* pBufQueueSts_X);
BOOL32  BufMngr_BufQueueLockBySystem(PDEVICE_EXTENSION pdx,ULONG BufQueueHandle_UL);
BOOL32  BufMngr_BufQueueUnlockBySystem(PDEVICE_EXTENSION pdx,ULONG BufQueueHandle_UL);

ULONG BufMngr_BufLock(PDEVICE_EXTENSION pdx,ULONG FrmBufQueueIdx_UL, ULONG FrameCount_UL, BUFFER_LOCK_PARAM *pParam_X);
BOOL32  BufMngr_BufUnlock(PDEVICE_EXTENSION pdx,ULONG FrmHandle_UL, BUFFER_UNLOCK_PARAM *pParam_X);

void BufMngr_TransfertHandler(PDEVICE_EXTENSION pdx,ULONG IntSource_UL, BOOL32 Early_B);
void  BufMngr_DPCHandler(PDEVICE_EXTENSION pdx,ULONG IntSource_UL);
ULONG BufMngr_DMADoneHandler(PDEVICE_EXTENSION pdx, ULONG DMARequestDoneHandle_UL, DMA_REQUEST_USER_DATA *pDMARequestUserData_X);
void  BufMngr_Abort(PDEVICE_EXTENSION pdx);


BUFFER_QUEUE * BufMngr_GetBufferQueue(PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL);
BUFFER       * BufMngr_GetBuffer(PDEVICE_EXTENSION pdx, BUFHANDLE BufHandle);

#endif // _BUFMNGR_H_

