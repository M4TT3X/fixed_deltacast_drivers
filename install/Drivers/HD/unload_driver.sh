#!/bin/bash

drivername="delta-hd"
echo "Unloading the ${drivername} module"
/sbin/rmmod ${drivername}

echo "Deleting device nodes..."
rm /dev/${drivername}*