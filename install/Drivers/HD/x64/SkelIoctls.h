/********************************************************************************************************************//**
 * @file   	SkelIoctls.h
 * @date   	2010/06/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * This file contains the definition of the provided IOCTL code. The common IOCTL Code definition has this following 
 * layout :
 *
 * #define IOCTL_MY_IOCTL CTL_CODE(FILE_DEVICE_UNKNOWN, NUMBER_HERE, METHOD_BUFFERED, FILE_ANY_ACCESS)
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/16  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _SKELIOCTLS_H_
#define _SKELIOCTLS_H_

/***** DOCUMENTATION **************************************************************************************************/

/** @defgroup IOCTLS_GROUP Available IO Control Codes

   The following list gives all available IOCTL codes that build the driver interface.
*/

/***** INCLUDES SECTION ***********************************************************************************************/

#include "Register.h"
#include "CommonBufMngrTypes.h"
#include "USBufMngrTypes.h"
#include "DMABufMngrTypes.h"
#include "EventMngrTypes.h"
#include "PropertiesMngrTypes.h"
#include "ShadowRegistersTypes.h"
#include "KThreadMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define NB_PCI_BARS 6
#define MAX_PROCESSES   16 //TODO_THUMB : should be increased?  ///< Defines the maximum number of processes that can be connected to a board at the same time.

/***** MACROS DEFINITIONS *********************************************************************************************/

#ifdef __linux__
#define MYDRV_IOC_MAGIC 0xDB
#define MYDRV_IOC_MAXNR 0xFFF

#define DEFINE_IOCTL_WR(code,size)     (_IOW(MYDRV_IOC_MAGIC,code, size))
#define DEFINE_IOCTL_RD(code,size)     (_IOR(MYDRV_IOC_MAGIC,code, size))
#define DEFINE_IOCTL_RDWR(code,size)   (_IOWR(MYDRV_IOC_MAGIC,code, size))
#define DEFINE_IOCTL(code)             (_IO(MYDRV_IOC_MAGIC,code))

#elif defined(__APPLE__)

#define DEFINE_IOCTL(code)             (code)
#define DEFINE_IOCTL_WR(code,size)     DEFINE_IOCTL(code)
#define DEFINE_IOCTL_RD(code,size)     DEFINE_IOCTL(code)
#define DEFINE_IOCTL_RDWR(code,size)     DEFINE_IOCTL(code)

#else

#ifndef CTL_CODE
#pragma message("CTL_CODE undefined. Include winioctl.h or wdm.h")
#endif

#define MYDRV_IOC_BASE 0x900
#define DEFINE_IOCTL_WR(code,size)     CTL_CODE(FILE_DEVICE_UNKNOWN, MYDRV_IOC_BASE+code, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define DEFINE_IOCTL_RD(code,size)     CTL_CODE(FILE_DEVICE_UNKNOWN, MYDRV_IOC_BASE+code, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define DEFINE_IOCTL_RDWR(code,size)   CTL_CODE(FILE_DEVICE_UNKNOWN, MYDRV_IOC_BASE+code, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define DEFINE_IOCTL(code)             CTL_CODE(FILE_DEVICE_UNKNOWN, MYDRV_IOC_BASE+code, METHOD_BUFFERED, FILE_ANY_ACCESS)
#endif

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum 
{
   IOCTL_PCI_RESOURCES_GET,
   IOCTL_PCI_RESOURCES_RELEASE
} IOCTL_PCI_RESOURCES_ACCESS_TYPE;

#pragma pack(push, 4)

typedef struct
{
   ULONG                            StructSize_UL;
   IOCTL_PCI_RESOURCES_ACCESS_TYPE  Type_E;
   PCI_RESOURCE_IOCTL               pPCIResources_X[NB_PCI_BARS];
} IOCTL_PCI_RESOURCES_ACCESS_PARAM;

typedef enum 
{
   IOCTL_IO_ACCESS_TYPE_RD_32BIT,
   IOCTL_IO_ACCESS_TYPE_RD_16BIT,
   IOCTL_IO_ACCESS_TYPE_RD_8BIT,
   IOCTL_IO_ACCESS_TYPE_WR_32BIT,
   IOCTL_IO_ACCESS_TYPE_WR_16BIT,
   IOCTL_IO_ACCESS_TYPE_WR_8BIT
} IOCTL_IO_ACCESS_TYPE;

#define IOCTL_IO_ACCESS_TYPE_IS_READ(x)   ((x) < IOCTL_IO_ACCESS_TYPE_WR_32BIT)

typedef struct
{
   ULONG                StructSize_UL;
   IOCTL_IO_ACCESS_TYPE Type_E;
   UWORD                Offset_UW;
   ULONG                Value_UL;
} IOCTL_IO_ACCESS_PARAM;

typedef struct
{
   ULONG StructSize_UL;
   ULONG ModuleID_UL;
   ULONG DbgMask_UL;
} IOCTL_SET_DBGMASK_PARAM;

typedef enum 
{
   IOCTL_PROCESS_GUID_ACCESS_TYPE_GET_CURRENT,
   IOCTL_PROCESS_GUID_ACCESS_TYPE_GET_ACTIVE_LIST,
} IOCTL_PROCESS_GUID_ACCESS_TYPE;

typedef struct
{
   ULONG                          StructSize_UL;
   IOCTL_PROCESS_GUID_ACCESS_TYPE Type_E;
   union
   {
      ULONG          CurGuid_UL;
      ULONG          pActiveGuidList_UL[MAX_PROCESSES];
   };
} IOCTL_PROCESS_GUID_ACCESS_PARAM;

typedef enum
{
   IOCTL_POWER_SLEEP_CONTEXT_ACCESS_TYPE_READ,
   IOCTL_POWER_SLEEP_CONTEXT_ACCESS_TYPE_WRITE
} IOCTL_POWER_SLEEP_CONTEXT_ACCESS_TYPE;

/* 
copy_to_user(pUserSpaceData_v, pSleepContextData_UB + Offset_UL, Length_UL) if access type is read or
copy_from_user(pSleepContextData_UB + Offset_UL, pUserSpaceData_v, Length_UL) if access type is write.
*/
typedef struct  
{
   IOCTL_POWER_SLEEP_CONTEXT_ACCESS_TYPE  Type_E;              /* Read or write ? */
   DELTA_VOID                             pUserSpaceData_v;    /* Pointer to the user space buffer */
   ULONG                                  Offset_UL;           /* Offset in the sleep context kernel buffer */
   ULONG                                  Length_UL;           /* Amount of bytes to copy */
} IOCTL_POWER_SLEEP_CONTEXT_ACCESS_PARAM;

typedef enum
{
   IOCTL_REGISTER_ACCESS_TYPE_GET,
   IOCTL_REGISTER_ACCESS_TYPE_SET,
   IOCTL_REGISTER_ACCESS_TYPE_AND,
   IOCTL_REGISTER_ACCESS_TYPE_OR,
   IOCTL_REGISTER_ACCESS_TYPE_XOR,
} IOCTL_REGISTER_ACCESS_TYPE;

typedef struct  
{
   IOCTL_REGISTER_ACCESS_TYPE Type_E;
   ULONG                      Bar_UL;
   ULONG                      Offset_UL;
   ULONG                      Value_UL;
} IOCTL_REGISTER_ACCESS_PARAM;

typedef enum 
{
   DRVPS_NORMAL=0,     //DDR=ON, Video=ON 
   DRVPS_LOW,          //DDR=ON, Video=OFF 
   DRVPS_ULTRA_LOW     //DDR=OFF, Video=OFF  
} DRV_POWER_STATE;

typedef enum
{
   IOCTL_DRV_POWER_STATE_ACCESS_TYPE_READ,
   IOCTL_DRV_POWER_STATE_ACCESS_TYPE_WRITE
} IOCTL_DRV_POWER_STATE_ACCESS_TYPE;

typedef struct  
{
   IOCTL_DRV_POWER_STATE_ACCESS_TYPE Type_E;
   DRV_POWER_STATE                       PowerState_E;
} IOCTL_DRV_POWER_STATE_PARAM;


#pragma pack(pop)


/** @addtogroup IOCTLS_GROUP *///@{

#define IOCTL_PCI_RESOURCES_ACCESS              DEFINE_IOCTL_RDWR(0x001,IOCTL_PCI_RESOURCES_ACCESS_PARAM)      ///< Gives access to PCI resources information and userspace mapping
#define IOCTL_IO_ACCESS                         DEFINE_IOCTL_RDWR(0x002,IOCTL_IO_ACCESS_PARAM)                 ///< Gives a IO access to the userspace
#define IOCTL_SET_DPC_INTSRC_MASK               DEFINE_IOCTL_WR  (0x003, ULONG )                               ///< Allows setting bits in the DPC mask
#define IOCTL_CLR_DPC_INTSRC_MASK               DEFINE_IOCTL_WR  (0x004, ULONG )                               ///< Allows clearing bits in the DPC mask
#define IOCTL_GET_INTSRC                        DEFINE_IOCTL_RD  (0x005, ULONG )                               ///< Retrieves the interrupt sources value
#define IOCTL_CLR_INTSRC                        DEFINE_IOCTL_WR  (0x006, ULONG )                               ///< clears  the interrupt sources value
#define IOCTL_SET_DBGMASK                       DEFINE_IOCTL_WR  (0x007, IOCTL_SET_DBGMASK_PARAM )             ///< Sets a debug mask for a specified module id
#define IOCTL_PROCESS_GUID_ACCESS               DEFINE_IOCTL_WR  (0x008, IOCTL_PROCESS_GUID_ACCESS_PARAM )     ///< Sets a debug mask for a specified module id
#define IOCTL_GET_DRV_SUBTYPE                   DEFINE_IOCTL_RD  (0x009, ULONG )                               ///< Retrieves driver subtype

#define IOCTL_COMMON_BUFFER_ACCESS              DEFINE_IOCTL_RDWR(0x010, IOCTL_COMMON_BUFFER_ACCESS_PARAM)
#define IOCTL_ALLOCATE_COMMON_BUFFER            DEFINE_IOCTL_RDWR(0x011, ULONG)
#define IOCTL_FREE_COMMON_BUFFER                DEFINE_IOCTL_WR  (0x012, ULONG)
#define IOCTL_LINK_COMMON_BUFFER                DEFINE_IOCTL_WR  (0x013, IOCTL_LINK_COMMON_BUFFER_PARAM)
#define IOCTL_GETPREALLOCINFO_COMMON_BUFFER     DEFINE_IOCTL_RD  (0x014, IOCTL_GETPREALLOCINFO_COMMON_BUFFER_PARAM)
#define IOCTL_COMMON_BUFFERPOOL_ACCESS          DEFINE_IOCTL_RDWR(0x015, IOCTL_COMMON_BUFFERPOOL_ACCESS_PARAM)
#define IOCTL_USBUF_ACCESS						      DEFINE_IOCTL_RDWR(0x018, IOCTL_USBUF_ACCESS_PARAM )
#define IOCTL_DMABUF_ACCESS                     DEFINE_IOCTL_RDWR(0x019, IOCTL_DMABUF_ACCESS_PARAM)
#define IOCTL_EVENT_ACCESS                      DEFINE_IOCTL_RDWR(0x01A, IOCTL_EVENT_ACCESS_PARAM)
#define IOCTL_SHADOWREG_ACCESS                  DEFINE_IOCTL_RDWR(0x01B, IOCTL_SHADOWREG_ACCESS_PARAM)
#define IOCTL_PROPERTIES_ACCESS                 DEFINE_IOCTL_RDWR(0x01C, IOCTL_PROPERTIES_ACCESS_PARAM)
#define IOCTL_KTHREAD_ACCESS                    DEFINE_IOCTL_RDWR(0x01D, IOCTL_KTHREAD_ACCESS_PARAM)

#define IOCTL_POWER_ACK_WAKEUP                  DEFINE_IOCTL     (0x01E)
#define IOCTL_POWER_SLEEP_CONTEXT_ACCESS        DEFINE_IOCTL_WR  (0x01F, IOCTL_POWER_SLEEP_CONTEXT_ACCESS_PARAM)

#define IOCTL_REGISTER_ACCESS                   DEFINE_IOCTL_RDWR(0x020, IOCTL_REGISTER_ACCESS_PARAM)
//@}

#define SKEL_IOC_USER(code) ((code)+0x21)  
/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _SKELIOCTLS_H_
