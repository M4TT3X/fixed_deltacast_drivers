/********************************************************************************************************************//**
 * @internal
 * @file   	BoardTypes.h
 * @date   	2010/07/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/16  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _BOARDTYPES_H_
#define _BOARDTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define MIN_REF_STABILISATION_TIME 300 //Minimum time (in ms) to wait before considering the ref to be OK
#define DEFAULT_MTG_WAITING_FRAMES  20 //Number of aligned frame to wait before considering MTG to be locked
#define DEFAULT_MTG_JITTER_WINDOW    1 //Number of aligned frame to wait before considering MTG to be locked


#define HDBRD_NB_ONBRD_BUF   8 ///< Defines the number of on board buffers per data type.

/** @name BRD_DMARQST_USRDATA_IDX Definition of the index for DMA request user data table *///@{
#define HDBRD_DMARQST_USRDATA_IDX_FLAGS       0   ///< This user data attached to a DMA request gives some flags specific to the board.
#define HDBRD_DMARQST_USRDATA_IDX_EOD_VALUE   1
//@}

#define HDBRD_DMARQST_USRDATA_FLAGS_MULTIRQST_FIRST (0x01) ///< This is the first DMA request from a multi-request data transfer     
#define HDBRD_DMARQST_USRDATA_FLAGS_MULTIRQST_LAST  (0x02) ///< This is the last DMA request from a multi-request data transfer   

typedef enum{
   HDBRD_CHN_DATA_TYPE_VIDEO      =0,
   HDBRD_CHN_DATA_TYPE_YANC       =1,
   HDBRD_CHN_DATA_TYPE_CANC       =2,
   HDBRD_CHN_DATA_TYPE_THUMBNAIL  =3,
   HDBRD_NBOF_CHN_DATA_TYPE
}HDBRD_CHN_DATA_TYPE;

typedef enum{
   HDBRD_BUFQ_DATA_TYPE_VIDEO,      
   HDBRD_BUFQ_DATA_TYPE_YANC,       
   HDBRD_BUFQ_DATA_TYPE_CANC,       
   HDBRD_BUFQ_DATA_TYPE_VIDEO2,     
   HDBRD_BUFQ_DATA_TYPE_YANC2,      
   HDBRD_BUFQ_DATA_TYPE_CANC2,
   HDBRD_BUFQ_DATA_TYPE_THUMBNAIL,

}HDBRD_BUFQ_DATA_TYPE;


#define HDBRD_BUFQUEUE_USRPARAM_IDX_DATACONTENT                     0
#define HDBRD_BUFQUEUE_USRPARAM_IDX_CHANNEL_INDEX                   1
#define HDBRD_BUFQUEUE_USRPARAM_IDX_EOD_VALUE                       2
#define HDBRD_BUFQUEUE_USRPARAM_IDX_CURRENT_ONBRD_BUF_IDX           3
#define HDBRD_BUFQUEUE_USRPARAM_IDX_NB_ON_BOARD_BUFFER              4
#define HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE             5
#define HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_BASE       (HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+HDBRD_BUFQ_DATA_TYPE_VIDEO*HDBRD_NB_ONBRD_BUF)
#define HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC_BASE        (HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+HDBRD_BUFQ_DATA_TYPE_YANC*HDBRD_NB_ONBRD_BUF)
#define HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC_BASE        (HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+HDBRD_BUFQ_DATA_TYPE_CANC*HDBRD_NB_ONBRD_BUF)
#define HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO2_BASE      (HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+HDBRD_BUFQ_DATA_TYPE_VIDEO2*HDBRD_NB_ONBRD_BUF)
#define HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC2_BASE       (HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+HDBRD_BUFQ_DATA_TYPE_YANC2*HDBRD_NB_ONBRD_BUF)
#define HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC2_BASE       (HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+HDBRD_BUFQ_DATA_TYPE_CANC2*HDBRD_NB_ONBRD_BUF)
#define HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_THUMBNAIL_BASE   (HDBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+HDBRD_BUFQ_DATA_TYPE_THUMBNAIL*HDBRD_NB_ONBRD_BUF)


#define HDBRD_BUFQUEUE_DATACONTENT_VIDEO     (1<<HDBRD_BUFQ_DATA_TYPE_VIDEO)
#define HDBRD_BUFQUEUE_DATACONTENT_YANC      (1<<HDBRD_BUFQ_DATA_TYPE_YANC)
#define HDBRD_BUFQUEUE_DATACONTENT_CANC      (1<<HDBRD_BUFQ_DATA_TYPE_CANC)
#define HDBRD_BUFQUEUE_DATACONTENT_VIDEO2    (1<<HDBRD_BUFQ_DATA_TYPE_VIDEO2)
#define HDBRD_BUFQUEUE_DATACONTENT_YANC2     (1<<HDBRD_BUFQ_DATA_TYPE_YANC2)
#define HDBRD_BUFQUEUE_DATACONTENT_CANC2     (1<<HDBRD_BUFQ_DATA_TYPE_CANC2)
#define HDBRD_BUFQUEUE_DATACONTENT_THUMBNAIL (1<<HDBRD_BUFQ_DATA_TYPE_THUMBNAIL)

#define HDBRD_BUFHDR_USRIDX_TIMESTAMP        0x0


#define HDBRD_BUFHDR_STS_FRAMING_ERROR       0x1


#define HDBRD_DMA_PADDING_SIZE   0x200
#define HDBRD_DMA_ADDR_ALIGN     0x1000


#define HDBRD_PAD_DMA_SIZE(Size)    ROUND_TO_x(Size, HDBRD_DMA_PADDING_SIZE)
#define HDBRD_ALIGN_DMA_ADDR(Size)  ROUND_TO_x(Size, HDBRD_DMA_ADDR_ALIGN)


#define GNLK_KTHREAD_CMD_REF_STS_CHANGE         0x01
#define GNLK_KTHREAD_CMD_MTG_STS_CHANGE         0x02
#define GNLK_KTHREAD_CMD_EXPECTED_REF_CHANGE    0x04
#define GNLK_KTHREAD_CMD_RST                    0x08


#define FW_START_ADDRESS   0
#define SPI_FLASH_PARTITION_SIZE            0x200000             


/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _BOARDTYPES_H_
