/********************************************************************************************************************//**
 * @internal
 * @file   	BoardData.h
 * @date   	2010/07/15
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/15  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _BOARDDATA_H_
#define _BOARDDATA_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "KernelObjects.h"
#include "HDDrv_RegistersMap.h"
#include "DMAMngr.h"
#include "KernelSysCall.h"
#include "FPGA_DMAController.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct
{
 
   BOOL32                    pDMADoneIntFromPCI_B[MAX_DMA_CHANNELS];
   BOOL32                    pDMADoneIntFromFPGA_B[MAX_DMA_CHANNELS];
   ULONG                   pLastRxBufID_UL[4];     // Keeps the last RX Buffer IS cought during ISR
   ULONG                   pLastRXTimeStamp_UL[4]; //TS
   SYSTEM_TIME             pLastRxSystemTime_X[4];  
   ULONG                   pAncCSize_UL[4];
   ULONG                   pAncYSize_UL[4];
   ULONG                   pAncC2Size_UL[4];
   ULONG                   pAncY2Size_UL[4];

   BOOL32                    LastRefOk_B;

   BOOL32 FPGADownloaded_B;

   ULONG HWFirmwareCheckVersion_UL;
   ULONG HWFSFirmwareCheckVersion_UL;


   FPGA_DMACTRL_PARAM DMACtrlParam_X;

} BOARD_DATA;

#endif // _BOARDDATA_H_

