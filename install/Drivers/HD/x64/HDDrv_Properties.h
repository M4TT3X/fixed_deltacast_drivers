/********************************************************************************************************************//**
 * @internal
 * @file   	HDDrv_Properties.h
 * @date   	2010/08/26
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/26  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _HDDRV_PROPERTIES_H_
#define _HDDRV_PROPERTIES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "StreamProperties.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define HDDRV_BOARD_FEATURES_GENLOCK            0x00000001
#define HDDRV_BOARD_FEATURES_KEYER              0x00000002
#define HDDRV_BOARD_FEATURES_WARMUPDATE         0x00000004
#define HDDRV_BOARD_FEATURES_THUMBNAIL          0x00000008

#define HDDRV_BOARD_FEATURES_MEMCFG_MASK        0x000000F0
#define HDDRV_BOARD_FEATURES_MEMCFG_256MB       0x00000010
#define HDDRV_BOARD_FEATURES_MEMCFG_512MB       0x00000020

#define HDDRV_BOARD_FEATURES_LOW_PROFILE        0x00000100
#define HDDRV_BOARD_FEATURES_HDMI_MONITORING    0x00000200

#define HDDRV_BOARD_FEATURES_RELAY_0            0x00001000
#define HDDRV_BOARD_FEATURES_RELAY_1            0x00002000

#define HDDRV_RELAYx_DISABLE_VAL_BYUSER       0
#define HDDRV_RELAYx_DISABLE_VAL_FGPALOADED   1
#define HDDRV_RELAYx_DISABLE_VAL_BOARDOPEN    2
#define HDDRV_RELAYx_ENABLE_VAL_BYUSER        0
#define HDDRV_RELAYx_ENABLE_VAL_DRIVERSLEEP   1
#define HDDRV_RELAYx_ENABLE_VAL_BOARDCLOSED   2

#define HDDRV_RX_CHN_STATE_BIT_COUPLED         0x00000001   

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
   HDDRV_BOARD_TYPE_HD_PCI,
   HDDRV_BOARD_TYPE_HD_PCIE,
   HDDRV_BOARD_TYPE_HD2_PCIE ///< New generation of HD PCIe base board with extension board support
} HDDRV_BOARD_TYPE;

typedef enum
{
   HDDRV_EXTBOARD_TYPE_NONE,
   HDDRV_EXTBOARD_TYPE_40,
   HDDRV_EXTBOARD_TYPE_3G,
   HDDRV_EXTBOARD_TYPE_04

} HDDRV_EXTBOARD_TYPE;

typedef enum
{
   HDDRV_FIRMWAREID_UNKOWN,    
   HDDRV_FIRMWAREID_PCI22,
   HDDRV_FIRMWAREID_PCIE_KEYER,
   HDDRV_FIRMWAREID_PCIE_CODEC,
   HDDRV_FIRMWAREID_PCIE_40,
   HDDRV_FIRMWAREID_PCIE_3G,
   HDDRV_FIRMWAREID_PCIE_3G_ARM,
   HDDRV_FIRMWAREID_PCIE_04

}HDDRV_FIRMWAREID;

typedef enum
{
   HDDRV_PROPERTIES_FIRST=NB_STREAM_PROPERTIES,
   HDDRV_PROPERTIES_BOARD_TYPE=HDDRV_PROPERTIES_FIRST,
   HDDRV_PROPERTIES_EXTBOARD_TYPE,
   HDDRV_PROPERTIES_BOARD_FEATURES,
   HDDRV_PROPERTIES_BOARD_FIRMWAREID,
   HDDRV_PROPERTIES_BOARD_FIRMWARE_VERSION,

   HDDRV_PROPERTIES_RELAY0_DISABLE,
   HDDRV_PROPERTIES_RELAY0_ENABLE,
   HDDRV_PROPERTIES_RELAY1_DISABLE,
   HDDRV_PROPERTIES_RELAY1_ENABLE,

   HDDRV_PROPERTIES_INCOMING_REF_STD,
   HDDRV_PROPERTIES_EXPECTED_REF_STD,
   HDDRV_PROPERTIES_MTG_WAITING_FRAMES,
   HDDRV_PROPERTIES_GNLK_KTHREADID,
   
   HDDRV_PROPERTIES_BOARD_ARM_VERSION,
   HDDRV_PROPERTIES_BOARD_RQST_FIRMWARE_VERSION,
   HDDRV_PROPERTIES_BOARD_RQST_ARM_VERSION,
   HDDRV_PROPERTIES_BOARD_NB_UPGRADABLE_STUFF,
   HDDRV_PROPERTIES_BOARD_ARM_FW_ID,


   NB_HDDRV_PROPERTIES
} HDDRV_PROPERTIES;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _HDDRV_PROPERTIES_H_
