/**********************************************************************************************************************
 
   Module   : UserShadowRegsitersTypes
   File     : UserShadowRegsitersTypes.h
   Created  : 2007/10/03
   Author   : cs

   Description : 

   
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/10/03  v00.01.0000    cs       Creation of this file


 **********************************************************************************************************************/

#ifndef _UserShadowRegsitersTypes_H_
#define _UserShadowRegsitersTypes_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

typedef enum 
{
   IOCTL_SHADOWREG_ACCESS_TYPE_DEFINE,
   IOCTL_SHADOWREG_ACCESS_TYPE_SET,
   IOCTL_SHADOWREG_ACCESS_TYPE_GET,
   IOCTL_SHADOWREG_ACCESS_TYPE_SETBIT,
   IOCTL_SHADOWREG_ACCESS_TYPE_CLRBIT,
   IOCTL_SHADOWREG_ACCESS_TYPE_RESET,
   IOCTL_SHADOWREG_ACCESS_TYPE_SETAUTOCLRBIT,   
   IOCTL_SHADOWREG_ACCESS_TYPE_SETMSK   
} IOCTL_SHADOWREG_ACCESS_TYPE;

/**********************************************************************************************************************//**
 * @struct USER_SHADOWREG_PARAM
 * @brief This data structure is used with associated IOCTLs related to user shadow registers. 
 *
 * The involved IOCTLs are :
 * -IOCTL_USRSHADOWREG_DEFINE  
 * -IOCTL_USRSHADOWREG_SETVALUE
 * -IOCTL_USRSHADOWREG_GETVALUE
 * -IOCTL_USRSHADOWREG_SETBIT  
 * -IOCTL_USRSHADOWREG_CLRBIT  
 * -IOCTL_USRSHADOWREG_RESET   
 *
 * For more information about User Shadow register, please refer to the @ref USER_SHADOW_REG_PAGE "User Shadow registers chapter".
 * 
 * @remark According to the IOCTL, the Value_UL parameter can be an input or an output.
 *************************************************************************************************************************/

#pragma pack(push, 4)

typedef struct  
{
   ULONG StructSize_UL;
   IOCTL_SHADOWREG_ACCESS_TYPE   AccessType_E;
   ULONG Idx_UL;
   ULONG Value_UL;   ///< Used to set or retrieve the register value, depending on the used IOCTL.
   ULONG Msk_UL;
} IOCTL_SHADOWREG_ACCESS_PARAM;

#pragma pack(pop)





#endif // _UserShadowRegsitersTypes_H_
