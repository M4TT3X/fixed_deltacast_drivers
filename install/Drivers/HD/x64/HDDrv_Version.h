/********************************************************************************************************************//**
 * @internal
 * @file   	HDDrv_Version.h
 * @date   	2010/08/26
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/26  v05.04.0000    cs       Creation of a test driver based on the new driver skeleton/architecture
   2011/01/05  v05.04.0013    ja/cs    Change GetBoardData() macro in DrvData.h to a function to avoid a LINUX compiler bug that doesn't calculate the right offset in the PDX structure in DMAMngr.cpp module
   2011/01/05  v05.04.0014    cs       Due to a missed implementation of some 2 IOCTL in eventmngr.c (Linux implementation), the wait on a driver event didn't work anymore.                                       
   2011/01/21  v05.04.0015    ja       After release 5.05 fixes reported:
                                           New FPGA (PCIeCodec=10110201) Bug fix: Link Fault Indicator (LFI) can now be used as unlocked indicator
                                           New FPGA (PCIeKeyer=10110202) Bug fix: Genlock US-EU problem
                                           New FPGA (PCIeKeyer=10112501) !! Timing score = 150 !! Bug fix: memory index restart for each channel stop
                                           Bug fix: Relay status with watchdog
                                           New FPGA (PCIeSDI3G=10112201) New: DELTA-3G ARM support (Mailbox)
                                           New : ARM upgrade on Board_Init()
                                           New ARM (PCIESDI3G=11010501) and New FPGA  (PCIESDI3G=10121001)
   2011/01/25  v05.04.0016    ja       New FPGA (PCIeSDI3G=10121001) Bug fix: HDMI monitoring
   2011/01/28  v05.04.0017    ja       Buf fix: Buffer lock BUFFER_LOCK_TYPE_NEXT fixed for RXmTX
   2011/01/31  v05.04.0018    ja       Buf fix: BufMngr_BufQueueGetStatus() fixed dissociate BUFFER_TYPE_RX and BUFFER_TYPE_TX
   2011/02/04  v05.04.0019    ja       Buf fix: used BUFFER_LOCK_TYPE_OLDEST in Board_BufMngrTxIntHandler in place of BUFFER_LOCK_TYPE_NEXT
   2011/02/07  v05.04.0020    ja       New FPGA (PCIESDI3G=11012401) - New ARM (PCIESDI3G=11020401) bug fix: HDMI output format
                                                                                                    new: Full Duplex and SC support
   2011/02/08  v05.04.0021    ja       New: add 3G keyer support
   2011/02/17  v05.04.0022    ja       Bug fix: Board_BufMngrRxIntHandler() - SubBufID_UL for all data types (Crashed with RX-modify-TX with no CANC in RX)
   2011/03/21  v05.04.0023    ja       New: add Board_ReadHDMIEDID()
                                            add Board_GetHDMISts()
   2011/02/04  v05.04.0024    cs       FPGA Update for HD 3G 22 to version 11030201 (Scatter/Gather support)
                                       FPGA Update for HD 40 to version 11022801 (Scatter/Gather support)

   2011/04/04  v05.04.0025    cs/gt    Add missing #pragma LOCKED_CODE In DMABufMngr module and in USBufMngr_Win.
   2011/04/04  v05.04.0026    gt       New fw (PCIeKeyer=11040402) : Bug fix : Psf detection doesn't work if there is no other ANC data in addition to the SMPTE352m packet
   2011/04/04  v05.04.0027    gt       New fw (3G=11033101) : Bug fix : New version of timing constraints to be compliant with 3G
   2011/04/04  v05.04.0028    gt       New fw (3G=11040401) : Bug fix : reception block stays in a incoherent state
   2011/04/05  v05.04.0029    gt       New fw (codec=11040404) : Bug fix : Psf detection doesn't work if there is no other ANC data in addition to the SMPTE352m packet
   2011/04/05  v05.04.0030    gt       New fw (3G=11040502) : Bug fix : Register reading on slow PCIe platform is buggy
   2011/04/05  v05.04.0031    gt       Bug fix : On 3G board, ARM is upgraded even if Stop HDMI fails
   2011/04/13  v05.04.0032    gt       New fw (HD40=0x11040601) : Bug fix : Register reading on slow PCIe platform is buggy
   2011/04/18  v05.04.0033    gt       New FPGA (HD40 : 0x11041801) FPGA_ISR_BIT_FRAME_IRQ interruption on BTC incrementation
   2011/04/18  v05.04.0034    gt       New FPGA (HD40 : 0x11041901) 24Hz support by the pseudo MTG
   2011/04/21  v05.04.0035    gt       Bug fix : setting FPGA_IMR_BIT_MB_TX_EMPTY is done with SHREG32_SET instead of SHREG32_SETBIT
                                       Bug fix : Board_DPCforISR is not registered in STREAM_CUSTOMIZATION
   2011/04/21  v05.04.0036    gt       Bug fix : 3G keyer delay is wrong
   2011/04/22  v05.04.0037    gt       Bug fix : User interrupt and FPGA_IMR_BIT_EOD|FPGA_IMR_BIT_EOD1 are disabled before the DMA abort (=> timeout)
   2011/04/22  v05.04.0038    gt       Bug fix : In Board_ISR, the INT_SOURCE_STS_CHANGE_DCM_INT bit is not set on a FPGA_ISR_BIT_DCM_STS_CHANGE hardware interrupt
   2011/05/04  v05.04.0039    ja       New fw (3G=11050302 - key=11050301): Patch to hide 4911 flags when it's crashed rised up from 16 to 32 frames
                                       New ARM (3G=11050401): HDMI monitoring bug
   2011/05/06  v05.04.0040    ja       New fw (codec=11050401): Patch to hide 4911 flags when it's crashed rised up from 16 to 32 frames
   2011/05/09  v05.04.0041    ja       New fw (3G=11050901): bug corrected in i/p detection for phase mesures
   2011/05/12  v05.04.0042    ja       Bug fix: SG was disabled on HD40
   2011/05/31  v05.04.0043    ja       New fw (3G=11053101): inverted field bug in HDMI has been fixed
   2011/07/05  v05.04.0044    ja       New fw (HD04=11070501): ANC corrupted with more than 2 TX with ANC
   2011/07/08  v05.04.0045    ja       New fw (3G=11070801): Intel platform bug fixed
                                                             64B repetition bug fixed
                                                             Small SG transfer bug fixed
   2011/07/11  v05.04.0046    gt       New fw (04=0x11071101): Change the product ID
                                       Add the new HD04 product ID in LinuxDrvInfo.h, DELTA-hd.inf
                                       Change Board_BoardTypeIdentify to take the new HD04 product ID into account
   2011/08/23  v05.04.0047    ja       New fw (3G=11081802): NTSC keyer black line bug fixed
                                                             Inverted key keyer bug fixed
   2011/08/24  v05.04.0048    ja       New ARM (3G and 04=11082401): HDMI moonitoring RGB444
   2011/08/25  v05.04.0049    ja       New ARM (3G and 04=11082501): HDMI moonitoring CSC wasn't enabled
   2011/09/08  v05.04.0050    ja       Bug fix : In Linux, trying to install the driver after fw upgrade crashes the system. -> In the probe, unload the driver after fw upgrade.
                                       Bug fix : In Linux Opensuse, unloading the driver freeze the console. -> Don't call do_exit in KOBJKT_ExitKThread()
                                       New FPGA (3G and 04 = 11090701): Mailbox access bug while reading EEDID
                                                                        VBI 10-bits
                                                                        Activation preload TX
                                                                        Correction PCIe Coolux
                                                                        Support HCCP (04)
                                                                        Correction of "Inverted key" calculation (3G)
                                                                        Correction of keyer output in NTSC (first line is always black) (3G)
                                       New FPGA (40 = 11090202):  VBI 10-bits
                                                                  Correction PCIe Coolux
                                       New FPGA (codec = 11090501):  VBI 10-bits
                                                                     Correction PCIe Vigor
                                                                     Uniformisation preload TX (like in HDe)
                                       New FPGA (key = 11090203):    VBI 10-bits
                                                                     Activation preload TX
                                                                     Correction PCIe Vigor
                                                                     Correction of "Inverted key" calculation
                                                                     Suppression buff TX index reporting
   2011/09/19  v05.04.0051    gt       New: Add DbgMask driver parameter to control the default debug output level
                                       Bug fix: Potential tearing on TX channel if EOD and Video INT occur at the same time -> solution : enable early transfert
   2011/09/28  v05.04.0052    gt       New FPGA (3G22=0x11092601 - HD40=0x11092702)
                                       Bug fix : 3D reception could be corrupted after retrieving a signal that has been lost on the last line
   2011/08/15  v05.04.0053    lp/cs    Support compatibility 32 & 64 bits :
                                          - All IOCTL structures are now packed on 4 bytes (allowing 32 - 64 bits exchange)
                                          - The PCI_RESOURCE structure has been duplicated, one represent the IOCTL exchange
                                            structure named PCI_RESOURCE_IOCTL and the other represent the saved resource. 
                                            The saved resource can be different between driver and User space. 
                                            (32 bits on the user space side and 64 bits in the driver)
                                          - A new type has been added DELTA_VOID. It represents a pointer, it is always 64 bits long
                                            and can be asigned in the user space by a void pointer using an operator overloading.
                                            Please note that this type is also used to store event HANDLE.
   2011/10/21  v05.04.0054    ja       New FPGA (key = 11102103) - Stereoscopy resynch after recovery bug fixed
                                       New FPGA (3G22 = 11102101) - Stereoscopy resynch after recovery bug fixed
                                       New FPGA (40 = 11102102) - Stereoscopy resynch after recovery bug fixed
   2011/10/25  v05.04.0055    ja       Go back to old FPGA (3G22 = 11090701) - To avoid RX stereoscopy bug
                                       Go back to old FPGA (HD40 = 11090202) - To avoid RX stereoscopy bug
   2011/11/16  v05.04.0056    ja       New FPGA (40 = 11111502) - NTSC distorted from AJA converter
                                       New FPGA (3G = 11111801) - NTSC distorted from AJA converter
   2012/02/02  v05.04.0057    gt       Bug fix : ASI transmission channel is on time-out after an underrun
                                       Bug fix : ASI underrun status is not cleared directly after leaving iddle state, but after streaming an entire user data buffer
   2012/01/24  v05.04.0058    ja       PID bug fixed in DriverAccess.c that caused crash when accessing from different thread
                                       current->tgid returns the pid of the first thread while current->pid returns the pid of the current thread
   2012/02/23  v05.04.0059    gt       New FPGA (3G = 12022301) Bug fix : 1080p60 is detected as 1080p59
   2012/03/20  v05.04.0060    ja       RXmTX new behaviour: ANC sizes are now checked for buffer corruption in the Board_ISR
                                                            RXmTX only transmit on RX if TX interruptions has been triggered (Stream_ISR)
                                       New FPGA (codec = 12041201) RXmTX support: based on SDI model
   2012/04/13  v05.04.0061    ja       Bug fix : RXmTX bug for DELTA-3G cards with DMA overload
   2012/08/14  v05.04.0062    gt       Under Windows, map in kernel space only the first page of each buffer to avoid VHDERR_OPERATIONFAILED on VHD_StartStream with tiny kernel address space
   2012/06/04  v05.04.0063    gt       New FPGA (codec = 12060102) 720p30, 720p25 and 720p24 support
   2012/10/11  v05.04.0064    gt       Modify kernel memory allocation to reserve space only for headers
                              ja       Add handshake protection in arm uart
                                       modify KOBJ_CreateMemPool Tag to be Windows Drivers Verifier compliant
                                       Add OBJ_KERNEL_HANDLE attribute in WDMHLP_MapPhysicalMemory to be Windows Drivers Verifier compliant
                                       SkelDrv_Close in DispatchClose is not anymore into SAFEAREA to be Windows Drivers Verifier compliant
                                       new EventMngr_CloseByHandle function  that dereferences object (nonpaged memory leak fix)
   2012/10/19 v05.04.0065     ja       Add EXTRA_CFLAGS += -fno-stack-protector in Makefile and Makefile_client
   2012/10/23 v05.04.0066     ja       Add protection in EventMngr_DPCHandler to avoid KOBJ_SetSyncEvent call if the event has been closed meanwhile                                 
   2012/11/26 v05.04.0067     ja       New FPGA (key = 12101502 and codec = 12101701): "74,25" issue
   2012/12/13 v05.04.0068     cs       Bug Fix: Add a missing pointer check after allocation before using it
   2012/10/30  v05.04.0069    cs       Includes an update in the PCI skel driver to get rid off the windows power management part.
   2012/11/12  v05.04.0070    cs/ja    Linux KOBJ_CreateMemPool allocation limited to 128K in old kernel. kmalloc has been replaced by vmalloc
   2012/11/12  v05.04.0071    cs/ja    MainDevice_Open/DispatchCreate and MainDevice_Release/DispatchClose functions have bad level of execution
   2013/01/15  v05.04.0072    gt       Under Windows, map in kernel space only the first page of each buffer to avoid VHDERR_OPERATIONFAILED on VHD_StartStream with tiny kernel address space
   2013/01/28  v05.04.0073    gt      bug fix : PCI core interruption of the second DMA channel were not enabled
   2013/01/30  v05.04.0074    gt      bug fix : code was not locked
   2013/02/26  v05.04.0075    ja       new: add NoPreloadWithSG key registry to disable preallocation when SG is available
   2013/03/04  v05.04.0076    ja       new FPGA (HDKey=0x13021401 - 3G22=0x13022002): No chroma on key feature
   2013/03/21  v05.04.0077    ja       New FPGA (codec = 13031401): bug fix: update Arbiter fifo size and DDR frequency (200MHZ) =>VBI error
   2013/03/28  v05.04.0078    ja       New FPGA (codec = 13032801): bug fix: new arbiter was not included
   2013/03/18  v05.04.0079    ja        New FPGA (key = 13021803)
                                                 (codec = 13021902)
                                                 (40 = 13022003)
                                                 (3G = 13022002): Change new_frame gestion to be compliant with RP168 in Raw Mode
   2013/05/08  v05.04.0080    ja       ARM 04 and ARM 3G was the same so we deleted ARM 04. ARM 3G is used for the two boards
   2013/05/17  v05.04.0081    bc       Add support Mac OSX + change KOBJ_SetSyncEvent mechanism  
   2013/05/29  v05.04.0080    ja       Debug print ISR and DPC
   2013/05/29  v05.04.0081    ja       Limit Max DMA Buf to 2*(1+DMABUF_MAX_NB_SLAVES) in place of 128*(1+DMABUF_MAX_NB_SLAVES)
                                       => Driver can't work this way
   2013/05/29  v05.04.0082    ja       Limit Max DMA Buf is back to 128*(1+DMABUF_MAX_NB_SLAVES)
                                          Mempool for DMA descriptors is now allocated dynamically in USBufMngr_RegisterUSBuf and freed in USBufMngr_UnregisterUSBuf
   2013/06/26  v05.04.0083    ja       Windows USBufMngr allocate several MDL to sustain size buffer limit
   2013/07/26  v05.04.0084    ja       New: add HDDRV_BOARD_FEATURES_RELAY_x fore concerned cards
   2013/07/08  v05.04.0085     ja       New FPGA (key = 13070801): Remove duplicate EAV in case of special switching case
   2013/08/05  v05.04.0086     gt       New FPGA (key = 13080501): Remove duplicate LN and CRC in case of special switching case
   2013/08/05  v05.04.0087     gt       New FPGA (key = 13082003): New sdi framer (compliant RP168) which resync into one TRS sequence
                                                                  Watchdog new model
   2013/08/29  v05.04.0088     gt       New FPGA (codec = 13082901): Watchdog new model
   2013/08/30  v05.04.0089     ja       Bug: relay must alway be available for HDe 22 and codec
   2013/09/04  v05.04.0090    ja       Bug: update Board to fit with new boarddata definition
   2013/09/26  v05.04.0091      gt     Bug fix : BoardDataMemPool_X was freed before disabling interrupt causing BSOD in ISR when an interrupt occurs after freeing BoardDataMemPool_X
   2013/10/17  v05.04.0092    gt       Bug fix : SkelDrv_AllocDrvData was called after SkelDrv_DeviceAdd. Move SkelDrv_AllocDrvData from WDFEvtPrepareHW to WDFEvtDeviceAdd
   2013/10/17  v05.04.0093    gt       Bug fix : SkelDrv_FreeDrvData was called to soon. Move SkelDrv_FreeDrvData from WDFEvtReleaseHW to WDFEvtDeviceContextCleanup
   2013/10/21  v05.04.0094     gt       Bug fix : In Stream_Wakeup, CallBack_Wakeup_fct returned status is overwritten by MultiChnDMAMngr_Wakeup returned status. 
   2013/10/21  v05.04.0095     gt       Bug fix : In Stream_Wakeup, MultiChnDMAMngr_Wakeup is called before CallBack_Wakeup_fct
   2013/11/08  v05.04.0096     ja        New FPGA (Key = 13102901): ThunderBolt performance and automatic insertion of SMPTE352M in case of 1080i
                                         New: power management support
                                         New FPGA (codec = 13101601): Thunderbolt performance and Use wdt_status instead of sr(28) to control relays
                                         New FPGA (3G = 13100101): Thunderbolt performance
   2013/11/13  v05.04.0097     ja        New FPGA (codec = 13111302): correction Thunderbolt
                                         New FPGA (key = 13111301): correction Thunderbolt
   2013/11/13  v05.04.0098     ja        New FPGA (3G=13111303): correction Thunderbolt
   2013/09/26  v05.04.0099      gt     Bug fix : BoardDataMemPool_X was freed before disabling interrupt causing BSOD in ISR when an interrupt occurs after freeing BoardDataMemPool_X
   2013/11/12  v05.04.0100    ja       New FPGA(key = 13102901): Correction of automatic insertion of SMPTE352M in case of 1080i
                                       New FPGA(3G = 13100101): Correction of automatic insertion of SMPTE352M in case of 1080i
                                       New FPGA(codec = 13101601): Correction of automatic insertion of SMPTE352M in case of 1080i
   2013/11/13  v05.04.0101    ja       New FPGA(key = 13111301): thunderbolt correction
                                       New FPGA(codec = 13111302): thunderbolt correction
   2013/11/13  v05.04.0102    ja       New FPGA (3G=13111303): correction Thunderbolt
   2013/11/19  v05.04.0103    ot       New FPGA (key = 13111901): configurable luma and chroma min and max clipping values for HD keyer cards
   2013/11/28  v05.04.0104    ot       Bug fix : the watchdog register address is incorrect (DELTA-key and DELTA-codec)
   2013/12/17  v05.04.0105    ja       New FPGA (key=13121202): Support of ANC freeze in case of underrun when VBI/ANC mixer is enabled
   2013/10/21  v05.04.0106    ot       New FPGA (40 = 13101702) : to fix PAL reception issues on RX3 on certain cards
   2014/01/03  v05.04.0107    gt      Bug fix : Windows device manager returned code 10 after updating firmware
   2014/02/06  v05.04.0108    ja       Bug fix: roll back FPGA_DMAController to the state before ADVLP introduction (X300)
   2014/02/13  v05.04.0109      gt       Bug fix: cold-unplug with a running app result in a call to DeviceClose after DeviceReleaseHW. A BSOD occurs because BoardData is accessed after unallocation.
                                       => move SkelDrv_AllocDrvData in WdfDeviceAdd and SkelDrv_FreeDrvData in WDFDeviceContextCleanup. 
   2014/02/19  v05.04.0110    ja       New FPGA (3G=14020403): Customisable clipping values at keyer level
   2014/02/19  v05.04.0111    bc       Bug fix : Bad sync event init under OSX
   2014/03/05  v05.04.0112    gt       Bug fix : LinuxAllocator doesn't compile with newer kernel : kmalloc is undefined. Add the include of <linux/slab.h>)
   2014/03/07  v05.04.0113    ja       new FPGA: (codec=14030702): Correction of automatic insertion of SMPTE352M in case of 1080i
   2014/03/28  v05.04.0114    ja       New FPGA (key = 14032701): Support of VBI Mixer into active lines
   2014/04/01  v05.04.0115    ja       Bug fix: Field mode underrun support: because in field mode, underrun interrupts are triggered one out of two, we need to increase STREAM_PROPERTIES_TX0_CHN_ONBOARD_FILLED_BUFFER_CNT on underrun (in field mode) to maintain correct value (see Stream_ISR() )
   2014/04/14  v05.04.0116    gt       Bug fix : Under Linux, boardData structure is not cleared before enabling interrupt => kernel panic in case of shared IRQ
   2014/04/15  v05.04.0117    gt       Improvement : Improve FPGA firmware upgrade in user mode.
   2014/04/15  v05.04.0118    gt       Bug fix : __debugbreak() was uncommented
   2014/05/02  v05.04.0119    ja       Bug fix: spinlock_t in KernelObject become raw_spinlock_t to avoid Linux RT interrupts freeze
   2014/05/16  v05.04.0120    ja       New FPGA (3G=14051603): Support of YUVK reception
													New FPGA (key=14051401): Change gestion of RX Cmd registers to ease YUVK reception at SW level
													New FPGA (HD40=14051702): Change gestion of RX Cmd registers to ease YUVK reception at SW level
   2014/06/20  v05.04.0121    gt       New FPGA (codec=14052201): Correction bitrate locking on RX channel
   2014/07/10  v05.04.0122    ja       Bug fix: Linux process id (PID) is saved in structure because it can be different in MainDevice_Release when the process is killed (see MainDevice_Open function). 
   2014/08/28  v05.04.0123    ja       Bug fix:  Call the entire remove() function execpt free_irq if request_irq() linux function fails in DriverEntre::probe
   2014/10/23  v05.04.0124    ja       New FPGA (key=14102303): Correction of thunderbolt (new handshake to avoid fifo overlap + Reduced C_MAX_READ_REQUEST_NBR)
   2014/11/26  v05.04.0125    bc       New FPGA (codec=14111202, 3G=14111201): Thunderbolt fix on v5 + Interrupt TX and TX Underrun Alignment
- SDK 5.17
   2015/05/15  v05.04.0126    bc       New FPGA (3G=0x15051805): 
                                       New FPGA (key=0x15051802):
                                       New FPGA (codec=0x15051803): Change behavior of sd_fifo_rst at line_in level to avoid erroneous write into DDR
   2015/05/28  v05.04.0127    bc       New FPGA (key=0x15052602): Debug removed
   2015/06/08  v05.04.0128    gt       In ARMUART_FlashUpgrade, ARMUART_CMD_BL_INIT is send once before waiting for ACK
   2015/06/25  v05.04.0129    gt       New FPGA (key=0x15062502) : Programmable C_MAX_READ_REQUEST_NBR at dma-master level (allow to disable/enable thunderbolt improvement)
   2015/06/10  v05.04.0130    gt       Bug fix : do_gettimeofday is subject to NTP server time adjustement => replace by getrawmonotonic
   2015/11/19  v05.04.0131    gt       Bug fix : In BufMngr, every RX user unlock try to a arm a transfer on buffer queue 0. 
                                       It can cause a drop on the TX channel associated to the buffer queue 0.
   2015/12/07  v05.04.0132    gt       New FPGA (codec=15112601)
                                       New : thunderbolt optimization can be disabled with a driver parameter
   2015/12/21  v05.04.0133    gt       SkelDrv_Sleep was not called in removeEx (Linux)
- SDK 5.19
   2016/01/26  v05.04.0134    gt       New : compatibility with Linux kernel >= 4.1 (remove IRQF_DISABLED usage)
   2015/11/27  v05.04.0135    ja       Fix: RXmTX drops with small genlock offset. Stream_ISR can now save two TX pending.
   2016/02/16  v05.04.0136    ja       Bug fix: some IOCTL used user space memory without KSYSCALL_CopyToUserSpace. That caused crash on linux with Z170 chipset.
   2016/03/23  v05.04.0137    bc       Bug fix : check device presence before calling SkelDrv_SetDrvPowerState to avoid blue screen if device is removed
                                  



 ====================================================================================================================
  =  TODO List                                                                                                       =
  ====================================================================================================================
  > Add a way to trig a board shutdown during a 'shutown operation' the the power management sequence to properly handle relays behaviour

 **********************************************************************************************************************/

#ifndef _HDDRV_VERSION_H_
#define _HDDRV_VERSION_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define HDBRD_VERMAJOR 5
#define HDBRD_VERMINOR 4
#define HDBRD_BUILD 137
#define HDBRD_PRODVER "5.04.0137\0"  

/** @def VALIDATE_ACCESS_CODE
 * The validate access code allows to check if the interface between driver and interface object are compatible.
 * If the object driver interface is linked to a greater minor code, it could inform that a newer driver is available, but, it
 * can work with the current one.
 * If the major access code mismatches, the interface object is not able to work  with the current driver.
 **/

#define HDBRD_VALIDATE_ACCESS_CODE_MAJOR 0x0006 ///< Gives a version number of the interface. This number must MATCH !
#define HDBRD_VALIDATE_ACCESS_CODE_MINOR 0x0001
#define HDBRD_VALIDATE_ACCESS_CODE ((HDBRD_VALIDATE_ACCESS_CODE_MAJOR<<16) | HDBRD_VALIDATE_ACCESS_CODE_MINOR)


#define HDSDI_PCI64_FW_VERSION         0x10011204
#define HDSDI_PCIE_KEYER_FW_VERSION 0x15062502
#define HDSDI_PCIE_CODEC_FW_VERSION 0x15112601
#define HDSDI_PCIE_SDI_40_FW_VERSION 0x14051702
#define HDSDI_PCIE_SDI_3G_FW_VERSION 0x15051805
#define HDSDI_PCIE_SDI_04_FW_VERSION 0x11090701

#define HDSDI_PCIE_KEYER_SAFEFW_VERSION   0x08051500
#define HDSDI_PCIE_CODEC_SAFEFW_VERSION   0x08051500
#define HDSDI_PCIE_SDI_40_SAFEFW_VERSION  0x10051501
#define HDSDI_PCIE_SDI_3G_SAFEFW_VERSION  0x11032402
#define HDSDI_PCIE_SDI_04_SAFEFW_VERSION  0x11032402

#define HDSDI_PCIE_SDI_ARM_FW_VERSION 0x11082501

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _HDDRV_VERSION_H_

