/********************************************************************************************************************//**
 * @internal
 * @file   	DrvParam.cpp
 * @date   	2009/01/22
 * @author 	gt
 * @brief   This module delivers driver parameters facilities
 **********************************************************************************************************************/
  
/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvParam.h"

/***** LOCAL SYMBOLS DEFINITIONS **************************************************************************************/

/***** LOCAL MACROS DEFINITIONS ***************************************************************************************/

/***** LOCAL TYPES AND STRUCTURES DEFINITIONS *************************************************************************/

/***** EXTERNAL VARIABLES *********************************************************************************************/

/***** GLOBAL VARIABLES ***********************************************************************************************/
//DRV_PARAM GL_DrvParam_X={0}; /* Set the default value */

/***** FUNCTIONS DEFINITION *******************************************************************************************/

/***** CLASSES DEFINITION *********************************************************************************************/

#ifdef __linux__

#include <linux/moduleparam.h>

/* module_param_named(Name,Variable,Type,S_IRUGO) */
module_param_named(DPN_RELAY0_DISABLE,GL_DrvParam_X.Relay0Disable_UL,uint,S_IRUGO);
module_param_named(DPN_RELAY0_ENABLE,GL_DrvParam_X.Relay0Enable_UL,uint,S_IRUGO);
module_param_named(DPN_RELAY1_DISABLE,GL_DrvParam_X.Relay1Disable_UL,uint,S_IRUGO);
module_param_named(DPN_RELAY1_ENABLE,GL_DrvParam_X.Relay1Enable_UL,uint,S_IRUGO);
module_param_named(DPN_FW_CHK_DISABLE,GL_DrvParam_X.FWCheckDisable_UL,uint,S_IRUGO);
module_param_named(DPN_ARM_CHK_DISABLE,GL_DrvParam_X.ARMCheckDisable_UL,uint,S_IRUGO);
module_param_named(DPN_DBG_MASK,GL_DrvParam_X.DbgMask_UL,uint,S_IRUGO);
module_param_named(DPN_PRELOAD_WITH_SG_DISABLE,GL_DrvParam_X.NoPreloadWithSG_UL,uint,S_IRUGO);

void DrvParam_Init(PDEVICE_EXTENSION pdx)
{
   /* Nothing to do */
}


#else

#define str(x) #x
#define g_str(x) str(x)

BOOL32 ParameterRead(PDEVICE_EXTENSION pdx, char *pParamName_c, ULONG *pParamValue_UL);

void DrvParam_Init(PDEVICE_EXTENSION pdx)
{
   ULONG Tmp_UL;
   
   if (ParameterRead(pdx, g_str(DPN_RELAY0_DISABLE),&Tmp_UL)) GL_DrvParam_X.Relay0Disable_UL=Tmp_UL;
   if (ParameterRead(pdx, g_str(DPN_RELAY0_ENABLE),&Tmp_UL)) GL_DrvParam_X.Relay0Enable_UL=Tmp_UL;
   if (ParameterRead(pdx, g_str(DPN_RELAY1_DISABLE),&Tmp_UL)) GL_DrvParam_X.Relay1Disable_UL=Tmp_UL;
   if (ParameterRead(pdx, g_str(DPN_RELAY1_ENABLE),&Tmp_UL)) GL_DrvParam_X.Relay1Enable_UL=Tmp_UL;
   if (ParameterRead(pdx, g_str(DPN_FW_CHK_DISABLE),&Tmp_UL)) GL_DrvParam_X.FWCheckDisable_UL=Tmp_UL;
   if (ParameterRead(pdx, g_str(DPN_ARM_CHK_DISABLE),&Tmp_UL)) GL_DrvParam_X.ARMCheckDisable_UL=Tmp_UL;
   
   if (ParameterRead(pdx, g_str(DPN_PRELOAD_WITH_SG_DISABLE),&Tmp_UL)) GL_DrvParam_X.NoPreloadWithSG_UL=Tmp_UL;
  
   if (ParameterRead(pdx, g_str(DPN_PREALLOC_POOL0_SIZE),&Tmp_UL)) GL_DrvParam_X.PreallocPool0Size_UL=Tmp_UL;
   if (ParameterRead(pdx, g_str(DPN_PREALLOC_POOL1_SIZE),&Tmp_UL)) GL_DrvParam_X.PreallocPool1Size_UL=Tmp_UL;
   if (ParameterRead(pdx, g_str(DPN_PREALLOC_POOL2_SIZE),&Tmp_UL)) GL_DrvParam_X.PreallocPool2Size_UL=Tmp_UL;
   if (ParameterRead(pdx, g_str(DPN_PREALLOC_POOL3_SIZE),&Tmp_UL)) GL_DrvParam_X.PreallocPool3Size_UL=Tmp_UL;
   
   if (ParameterRead(pdx, g_str(DPN_DBG_MASK),&Tmp_UL)) GL_DrvParam_X.DbgMask_UL=Tmp_UL;
   
   if (ParameterRead(pdx, g_str(DPN_THUNDERBOLT_OPTIMIZATION_ENABLE),&Tmp_UL)) GL_DrvParam_X.ThunderboltOptimizationEnable_UL=Tmp_UL;
   
}

#endif
