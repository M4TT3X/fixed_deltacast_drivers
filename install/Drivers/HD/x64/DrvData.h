/********************************************************************************************************************//**
 * @internal
 * @file   	DrvData.h
 * @date   	2010/06/10
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/10  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _DRVDATA_H_
#define _DRVDATA_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DMAMngr.h"
#include "BufMngr.h"
#include "KernelObjects.h"
#include "StreamCallBacks.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

#define GetStreamData() (GetDrvData())

//#define GetBoardData() (&(GetStreamData()->BoardData_X))

// To avoid a LINUX compiler bug that doesn't calculate the right offset in the PDX structure in DMAMngr.cpp module
//#define GetBoardData() (MyGetBoardData(pdx))

/** @name STREAM_MACRO Stream layer Macros
 The Stream layer provides a set of macro to make easier the access to some stuffs.
*///@{

#define STREAM_INIT_SAFEAREA()  KOBJ_InitializeSpinLock(pdx,&GetStreamData()->Spinlock_KO)
#define STREAM_ENTER_SAFEAREA() KOBJ_AcquireSpinLock(pdx,&GetStreamData()->Spinlock_KO)
#define STREAM_LEAVE_SAFEAREA() KOBJ_ReleaseSpinLock(pdx,&GetStreamData()->Spinlock_KO)


//@}


/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct
{
	KOBJ_SPINLOCK           Spinlock_KO;
 	MULTI_CHN_DMA_MNGR_DATA MultiChnDMAMngrData_X;  // Needed data for Multi Channel DMA Manager
 	BUFFER_MNGR_DATA        BufMngrData_X;          // Needed data for Frame Buffer Manager
   
   STREAM_CUSTOMIZATION    CustomizationTable_X;
   BOOL32 pTXIntPending_B[4];
   BOOL32 pEvenParity_B[8];

   KOBJ_MEMPOOL BoardDataMemPool_X;
} DRV_DATA;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _DRVDATA_H_
