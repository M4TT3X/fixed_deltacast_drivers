/********************************************************************************************************************//**
 * @internal
 * @file   	HDDrv_RegistersMap.h
 * @date   	2011/01/25
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2011/01/25  v00.01.0000    cs       Creation of this file
   2011/01/25  v00.01.0001    cs       Add new registers related to DMA Channel 1 (suppored by HD 3D board)

 **********************************************************************************************************************/

#ifndef _HDDRV_REGISTERSMAP_H_
#define _HDDRV_REGISTERSMAP_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "Register.h"

#define HDSDI_DMA_PADDING_SIZE 0x200

/* This enumeration contains symbols that defines all needed shadow registers */
typedef enum
{
   IDX_FPGA_IMR_SHRMM32,
   IDX_FPGA_GCMD_SHRMM32,
   IDX_FPGA_KEYERCTL_SHRMM32,
   IDX_FPGA_HDMI_CTL_SHRMM32,
   IDX_FPGA_TX0_SHRMM32,
   IDX_FPGA_TX1_SHRMM32,
   IDX_FPGA_TX2_SHRMM32,
   IDX_FPGA_TX3_SHRMM32,
   IDX_FPGA_MTG_CFG_SHRMM32,
   IDX_FPGA_WATCHDOG_SHRMM32,
   IDX_NB
} HDDRV_SHREG32;

/*******************************************************
* BAR 
*******************************************************/

#define CFGPLD_PCI_BAR              0     //only DELTA-hd
#define CFG_BD_PCI_BAR              0     //only DELTA-3G
#define FPGA_PCI_BAR                1

/*******************************************************
* CFGPLD_PCI_BAR
*******************************************************/

/* Registers address **********************/

#define CFGPLD_BASE                 0x100
#define CFGPLD_CMD_MM32          DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+0x00)
#define CFGPLD_STS_MM32          DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+0x00)
#define CFGPLD_DATA_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+0x08)
#define CFGPLD_REV0_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+ 0x0020)    ///<PLD revision id: Version
#define CFGPLD_REV1_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+ 0x0028)    ///<PLD revision id: Year
#define CFGPLD_REV2_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+ 0x0030)    ///<PLD revision id: Month
#define CFGPLD_REV3_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+ 0x0038)    ///<PLD revision id: Day


/* Registers bits *************************/

#define CFGPLD_REV2_BIT_CODEC       0x80

/* HD 40 cpld bit definition */
#define CFGPLD_REV1_BIT_FPGA_LX50T  0x04

/* HD 40,20 and 10 Low Profile definition */
#define CFGPLD_REV1_BIT_LOW_PROFILE 0x02

/* HD key cpld bit definition */
#define CFGPLD_REV1_BIT_WARM_LOAD   0x01
#define CFGPLD_REV1_BIT_KEY         0x10
#define CFGPLD_REV1_BIT_SD_ONLY     0x20
#define CFGPLD_REV1_BIT_PCIE        0x80

/* HD codec cpld bit definition */
#define CFGPLD_REV1_SDI_RX0_EN      0x80
#define CFGPLD_REV1_SDI_RX1_EN      0x40
#define CFGPLD_REV1_SDI_TX0_EN      0x20
#define CFGPLD_REV1_SDI_TX1_EN      0x10
#define CFGPLD_REV2_ASI_RX0_EN      0x08
#define CFGPLD_REV2_ASI_RX1_EN      0x04
#define CFGPLD_REV2_ASI_TX0_EN      0x02
#define CFGPLD_REV2_ASI_TX1_EN      0x01



/*******************************************************
* CFG_BD_PCI_BAR
*******************************************************/

/* Registers address **********************/

#define CFG_BD_ID_M32            DEFINE_MMREG32(CFG_BD_PCI_BAR,0x20)    


/* Registers bits *************************/

/* 3G cfg bit definition */
#define CFG_BD_MSK_MISC_FEATURES    0x000000FF
#define CFG_BD_BIT_KEY              0x00000080
#define CFG_BD_MSK_PROG_REVISION    0x00000F00
#define CFG_BD_MSK_CHAN_CFG         0x000FF000
#define CFG_BD_MSK_HW_ID            0x0FF00000
#define CFG_BD_MSK_BOARD_FAMILY_ID  0xF0000000

#define CFG_BD_GET_PROG_REVISION(cfg)     ((cfg&CFG_BD_MSK_PROG_REVISION)>>8)
#define CFG_BD_GET_CHAN_CFG(cfg)          ((cfg&CFG_BD_MSK_CHAN_CFG)>>12)
#define CFG_BD_GET_HW_ID(cfg)             ((cfg&CFG_BD_MSK_HW_ID)>>20)
#define CFG_BD_GET_BAORD_FAMILY_ID(cfg)   ((cfg&CFG_BD_MSK_BOARD_FAMILY_ID)>>28)



/*******************************************************
* CFG_BD_PCI_BAR
*******************************************************/

/* Registers address **********************/

#define FPGA_GCMD_SHRMM32        DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x00)
#define FPGA_GSTS_MM32           DEFINE_MMREG32(FPGA_PCI_BAR,0x00)
#define FPGA_RX0_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x08)
#define FPGA_RX1_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x10)
#define FPGA_TX2_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x08)
#define FPGA_TX3_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x10)
#define FPGA_RX2_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x130)
#define FPGA_RX3_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x150)
#define FPGA_TX0_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x18)
#define FPGA_TX0_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x18)
#define FPGA_TX1_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x20)
#define FPGA_TX1_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x20)
#define FPGA_IMR_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x28)
#define FPGA_SR_MM32             DEFINE_MMREG32(FPGA_PCI_BAR,0x28)
#define FPGA_ICR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x30)
#define FPGA_ISR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x30)
#define FPGA_GSPI_MM32           DEFINE_MMREG32(FPGA_PCI_BAR,0x38)
#define FPGA_PARAM_ROM_A_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x40)
#define FPGA_PARAM_ROM_D_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x48)
#define FPGA_UART_CTRL_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x50) /* UART command and data register */
#define FPGA_UART_STS_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x50) /* UART status register */
#define FPGA_TX0_TC_START_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x50)
#define FPGA_TX1_TC_START_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x58)
#define FPGA_UART_DATA_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x58) /* [7..0] = UART data */
#define FPGA_DMA_BC_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x60)
#define FPGA_DMA_A_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x68)
#define FPGA_END_OF_DMA_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x70)
#define FPGA_WATCHDOG_SHRMM32    DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x80)
#define FPGA_RX0_STD_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x78) /* RX0 Standard identification (Read Only - PCIe Only)*/
#define FPGA_RX1_STD_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x80) /* RX1 Standard identification (Read Only - PCIe Only) */
#define FPGA_RX2_STD_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x138) /* RX2 Standard identification (Read Only - PCIe Only)*/
#define FPGA_RX3_STD_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x158) /* RX3 Standard identification (Read Only - PCIe Only) */
#define FPGA_VPULSE_OFFSET_MM32  DEFINE_MMREG32(FPGA_PCI_BAR,0x88)
#define FPGA_SSN_LSW_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x88)
#define FPGA_SSN_MSW_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x90)
#define FPGA_KEYERCTL_SHRMM32    DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x90)
#define FPGA_RX0_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x98)
#define FPGA_PP_PARAMS_A         DEFINE_MMREG32(FPGA_PCI_BAR,0x98)
#define FPGA_RX1_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0xA0)
#define FPGA_PP_PARAMS_D         DEFINE_MMREG32(FPGA_PCI_BAR,0xA0)
#define FPGA_VCXO_CTL_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0xA8) /* VCXO Control Register */
#define FPGA_RXBUFID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xB8)
#define FPGA_TX0_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0xC8) // Write Only
#define FPGA_TX1_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0xD0) // Write Only
#define FPGA_TX2_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x140) // Write Only
#define FPGA_TX3_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x148) // Write Only
#define FPGA_MVTG_CFG_D_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0xD8) // Write Only
#define FPGA_FABC0_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xC0) // Read Only
#define FPGA_FABC0_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xC8) // Read Only
#define FPGA_FABC1_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xD0) // Read Only
#define FPGA_FABC1_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xD8) // Read Only
#define FPGA_HDMI_CTL_SHRMM32    DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x118)
#define FPGA_FABC2_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x140) // Read Only
#define FPGA_FABC2_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x148) // Read Only
#define FPGA_FABC3_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x160) // Read Only
#define FPGA_FABC3_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x168) // Read Only
#define FPGA_SSR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0xE0)
#define FPGA_MTG_CFG_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xE8)
#define FPGA_MTG_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xE8)
#define FPGA_BTC_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0xF0)
#define FPGA_REVID_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xF8)
#define FPGA_FWSPI_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x110)

#define FPGA_MAILBOX_DATA_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x130) /* 32 bit Mailbox data */
#define FPGA_MAILBOX_ADDR_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x138) /* Mailbox address */

/* Codec registers */
#define FPGA_TX0_ASI_CMD_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x130) /* Write only. ASI TX0 command register */
#define FPGA_TX0_ASI_STS_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x130) /* Read only. ASI TX0 status register */
#define FPGA_TX0_ASI_THR_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x138) /* Write only. ASI TX0 interrupt threshold */
#define FPGA_TX0_ASI_BAUD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x140) /* Write only. Reference Bit Rate Generator: 32-bit phase increment */
#define FPGA_TX0_ASI_RATE_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x140) /* Read only. Incoming - Reference Bit Rate difference */
#define FPGA_TX0_ASI_DEV_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x148) /* Write only. Channel Bit Rate Deviation */

#define FPGA_TX1_ASI_CMD_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x150) /* Write only. ASI TX1 command register */
#define FPGA_TX1_ASI_STS_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x150) /* Read only. ASI TX1 status register */
#define FPGA_TX1_ASI_THR_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x158) /* Write only. ASI TX1 interrupt threshold */
#define FPGA_TX1_ASI_BAUD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x160) /* Write only. Reference Bit Rate Generator: 32-bit phase increment */
#define FPGA_TX1_ASI_RATE_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x160) /* Read only. Incoming - Reference Bit Rate difference */
#define FPGA_TX1_ASI_DEV_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x168) /* Write only. Channel Bit Rate Deviation */

#define FPGA_DMA1_BC_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x170) /* DMA Channel 1 transfer count (in bytes). The transfer size of the DMAshould be programmed before it is started. It is used to generate an interrupt once the number of bytes transferred reaches the DMA_BC value. */
#define FPGA_DMA1_A_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x178) /* DMA Channel 1 Target Address : 32bit sdram address bit 31..30 are used to select the channel: �01� for RX1 - �00� for RX0  */

#define FPGA_RX0_ASI_CMD_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x170) /* Write only. ASI RX0 command register */
#define FPGA_RX0_ASI_STS_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x170) /* Read only. ASI RX0 status register */
#define FPGA_RX0_ASI_THR_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x178) /* Write only. ASI RX0 interrupt threshold */
#define FPGA_RX0_ASI_BAUD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x180) /* Write only. Reference Bit Rate Generator: 32-bit phase increment */
#define FPGA_RX0_ASI_RATE_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x180) /* Read only. Incoming - Reference Bit Rate difference */
#define FPGA_RX0_ASI_DEV_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x188) /* Write only. Channel Bit Rate Deviation */
#define FPGA_RX0_ASI_PID_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x190) /* Write only. PID data input register */

#define FPGA_RX1_ASI_CMD_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x198) /* Write only. ASI RX1 command register */
#define FPGA_RX1_ASI_STS_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x198) /* Read only. ASI RX1 status register */
#define FPGA_RX1_ASI_THR_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x1A0) /* Write only. ASI RX1 interrupt threshold */
#define FPGA_RX1_ASI_BAUD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1A8) /* Write only. Reference Bit Rate Generator: 32-bit phase increment */
#define FPGA_RX1_ASI_RATE_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1A8) /* Read only. Incoming - Reference Bit Rate difference */
#define FPGA_RX1_ASI_DEV_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x1B0) /* Write only. Channel Bit Rate Deviation */
#define FPGA_RX1_ASI_PID_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x1B8) /* Write only. PID data input register */

#define FPGA_STC_LSW_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1C0) /* Read only. System Time Clock Least Significant 23 bits (23 bits timer from 90 Khz reference) and 9 bit prescaler value (0 ..299) */
#define FPGA_STC_MSW_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1C8) /* Read only. System Time Clock 10 Most Significant Bits.(This value is latched during read of STC_LSW to be coherent so, this will be read after STC_LSW) */


// -- Common symbols --------------------------------------------------------------------------------------------------

#define PACKING_V208     0x00000000  // 4:2:2:0  8-bit YUV  video packing 
#define PACKING_AV208    0x00000002  // 4:2:2:4  8-bit YUVA video packing 
#define PACKING_V210     0x00000001  // 4:2:2:0 10-bit YUV  video packing 
#define PACKING_AV210    0x00000003  // 4:2:2:4 10-bit YUVA video packing 
#define PACKING_V408     0x00000004  // 4:4:4:0  8-bit YUV  video packing 
#define PACKING_AV408    0x00000006  // 4:4:4:4  8-bit YUVA video packing 
#define PACKING_V410     0x00000005  // 4:4:4:0 10-bit YUV  video packing 
#define PACKING_AV410    0x00000007  // 4:4:4:4 10-bit YUVA video packing 
#define PACKING_C408     0x0000000C  // 4:4:4:0  8-bit RGB  video packing 
#define PACKING_AC408    0x0000000E  // 4:4:4:4  8-bit RGBA video packing 

// -- FPGA_GCMD_MM32 : General Command Register (WO) ------------------------------------------------------------------
// General board status and configuration register.

#define GENLOCK_SOURCE_FREERUN      0x0
#define GENLOCK_SOURCE_RX0_RX2      0x1   // RX0/RX2 (FPGA_GCMD_BIT_GEN_MODE_RX2_RX3) is taken as Genlock source
#define GENLOCK_SOURCE_RX1_RX3      0x2   // RX1/RX3 (FPGA_GCMD_BIT_GEN_MODE_RX2_RX3) is taken as Genlock source     
#define GENLOCK_SOURCE_BLACK_BURST  0x3   // Black burst is taken as Genlock source

#define OUTPUT_PATH_DIRECT          0x0   // TX channels output data from SDRAM
#define OUTPUT_PATH_PASSTHROUGH     0x3   // TX channels forward data from input channels (RX0->TX0, RX1->TX1)   
#define OUTPUT_PATH_CROSSED         0x7   // Same as passthrough, but data are crossed (RX1->TX0, RX0->TX1)

#define PCIE_LANE_NB_UNKNOWN        0x0   // always this value on older firmware than 0901120
#define PCIE_LANE_NB_1              0x1
#define PCIE_LANE_NB_2              0x2
#define PCIE_LANE_NB_4              0x3

#define GEN_CLK_FREQ_SD             0x0   
#define GEN_CLK_FREQ_HD             0x1   
#define GEN_CLK_FREQ_3G             0x3   

#define GEN_CLK_FREQ_TX0TX1         0x0
#define GEN_CLK_FREQ_TX2TX3         0x1

#define FPGA_GCMD_BIT_GENLOCK_RST                     0x40000000 // Serdes & genlock reset
#define FPGA_GCMD_BIT_GTP_RST                         0x20000000 // Reset GTP interfaces (on HD PCIe only, no effect on HD PCI-64)
#define FPGA_GCMD_BIT_HD_DCM_LOCKED                   0x10000000 
#define FPGA_GCMD_BIT_SD_DCM_LOCKED                   0x08000000 
#define FPGA_GCMD_ARM_RST_N                           0x08000000 /* Set �0� to reset ARM �C */
#define FPGA_GCMD_BIT_LINEOUT_DCM_RST                 0x04000000 // Line out DCM reset
#define FPGA_40_GCMD_MSK_BTC_FRAME_RATE               0x03800000 
#define FPGA_GCMD_MSK_ARM_BOOT                        0x03000000 /* �x0�: main flash memory is selected as boot space 
                                                                    �01�: system memory is selected as boot space 
                                                                    �11�: embedded SRAM is selected as boot space */ 
#define FPGA_GCMD_MSK_GENLOCK_SOURCE                  0x0000C000
#define FPGA_GCMD_BIT_GENLOCK_ENABLE                  0x00002000
#define FPGA_GCMD_MSK_OUTPUT_PATH                     0x00F00000
#define FPGA_GCMD_MSK_4911_ASR                        0x00000C00 // Audio Sample Rate
#define FPGA_GCMD_MSK_PCIE_LANE_NB                    0x00300000 
#define FPGA_GCMD_MSK_RELAY_CTRL                      0x000F0000
#define FPGA_GCMD_BIT_RELAY_CTRL_LOOP_0               0x00050000 // RX and TX Relay control for channel 0 : set for direct, clear for loop
#define FPGA_GCMD_BIT_RELAY_CTRL_REL_OUT_0            0x00040000 // TX Relay control for channel 0 : set for direct, clear for loop
#define FPGA_GCMD_BIT_RELAY_CTRL_LOOP_1               0x000A0000 // RX and TX Relay control for channel 1 : set for direct, clear for loop
#define FPGA_GCMD_BIT_RELAY_CTRL_REL_OUT_1            0x00080000 // TX Relay control for channel 1 : set for direct, clear for loop
#define FPGA_GCMD_MSK_HD_TYPE                         0x00001000
#define FPGA_GCMD_BIT_HD_TYPE_DEFAULT                 0x00000000
#define FPGA_GCMD_BIT_HD_TYPE_MODIFIED_CLK            0x00001000
#define FPGA_GCMD_BIT_VTC_START                       0x00000800 // Start the master timing generator
#define FPGA_GCMD_BIT_VTC_RESET_N                     0x00000400 // Master video timing generator reset, clears the configuration address pointer
#define FPGA_GCMD_BIT_START_OF_FRAME                  0x00000200 // Enable global start_of_frame. If 0, MVTG and VTG are in freerun. Force MVTG alignement on a 0 to 1 transition. 
#define FPGA_GCMD_BIT_GEN_PROGRESSIVE                 0x00000100 // Set when genlock video standard is progressive
#define FPGA_GCMD_BIT_GEN_MODE_RX2_RX3                0x00000080 // Set this bit to use RX2/RX3 instead of RX0/RX1 in Genlock Mode configuration (FPGA_GCMD_MSK_GENLOCK_SOURCE)
#define FPGA_GCMD_BIT_4911_LOCK_LOST                  0x00000080 // Set when 4911 lost lock (the output clock is not locked on the input. Possible causes : 1. bad config (different input and output clk_div) 2. Limit input (freq...) 3. other
#define FPGA_GCMD_BIT_4911_REF_LOST                   0x00000040 // Set when 4911 lost ref (a video std cannot be identified on the base of F,H,V flags)
#define FPGA_GCMD_BIT_MVTG_LOCKED                     0x00000020 // Maser Video Timing Generator Locked status (Read only)
#define FPGA_04_GCMD_BIT_TX3_TX2_CLK_TYPE             0x00000040 // '0' = HD - '1' = SD
#define FPGA_04_GCMD_BIT_TX1_TX0_CLK_TYPE             0x00000020 // '0' = HD - '1' = SD
#define FPGA_3G_GCMD_MSK_GEN_CLK_FREQ                 0x00000018 // �00� SD clock = 13.5 MHz - �01� HD clock = 74.25 MHz / 74.175 MHz - �11� 3G clock = 148.5 MHz / 148.35 MHz
#define FPGA_HD_GCMD_BIT_GEN_CLK_HD                   0x00000008 // Gen_clk is SD_CLK when 0 else HD_CLK
#define FPGA_GCMD_BIT_SSN_NO_SLAVE                    0x00000004 // No slave response detected
#define FPGA_GCMD_BIT_SSN_RDY                         0x00000002 // SSN interface ready
#define FPGA_GCMD_BIT_SSN_READ                        0x00000002 // SSN read command pulse (write 1 then 0)
#define FPGA_GCMD_BIT_DD_READY_STS                    0x00000001 // DDRAM Interface ready status (Read Only)
#define FPGA_GCMD_BIT_FW_WARM_LOAD                    0x00000001 // Set this bit to �1� to active loading of new FPGA firmware during next PCIe reset event.  (Write Only)

#define FPGA_GCMD_BIT_GENLOCK_SOURCE(genlock_source)      ((genlock_source<<14)&FPGA_GCMD_MSK_GENLOCK_SOURCE)
#define FPGA_GCMD_GET_BIT_GENLOCK_SOURCE(reg)             ((reg&FPGA_GCMD_MSK_GENLOCK_SOURCE) >>14)
#define FPGA_GCMD_BIT_OUTPUT_PATH(outputpath)             ((outputpath<<20) &FPGA_GCMD_MSK_OUTPUT_PATH)
#define FPGA_GCMD_GET_BIT_OUTPUT_PATH(reg)                ((reg & FPGA_GCMD_MSK_OUTPUT_PATH)>>20)
#define FPGA_GCMD_GET_BIT_PCIE_LANE_NB(reg)               ((reg&FPGA_GCMD_MSK_PCIE_LANE_NB) >>20)
#define FPGA_3G_GCMD_BIT_GEN_CLK_FREQ(clk_freq)           ((clk_freq<<3)&FPGA_3G_GCMD_MSK_GEN_CLK_FREQ)
#define FPGA_GCMD_BIT_ARM_BOOT(boot_space)                ((boot_space<<24)&FPGA_GCMD_MSK_ARM_BOOT)

#define FPGA_HDMI_CTL_BIT_ENABLE                      0x00000001
#define FPGA_HDMI_CTL_BIT_TX                          0x00000002

#define FPGA_04_HDMI0_CTL_BIT_PHYSICAL_TX0             0x00000000
#define FPGA_04_HDMI0_CTL_BIT_PHYSICAL_TX1             0x00000002
#define FPGA_04_HDMI0_CTL_BIT_PHYSICAL_TX2             0x00000004
#define FPGA_04_HDMI0_CTL_BIT_PHYSICAL_TX0_SBS_TX1     0x00000008
#define FPGA_04_HDMI0_CTL_BIT_PHYSICAL_TX0_SBS_TX2     0x0000000A


#define FPGA_04_HDMI1_CTL_BIT_PHYSICAL_TX2             0x00000000
#define FPGA_04_HDMI1_CTL_BIT_PHYSICAL_TX3             0x00000002
#define FPGA_04_HDMI1_CTL_BIT_PHYSICAL_TX1             0x00000004
#define FPGA_04_HDMI1_CTL_BIT_PHYSICAL_TX2_SBS_TX3     0x00000008
#define FPGA_04_HDMI1_CTL_BIT_PHYSICAL_TX1_SBS_TX3     0x0000000A

#define FPGA_04_HDMI0_CTL_MSK                          0x0000000E
#define FPGA_04_HDMI1_CTL_MSK                          0x000000E0


#define FPGA_HDMI_CTL_HDMI0(reg)                      (reg)
#define FPGA_HDMI_CTL_HDMI1(reg)                      (reg<<4)

#define ARM_BOOT_FLASH                 0x0 /* main flash memory is selected as boot space */ 
#define ARM_BOOT_ROM                   0x1 /* system memory is selected as boot space  */ 
#define ARM_BOOT_RAM                   0x2 /* embedded SRAM is selected as boot space */ 

#define FPGA_40_GCMD_BIT_BTC_FRAME_RATE(frame_rate)       ((frame_rate<<23)&FPGA_40_GCMD_MSK_BTC_FRAME_RATE)


#define BTC_FRAME_RATE_24        0
#define BTC_FRAME_RATE_25        1
#define BTC_FRAME_RATE_30        2 
#define BTC_FRAME_RATE_50        3
#define BTC_FRAME_RATE_60        4


// -- FPGA_PARAM_ROM_A_MM32 : Base Addresses configurator Indirext access address (W)----------------------------------
// This register is uses with the FPGA_PARAM_ROM_D_MM32 (data) register to configure the on board base addresses for all
// storage area
#define FPGA_PARAM_ROM_A_VALUE_RXx_VID(ChannelIdx, BufIdx)  (0x40 + (ChannelIdx*0x18) + BufIdx)
#define FPGA_PARAM_ROM_A_VALUE_RXx_CANC(ChannelIdx, BufIdx) (0x40 + (ChannelIdx*0x18) + BufIdx + 0x08)
#define FPGA_PARAM_ROM_A_VALUE_RXx_YANC(ChannelIdx, BufIdx) (0x40 + (ChannelIdx*0x18) + BufIdx + 0x10)

#define FPGA_PARAM_ROM_A_VALUE_TXx_VID(ChannelIdx, BufIdx)  (0x80 + (ChannelIdx*0x18) + BufIdx)
#define FPGA_PARAM_ROM_A_VALUE_TXx_CANC(ChannelIdx, BufIdx) (0x80 + (ChannelIdx*0x18) + BufIdx + 0x08)
#define FPGA_PARAM_ROM_A_VALUE_TXx_YANC(ChannelIdx, BufIdx) (0x80 + (ChannelIdx*0x18) + BufIdx + 0x10)

#define FPGA_PARAM_ROM_A_VALUE_RX0_VBICONF  0x0C0
#define FPGA_PARAM_ROM_A_VALUE_RX1_VBICONF  0x100
#define FPGA_PARAM_ROM_A_VALUE_RX2_VBICONF  0x2C0
#define FPGA_PARAM_ROM_A_VALUE_RX3_VBICONF  0x300

#define VBICONF_MAX_NBOF_CAPTURE_LINE                    64
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_MSK_LINE      0x07FF
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_BIT_CAPT_C    0x0800
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_BIT_CAPT_Y    0x1000





// -- FPGA_GSPI_MM32 : General SPI Register (RW) ----------------------------------------------------------------------
// This register allows to write or read 16-bit data on SPI bus. For a write access, write the 32-bit correctly formatted
// with FPGA_GSPI_BIT_ACCESS_WR set. For read access, write a 32-bit data in the register with dummy data in data field and
// the ACCESS bit set to RD. After that, perform a read access to this register to retrieve the read data.

#define FPGA_GSPI_MSK_DATA                            0xFFFF0000
#define FPGA_GSPI_BIT_ACCESS_WR                       0x00008000
#define FPGA_GSPI_BIT_ACCESS_RD                       0x00000000
#define FPGA_GSPI_MSK_CHIP_SELECT                     0x00007000
#define FPGA_GSPI_BIT_CHIP_SELECT_GEN4911             0x00000000
#define FPGA_GSPI_BIT_CHIP_SELECT_GEN_RX0             0x00001000
#define FPGA_GSPI_BIT_CHIP_SELECT_GEN_RX1             0x00002000
#define FPGA_GSPI_BIT_CHIP_SELECT_GEN_TX0             0x00003000
#define FPGA_GSPI_BIT_CHIP_SELECT_GEN_TX1             0x00004000
#define FPGA_GSPI_MSK_REG_ADDR                        0x00000FFF
#define FPGA_GSPI_BIT_BUSY                            0x00000001

#define GSPI_CHIP_SELECT_GEN4911                      0x00
#define GSPI_CHIP_SELECT_GEN_RX0                      0x01
#define GSPI_CHIP_SELECT_GEN_RX1                      0x03
#define GSPI_CHIP_SELECT_GEN_TX0                      0x02
#define GSPI_CHIP_SELECT_GEN_TX1                      0x04
#define GSPI_CHIP_SELECT_GEN_TX2                      0x05
#define GSPI_CHIP_SELECT_GEN_TX3                      0x06

#define FPGA_GSPI_BIT_CHIP_SELECT_x(chip_select)      ((chip_select<<12)&FPGA_GSPI_MSK_CHIP_SELECT)

// -- FPGA_CHANNEL_CTL_MM32 : Channels Command Registers (WR) ----------------------------------------------------------
// Managing of Gennum ser/des interface chips reset
#define FPGA_CHANNEL_CTL_BIT_RX0_ACTIVE               0x00000001  // Active Low Reset for RX 0 Channel Gennum de-serializer
#define FPGA_CHANNEL_CTL_BIT_RX1_ACTIVE               0x00000002  // Active Low Reset for RX 1 Channel Gennum de-serializer
#define FPGA_CHANNEL_CTL_BIT_TX0_ACTIVE               0x00000004  // Active Low Reset for TX 0 Channel Gennum serializer
#define FPGA_CHANNEL_CTL_BIT_TX1_ACTIVE               0x00000008  // Active Low Reset for TX 1 Channel Gennum serializer

// -- FPGA_RX0_MM32 : RX0 Cmd/Sts Register (WR) -----------------------------------------------------------------------
// -- FPGA_RX1_MM32 : RX1 Cmd/Sts Register (WR) -----------------------------------------------------------------------

#define FPGA_RXx_BIT_RX1_STS_VALID       0x80000000  // Valid Data detected on Deserializer 1              (R)
#define FPGA_RXx_BIT_RX0_STS_VALID       0x40000000  // Valid Data detected on Deserializer 0              (R)
#define FPGA_RXx_BIT_SMPTE_COMPLIANT     0x20000000  // SMPTE Compliant data found                         (R)
#define FPGA_RXx_BIT_DVB_ASI_COMPLIANT   0x10000000  // DVB ASI Compliant data found                       (R)
#define FPGA_RXx_BIT_LOCKED              0x08000000  // Deserializer locked                                (R)
#define FPGA_3G_RXx_BIT_DATA_ERROR       0x04000000  // Error during CRC checking (HD only)                (R)
#define FPGA_3G_RXx_BIT_STDI_VALID       0x02000000  // RXi_STD register value is valid                    (R)
#define FPGA_HD_RXx_BIT_SD_SDI           0x02000000  // SD SDI content detected (HD oherwise)              (R)
#define FPGA_RXx_BIT_RXS_ALIGNED         0x01000000  // RX1 and RX2 aligned (useful for channel pairing)   (R)
#define FPGA_HD_RXx_BIT_CLK_DISCR        0x00800000  // Discrimination between EU and US clock is done. Before this, only European std are reported. (R)
#define FPGA_3G_RXx_BIT_EU_CLK           0x00800000  // '1' when EU and '0' when US clock is reported      (R)
#define FPGA_3G_RXx_MSK_SAMPLE_RATE      0x00600000  // '00' = HD stream - '01' = SD stream - '10' = 3G stream (R)
#define FPGA_RXx_BIT_3D_MODE             0x00400000  // '0' = Normal mode - '1'= 3D mode                  (W)
#define FPGA_RXx_BIT_CARRIER_NOT_DETECT  0x00100000  // Equalizer carrier detect                          (R)
#define FPGA_3G_RXx_MSK_INPUT_FORMAT     0x000F0000  // Input format (valid if locked = '1' and if input mode = '00'(HD)) (R)
#define FPGA_3G_RXx_BIT_DETECTED_LEVELB  0x00008000  // LevelB detected                                    (R)
#define FPGA_RXx_BIT_DUAL_LINK           0x00000800  // Set this bit to capture in dual link mode          (W)
#define FPGA_3G_RXx_BIT_LEVELB           0x00000400  // Set this bit to enable capture of 3G.B stream (RX0 only) (W)
#define FPGA_RXx_BIT_VBI10BITS           0x00000400  // Set this bit to select VBI 10-bits packing - Clr this bit to select VBI 8-bits packing
#define FPGA_RXx_MSK_VIDEO_STD           0x00000300  // Video standard (this information is obtained from Genum chip) (W)
#define FPGA_RXx_MSK_PACKING             0x000000F0  // Packing schema                                                (W)
#define FPGA_RXx_MSK_CAPTURE_MODE        0x00000007  // Capture Mode                                                  (W)
#define FPGA_RXx_BIT_THUMBNAIL           0x00000008  // Enable thumbnail feature 
#define FPGA_RXx_STD_BIT_PROGRESSIVE     0x00400000  // Scanning Method : '0' = interlace - '1' = progressive         (R)

#define FPGA_3G_RXx_GET_BIT_SAMPLE_RATE(reg)         ((reg&FPGA_3G_RXx_MSK_SAMPLE_RATE)>>21)
#define RX_SAMPLE_RATE_HD               0x00        // HD stream
#define RX_SAMPLE_RATE_SD               0x01        // SD stream
#define RX_SAMPLE_RATE_3G               0x02        // 3G stream
#define RX_SAMPLE_RATE_INVALID          0x03        // Invalid


#define FPGA_3G_RXx_GET_BIT_INPUT_FORMAT(reg)       ((reg&FPGA_3G_RXx_MSK_INPUT_FORMAT)>>16)
#define RX_INPUT_FORMAT_S274M_1080i_60Hz  0x2         // SMPTE 274M 1080i 59.94Hz and 60Hz
#define RX_INPUT_FORMAT_S274M_1080i_50Hz  0x3         // SMPTE 274M 1080i 50Hz
#define RX_INPUT_FORMAT_S274M_1080p_30Hz  0x4         // SMPTE 274M 1080p 29.97Hz and 30Hz
#define RX_INPUT_FORMAT_S274M_1080p_25Hz  0x5         // SMPTE 274M 1080p 25Hz
#define RX_INPUT_FORMAT_S274M_1080p_24Hz  0x6         // SMPTE 274M 1080p 23.98Hz and 24Hz
#define RX_INPUT_FORMAT_S296M_720p_60Hz   0x7         // SMPTE 296M 720p 59.94Hz and 60Hz
#define RX_INPUT_FORMAT_S296M_720p_50Hz   0x9         // SMPTE 296M 720p 50Hz
#define RX_INPUT_FORMAT_S296M_720p_30Hz   0xA         // SMPTE 296M 720p 29.97Hz and 30Hz
#define RX_INPUT_FORMAT_S296M_720p_25Hz   0xB         // SMPTE 296M 720p 25Hz
#define RX_INPUT_FORMAT_S296M_720p_24Hz   0xC         // SMPTE 295M 720p 23.98Hz and 24Hz
#define RX_INPUT_FORMAT_S274M_1080p_60Hz  0xD         // SMPTE 274M 1080p 59.94Hz and 60Hz
#define RX_INPUT_FORMAT_S274M_1080p_50Hz  0xE         // SMPTE 274M 1080p 50Hz
#define RX_INPUT_FORMAT_S274M_1080psf_24Hz  0x8       // SMPTE 274M 1080psf 24Hz

#define FPGA_RXx_BIT_VIDEO_STD(video_standard)   ((video_standard<<8) & FPGA_RXx_MSK_VIDEO_STD)
#define RX_VIDEO_STD_HD                0x1         // HD Video Standard
#define RX_VIDEO_STD_SD_PAL            0x2         // SD PAL
#define RX_VIDEO_STD_SD_NTSC           0x3         // SD NTSC

#define FPGA_RXx_BIT_PACKING(packing_mode)  ((packing_mode<<4) & FPGA_RXx_MSK_PACKING)
#define RX_PACKING_V208     PACKING_V208   // 4:2:2:0  8-bit YUV  video packing 
#define RX_PACKING_AV208    PACKING_AV208  // 4:2:2:4  8-bit YUVA video packing 
#define RX_PACKING_V210     PACKING_V210   // 4:2:2:0 10-bit YUV  video packing 
#define RX_PACKING_AV210    PACKING_AV210  // 4:2:2:4 10-bit YUVA video packing 
#define RX_PACKING_V408     PACKING_V408   // 4:4:4:0  8-bit YUV  video packing 
#define RX_PACKING_AV408    PACKING_AV408  // 4:4:4:4  8-bit YUVA video packing 
#define RX_PACKING_V410     PACKING_V410   // 4:4:4:0 10-bit YUV  video packing 
#define RX_PACKING_AV410    PACKING_AV410  // 4:4:4:4 10-bit YUVA video packing 
#define RX_PACKING_C408     PACKING_C408   // 4:4:4:0  8-bit RGB  video packing 
#define RX_PACKING_AC408    PACKING_AC408  // 4:4:4:4  8-bit RGBA video packing 

#define FPGA_RXx_BIT_CAPTURE_MODE(mode)      ((mode)&FPGA_RXx_MSK_CAPTURE_MODE)
#define RX_CAPTURE_MODE_RAW            0x7
#define RX_CAPTURE_MODE_VIDEO          0x3
#define RX_CAPTURE_MODE_VIDEO_ANC      0x1
#define RX_CAPTURE_MODE_ANC            0x5
#define RX_CAPTURE_MODE_DISABLED       0x0


#define RX0 0
#define RX1 1
#define RX2 2
#define RX3 3
#define FPGA_RXx_MM32(Channel)         FPGA_RX##Channel##_MM32



// -- FPGA_TX0_MM32 : TX1 Cmd/Sts Register (WR) -----------------------------------------------------------------------
// -- FPGA_TX1_MM32 : TX2 Cmd/Sts Register (WR) -----------------------------------------------------------------------
#define FPGA_TXx_BIT_VTG_RST_N         0x80000000  // Reset the address pointer and all counter
#define FPGA_HD_TXx_BIT_FRAME_ACC_ENABLE  0x40000000  // Enable Frame Accurate Feature on TXx
#define FPGA_3G_TXx_BIT_LEVELB         0x40000000  // Set to 1 to activate level B on TX0
#define FPGA_TXx_BIT_GENLOCK_ENABLE    0x20000000  // Enable TXx Genlocking
#define FPGA_TXx_BIT_SMPTE352M_ENABLE  0x10000000  // Enable automatic SMPTE352M packet insertion on HDe board
#define FPGA_HD_TXx_BIT_3D_MODE        0x08000000  // '0' = Normal mode - '1'= 3D mode                  (R/W)
#define FPGA_3G_TXx_MSK_SAMPLE_RATE    0x0C000000  // 3G ONLY : 00 = HD-SDI (including dual link) - 01 = SD-SDI - 10 = 3G-SDI(level A and level B) - 11 = Invalid
#define FPGA_3G_TXx_BIT_3D_MODE        0x02000000  // '0' = Normal mode - '1'= 3D mode                  (R/W)
#define FPGA_TXx_BIT_RX_ON_TX(delay)   ((1<<23)|((delay)<<20))
#define FPGA_HD_TXx_HD                 0x00080000  // Set for HD transmission, clear for SD transmission
#define FPGA_TXx_CLR_BUF_RDY           0x00040000  // Set to clear buffer ready information
#define FPGA_HD04_TXx_BIT_CSC_ENABLE   0x00020000  // Enable the Color Space Converter (RGB>YUV)
#define FPGA_HD04_TXx_BIT_CHROMA_BLACK_B 0x00010000  // '0' = Replace chroma by black into paired stream - '1' = Use replicated chroma into paired stream
#define FPGA_CODEC_TXx_MSK_HW_BUFIDX   0x00038000  // Hardware current buffer index
#define FPGA_TXx_BIT_IOPROC_DISABLE    0x00004000  // Disable Serializer IO Processing
#define FPGA_TXx_BIT_KEY_NEUTRAL_CHROMA 0x00001000 // '0' = Chroma values of Key stream are equal to Fill ones - '1' = Chroma values of Key stream are forced to Neutral value (512 on 10-bit)
#define FPGA_TXx_MSK_PACKING           0x00000F00  // Packing scheme
#define FPGA_TXx_ARM_FOR_PRELOAD       0x00000020  // Arm the channel allowing preloading frames on-board
#define FPGA_TXx_BIT_CLAMPING          0x00000010  // Enable clamping
#define FPGA_TXx_MSK_OUTPUT_MODE       0x0000000F  // Output Mode

#define FPGA_3G_TXx_BIT_SAMPLE_RATE(tx_mode)        (tx_mode<<26) & FPGA_3G_TXx_MSK_SAMPLE_RATE
#define FPGA_3G_TXx_GET_BIT_SAMPLE_RATE(reg)        ((reg& FPGA_3G_TXx_MSK_SAMPLE_RATE) >> 26)
#define TX_MODE_HD                    0x00        // HD stream
#define TX_MODE_SD                    0x01        // SD stream
#define TX_MODE_3G                    0x02        // 3G stream
#define TX_MODE_INVALID               0x03        // Invalid

#define FPGA_TXx_GET_BIT_HW_BUFIDX(reg)   ((reg&FPGA_CODEC_TXx_MSK_HW_BUFIDX) >> 15)

#define FPGA_TXx_BIT_PACKING(packing_mode)  (packing_mode<<8) & FPGA_TXx_MSK_PACKING
#define FPGA_TXx_GET_BIT_PACKING(reg)       ((reg& FPGA_TXx_MSK_PACKING) >> 8)
#define TX_PACKING_V208     PACKING_V208   // 4:2:2:0  8-bit YUV  video packing 
#define TX_PACKING_AV208    PACKING_AV208  // 4:2:2:4  8-bit YUVA video packing 
#define TX_PACKING_V210     PACKING_V210   // 4:2:2:0 10-bit YUV  video packing 
#define TX_PACKING_AV210    PACKING_AV210  // 4:2:2:4 10-bit YUVA video packing 
#define TX_PACKING_V408     PACKING_V408   // 4:4:4:0  8-bit YUV  video packing 
#define TX_PACKING_AV408    PACKING_AV408  // 4:4:4:4  8-bit YUVA video packing 
#define TX_PACKING_V410     PACKING_V410   // 4:4:4:0 10-bit YUV  video packing 
#define TX_PACKING_AV410    PACKING_AV410  // 4:4:4:4 10-bit YUVA video packing 
#define TX_PACKING_C408     PACKING_C408   // 4:4:4:0  8-bit RGB  video packing 
#define TX_PACKING_AC408    PACKING_AC408  // 4:4:4:4  8-bit RGBA video packing 

#define FPGA_TXx_BIT_OUTPUT_MODE(mode)       ((mode)&FPGA_TXx_MSK_OUTPUT_MODE)
#define FPGA_TXx_GET_BIT_OUTPUT_MODE(reg)   (reg&FPGA_TXx_MSK_OUTPUT_MODE)
#define TX_OUTPUT_MODE_DISABLED        0x0
#define TX_OUTPUT_MODE_VIDEO_ANC       0x1
#define TX_OUTPUT_MODE_COLOR_BAR       0x2
#define TX_OUTPUT_MODE_VIDEO           0x3
#define TX_OUTPUT_MODE_ANC             0x5
#define TX_OUTPUT_MODE_RAW             0x7


#define TX1 0
#define TX2 1
#define FPGA_TXx_MM32(Channel)         FPGA_TX##Channel##_MM32


// -- FPGA_ISR_MM32 : Interrupt Statue Register (RO) ------------------------------------------------------------------




#define FPGA_ISR_BIT_RX0               0x00000008
#define FPGA_ISR_BIT_TX2               0x00000008
#define FPGA_ISR_BIT_RX0_STS_CHANGE    0x00000004
#define FPGA_ISR_BIT_RX0_OVERRUN       0x00000002
#define FPGA_ISR_BIT_TX2_UNDERRUN      0x00000002
#define FPGA_ISR_BIT_RX0_FIELD         0x00000001
#define FPGA_ISR_BIT_RX1               0x00000080
#define FPGA_ISR_BIT_TX3               0x00000080
#define FPGA_ISR_BIT_RX1_STS_CHANGE    0x00000040
#define FPGA_ISR_BIT_RX1_OVERRUN       0x00000020
#define FPGA_ISR_BIT_TX3_UNDERRUN      0x00000020
#define FPGA_ISR_BIT_RX1_FIELD         0x00000010
#define FPGA_ISR_BIT_RX2               0x00000800
#define FPGA_ISR_BIT_RX2_STS_CHANGE    0x00000400
#define FPGA_ISR_BIT_RX2_OVERRUN       0x00000200
#define FPGA_ISR_BIT_RX2_FIELD         0x00000100
#define FPGA_ISR_BIT_RX3               0x00008000
#define FPGA_ISR_BIT_RX3_STS_CHANGE    0x00004000
#define FPGA_ISR_BIT_RX3_OVERRUN       0x00002000
#define FPGA_ISR_BIT_RX3_FIELD         0x00001000
#define FPGA_ISR_BIT_TX0               0x00000800
#define FPGA_ISR_BIT_TX0_UNDERRUN      0x00000200
#define FPGA_ISR_BIT_TX1               0x00008000
#define FPGA_ISR_BIT_TX1_UNDERRUN      0x00002000
#define FPGA_ISR_BIT_EOD               0x00010000     // End Of DMA
#define FPGA_ISR_BIT_EOD1              0x00020000     /* Interrupt generated when the DMA byte count has been reached. */     
#define FPGA_ISR_BIT_RX0_CANC          0x00020000     // RX 0 Channel C ANC Data ready
#define FPGA_ISR_BIT_RX0_YANC          0x00040000     // RX 0 Channel Y ANC Data ready
#define FPGA_ISR_BIT_MB_TX_EMPTY       0x00040000 /* Mailbox TX empty / End of �C Command */
#define FPGA_ISR_BIT_MB_RX_FULL        0x00080000 /* Mailbox RX full */
#define FPGA_ISR_BIT_RX1_CANC          0x00080000     // RX 1 Channel C ANC Data ready
#define FPGA_ISR_BIT_DCM_STS_CHANGE    0x00100000     
#define FPGA_ISR_BIT_GS4911_LOCK_LOST  0x00200000
#define FPGA_ISR_BIT_GS4911_REF_LOST   0x00400000
#define FPGA_ISR_BIT_GNLK_STS_CHANGE   0x00800000     // Genlock status change
#define FPGA_ISR_BIT_WDTO              0x10000000     // Watchdog Timeout detected
#define FPGA_ISR_BIT_WDEN              0x20000000     // Watchdog Enabled
#define FPGA_ISR_BIT_FIELD_IRQ         0x40000000     // Field interrupt from MVTG
#define FPGA_ISR_BIT_FRAME_IRQ         0x80000000     // Frame interrupt from MVTG




// -- FPGA_IMR_MM32 : Interrupt Mask Register (WO) --------------------------------------------------------------------

#define FPGA_IMR_BIT_RX0               FPGA_ISR_BIT_RX0 
#define FPGA_IMR_BIT_RX0_STS_CHANGE    FPGA_ISR_BIT_RX0_STS_CHANGE
#define FPGA_IMR_BIT_RX0_OVERRUN       FPGA_ISR_BIT_RX0_OVERRUN     
#define FPGA_IMR_BIT_RX1               FPGA_ISR_BIT_RX1 
#define FPGA_IMR_BIT_RX1_STS_CHANGE    FPGA_ISR_BIT_RX1_STS_CHANGE
#define FPGA_IMR_BIT_RX1_OVERRUN       FPGA_ISR_BIT_RX1_OVERRUN    
#define FPGA_IMR_BIT_RX2               FPGA_ISR_BIT_RX2 
#define FPGA_IMR_BIT_RX2_STS_CHANGE    FPGA_ISR_BIT_RX2_STS_CHANGE
#define FPGA_IMR_BIT_RX2_OVERRUN       FPGA_ISR_BIT_RX2_OVERRUN     
#define FPGA_IMR_BIT_RX3               FPGA_ISR_BIT_RX3 
#define FPGA_IMR_BIT_RX3_STS_CHANGE    FPGA_ISR_BIT_RX3_STS_CHANGE
#define FPGA_IMR_BIT_RX3_OVERRUN       FPGA_ISR_BIT_RX3_OVERRUN  
#define FPGA_IMR_BIT_TX0               FPGA_ISR_BIT_TX0 
#define FPGA_IMR_BIT_TX0_UNDERRUN      FPGA_ISR_BIT_TX0_UNDERRUN
#define FPGA_IMR_BIT_TX1               FPGA_ISR_BIT_TX1
#define FPGA_IMR_BIT_TX1_UNDERRUN      FPGA_ISR_BIT_TX1_UNDERRUN
#define FPGA_IMR_BIT_TX2               FPGA_ISR_BIT_TX2 
#define FPGA_IMR_BIT_TX2_UNDERRUN      FPGA_ISR_BIT_TX2_UNDERRUN
#define FPGA_IMR_BIT_TX3               FPGA_ISR_BIT_TX3
#define FPGA_IMR_BIT_TX3_UNDERRUN      FPGA_ISR_BIT_TX3_UNDERRUN
#define FPGA_IMR_BIT_EOD               FPGA_ISR_BIT_EOD 
#define FPGA_IMR_BIT_EOD1              FPGA_ISR_BIT_EOD1 

#define FPGA_IMR_BIT_RX0_CANC          FPGA_ISR_BIT_RX0_CANC         
#define FPGA_IMR_BIT_RX0_YANC          FPGA_ISR_BIT_RX0_YANC         
#define FPGA_IMR_BIT_RX1_CANC          FPGA_ISR_BIT_RX1_CANC         
#define FPGA_IMR_BIT_DCM_STS_CHANGE    FPGA_ISR_BIT_DCM_STS_CHANGE  
#define FPGA_IMR_BIT_GS4911_LOCK_LOST  FPGA_ISR_BIT_GS4911_LOCK_LOST  
#define FPGA_IMR_BIT_GS4911_REF_LOST   FPGA_ISR_BIT_GS4911_REF_LOST   
#define FPGA_IMR_BIT_GNLK_STS_CHANGE   FPGA_ISR_BIT_GNLK_STS_CHANGE  
#define FPGA_IMR_BIT_WDTO              FPGA_ISR_BIT_WDTO  
#define FPGA_IMR_BIT_WDEN              FPGA_ISR_BIT_WDEN             
#define FPGA_IMR_BIT_MB_TX_EMPTY       FPGA_ISR_BIT_MB_TX_EMPTY     
#define FPGA_IMR_BIT_MB_RX_FULL        FPGA_ISR_BIT_MB_RX_FULL      




// -- FPGA_SSR_MM32 : Manual Set of Interrupt Status (WO) -------------------------------------------------------------
// This register is used to manually set an interrupt. This is useful to simulate TX interrupt during pre-loading 
// phase. The bits are auto-clear.

#define FPGA_SSR_BIT_RX0               FPGA_ISR_BIT_RX0 
#define FPGA_SSR_BIT_RX0_STS_CHANGE    FPGA_ISR_BIT_RX0_STS_CHANGE
#define FPGA_SSR_BIT_RX1               FPGA_ISR_BIT_RX1 
#define FPGA_SSR_BIT_RX1_STS_CHANGE    FPGA_ISR_BIT_RX1_STS_CHANGE
#define FPGA_SSR_BIT_RX2               FPGA_ISR_BIT_RX2 
#define FPGA_SSR_BIT_RX2_STS_CHANGE    FPGA_ISR_BIT_RX2_STS_CHANGE
#define FPGA_SSR_BIT_RX3               FPGA_ISR_BIT_RX3 
#define FPGA_SSR_BIT_RX3_STS_CHANGE    FPGA_ISR_BIT_RX3_STS_CHANGE
#define FPGA_SSR_BIT_TX0               FPGA_ISR_BIT_TX0 
#define FPGA_SSR_BIT_TX1               FPGA_ISR_BIT_TX1 
#define FPGA_SSR_BIT_TX2               FPGA_ISR_BIT_TX2 
#define FPGA_SSR_BIT_TX3               FPGA_ISR_BIT_TX3 
#define FPGA_SSR_BIT_EOD               FPGA_ISR_BIT_EOD 
#define FPGA_SSR_BIT_RX0_CANC          FPGA_ISR_BIT_RX0_CANC         
#define FPGA_SSR_BIT_RX0_YANC          FPGA_ISR_BIT_RX0_YANC         
#define FPGA_SSR_BIT_RX1_CANC          FPGA_ISR_BIT_RX1_CANC         
#define FPGA_SSR_BIT_DCM_STS_CHANGE    FPGA_ISR_BIT_DCM_STS_CHANGE         
#define FPGA_SSR_BIT_GNLK_STS_CHANGE   FPGA_ISR_BIT_GNLK_STS_CHANGE  
#define FPGA_SSR_BIT_WDTO              FPGA_ISR_BIT_WDTO  
#define FPGA_SSR_BIT_WDEN              FPGA_ISR_BIT_WDEN     

// -- FPGA_END_OF_DMA_MM32 : End of DMA (WO) -------------------------------------------------------------
#define FPGA_EOD_RX0                    0x00000001
#define FPGA_EOD_RX1                    0x00000002
#define FPGA_EOD_TX0                    0x00000004
#define FPGA_EOD_TX1                    0x00000008
#define FPGA_EOD_RX2                    0x00000010
#define FPGA_EOD_RX3                    0x00000020
#define FPGA_EOD_TX2                    0x00000040
#define FPGA_EOD_TX3                    0x00000080

// -- FPGA_TXi_VTC_A_MM32 : TX channels VTC programming register (indirect addressing) --------------------------------

#define FPGA_TXi_VTC_A_TY_SPL          0     /* Total pixel samples per line */
#define FPGA_TXi_VTC_A_AY_SPL          1     /* Active pixel samples per line */
#define FPGA_TXi_VTC_A_TL_LPF          2     /* Total lines per frame */
#define FPGA_TXi_VTC_A_AL_LPF          3     /* Active lines per frame */
#define FPGA_TXi_VTC_A_VBLANK_START1   4     /* Start of first vertical line blanking */
#define FPGA_TXi_VTC_A_VBLANK_STOP1    5     /* End of first vertical line blanking */
#define FPGA_TXi_VTC_A_VBLANK_START2   6     /* Start of second vertical line blanking (interlaced modes only) */
#define FPGA_TXi_VTC_A_VBLANK_STOP2    7     /* End of first second line blanking (interlaced modes only) */
#define FPGA_TXi_VTC_A_FIELD_START     8     /* Start of odd field */
#define FPGA_TXi_VTC_A_FIELD_STOP      9     /* End of odd field (= start of odd field in progressive modes) */

// -- FPGA_MTG_CFG_SHRMM32 : MTG configuration  (W) -------------------------------------------------------------
#define FPGA_MTG_CFG_MSK_WAITING_FRAMES      0xFF000000  /* (W) Number of frame to wait during which internal locked signal has to be set. (MAX  = 64)
                                                            Remark : The FPGA_MTG_CFG_MSK_WAITING_FRAMES is used to implement the safe MTG lock workaround.
                                                            The safe MTG lock workaround waits for x frames to be aligned between genlock source and MTG 
                                                            before setting the MTG locked status. This workaround avoids multiple MTG lock-unlock togglings
                                                            due to jitter in the pixel clock that could exists during the 4911 locking phase */
#define FPGA_MTG_CFG_MSK_JITTER_WINDOW       0x00FF0000  /* (W) Window in which the rebuilt top of frame will stand relative to the
                                                            reference top of frame. Setting this value to X actually defines a
                                                            window between �X and +X around the reference pulse. */

#define FPGA_MTG_CFG_BIT_WAITING_FRAMES(waiting_frames)   (((waiting_frames-1)<<24) & FPGA_MTG_CFG_MSK_WAITING_FRAMES)
#define FPGA_MTG_CFG_BIT_JITTER_WINDOW(jitter_win)        ((jitter_win<<16) & FPGA_MTG_CFG_MSK_JITTER_WINDOW)


// -- FPGA_MTG_STS_MM32 : MTG status  (R) -------------------------------------------------------------
#define FPGA_MTG_STS_MSK_JITTER_CNT_SIGN     0x000000C0  /* (R) 10 = Lagging: the rebuilt top of frame comes after the reference
                                                                01 = Leading: the rebuilt top of frame comes before the reference 
                                                                00 = Perfect alignment */
#define FPGA_MTG_STS_MSK_JITTER_CNT          0x0000003F  /* (R) Jitter counter value representing the offset between the rebuilt and
                                                            the reference top of frame with the imprecision of 1 pixel.*/

#define JITTER_LAG         0x10
#define JITTER_LEAD        0x01
#define JITTER_NO          0x00


#define FPGA_MTG_STS_GET_BIT_JITTER_CNT_SIGN(reg)        ((reg&FPGA_MTG_STS_MSK_JITTER_CNT_SIGN)>>6)
#define FPGA_MTG_STS_GET_BIT_JITTER_CNT(reg)             (reg&FPGA_MTG_STS_MSK_JITTER_CNT)


// -- FPGA_KEYERCTL_MM32 : Chromakeyer control (WO) -------------------------------------------------------------

#define FPGA_KEYERCTL_BIT_CHROMAKEY_EN       0x00000001  /* Set this bit to '1' to activate the Video Processing */
#define FPGA_KEYERCTL_BIT_RX_RGBA2YUV        0x00000002  /* Set this bit to '1' when RXs are in dual link (4:4:4:4; RGBA). Active automatic RGB->YUV */
#define FPGA_KEYERCTL_MSK_RX_ENABLE          0x0000000C
#define FPGA_KEYERCTL_BIT_RX0_ENABLE         0x00000004
#define FPGA_KEYERCTL_BIT_RX1_ENABLE         0x00000008
#define FPGA_KEYERCTL_BIT_TX_RGBA2YUV        0x00000010  /* Set this bit to '1' to activate RGB->YUV conversion on TX0 (PC) channels */
#define FPGA_KEYERCTL_MSK_FGINPUT_MUX        0x00000060  /* Foreground input mux selection */
#define FPGA_KEYERCTL_BIT_FGINPUT_MUX_RX0    0x00000000  /*    00 : RX0 or coupled RX */
#define FPGA_KEYERCTL_BIT_FGINPUT_MUX_RX1    0x00000020  /*    01 : RX1 */
#define FPGA_KEYERCTL_BIT_FGINPUT_MUX_TX0    0x00000040  /*    10 : TX0 or coupled TX */
#define FPGA_KEYERCTL_BIT_FGINPUT_MUX_TX1    0x00000060  /*    11 : TX1 */
#define FPGA_KEYERCTL_MSK_BGINPUT_MUX        0x00000180  /* Background input mux selection */
#define FPGA_KEYERCTL_BIT_BGINPUT_MUX_RX0    0x00000000  /*    00 : RX0 or coupled RX */
#define FPGA_KEYERCTL_BIT_BGINPUT_MUX_RX1    0x00000080  /*    01 : RX1 */
#define FPGA_KEYERCTL_BIT_BGINPUT_MUX_TX0    0x00000100  /*    10 : TX0 or coupled TX */
#define FPGA_KEYERCTL_BIT_BGINPUT_MUX_TX1    0x00000180  /*    11 : TX1 */
#define FPGA_KEYERCTL_MSK_KEYINPUT_MUX       0x00000600  /* Key input mux selection */
#define FPGA_KEYERCTL_BIT_KEYINPUT_MUX_RX0   0x00000000  /*    00 : RX0 or coupled RX */
#define FPGA_KEYERCTL_BIT_KEYINPUT_MUX_RX1   0x00000200  /*    01 : RX1 */
#define FPGA_KEYERCTL_BIT_KEYINPUT_MUX_TX0   0x00000400  /*    10 : TX0 or coupled TX */
#define FPGA_KEYERCTL_BIT_KEYINPUT_MUX_TX1   0x00000600  /*    11 : TX1 */
#define FPGA_KEYERCTL_BIT_KEY_SHRINK         0x00000800  /* Set this bit to '1' to use key shrink */
#define FPGA_KEYERCTL_MSK_ALPHA_B1_MUX       0x00003000  /* Alpha blender B input mux selection */
#define FPGA_KEYERCTL_BIT_ALPHA_B1_MUX_RX0   0x00000000  /*    00 : RX0 or coupled RX */
#define FPGA_KEYERCTL_BIT_ALPHA_B1_MUX_RX1   0x00001000  /*    01 : RX1 */
#define FPGA_KEYERCTL_BIT_ALPHA_B1_MUX_TX0   0x00002000  /*    10 : TX0 or coupled TX */
#define FPGA_KEYERCTL_BIT_ALPHA_B1_MUX_TX1   0x00003000  /*    11 : TX1 */
#define FPGA_KEYERCTL_BIT_RX_PROGRESSIVE     0x00004000  /* Set this bit to '1' when RX are fed with progressive video */
#define FPGA_KEYERCTL_BIT_INV_KEY            0x00008000  /* Set this bit to '1' to complement the alpha blending key */
#define FPGA_KEYERCTL_BIT_HD                 0x00010000  /* Set this bit to '1' if working in HD, or '0' when working in SD */
#define FPGA_KEYERCTL_BIT_TX0_KEYOUT_EN      0x00020000  /* Set when user is going to transmit key on tx0 after RGBA to YUVK conversion */
#define FPGA_KEYERCTL_BIT_TX1_KEYOUT_EN      0x00040000  /* Set when user is going to transmit key on tx1 after RGBA to YUVK conversion */
#define FPGA_KEYERCTL_BIT_TX0_DUALLINK       0x00080000  /* Set this bit to 1 when tx0 PC stream works in dual link 4:4:4:4; YUVA or RGBA. Keep it reset for 4224 dual link format */
#define FPGA_KEYERCTL_BIT_TX0_4444_KEY       0x00100000  /* Only relevant when FPGA_KEYERCTL_BIT_TX0_KEYOUT_EN=1. Set this bit to 1 when physical tx0 must output key signal of a 4:4:4:4 stream, reset to 0 if tx0 must output key signal of a 4:2:2:4 stream */
#define FPGA_KEYERCTL_BIT_TX1_4444_KEY       0x00200000  /* Only relevant when FPGA_KEYERCTL_BIT_TX1_KEYOUT_EN=1. Set this bit to 1 when physical tx1 must output key signal of a 4:4:4:4 stream, reset to 0 if tx1 must output key signal of a 4:2:2:4 stream */
#define FPGA_KEYERCTL_MSK_TRS_MUX            0x00C00000  /* TRS source mux selection */
#define FPGA_KEYERCTL_BIT_TRS_MUX_MTG        0x00000000  /*    00 : MTG */
#define FPGA_KEYERCTL_BIT_TRS_MUX_TX0        0x00400000  /*    01 : TX0 */
#define FPGA_KEYERCTL_BIT_TRS_MUX_TX1        0x00800000  /*    10 : TX1 */
#define FPGA_KEYERCTL_BIT_RX0_BLACKOUT       0x01000000  /* Set this bit to '1' for blackout forced mode, or '0' for normal mode on RX0 */
#define FPGA_KEYERCTL_BIT_RX1_BLACKOUT       0x02000000  /* Set this bit to '1' for blackout forced mode, or '0' for normal mode on RX1 */
#define FPGA_KEYERCTL_BIT_3G                 0x04000000  /* Set this bit to '1' if working in 3G, or '0' when working in HD/SD */
#define FPGA_KEYERCTL_CHROMAKEY_RESET_B      0x80000000  /* Video Processing async Reset (reset when 0) Maintain all process in reset state */

// -- FPGA_WATCHDOG_CTRL_MM32
#define FPGA_WATCHDOG_MSK_VALUE     0x3FFFFFFF
#define FPGA_WATCHDOG_BIT_REARM     0x40000000
#define FPGA_WATCHDOG_BIT_ENABLE    0x80000000

#define FPGA_WATCHDOG_BIT_VALUE(val)   (val&FPGA_WATCHDOG_MSK_VALUE)

// -- FPGA_UART_CTRL_MM32 : UART command and data register -----------------------------------------------------------------------------/
#define FPGA_UART_CTRL_BIT_RST               0x1000000 /* Write �1� to assert reset of UART component */
#define FPGA_UART_CTRL_BIT_BAUDRATE_115200   0x0000080 /* �0� =  19200 - �1� = 115200 */


// -- FPGA_UART_STS_MM32 : UART status register -----------------------------------------------------------------------------/
#define FPGA_UART_STS_MSK_DATA               0xFF
#define FPGA_UART_STS_BIT_BAUDRATE           0x80 /* �0� =  19200 - �1� = 115200 */
#define FPGA_UART_STS_BIT_TSR_EMPTY          0x40 /* �1� = Transmit Serial Register empty */
#define FPGA_UART_STS_BIT_TDR_EMPTY          0x20 /* �1� = Transmit Data Register empty */
#define FPGA_UART_STS_BIT_PARITY             0x10 /* Parity */
#define FPGA_UART_STS_BIT_FRAMING_ERROR      0x08 /* Framing error */
#define FPGA_UART_STS_BIT_PARITY_ERROR       0x04 /* Parity error */
#define FPGA_UART_STS_BIT_OVERRUN            0x02 /* Overrun */
#define FPGA_UART_STS_BIT_RX_FULL            0x01 /* RX buffer full */

// -- FPGA_UART_DATA_MM32 : UART data register -----------------------------------------------------------------------------/
// UART command
#define ARMUART_CMD_BL_INIT      0x7F  //BootLoader Init Command
#define ARMUART_CMD_GET_CMD      0x00  //Get Command Set - Part 1
#define ARMUART_CMD_GET_VS       0x01  //Get FW Version - Part 1 
#define ARMUART_CMD_GET_ID       0x02  //Get device ID - Part 1
#define ARMUART_CMD_RD_MEM       0x11  //Read Memory - Part 1
#define ARMUART_CMD_WR_MEM       0x31  //Write Memory - Part 1
#define ARMUART_CMD_ERASE        0x43  //Erase Memory - Part 1
#define ARMUART_CMD_ER_ALL       0xFF  //Erase All Memory - Part 1

// UART return
#define ARMUART_ACK              0x79
#define ARMUART_NACK             0x1F

//MAILBOX_DATA indirect address
//Status
#define MB_CMD_STS              0x000

//Cmd rtrn
#define MB_CMD_RTRN             0x021
#define MB_CMD_PARAM            0x201

//Cmd
#define MB_CMD_TAG              0x200

// -- MB_CMD_STS : Command Status -----------------------------------------------------------------------------/
#define MB_CMD_STS_BIT_DONE                    0x00000001           /* �1� = Command done */
#define MB_CMD_STS_BIT_NOTDEFINED              0x00000002           /* �1� = Command not defined */
#define MB_CMD_STS_BIT_ERROR                   0x00000004           /* �1� = Error occurs during execution */
#define MB_CMD_STS_BIT_DATA                    0x00000008           /* �1� = Data are available (end of command which need answer) -> Data can be read at indirect address 0x21 */
#define MB_CMD_STS_BIT_WRONG_NBOF_PARAM        0x00000010           /* �1� = Number of parameters is wrong */
#define MB_CMD_STS_MSK_CMD_TAG                 0xFFFF0000           /* TAG of command concerned by status */

#define MB_CMD_STS_BIT_CMD_TAG(cmd_tag)        (((cmd_tag)<<16) & MB_CMD_STS_MSK_CMD_TAG)
#define MB_CMD_STS_GET_BIT_CMD_TAG(reg)        ((reg& MB_CMD_STS_MSK_CMD_TAG) >> 16)

//Cmd Tag
#define MB_GET_REVID               0x00000000   /* Revision ID of ARM Firmware �yymmddvv- */
#define MB_START_HDMI0             0x00010004   /* Start HDMI0 monitoring*/
#define MB_START_HDMI1             0x00010005   /* Start HDMI1 monitoring*/
#define MB_STOP_HDMI0              0x00000006   /* Stop HDMI0 monitoring*/
#define MB_STOP_HDMI1              0x00000007   /* Stop HDMI1 monitoring*/
#define MB_GET_HDMI0_STS           0x00000008   /* Get HDMI0 status*/
#define MB_GET_HDMI1_STS           0x00000009   /* Get HDMI1 status*/
#define MB_READ_HDMI0_EDID         0x0000000A   /* Read HDMI0 EDID*/
#define MB_READ_HDMI1_EDID         0x0000000B   /* Read HDMI1 EDID*/

#define MB_CMDTAG_MSK_ID           0x0000FFFF
#define MB_CMDTAG_BIT_MSK_ID(cmd_id)      ((cmd_id) & MB_CMDTAG_MSK_ID)
#define MB_CMDTAG_GET_BIT_MSK_ID(reg)     (((reg)&MB_CMDTAG_MSK_ID))

// HDMI status
#define HDMISTS_HOTPLUG_DETECTED       0x00000100
#define HDMISTS_CORRECTLY_IDENTIFIED   0x00000200


// -- FPGA_TXx_ASI_CMD_MM32 : ASI TXx command register -----------------------------------------------------------------------------/
#define FPGA_TXx_ASI_CMD_BIT_EN              0x00000001  /* Channel enable (soft reset) */
#define FPGA_TXx_ASI_CMD_MSK_FMT             0x0000000E  /* Packet Format */
#define FPGA_TXx_ASI_CMD_MSK_BRSS            0x00000700  /* Channel Bit Rate Source Select */
#define FPGA_TXx_ASI_CMD_BIT_SXBR            0x00000800  /* Synchronous external bit rate source (can forward bursts) */
#define FPGA_TXx_ASI_CMD_BIT_BSE             0x00001000  /* Automatic bit clock switch enable.
                                                            0 => keep the selected bit clock even if a bit clock failure is detected (bit TXi_BCF of Txi_STS)
                                                            1 => whenever a bit clock failure is detected, automatically switch to the internal phase accumulator reference.
                                                                 When Txi_BCF is deasserted, switch back to selected external reference. */
#define FPGA_TXx_ASI_CMD_MSK_INTSEL          0x00600000  /* Integration period selection for bit rate check */
#define FPGA_TXx_ASI_CMD_BIT_ASI             0x80000000  /* Set this bit to 1 to configure channel as ASI transmitter */


#define FPGA_TXx_ASI_CMD_BIT_FMT(fmt)           ((fmt<<1) & FPGA_TXx_ASI_CMD_MSK_FMT)
#define FPGA_TXx_ASI_CMD_GET_BIT_FMT(reg)       ((reg&FPGA_TXx_ASI_CMD_MSK_FMT) >> 1)
#define xXx_ASI_FMT_188                             0x0 /* 188 bytes per packet */
#define xXx_ASI_FMT_188_TO_204                      0x1 /* 188 bytes per packet extended to 204 */
#define xXx_ASI_FMT_204                             0x2 /* 204 bytes per packet */
#define xXx_ASI_FMT_204_TO_188                      0x3 /* 204 bytes stripped down to 188 */
#define xXx_ASI_FMT_RAW                             0x4 /* raw data mode */

#define FPGA_TXx_ASI_CMD_BIT_BRSS(brss)         ((brss<<8) & FPGA_TXx_ASI_CMD_MSK_BRSS)
#define FPGA_TXx_ASI_CMD_GET_BIT_BRSS(reg)      ((reg&FPGA_TXx_ASI_CMD_MSK_BRSS) >> 8)
#define TXx_ASI_BRSS_RX0                        0x0 /* RX Channel 0 (not yet implemented) */
#define TXx_ASI_BRSS_RX2                        0x1 /* RX Channel 2 (not yet implemented) */
#define TXx_ASI_BRSS_EXT_CLK                    0x2 /* External clock 0 (not yet implemented) */
#define TXx_ASI_BRSS_LOCAL                      0x4 /* Phase accumulator */

#define FPGA_TXx_ASI_CMD_BIT_INTSEL(int_sel)   ((int_sel<<21) & FPGA_TXx_ASI_CMD_MSK_INTSEL)
#define FPGA_TXx_ASI_CMD_GET_BIT_INTSEL(reg)   ((reg&FPGA_TXx_ASI_CMD_MSK_INTSEL) >> 21)
#define xXx_ASI_INTSEL_1                       0x0 /* 1 ms */
#define xXx_ASI_INTSEL_10                      0x1 /* 10 ms */
#define xXx_ASI_INTSEL_100                     0x2 /* 100 ms */
#define xXx_ASI_INTSEL_1000                    0x3 /* 1000 ms */


// -- FPGA_TXx_ASI_STS_MM32 : ASI TXx status register -----------------------------------------------------------------------------/
#define FPGA_TXx_ASI_STS_BIT_LFI            0x00000001  /* RX Link fault indicator */
#define FPGA_TXx_ASI_STS_BIT_RCF            0x00000002  /* Reference Clock FailSee Txi_rdev.This status is automatically cleared at the end of the 1 msec integration period, if the external reference clock is valid again. */
#define FPGA_TXx_ASI_STS_BIT_BCF            0x00000004  /* Bit clock failure */
#define FPGA_TXx_ASI_STS_BIT_UDR            0x00000008  /* TX Channel Underrun */
#define FPGA_TXx_ASI_STS_BIT_FER            0x00000010  /* Framing error */
#define FPGA_TXx_ASI_STS_BIT_PLK            0x00000020  /* RX packet lock indicator */
#define FPGA_TXx_ASI_STS_BIT_BRST           0x00000040  /* Burst mode indicator; TX: 0 */
#define FPGA_TXx_ASI_STS_BIT_RDY            0x00008000  /* TX channel ready(TBD) */



// -- FPGA_RXx_ASI_CMD_MM32 : ASI RXx command register -----------------------------------------------------------------------------/
#define FPGA_RXx_ASI_CMD_BIT_EN              0x00000001  /* Channel enable (soft reset) */
#define FPGA_RXx_ASI_CMD_MSK_FMT             0x0000000E  /* Packet Format */
#define FPGA_RXx_ASI_CMD_BIT_FLUSH           0x00000010  /* Flush channel buffers by pushing padding 0s. Channel MUST be disabled after a flush operation */
#define FPGA_RXx_ASI_CMD_BIT_TS_EN           0x00000020  /* Enable Time Stamp insertion after each valid DVB packet. Time stamp is STC + prescaler value zero-extended and packed in eight bytes. */
#define FPGA_RXx_ASI_CMD_MSK_CMS             0x00007000  /* Clocks monitor & auto-switch configuration */
#define FPGA_RXx_ASI_CMD_BIT_ESC             0x00008000  /* Error Status Clear (TBD) */
#define FPGA_RXx_ASI_CMD_BIT_PID_ADD_RST     0x00080000  /* Reset write address of PID RAM (must be cleared by software) */
#define FPGA_RXx_ASI_CMD_BIT_ASI             0x00100000  /* Set this bit to 1 to configure channel as ASI receiver */
#define FPGA_RXx_ASI_CMD_MSK_INTSEL          0x00600000  /* Integration period selection for bit rate check */
#define FPGA_RXx_ASI_CMD_BIT_PID_EN          0x00800000  /* PID filter enable */
#define FPGA_RXx_ASI_CMD_MSK_MAX_PID         0xFF000000  /* Last valid PID table entry (number of valid PIDs - 1) */


#define FPGA_RXx_ASI_CMD_BIT_FMT(fmt)           ((fmt<<1) & FPGA_RXx_ASI_CMD_MSK_FMT)
#define FPGA_RXx_ASI_CMD_GET_BIT_FMT(reg)       ((reg&FPGA_RXx_ASI_CMD_MSK_FMT) >> 1)

#define FPGA_RXx_ASI_CMD_BIT_CMS(cms)           ((cms<<12) & FPGA_RXx_ASI_CMD_MSK_CMS)
#define FPGA_RXx_ASI_CMD_GET_BIT_CMS(reg)       ((reg&FPGA_RXx_ASI_CMD_MSK_CMS) >> 12)

#define FPGA_RXx_ASI_CMD_BIT_INTSEL(int_sel)    ((int_sel<<21) & FPGA_RXx_ASI_CMD_MSK_INTSEL)
#define FPGA_RXx_ASI_CMD_GET_BIT_INTSEL(reg)    ((reg&FPGA_RXx_ASI_CMD_MSK_INTSEL) >> 21)

#define FPGA_RXx_ASI_CMD_BIT_MAX_PID(max_pid)   ((max_pid<<24) & FPGA_RXx_ASI_CMD_MSK_MAX_PID)
#define FPGA_RXx_ASI_CMD_GET_BIT_MAX_PID(reg)   ((reg&FPGA_RXx_ASI_CMD_MSK_MAX_PID) >> 24)

// -- FPGA_RXx_ASI_STS_MM32 : ASI RXx status register -----------------------------------------------------------------------------/
#define FPGA_RXx_ASI_STS_BIT_LFI            0x00000001  /* RX Link fault indicator */
#define FPGA_RXx_ASI_STS_BIT_RCF            0x00000002  /* Reference Clock Fail */
#define FPGA_RXx_ASI_STS_BIT_BCF            0x00000004  /* Bit rate failure */
#define FPGA_RXx_ASI_STS_BIT_OVR            0x00000008  /* RX Channel Overrun */
#define FPGA_RXx_ASI_STS_BIT_FER            0x00000010  /* Framing error */
#define FPGA_RXx_ASI_STS_BIT_FER_LTCH       0x00000020  /* Framing error latched. (updated for each new buffer) */
#define FPGA_RXx_ASI_STS_BIT_BRST           0x00000040  /* Burst mode indicator*/
#define FPGA_RXx_ASI_STS_BIT_RDY            0x00008000  /* RX channel ready(TBD) */
#define FPGA_RXx_ASI_STS_MSK_FMT_CTRL       0x00030000  /* Format control. The format control information is significant when there is no framing error only. */


#define FPGA_RXx_ASI_STS_BIT_FMT_CTRL(fmt_ctrl)   ((fmt_ctrl<<16) & FPGA_RXx_ASI_STS_MSK_FMT_CTRL)
#define FPGA_RXx_ASI_STS_GET_BIT_FMT_CTRL(reg)    ((reg&FPGA_RXx_ASI_STS_MSK_FMT_CTRL) >> 16)
#define ASI_FMTCTRL_OK                            0x0 /* Received stream has the expected format */
#define ASI_FMTCTRL_188                           0x1 /* Expected packet length is 204 and received packets are 188 byte long */
#define ASI_FMTCTRL_204                           0x2 /* Expected packet length is 188 and received packets are 204 byte long */


#define HWPROCESSING_CK_OUTPUT_FOREGROUND             0x00  ///< Specifies foreground as output of CK module
#define HWPROCESSING_CK_OUTPUT_BACKGROUND             0x01  ///< Specifies background as output of CK module

#define HWPROCESSING_PARAM_CK_ENABLE                  0x00
#define HWPROCESSING_PARAM_CK_MAIN_OUT_SELECT         0x10
#define HWPROCESSING_PARAM_CK_MONITOR_OUT_SELECT      0x15

#define HWPROCESSING_PARAM_Y_MINMAX_CLIP              0x17
#define HWPROCESSING_PARAM_C_MINMAX_CLIP              0x18

#define HWPROCESSING_PARAM_KCOMPRESSOR                0x19
#define HWPROCESSING_PARAM_ALPHA_TBAR                 0x1A
#define HWPROCESSING_PARAM_BLENDER_MIN_CLIP           0x1B
#define HWPROCESSING_PARAM_BLENDER_MAX_CLIP           0x1C
#define HWPROCESSING_PARAM_BLENDER_ONE_OVER_MAX_CLIP  0x1D
#define HWPROCESSING_PARAM_BLENDER_MODE               0x1E
#define HWPROCESSING_PARAM_OUTPUT_SELECTION           0x1F


// -- HWPROCESSING_PARAM_BLENDER_MODE
#define HWPROCESSING_PARAM_BLENDER_MODE_BIT_ADDITIVE     0x1   //0 : Alpha blender in multiplicative mode (default mode) - 1 : Alpha blender in additive mode
#define HWPROCESSING_PARAM_BLENDER_MODE_BIT_ALPHA_ON_Y   0x2   //0 : Alpha channel is on alpha path - 1 : Alpha channel is in luminance path

// -- HWPROCESSING_PARAM_OUTPUT_SELECTION
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_MASK                0x07
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_BACK                0x00
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_RX0                 0x01
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_RX1                 0x02
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_CK_MAIN             0x03
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_CK_BLENDED_MAIN     0x04
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_TX0                 0x05
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_TX1                 0x06

#define HWPROCESSING_OUTPUT_ANC_SOURCE_MASK                  0x07
#define HWPROCESSING_OUTPUT_ANC_SOURCE_BACK                  0x00
#define HWPROCESSING_OUTPUT_ANC_SOURCE_RX0                   0x01
#define HWPROCESSING_OUTPUT_ANC_SOURCE_RX1                   0x02
#define HWPROCESSING_OUTPUT_ANC_SOURCE_TX0                   0x03
#define HWPROCESSING_OUTPUT_ANC_SOURCE_TX1                   0x04

#define HWPROCESSING_OUTPUT_ANC_PRIORITY_MASK                0x03
#define HWPROCESSING_OUTPUT_ANC_PRIORITY_NONE                0x00
#define HWPROCESSING_OUTPUT_ANC_PRIORITY_TX0                 0x01
#define HWPROCESSING_OUTPUT_ANC_PRIORITY_TX1                 0x02
#define HWPROCESSING_OUTPUT_ANC_PRIORITY_TX0_TX1             0x03

#define HWPROCESSING_OUTPUT_SELECTION_BIT_VIDEO_TXi(idx,source)  ((source&0x07)<<(8*idx))
#define HWPROCESSING_OUTPUT_SELECTION_BIT_ANC_TXi(idx,source)    ((source&0x07)<<(3+(8*idx)))
#define HWPROCESSING_OUTPUT_PRIORITY_BIT_ANC_TXi(idx,source)     ((source&0x03)<<(6+(8*idx)))



#endif // _HDDRV_REGISTERSMAP_H_
