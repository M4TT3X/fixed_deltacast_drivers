/********************************************************************************************************************//**
 * @file   BufMngrTypes.h
 * @date   2008/02/12
 * @Author cs
 * @brief  
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2008/02/12  v00.01.0000    cs       Creation of this file
   2009/06/03  v00.02.0000    cs       The UserData_UL field of the BUFFER_QUEUE_OPEN_PARAM structure is now bit-split in
                                       three parts, to be able to specify inside of the same 32-bit value, a 16-bit user data,
                                       a 8-bit DMA queue index and a 8-bit DMA channel index.


 **********************************************************************************************************************/

#ifndef _BUFMNGRTYPES_H_
#define _BUFMNGRTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"
#include "EventMngrTypes.h"
#include "DMABufMngrTypes.h"


/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define BUFFER_TYPE_NOT_USED           0x00000000
#define BUFFER_TYPE_MASK               0x1F000000
#define BUFFER_TYPE_RX                 0x01000000
#define BUFFER_TYPE_TX                 0x02000000
#define BUFFER_TYPE_LINKED_FLAG        0x10000000
#define BUFFER_TYPE_RX_LINKED          (BUFFER_TYPE_LINKED_FLAG|BUFFER_TYPE_RX)
#define BUFFER_TYPE_TX_LINKED          (BUFFER_TYPE_LINKED_FLAG|BUFFER_TYPE_TX)
#define BUFFER_TYPE_FEATURE_MASK       0x00FF0000
#define BUFFER_TYPE_FEATURE_EARLY_TRANSFERT  0x00800000


#define BUFFER_LOCK_TYPE_MASK                   0x00000000FF

typedef enum
{
   BUFFER_LOCK_TYPE_NEXT   =                0x01,  ///< Returns the buffer with the smaller count, but greater than the given buffer count 
   BUFFER_LOCK_TYPE_LAST   =                0x02,  ///< Returns the buffer with the highest buffer count (potentially smaller are discarded)
   BUFFER_LOCK_TYPE_INCOMING  =             0x03,
   BUFFER_LOCK_TYPE_SPECIFIED  =            0x04,
   BUFFER_LOCK_TYPE_OLDEST    =             0x05,  ///< Returns the buffer with the smallest buffer count.
} BUFFER_LOCK_TYPE;


#define INVALID_BUFHANDLE             0xFFFFFFFF
#define INVALID_BUFQUEUEHANDLE       0xFFFFFFFF
#define INVALID_BUFQUEUEGRPID          0xFFFFFFFF ///< Invalid GroupID



#define BUFFER_HEADER_SIGN                      0x4D415246

/** @addtogroup BUFFER_QUEUE_RESET_PARAM_GROUP Available bitmap values for the Buffer queue reset parameter.
The Buffer Queue Reset feature needs a parameter to selected which part has to be reset. */
//! @{
#define BUFFER_QUEUE_RESET_PARAM_BUFFER_COUNTER       0x00000001  // Reset the buffer queue counter
#define BUFFER_QUEUE_RESET_PARAM_ERROR_COUNTER        0x00000002  // Reset the buffer queue error counter
#define BUFFER_QUEUE_RESET_PARAM_ONBOARD_BUF_IDX      0x00000004  // Reset the on-board buffer index. To use when a channel is stop without buffer queue destroying
#define BUFFER_QUEUE_RESET_PARAM_BUF_LOCK_STS         0x00000008  // Reset all lock status
#define BUFFER_QUEUE_RESET_PARAM_BUF_NUMBER           0x00000010  
#define BUFFER_QUEUE_RESET_PARAM_USER                 0x00000020  // reset the buffer queue user param
//! @}

#define BUFFER_QUEUE_ERROR_TYPE_NOTAVAILABLE          0
#define BUFFER_QUEUE_ERROR_TYPE_DISCARDED_RXBUF       1
#define BUFFER_QUEUE_ERROR_TYPE_BANDWIDTH             2
#define BUFFER_QUEUE_ERROR_TYPE_CORRUPTED_RXBUF       3

#define BUFFER_QUEUE_USER_DATA_TYPE_NB_CRC_ERRORS     0

#define MAX_BUFFER_QUEUE_DATA_TYPES     28    ///< Specifies the max number of different data kind that a buffer queue is able to handle.
#define MAX_BUFFER_QUEUE_ERROR_TYPE     8     ///< Specifies the max number of different errors attached to a buffer queue.
#define MAX_BUFFER_QUEUE_USER_DATA_TYPE 8     ///< Specifies the max number of different user data attached to a buffer queue.
#define MAX_EVENTID_PER_BUFFER_QUEUE    4     ///< Specifies the maximum number of Driver event able to be bond to a buffer queue.
#define MAX_BUFFER_HEADER_USER_DATA     128   ///< Specifies the among of ULONG values, reserved in the buffer header of user specific purpose.

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/**********************************************************************************************************************//**
 * @struct BUFFER_USER_LOCK_PARAM
 * @brief  This structure contains all needed parameter for IOCTL_BUFFER_LOCK I/O Control. 
 *************************************************************************************************************************/
#pragma pack(push, 4)

typedef struct
{
   ULONG StructSize_UL;
   ULONG BufQueueHandle_UL;   ///< Specifies the buffer queue handle given by the Buffer Queue Open operation.
   ULONG Count_UL;            ///< Gives the actual count needed for several lock types.
   BUFFER_LOCK_TYPE LockType_UL;         ///< Specifies the lock type
   ULONG UserBitMapID_UL;     ///< Gives a user identification for lock operation.
} BUFFER_USER_LOCK_PARAM;

typedef BUFFER_USER_LOCK_PARAM IOCTL_BUFFER_USER_LOCK_PARAM;

typedef ULONG BUFHANDLE;
typedef ULONG BUFQUEUEHANDLE;

#define BUFHANDLE_Create(BufQueueID, BufIdx) (((BufQueueID)<<24) | ((BufIdx)&0xFFFFFF))
#define BUFHANDLE_GetBufQueue(BufHandle)     ((BufHandle)>>24)
#define BUFHANDLE_GetBufIdx(BufHandle)       ((BufHandle)&0xFFFFFF)


typedef struct
{
   ULONG     StructSize_UL;
   BUFHANDLE BufHandle_UL;            ///< Specifies the buffer handle to unlock (this value is returned by a lock operation 
   ULONG     UserBitMapID_UL;         ///< Gives a user identification for unlock operation.
} BUFFER_USER_UNLOCK_PARAM;

typedef BUFFER_USER_UNLOCK_PARAM IOCTL_BUFFER_USER_UNLOCK_PARAM;

typedef struct
{
   ULONG StructSize_UL;
   ULONG RxQueueHandle_UL;
   ULONG TxQueueHandle_UL;
} BUFFER_QUEUE_LINK_PARAM;

typedef BUFFER_QUEUE_LINK_PARAM IOCTL_BUFFER_QUEUE_LINK_PARAM;

#pragma pack(pop)

typedef struct
{
   ULONG StructSize_UL;
   ULONG BufQueueHandle_UL;
   ULONG ResetParameter_UL;
} BUFFER_QUEUE_RESET_PARAM;




#define MAX_BUFFER_QUEUE_USER_PARAM                   296

#pragma pack(push, 4)

typedef struct _BUFFER_QUEUE_OPEN_PARAM
{
   ULONG    StructSize_UL;
   ULONG    Type_UL;
   UBYTE    DMAQueue_UB;                                    ///< Specifies the DMA queue to use
   UBYTE    DMAChannel_UB;                                  ///< Specifies the DMA channel to use
   EVENT_ID pEventID_e[MAX_EVENTID_PER_BUFFER_QUEUE];       ///< List of Event ID associated to the buffer queue
   ULONG    IntSource_UL;                                   ///< Associated interrupt source
   ULONG    GroupID_UL;                                     ///< Attach a buffer queue to a group id.
   BOOL32     Disabled_B;                                     ///< TRUE to disable the buffer queue. The buffer queue will not be parsed by the BufMngr_TransfertHandler
   ULONG    pDataSize_UL[MAX_BUFFER_QUEUE_DATA_TYPES];      ///< Specifies sizes for data transfers
   ULONG    pDataSubBufID_UL[MAX_BUFFER_QUEUE_DATA_TYPES];  ///< Specifies sizes for data transfers
   ULONG    pDataOffset_UL[MAX_BUFFER_QUEUE_DATA_TYPES];    ///< Specifies sizes for data transfers
   ULONG    pUserParam_UL[MAX_BUFFER_QUEUE_USER_PARAM];
   ULONG    LBDataSize_UL;
   
} BUFFER_QUEUE_OPEN_PARAM;

typedef BUFFER_QUEUE_OPEN_PARAM IOCTL_BUFFER_QUEUE_OPEN_PARAM;

#pragma pack(pop)


typedef enum
{
   BUFFER_QUEUE_UPDATE_TYPE_SET_USER_PARAM,
   BUFFER_QUEUE_UPDATE_TYPE_DISABLED_PARAM,

} BUFFER_QUEUE_UPDATE_TYPE;

#pragma pack(push, 4)

typedef struct _BUFFER_QUEUE_UPDATE_PARAM
{
   ULONG    StructSize_UL;
   ULONG    BufQueueHandle_UL;
   BUFFER_QUEUE_UPDATE_TYPE Type_e;
   union 
   {
      struct 
      {
         ULONG UserParamIdx_UL;
         ULONG Value_UL;
      } BufQueueUpdate_SetUserParam_X;
      BOOL32 Disabled_B;
      BUFFER_QUEUE_OPEN_PARAM BufQueueUpdate_OpenParam_X;
   };
} BUFFER_QUEUE_UPDATE_PARAM;

typedef BUFFER_QUEUE_UPDATE_PARAM IOCTL_BUFFER_QUEUE_UPDATE_PARAM;

typedef struct _BUFFER_QUEUE_ADD_BUF_PARAM
{
   ULONG StructSize_UL;
   ULONG BufQueueHandle_UL;
   BUFID BufIdx_UL;
} BUFFER_QUEUE_ADD_BUF_PARAM;

typedef BUFFER_QUEUE_ADD_BUF_PARAM IOCTL_BUFFER_QUEUE_ADD_BUF_PARAM;

typedef struct  
{
   ULONG StructSize_UL;
   ULONG NbBufInQueue_UL;
   ULONG NbBufLockedByUser_UL;
   ULONG NbBufLockedBySystem_UL;
   ULONG NbBufFilledByData_UL;
   ULONG pErrorCount_UL[MAX_BUFFER_QUEUE_ERROR_TYPE];
   ULONG pUserData_UL[MAX_BUFFER_QUEUE_USER_DATA_TYPE];
} BUFFER_QUEUE_STATUS;

typedef BUFFER_QUEUE_STATUS IOCTL_BUFFER_QUEUE_STATUS;

#pragma pack(pop)

/**********************************************************************************************************************//**
 * @struct BUFFER_HEADER
 * @brief This structure, added in the beginning of each data buffer.
 *       
 * It contains information about the data location inside the buffer and more information like size, buffer queue status, ...
 *************************************************************************************************************************/

typedef struct _BUFFER_HEADER
{
   ULONG Sign_UL;                                        ///< Signature of the buffer header (must be equal to BUFFER_HEADER_SIGN symbol)
   ULONG BufferNumber_UL;                                ///< Number of the buffer 
   ULONG pDataSize_UL[MAX_BUFFER_QUEUE_DATA_TYPES];      ///< Data sizes for the different kinds of data
   ULONG pDataSubBufID_UL[MAX_BUFFER_QUEUE_DATA_TYPES];  ///< Data offsets for the different kinds of data
   ULONG pDataOffset_UL[MAX_BUFFER_QUEUE_DATA_TYPES];    ///< Data offsets for the different kinds of data
   ULONG Status_UL;                                      ///< Status
   ULONG Reserved_UL;                                    ///< Reserved
   LONGLONG SystemTime_LL;                               ///< System Time value associated with the buffer (RX-only)
   ULONG pUserData_UL[MAX_BUFFER_HEADER_USER_DATA];      ///< Status of the buffer queue 
   ULONG pReserved_UL[1024 - (7 + MAX_BUFFER_QUEUE_DATA_TYPES+MAX_BUFFER_QUEUE_DATA_TYPES+MAX_BUFFER_QUEUE_DATA_TYPES+MAX_BUFFER_HEADER_USER_DATA)]; ///< Padding to have 4K alignement
   ULONG LBDataSize_UL;
   

   
   
} BUFFER_HEADER;



#endif // _BUFMNGRTYPES_H_
