/********************************************************************************************************************//**
 * @internal
 * @file   	StreamProperties.h
 * @date   	2010/08/20
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/20  v00.01.0000    cs       Creation of this file
   2011/01/26  v00.01.0001    cs       Add STREMPROP_DRVFEATURES_DMA_FULLDUPLEX_SUPPORT symbol

 **********************************************************************************************************************/

#ifndef _STREAMPROPERTIES_H_
#define _STREAMPROPERTIES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "SkelProperties.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define STREAMPROP_DRVFEATURES_DMA_SCATTERGATHER_SUPPORT   0x000000001 ///< Indicates that the driver support the scatter/gather DMA transfer mode
#define STREAMPROP_DRVFEATURES_DMA_FULLDUPLEX_SUPPORT      0x000000002 ///< Enables the support for full-duplex DMA

#define RX_CHN_STATUS_OVERRUN            0x00000001

#define TX_CHN_STATUS_UNDERRUN           0x00000001

#define TX_CHN_STATE_BIT_RXMTX                        0x00000001    // Bit='1' if the TX channel is linked to an RX channel by RXmTX
#define TX_CHN_STATE_BIT_FIELD_MODE                   0x00000002
#define TX_CHN_STATE_MASK_NBOF_ONBOARD_BUFFER         0x00000F00
#define TX_CHN_STATE_BIT_PREVIOUS_PARITY_WAS_EVEN     0x00001000

#define TX_CHN_STATE_BIT_NBOF_ONBOARD_BUFFER(nbof_onboard_buffer) ((nbof_onboard_buffer<<8)&TX_CHN_STATE_MASK_NBOF_ONBOARD_BUFFER)
#define TX_CHN_STATE_GET_BIT_NBOF_ONBOARD_BUFFER(prop_val)        ((prop_val&TX_CHN_STATE_MASK_NBOF_ONBOARD_BUFFER)>>8)

typedef enum 
{
   CHANNEL_TYPE_UNKNOWN=0,
   CHANNEL_TYPE_SDI,
   CHANNEL_TYPE_HDSDI,
   CHANNEL_TYPE_DVI,
   CHANNEL_TYPE_ASI,
   CHANNEL_TYPE_HDSDI_3G,
   CHANNEL_TYPE_HDMI
} CHANNEL_TYPE;

typedef enum
{
   CHANNEL_STATE_NOT_USED=0,
   CHANNEL_STATE_IDLE,
   CHANNEL_STATE_OPENED,
   CHANNEL_STATE_STARTED
} CHANNEL_STATE;

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/*********************************************************************************************************************//**
* @enum  STREAM_BOARD_STATE
* @brief This enumeration gives the state of an opened board.
*
* According to the returned state, the board :
* - can be Ready to use
* - need a firmware update
* - is in SAFE mode and need a firmware update too.
*************************************************************************************************************************/
typedef enum 
{
   STREAM_BOARD_STATE_READY,             ///< This state corresponds to a "ready-to-use" state
   STREAM_BOARD_STATE_STATE_NEED_UPDATE, ///< The board is present, but its firmware is not up-to-date
   STREAM_BOARD_STATE_FAIL_SAFE          ///< The board has booted with its Fail-Safe firmware
} STREAM_BOARD_STATE;


typedef enum 
{
   STREAM_BUS_TYPE_UNKNOWN,
   STREAM_BUS_TYPE_PCI,
   STREAM_BUS_TYPE_PCI64,
   STREAM_BUS_TYPE_PCIe1x,
   STREAM_BUS_TYPE_PCIe2x,
   STREAM_BUS_TYPE_PCIe4x,   
   STREAM_BUS_TYPE_PCIeGen2_1x,
   STREAM_BUS_TYPE_PCIeGen2_2x,
   STREAM_BUS_TYPE_PCIeGen2_4x,   
   STREAM_BUS_TYPE_PCIeGen2_8x,
   STREAM_BUS_TYPE_PCIe8x
}STREAM_BUS_TYPE;

enum STREAM_PROPERTIES
{
   STREAM_FIRST_PROPERTY=NB_SKEL_PROPERTIES,
   STREAM_PROPERTIES_STREAMFEATURES=STREAM_FIRST_PROPERTY,
   STREAM_PROPERTIES_VALIDATE_ACCESS_CODE,
   STREAM_PROPERTIES_BRDLAYOUT_BOARD_STATE,
   STREAM_PROPERTIES_BRDLAYOUT_BUS_TYPE,

   STREAM_PROPERTIES_BRDLAYOUT_NB_RX_CHN,
   STREAM_PROPERTIES_BRDLAYOUT_NB_TX_CHN,

   STREAM_PROPERTIES_BRDLAYOUT_RX0_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_RX1_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_RX2_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_RX3_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_RX4_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_RX5_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_RX6_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_RX7_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_TX0_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_TX1_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_TX2_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_TX3_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_TX4_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_TX5_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_TX6_CHN_TYPE,
   STREAM_PROPERTIES_BRDLAYOUT_TX7_CHN_TYPE,

   STREAM_PROPERTIES_TX0_CHN_STATUS,
   STREAM_PROPERTIES_TX1_CHN_STATUS,
   STREAM_PROPERTIES_TX2_CHN_STATUS,
   STREAM_PROPERTIES_TX3_CHN_STATUS,
   STREAM_PROPERTIES_TX4_CHN_STATUS,
   STREAM_PROPERTIES_TX5_CHN_STATUS,
   STREAM_PROPERTIES_TX6_CHN_STATUS,
   STREAM_PROPERTIES_TX7_CHN_STATUS,

   STREAM_PROPERTIES_TX0_CHN_STATE,
   STREAM_PROPERTIES_TX1_CHN_STATE,
   STREAM_PROPERTIES_TX2_CHN_STATE,
   STREAM_PROPERTIES_TX3_CHN_STATE, 
   STREAM_PROPERTIES_TX4_CHN_STATE,
   STREAM_PROPERTIES_TX5_CHN_STATE,
   STREAM_PROPERTIES_TX6_CHN_STATE,
   STREAM_PROPERTIES_TX7_CHN_STATE, 

   STREAM_PROPERTIES_TX0_CHN_UNDERRUN_CNT,
   STREAM_PROPERTIES_TX1_CHN_UNDERRUN_CNT,
   STREAM_PROPERTIES_TX2_CHN_UNDERRUN_CNT,
   STREAM_PROPERTIES_TX3_CHN_UNDERRUN_CNT, 
   STREAM_PROPERTIES_TX4_CHN_UNDERRUN_CNT,
   STREAM_PROPERTIES_TX5_CHN_UNDERRUN_CNT,
   STREAM_PROPERTIES_TX6_CHN_UNDERRUN_CNT,
   STREAM_PROPERTIES_TX7_CHN_UNDERRUN_CNT, 

   STREAM_PROPERTIES_RX0_CHN_STATUS,
   STREAM_PROPERTIES_RX1_CHN_STATUS,
   STREAM_PROPERTIES_RX2_CHN_STATUS,
   STREAM_PROPERTIES_RX3_CHN_STATUS,
   STREAM_PROPERTIES_RX4_CHN_STATUS,
   STREAM_PROPERTIES_RX5_CHN_STATUS,
   STREAM_PROPERTIES_RX6_CHN_STATUS,
   STREAM_PROPERTIES_RX7_CHN_STATUS,

   STREAM_PROPERTIES_RX0_CHN_OVERRUN_CNT,
   STREAM_PROPERTIES_RX1_CHN_OVERRUN_CNT,
   STREAM_PROPERTIES_RX2_CHN_OVERRUN_CNT,
   STREAM_PROPERTIES_RX3_CHN_OVERRUN_CNT, 
   STREAM_PROPERTIES_RX4_CHN_OVERRUN_CNT,
   STREAM_PROPERTIES_RX5_CHN_OVERRUN_CNT,
   STREAM_PROPERTIES_RX6_CHN_OVERRUN_CNT,
   STREAM_PROPERTIES_RX7_CHN_OVERRUN_CNT, 

   STREAM_PROPERTIES_TX0_CHN_ONBOARD_FILLED_BUFFER_CNT,
   STREAM_PROPERTIES_TX1_CHN_ONBOARD_FILLED_BUFFER_CNT,
   STREAM_PROPERTIES_TX2_CHN_ONBOARD_FILLED_BUFFER_CNT,
   STREAM_PROPERTIES_TX3_CHN_ONBOARD_FILLED_BUFFER_CNT,
   STREAM_PROPERTIES_TX4_CHN_ONBOARD_FILLED_BUFFER_CNT,
   STREAM_PROPERTIES_TX5_CHN_ONBOARD_FILLED_BUFFER_CNT,
   STREAM_PROPERTIES_TX6_CHN_ONBOARD_FILLED_BUFFER_CNT,
   STREAM_PROPERTIES_TX7_CHN_ONBOARD_FILLED_BUFFER_CNT,

   STREAM_PROPERTIES_RX0_CHN_STATE,
   STREAM_PROPERTIES_RX1_CHN_STATE,
   STREAM_PROPERTIES_RX2_CHN_STATE,
   STREAM_PROPERTIES_RX3_CHN_STATE,
   STREAM_PROPERTIES_RX4_CHN_STATE,
   STREAM_PROPERTIES_RX5_CHN_STATE,
   STREAM_PROPERTIES_RX6_CHN_STATE,
   STREAM_PROPERTIES_RX7_CHN_STATE,

   STREAM_PROPERTIES_BRD_IDX,
  

   NB_STREAM_PROPERTIES=STREAM_FIRST_PROPERTY+128   //Keep 128 free space for future stream properties
};


#endif // _STREAMPROPERTIES_H_
