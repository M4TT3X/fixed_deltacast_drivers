/********************************************************************************************************************//**
 * @file   	FPGA_DMAController.h
 * @date   	2011/01/28
 * @author 	cs
 * @version v00.01.0000 
 * @brief   This file describes the behavior of the deltacast DMA controller on the FPGA side.
 *
 * Some operations are required to perform either BLOCK MODE transfer or SCATTER/GATHER transfer. The scatter/gather
 * transfer works with a list of pages (physical addresses) given to the controller to achieve a tranfser to/from a 
 * spreaded in the physical memory.
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2011/01/28  v00.01.0000    cs       Creation of this file
   2011/04/20  v00.02.0000    cs       Fix a multiple boards issue due to a stupid global variable shared between boards
   2011/11/04  v00.02.1000    jj       Removed the typedef from the first enumeration (DMACTRL_DMA0_A_REG, ...), a
                                       typedef requires a name.

 **********************************************************************************************************************/

#ifndef _FPGA_DMACONTROLLER_H_
#define _FPGA_DMACONTROLLER_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "PCIeBridge.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

enum
{
   DMACTRL_DMA0_A_REG,
   DMACTRL_DMA0_BC_REG,
   DMACTRL_DMA1_A_REG,
   DMACTRL_DMA1_BC_REG,
   DMACTRL_IMR_REG,
   NB_DMACTRL_REGISTERS
};

typedef struct  
{
   BOOL32  FullDuplexSupport_B;
   BOOL32  ScatterGatherSupport_B;
   PMM32_REGISTER pDMARegisterList_MM32[NB_DMACTRL_REGISTERS];
   ULONG pSGDescrTargetLBAddr_UL[2];

} FPGA_DMACTRL_PARAM;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

BOOL32 FPGADMACtrl_DMAMngrDMAStart(PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL, TRANSLATED_DMA_REQUEST * pRequest_X);
BOOL32 FPGADMACtrl_DMAAddRequest( PDEVICE_EXTENSION pdx,ULONG *pDMAChannel_UL, DMA_REQUEST *pDMARequ_X );
void FPGADMACtrl_Init( PDEVICE_EXTENSION pdx,FPGA_DMACTRL_PARAM * pParam_X );

#endif // _FPGA_DMACONTROLLER_H_
