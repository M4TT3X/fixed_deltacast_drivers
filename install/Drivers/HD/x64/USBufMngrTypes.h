/********************************************************************************************************************//**
 * @internal
 * @file   	USBufMngrTypes.h
 * @date   	2010/07/01
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/01  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _USBUFMNGRTYPES_H_
#define _USBUFMNGRTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum 
{
   IOCTL_USBUF_ACCESS_TYPE_REGISTER,   ///< register a userspace buffer as DMA buffer.
   IOCTL_USBUF_ACCESS_TYPE_UNREGISTER
} IOCTL_USBUF_ACCESS_TYPE;


/**********************************************************************************************************************//**
 * @struct IOCTL_USBUF_ACCESS_PARAM
 * @brief  This structure is attached to the IOCTL_USBUF_ACCESS IOCTL command.
 *
 * @remark All fields are not necessary relevant, depending on the access type.
 *************************************************************************************************************************/

#pragma pack(push, 4)

typedef struct
{
   ULONG StructSize_UL;                   ///< Size of the IOCTL_USBUF_ACCESS_PARAM structure
   IOCTL_USBUF_ACCESS_TYPE AccessType_E;  ///< Access type 
   DELTA_VOID pUSBufBaseAddress_v;            ///< Pointer to a userspace buffer
   ULONG  USBufSize_UL;                   ///< Userspace buffer size (in bytes)
   ULONG  USBufID_UL;                     ///< BufferID
   ULONG  UserAddrOffset_UL;              ///< Defines an offset within the buffer to point to start of the physical page aligned area
} IOCTL_USBUF_ACCESS_PARAM;

#pragma pack(pop)

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _USBUFMNGRTYPES_H_
