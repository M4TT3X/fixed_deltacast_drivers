/**********************************************************************************************************************
 
   Module   : LinuxDrvHlp
   File     : LinuxDrvHlp.h
   Created  : 2006/11/22
   Author   : cs

   Description : 

   
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2006/11/22  v00.01.0000    cs       Creation of this file


 **********************************************************************************************************************/

#ifndef _LinuxDrvHlp_H_
#define _LinuxDrvHlp_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"
#include "Driver.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/


int LinuxDrvHlp_MMap(PDEVICE_EXTENSION pdx, struct vm_area_struct *vma, LONGLONG PhysicalAddr_LL, ULONG Size_UL);
int LinuxDrvHlp_MMap_PCIResource(PDEVICE_EXTENSION pdx,struct vm_area_struct *vma, UBYTE PCIBar_UB );
UCHAR IO_Read8(ULONG Offset_UL);
USHORT IO_Read16(ULONG Offset_UL);
ULONG IO_Read32(ULONG Offset_UL);
void IO_Write8(ULONG Offset_UL, ULONG Value_UL);
void IO_Write16(ULONG Offset_UL, ULONG Value_UL);
void IO_Write32(ULONG Offset_UL, ULONG Value_UL);


#endif // _LinuxDrvHlp_H_
