#ifndef _CBOARDUPGRADE_H_
#define _CBOARDUPGRADE_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "IBoardUpgrade.h"
#include "CDVIDrvIF.h"

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

using namespace CDVIDrvIF_NS;

class CDVIBoardUpgrade : public CDVIDrvIF, public IBoardUpgrade
{
public:
   
   CDVIBoardUpgrade(ULONG BoardIdx_UL);
   ~CDVIBoardUpgrade(){};
   
   char mpBoardName_c[64];
   
   virtual ULONG GetNbUpgradableStuff();
   virtual CDRVIF_STATUS GetUpgradeInfo(ULONG StuffIndex_UL, ULONG* pCurrentFirmware_UL, ULONG* pRequestedFirmware_UL);
   virtual CDRVIF_STATUS UpgradeFirmware(ULONG StuffIndex_UL, INotifyProgressFunctor *pSignalProgress_fct);
   virtual char * GetBoardName();
};



#endif