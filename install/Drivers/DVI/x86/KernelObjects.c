/**********************************************************************************************************************
 
   Module   : KernelObjects
   File     : KernelObjects.cpp
   Created  : 2006/10/19
   Author   : cs

   Remark   : This is the Linux version of the implementation of the Portable Kernel Objects.

 **********************************************************************************************************************/

/***** INCLUDES *******************************************************************************************************/

#define MODID MID_SYSDRV

#include "SkelDrv.h"
#include "KernelObjects.h"
#include "DrvDbg.h"
#include "InterlockedOp.h"
#include "OSDrvData.h"
#ifdef __linux__
#include <linux/vmalloc.h>
#include <linux/semaphore.h>
#endif


/***** EXTERNAL VARIABLES *********************************************************************************************/

/***** GLOBAL VARIABLES ***********************************************************************************************/

ULONG FindFreeIndex(ULONG *pFlags_UL, ULONG Nb_UL)
{
ULONG Index_UL = 0xFFFFFFFF;
   ULONG i;

   for (i=0; i<Nb_UL; i++)
   {
      if (!pFlags_UL[i])
      {
         pFlags_UL[i]=1;
         Index_UL = i;
         break;
      }
   }

   return Index_UL;
}

#ifdef CONFIG_PREEMPT_RT
raw_spinlock_t * RequestSpinlockFromPool(PDEVICE_EXTENSION pdx)
{
	raw_spinlock_t * pSpinlock=NULL;
#else
spinlock_t * RequestSpinlockFromPool(PDEVICE_EXTENSION pdx)
{
	spinlock_t * pSpinlock=NULL;
#endif

ULONG Idx_UL;

   Idx_UL=FindFreeIndex(pdx->pOsDrvData_X->pSpinlockFlags_UL,SPINLOCK_POOL_SIZE);

   if (Idx_UL!=0xFFFFFFFF)
   {
      pSpinlock=&pdx->pOsDrvData_X->pSpinlockPool[Idx_UL];
      DbgOutput("Spinlock:%08p\n",pSpinlock);
   }
   else ErrOutput("Unable to find a free spinlock\n");

   return pSpinlock;
}

struct semaphore  * RequestSemaphoreFromPool(PDEVICE_EXTENSION pdx)
{
   struct semaphore  * pSemaphore=NULL;
   ULONG Idx_UL;

   Idx_UL=FindFreeIndex(pdx->pOsDrvData_X->pMutexFlags_UL,MUTEX_POOL_SIZE);

   if (Idx_UL!=0xFFFFFFFF)
   {
      pSemaphore=&pdx->pOsDrvData_X->pSemaphorePool[Idx_UL];
      DbgOutput("Semaphore:%08p\n",pSemaphore);
   }
   else ErrOutput("Unable to find a free semaphore\n");

   return pSemaphore;
}

wait_queue_head_t * RequestEventFromPool(PDEVICE_EXTENSION pdx)
{
   wait_queue_head_t * pEvent=NULL;
   ULONG Idx_UL;


   InfoOutput("Requesting event\n");
   Idx_UL=FindFreeIndex(pdx->pOsDrvData_X->pEventFlags_UL,EVENT_POOL_SIZE);

   if (Idx_UL!=0xFFFFFFFF)
   {
      pEvent=&pdx->pOsDrvData_X->pEventPool[Idx_UL];
   }
   else ErrOutput("Unable to find a free event\n");

   return pEvent;
}



ULONG GL_SpinlockID_UL=0x8000;

void KOBJ_InitializeSpinLock(PDEVICE_EXTENSION pdx, KOBJ_SPINLOCK * pSpinLock_KO )
{
   if (pSpinLock_KO) 
   {
      pSpinLock_KO->pSpinlock_X = RequestSpinlockFromPool(pdx);
#ifdef CONFIG_PREEMPT_RT
      if (pSpinLock_KO->pSpinlock_X) raw_spin_lock_init(pSpinLock_KO->pSpinlock_X);
#else
		if (pSpinLock_KO->pSpinlock_X) spin_lock_init(pSpinLock_KO->pSpinlock_X);
#endif
   }
}

void KOBJ_AcquireSpinLock(PDEVICE_EXTENSION pdx, KOBJ_SPINLOCK * pSpinLock_KO )
{
   if ((!pSpinLock_KO) || (!pSpinLock_KO->pSpinlock_X)) {ErrOutput("Invalid Spinlock (%08p/%08p)\n",pSpinLock_KO,pSpinLock_KO->pSpinlock_X);return;}
   spin_lock_irqsave(pSpinLock_KO->pSpinlock_X, pSpinLock_KO->Flags_UL);
}

void KOBJ_ReleaseSpinLock(PDEVICE_EXTENSION pdx, KOBJ_SPINLOCK * pSpinLock_KO )
{
   if ((!pSpinLock_KO) || (!pSpinLock_KO->pSpinlock_X)) {ErrOutput("Invalid Spinlock\n");return;}
   spin_unlock_irqrestore(pSpinLock_KO->pSpinlock_X, pSpinLock_KO->Flags_UL);
}

void KOBJ_InitializeFastMutex(PDEVICE_EXTENSION pdx, KOBJ_FAST_MUTEX * pFastMutex_KO )
{
   if (pFastMutex_KO) 
   {
      pFastMutex_KO->pSemaphore_X = RequestSemaphoreFromPool(pdx);
      if (pFastMutex_KO->pSemaphore_X) sema_init(pFastMutex_KO->pSemaphore_X, 1);
   }
}

void KOBJ_AcquireFastMutex(PDEVICE_EXTENSION pdx, KOBJ_FAST_MUTEX * pFastMutex_KO )
{
   if ((!pFastMutex_KO) || (!pFastMutex_KO->pSemaphore_X)) {ErrOutput("Invalid Mutex (%08p/%08p)\n",pFastMutex_KO,pFastMutex_KO->pSemaphore_X);return;}
   down_interruptible(pFastMutex_KO->pSemaphore_X);
}

void KOBJ_ReleaseFastMutex(PDEVICE_EXTENSION pdx, KOBJ_FAST_MUTEX * pFastMutex_KO )
{
   if ((!pFastMutex_KO) || (!pFastMutex_KO->pSemaphore_X)) {ErrOutput("Invalid Mutex\n");return;}
   up(pFastMutex_KO->pSemaphore_X);
}

void KOBJ_InitializeSyncEvent(PDEVICE_EXTENSION pdx,KOBJ_SYNC_EVENT * pSyncEvent_KO, BOOL32 State_B)
{
   
   if (!pSyncEvent_KO->pEvent_X) pSyncEvent_KO->pEvent_X = RequestEventFromPool(pdx);
   if (pSyncEvent_KO->pEvent_X) init_waitqueue_head(pSyncEvent_KO->pEvent_X);
   pSyncEvent_KO->Flags_i=0;
}

BOOL32 KOBJ_SetSyncEvent(PDEVICE_EXTENSION pdx,KOBJ_SYNC_EVENT *pSyncEvent_KO)
{
   pSyncEvent_KO->Flags_i=1;
   wake_up_interruptible((wait_queue_head_t*)pSyncEvent_KO->pEvent_X);
   return TRUE;
}
BOOL32 KOBJ_ResetSyncEvent(PDEVICE_EXTENSION pdx,KOBJ_SYNC_EVENT *pSyncEvent_KO)
{
   /* To implement the resest, just wait on event with a timeout of 0 */
   pSyncEvent_KO->Flags_i=0;
   return (TRUE);
}

void KOBJ_WaitOnSyncEvent(PDEVICE_EXTENSION pdx,KOBJ_SYNC_EVENT *pSyncEvent_KO)
{
   wait_event_interruptible(*pSyncEvent_KO->pEvent_X,pSyncEvent_KO->Flags_i!=0);
   pSyncEvent_KO->Flags_i=0;
}

BOOL32 KOBJ_WaitOnSyncEvent_Timeout(PDEVICE_EXTENSION pdx,KOBJ_SYNC_EVENT *pSyncEvent_KO,ULONG Timeout_UL)
{
BOOL32 Sts_B = FALSE;
   
   long val = wait_event_interruptible_timeout(*pSyncEvent_KO->pEvent_X,pSyncEvent_KO->Flags_i!=0,(Timeout_UL*HZ)/1000);
   pSyncEvent_KO->Flags_i=0;
   if (!val) Sts_B=FALSE; /* timeout */
   else if (val >0) Sts_B = TRUE; /* Process receive event */
   else Sts_B = FALSE; /* Other conditions occurs */
   

   return (Sts_B);
}

BOOL32 KOBJ_CreateKThread(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ, KTHREADPROC fct, void * pThreadParam_v )
{
   if (pKThread_KOBJ)
   {
      KOBJ_InitializeSyncEvent(pdx,&pKThread_KOBJ->SyncObject_KOBJ,FALSE);
      pKThread_KOBJ->pContext_v=pThreadParam_v;
      pKThread_KOBJ->SyncCmd_UL=0;
      pKThread_KOBJ->ThreadProc_fct=fct;
      pKThread_KOBJ->ts=kthread_run(fct,pKThread_KOBJ,"kthread");
      pKThread_KOBJ->State_e = KTHREAD_STATE_STARTED;

      return TRUE;
   }

   return FALSE;

   
}

void KOBJ_StopKThread(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ )
{
   if (pKThread_KOBJ)
   {
      if (pKThread_KOBJ->State_e==KTHREAD_STATE_STARTED)
      {
         KOBJ_SendKThreadSyncCmd(pdx,pKThread_KOBJ,KTHREAD_SYNCMD_STOP);
         kthread_stop(pKThread_KOBJ->ts);
         pKThread_KOBJ->State_e=KTHREAD_STATE_STOPPED;
      }
   }
}

BOOL32 KOBJ_SendKThreadSyncCmd(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ, ULONG SyncCmd_UL )
{
   if (pKThread_KOBJ && (pKThread_KOBJ->State_e==KTHREAD_STATE_STARTED))
   {
      InterlockedOr(&pKThread_KOBJ->SyncCmd_UL,SyncCmd_UL);
      return KOBJ_SetSyncEvent(pdx,&pKThread_KOBJ->SyncObject_KOBJ);
   }
   return FALSE;
}

void KOBJKT_ExitKThread(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ )
{
   //do_exit(0); Cannot be done after kthread_stop
}

void * KOBJKT_GetKThreadParam(KOBJ_KTHREAD *pKThread_KOBJ )
{
   return pKThread_KOBJ->pContext_v;
}

BOOL32 KOBJKT_WaitForKThreadSyncCmd(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ, ULONG *pSyncCmd_UL, ULONG Timeout_UL )
{
   BOOL32 Sts_B;
   ULONG SyncCmd_UL;

   Sts_B = KOBJ_WaitOnSyncEvent_Timeout(pdx,&pKThread_KOBJ->SyncObject_KOBJ,Timeout_UL);

   if (Sts_B) 
   {  
      SyncCmd_UL = InterlockedExchange(&pKThread_KOBJ->SyncCmd_UL,0);
      if (pSyncCmd_UL) *pSyncCmd_UL = SyncCmd_UL;
   }

   if (kthread_should_stop()) {Sts_B = TRUE; SyncCmd_UL=KTHREAD_SYNCMD_STOP;} // Force sending STOP COMMAND

   return Sts_B;
}



BOOL32 KOBJ_CreateMemPool(PDEVICE_EXTENSION pdx, KOBJ_MEMPOOL * pMemPool_KOBJ, ULONG Size_UL, ULONG Tag_UL )
{
   BOOL32 Sts_B = FALSE;

   if (pMemPool_KOBJ)
   {
      pMemPool_KOBJ->pPtr_v = vmalloc(Size_UL);
      if (pMemPool_KOBJ->pPtr_v)
      {
      	 memset(pMemPool_KOBJ->pPtr_v,0,Size_UL);
         pMemPool_KOBJ->Size_UL = Size_UL;
         pMemPool_KOBJ->Tag_UL = Tag_UL;
         Sts_B = TRUE;
      }
      else
         ErrOutput("vmalloc allocation failure: %u (requires vmalloc size increase)\n",Size_UL);
   }

   return Sts_B;
}

void KOBJ_DestroyMemPool(PDEVICE_EXTENSION pdx, KOBJ_MEMPOOL * pMemPool_KOBJ )
{
   if (pMemPool_KOBJ && pMemPool_KOBJ->pPtr_v && pMemPool_KOBJ->Size_UL)
   {
      vfree(pMemPool_KOBJ->pPtr_v);
      pMemPool_KOBJ->pPtr_v=NULL;
      pMemPool_KOBJ->Size_UL=0;
      pMemPool_KOBJ->Tag_UL=0; 
   }
}

void * KOBJ_MemPoolGetDataPtr(PDEVICE_EXTENSION pdx, KOBJ_MEMPOOL * pMemPool_KOBJ )
{
   if (pMemPool_KOBJ) return pMemPool_KOBJ->pPtr_v;
   else return NULL;
}
