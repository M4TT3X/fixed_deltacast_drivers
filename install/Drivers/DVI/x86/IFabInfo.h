
#ifndef _IFABINFO_H_
#define _IFABINFO_H_

/***** INCLUDES SECTION ***********************************************************************************************/

class IFabInfo
{
public:
   virtual CDRVIF_STATUS Board_ReadFabInfo(ULONG *pFabInfo_UL, ULONG *pSize_UL)=0;
   virtual CDRVIF_STATUS Board_WriteFabInfo(ULONG *pFabInfo_UL, ULONG *pSize_UL)=0;
};



#endif // _IFABINFO_H_

