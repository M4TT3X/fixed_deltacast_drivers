/********************************************************************************************************************//**
 * @internal
 * @file   	DMABufMngr.h
 * @date   	2010/07/08
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/08  v00.01.0000    cs       Creation of this file
   2011/04/04  v00.01.0001    cs/gt    Add a #pragma LOCKED_CODE for this module
   2011/04/07  v00.01.0002    cs       Add debug purpose functions / IOCTLs
   2011/04/20  v00.01.0003    cs       Add a condition during the LockBySystem operation : the buffer cannot be marked as 
                                       "MARKED_AS_TO_FREE"

 **********************************************************************************************************************/

#ifndef _DMABUFMNGR_H_
#define _DMABUFMNGR_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvTypes.h"
#include "DMABufMngrTypes.h"
#include "KernelObjects.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/



#define INCREMENT_FLAGS_LOCK_BY_SYSTEM_CNT(pFlag) do{((UBYTE*)(pFlag))[1]++;}while(0)
#define DECREMENT_FLAGS_LOCK_BY_SYSTEM_CNT(pFlag) do{((UBYTE*)(pFlag))[1]--;}while(0)
#define GET_FLAGS_LOCK_BY_SYSTEM_CNT(pFlag) (((UBYTE*)(pFlag))[1])

/***** MACROS DEFINITIONS *********************************************************************************************/



/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct 
{
   ULONG pBufID_UL[MAX_DMABUF];
   ULONG pFlags_UL[MAX_DMABUF];

   KOBJ_SPINLOCK Spinlock_KO;
   
} DMABUFMNGR_DATA;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

BOOL32 DMABufMngr_DeviceAdd(PDEVICE_EXTENSION pdx);
BOOL32 DMABufMngr_LockBySystem(PDEVICE_EXTENSION pdx, DMABUFID BufID, BOOL32 LockSlaves_B);
BOOL32 DMABufMngr_UnlockBySystem(PDEVICE_EXTENSION pdx, DMABUFID BufID, BOOL32 UnlockSlaves_B);
BOOL32 DMABufMngr_Release(PDEVICE_EXTENSION pdx, DMABUFID BufID);

ULONG DMABufMngr_GetFlags(PDEVICE_EXTENSION pdx, DMABUFID BufID);
BOOL32 DMABufMngr_RetrieveFlags(PDEVICE_EXTENSION pdx, DMABUFID BufID, ULONG *pFlags_UL);
BOOL32 DMABufMngr_SetBitFlag(PDEVICE_EXTENSION pdx, DMABUFID BufID, ULONG *pFlag_UL);
BOOL32 DMABufMngr_ClrBitFlag(PDEVICE_EXTENSION pdx, DMABUFID BufID, ULONG *pFlag_UL);

BOOL32   DMABufMngr_Check(PDEVICE_EXTENSION pdx, DMABUFID BufID);
void * DMABufMngr_GetKAddr(PDEVICE_EXTENSION pdx, DMABUFID BufID);
ULONG  DMABufMngr_GetSize(PDEVICE_EXTENSION pdx, DMABUFID BufID);
DMABUFID  DMABufMngr_GetSlaveBufID(PDEVICE_EXTENSION pdx, DMABUFID MasterBufID, ULONG SlaveIdx_UL);

BOOL32 DMABufMngr_LinkBuffer( PDEVICE_EXTENSION pdx, ULONG BufID_UL, ULONG MasterBufID_UL );

IOCTL_STATUS DMABufMngr_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferSize_UL, PID_TYPE PID_UL);

BOOL32 DMABufHlp_LockBySystem(BUFID BufID_UL, ULONG *pFlags_UL, ULONG *pLinkedSlaveID_UL);
BOOL32 DMABufHlp_UnlockBySystem( BUFID BufID_UL, ULONG *pFlags_UL, ULONG *pLinkedSlaveID_UL);//, BOOL32 * pNeedToFree_B, BOOL32 *pSlavesToFree_B);


#endif // _DMABUFMNGR_H_
