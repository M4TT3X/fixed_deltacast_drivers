#ifndef _IGSPIINTERFACE_H_
#define _IGSPIINTERFACE_H_

/***** INCLUDES SECTION ***********************************************************************************************/
#include "IStreamDrvIF.h"

class IGSPIIF
{
public:  
   
   virtual CDRVIF_STATUS GSPIRead(ULONG ChipID_UL, ULONG RegAddr_UL, ULONG *pValue_UL)=0;
   virtual CDRVIF_STATUS GSPIWrite(ULONG ChipID_UL, ULONG RegAddr_UL, ULONG Value_UL)=0;

};

#endif // _IGSPIINTERFACE_H_
