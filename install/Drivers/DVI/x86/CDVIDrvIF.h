/********************************************************************************************************************//**
 * @internal
 * @file   	CDVIDrvIF.h
 * @date   	2010/07/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/16  v00.01.0000    cs       Creation of this file
   2011/04/08  v00.01.0005    cs       Add a CfgPLDVersion_UL member in the BOARD_INFO structure


 **********************************************************************************************************************/

#ifndef _CDVIDrvIF_H_
#define _CDVIDrvIF_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CStreamDrvIF.h"
#include "DVIDrv_Types.h"
#include "DVIDrv_Properties.h"
#include "DVIDrv_RegistersMap.h"
#include "DVIDrv_Version.h"
#include "DVIDrv_InterruptSourcesDef.h"
#include "DVI.h"
#include "SPI.h"
#include "IFwUpdate.h"
#include "IFabInfo.h"

namespace CDVIDrvIF_NS
{

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
 
typedef struct  
{
   DVIDRV_BOARD_TYPE BoardType_e;
   ULONG FirmwareVersion_UL;
   DVIDRV_FIRMWAREID FirmwareID_e;
   DVIDRV_FIRMWAREID ARMFirmwareID_e;
   STREAM_BOARD_STATE FirmwareState_e;
   ULONG CPLDVersion_UL;
   ULONG BoardFeatures_UL;
   UBYTE NbRxChannels_UB;
   BOOL32  DualDVI_B;
   ULONG RqstFpgaFwVersion_UL;
   ULONG ArmVersion_UL;
   ULONG RqstArmFwVersion_UL;
   ULONG NbUpgradableStuff_UL;
} BOARD_INFO;


enum CHANNEL
{
   CHANNEL_RX,
};

typedef struct  
{
   BOOL32            AutoDetectInputVidStd_B;  ///< If this parameter is set to TRUE, the VidStd_e will be automatically filled. But it will failed if no RX input is present
   DVI_DATA_FORMAT DataFormat_e;             ///< Specifies the data format
   ULONG           AdditionalFeatures_UL;

   BOOL32 ConfigureFrontEnd_B;
   DVI_MODE Mode_E;
   DVI_TIMING Timing_X;
   DVI_COLOR_SPACE InCS_E;
   DVI_COLOR_SPACE OutCS_E;
   BOOL32 Dual_B;

   BOOL32 WinCropEnable_B;
   USHORT WinCropHorStart_US;
   USHORT WinCropHorStop_US;
   USHORT WinCropVerStart_US;
   USHORT WinCropVerStop_US;

} RX_CHANNEL_SETUP_PARAM;

typedef enum
{
   CURRENTTEMPERATURE,
   MAXTEMPERATURE,
   MINTEMPERATURE
}DVI_TEMPERATURE_TYPE;



/***** CLASSES DECLARATIONS *******************************************************************************************/

class CDVIDrvIF : public CStreamDrvIF, public ISPIInterface, public IFwUpdate, public IFabInfo, public IArmUpdateOldWay
{
public:

   CDVIDrvIF(ULONG BoardIdx_UL);
   static ULONG GetNbBoard();

   virtual CBufferQueue * CreateBufferQueue();
   virtual CDRVIF_STATUS Board_IsOK();

   CDRVIF_STATUS Board_GetArmRevID(ULONG *pArmRevId_UL);
   CDRVIF_STATUS Board_GetArmSSN(ULONG *pArmSSNLS_UL,ULONG *pArmSSNIS_UL,ULONG *pArmSSNMS_UL);
   ULONG Board_GetNbOfLane();
   BOOL32  Board_FWWarmLoad();

   CDRVIF_STATUS Board_SetIntMask( ULONG Mask_UL );
   CDRVIF_STATUS Board_ClrIntMask( ULONG Mask_UL );

   const BOARD_INFO * Board_GetInfo() {return &m_BoardInfo_X;}

   BOOL32 Board_IsDual(void);

   CDRVIF_STATUS Channel_DviRx_Start(UBYTE Channel_UB, RX_CHANNEL_SETUP_PARAM * pParam_X);
   CDRVIF_STATUS Channel_DviRx_Stop(UBYTE Channel_UB);
   
   CDRVIF_STATUS Channel_DviRx_GetStatus(UBYTE Channel_UB, BOOL32 *pLocked_B);
   CDRVIF_STATUS Channel_DviRx_GetMode(UBYTE Channel_UB, BOOL32 *pValid_B, DVI_MODE *pMode_E);
   CDRVIF_STATUS Channel_DviRx_GetDigitalVideoInfo(UBYTE Channel_UB, BOOL32 *pLocked_B, float *pRate_f, BOOL32 *pInterlaced_B, ULONG *pWidth_UL, ULONG *pHeight_UL,BOOL32 *pDual_B=NULL, double *pPxlClk_d=NULL,ULONG *pTotalWidth_UL=NULL,ULONG *pTotalHeight_UL=NULL, DVI_MODE *pMode_E=NULL);
   CDRVIF_STATUS Channel_DviRx_GetAnalogVideoInfo(UBYTE Channel_UB, BOOL32 *pLocked_B, POLARITY *pHpol_E, POLARITY *pVpol_E, ULONG *pVSLength_UL, double *pLineDuration_d, ULONG *pFieldLength_UL, double *pFieldDuration_d);

   CDRVIF_STATUS Channel_DviRx_ConfigFrontEnd( UBYTE Channel_UB, RX_CHANNEL_SETUP_PARAM *pParam_X );

   CDRVIF_STATUS Channel_DviRx_SetPxlShift( UBYTE Channel_UB, int PxlShift_i);
   CDRVIF_STATUS Channel_DviRx_SetLineShift( UBYTE Channel_UB, int LineShift_i);
   CDRVIF_STATUS Channel_DviRx_SetPhase( UBYTE Channel_UB, ULONG Phase_UL);
   CDRVIF_STATUS Channel_DviRx_SetFrameDecimation( UBYTE Channel_UB, BOOL32 Enable_B, UBYTE Denominator_UB=2, UBYTE Numerator_UB=1);
   CDRVIF_STATUS Channel_DviRx_SetWinCrop(UBYTE Channel_UB, BOOL32 Enable_B, USHORT HorStart_US=0, USHORT HorStop_US=0, USHORT VerStart_US=0, USHORT VerStop_US=0);

   CDRVIF_STATUS Channel_DviRx_LoadEDID(UBYTE Channel_UB, BYTE *pBuffer_UB, ULONG BufferSize_UL);
   CDRVIF_STATUS Channel_DviRx_ReadEDID(UBYTE Channel_UB,BYTE *pEEDIDBuffer, ULONG *pEEDIDBufferSize);
  
   CDRVIF_STATUS Channel_DviRx_GetEDIDConfig(UBYTE Channel_UB,BOOL32 *pConReadable_B);
   CDRVIF_STATUS Channel_DviRx_SetEDIDConfig(UBYTE Channel_UB,BOOL32 ConReadable_B);
   
   
   CDRVIF_STATUS Board_GetTemperature(DVI_TEMPERATURE_TYPE TemperatureType_E,double *pValue_d);


   static ULONG Helper_GetOnBoardAddress(CHANNEL Channel_e,UBYTE ChnIdx_i, DVIBRD_CHN_DATA_TYPE DataType_e, ULONG BufIdx_UL);
   static void Helper_FillOnBoardAddresses(CHANNEL Channel_e,UBYTE ChnIdx_i, BUFFERQUEUE_PARAM *pBQParam_X);

   void Board_SetOnBoardBufferBaseAddr(CHANNEL Channel_e, UBYTE ChannelIdx_UB, DVIBRD_CHN_DATA_TYPE DataType_e, UBYTE BufferIdx_UB, ULONG BufferBaseAddr_UL );
   void Board_SetOnBoardBufferBaseAddr( CHANNEL Channel_e, UBYTE ChannelIdx_UB, BUFFERQUEUE_PARAM *pBQParam_X );

   CDRVIF_STATUS Board_SerialNumberRead(LONGLONG *pSerialNumber_LL,LONGLONG *pSerialNumberEx_LL);
   ULONG Board_GetCPLDVersion();

   CDRVIF_STATUS Board_FWPROMUpdate(UBYTE FirmwareID_UB, UBYTE *pBuffer_UB, ULONG BufferSize_UL,BOOL32 CheckFirmware_B=FALSE, INotifyProgressFunctor *pSignalProgress_fct=NULL);

   void           MonitorProgress(ULONG ProgressInPrc_UL);

   CDRVIF_STATUS Board_ARMUpgrade( UBYTE *pBuffer_UB, ULONG BufferSize_UL );
   CDRVIF_STATUS Board_ARMUpgradeOldWay( UBYTE *pBuffer_UB, ULONG BufferSize_UL );

   CDRVIF_STATUS Board_LoadDriverProperties();

   ULONG Board_BrandingChallenge( ULONG Challenge_UL );
   ULONG Helper_SoftBrandingChallenge( ULONG Challenge_UL, ULONG BrandingKey_UL );
   CDRVIF_STATUS Board_ReadFabInfo(ULONG *pFabInfo_UL, ULONG *pSize_UL);
   CDRVIF_STATUS Board_WriteFabInfo(ULONG *pFabInfo_UL, ULONG *pSize_UL);

public:
   virtual BOOL32 SPI_CheckErasing( ULONG StartAddr_UL,ULONG Nb_UL, INotifyProgressFunctor *pSignalProgress_fct );
   virtual BOOL32 SPI_CheckWriting( UBYTE *pBuffer_UB, ULONG StartAddr_UL, ULONG Nb_UL, INotifyProgressFunctor *pSignalProgress_fct );
   virtual void SPI_ReadStatus( UBYTE *pStatus_UB );
   virtual void SPI_SectorErase( ULONG StartAddr_UL, ULONG NbSector_UL, BOOL32 Sector64K_B, INotifyProgressFunctor *pSignalProgress_fct );
   virtual void SPI_WriteEnable();
   virtual void SPI_WriteDisable();
   virtual void SPI_Read( UBYTE *pBuffer_UB, ULONG StartAddr_UL, ULONG Nb_UL, INotifyProgressFunctor *pSignalProgress_fct );
   virtual void SPI_Write( UBYTE *pBuffer_UB, ULONG StartAddr_UL, ULONG Nb_UL, INotifyProgressFunctor *pSignalProgress_fct );
   virtual void SPI_WaitBusy();


protected:
   CDRVIF_STATUS Board_ARMUpgradeEx( UBYTE *pBuffer_UB, ULONG BufferSize_UL, BOOL32 OldWay_B );
   CDRVIF_STATUS Board_ArmCmd(ULONG Cmd_UL, ULONG *pStatus_UL=NULL);
   BOOL32  Board_IsDDRAMOk();

   BOARD_INFO m_BoardInfo_X;


 

protected:
   virtual BOOL32 CheckValidateAccessCode(ULONG ValidateAccessCode_UL);
   static void    MonitorSpiProgress(ULONG ProgressInPrc_UL, void *pUser );



   int         m_FwUpdateStepCnt_i;
   int         m_FwUpdateTotalStep_i;
   INotifyProgressFunctor * m_pUserSignalProgress_fct;
};

class CDVIDrvBufferQueue : public CBufferQueue
{



   friend class CDVIDrvIF;

public:

   typedef struct 
   {
      ULONG NbBuffer_UL;
      UBYTE ChannelIndex_UB;
      CHANNEL  Channel_e;
      DVI_DATA_FORMAT DataFormat_e;
      ULONG NbPixels_UL;
      CDMABuffer::TYPE BufferType_E;
   } DVI_BUFFERQUEUE_PARAM;

   CDRVIF_STATUS Create(DVI_BUFFERQUEUE_PARAM * pParam_X);
public:
   

protected:

   CDVIDrvBufferQueue(CDVIDrvIF * pOwner_O);
private:

   CDVIDrvIF * m_pOwner_O;
};

};
#endif // _CDVIDrvIF_H_
