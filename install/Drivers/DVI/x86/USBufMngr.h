/********************************************************************************************************************//**
 * @file   	USBufMngr.h
 * @date   	2010/07/01
 * @author 	cs
 * @version v00.01.0000 
 * @brief   This file describes the functions associated to the User-Space buffer management.
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/01  v00.01.0000    cs       Creation of this file
   2011/04/04  v00.01.0001    cs/gt    Add a #pragma LOCKED_CODE for this module in the Windows Implementation
   2011/04/07  v00.01.0002    cs       Add a protection against a race condition that could lead to a parallel call to 
                                       USBufMngr_UnregisterUSBuf() and induce a second unwanted page unlocking.
   2011/04/07  v00.01.0003    cs       Increase the number of available USBuffer to get the same number than the Common buffer manager.
   2011/09/08  v00.02.0000    cs       Deep update in the way free a User Space buffer. Just like the common buffer, a mechanism
                                       existed to prevent to free a buffer currently used in a DMA process.
                                       This mechanism really freed buffer used in a DMA process on the DMA done (in the DPC). The linux
                                       implementation is not able to release the buffer in a DP (IRQL is too high).
                                       So, in the case of User Space buffer, a call a a free buffer now waits for the buffer unlock before freeing it.
   2011/10/28  v00.02.0001    cs/jj    Fix a buffer overflow in the USBufMngr_RegisterUSBuf() 


 **********************************************************************************************************************/

#ifndef _USBUFMNGR_H_
#define _USBUFMNGR_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DMABufMngr.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define MAX_USBUF           MAX_DMABUF     ///< Defines the maximum number of User space buffer that can be registered.
#define MAX_PAGES_PER_USBUF 8704    ///< Defines the maximum number of pages per User space buffer.

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/**********************************************************************************************************************//**
 * @struct USBUF_DESC
 * @brief  Descriptor for a registered user space buffer.
 *
 * This descriptor contains all required data to access to a user space buffer registered as "DMA buffer". This structure
 * is filled by the USBufMngr_RegisterUSBuf function and cleared by the USBufMngr_UnregisterUSBuf.
 *************************************************************************************************************************/
typedef struct  
{
	void * pUserSpacePtr_v;
	PID_TYPE PIDOwner_UL;			///< Keep trace of PID owner for cleanup during userspace application crash.
	ULONG Size_UL;
	
   void * pOSData_v;                ///< Store OS-dependent data about the User Space buffer.
   
	ULONG NbPages_UL;
	ULONG Offset_UL;
   ULONG StartPageIndex_UL;         ///< Index of the first page rellay used as DMA buffer
   ULONG NbUsedPages_UL;            ///< Number of page used as DMA buffer

   ULONGLONG *pPagesPhysAddr_ULL;   ///< Stores the buss addresses table
   void      *pKAddr_v;             ///< Kernel Address of the first page (StartPageIndex_UL)
   
   BOOL32   Slave_B; 
   ULONG  pLinkedSlaveID_UL[DMABUF_MAX_NB_SLAVES]; ///< @remark These values must be set by default to INVALID_BUFID

} USBUF_DESCR;

/**********************************************************************************************************************//**
 * @struct US_BUF_MNGR_DATA
 * @brief  Contains all needed data for User Space Buffers Manager
 *
 * These data are part of the device extension (PDEVICE_EXTENSION) defined in Driver.h.
 *************************************************************************************************************************/
typedef struct 
{
   KOBJ_MEMPOOL   MemPoolForDescr_KOBJ;
   KOBJ_MEMPOOL   pMemPoolForPagesDescr_KOBJ[MAX_USBUF];
	USBUF_DESCR		*pUSBufList_X;//[MAX_USBUF];
	ULONG			   pUSBufFlags[MAX_USBUF];
	KOBJ_SPINLOCK	Spinlock_X;					///< Spinlock to protect exclusive access to the descriptors list and flags.
} US_BUF_MNGR_DATA;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void   USBufMngr_DeviceAdd( PDEVICE_EXTENSION pdx);
void   USBufMngr_ReleasePermanentResources(PDEVICE_EXTENSION pdx);
void   USBufMngr_CleanUp(PDEVICE_EXTENSION pdx,PID_TYPE PID_UL, BOOL32 DevicePresent_B);
BOOL32 USBufMngr_LockBySystem(PDEVICE_EXTENSION pdx, ULONG BufID_UL, BOOL32 LockSlaves_B);
BOOL32 USBufMngr_UnlockBySystem(PDEVICE_EXTENSION pdx, ULONG BufID_UL, BOOL32 UnlockSlaves_B);

ULONG  USBufMngr_RegisterUSBuf(PDEVICE_EXTENSION pdx,void * pUSBufAddr, ULONG Size_UL,ULONG * pPageAlignedOffset_UL, PID_TYPE PID_UL);
BOOL32 USBufMngr_UnregisterUSBuf(PDEVICE_EXTENSION pdx, ULONG BufID_UL);
BOOL32 USBufMngr_GetBuf(PDEVICE_EXTENSION pdx, COMMON_BUFFER_DESCR *pDescr_X);
BOOL32 USBufMngr_ReleaseBuf(PDEVICE_EXTENSION pdx, COMMON_BUFFER_DESCR *pDescr_X);
IOCTL_STATUS USBufMngr_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferSize_UL, PID_TYPE PID_UL);

void   USBufMngr_Check(PDEVICE_EXTENSION pdx);
BOOL32 USBufMngr_LinkBuffer(PDEVICE_EXTENSION pdx, ULONG BufID_UL, ULONG MasterBufID_UL);
BUFID  USBufMngr_GetSlaveBufID( PDEVICE_EXTENSION pdx, BUFID MasterBufID, ULONG SlaveIdx_UL );


USBUF_DESCR * USBufMngr_GetDescr(PDEVICE_EXTENSION pdx, ULONG BufID_UL);
#endif // _USBUFMNGR_H_
