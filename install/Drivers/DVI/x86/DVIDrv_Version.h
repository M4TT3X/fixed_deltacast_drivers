/********************************************************************************************************************//**
 * @internal
 * @file   	DVIDrv_Version.h
 * @date   	2010/08/26
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/26  v05.03.0000    cs       Creation of a test driver based on the new driver skeleton/architecture
   2011/01/21  v05.03.0007    ja       New FPGA (11011101) Dual Link RGB 24b bug fix
   2011/02/17  v05.03.0008    ja       Bug fix: Board_BufMngrRxIntHandler() - SubBufID_UL for all data types
   2011/03/08  v05.03.0009    cs       Firmware Updates :
                                       FPGA  : v11030301
                                       ARMv1 : v11030801
                                       ARMv2 : v11030810
   2011/02/23  v05.03.0010    ja       New: Add link between hardware interrupt FPGA_IMR_BIT_RXx_OVERRUN and logical interrupt INT_SOURCE_STS_CHANGE_RXx_INT
   2011/09/08  v05.03.0011    ja       New ARMv2: 11090810 - DVI-A after DVI-D Dual detection bug fixed
                                       New FPGA: 11090602 - PCIe Vigor bug 
                                                            Corrupted bit mapping for RX1
                                       Bug fix : In Linux, trying to install the driver after fw upgrade crashes the system. -> In the probe, unload the driver after fw upgrade.
   2011/09/19  v05.03.0012    gt       New: Add DbgMask driver parameter to control the default debug output level
                                       Bug fix: Potential tearing on TX channel if EOD and Video INT occur at the same time -> solution : enable early transfert
   2011/08/15  v05.03.0013    lp/cs    Support compatibility 32 & 64 bits
   2012/08/14  v05.03.0014    gt       Under Windows, map in kernel space only the first page of each buffer to avoid VHDERR_OPERATIONFAILED on VHD_StartStream with tiny kernel address space
   2012/10/12  v05.03.0015    ja       SkelDrv_Close in DispatchClose is now out of the SAFEAREA
                                       new EventMngr_CloseByHandle function  that dereferences object (nonpaged memory leak fix)
   2012/10/19  v05.03.0016    ja       Add EXTRA_CFLAGS += -fno-stack-protector in Makefile and Makefile_client
   2012/10/23  v05.03.0017    ja       Add protection in EventMngr_DPCHandler to avoid KOBJ_SetSyncEvent call if the event has been closed meanwhile   
   2012/12/13  v05.03.0018    cs       Bug Fix: Add a missing pointer check after allocation before using it
   2012/10/30  v05.03.0019    cs       Includes an update in the PCI skel driver to get rid off the windows power management part.
   2012/11/12  v05.03.0020    cs/ja    Linux KOBJ_CreateMemPool allocation limited to 128K in old kernel. kmalloc has been replaced by vmalloc
   2012/11/12  v05.03.0021    cs/ja    MainDevice_Open/DispatchCreate and MainDevice_Release/DispatchClose functions have bad level of execution
   2012/12/17  v05.03.0022    ja       new ARMv1: 12121401
                                       new ARMv2: 12121410
                                       Color space converter bug
   2013/01/15  v05.03.0023    gt       Under Windows, map in kernel space only the first page of each buffer to avoid VHDERR_OPERATIONFAILED on VHD_StartStream with tiny kernel address space
   2013/01/30  v05.03.0024    gt       bug fix : code was not locked
   2013/05/17  v05.03.0025    bc       Add support Mac OSX + change KOBJ_SetSyncEvent mechanism  
   2013/05/29  v05.00.0025    ja      Mempool for DMA descriptors is now allocated dynamically in USBufMngr_RegisterUSBuf and freed in USBufMngr_UnregisterUSBuf
   2013/06/26  v05.00.0026    ja       Windows USBufMngr allocate several MDL to sustain size buffer limit
   2013/09/04  v05.00.0027    ja       Bug: update Board to fit with new boarddata definition
   2013/09/26  v05.00.0028    gt       Bug fix : BoardDataMemPool_X was freed before disabling interrupt causing BSOD in ISR when an interrupt occurs after freeing BoardDataMemPool_X
   2013/10/17  v05.00.0029    gt       Bug fix : SkelDrv_AllocDrvData was called after SkelDrv_DeviceAdd. Move SkelDrv_AllocDrvData from WDFEvtPrepareHW to WDFEvtDeviceAdd
   2013/10/17  v05.00.0030    gt       Bug fix : SkelDrv_FreeDrvData was called to soon. Move SkelDrv_FreeDrvData from WDFEvtReleaseHW to WDFEvtDeviceContextCleanup
   2013/10/21  v05.00.0031     gt       Bug fix : In Stream_Wakeup, CallBack_Wakeup_fct returned status is overwritten by MultiChnDMAMngr_Wakeup returned status. 
   2013/10/21  v05.00.0032     gt       Bug fix : In Stream_Wakeup, MultiChnDMAMngr_Wakeup is called before CallBack_Wakeup_fct
   2014/01/03  v05.03.0033     gt      Bug fix : Windows device manager returned code 10 after updating firmware
   2014/02/13  v05.03.0034      gt       Bug fix: cold-unplug with a running app result in a call to DeviceClose after DeviceReleaseHW. A BSOD occurs because BoardData is accessed after unallocation.
                                       => move SkelDrv_AllocDrvData in WdfDeviceAdd and SkelDrv_FreeDrvData in WDFDeviceContextCleanup. 
   2014/02/19  v05.03.0035    bc       Bug fix : Bad sync event init under OSX
   2014/03/05  v05.03.0036    gt       Bug fix : LinuxAllocator doesn't compile with newer kernel : kmalloc is undefined. Add the include of <linux/slab.h>)
   2014/04/01  v05.03.0037    ja       Bug fix: Field mode underrun support: because in field mode, underrun interrupts are triggered one out of two, we need to increase STREAM_PROPERTIES_TX0_CHN_ONBOARD_FILLED_BUFFER_CNT on underrun (in field mode) to maintain correct value (see Stream_ISR() )
   2014/04/14  v05.03.0038    gt       Bug fix : Under Linux, boardData structure is not cleared before enabling interrupt => kernel panic in case of shared IRQ
   2014/04/15  v05.03.0039    gt       Improvement : Improve FPGA firmware upgrade in user mode.
   2014/05/02  v05.03.0040    ja       Bug fix: spinlock_t in KernelObject become raw_spinlock_t to avoid Linux RT interrupts freeze
	2014/06/17  v05.03.0041    ja       New FPGA (DVI=0x14061701): 4:2:2:0 dual link format support (front end decimation)
	2014/08/28	v05.03.0042		ja			Bug fix:  Call the entire remove() function execpt free_irq if request_irq() linux function fails in DriverEntre::probe
	2014/07/10	v05.03.0042		ja			Bug fix: Linux process id (PID) is saved in structure because it can be different in MainDevice_Release when the process is killed (see MainDevice_Open function). 
- SDK 5.17
   2015/04/24	v05.03.0043		ms			New FPGA (DVI=0x15031601): Add support of branding challenge
   2015/05/19	v05.03.0044		bc			New FPGA (DVI=0x15051804) : Change behavior of sd_fifo_rst at line_in level to avoid erroneous write into DDR
   2015/06/08  v05.03.0045    gt      In ARMUART_FlashUpgrade, ARMUART_CMD_BL_INIT is send once before waiting for ACK
   2015/06/10 v05.03.0046     gt       Bug fix : do_gettimeofday is subject to NTP server time adjustement => replace by getrawmonotonic
   2015/11/18  v05.03.0047    gt       New FPGA (DVI=0x15101301) Bug fix : Packing for Dual-Link YUV422 was not correct
                              gt       SkelDrv_Sleep was not called in removeEx (Linux)
- SDK 5.19
   2016/01/26 v05.03.0048     gt       New : compatibility with Linux kernel >= 4.1 (remove IRQF_DISABLED usage)
   2016/02/16 v05.03.0049     ja       Bug fix: some IOCTL used user space memory without KSYSCALL_CopyToUserSpace. That caused crash on linux with Z170 chipset.
   2016/03/23 v05.03.0050     bc       Bug fix : check device presence before calling SkelDrv_SetDrvPowerState to avoid blue screen if device is removed


 **********************************************************************************************************************/

#ifndef _DVIDRV_VERSION_H_
#define _DVIDRV_VERSION_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define DVIBRD_VERMAJOR 5
#define DVIBRD_VERMINOR 3
#define DVIBRD_BUILD 50
#define DVIBRD_PRODVER "05.03.0050\0" 

#define DVIDRV_VERSION ((DVIBRD_VERMAJOR<<24)|(DVIBRD_VERMINOR<<16)|(DVIBRD_BUILD))

/** @def VALIDATE_ACCESS_CODE
 * The validate access code allows to check if the interface between driver and interface object are compatible.
 * If the object driver interface is linked to a greater minor code, it could inform that a newer driver is available, but, it
 * can work with the current one.
 * If the major access code mismatches, the interface object is not able to work  with the current driver.
 **/

#define DVIBRD_VALIDATE_ACCESS_CODE_MAJOR 0x0006 ///< Gives a version number of the interface. This number must MATCH !
#define DVIBRD_VALIDATE_ACCESS_CODE_MINOR 0x0001
#define DVIBRD_VALIDATE_ACCESS_CODE ((DVIBRD_VALIDATE_ACCESS_CODE_MAJOR<<16) | DVIBRD_VALIDATE_ACCESS_CODE_MINOR)

#define DVI_PCIE_FW_VERSION       0x15101301
#define DVI_PCIE_SAFEFW_VERSION   0x09022500

#define DVI_PCIE_ARM_FW_VERSION   0x12121401
#define DVI_PCIE2_ARM_FW_VERSION  0x12121410

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _DVIDRV_VERSION_H_
