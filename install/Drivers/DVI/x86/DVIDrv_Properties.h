/********************************************************************************************************************//**
 * @internal
 * @file   	DVIDrv_Properties.h
 * @date   	2010/08/26
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/26  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _DVIDRV_PROPERTIES_H_
#define _DVIDRV_PROPERTIES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "StreamProperties.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
  
   DVIDRV_BOARD_TYPE_PCIE,
   DVIDRV_BOARD_TYPE_PCIE2
   
} DVIDRV_BOARD_TYPE;

typedef enum
{
   DVIDRV_FIRMWAREID_UNKOWN,    
   DVIDRV_FIRMWAREID_PCIE_NOFW,
   DVIDRV_FIRMWAREID_ARM,
   DVIDRV_FIRMWAREID_ARM2,


}DVIDRV_FIRMWAREID;

typedef enum
{
   DVIDRV_PROPERTIES_FIRST=NB_STREAM_PROPERTIES,
   DVIDRV_PROPERTIES_BOARD_TYPE=DVIDRV_PROPERTIES_FIRST,
   DVIDRV_PROPERTIES_BOARD_FEATURES,
   DVIDRV_PROPERTIES_BOARD_FIRMWAREID,
   DVIDRV_PROPERTIES_BOARD_ARM_FIRMWAREID,
   DVIDRV_PROPERTIES_BOARD_FIRMWARE_VERSION,
   DVIDRV_PROPERTIES_BOARD_ARM_FIRMWARE_VERSION,
   
   DVIDRV_PROPERTIES_BOARD_RQST_ARM_VERSION,
   DVIDRV_PROPERTIES_BOARD_NB_UPGRADABLE_STUFF,
   DVIDRV_PROPERTIES_BOARD_ARM_VERSION,
   DVIDRV_PROPERTIES_BOARD_RQST_FPGA_VERSION,
  
   NB_DVIDRV_PROPERTIES
} DVIDRV_PROPERTIES;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _DVIDRV_PROPERTIES_H_
