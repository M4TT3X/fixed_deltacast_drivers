/**********************************************************************************************************************
 
   Module   : InterlockedOp
   File     : InterlockedOp.cpp
   Created  : 2014/04/25
   Author   : ja

 **********************************************************************************************************************/

/***** INCLUDES *******************************************************************************************************/

#include "InterlockedOp.h"
#include <asm/atomic.h>

LONG InterlockedIncrement(volatile LONG* Addend_L )
{
   return InterlockedExchangeAdd( Addend_L, 1 );
}

LONG InterlockedDecrement(volatile LONG* Addend_L )
{
   return InterlockedExchangeAdd( Addend_L, -1 );
}

LONG InterlockedExchangeAdd(volatile LONG* Addend_L, LONG Increment_L )
{
   return __sync_fetch_and_add(Addend_L,Increment_L);
#if 0
   long ReturnValue;

   __asm __volatile(
      "lock xaddl %2,(%1)"
      : "=r" (ReturnValue)
      : "r" (Addend_L), "0" (Increment_L)
      : "memory");

   return ReturnValue;
#endif

}

LONG InterlockedOr(volatile LONG* pTarget_L, LONG Value_L )
{
   return __sync_fetch_and_or(pTarget_L, Value_L);

#if 0
   asm volatile ( "lock orl %1,%0"
      : "=m" (* pTarget_L)
      : "r" (Value_L) );
#endif

}

LONG InterlockedAnd(volatile LONG* pTarget_L, LONG Value_L )
{
   return __sync_fetch_and_and(pTarget_L, Value_L);

#if 0
   asm volatile ( "lock andl %1,%0"
      : "=m" (* pTarget_L)
      : "r" (Value_L) );
#endif
}

LONG InterlockedExchange(volatile LONG* pTarget_L, LONG Value_L)
{
   return xchg(pTarget_L, Value_L);
}
