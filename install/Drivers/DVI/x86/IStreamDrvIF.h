/********************************************************************************************************************//**
 * @file   CSDIDrvInterface.h
 * @date   2008/02/21
 * @Author cs
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*

 ====================================================================================================================
 =  Version History                                                                                                 =
 ====================================================================================================================
 Date     	Version     	Author   Description
 ====================================================================================================================
 2008/02/21  v00.01.0000    cs       Creation of this file
 2008/03/07  v00.02.0000    cs       Define GetVidStdCharacteristics() method as static
 2008/03/11  v00.03.0000    cs       Add a pure virtual Genlock_GetStatus() method.
 2008/03/13  v00.04.0000    cs       Add the way to create buffer queue with default parameters. This method is the
 same for both SD and HD boards. The method build the full parameter set from 
 basic parameters like associated channel, channel data format and data type.
 This is an easy way that cover 99% of cases.
 2008/03/18  v00.05.0000    cs       Change some comparison in the buffer quque Link method() to allow to link buffer queue
 with ANC.
 2008/04/09  v00.06.0000    cs       **INTERFACE CHANGE ** : add an extra parameter in the TX_CHANNEL_START_PARAM and in RX_CHANNEL_START_PARAM
 Set this extra paramter to 0 as default to maintain the same bahavior.

 2008/06/17  v00.07.0000    cs       ** INTERFACE CHANGE ** : 
 The Board_IsOK() method doesn't return a BOOL32 anymore, but a CDRVIF_STATUS (ULONG). 
 Be carefull, the returned value is not compatible, because the CDRVIF_STATUS_SUCCESS is 0 !
 The new driver interface takes care of new driver validate code access to check coherency between 
 the driver and the driver interface object.

 Add a way to validate coherency between interface object and driver.
 2008/07/10   v00.08.0000    cs       CSDI_BufferQueue contructors are now protected. If you have to create this knid of objects, use
 the virtual CSDIDrvInterface::CreateBufferQueue method.
 A new m_DMASize_UL member has been introduced to automatically increase the memory size
 of the allocated memory block (by AllocateMemory()) to a multiplie of this value.
 This value is board Dependant, so it is no more possible to create CSDI_BufferQueue object.
 So, if you are using new on the kind of object, replace your code by :

 CSDI_DrvInterface *pMyDrv_O;

 ... pMyDrv_O is created somewhere 

 CSDI_BufferQueue *pPtr_O;

 pPtr_O = new CSDI_BufferQueue(pMyDrv_O,&BufferQueueParam_X);

 ...


 Replace the last line by :

 pPtr_O = pMyDrv_O->CreateBufferQueue();
 pPtr_O->Create(&BufferQueueParam_X); // Here you can check the returned value

 2009/06/08  v00.09.0000    cs       Change the prototype of the AddDMARequest() method to take care of multi-queues and multi-channels features. 
 Default values for the additional parameters maintain the backward compatibility with old source code. By default, 
 the request is given to the queue 0 of DMA channel 0.
 Add a DMA queue max size setup method (DMAQueueSetup())
 2009/06/22  v00.09.0001    gt       Add the preload method in CSDIDrvInterface and CSDI_BufferQueue class
 2009/09/07  v00.09.0002    cs/gt/yf AddDMARequest() method was enhanced to match to the new DMA managment implementation. In any cases, 
 it calls the new IOCTL with DMA channel set to 0. This implementaion has broken the backward compatibility,
 because a new DLL running on a older driver fails to call the inexisting new IOCTL. The VAC number hasn't change
 because the driver interface stays unchanged.
 2010/02/11  v00.09.0003    jm       new : Support of memory pool preallocation
 Add PreallocUsed_B and pPreallocPoolSize_UL fields in BUFFERQUEUE_PARAM
 2010/03/15  v00.09.0004    cs       Fix a wrong definition of symbols BUFFERQUEUE_DATA_YANC and BUFFERQUEUE_DATA_CANC. Using these symbols to set 
 size in the buffer header mixed up the size between CANC and YANC.
 2010/06/03  v00.09.0005    gt       New : add the virtual function TxChannel_SetTristate()

   2011/05/06  v00.10.0000    cs     Add UpdateUserParameter_Set(),UpdateUserParameter_SetBit() and UpdateUserParameter_ClrBit()
                                     to buffer queue object.

 **********************************************************************************************************************/

#ifndef _ISTREAMDRVIF_H_
#define _ISTREAMDRVIF_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#ifdef __linux__
#include <linux/string.h>
#else
#include <string.h>
#endif

#include "BufMngrTypes.h"
#include "DMAMngrTypes.h"
#include "StreamProperties.h"
#include "ISkelDrvIF.h"
#include "Deprecated.h"



/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/** @defgroup BUFFERQUEUE_STS_GROUP Buffer Queue Status
* These values defines status values for the buffer queue. The status can be retrieved by the 
* CHDSDI_BufferQueue::Check() method. 
**///@{
enum BUFFERQUEUE_CHECK_STS
{
   BUFFERQUEUE_CHECK_STS_OK                	, ///< Buffer queue is ready to use
   BUFFERQUEUE_CHECK_STS_ALLOC_ERROR       	, ///< Error occurred during buffer allocation
   BUFFERQUEUE_CHECK_STS_EVENT_ERROR       	, ///< Error occurred during driver event opening
   BUFFERQUEUE_CHECK_STS_OPEN_ERROR        	, ///< Error occurred during the buffer queue opening
   BUFFERQUEUE_CHECK_STS_ADD_ERROR         	, ///< Error occurred during the buffers adding
   BUFFERQUEUE_CHECK_STS_UNKNOWN_ERROR     	, ///< Unknown error
   BUFFERQUEUE_CHECK_STS_LINK_ERROR          , ///< Error occurred during the link between two buffer queues.
   BUFFERQUEUE_CHECK_STS_INVALID_PARAM       , ///< Error occurred during the link between two buffer queues.
   BUFFERQUEUE_CHECK_STS_SLAVELINK_ERROR       ///< Error occurred during the link between two buffer queues.
};
//@}

/** @ingroup CDRVIF_STATUS_GROUP */
//@{
#define CDRVIF_STATUS_VALIDATE_ACCESS_CODE_MISMATCH   (CDRVIF_STATUS)(-1) //!< Validation code to control coherency between interface object and driver mismatch
#define CDRVIF_STATUS_DRIVER_ERROR                    (CDRVIF_STATUS)(-2) //!< Driver is not loaded 
#define CDRVIF_STATUS_HARDWARE_NOT_READY              (CDRVIF_STATUS)(-3) //!< Hardware is not ready 
//@}


/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/



/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/


/*********************************************************************************************************************//**
* @class CSDI_BufferQueue
* @brief This is the base object to implement a buffer queue.
*
* The buffer queue is a DMA buffer pole, used as interface between hardware data transfers at driver level, and user access
* to these data.
* The buffer queue data types are generic, so each inherited buffer queue objects can implement their creation method to
* abstract involved data types.
*
*************************************************************************************************************************/

#define MAX_NBOF_SUBBUF    (COMMON_BUFFER_MAX_SLAVE+1)


struct BUFFERQUEUE_PARAM
{
   ULONG Type_UL;			                     ///< Specifies the buffer queue types among the ref BUFFER_QUEUE_TYPE_GROUP "available ones" 
   union
   {
      ULONG BufferSize_UL;			                        ///< Specifies the size of the allocated buffers
      ULONG pBufferSize_UL[MAX_NBOF_SUBBUF];			      ///< Specifies the size of the allocated buffers
   };
   ULONG    pDataSize_UL[MAX_BUFFER_QUEUE_DATA_TYPES];	   ///< Specifies the video data size (use during video data transfer DMA)
   ULONG    pDataSubBufIdx_UL[MAX_BUFFER_QUEUE_DATA_TYPES];  ///< Specifies the video data size (use during video data transfer DMA)
   ULONG    NbBuffer_UL;				                     ///< Specifies the t number of the buffer to allocate and to add to the queue
   IDMABuffer::TYPE BufferType_E;
   BOOL32     EnableDPCMask_B;			                     ///< Enable DPC mask for buffer queue processing on interrupt trigger (should be TRUE)
   ULONG    IntSource_UL;				                     ///< Specifies the interrupt source among the ref INTERRUPT_SOURCE_GROUP "avilable ones"
   UBYTE    DMAQueue_UB;                                 ///< Specifies the DMA queue to use
   UBYTE    DMAChannel_UB;                               ///< Specifies the DMA channel to use
   ULONG    GroupID_UL;
   BOOL32     Disabled_B;
   ULONG    pUserParam_UL[MAX_BUFFER_QUEUE_USER_PARAM];
   ULONG    LBDataSize_UL;
};



class IBufferQueue
{

public:
   virtual ~IBufferQueue(){};

   virtual BUFFERQUEUE_CHECK_STS Check()=0;

   virtual DEPRECATED_FUNC(BUFHANDLE BufferLock(BUFFER_LOCK_TYPE LockType_E, ULONG Timeout_UL, ULONG Count_UL=0, UWORD RequesterID_UW=0xFFFF))=0; ///< DEPRECATED because it doesn't return CDRVIF_STATUS. We need to identify the error status in case of wakeup
   virtual CDRVIF_STATUS BufferUnlock(BUFHANDLE BufID_UL, UWORD RequesterID_UW=0xFFFF)=0;
   virtual CDRVIF_STATUS GetStatus(BUFFER_QUEUE_STATUS *pStatus_X)=0;

   virtual ULONG GetBufferSize(ULONG SubBufIdx_UL=0)=0;
   virtual ULONG GetBufferQueueHandle()=0;
   virtual ULONG GetBufferQueueType()=0;

   virtual BUFFER_HEADER * GetBufferHeader(BUFHANDLE BufID_UL)=0;
   virtual UBYTE * GetBufferPtr(BUFHANDLE BufHandle_UL, ULONG SubBufIdx_UL=0)=0;
   virtual UBYTE * GetDataType(BUFHANDLE BufHandle_UL, ULONG DataType_UL, ULONG *pSize_UL=NULL)=0;

   virtual CDRVIF_STATUS Reset(ULONG ResetParameter_UL)=0;													///< Performs a buffer queue reset 
   virtual CDRVIF_STATUS UpdateDisabled(BOOL32 Disabled_B)=0;
   virtual CDRVIF_STATUS UpdateUserParameter_Set(ULONG UserParameterIdx_UL, ULONG Value_UL)=0;
   virtual CDRVIF_STATUS UpdateUserParameter_SetBit(ULONG UserParameterIdx_UL, ULONG BitMask_UL)=0;
   virtual CDRVIF_STATUS UpdateUserParameter_ClrBit(ULONG UserParameterIdx_UL, ULONG BitMask_UL)=0;
   virtual CDRVIF_STATUS Link(IBufferQueue * pBufQueue_O)=0;
   virtual CDRVIF_STATUS Create(BUFFERQUEUE_PARAM *pParam_X)=0;


   virtual BUFFER_QUEUE_OPEN_PARAM * GetOpenParameters()=0;
   virtual CDRVIF_STATUS WaitOnEvent(ULONG Timeout_UL)=0;


   virtual CDRVIF_STATUS BufferLock(BUFHANDLE *pBufHandle_UL, BUFFER_LOCK_TYPE LockType_E, ULONG Timeout_UL, ULONG Count_UL=0, UWORD RequesterID_UW=0xFFFF)=0;
};


class IStreamDrvIF //: public ISkelDrvIF
{
public:

   virtual ~IStreamDrvIF(){};

   virtual STREAM_BOARD_STATE GetBoardState()=0;
   virtual STREAM_BUS_TYPE  GetBusType()=0;
   virtual CDRVIF_STATUS ValidateDriverAccess()=0;
   
   virtual CDRVIF_STATUS Board_IsOK()=0;
   
   virtual IBufferQueue * CreateBufferQueue()=0;
   virtual void  ReleaseBufferQueue(IBufferQueue *pBufQ_O)=0;

   virtual CDRVIF_STATUS BufferQueueAddBuf(const BUFFER_QUEUE_ADD_BUF_PARAM *pBufferQueueAddBufParam_X)=0;
   virtual CDRVIF_STATUS BufferQueueOpen(const BUFFER_QUEUE_OPEN_PARAM *pOpenParam_X, ULONG *pBufQueueIdx_UL)=0;
   virtual CDRVIF_STATUS BufferQueueClose(ULONG BQIdx_UL)=0;
   virtual CDRVIF_STATUS BufferQueueLink(const BUFFER_QUEUE_LINK_PARAM *pLinkParam_X)=0;
   virtual CDRVIF_STATUS BufferQueueUpdate(const BUFFER_QUEUE_UPDATE_PARAM *pUpdateParam_X)=0;
   virtual CDRVIF_STATUS BufferQueueReset(ULONG BQIdx_UL, ULONG ResetParameters_UL=0xFFFFFFFF)=0;
   virtual CDRVIF_STATUS BufferQueueGetStatus(ULONG BQIdx_UL, BUFFER_QUEUE_STATUS *pBufQueueStatus_X)=0;
   virtual CDRVIF_STATUS BufferLock(const BUFFER_USER_LOCK_PARAM *pParam_X, ULONG *pID_UL)=0;
   virtual CDRVIF_STATUS BufferUnlock(const BUFFER_USER_UNLOCK_PARAM *pUnlockParam_X)=0;

   virtual CDRVIF_STATUS AddDMARequest( DMA_REQUEST *pDMARequest_X, ULONG DMAChannel_UL)=0;
   virtual CDRVIF_STATUS AddDMARequest( IDMABuffer * pDMABuf, ULONG LocalBusAddr_UL, ULONG Flags_UL, DRVEVENT * pSynchroEvent_X, ULONG TimeoutForCompletion_UL=0, ULONG DMAQueue_UL=0, ULONG DMAChannel_UL=0 )=0;
   virtual CDRVIF_STATUS DMAQueueSetup(ULONG DMAChannel_UL, ULONG DMAQueueIdx_UL, ULONG MaxDMASize_UL, ULONG Priority_UL )=0;
   
   virtual BOOL32 StreamFeatures_Check(ULONG Features_UL)=0;
   virtual ULONG StreamFeatures_Get()=0;

   virtual CDRVIF_STATUS Board_LoadDriverProperties()=0;
};



#endif // _ISTREAMDRVIF_H_

