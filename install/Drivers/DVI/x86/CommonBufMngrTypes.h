/********************************************************************************************************************//**                                                                                                                     * @file   CommonBufMngrTypes.h
 * @date   2008/01/07
 * @Author cs
 * @brief  This file describes common buffer manager associated types used in the driver.h file to avoid  conflict between the
 *         need of these types and the need of the DEVICE_EXTENSION definition.
 *
 **********************************************************************************************************************//*

====================================================================================================================
=  Version History                                                                                                 =
====================================================================================================================
Date     	Version     	Author   Description
====================================================================================================================
2008/01/07  v00.01.0000    cs       Creation of this file

**********************************************************************************************************************/

#ifndef _COMMONBUFMNGRTYPES_H_
#define _COMMONBUFMNGRTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"
#include "DMABufMngrTypes.h"
#include "SkelIoctls.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS   4
#define COMMON_BUFFER_NBOF_POOLSIDE 2
#define COMMON_BUFFER_MAX_SLAVE                 DMABUF_MAX_NB_SLAVES



/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
   IOCTL_CB_ACCESS_GET,
   IOCTL_CB_ACCESS_RELEASE

} IOCTL_COMMON_BUFFER_ACCESS_TYPE;

typedef enum
{
   IOCTL_CBP_ACCESS_RESERVE_POOL,
   IOCTL_CBP_ACCESS_FREE_POOL,
   IOCTL_CBP_ACCESS_RESERVE_BUFFER,
} IOCTL_COMMON_BUFFERPOOL_ACCESS_TYPE;

typedef struct
{
   ULONG             SlaveBufID_UL;
   ULONG             MasterBufID_UL;
} COMMON_BUFFER_LINK;

#pragma pack(push, 4)

typedef struct
{
	ULONG             Size_UL;
	PHYSICAL_ADDRESS  PhysicalAddr_PA;
	DELTA_VOID        pAddr_UB; 
	PID_TYPE          PID_UL;
	BOOL32              Slave_B; 
	ULONG             pLinkedSlaveID_UL[COMMON_BUFFER_MAX_SLAVE]; //TODO : attention remplir avec INVLIAD_BUFFER_ID */
    ULONG             BufId_UL;
} COMMON_BUFFER_DESCR;

typedef struct
{
   ULONG                           StructSize_UL;
   IOCTL_COMMON_BUFFER_ACCESS_TYPE AccessType_E;
   union 
   {
		COMMON_BUFFER_DESCR	  Descr_X;
		BUFID				  BufID_UL;	 
   };
} IOCTL_COMMON_BUFFER_ACCESS_PARAM;


typedef struct
{
   ULONG             StructSize_UL;
   BUFID             SlaveBufID_UL;
   BUFID             MasterBufID_UL;
} IOCTL_LINK_COMMON_BUFFER_PARAM;

typedef struct 
{
   ULONG             StructSize_UL;
   ULONG             pPreallocPoolSize_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS]; ///< Sizes of the preallocated buffer pools
   ULONG             ppPreallocPoolSideSize_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS][COMMON_BUFFER_NBOF_POOLSIDE];
}IOCTL_GETPREALLOCINFO_COMMON_BUFFER_PARAM;


typedef struct 
{
   ULONG                               StructSize_UL;
   IOCTL_COMMON_BUFFERPOOL_ACCESS_TYPE AccessType_E;
   ULONG                               PoolId_UL;
   ULONG                               PoolSide_UL;
   union
   {
      struct
      {
         ULONG                            NbOfBuffers_UL;
         ULONG                            pBuffersSize_UL[COMMON_BUFFER_MAX_SLAVE+1]; 
      };
      ULONG                            BufSize_UL;
   };
}IOCTL_COMMON_BUFFERPOOL_ACCESS_PARAM;

#pragma pack(pop)

#endif // _COMMONBUFMNGRTYPES_H_

