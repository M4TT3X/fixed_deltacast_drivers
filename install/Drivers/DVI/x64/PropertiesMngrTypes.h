/********************************************************************************************************************//**
 * @file   PropertiesMngrTypes.h
 * @date   2007/12/03
 * @Author cs
 * @brief  This files defines symbols and structures exposed outside the driver.
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/12/03  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _PROPERTIESMNGRTYPES_H_
#define _PROPERTIESMNGRTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
typedef enum 
{
   IOCTL_PROPERTIES_ACCESS_TYPE_SETVALUE,
   IOCTL_PROPERTIES_ACCESS_TYPE_GETVALUE,
   IOCTL_PROPERTIES_ACCESS_TYPE_SETBIT,
   IOCTL_PROPERTIES_ACCESS_TYPE_CLRBIT,
   IOCTL_PROPERTIES_ACCESS_TYPE_RESET,
   IOCTL_PROPERTIES_ACCESS_TYPE_TEST_AND_SET,
   IOCTL_PROPERTIES_ACCESS_TYPE_TEST_AND_CLR
} IOCTL_PROPERTIES_ACCESS_TYPE;
/**********************************************************************************************************************//**
 * @struct PROPERTIES_PARAM
 * @brief  Parameters structures for IOCTL related to properties access.
 *
 * This parameters structure is used as parameters for the following IOCTLs :
 * -IOCTL_SET_PROPERTIES         
 * -IOCTL_GET_PROPERTIES         
 * -IOCTL_RESET_PROPERTIES       
 * -IOCTL_TEST_AND_SET_PROPERTIES
 * -IOCTL_TEST_AND_CLR_PROPERTIES
 *
 * Depending to the IOCTL, the Value_UL field can be an input or an output parameter.
 *************************************************************************************************************************/
#pragma pack(push, 4)
typedef struct
{
   ULONG StructSize_UL;
   IOCTL_PROPERTIES_ACCESS_TYPE AccessType_E;
	ULONG Idx_UL;     ///< Specifies the property index
	ULONG Value_UL;   ///< Used for retrieving for setting a property depending to the used IOCTL
} IOCTL_PROPERTIES_ACCESS_PARAM;
#pragma pack(pop)

#endif // _PROPERTIESMNGRTYPES_H_
