/**********************************************************************************************************************
 
   Module   : EventMngr
   File     : EventMngr.cpp
   Created  : 2006/10/18
   Author   : cs

 **********************************************************************************************************************/

/***** INCLUDES *******************************************************************************************************/

#define MODID MID_EVENTMNGR

#include "Driver.h"
#include "EventMngr.h"
#include "SkelIoctls.h"
#include "DrvDbg.h"
#include "InterlockedOp.h"


/***** EXTERNAL VARIABLES *********************************************************************************************/

/***** GLOBAL VARIABLES ***********************************************************************************************/

EVENT_ID EventMngr_Open(PDEVICE_EXTENSION pdx, ULONG PID_UL)
{
EVENT_ID Rt_e=INVALID_EVENT_ID;
ULONG EventIdx_UL=0;
   
   EnterOutput("\n");

   KOBJ_AcquireSpinLock(pdx,&pdx->EventMngrData_X.SpinLock_KO);
   
      // Find a free Event ID and mark it as used in Multi-processor protected area
      if (pdx->EventMngrData_X.EventUsed_UL!=0xFFFFFFFF)
      {
         Rt_e=0x00000001;
         while ((Rt_e) && (pdx->EventMngrData_X.EventUsed_UL&Rt_e)) {EventIdx_UL++;Rt_e<<=1;} 
         pdx->EventMngrData_X.EventUsed_UL|=Rt_e;
         pdx->EventMngrData_X.pPID_UL[(EventIdx_UL>=MAX_EVENT_ID)?(MAX_EVENT_ID-1):EventIdx_UL]=PID_UL;
      }
      //else ErrOutput( "Unable to find a free Event ID\n");

   KOBJ_ReleaseSpinLock(pdx,&pdx->EventMngrData_X.SpinLock_KO);

   if (Rt_e!=INVALID_EVENT_ID)
   {
      KOBJ_InitializeSyncEvent(pdx,&pdx->EventMngrData_X.pEventTable_KO[EventIdx_UL],FALSE);
   }

   ExitOutput( "(EventID:%08x)\n",(ULONG)Rt_e);

   return (Rt_e);

}

void EventMngr_OSDeviceAdd(PDEVICE_EXTENSION pdx)
{

}
