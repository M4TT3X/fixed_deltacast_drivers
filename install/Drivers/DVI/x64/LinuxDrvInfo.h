#define BOARD_PCI_VENDOR_ID      0x1b66
#define BOARD_PCI_DEVICE_ID      0xADDB

#define BOARD_PCI_INFO           {{PCI_DEVICE(BOARD_PCI_VENDOR_ID, BOARD_PCI_DEVICE_ID),},{0,}}

#define DRIVERNAME               "delta-dvi"
#define DRIVERSHORTNAME          "delta-dvi"
#define DRIVERID                 DRIVERNAME               
