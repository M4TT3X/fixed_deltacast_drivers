/********************************************************************************************************************//**
 * @internal
 * @file   	DVIDrv_RegistersMap.h
 * @date   	2010/09/20
 * @author 	cs
 * @version v00.01.0000 
 * @brief   This file describes the Registers mapping for the DVI board.
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/09/20  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _DVIDRV_REGISTERSMAP_H_
#define _DVIDRV_REGISTERSMAP_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "Register.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define DVI_DMA_PADDING_SIZE 0x200

#define CFGPLD_BASE           0x100
#define CFGPLD_PCI_BAR        0
#define FPGA_PCI_BAR          1

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/*********************************************************************************************************************//**
 * @internal
 * @enum  
 * @brief This enumeration contains symbols that defines all needed shadow registers
 * @remark  The names in the enumeration must match with the registers definition (defined by the DEFINE_SHRMMREG32 macro)
 *************************************************************************************************************************/
typedef enum
{
   IDX_FPGA_IMR_SHRMM32,
   IDX_FPGA_GCMD_SHRMM32,
   IDX_FPGA_RX0_SHRMM32,
   IDX_FPGA_RX1_SHRMM32,
   IDX_NB
} HDDRV_SHREG32;

/***** REGISTERS MAPPING DEFINITION FOR CPLD **************************************************************************/

#define CFGPLD_CMD_MM32          DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+0x00)
#define CFGPLD_STS_MM32          DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+0x00)
#define CFGPLD_DATA_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+0x08)
#define CFGPLD_REV0_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+ 0x0020)    ///<PLD revision id: Version
#define CFGPLD_REV1_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+ 0x0028)    ///<PLD revision id: Year
#define CFGPLD_REV2_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+ 0x0030)    ///<PLD revision id: Month
#define CFGPLD_REV3_MM32         DEFINE_MMREG32(CFGPLD_PCI_BAR,CFGPLD_BASE+ 0x0038)    ///<PLD revision id: Day

/***** REGISTERS MAPPING DEFINITION FOR FPGA **************************************************************************/

#define FPGA_GCMD_SHRMM32        DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x00) /* Global Command Register */
#define FPGA_GSTS_MM32           DEFINE_MMREG32(FPGA_PCI_BAR,0x00) /* Global Status register */
#define FPGA_RX0_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x08) /* RX0 Command */ 
#define FPGA_RX1_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x10) /* RX1 Command */
#define FPGA_IMR_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x28) /* Interrupt Mask Register */
#define FPGA_SR_MM32             DEFINE_MMREG32(FPGA_PCI_BAR,0x28) /* Status Register */
#define FPGA_ICR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x30) /* Interrupt Clear Register */
#define FPGA_ISR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x30) /* Interrupt Status Register */
#define FPGA_GSPI_MM32           DEFINE_MMREG32(FPGA_PCI_BAR,0x38) /*  */
#define FPGA_PARAM_ROM_A_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x40) /* Frame buffers start address for the RX channels. Indirect ... */
#define FPGA_PARAM_ROM_D_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x48) /* ... addressing.(Offsets 0x40 & 0x80) */
#define FPGA_BRANDING_CHG_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x50) /* Challenge Number used for branding check */
#define FPGA_BRANDING_RLT_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x50) /* Result used for branding check */
#define FPGA_SYSMON_CMD_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x58) /* System Monitor Command */
#define FPGA_SYSMON_STS_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x58) /* System Monitor Status */
#define FPGA_DMA_BC_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x60) /* DMA transfer count (in bytes). The transfer size of the DMAshould be programmed before it is started. It is used to generate an interrupt once the number of bytes transferred reaches the DMA_BC value. */
#define FPGA_DMA_A_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x68) /* 32bit sdram address bit 31..30 are used to select the channel: �01� for RX1 - �00� for RX0  */
#define FPGA_END_OF_DMA_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x70) /* For each channel, set the end of a dma bit to 1 once a FRAME has been transferred. This bit is autocleared.  */
#define FPGA_WDT_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x78) /* Watchdog timer register :  time-out value  (133 Mhz clock)*/
#define FPGA_WDC_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x80) /* Watchdog control register : WACT : 0x57414354 - WARM : 0x5741524D - WDIS : 0x57444953 */
#define FPGA_RX0_STD_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x78) /* RX0 Standard identification (Read Only - PCIe Only)*/
#define FPGA_RX1_STD_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x80) /* RX1 Standard identification (Read Only - PCIe Only) */
#define FPGA_MAILBOX_DATA_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x98) /* 32 bit Mailbox data */
#define FPGA_MAILBOX_ADDR_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0xA0) /* Mailbox address */
#define FPGA_UART_CTRL_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0xA8) /* UART command and data register */
#define FPGA_UART_STS_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0xA8) /* UART status register */
#define FPGA_UART_DATA_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0xB0) /* [7..0] = UART data */
#define FPGA_RXBUFID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xB8) /* [2..0] = Index of last RX0 buffer filled 
                                                                      [3] = �1� if last RX0 buffer filled is corrupted 
                                                                      [6..4] = Index of last RX1 buffer filled 
                                                                      [7] = �1� if last RX1 buffer filled is corrupted  */
#define FPGA_RX0_H_CROP_WIN_MM32 DEFINE_MMREG32(FPGA_PCI_BAR,0xC0) /* RX0 horizontal cropping window 
                                                                      [15..0] = Index of first pixel captured in the line 
                                                                      [31..16] = Index of last pixel captured in the line 
                                                                      [(stop index � start index) + 1] must be even !! */
#define FPGA_RX0_V_CROP_WIN_MM32 DEFINE_MMREG32(FPGA_PCI_BAR,0xC8) /* RX0 vertical cropping window 
                                                                      [15..0] = Index of first line captured in the field 
                                                                      [31..16] = Index of last line captured in the field */
#define FPGA_RX1_H_CROP_WIN_MM32 DEFINE_MMREG32(FPGA_PCI_BAR,0xD0) /* RX0 horizontal cropping window 
                                                                      [15..0] = Index of first pixel captured in the line 
                                                                      [31..16] = Index of last pixel captured in the line 
                                                                      [(stop index � start index) + 1] must be even !! */
#define FPGA_RX1_V_CROP_WIN_MM32 DEFINE_MMREG32(FPGA_PCI_BAR,0xD8) /* RX0 vertical cropping window 
                                                                      [15..0] = Index of first line captured in the field 
                                                                      [31..16] = Index of last line captured in the field */
#define FPGA_SSR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0xE0) /* Set SR register. This register is used to generate interruption manually. 
                                                                      Writing a bit to 1 will set the corresponding bit in the SR register, and
                                                                      generate an interrupt if enabled in IMR. This register is autocleared. */
#define FPGA_BTC_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0xF0) /* Board Timecode */
#define FPGA_REVID_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xF8) /* Year month day version */
#define FPGA_I2CIDR_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x100) /* I2C data register */
#define FPGA_I2CSR_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x108) /* I2C control/status register */
#define FPGA_FWSPI_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x110) /* Firmware flash SPI Interface Bridge */
#define FPGA_TEST_MM32           DEFINE_MMREG32(FPGA_PCI_BAR,0x118) /* BAR 1 Test Register */
#define FPGA_DQS_DLY_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x128) /* RESERVED (DQ-DQS tap delay value) */



// -- CFGPLD_REV1_MM8 : CPLD REV1 (RO) ------------------------------------------------------------------
#define CFGPLD_REV1_BIT_DUAL     0x20                       /* '1' indicates Dual Link Support */


// -- FPGA_GCMD_MM32 : General Command Register (WO) ------------------------------------------------------------------
// General board status and configuration register.
#define PCIE_LANE_NB_UNKNOWN        0x0   // always this value on older firmware than 0901120
#define PCIE_LANE_NB_1              0x1
#define PCIE_LANE_NB_2              0x2
#define PCIE_LANE_NB_4              0x3

#define FPGA_GCMD_SDRAM_RST                           0x80000000 /* Set �1� to reset SD manager and SD controller. */
#define FPGA_GCMD_SYS_DCM_LOCKED                      0x80000000 /* �1� when FPGA 133 MHz DCM is locked */
#define FPGA_GCMD_BOARD_RST                           0x40000000 /* Set �1� to force new FPGA programming without PC shutdown (DANGER) */
#define FPGA_GCMD_ARM_RST_N                           0x20000000 /* Set �0� to reset ARM �C */
#define FPGA_GCMD_READ_BACK_CPLD                      0x10000000 /* Write �1� to read back CPLD rev ID one more time (must be manually cleared) */
#define FPGA_GCMD_BIT_FE1_RST                         0x08000000 /* Set �1� to reset Analog DVI Front End 1 */ 
#define FPGA_GCMD_BIT_FE0_RST                         0x04000000 /* Set �1� to reset Analog DVI Front End 0 */
#define FPGA_GCMD_MSK_ARM_BOOT                        0x03000000 /* �x0�: main flash memory is selected as boot space 
                                                                    �01�: system memory is selected as boot space 
                                                                    �11�: embedded SRAM is selected as boot space */ 
#define FPGA_GCMD_MSK_PCIE_LANE_NB                    0x00300000 /* �01� for 1 lane, �10� for 2 lane, �11� for 4 lane negotiated */ 
#define FPGA_GCMD_BIT_DD_READY_STS                    0x00000001 /* This bit is high when the SDRAM controller is ready (Read Only) */ 
#define FPGA_GCMD_BIT_FW_WARM_LOAD                    0x00000001 /* Set this bit to �1� to active loading of new FPGA firmware 
                                                                    during next PCIe reset event.  (Write Only) */

#define FPGA_GCMD_BIT_ARM_BOOT(boot_space)                ((boot_space<<24)&FPGA_GCMD_MSK_ARM_BOOT)
#define FPGA_GCMD_GET_BIT_PCIE_LANE_NB(reg)               ((reg&FPGA_GCMD_MSK_PCIE_LANE_NB) >>20)

#define ARM_BOOT_FLASH                 0x0 /* main flash memory is selected as boot space */ 
#define ARM_BOOT_ROM                   0x1 /* system memory is selected as boot space  */ 
#define ARM_BOOT_RAM                   0x2 /* embedded SRAM is selected as boot space */ 

// -- FPGA_PARAM_ROM_A_MM32 : Base Addresses configurator Indirext access address (W)----------------------------------
// This register is uses with the FPGA_PARAM_ROM_D_MM32 (data) register to configure the on board base addresses for all
// storage area
#define FPGA_PARAM_ROM_A_VALUE_RXx_VID(ChannelIdx, BufIdx)  (0x40 + (ChannelIdx*0x18) + BufIdx)

// -- FPGA_GSPI_MM32 : General SPI Register (RW) ----------------------------------------------------------------------
// This register allows to write or read 16-bit data on SPI bus. For a write access, write the 32-bit correctly formatted
// with FPGA_GSPI_BIT_ACCESS_WR set. For read access, write a 32-bit data in the register with dummy data in data field and
// the ACCESS bit set to RD. After that, perform a read access to this register to retrieve the read data.

#define FPGA_GSPI_MSK_DATA                            0xFFFF0000
#define FPGA_GSPI_BIT_ACCESS_WR                       0x00008000
#define FPGA_GSPI_BIT_ACCESS_RD                       0x00000000
#define FPGA_GSPI_MSK_CHIP_SELECT                     0x00007000
#define FPGA_GSPI_MSK_REG_ADDR                        0x00000FFF


#define FPGA_GSPI_BIT_CHIP_SELECT_x(chip_select)      ((chip_select<<12)&FPGA_GSPI_MSK_CHIP_SELECT)


// -- FPGA_RX0_MM32 : RX0 Cmd/Sts Register (WR) -----------------------------------------------------------------------
// -- FPGA_RX1_MM32 : RX1 Cmd/Sts Register (WR) -----------------------------------------------------------------------

#define FPGA_RXx_BIT_CAPTURE_ENABLE    0x00000001     /* Set this bit to start capture */
#define FPGA_RXx_BIT_PACKING444        0x00000002     /* �0� = 4:2:2 - �1� = 4:4:4  */
#define FPGA_RXx_BIT_PROGRESSIVE       0x00000004     /* �0� = Interlaced - �1� = Progressive */
#define FPGA_RXx_BIT_422DECIMATE       0x00000008     /* �0� = Normal mode - �1� = Decimation 4:4:4 to 4:2:2 made in FPGA. 
                                                          For a normal behaviour, Front End must be configured to provide 
                                                          4:4:4 data to FPGA. If this bit is set, packing bit is ignored! */
#define FPGA_RXx_BIT_NO_SWITCH_CHROMA  0x00000010     /* �0� = Packing with switched Cr/Cb - �1� = Normal Packing */
#define FPGA_RXx_BIT_FRAME_DEC_ENABLE  0x00000020     /* �0� = No frame decimation - �1� = Activate frame decimation */
#define FPGA_RXx_BIT_WIN_CROP_ENABLE   0x00000040     /* �0� = Capture all field - �1� = Capture window delimited by user. Delimitation of this 
                                                          window is defined by RXi_HOR_CROPPING_WINDOWS and RXi_VER_CROPPING_WINDOWS registers */
#define FPGA_RXx_BIT_DUAL_LINK_ENABLE  0x00000080     /* �0� = Dual Link disabled - �1� = Dual Link enabled (Relevant only on input RX0) */
#define FPGA_RXx_BIT_DISABLE_ALPHA     0x00000100     /* '0' = Capture 4:4:4 with alpha channel (Packing 4:4:4:4) - '1' = Capture 4:4:4 without alpha channel (Packing 4:4:4:0) */
#define FPGA_RXx_MSK_FRAME_DEC_DEN     0x00FF0000     /* Denominator of frame rate ratio. Must be bigger than numerator. */
#define FPGA_RXx_MSK_FRAME_DEC_NUM     0xFF000000     /* Numerator of frame rate ratio. Must be smaller than denominator. */

#define FPGA_RXx_BIT_FRAME_DEC_DEN(denominator)     ((denominator<<16) & FPGA_RXx_MSK_FRAME_DEC_DEN)
#define FPGA_RXx_GET_BIT_FRAME_DEC_DEN(reg)         ((reg& FPGA_RXx_MSK_FRAME_DEC_DEN)>>16)

#define FPGA_RXx_BIT_FRAME_DEC_NUM(numerator)       ((numerator<<24) & FPGA_RXx_MSK_FRAME_DEC_NUM)
#define FPGA_RXx_GET_BIT_FRAME_DEC_NUM(reg)         ((reg& FPGA_RXx_MSK_FRAME_DEC_NUM)>>24)


// -- FPGA_RXx_x_CROP_WIN_MM32 : RXx horizontal/vertical cropping window register (W) --------------------
#define FPGA_RXx_x_CROP_WIN_MSK_START     0x0000FFFF     /* Index of first pixel captured in the line (first line captured in the field)  */
#define FPGA_RXx_x_CROP_WIN_MSK_STOP      0xFFFF0000     /* Index of last pixel captured in the line (last line captured in the field)  */

#define FPGA_RXx_BIT_MSK_START(start)           ((start) & FPGA_RXx_x_CROP_WIN_MSK_START)
#define FPGA_RXx_GET_BIT_MSK_START(reg)         ((reg& FPGA_RXx_x_CROP_WIN_MSK_START))

#define FPGA_RXx_BIT_MSK_STOP(stop)             ((stop<<16) & FPGA_RXx_x_CROP_WIN_MSK_STOP)
#define FPGA_RXx_GET_BIT_MSK_STOP(reg)          ((reg& FPGA_RXx_x_CROP_WIN_MSK_STOP)>>16)



#define RX0 0
#define RX1 1
#define FPGA_RXx_MM32(Channel)         FPGA_RX##Channel##_MM32



// -- FPGA_ISR_MM32 : Interrupt Statue Register (RO) ------------------------------------------------------------------
#define FPGA_ISR_BIT_MB_TX_EMPTY       0x00000001 /* Mailbox TX empty / End of �C Command */
#define FPGA_ISR_BIT_MB_RX_FULL        0x00000002 /* Mailbox RX full */
#define FPGA_ISR_BIT_RX0_LOST          0x00000004 /* Lost of locked on RX0 channel (Not yet implemented) */
#define FPGA_ISR_BIT_RX0               0x00000008 /* Frame buffer ready */
#define FPGA_ISR_BIT_RX1_LOST          0x00000040 /* Lost of locked on RX1 channel (Not yet implemented) */
#define FPGA_ISR_BIT_RX1               0x00000080 /* Frame buffer ready */
#define FPGA_ISR_BIT_UART_TX_EMPTY     0x00000100 /* UART TX register empty */
#define FPGA_ISR_BIT_UART_RX_FULL      0x00000200 /* UART RX register full */
#define FPGA_ISR_BIT_RX0_OVERRUN       0x00000400 /* RX0 overrun */
#define FPGA_ISR_BIT_RX1_OVERRUN       0x00000800 /* RX1 overrun */
#define FPGA_ISR_BIT_EOD               0x00010000 /* Interrupt generated when the DMA byte count has been reached. */     
#define FPGA_ISR_BIT_WDTO              0x10000000 /* Watchdog time out detected */    
#define FPGA_ISR_BIT_WDEN              0x20000000 /* Watchdog enabled */     


// -- FPGA_IMR_MM32 : Interrupt Mask Register (WO) --------------------------------------------------------------------
#define FPGA_IMR_BIT_MB_TX_EMPTY       FPGA_ISR_BIT_MB_TX_EMPTY     
#define FPGA_IMR_BIT_MB_RX_FULL        FPGA_ISR_BIT_MB_RX_FULL      
#define FPGA_IMR_BIT_RX0_LOST          FPGA_ISR_BIT_RX0_LOST        
#define FPGA_IMR_BIT_RX0               FPGA_ISR_BIT_RX0             
#define FPGA_IMR_BIT_RX1_LOST          FPGA_ISR_BIT_RX1_LOST        
#define FPGA_IMR_BIT_RX1               FPGA_ISR_BIT_RX1             
#define FPGA_IMR_BIT_UART_TX_EMPTY     FPGA_ISR_BIT_UART_TX_EMPTY   
#define FPGA_IMR_BIT_UART_RX_FULL      FPGA_ISR_BIT_UART_RX_FULL    
#define FPGA_IMR_BIT_RX0_OVERRUN       FPGA_ISR_BIT_RX0_OVERRUN    
#define FPGA_IMR_BIT_RX1_OVERRUN       FPGA_ISR_BIT_RX1_OVERRUN    
#define FPGA_IMR_BIT_EOD               FPGA_ISR_BIT_EOD             
#define FPGA_IMR_BIT_WDTO              FPGA_ISR_BIT_WDTO            
#define FPGA_IMR_BIT_WDEN              FPGA_ISR_BIT_WDEN            


// -- FPGA_SSR_MM32 : Manual Set of Interrupt Status (WO) -------------------------------------------------------------
// This register is used to manually set an interrupt. This is useful to simulate TX interrupt during pre-loading 
// phase. The bits are auto-clear.
#define FPGA_SSR_BIT_MB_TX_EMPTY       FPGA_ISR_BIT_MB_TX_EMPTY     
#define FPGA_SSR_BIT_MB_RX_FULL        FPGA_ISR_BIT_MB_RX_FULL      
#define FPGA_SSR_BIT_RX0_LOST          FPGA_ISR_BIT_RX0_LOST        
#define FPGA_SSR_BIT_RX0               FPGA_ISR_BIT_RX0             
#define FPGA_SSR_BIT_RX1_LOST          FPGA_ISR_BIT_RX1_LOST        
#define FPGA_SSR_BIT_RX1               FPGA_ISR_BIT_RX1             
#define FPGA_SSR_BIT_UART_TX_EMPTY     FPGA_ISR_BIT_UART_TX_EMPTY   
#define FPGA_SSR_BIT_UART_RX_FULL      FPGA_ISR_BIT_UART_RX_FULL    
#define FPGA_SSR_BIT_EOD               FPGA_ISR_BIT_EOD             
#define FPGA_SSR_BIT_WDTO              FPGA_ISR_BIT_WDTO            
#define FPGA_SSR_BIT_WDEN              FPGA_ISR_BIT_WDEN            



// -- FPGA_FWSPI_MM32 : SPI Firmware PROM bridge (RW) -----------------------------------------------------------
#define FPGA_FWSPI_BIT_CS              0x000001    /* CS# of the SPI PROM */
#define FPGA_FWSPI_BIT_RESET           0x000002    /* Auto-clear Active HIGH Reset */
#define FPGA_FWSPI_MSK_DOUT            0x00FF00    /* Data to shift to the PROM (Write Only) */
#define FPGA_FWSPI_MSK_DIN             0x0000FF    /* Shifted in data from the PROM (Read Only) */
#define FPGA_FWSPI_BIT_DOUT(val)       (((val)<<8)&FPGA_FWSPI_MSK_DOUT)
#define FPGA_FWSPI_GET_DIN()           ((FPGA_FWSPI_MM32&FPGA_FWSPI_MSK_DIN))
#define FPGA_FWSPI_BIT_BUSY            0x000100    /* Busy bit for FPGA SPI Access */


// -- FPGA_END_OF_DMA_MM32 : End of DMA (WO) -------------------------------------------------------------
#define FPGA_EOD_RX0                    0x00000001
#define FPGA_EOD_RX1                    0x00000002


// -- FPGA_WDC_MM32 : Watchdog Command register -----------------------------------------------------------------------------/
/* This register controls the Watchdog features. Through this register, Watchdog can be Enabled, Disabled and Re-armed, just
by writing one of these pattern values .*/
#define FPGA_WDC_VAL_ENABLE                  0x57414354
#define FPGA_WDC_VAL_ARM                     0x5741524D
#define FPGA_WDC_VAL_DISABLE                 0x57444953





//MAILBOX_DATA indirect address
//Status
#define MB_CMD_STS              0x000
#define MB_RX0_STS0             0x001
#define MB_RX0_STS1             (MB_RX0_STS0+1)
#define MB_RX0_STS2             (MB_RX0_STS0+2)
#define MB_RX0_STS3             (MB_RX0_STS0+3)
#define MB_RX0_STS4             (MB_RX0_STS0+4)
#define MB_RX0_STS5             (MB_RX0_STS0+5)
#define MB_RX0_STS6             (MB_RX0_STS0+6)
#define MB_RX1_STS0             0x011
#define MB_RX1_STS1             (MB_RX1_STS0+1)
#define MB_RX1_STS2             (MB_RX1_STS0+2)
#define MB_RX1_STS3             (MB_RX1_STS0+3)
#define MB_RX1_STS4             (MB_RX1_STS0+4)
#define MB_RX1_STS5             (MB_RX1_STS0+5)
#define MB_RX1_STS6             (MB_RX1_STS0+6)

//Cmd rtrn
#define MB_CMD_RTRN             0x021

//Cmd
#define MB_CMD_TAG              0x200
#define MB_CMD_PARAM            0x201

//Sysmon Cmd
#define MB_SYSMON_CMD_READ_CURRENT_TEMP   0x00000000
#define MB_SYSMON_CMD_READ_MAX_TEMP       0x00200000
#define MB_SYSMON_CMD_READ_MIN_TEMP       0x00240000
#define MB_SYSMON_CMD_RESET               0x80000000

//Sysmon Sts
#define MB_SYSMON_STS_BIT_BUSY            0x80000000
#define MB_SYSMON_STS_MSK_DATA            0x0000ffff

#define MB_SYSMON_STS_GET_BIT_DATA(reg)   (((reg)&MB_SYSMON_STS_MSK_DATA)>>6)


// -- MB_CMD_STS : Command Status -----------------------------------------------------------------------------/
#define MB_CMD_STS_BIT_DONE                    0x00000001           /* �1� = Command done */
#define MB_CMD_STS_BIT_NOTDEFINED              0x00000002           /* �1� = Command not defined */
#define MB_CMD_STS_BIT_ERROR                   0x00000004           /* �1� = Error occurs during execution */
#define MB_CMD_STS_BIT_DATA                    0x00000008           /* �1� = Data are available (end of command which need answer) -> Data can be read at indirect address 0x21 */
#define MB_CMD_STS_BIT_WRONG_NBOF_PARAM        0x00000010           /* �1� = Number of parameters is wrong */
#define MB_CMD_STS_MSK_CMD_TAG                 0xFFFF0000           /* TAG of command concerned by status */

#define MB_CMD_STS_BIT_CMD_TAG(cmd_tag)        (((cmd_tag)<<16) & MB_CMD_STS_MSK_CMD_TAG)
#define MB_CMD_STS_GET_BIT_CMD_TAG(reg)        ((reg& MB_CMD_STS_MSK_CMD_TAG) >> 16)


// -- MB_RXx_STS0 : Front end  status0 -----------------------------------------------------------------------------/
#define MB_RXx_STS0_BIT_HDMI_CLK_DETECTED      0x00000001           /* '1' = HDMI clock detected */
#define MB_RXx_STS0_BIT_HDMI_CLK_LOCKED        0x00000002           /* '1' = HDMI clock locked */
#define MB_RXx_STS0_BIT_HDMI_HFILTER_LOCKED    0x00000004           /* '1' = HDMI horizontal filter locked */
#define MB_RXx_STS0_BIT_HDMI_VFILTER_LOCKED    0x00000008           /* '1' = HDMI vertical filter locked */
#define MB_RXx_STS0_BIT_STD_DETECTED           0x00000010           /* '1' = Video Standard identified */
#define MB_RXx_STS0_BIT_TLLC_LOCKED            0x00000020           /* '1' = LLC PLL Locked */
#define MB_RXx_STS0_BIT_FREERUN                0x00000040           /* '1' = Component Processor in Free Run Mode */
#define MB_RXx_STS0_BIT_VIDEO_LOCKED           0x00000080           /* '1' = Valid Video Data (Main global status) */
#define MB_RXx_STS0_BIT_VIDEO_INTERLACED       0x00000100           /* �0� = Input video with progressive timing - �1� = Input video with interlaced timing  */
#define MB_RXx_STS0_BIT_DUAL                   0x00002000           /* '1' = Dual Link Detected */
#define MB_RXx_STS0_BIT_HDMI_MODE              0x00000200           /* '0' = DVI
                                                                       '1' = HDMI */
#define MB_RXx_STS0_BIT_SSPDVALID              0x00000400           /* '1' = Synchronization Source and Polarity Validated */
#define MB_RXx_STS0_MSK_SYNCSOURCE             0x00001800           /* '00' = Invalid 
                                                                       '01' = External HS & VS � VGA 
                                                                       '10' = External CS/HS � Composite
                                                                       '11' = Embedded (SOY/SOG) � Component */
#define MB_RXx_STS0_GET_BIT_SYNCSOURCE(reg)    ((reg&MB_RXx_STS0_MSK_SYNCSOURCE)>>11)

// -- MB_RXx_STS1 : Front end status1 -----------------------------------------------------------------------------/
#define MB_RXx_STS1_MSK_MEASURED_PXL_CLK       0x000000FF           /* Only valid if HDMIClkLocked bit is set. To obtain pixel 
                                                                       clock in MHz, use this: Freq (MHz) = (28,636363 / 27) * Measurement value 
                                                                       (Attention: Relevant information only in case of working with digital capture)  */
#define MB_RXx_STS1_MSK_ACT_LINE_WIDTH          0xFFFF0000          /* Number of active pixels per line (Attention: Relevant 
                                                                       information only in case of working with digital capture)  */
#define MB_RXx_STS1_BIT_MEASURED_PXL_CLK(pxl_clk)     ((pxl_clk) & MB_RXx_STS1_MSK_MEASURED_PXL_CLK)
#define MB_RXx_STS1_GET_BIT_MEASURED_PXL_CLK(reg)     ((reg& MB_RXx_STS1_MSK_MEASURED_PXL_CLK))
#define MB_RXx_STS1_BIT_ACT_LINE_WIDTH(width)         ((width<<16) & MB_RXx_STS1_MSK_ACT_LINE_WIDTH)
#define MB_RXx_STS1_GET_BIT_ACT_LINE_WIDTH(reg)       ((reg& MB_RXx_STS1_MSK_ACT_LINE_WIDTH) >> 16)

// -- MB_RXx_STS2 : Front end status2 -----------------------------------------------------------------------------/
#define MB_RXx_STS2_MSK_ACT_LINE_FIELD0                 0x0000FFFF   /* Number of active lines in field 0 (Attention: Relevant 
                                                                     information only in case of working with digital capture) */
#define MB_RXx_STS2_MSK_ACT_LINE_FIELD1                 0xFFFF0000   /* Number of active lines in field 1 (Attention: Relevant 
                                                                     information only in case of working with digital capture) */
#define MB_RXx_STS2_BIT_ACT_LINE_FIELD0(field0)         ((field0) & MB_RXx_STS2_MSK_ACT_LINE_FIELD0)
#define MB_RXx_STS2_GET_BIT_ACT_LINE_FIELD0(reg)        ((reg& MB_RXx_STS2_MSK_ACT_LINE_FIELD0))
#define MB_RXx_STS2_BIT_ACT_LINE_FIELD1(field1)         ((field1<<16) & MB_RXx_STS2_MSK_ACT_LINE_FIELD1)
#define MB_RXx_STS2_GET_BIT_ACT_LINE_FIELD1(reg)        ((reg& MB_RXx_STS2_MSK_ACT_LINE_FIELD1) >> 16)

// -- MB_RXx_STS3 : Front end status3 -----------------------------------------------------------------------------/
#define MB_RXx_STS3_MSK_TOT_LINE_FIELD0                 0x0000FFFF   /* Number of total lines in field 0 (Attention: Relevant 
                                                                        information only in case of working with digital capture) */
#define MB_RXx_STS3_MSK_TOT_LINE_FIELD1                 0xFFFF0000   /* Number of total lines in field 1 (Attention: Relevant 
                                                                        information only in case of working with digital capture) */
#define MB_RXx_STS3_BIT_TOT_LINE_FIELD0(field0)         ((field0) & MB_RXx_STS3_MSK_TOT_LINE_FIELD0)
#define MB_RXx_STS3_GET_BIT_TOT_LINE_FIELD0(reg)        ((reg& MB_RXx_STS3_MSK_TOT_LINE_FIELD0))
#define MB_RXx_STS3_BIT_TOT_LINE_FIELD1(field1)         ((field1<<16) & MB_RXx_STS3_MSK_TOT_LINE_FIELD1)
#define MB_RXx_STS3_GET_BIT_TOT_LINE_FIELD1(reg)        ((reg& MB_RXx_STS3_MSK_TOT_LINE_FIELD1) >> 16)

// -- MB_RXx_STS4 : Front end status4 -----------------------------------------------------------------------------/
#define MB_RXx_STS4_MSK_TOT_LINE_WIDTH             0x0000FFFF       /* Number of pixels per line (Attention: Relevant information 
                                                                       only in case of working with digital capture) */
#define MB_RXx_STS4_MSK_IMPLEMENTED_CSC            0xFFFF0000       /* Implemeneted color space converter */

#define MB_RXx_STS4_BIT_TOT_LINE_WIDTH(tot_line)  ((tot_line) & MB_RXx_STS4_MSK_TOT_LINE_WIDTH)
#define MB_RXx_STS4_GET_BIT_TOT_LINE_WIDTH(reg)   ((reg& MB_RXx_STS4_MSK_TOT_LINE_WIDTH))
#define MB_RXx_STS4_BIT_IMPLEMENTED_CSC(csc)       ((csc<<16) & MB_RXx_STS4_MSK_IMPLEMENTED_CSC)
#define MB_RXx_STS4_GET_BIT_IMPLEMENTED_CSC(reg)   ((reg& MB_RXx_STS4_MSK_IMPLEMENTED_CSC)>>16)

// -- MB_RXx_STS5 : Front end status5 -----------------------------------------------------------------------------/
#define MB_RXx_STS5_BIT_HSPOL       0x00000001  /* '1' = Positive
                                                   '0' = Negative */
#define MB_RXx_STS5_BIT_VSPOL       0x00000002  /* '1' = Positive
                                                   '0' = Negative */
#define MB_RXx_STS5_MSK_VSLENGTH    0x0000007c  /* Number of lines within a vertical synchronization period */
#define MB_RXx_STS5_MSK_8LINELENGTH   0xffff0000  /* Number of 28.63636 MHz (Xtal frequency) cycles in a block of eight lines of incoming video */

#define MB_RXx_STS5_GET_BIT_VSLENGTH(reg)    ((reg&MB_RXx_STS5_MSK_VSLENGTH)>>2)
#define MB_RXx_STS5_GET_BIT_8LINESLENGTH(reg)  ((reg&MB_RXx_STS5_MSK_8LINELENGTH)>>16) 

// -- MB_RXx_STS6 : Front end status6 -----------------------------------------------------------------------------/
#define MB_RXx_STS6_MSK_FIELDLENGTHL   0x0000ffff  /* Number of lines between two Vsyncs (= 1 field) */
#define MB_RXx_STS6_MSK_FIELDLENGTHS   0xffff0000  /* Number of Xtal (28.63636 MHz) clocks in 1/256th of a field */

#define MB_RXx_STS6_GET_BIT_FIELDLENGTHL(reg)  (reg&MB_RXx_STS6_MSK_FIELDLENGTHL)
#define MB_RXx_STS6_GET_BIT_FIELDLENGTHS(reg)  ((reg&MB_RXx_STS6_MSK_FIELDLENGTHS)>>8)


#define CSC_YPbPr601ToRGB                      0x0001
#define CSC_YPbPr709ToRGB                      0x0003
#define CSC_RGBToYPbPr601                      0x0005
#define CSC_RGBToYPbPr709                      0x0007
#define CSC_YPbPr601ToYPbPr709                 0x0009
#define CSC_YPbPr709ToYPbPr601                 0x0010


//Cmd Tag
#define MB_GET_REVID               0x00000000   /* Revision ID of ARM Firmware �yymmddvv- */
#define MB_GET_SSN                 0x00000001   /* Unique 96 bits Serial Number of ARM */
#define MB_RST_FE0                 0x00000002   /* Start Reset sequence for Front End 0 */
#define MB_RST_FE1                 0x00000003   /* Start Reset sequence for Front End 1 */
#define MB_PROGRAM_EDID0           0x00800004   /* Program EDID for Front End 0. Must be done before start capture on RX0. */
#define MB_PROGRAM_EDID1           0x00800005   /* Program EDID for Front End 1. Must be done before start capture on RX1. */
#define MB_START_RX0               0x000C0006   /* Configure Front End 0 for video capture */
#define MB_START_RX1               0x000C0007   /* Configure Front End 1 for video capture */
#define MB_STOP_RX0                0x00000008   /* Stop capture with Front End 0 */
#define MB_STOP_RX1                0x00000009   /* Stop capture with Front End 1 */
#define MB_COLOR_CTRL0             0x0004000A   /* Control color correction on Front End 0 */
#define MB_COLOR_CTRL1             0x0004000B   /* Control color correction on Front End 1 */
#define MB_H_ADJ_RX0               0x00010010   /* Adjust horizontal position of RX0 */
#define MB_V_ADJ_RX0               0x00010011   /* Adjust vertical position of RX0 */
#define MB_H_ADJ_RX1               0x00010012   /* Adjust horizontal position of RX1 */
#define MB_V_ADJ_RX1               0x00010013   /* Adjust vertical position of RX1 */
#define MB_PHASE_RX0               0x00010014   /* Adjust analog sampling phase of RX0 */
#define MB_PHASE_RX1               0x00010015   /* Adjust analog sampling phase of RX0 */
#define MB_READ_EDID0              0x00000016   /* Read EDID from Front End 0 */
#define MB_READ_EDID1              0x00000017   /* Read EDID from Front End 1 */
#define MB_READ_CFG_EDID0          0x0000001C   /* Read Configuration of E-EDID PROM of RX0 */
#define MB_READ_CFG_EDID1          0x0000001D   /* Read Configuration of E-EDID PROM of RX1 */
#define MB_CONFIG_EDID0            0x0001001A   /* Configuration of E-EDID PROM of RX0 */
#define MB_CONFIG_EDID1            0x0001001B   /* Configuration of E-EDID PROM of RX1 */

#define MB_CMDTAG_MSK_ID           0x0000FFFF
#define MB_CMDTAG_BIT_MSK_ID(cmd_id)      ((cmd_id) & MB_CMDTAG_MSK_ID)
#define MB_CMDTAG_GET_BIT_MSK_ID(reg)     (((reg)&MB_CMDTAG_MSK_ID))



// -- MB_START_RXx : Configure Front End 0 for video capture  -----------------------------------------------------/
#define MB_START_RXx_PARAM0_MSK_PRIMARY_MODE        0x0000000F  /* Input Primary Mode */
#define MB_START_RXx_PARAM0_MSK_VIDEO_STD           0x000001F0  /* Input Video Standard */
#define MB_START_RXx_PARAM0_BIT_RGB_OUT             0x00000200  /* Output Color space : �0� = YUV - �1� = RGB */
#define MB_START_RXx_PARAM0_BIT_LIMITED_RANGE_OUT   0x00000400  /* Output Range (Only if RGB) : �0� = [0 � 255] - �1� = [16 � 235] */
#define MB_START_RXx_PARAM0_BIT_444_OUT             0x00000800  /* Output Downsampling : �0� = 4:2:2 - �1� = 4:4:4 */
#define MB_START_RXx_PARAM0_MSK_DOWNSAMPLING_MODE   0x00003000  /* Output Downsampling mode */
#define MB_START_RXx_PARAM0_MSK_IN_COLOR_SPACE      0x00070000  /* Input Color Space */
#define MB_START_RXx_PARAM0_BIT_CSC_COEFF_SEL       0x00080000  /* Color Space Coefficient Selection */
#define MB_START_RXx_PARAM0_BIT_FAST_ANALOG_PXL_CLK 0x00100000  /* '1' if analog reception with pixel clock faster than 135 MHz */
#define MB_START_RXx_PARAM0_BIT_ANALOG_RB           0x00200000  /* '1' if analog reception with Reduced Blanking timing */
#define MB_START_RXx_PARAM0_BIT_ANALOG_PROGRESSIVE  0x00400000  /* Set this bit only in case of analog reception with progressive video */
#define MB_START_RXx_PARAM0_MSK_FRAME_RATE          0x03800000  /* Analog HD Component Frame Rate */
#define MB_START_RXx_PARAM0_BIT_SPECIAL_1080p       0x04000000  /* Special YPrPB 1080p mode. Set this bit when we received one of 
                                                                   this YPrPb format: 1080p24, 1080p25 or 1080p30 (Analog mode only) */
#define MB_START_RXx_PARAM1_MSK_PLL_DIV_RATIO       0x00000FFF  /* PLL Div Ratio	= �Pixel Freq�/�Horizontal Freq� (Analog mode only) */
#define MB_START_RXx_PARAM1_MSK_FREE_RUN_LINE_LEN   0x07FF0000  /* Free Run Line Length = �28636363�/�Horizontal Freq� (Analog mode only) */
#define MB_START_RXx_PARAM2_MSK_TOTAL_LINE_NB       0x00000FFF  /* Total Line Number = Total number of lines per field (Analog mode only) */
#define MB_START_RXx_PARAM2_MSK_SAV_POS             0x0FFF0000  /* SAV position = H_sync + H_backporch � 10 (Analog mode only) */
#define MB_START_RXx_PARAM3_MSK_EAV_POS             0x00000FFF  /* EAV position = SAV + Active pixel per line + 4 (Analog mode only) */
#define MB_START_RXx_PARAM3_MSK_VBI_STOP_POS        0x0FFF0000  /* VBI stop position = V_sync + V_backporch (Analog mode only) */
#define MB_START_RXx_PARAM4_MSK_VBI_START_POS       0x00000FFF  /* VBI start position = VBI start + Active lines per field (Analog mode only) */
#define MB_START_RXx_PARAM4_MSK_FRAME_RATE          0x00FF0000  /* Frame Rate */
#define MB_START_RXx_PARAM5_MSK_ACTIVE_PXL          0x0000FFFF  /* Number of active pixels per line */
#define MB_START_RXx_PARAM5_MSK_ACTIVE_LINE         0xFFFF0000  /* Number of active lines per frame(In case of interlaced standard, please add number of active lines of both fields) */
#define MB_START_RXx_PARAM0_BIT_DUAL                0x80000000  /* '1' if dual link support is needed (only on RX0) */


#define MB_START_RXx_PARAM0_BIT_PRIMARY_MODE(mode)          ((mode) & MB_START_RXx_PARAM0_MSK_PRIMARY_MODE)
#define MB_START_RXx_PARAM0_GET_BIT_PRIMARY_MODE(reg)       ((reg&MB_START_RXx_PARAM0_MSK_PRIMARY_MODE))

#define MB_START_RXx_PARAM0_BIT_VIDEO_STD(standard)         ((standard<<4) & MB_START_RXx_PARAM0_MSK_VIDEO_STD)
#define MB_START_RXx_PARAM0_GET_BIT_VIDEO_STD(reg)          ((reg&MB_START_RXx_PARAM0_MSK_VIDEO_STD) >> 4)

#define MB_START_RXx_PARAM0_BIT_DOWNSAMPLING_MODE(mode)     ((mode<<12) & MB_START_RXx_PARAM0_MSK_DOWNSAMPLING_MODE)
#define MB_START_RXx_PARAM0_GET_BIT_DOWNSAMPLING_MODE(reg)  ((reg&MB_START_RXx_PARAM0_MSK_DOWNSAMPLING_MODE) >> 12)

#define MB_START_RXx_PARAM0_BIT_IN_COLOR_SPACE(color_space) ((color_space<<16) & MB_START_RXx_PARAM0_MSK_IN_COLOR_SPACE)
#define MB_START_RXx_PARAM0_GET_BIT_IN_COLOR_SPACE(reg)     ((reg&MB_START_RXx_PARAM0_MSK_IN_COLOR_SPACE) >> 16)

#define MB_START_RXx_PARAM0_BIT_FRAME_RATE(frame_rate)      ((frame_rate<<23) & MB_START_RXx_PARAM0_MSK_FRAME_RATE)
#define MB_START_RXx_PARAM0_GET_BIT_FRAME_RATE(reg)         ((reg&MB_START_RXx_PARAM0_MSK_FRAME_RATE) >> 23)

#define MB_START_RXx_PARAM1_BIT_PLL_DIV_RATIO(pll_div_ratio) ((pll_div_ratio) & MB_START_RXx_PARAM1_MSK_PLL_DIV_RATIO)
#define MB_START_RXx_PARAM1_GET_BIT_PLL_DIV_RATIO(reg)       ((reg&MB_START_RXx_PARAM1_MSK_PLL_DIV_RATIO))

#define MB_START_RXx_PARAM1_BIT_FREE_RUN_LINE_LEN(line_len) ((line_len<<16) & MB_START_RXx_PARAM1_MSK_FREE_RUN_LINE_LEN)
#define MB_START_RXx_PARAM1_GET_BIT_FREE_RUN_LINE_LEN(reg)  ((reg&MB_START_RXx_PARAM1_MSK_FREE_RUN_LINE_LEN) >> 16)

#define MB_START_RXx_PARAM2_BIT_TOTAL_LINE_NB(line_nb)      ((line_nb) & MB_START_RXx_PARAM2_MSK_TOTAL_LINE_NB)
#define MB_START_RXx_PARAM2_GET_BIT_TOTAL_LINE_NB(reg)      ((reg&MB_START_RXx_PARAM2_MSK_TOTAL_LINE_NB))

#define MB_START_RXx_PARAM2_BIT_SAV_POS(sav_pos)            ((sav_pos<<16) & MB_START_RXx_PARAM2_MSK_SAV_POS)
#define MB_START_RXx_PARAM2_GET_BIT_SAV_POS(reg)            ((reg&MB_START_RXx_PARAM2_MSK_SAV_POS) >> 16)

#define MB_START_RXx_PARAM3_BIT_EAV_POS(eav_pos)            ((eav_pos) & MB_START_RXx_PARAM3_MSK_EAV_POS)
#define MB_START_RXx_PARAM3_GET_BIT_EAV_POS(reg)            ((reg&MB_START_RXx_PARAM3_MSK_EAV_POS))

#define MB_START_RXx_PARAM3_BIT_VBI_STOP_POS(vbi_stop_pos)  ((vbi_stop_pos<<16) & MB_START_RXx_PARAM3_MSK_VBI_STOP_POS)
#define MB_START_RXx_PARAM3_GET_BIT_VBI_STOP_POS(reg)       ((reg&MB_START_RXx_PARAM3_MSK_VBI_STOP_POS) >> 16)

#define MB_START_RXx_PARAM4_BIT_VBI_START_POS(vbi_start_pos) ((vbi_start_pos) & MB_START_RXx_PARAM4_MSK_VBI_START_POS)
#define MB_START_RXx_PARAM4_GET_BIT_VBI_START_POS(reg)       ((reg&MB_START_RXx_PARAM4_MSK_VBI_START_POS))

#define MB_START_RXx_PARAM4_BIT_FRAME_RATE(frame_rate)      ((frame_rate<<16) & MB_START_RXx_PARAM4_MSK_FRAME_RATE)
#define MB_START_RXx_PARAM4_GET_BIT_FRAME_RATE(reg)         ((reg&MB_START_RXx_PARAM4_MSK_FRAME_RATE) >> 16)

#define MB_START_RXx_PARAM5_BIT_ACTIVE_PXL(active_pxl)    ((active_pxl) & MB_START_RXx_PARAM5_MSK_ACTIVE_PXL)
#define MB_START_RXx_PARAM5_GET_BIT_ACTIVE_PXL(reg)       ((reg&MB_START_RXx_PARAM5_MSK_ACTIVE_PXL))

#define MB_START_RXx_PARAM5_BIT_ACTIVE_LINE(active_line)     ((active_line<<16) & MB_START_RXx_PARAM5_MSK_ACTIVE_LINE)
#define MB_START_RXx_PARAM5_GET_BIT_ACTIVE_LINE(reg)         ((reg&MB_START_RXx_PARAM5_MSK_ACTIVE_LINE) >> 16)



#define PRIMARY_MODE_ANALOG_SD          0x0
#define PRIMARY_MODE_ANALOG_HD          0x1
#define PRIMARY_MODE_ANALOG_GR          0x2
#define PRIMARY_MODE_HDMI_SD            0x4
#define PRIMARY_MODE_HDMI_HD            0x5
#define PRIMARY_MODE_HDMI_GR            0x6

#define VIDEO_STD_525i                  0xA
#define VIDEO_STD_625i                  0xB

#define VIDEO_STD_525p                  0x6
#define VIDEO_STD_625p                  0x7

#define VIDEO_STD_720p                  0xA
#define VIDEO_STD_1080p                 0xB
#define VIDEO_STD_1080i                 0xC

#define VIDEO_STD_800x600_56            0x00
#define VIDEO_STD_800x600_60            0x01
#define VIDEO_STD_800x600_72            0x02
#define VIDEO_STD_AUTO_HDMI             VIDEO_STD_800x600_72
#define VIDEO_STD_800x600_75            0x03
#define VIDEO_STD_800x600_85            0x04
#define VIDEO_STD_1280x1024_60          0x05
#define VIDEO_STD_1280x1024_75          0x06
#define VIDEO_STD_CUSTOM_ANALOG         0x07
#define VIDEO_STD_640x480_60            0x08
#define VIDEO_STD_640x480_72            0x09
#define VIDEO_STD_640x480_75            0x0A
#define VIDEO_STD_640x480_85            0x0B
#define VIDEO_STD_1024x768_60           0x0C
#define VIDEO_STD_1024x768_70           0x0D
#define VIDEO_STD_1024x768_75           0x0E
#define VIDEO_STD_1024x768_85           0x0F

#define DOWNSAMPLING_MODE_NONE         0x0 /* None */
#define DOWNSAMPLING_MODE_NOFILTER     0x1 /* Downsamples only (no filtering) */
#define DOWNSAMPLING_MODE_SHARP        0x2 /* Filters and downsamples (High bandwidth, sharp transition) */
#define DOWNSAMPLING_MODE_SOFT         0x3 /* Filters and downsamples (Soft filter with minimized ringing) */

#define IN_COLOR_SPACE_RGB_LIMITED     0x0 /* RGB limited range (16-235) input */
#define IN_COLOR_SPACE_RGB_FULL        0x1 /* RGB full range (0-255) input */
#define IN_COLOR_SPACE_YPbPr601        0x2 /* YCrCb 601 input */
#define IN_COLOR_SPACE_YPbPr709        0x3 /* YCrCb 709 input */
#define IN_COLOR_SPACE_XVYCC601        0x4 /* XVYCC 601 */
#define IN_COLOR_SPACE_XVYCC709        0x5 /* XVYCC 709 */
#define IN_COLOR_SPACE_AUTO            0x7 /* Automatic (Do not use with DVI input signal) */

#define FRAME_RATE_60HZ                0x0 /* �000� = 60 Hz or 59.94 Hz  */ 
#define FRAME_RATE_50HZ                0x1 /* �001� = 50 Hz */ 
#define FRAME_RATE_30HZ                0x2 /* �010� = 30 Hz or 29.97 Hz */ 
#define FRAME_RATE_25HZ                0x3 /* �011� = 25 Hz */ 
#define FRAME_RATE_24HZ                0x4 /* �100� = 24 Hz */ 


// -- MB_COLOR_CTRLx : Control color correction on Front End x  -----------------------------------------------------/
#define MB_COLOR_CTRLx_PARAM0_MSK_CONTRAST       0x000000FF  /* This function provides a user control for the contrast adjustment. All intermediate values are possible:0x80 = Gain on luma channel = 1.00x00 = Gain on luma channel = 0.00xFF = Gain on luma channel = 1.99 */
#define MB_COLOR_CTRLx_PARAM0_MSK_SATURATION     0x00FF0000  /* This function provides a user control for the saturation adjustment. All intermediate values are possible:0x80 = Gain on chroma channel = 1.00x00 = Gain on chroma channel = 0.00xFF = Gain on chroma channel = 1.99 */
#define MB_COLOR_CTRLx_PARAM1_MSK_BRIGHTNESS     0x000000FF  /* This function provides a user control for the brightness adjustment. It is a signed number and a gain of 4 is applied to the programmed value to provide a range of -512d to 508d.0x00 = Offset of luma channel = 0d0x80 = Offset of luma channel = -512d (-128 * 4 )0x7F = Offset of luma channel = 508d (127 *4 ) */
#define MB_COLOR_CTRLx_PARAM1_MSK_HUE            0x00FF0000  /* This function provides a user control for the hue adjustment. Value must be content into interval:0x00 = Phase of the chroma channel = 0 degrees0x40 = Phase of the chroma channel = 90 degrees0x80 = Phase of the chroma channel = 180 degrees */

#define MB_COLOR_CTRLx_PARAM0_BIT_CONTRAST (contrast)    ((contrast) & MB_COLOR_CTRLx_PARAM0_MSK_CONTRAST)
#define MB_COLOR_CTRLx_PARAM0_GET_BIT_CONTRAST(reg)      ((reg&MB_COLOR_CTRLx_PARAM0_MSK_CONTRAST))

#define MB_COLOR_CTRLx_PARAM0_BIT_SATURATION(saturation) ((saturation<<16) & MB_COLOR_CTRLx_PARAM0_MSK_SATURATION)
#define MB_COLOR_CTRLx_PARAM0_GET_BIT_SATURATION(reg)    ((reg&MB_COLOR_CTRLx_PARAM0_MSK_SATURATION) >> 16)

#define MB_COLOR_CTRLx_PARAM1_BIT_BRIGHTNESS (brightness) (brightness) & MB_COLOR_CTRLx_PARAM1_MSK_BRIGHTNESS)
#define MB_COLOR_CTRLx_PARAM1_GET_BIT_BRIGHTNESS(reg)     ((reg&MB_COLOR_CTRLx_PARAM1_MSK_BRIGHTNESS))

#define MB_COLOR_CTRLx_PARAM1_BIT_HUE(hue)               ((hue<<16) & MB_COLOR_CTRLx_PARAM1_MSK_HUE)
#define MB_COLOR_CTRLx_PARAM1_GET_BIT_HUE(reg)           ((reg&MB_COLOR_CTRLx_PARAM1_MSK_HUE) >> 16)


// -- MB_H_ADJ_RXx : Horizontal Adjustment parameters   -----------------------------------------------------/
#define MB_H_ADJ_RXx_PARAM_MSK_PXL_SHIFT              0x000003FF  /* Number of pixel shift (This 10-bit word operates in a twos compliment mode.) */

#define MB_H_ADJ_RXx_PARAM_BIT_PXL_SHIFT(pxl_shift)   ((pxl_shift) & MB_H_ADJ_RXx_PARAM_MSK_PXL_SHIFT)
#define MB_H_ADJ_RXx_PARAM_GET_BIT_PXL_SHIFT(reg)     ((reg&MB_H_ADJ_RXx_PARAM_MSK_PXL_SHIFT))


// -- MB_V_ADJ_RXx : Vertical Adjustment parameters   -----------------------------------------------------/
#define MB_V_ADJ_RXx_PARAM_MSK_LINE_SHIFT             0x0000000F  /* Number of line shift (This 4-bit word operates in a twos compliment mode.) */

#define MB_V_ADJ_RXx_PARAM_BIT_LINE_SHIFT(line_shift)  ((line_shift) & MB_V_ADJ_RXx_PARAM_MSK_LINE_SHIFT)
#define MB_V_ADJ_RXx_PARAM_GET_BIT_LINE_SHIFT(reg)     ((reg&MB_V_ADJ_RXx_PARAM_MSK_LINE_SHIFT))


// -- MB_PHASE_RXx : Analog sampling phase of RXx adjustement  -----------------------------------------------/
#define MB_PHASE_RXx_PARAM_MSK_PHASE                  0x0000001F  /* Index of phase selected to sample analog video (0:31) */

#define MB_PHASE_RXx_PARAM_BIT_PHASE(phase)        ((phase) & MB_PHASE_RXx_PARAM_MSK_PHASE)
#define MB_PHASE_RXx_PARAM_GET_BIT_PHASE(reg)      ((reg&MB_PHASE_RXx_PARAM_MSK_PHASE))

#endif // _DVIDRV_REGISTERSMAP_H_
