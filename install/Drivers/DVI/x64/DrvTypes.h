#ifndef _LINUX_TYPES_H_
#define _LINUX_TYPES_H_

#ifdef __linux__

#include <linux/module.h>     /*mandatory for a linux module*/
#include <linux/kernel.h>
#include <linux/version.h>    /*information about kernel being built*/
#include <linux/init.h>       /*to specify our init and cleanup functions of our driver*/
#include <linux/fs.h>         /*to get alloc_chrdev_region*/
#include <linux/pci.h>        /*PCI specific functions*/
#include <linux/cdev.h>       /*Character Devices specific functions*/
#include <linux/firmware.h>   /*firmware class to get firmware from user space*/
#include <linux/delay.h>      /*mdelay function*/
#include <linux/interrupt.h>  /*Interrupts stuff*/
#include <linux/mm.h>         /*mmap*/
#include <linux/wait.h>
#include <linux/spinlock.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <asm/uaccess.h>      /*copy_to_user*/
#include <asm/io.h>


#define LINUX_DRIVER
#include "CTypes.h"
#include "InterlockedOp.h"

#define NB_AUX_CHAR_DEVICES         10

#else
#error "This must must be used in Linux environment"
#endif


struct _OS_DRV_DATA;
typedef struct _OS_DRV_DATA OS_DRV_DATA;



#define INFINITE  0xFFFFFFFF


struct _DEVICE_EXTENSION;

typedef struct _DEVICE_EXTENSION* PDEVICE_EXTENSION;
typedef struct _DEVICE_EXTENSION  DEVICE_EXTENSION;

typedef enum
{
   IOCTL_STATUS_OK,                  ///< Success Status.
   IOCTL_STATUS_FAILED,              ///< Failed status.
   IOCTL_STATUS_INVALID_PARAMETER,   ///< At least one given parameter is invalid.
   IOCTL_STATUS_UNKNOWN_IOCTL,       ///< Returned value when the given IOCTL command is not recognized.
   IOCTL_STATUS_NOT_IMPLEMENTED,     ///< The IOCTL exists but not yet implemented
   IOCTL_STATUS_MEM_FAULT,           ///< A mem fault occured when accessing the IOCTL buffer
   IOCTL_STATUS_WAKEUP_ACK,          ///< The wakeup needs to be acknowledged before continuing
   IOCTL_STATUS_DEVICE_REMOVED,      ///< The device has been removed, the clients should stop referencing this driver instance so the OS can unload it
   IOCTL_STATUS_LOW_POWER_STATE,     ///< The device is in low power state due tu high temperature
} IOCTL_STATUS;


#endif



