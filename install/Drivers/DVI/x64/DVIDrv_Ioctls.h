/********************************************************************************************************************//**
 * @internal
 * @file   	TstDrvIoctls.h
 * @date   	2010/08/10
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/10  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _TSTDRVIOCTLS_H_
#define _TSTDRVIOCTLS_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "StreamIoctls.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define TSTDRV_IOC(code) (STREAM_IOC_USER(code))

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/



#pragma pack(push, 4)

typedef enum
{
   IOCTL_SPI_FLASH_ACCESS_TYPE_WRITE_AND_CHECK_SECTOR,
   IOCTL_SPI_FLASH_ACCESS_TYPE_CHECK_SECTOR,
} IOCTL_SPI_FLASH_ACCESS_TYPE;

typedef struct 
{
   ULONG                            StructSize_UL;
   IOCTL_SPI_FLASH_ACCESS_TYPE      Type_e;
   ULONG                            FlashAddress_UL;
   DELTA_VOID                       pBuffer_v;
   ULONG                            BufferSize_UL;      
} IOCTL_SPI_FLASH_ACCESS_PARAM;


#pragma pack(pop)

#define IOCTL_SPI_FLASH_ACCESS  DEFINE_IOCTL_RDWR(TSTDRV_IOC(0x12), IOCTL_SPI_FLASH_ACCESS_PARAM)





/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _TSTDRVIOCTLS_H_
