#ifndef _INTERRUPT_SOURCES_DEF_H_
#define _INTERRUPT_SOURCES_DEF_H_

//#include "../DrvIF/StreamIntSourcesDef.h"

#define INT_SOURCE_RX_INT                  0x0000000F
#define INT_SOURCE_RX0_INT                 0x00000001
#define INT_SOURCE_RX1_INT                 0x00000002
#define INT_SOURCE_OVERRUN_RX0_INT         0x00000010
#define INT_SOURCE_OVERRUN_RX1_INT         0x00000020
#define INT_SOURCE_DMA_DONE                0x00000100
#define INT_SOURCE_TC_INT                  0x00000200
#define INT_SOURCE_TIMECODE                0x00000200
#define INT_SOURCE_LTC_INT                 0x00000400
#define INT_SOURCE_RX0_FIELD_INT           0x00001000
#define INT_SOURCE_RX1_FIELD_INT           0x00002000
#define INT_SOURCE_MVTG_FIELD_INT          0x00200000
#define INT_SOURCE_MVTG_FRAME_INT          0x00800000
#define INT_SOURCE_STS_CHANGE_INT          0x0F000000
#define INT_SOURCE_STS_CHANGE_RX0_INT      0x01000000
#define INT_SOURCE_STS_CHANGE_RX1_INT      0x02000000
#define INT_SOURCE_ARM_MB_TX_EMPTY         0x08000000
#define INT_SOURCE_STS_CHANGE_RX2_INT      0x10000000
#define INT_SOURCE_STS_CHANGE_RX3_INT      0x20000000
#define INT_SOURCE_USR_INT                 0x80000000




// #define INT_SOURCE_RX_INT                    0x00000F00
// #define INT_SOURCE_RX0_INT                   0x00000100
// #define INT_SOURCE_RX1_INT                   0x00000200
// #define INT_SOURCE_TX_INT                    0x0000F000
// #define INT_SOURCE_TX0_INT                   0x00001000
// #define INT_SOURCE_TX1_INT                   0x00002000


#endif


