/********************************************************************************************************************//**
 * @internal
 * @file   	SkelDrv.h
 * @date   	2010/08/12
 * @author 	cs
 * @version v00.01.1000 
 * @brief 
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/12  v00.01.0000    cs       Creation of this file
   2013/06/21  v00.01.1000    jj       Power management added

 **********************************************************************************************************************/

#ifndef _SKELDRV_H_
#define _SKELDRV_H_

/***** INCLUDES SECTION ***********************************************************************************************/

//#include "Driver.h"
#include "SkelCallBacks.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define SKELDRV_TRACE_MSG_ACCESS  DBG_OUTPUT_USR_MSG(0)
#define SKELDRV_TRACE_MSG_DPCMASK DBG_OUTPUT_USR_MSG(1)
#define SKELDRV_TRACE_MSG_CLEANUP DBG_OUTPUT_USR_MSG(2)
#define SKELDRV_TRACE_MSG_ISR DBG_OUTPUT_USR_MSG(3)

/***** MACROS DEFINITIONS *********************************************************************************************/

/** @name SKELDRV_MACRO Skeleton Driver Macros
 The skeleton driver provides a set of macro to make easier the access to some stuffs.
*///@{

#define GetDrvData() (&(pdx->DrvData_X))	   ///< This macro returns a pointer to the user driver data area defined in the DrvData.h
#define GetOSData()  ((pdx->pOsDrvData_X))	

#define SKELDRV_INIT_SAFEAREA()  KOBJ_InitializeSpinLock(pdx, &pdx->SpinLock_KO)
#define SKELDRV_ENTER_SAFEAREA() KOBJ_AcquireSpinLock(pdx, &pdx->SpinLock_KO)
#define SKELDRV_LEAVE_SAFEAREA() KOBJ_ReleaseSpinLock(pdx, &pdx->SpinLock_KO)

//#define SkelDrv_GetCustomizationTable() (&GL_SkelDrvCustomizationTable_X)   ///< Return a pointer to the callbacks list
//extern SKEL_CUSTOMIZATION GL_SkelDrvCustomizationTable_X;
#define SkelDrv_GetCustomizationTable() (&pdx->CustomizationTable_X)   ///< Return a pointer to the callbacks list

//@}

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void              SkelDrv_DeviceAdd( PDEVICE_EXTENSION pdx);      /* Sets pdx->BoardInitStatus_E */
void              SkelDrv_Wakeup(PDEVICE_EXTENSION pdx);          /* Sets pdx->BoardInitStatus_E */
void              SkelDrv_Sleep(PDEVICE_EXTENSION pdx, BOOL32 PowerOff_B);
void              SkelDrv_DeviceRemove(PDEVICE_EXTENSION pdx);
void              SkelDrv_ReleasePermanentResources(PDEVICE_EXTENSION pdx);

void              SkelDrv_DeviceSurpriseRemoval(PDEVICE_EXTENSION pdx);
void              SkelDrv_SetDeviceAsRemoved(PDEVICE_EXTENSION pdx);
BOOL32            SkelDrv_CheckDevicePresence(PDEVICE_EXTENSION pdx);
BOOL32            SkelDrv_IsAckWakeupRequired(PDEVICE_EXTENSION pdx, PID_TYPE Process_pid);
void SkelDrv_AllocDrvData(PDEVICE_EXTENSION pdx);
void SkelDrv_FreeDrvData(PDEVICE_EXTENSION pdx);


BOOL32            SkelDrv_Open(PDEVICE_EXTENSION pdx, PID_TYPE PID_UL);
IOCTL_STATUS      SkelDrv_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,PID_TYPE Process_pid, void* pSysParam_v);
BOOL32            SkelDrv_Close(PDEVICE_EXTENSION pdx, PID_TYPE PID_UL);
void              SkelDrv_Cleanup(PDEVICE_EXTENSION pdx, PID_TYPE Process_pid, BOOL32 DevicePresent_B);

void              SkelDrv_SetDPCMask(PDEVICE_EXTENSION,ULONG Bit_UL,BOOL32 Safe_B);
void              SkelDrv_ClrDPCMask(PDEVICE_EXTENSION,ULONG Bit_UL,BOOL32 Safe_B);
void              SkelDrv_DPCforISR( PDEVICE_EXTENSION pdx, ULONG DPCIntSource_UL );
void              SkelDrv_PropertiesInit(ULONG * pDefaultValues_UL);
BOOL32            SkelDrv_SetDrvPowerState(PDEVICE_EXTENSION pdx, DRV_POWER_STATE PowerState_E);

/* 
Possible call stacks: 

Board wonders if the device is removed 
   => SkelDrv_CheckDevicePresence
      => CustomTable->Power_IsDevicePresent_fct
         => Board_IsDevicePresent_fct
         => SkelDrv_SetDeviceAsRemoved

Board shouldn't call Board_IsDevicePresent_fct directly as it would bypass SkelDrv_SetDeviceAsRemoved.

OS-dependent function "surprise removal"
   => SkelDrv_DeviceRemoval
      => SkelDrv_SetDeviceAsRemoved
      => CustomTable->Power_DeviceRemoval_fct

Once SkelDrv_SetDeviceAsRemoved has been called, SkelDrv_CheckDevicePresence always 
returns FALSE without doing anything else. The driver should not start any operation
until it is unloaded.
*/

#include "Driver.h"

#endif // _SKELDRV_H_
