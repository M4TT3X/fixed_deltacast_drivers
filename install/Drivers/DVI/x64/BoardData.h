/********************************************************************************************************************//**
 * @internal
 * @file   	BoardData.h
 * @date   	2010/07/15
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/15  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _BOARDDATA_H_
#define _BOARDDATA_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "KernelObjects.h"
#include "DVIDrv_RegistersMap.h"
#include "KernelSysCall.h"
#include "FPGA_DMAController.h"


/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct
{
  BOOL32                    DMADoneIntFromPCI_B;
  BOOL32                    DMADoneIntFromFPGA_B;
  ULONG pLastRxBufID_UL[2];     // Keeps the last RX Buffer IS cough during ISR
  ULONG pLastRXTimeStamp_UL[2];
  SYSTEM_TIME             pLastRxSystemTime_X[4];   
  ULONG HWFSFirmwareCheckVersion_UL;


  FPGA_DMACTRL_PARAM DMACtrlParam_X;

} BOARD_DATA;

#endif // _BOARDDATA_H_

