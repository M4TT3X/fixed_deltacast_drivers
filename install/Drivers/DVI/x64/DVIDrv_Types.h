/********************************************************************************************************************//**
 * @internal
 * @file   	BoardTypes.h
 * @date   	2010/07/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/16  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _DVIBOARDTYPES_H_
#define _DVIBOARDTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define DVIBRD_NB_ONBRD_BUF   8 ///< Defines the number of on board buffers per data type.

/** @name BRD_DMARQST_USRDATA_IDX Definition of the index for DMA request user data table *///@{
#define DVIBRD_DMARQST_USRDATA_IDX_FLAGS       0   ///< This user data attached to a DMA request gives some flags specific to the board.
#define DVIBRD_DMARQST_USRDATA_IDX_EOD_VALUE   1
//@}

#define DVIBRD_DMARQST_USRDATA_FLAGS_MULTIRQST_FIRST (0x01) ///< This is the first DMA request from a multi-request data transfer     
#define DVIBRD_DMARQST_USRDATA_FLAGS_MULTIRQST_LAST  (0x02) ///< This is the last DMA request from a multi-request data transfer   

typedef enum
{
   DVIBRD_CHN_DATA_TYPE_VIDEO,
   DVIBRD_NBOF_CHN_DATA_TYPE
}DVIBRD_CHN_DATA_TYPE;

typedef enum
{
	DVIBRD_BUFQ_DATA_TYPE_VIDEO
}DVIBRD_BUFQ_DATA_TYPE;

/** @name BRD_BUFQUEUE_USRPARAM_IDX Definition of User parameters index for "Buffer Queue User Parameters table" *///@{
#define DVIBRD_BUFQUEUE_USRPARAM_IDX_DATACONTENT                  0  ///< 
#define DVIBRD_BUFQUEUE_USRPARAM_IDX_CHANNEL_INDEX                1  ///< Stores the channel index
#define DVIBRD_BUFQUEUE_USRPARAM_IDX_EOD_VALUE                    2  ///< Stores the End OF DMA value to write at the end of a DMA operation
#define DVIBRD_BUFQUEUE_USRPARAM_IDX_CURRENT_ONBRD_BUF_IDX        3  ///< Stores the current buffer index on the board
#define DVIBRD_BUFQUEUE_USRPARAM_IDX_NB_ON_BOARD_BUFFER           4  ///< Stores the number of used on board buffers
#define DVIBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE          5  ///< This index as well as the 7 next stores the on board buffer mapping.
#define DVIBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_BASE    (DVIBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+DVIBRD_BUFQ_DATA_TYPE_VIDEO*DVIBRD_NB_ONBRD_BUF)

//@}



/** @name BRD_DATACONTENT_DEFINITION DVI Data content definition
 * These values are using as bitmap value in the BRD_BUFQUEUE_USRPARAM_IDX_DATACONTENT user parameter
 **/

#define DVIBRD_BUFQUEUE_DATACONTENT_VIDEO (1<<DVIBRD_BUFQ_DATA_TYPE_VIDEO)

#define DVIBRD_BUFHDR_USRIDX_TIMESTAMP        0x0

#define DVIBRD_DMA_PADDING_SIZE   0x200
#define DVIBRD_DMA_ADDR_ALIGN     0x1000

// Moved to CTypes.h #define ROUND_TO_x(Size, x)  (((ULONG)(Size) + x - 1) & ~(x - 1))
#define DVIBRD_PAD_DMA_SIZE(Size)    ROUND_TO_x(Size, DVIBRD_DMA_PADDING_SIZE)
#define DVIBRD_ALIGN_DMA_ADDR(Size)  ROUND_TO_x(Size, DVIBRD_DMA_ADDR_ALIGN)


#define FW_START_ADDRESS                     0
#define SPI_FLASH_PARTITION_SIZE            0x200000             

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _DVIBOARDTYPES_H_
