/*

 @file   MailboxMngr.h
 @date   2013/09/23
 @Author jj
 @brief  

   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2013/09/23  v00.01.0000    jj       Creation of this file
   2014/01/28  v00.01.0001    ja       Modify MailboxMngr_Ioctl to fit with IOCT cascade style to manage IOCT different than IOCTL_MAILBOX_ACCESS
   2014/04/02  v00.01.0002    gt       Remove dependence toward X3XXDrv_Ioctls.h
                                       MAILBOX_DATA is not stored in the skel level
                                                                             
 */

#ifndef _MAILBOX_MNGR_H_
#define _MAILBOX_MNGR_H_

#include "DrvTypes.h"

/* Maximum size of a mailbox message in 32-bit words. A buffer of this 
 * size is statically allocated in the IOCTL in order to copy[from/to]user
 * the whole message buffer at once */
#define MAILBOX_MAX_MESSAGE_LENGTH     64
#define MAILBOX_MAX_NBOF_MB            1

typedef struct 
{
   PMM32_REGISTER    pAddr_UL;      /* Mailbox address register */
   PMM32_REGISTER    pStatus_UL;    /* Mailbox status register */
   PMM32_REGISTER    pDataOut_UL;   /* Mailbox data out register */
   PMM32_REGISTER    pDataIn_UL;    /* Mailbox data in register */
   ULONG             ReadReady_UL;  /* The data out is ready to be read when (!ReadReady_UL || *pStatus_UL & ReadReady_UL) */
   ULONG             WriteReady_UL; /* The data in is ready to be written when (!WriteReady_UL || *pStatus_UL & WriteReady_UL) */
   KOBJ_SPINLOCK     Lock_X;        /* A lock protecting the mailbox from concurrent access */
} MAILBOX_DEF, *PMAILBOX_DEF;


typedef struct  
{
   MAILBOX_DEF          pMailboxes_X[MAILBOX_MAX_NBOF_MB];
   ULONG                MailboxCount_UL;
   ULONG                IoctlCmd_UL;
   PDEVICE_EXTENSION    pDeviceExt_X;
} MAILBOX_DATA, *PMAILBOX_DATA;

void           MailboxMngr_Init(PMAILBOX_DATA pMailBoxData_X, PDEVICE_EXTENSION pdx, PMAILBOX_DEF pMailboxes_X, ULONG MailboxCount_UL, ULONG IoctlCmd_UL);
void           MailboxMngr_Read(PMAILBOX_DATA pMailBoxData_X, ULONG MailboxIdx_UL, ULONG Addr_UL, ULONG* pBuffer_UL, ULONG Count_UL);
void           MailboxMngr_Write(PMAILBOX_DATA pMailBoxData_X, ULONG MailboxIdx_UL, ULONG Addr_UL, const ULONG* pBuffer_UL, ULONG Count_UL);
IOCTL_STATUS   MailboxMngr_Ioctl(PMAILBOX_DATA pMailBoxData_X, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,PID_TYPE PID_UL);

#endif


/* 
Example :

=== Board.cpp ===

static MAILBOX_DEF GL_pMailboxDefs_X[] =
{
   MAILBOX_INIT(&FPGA_MAILBOX_APPLE_ADDR_MM32, &FPGA_MAILBOX_APPLE_STS_MM32, ... ),
   MAILBOX_INIT(&FPGA_MAILBOX_PEACH_ADDR_MM32, &FPGA_MAILBOX_PEACH_STS_MM32, ... ),
   MAILBOX_END
};

void StreamCustomizeCallback()
{
   CustomTable->pMailboxDefs_X = GL_pMailboxDefs_X;
}

=== DrvIF.h ===

#define BOARD_MAILBOX_APPLE_IDX     0
#define BOARD_MAILBOX_PEACH_IDX     1

=== CDrvIF.cpp ===

// Read 16 32-bit words starting at address 42
ULONG pBuffer_UL[16];
IOCTL_MAILBOX_READ_PARAM Param_X;
Param_X.MailboxIdx_UL = BOARD_MAILBOX_APPLE_IDX;
Param_X.Address_UL = 42;
Param_X.pBuffer_v = pBuffer_UL;
Param_X.Length_UL = 16;
DeviceIoctl(&Param_X);

*/
