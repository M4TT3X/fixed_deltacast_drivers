
#ifndef _INOTIFYPROGRESSFUNCTOR_H_
#define _INOTIFYPROGRESSFUNCTOR_H_

/***** INCLUDES SECTION ***********************************************************************************************/

class INotifyProgressFunctor
{
public :
   virtual void operator()(ULONG ProgressInPrc_UL)=0;
};

class TStaticNotifyProgressFunctor : public INotifyProgressFunctor
{
private:
   void (*fpt)(ULONG);         // pointer to member function
public:
   // constructor - takes pointer to an object and pointer to a member and stores them in two private variables
   TStaticNotifyProgressFunctor(void(*_fpt)(ULONG))
   { fpt=_fpt; };
   // override operator "()"
   virtual void operator()(ULONG ProgressInPrc_UL)
   { (*fpt)(ProgressInPrc_UL);}; // execute static function
};

template <class TClass> class TSpecificNotifyProgressFunctor : public INotifyProgressFunctor
{
private:
   void (TClass::*fpt)(ULONG);         // pointer to member function
   TClass* pt2Object;                              // pointer to object
public:
   // constructor - takes pointer to an object and pointer to a member and stores them in two private variables
   TSpecificNotifyProgressFunctor(TClass* _pt2Object, void(TClass::*_fpt)(ULONG))
   { pt2Object = _pt2Object; fpt=_fpt; };
   // override operator "()"
   virtual void operator()(ULONG ProgressInPrc_UL)
   { (*pt2Object.*fpt)(ProgressInPrc_UL);}; // execute member function
};


#endif // _INOTIFYPROGRESSFUNCTOR_H_

