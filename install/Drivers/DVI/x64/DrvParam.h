/********************************************************************************************************************//**
 * @internal
 * @file   	DrvParam.h
 * @date   	2009/01/22
 * @author 	gt
 * @brief   
 **********************************************************************************************************************/
#ifndef DrvParam_h__
#define DrvParam_h__

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"
#include "Driver.h"
#ifdef __linux__
#include <linux/stat.h>
#endif

/***** LOCAL SYMBOLS DEFINITIONS **************************************************************************************/

/* Define driver parameter name (without quotation marks !)*/
#define DPN_FW_CHK_DISABLE    FWCheckDisable
#define DPN_ARM_CHK_DISABLE   ARMCheckDisable

#define DPN_PREALLOC_POOL0_SIZE PreallocPool0Size
#define DPN_PREALLOC_POOL1_SIZE PreallocPool1Size
#define DPN_PREALLOC_POOL2_SIZE PreallocPool2Size
#define DPN_PREALLOC_POOL3_SIZE PreallocPool3Size

#define DPN_DBG_MASK          DbgMask


/***** LOCAL MACROS DEFINITIONS ***************************************************************************************/

/***** LOCAL TYPES AND STRUCTURES DEFINITIONS *************************************************************************/

typedef struct _DRV_PARAM
{
   /* Put here the driver parameters */
   ULONG FWCheckDisable_UL;
   ULONG ARMCheckDisable_UL;
   ULONG PreallocPool0Size_UL;
   ULONG PreallocPool1Size_UL;
   ULONG PreallocPool2Size_UL;
   ULONG PreallocPool3Size_UL;

   ULONG DbgMask_UL;

}DRV_PARAM;

/***** EXTERNAL VARIABLES *********************************************************************************************/
extern DRV_PARAM GL_DrvParam_X;

#ifdef ALLOCATE_EXTERN
DRV_PARAM GL_DrvParam_X = {0,0,0,0,0,0,0}; /* Set the default value */
#else
extern DRV_PARAM GL_DrvParam_X; 
#endif



/***** FUNCTIONS DEFINITION *******************************************************************************************/
void DrvParam_Init(PDEVICE_EXTENSION pdx);












#endif // DrvParam_h__
