/********************************************************************************************************************//**
 * @file   	USBufMngr_Linux.cpp
 * @date   	2011/03/09
 * @author 	cs
 * @brief   This file implements the OS-depenent function related to the Userspace buffer manager.
 **********************************************************************************************************************/
  
/***** INCLUDES SECTION ***********************************************************************************************/

#define MODID MID_USBUFMNGR

#include "SkelDrv.h"
#include "DrvDbg.h"
#include "KernelSysCall.h"
#include "KernelObjects.h"
#include "SkelIoctls.h"
#include <linux/pagemap.h>

/***** FUNCTIONS DEFINITION *******************************************************************************************/

/**********************************************************************************************************************//**
 * @fn BOOL32 USBufMngr_RetrievePages( PDEVICE_EXTENSION pdx, USBUF_DESCR * pDescr_X, void * pUSBufAddr, ULONG Size_UL )
 * @brief This function attempts to lock the userspace buffer in memory and retrieve the list of physical pages.
 *
 * The physical addresses of all involved pages are stored in the descriptor itself.
 *
 * @param [in] pdx Pointer to the device extension
 * @param [in,out] pDescr_X Pointer to the USBUF descriptor
 * @param [in] pUSBufAddr Pointer to the userspace buffer (userspace virtual address)
 * @param [in] Size_UL Size of the userspace area.
 *
 * @return TRUE when the buffer is locked un physical memory and all pages informations are retrieved, FALSE otherwise.
 **********************************************************************************************************************/
BOOL32 USBufMngr_RetrievePages( PDEVICE_EXTENSION pdx, USBUF_DESCR * pDescr_X, void * pUSBufAddr, ULONG Size_UL ) 
{
   BOOL32 Sts_B = FALSE;
   BOOL32 Locked_B;
   ULONG i;
   ULONG NbPages_UL=Size_UL/PAGE_SIZE;
  
   struct page ** ppPage_X;
   int RetVal_i;

   pDescr_X->pOSData_v = kmalloc((MAX_PAGES_PER_USBUF+1) * sizeof(struct page *), GFP_KERNEL);

   if (pDescr_X->pOSData_v)
   {
      ppPage_X = (struct page **)pDescr_X->pOSData_v;

      down_read(&current->mm->mmap_sem);
      RetVal_i = get_user_pages(current, current->mm, ((unsigned long) pUSBufAddr), NbPages_UL, 1, 0, ppPage_X, NULL);
      up_read(&current->mm->mmap_sem);

      if (RetVal_i < 0) 
      {
         ErrOutput("Unable to retreive user pages (err=%d)\n",RetVal_i);
         Sts_B=FALSE;
      }
      else
      {
         DbgOutput("Nb Retrieved pages : %d (requested :%d)\n",RetVal_i,NbPages_UL);
         NbPages_UL = RetVal_i;
      
         
         
         
         pDescr_X->Size_UL = Size_UL;									         // Get the size covered by the MDL
         pDescr_X->Offset_UL = pUSBufAddr - ((unsigned long)pUSBufAddr & PAGE_MASK);;									      // Get the offset within the first page
         
         if (pDescr_X->Offset_UL) pDescr_X->StartPageIndex_UL = 1; // Skip first page 
         else                     pDescr_X->StartPageIndex_UL = 0;
         pDescr_X->NbPages_UL= NbPages_UL;	// Retrieve the number of pages
         pDescr_X->NbUsedPages_UL= NbPages_UL-pDescr_X->StartPageIndex_UL ;	// Retrieve the number of pages
         pDescr_X->pKAddr_v = kmap(ppPage_X[pDescr_X->StartPageIndex_UL]);
         

         pDescr_X->Size_UL-=4096; // Skip the first page

         InfoOutput( "System virtual address for 'kmapped' page is %X\n",pDescr_X->pKAddr_v);
         InfoOutput( "Userspace buffer size is %X\n",pDescr_X->Size_UL);
         InfoOutput( "Buffer offset within first page is %X\n",pDescr_X->Offset_UL);
         InfoOutput( "Buffer span to %lu pages (skip %d page(s)):\n",pDescr_X->NbPages_UL,pDescr_X->StartPageIndex_UL);

         DbgOutput("Dump first pages :\n");
         //pDescr_X->StartPageIndex_UL
         if (pDescr_X->NbUsedPages_UL<=MAX_PAGES_PER_USBUF)
         {
            for (i = 0; i < pDescr_X->NbPages_UL; i++)
            {
               pDescr_X->pPagesPhysAddr_ULL[i] = (ULONGLONG)page_to_phys(ppPage_X[i]);
               if (i<10) DbgOutput("-PageAddr[%d]:%016llx\n",i,pDescr_X->pPagesPhysAddr_ULL[i]);
            }

            Sts_B = TRUE;

         }
         else
         {
            ErrOutput("User space is too large to be used (nb pages is %d instead of maximum of %d)\n",pDescr_X->NbPages_UL,MAX_PAGES_PER_USBUF);
            pDescr_X->pOSData_v = NULL;
         }
      }
   }
   else ErrOutput("Unable to allocate memory to store pages information\n");
   
  
   return Sts_B;
}


/**********************************************************************************************************************//**
 * @fn void USBufMngr_ReleasePages( PDEVICE_EXTENSION pdx, USBUF_DESCR * pDescr_X )
 * @brief This function calls OS system functions to unlock and release an userspace buffer.
 *
 * @param [in] pdx Pointer to device extension
 * @param [in] pDescr_X Pointer to a USBUF descriptor
 * @remark This function is called internally by the USBUFMngr_UnregisterBuffer()
 **********************************************************************************************************************/
void USBufMngr_ReleasePages( PDEVICE_EXTENSION pdx, USBUF_DESCR * pDescr_X ) 
{
struct page ** ppPage_X;
ULONG i;
   if (pDescr_X->pOSData_v) 
   {
      ppPage_X = (struct page **)pDescr_X->pOSData_v; // Retrieve pages iformation storage

      kunmap(ppPage_X[pDescr_X->StartPageIndex_UL]);  // Unmap the first page out of the kernel space

      for (i=0; i<pDescr_X->NbPages_UL; i++) 
      {
         if (!PageReserved(ppPage_X[i])) 
            SetPageDirty(ppPage_X[i]);
         else RetailOutput("Page %d is reserverd\n",i);
         page_cache_release(ppPage_X[i]);
      }

      kfree(pDescr_X->pOSData_v);
      pDescr_X->pOSData_v=NULL;
   }
   else ErrOutput("pOSData_v is NULL\n");
}
