#!/bin/bash

drivername="delta-x300"
echo "Unloading the ${drivername} module"
/sbin/rmmod ${drivername}

echo "Deleting device nodes..."
rm /dev/${drivername}*
