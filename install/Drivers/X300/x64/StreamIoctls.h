/********************************************************************************************************************//**
 * @file   	StreamIoctls.h
 * @date   	2010/06/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/16  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _STREAMIOCTLS_H_
#define _STREAMIOCTLS_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "SkelIoctls.h"
#include "BufMngrTypes.h"
#include "DMAMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define STREAM_IOC(code) (SKEL_IOC_USER(code))

/** @addtogroup IOCTLS_GROUP *///@{

#define IOCTL_GET_DRIVER_VALIDATE_ACCESS_CODE   DEFINE_IOCTL_WR  (STREAM_IOC(0),ULONG)

#define IOCTL_BUFFER_QUEUE_OPEN                 DEFINE_IOCTL_RDWR(STREAM_IOC(0x5), IOCTL_BUFFER_QUEUE_OPEN_PARAM)
#define IOCTL_BUFFER_QUEUE_CLOSE                DEFINE_IOCTL_WR  (STREAM_IOC(0x6), ULONG)
#define IOCTL_BUFFER_QUEUE_ADD_BUF              DEFINE_IOCTL_WR  (STREAM_IOC(0x7), IOCTL_BUFFER_QUEUE_ADD_BUF_PARAM)
#define IOCTL_BUFFER_QUEUE_RESET                DEFINE_IOCTL_WR  (STREAM_IOC(0x8), ULONG)
#define IOCTL_BUFFER_QUEUE_UPDATE               DEFINE_IOCTL_RDWR(STREAM_IOC(0x9), IOCTL_BUFFER_QUEUE_UPDATE_PARAM)
#define IOCTL_BUFFER_USER_LOCK                  DEFINE_IOCTL_RDWR(STREAM_IOC(0xA), IOCTL_BUFFER_USER_LOCK_PARAM)
#define IOCTL_BUFFER_USER_UNLOCK                DEFINE_IOCTL_WR  (STREAM_IOC(0xB), IOCTL_BUFFER_USER_UNLOCK_PARAM)
#define IOCTL_BUFFER_QUEUE_LINK                 DEFINE_IOCTL_WR  (STREAM_IOC(0xC), IOCTL_BUFFER_QUEUE_LINK_PARAM)
#define IOCTL_BUFFER_QUEUE_GET_STATUS           DEFINE_IOCTL_RDWR(STREAM_IOC(0xD), IOCTL_BUFFER_QUEUE_STATUS)

#define IOCTL_MULTICHANNEL_ADD_DMA_REQUEST      DEFINE_IOCTL_WR  (STREAM_IOC(0x10), IOCTL_MULTICHANNEL_ADD_DMA_REQUEST_PARAM)
#define IOCTL_MULTICHANNEL_DMA_QUEUE_SETUP      DEFINE_IOCTL_WR  (STREAM_IOC(0x11), IOCTL_MULTICHANNEL_DMA_QUEUE_SETUP_PARAM)
#define IOCTL_MULTICHANNEL_ABORT_DMA            DEFINE_IOCTL_WR  (STREAM_IOC(0x12), IOCTL_MULTICHANNEL_ABORT_DMA_PARAM)

#define STREAM_IOC_USER(code) (STREAM_IOC(code)+0x20)  

//@}

#endif // _STREAMIOCTLS_H_
