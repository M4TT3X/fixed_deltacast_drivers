/********************************************************************************************************************//**
 * @internal
 * @file   	KernelSysCall.c
 * @date   	2006/11/21
 * @author 	cs
 * @brief  
 **********************************************************************************************************************/
  
/***** INCLUDES SECTION ***********************************************************************************************/

#define MODID MID_BOARD

#include "KernelSysCall.h"
#include "DrvDbg.h"
#include "Driver.h"
#include "OSDrvData.h"

#include <linux/delay.h>

/***** EXTERNAL VARIABLES *********************************************************************************************/

/***** GLOBAL VARIABLES ***********************************************************************************************/
void KSYSCALL_Sleep(ULONG MilliSec_UL)
{
   if(MilliSec_UL <= 20)
      udelay(MilliSec_UL*1000);
   else
      msleep(MilliSec_UL);
}

BOOL32 KSYSCALL_RequestFirmware(PDEVICE_EXTENSION pdx, FIRMWARE_DATA *pFWInfo_X, FIRMWARE_REQUEST *pRequest_X)
{
 
   pFWInfo_X->pFw_X=NULL;

   if (pRequest_X->pFilename_c)
   {
      if (!request_firmware((const struct firmware **)&pFWInfo_X->pFw_X,pRequest_X->pFilename_c, &pdx->pOsDrvData_X->pPCIDevice_X->dev)) 
      {
         pFWInfo_X->pData_UB = (UBYTE*)pFWInfo_X->pFw_X->data; 
         pFWInfo_X->Size_UL = (ULONG)pFWInfo_X->pFw_X->size;

         return (TRUE);
      }
      else return FALSE;
   }

   return TRUE;
}

BOOL32 KSYSCALL_ReleaseFirmware(PDEVICE_EXTENSION pdx, FIRMWARE_DATA *pFWInfo_X)
{
   release_firmware(pFWInfo_X->pFw_X);

   return TRUE;
}

//BOOL32 KSYSCALL_NVParameterRead(PDEVICE_EXTENSION pdx, char *pParamName_c, ULONG *pParamValue_UL)
//{
//   return TRUE;
//}
void KSYSCALL_GetSystemTime(SYSTEM_TIME *pCT_X)
{
  // do_gettimeofday(pCT_X);
   getrawmonotonic(pCT_X);

}
void KSYSCALL_GetPCIEVendorID(PDEVICE_EXTENSION pdx, UWORD *pPCIEVendorID_UW, UWORD *pPCIEDeviceID_UW)
{
   WORD PCIEVendorID_W=0xDEAD, PCIEDeviceID_W=0xBEEF;
   char PCIEVendorID1_W[10], PCIEDeviceID1_W[10];
   int i;

   pci_read_config_word(pdx->pOsDrvData_X->pPCIDevice_X,PCI_VENDOR_ID,&PCIEVendorID_W);
   pci_read_config_word(pdx->pOsDrvData_X->pPCIDevice_X,PCI_DEVICE_ID,&PCIEDeviceID_W);

   if (pPCIEVendorID_UW) *pPCIEVendorID_UW=PCIEVendorID_W;
   if (pPCIEDeviceID_UW) *pPCIEDeviceID_UW=PCIEDeviceID_W;

}

LONGLONG KSYSCALL_SystemTimeToMicroSec( SYSTEM_TIME *pCT_X )
{
   //return ((LONGLONG)pCT_X->tv_sec*1000000+pCT_X->tv_usec);
   return ((LONGLONG)pCT_X->tv_sec*1000000+pCT_X->tv_nsec/1000);
}

int KSYSCALL_CopyFromUserSpace( void * pSrc_v, void *pDest_v, ULONG Size_UL )
{
   /*return copy_from_user(pSrc_v,pDest_v,Size_UL);*/
    return copy_from_user_toio(pSrc_v,pDest_v,Size_UL);
}

int KSYSCALL_CopyToUserSpace( void * pSrc_v, void *pDest_v, ULONG Size_UL )
{
   /*return copy_to_user( pSrc_v,pDest_v, Size_UL );*/
   return copy_to_user_fromio( pSrc_v,pDest_v, Size_UL );
}

BOOL32 KSYSCALL_MapMemory(ULONGLONG PhysicalAddress_ULL, ULONG Length_UL, void** ppUserSpace_v)
{
   return TRUE;
}

void KSYSCALL_UnmapMemory(ULONGLONG PhysicalAddress_ULL, ULONG Length_UL, void* pUserSpace_v)
{
}
