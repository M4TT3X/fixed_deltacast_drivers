/********************************************************************************************************************//**
 * @internal
 * @file   	X330Drv_Properties.h
 * @date   	2014/09/29
 * @author 	bc
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2014/09/29  v00.01.0000    bc       Creation of this file

 **********************************************************************************************************************/

#ifndef _X330DRV_PROPERTIES_H_
#define _X330DRV_PROPERTIES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "StreamProperties.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define X330DRV_BOARD_FEATURES_GENLOCK0         0x00000001
#define X330DRV_BOARD_FEATURES_GENLOCK1         0x00000002
#define X330DRV_BOARD_FEATURES_UNUSED           0x00000040
#define X330DRV_BOARD_FEATURES_4K               0x00000100
#define X330DRV_BOARD_FEATURES_THUMBNAIL        0x00000200
#define X330DRV_BOARD_FEATURES_BIDIR            0x00000800
#define X330DRV_BOARD_FEATURES_CSCINPUT         0x00020000
#define X330DRV_BOARD_FEATURES_CSCOUTPUT        0x00040000
#define X330DRV_BOARD_FEATURES_LTC              0x00080000
#define X330DRV_BOARD_FEATURES_KEYER            0x00100000
 
#define X330DRV_RX_CHN_STATE_BIT_FIELD_MODE     0x00000001
#define X330DRV_RX_CHN_STATE_BIT_DUAL_LINK      0x00000002
#define X330DRV_RX_CHN_STATE_BIT_QUAD_LINK      0x00000004
#define X330DRV_RX_CHN_STATE_BIT_LINK_B         0x00000008

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
   X330DRV_PROPERTIES_FIRST=NB_STREAM_PROPERTIES,
   X330DRV_PROPERTIES_PRODUCT_FAMILY=X330DRV_PROPERTIES_FIRST,
   X330DRV_PROPERTIES_BOARD_FIRMWARE_VERSION,
   X330DRV_PROPERTIES_BOARD_ARM_VERSION,
   X330DRV_PROPERTIES_BOARD_SSN_MSB,
   X330DRV_PROPERTIES_BOARD_SSN_LSB,
   X330DRV_PROPERTIES_EXTBOARD_TYPE,
   X330DRV_PROPERTIES_BOARD_FIRMWAREID,
   X330DRV_PROPERTIES_BOARD_FEATURES,
   X330DRV_PROPERTIES_BOARD_FPGA_FW_ID,
   X330DRV_PROPERTIES_BOARD_ARM_FW_ID,
   X330DRV_PROPERTIES_BOARD_RQST_FIRMWARE_VERSION,
   X330DRV_PROPERTIES_BOARD_RQST_ARM_VERSION,
   X330DRV_PROPERTIES_BOARD_NB_UPGRADABLE_STUFF, 
   X330DRV_PROPERTIES_BOARD_NB_KEYER, 

   NB_X330DRV_PROPERTIES
} X330DRV_PROPERTIES;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _X330DRV_PROPERTIES_H_
