/********************************************************************************************************************//**
 * @file   	KThreadMngr.h
 * @date   	2011/02/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief   This file describes the manager part of the driver skeleton which manages the thread at kernel level.
 *
 * KThread manager
 * 
 * The KThread manager is based on an OS-abstract kernel thread : KOBJ_KTHREAD. But, to make easier the way to write 
 * the kernel thread (which needs some calls, and some restrictions), the KTHread manager want NON BLOCKING functions
 * as thread processing function. It will ensure that the function will be either called on event, or periodically.
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2011/02/16  v00.01.0000    cs       Creation of this file
   2011/02/25  v00.02.0000    cs       Add an IOCTL handler.
   2011/02/25  v00.02.0001    cs/gt    Fix a bug : STOP IOCTL did nothing.
   2011/03/02  v00.02.0002    cs       Add Traceing messages.

 **********************************************************************************************************************/

#ifndef _KTHREADMNGR_H_
#define _KTHREADMNGR_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvTypes.h"
#include "KernelObjects.h"
#include "KThreadMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#ifndef MAX_KTHREADS 
#define MAX_KTHREADS 1
#endif


#define KTHREAD_FLAGS_USED 0x80000000

#define INVALID_KTHREADID 0xFFFFFFFF
#define KTHREAD_SYNC_PERIOD_UPDATE 0x40000000

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
   KTHREAD_EVENT_INIT,
   KTHREAD_EVENT_STOP,
   KTHREAD_EVENT_WAKEUP,
   KTHREAD_EVENT_PERIODIC,
   KTHREAD_EVENT_PERIOD_UPDATE,

} KTHREAD_EVENT;



typedef void (*KTHREADMNGR_FCT)(PDEVICE_EXTENSION pdx, KTHREADID KThreadID, void * pThreadParam_v,KTHREAD_EVENT KThreadEvent_e, ULONG EventParam_UL);

typedef struct  
{
   ULONG Flags_UL;
   KOBJ_KTHREAD KThread_KOBJ;
   void * pContext_v;
   PDEVICE_EXTENSION pdxcopy;
   KTHREADMNGR_FCT KThreadFunction_fct;
   volatile ULONG KThreadPeriod_UL;
   KTHREADID KThreadID;
} KTHREAD_DESCR;

typedef struct  
{
   KOBJ_SPINLOCK SpinLock_KO;
   KTHREAD_DESCR  pKThreadList_X[MAX_KTHREADS];
} KTHREAD_MNGR_DATA;


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void         KThreadMngr_DeviceAdd(PDEVICE_EXTENSION pdx);
void         KThreadMngr_ReleasePermanentResources(PDEVICE_EXTENSION pdx);
KTHREADID    KThreadMngr_CreateKThread(PDEVICE_EXTENSION pdx, KTHREADMNGR_FCT ThreadProc_fct, void * Context_v, ULONG ThreadPeriod_UL);
BOOL32       KThreadMngr_StopKThread(PDEVICE_EXTENSION pdx, KTHREADID KThreadID);
BOOL32       KThreadMngr_WaitForKThread(PDEVICE_EXTENSION pdx, KTHREADID KThreadID, ULONG Timeout_UL);
BOOL32       KThreadMngr_SignalKThread(PDEVICE_EXTENSION pdx,KTHREADID KThreadID,ULONG Param_UL);
BOOL32       KThreadMngr_SetKThreadPeriod(PDEVICE_EXTENSION pdx,KTHREADID KThreadID,ULONG Timeout_UL, BOOL32 Signal_B);
IOCTL_STATUS KThreadMngr_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,PID_TYPE PID_UL);


#endif // _KTHREADMNGR_H_
