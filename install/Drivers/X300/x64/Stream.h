/********************************************************************************************************************//**
 * @file   	Stream.h
 * @date   	2010/06/16
 * @author 	cs/gt
 * @version v00.02.0002 
 * @brief   This file describes the stream driver layer.
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/16  v00.01.0000    cs/gt    Creation of this file
   2010/09/20  v00.02.0000    cs       Update the IOCTL functions to properly call all modules IOCTL functions before the 
                                       board specific one.
   2010/09/23  v00.02.0001    cs       Add some check to the customization table function pointers, allowing higher driver layer to let
                                       some to NULL.
   2011/01/26  v00.02.0002    cs       Change the Stream_CheckDrvFeature() behavior to use the new unsafeget() macro to access
                                       to the driver property instead of the safe way to make the access more efficient.

 **********************************************************************************************************************/

#ifndef _STREAM_H_
#define _STREAM_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "SkelCallBacks.h"
#include "StreamProperties.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

BOARD_INIT_STATUS Stream_DeviceAdd(PDEVICE_EXTENSION pdx);
BOARD_INIT_STATUS Stream_Wakeup(PDEVICE_EXTENSION pdx);
void              Stream_Sleep(PDEVICE_EXTENSION pdx, BOOL32 PowerOff_B);
void              Stream_DeviceRemove(PDEVICE_EXTENSION pdx);
BOOL32            Stream_ISR(PDEVICE_EXTENSION pdx,ULONG *pIntSources_UL);
void              Stream_DPCforISR(PDEVICE_EXTENSION pdx, ULONG DPCIntSource_UL);
BOOL32            Stream_Open(PDEVICE_EXTENSION pdx, ULONG RefCount_UL, PID_TYPE PID_UL);
BOOL32            Stream_Close(PDEVICE_EXTENSION pdx, ULONG RefCount_UL, PID_TYPE PID_UL);
IOCTL_STATUS      Stream_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL, PID_TYPE PID_UL);
BOOL32            Stream_CheckResources(PDEVICE_EXTENSION pdx);
void              Stream_PropMngrPropertiesInit(PDEVICE_EXTENSION pdx, ULONG *pDefaultProperties_UL);
void              Stream_PropertiesInit(ULONG *pDefaultProperties_UL);
void              Stream_CleanUp(PDEVICE_EXTENSION pdx, PID_TYPE PID_UL, BOOL32 DevicePresent_B); 



/** @group STREAM_PROPERTIES_HELPER_GROUP Stream Properties Access Helpers
 These functions make easier the access to some usual properties defined at stream level.
*/
//@{
void Stream_SetNbRXChannels(PDEVICE_EXTENSION pdx,ULONG Nb_UL);                        ///< Specifies the numbers of RX channels
void Stream_SetTXChannelType(PDEVICE_EXTENSION pdx,ULONG Idx_UL, CHANNEL_TYPE Type);   ///< Specifies the type of a given RX channel
void Stream_SetNbTXChannels(PDEVICE_EXTENSION pdx,ULONG Nb_UL);                        ///< Specifies the number of TX channels
void Stream_SetRXChannelType(PDEVICE_EXTENSION pdx,ULONG Idx_UL, CHANNEL_TYPE Type);   ///< Specifies the type of a given TX channel

void Stream_SetBoardState(PDEVICE_EXTENSION pdx,STREAM_BOARD_STATE State_e);           ///< Sets the board state
STREAM_BOARD_STATE Stream_GetBoardState(PDEVICE_EXTENSION pdx);                        ///< Retrieves the board state

void Stream_SetDrvFeature(PDEVICE_EXTENSION pdx,ULONG FeatureBitMask_UL);
void Stream_ClrDrvFeature(PDEVICE_EXTENSION pdx,ULONG FeatureBitMask_UL);
BOOL32 Stream_CheckDrvFeature(PDEVICE_EXTENSION pdx,ULONG FeatureBitMask_UL);


ULONG Stream_GetNbRXChannels(PDEVICE_EXTENSION pdx);
ULONG Stream_GetNbTXChannels(PDEVICE_EXTENSION pdx);
CHANNEL_TYPE Stream_GetTXChannelType(PDEVICE_EXTENSION pdx,ULONG Idx_UL);
CHANNEL_TYPE Stream_GetRXChannelType(PDEVICE_EXTENSION pdx,ULONG Idx_UL);

void Stream_SetBusType( PDEVICE_EXTENSION pdx, STREAM_BUS_TYPE BusType_E );
STREAM_BUS_TYPE Stream_GetBusType( PDEVICE_EXTENSION pdx );

//@}

void * MyGetBoardData(PDEVICE_EXTENSION pdx);
BOOL32 Stream_BufMngrIsTransferAllowed( PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL, BOOL32 Early_B );
BOOL32 Stream_BufMngrTxIntHandler(PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL);
void Stream_AllocDrvData(PDEVICE_EXTENSION pdx);
void Stream_FreeDrvData(PDEVICE_EXTENSION pdx);
#endif // _STREAM_H_




