/********************************************************************************************************************//**
 * @internal
 * @file   	CX320DrvIF.h
 * @date   	2013/03/26
 * @author 	ja
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	c     	      Author   Description
   ====================================================================================================================
   2013/03/26  v01.00.0000    ja       Creation of this file
                                       
 **********************************************************************************************************************/

#ifndef _CX320DRVIF_H_
#define _CX320DRVIF_H_

#include "CX3XXDrvIF.h"
#include "CStreamDrvIF.h"
#include "IFwUpdate.h"
#include "NorFlash.h"
#include "X3XXDrv_Types.h"
#include "X320Drv_Types.h"
#include "HDMI.h"
#include "DVI.h"
#include "X320Drv_Properties.h"
#include "CMailboxIF.h"

namespace CX320DrvIF_NS
{

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define RXCHANNEL_ADDITIONAL_FEATURES_FIELD_MODE            0x000000001
#define RXCHANNEL_ADDITIONAL_FEATURES_THUMBNAIL             0x000000002

   typedef struct  
   {
      UBYTE NbRxChannels_UB;
      UBYTE NbTxChannels_UB;
      ULONG FirmwareVersion_UL;
      X3XX_PRODUCT_FAMILY ProductFamily_E;
      LONGLONG SSN_LL;
      ULONG BoardFeatures_UL;
      X320_FW_ID FpgaFwId_E;
      X320_FW_ID ARMFwId_E;
      ULONG RqstFpgaFwVersion_UL;
      ULONG ArmVersion_UL;
      ULONG RqstArmFwVersion_UL;
      ULONG NbUpgradableStuff_UL;
   } BOARD_INFO;

   enum X320_ONBOARD_BUFFER_MAPPING
   {
      X320_ONBOARD_BUFFER_MAPPING_20
   } ;


   typedef struct  
   {
      DVI_TIMING        Timing_X;
      DVI_DATA_FORMAT   OutputDataFormat_E;
      DVI_COLOR_SPACE   InputColorSpace_E;
      BOOL32            AudioCaptureEnabled_B;
      ULONG             AdditionalFeatures_UL;
      DVI_MODE          Mode_E;//!< DVI mode.
   }RX_CHANNEL_START_PARAM;

   typedef union
   {
      struct  
      {
         //Byte 0
         UBYTE Professional_UB   : 1;
         UBYTE LinearPCM_UB      : 1;
         UBYTE Copyright_UB      : 1;
         UBYTE Emphasis_UB       : 3;
         UBYTE Reserved1_UB      : 2;

         //Byte 1
         UBYTE CategoryCode_UB   : 8;

         //Byte 2
         UBYTE SourceNb_UB    : 4;
         UBYTE ChannelNb_UB   : 4;

         //Byte 3
         UBYTE SamplingFrequency_UB : 4;
         UBYTE ClockAccuracy_UB     : 2;
         UBYTE Reserved2_UB         : 2;

         //Byte 4
         UBYTE MaxWordLengthSize_UB : 1;
         UBYTE SampleWordLength_UB  : 3;
         UBYTE Reserved3_UB         : 4;

         //BYTE 5
         UBYTE Reserved4_UB : 8;

         //BYTE 6
         UBYTE Reserved5_UB : 8;

         //BYTE 7
         UBYTE Reserved6_UB : 8;

      };
      UBYTE pData_UB[8];
   }AUDIO_CHANNEL_STS;

   typedef union
   {
      struct
      {
         //Byte 0
         UBYTE ChannelNb_UB   : 3;
         UBYTE Reserved1_UB   : 1;
         UBYTE CodingType_UB  : 4;

         //Byte 1
         UBYTE SampleSize_UB        : 2;
         UBYTE SamplingFrequency_UB : 3;
         UBYTE Reserved2_UB         : 3;

         //Byte 3
         UBYTE CodingTypeExt_UB  : 5;
         UBYTE Reserved3_UB      : 3;

         //Byte 4
         UBYTE SpeakerPlacement_UB  : 8;

         //Byte 5
         UBYTE LFEPlaybackLevel_UB  : 2;
         UBYTE Reserved4_UB         : 1;
         UBYTE LevelShiftValue_UB   : 4;
         UBYTE DownMixedInhibit_UB  : 1;

         //Byte 6
         UBYTE Reserved5_UB   : 8;

         //Byte 7
         UBYTE Reserved6_UB   : 8;

         //Byte 8
         UBYTE Reserved7_UB   : 8;

         //Byte 9
         UBYTE Reserved8_UB   : 8;

         //Byte 10
         UBYTE Reserved9_UB   : 8;

      };
      UBYTE pData_UB[10];
   }AUDIO_INFOFRAME;

   class CX320DrvIF: public CX3XXDrvIF
   {
   public:
      CX320DrvIF(ULONG BoardIdx_ULs);

      static ULONG GetNbBoard();

      virtual CBufferQueue * CreateBufferQueue();

      const BOARD_INFO * Board_GetInfo() {return &m_BoardInfo_X;}
      
      CDRVIF_STATUS Board_ReadEDID(int Idx_i, BYTE *pEDIDBuffer, ULONG *pBufferSize_UL);
      CDRVIF_STATUS Board_WriteEDID(int Idx_i, BYTE *pEDIDBuffer, ULONG BufferSize_UL);      
      CDRVIF_STATUS Board_GetTemperature(ULONG *pTemperature_UL);
      CDRVIF_STATUS Channel_HDMIRx_GetVideoInfo(UBYTE Channel_UB,BOOL32 *pVideoDetected_B =NULL, BOOL32 *pVideoLocked_B =NULL,BOOL32 *pHDMIMode_B =NULL,BOOL32 *pVideoInterlaced_B =NULL,ULONG *pVideoWidth_UL =NULL,ULONG *pVideoField0Height_UL =NULL,ULONG *pVideoField1Height_UL =NULL,float *pVideoRate_f =NULL,HDMI_SAMPLING *pSamplingStruct_E =NULL,HDMI_BIT_DEPTH *pBitDepth_E =NULL,DVI_COLOR_SPACE *pColorSpace_E =NULL,HDMI_3D_MODE *pMode3D_E =NULL, HDMI_AUDIO_TYPE *pAudioType_E =NULL, double *pPxlClk_d =NULL, ULONG *pVideoTotalWidth_UL =NULL,ULONG *pVideoField0TotalHeight_UL =NULL,ULONG *pVideoField1TotalHeight_UL =NULL);
      CDRVIF_STATUS Channel_HDMIRx_Start(UBYTE Channel_UB,RX_CHANNEL_START_PARAM *pHDMIParam_X);
      CDRVIF_STATUS Channel_HDMIRx_Stop( UBYTE Channel_UB );
      CDRVIF_STATUS Channel_HDMIRx_GetStatus( UBYTE ChannelIdx_UL, BOOL32 *pLocked_B);
      CDRVIF_STATUS Channel_HDMIRx_GetMode( UBYTE Channel_UB, BOOL32 *pValid_B, DVI_MODE *pMode_E );

      static void Helper_FillOnBoardAddresses(X320_ONBOARD_BUFFER_MAPPING MapID_e,  UBYTE ChnIdx_i,BUFFERQUEUE_PARAM *pBQParam_X, BOOL32 FieldMerge_B, ULONG LineSize_UL, ULONG OddLineNb_UL);
      static ULONG Helper_GetOnBoardAddress(X320_ONBOARD_BUFFER_MAPPING MapID_e, UBYTE ChnIdx_i, X320BRD_CHN_DATA_TYPE DataType_e, ULONG BufIdx_UL);
      void Board_SetOnBoardBufferBaseAddr(X320_ONBOARD_BUFFER_MAPPING MapID_e,UBYTE ChannelIdx_UB, BUFFERQUEUE_PARAM *pBQParam_X,BOOL32 InvertedPlanar_B,ULONG PxlSize_UL);
      void Board_SetOnBoardBufferBaseAddr(UBYTE ChannelIdx_UB, X320BRD_CHN_DATA_TYPE DataType_e, UBYTE BufferIdx_UB, ULONG BufferBaseAddr_UL );
      CDRVIF_STATUS ConfigureCSC(UBYTE Channel_UB, DVI_COLOR_SPACE InputColorSpace_E, DVI_DATA_FORMAT OutputDataFormat_E,BOOL32 IsHD_B);

      CDRVIF_STATUS Board_LoadDriverProperties();
      CDRVIF_STATUS Board_WatchdogTimerRegister(ULONG *pRegister_UL);
      virtual CDRVIF_STATUS Board_ArmCmd(ULONG Tag, const ULONG* pBuffer_UL=NULL, ULONG ParamSize_UL=0, ULONG *pRtrn_UL=NULL, ULONG RtrnSize_UL=0, ULONG *pSts_UL=NULL);


   protected:
      BOARD_INFO m_BoardInfo_X;      
   };

   class CX320DrvBufferQueue : public CBufferQueue
   {

      friend class CX320DrvIF;

   protected:

      CX320DrvBufferQueue(CX320DrvIF * pOwner_O);

   private:

      CX320DrvIF * m_pOwner_O;


   };
};

#endif //_CX320DRVIF_H_
