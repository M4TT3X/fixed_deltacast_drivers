#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=snd";

MODULE_ALIAS("pci:v00001B66d00000003sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000001sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000004sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000005sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000007sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000006sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000008sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000009sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d0000000Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d0000000Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d0000000Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000010sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d0000000Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000013sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000011sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000012sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001B66d00000014sv*sd*bc*sc*i*");
