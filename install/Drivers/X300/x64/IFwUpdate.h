
#ifndef _IFWUPDATE_H_
#define _IFWUPDATE_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "INotifyProgressFunctor.h"


class IFwUpdate 
{
public:
   virtual CDRVIF_STATUS  Board_FWPROMUpdate(UBYTE FirmwareID_UB, UBYTE *pBuffer_UB, ULONG BufferSize_UL,BOOL32 CheckFirmware_B=FALSE, INotifyProgressFunctor * pSignalProgress_fct=NULL)=0;
   virtual CDRVIF_STATUS  Board_ARMUpgrade( UBYTE *pBuffer_UB, ULONG BufferSize_UL )=0;
   virtual CDRVIF_STATUS  Board_GetArmRevID(ULONG *pArmRevId_UL)=0;
   virtual CDRVIF_STATUS  Board_GetArmSSN(ULONG *pArmSSNLS_UL,ULONG *pArmSSNIS_UL,ULONG *pArmSSNMS_UL)=0;
};

class IArmUpdateOldWay 
{
public:
   virtual CDRVIF_STATUS  Board_ARMUpgradeOldWay( UBYTE *pBuffer_UB, ULONG BufferSize_UL )=0;
};




#endif // _IFWUPDATE_H_

