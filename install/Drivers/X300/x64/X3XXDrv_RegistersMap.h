#ifndef _X3XXDRV_REGISTERSMAP_H_
#define _X3XXDRV_REGISTERSMAP_H_

#include "Register.h"

typedef enum
{
   IDX_FPGA_IMR_SHRMM32,
   IDX_FPGA_GCMD_SHRMM32,
   IDX_FPGA_IMR_RX_SHRMM32,
   IDX_FPGA_IMR_TX_SHRMM32,
   IDX_FPGA_TX0_CMD_SHRMM32,
   IDX_FPGA_TX1_CMD_SHRMM32,
   IDX_FPGA_TX2_CMD_SHRMM32,
   IDX_FPGA_TX3_CMD_SHRMM32,
   IDX_FPGA_TX4_CMD_SHRMM32,
   IDX_FPGA_TX5_CMD_SHRMM32,
   IDX_FPGA_TX6_CMD_SHRMM32,
   IDX_FPGA_TX7_CMD_SHRMM32,
   IDX_FPGA_WATCHDOG_SHRMM32,
   IDX_FPGA_HDMI_CTL_SHRMM32,
   IDX_FPGA_KEYER_CTRL_SHRMM32,
   IDX_FPGA_RX0_CMD_SHRMM32,
   IDX_FPGA_RX1_CMD_SHRMM32,   
   IDX_FPGA_RX2_CMD_SHRMM32,
   IDX_FPGA_RX3_CMD_SHRMM32,
   IDX_FPGA_RX4_CMD_SHRMM32,
   IDX_FPGA_RX5_CMD_SHRMM32,
   IDX_FPGA_RX6_CMD_SHRMM32,
   IDX_FPGA_RX7_CMD_SHRMM32,
   IDX_FPGA_TX_SCEN_SHRMM32,
   IDX_FPGA_TCE_CMD_SHRMM32,
   IDX_NB
} HDDRV_SHREG32;


#define FPGA_PCI_BAR                1

//Bar 0 register map
#define CFG_BD_ID                DEFINE_MMREG32(0,0x20)

//Bar 1 register map (WR)
#define FPGA_GCMD_SHRMM32        DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x00)
#define FPGA_SSR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x08)
#define FPGA_IMR_SHRMM32         DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x10)
#define FPGA_ICR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x18)
#define FPGA_END_OF_DMA_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x20)
#define FPGA_DMADATABC0_L_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x28)
#define FPGA_DMADATAADR0_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x30)
#define FPGA_DMADATABC1_L_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x38) /* DMA Channel 1 transfer count (in bytes). The transfer size of the DMAshould be programmed before it is started. It is used to generate an interrupt once the number of bytes transferred reaches the DMA_BC value. */
#define FPGA_DMADATAADR1_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x40) /* DMA Channel 1 Target Address : 32bit sdram address bit 31..30 are used to select the channel: �01� for RX1 - �00� for RX0  */
#define FPGA_WATCHDOG_SHRMM32    DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x48)
//#define FPGA_VCXO_CTL            DEFINE_MMREG32(FPGA_PCI_BAR,0x50)
#define FPGA_BRANDING_CHG_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x58)
#define FPGA_FIRMWARE_CMD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x60)
#define FPGA_FIRMWARE_ADD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x68)
//#define FPGA_TEMP_CMD            DEFINE_MMREG32(FPGA_PCI_BAR,0x70)
//#define FPGA_RESERVED            DEFINE_MMREG32(FPGA_PCI_BAR,0x78)
#define FPGA_UART_DATA_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x80)
#define FPGA_UART_CTRL_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x88)

//Bar 1 register map (RD)
#define FPGA_GSTS_MM32           DEFINE_MMREG32(FPGA_PCI_BAR,0x00)
#define FPGA_TEMP_STS_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x20)
#define FPGA_BRANDING_RLT_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x58)
#define FPGA_FIRMWARE_DATA_MM32  DEFINE_MMREG32(FPGA_PCI_BAR,0x60)
#define FPGA_SSN_LSB_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x68)
#define FPGA_SSN_MSB_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x70)
#define FPGA_UART_STS_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x78)
#define FPGA_UART_DATA_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x80)

//FPGA_GCMD_SHRMM32
// -> see Board.h
#define FPGA_GCMD_BIT_FW_WARM_LOAD                    0x00000001 // Set this bit to �1� to active loading of new FPGA firmware during next PCIe reset event.  (Write Only)
#define FPGA_GCMD_BIT_RX_PHASE_LATCH                  0x00000002 // �1� to latch all RX phase counters before registers reading (Auto-Clear)
#define FPGA_GCMD_BIT_RELAY_0                         0x00000004 // �1� to open relay and �0� to loop
#define FPGA_GCMD_BIT_RELAY_1                         0x00000008 // �1� to open relay and �0� to loop
#define FPGA_GCMD_RX0_LOGIC_CHANNEL_MAPPING           0x00000100 // �0� HD 80 and �1� to enable 3G level B  (3G 40)
#define FPGA_GCMD_RX1_LOGIC_CHANNEL_MAPPING           0x00000200 // �0� HD 80 and �1� to enable 3G level B  (3G 40)
#define FPGA_GCMD_RX2_LOGIC_CHANNEL_MAPPING           0x00000400 // �0� HD 80 and �1� to enable 3G level B  (3G 40)
#define FPGA_GCMD_RX3_LOGIC_CHANNEL_MAPPING           0x00000800 // �0� HD 80 and �1� to enable 3G level B  (3G 40)
#define FPGA_GCMD_MSK_ARM_BOOT                        0x03000000
#define FPGA_GCMD_BIT_ARM_RESET                       0x08000000 //�0� to reset ARM
#define FPGA_GCMD_BIT_TX0_TX1_MUX                     0x10000000 //�1� to out logical TX0 on physical TX1 (L3B only)
#define FPGA_GCMD_BIT_GTX_RX_RESET                    0x20000000 //�1� to reset Receiver part of GTX
#define FPGA_GCMD_BIT_GTX_TX_RESET                    0x40000000 //�1� to reset Transmitter part of GTX
#define FPGA_GCMD_BIT_GLOBAL_RESET                    0x80000000 //�1� to reset FPGA logic

#define FPGA_GCMD_BIT_ARM_BOOT(boot_space)                ((boot_space<<24)&FPGA_GCMD_MSK_ARM_BOOT)

#define ARM_BOOT_FLASH                 0x0 /* main flash memory is selected as boot space */ 
#define ARM_BOOT_ROM                   0x1 /* system memory is selected as boot space  */ 
#define ARM_BOOT_RAM                   0x2 /* embedded SRAM is selected as boot space */ 

//FPGA_GSTS_MM32
#define FPGA_GSTS_BIT_MEM_INIT_DONE       0x00000001  //�1� when DDR3 init done
#define FPGA_GSTS_BIT_MEM_PLL_LOCKED      0x00000002  //�1� when MIG PLL is locked
#define FPGA_GSTS_BIT_SSN_RDY             0x00000004  //�1� when SSN is available into FPGA
#define FPGA_GSTS_BIT_WARM_LOAD           0x00000008  //�1� when warm load is activated
#define FPGA_GSTS_BIT_SECURITY_READER     0x00000010  //�1� when ID and Branding are available into FPGA
#define FPGA_GSTS_BIT_SECURITY_ERROR      0x00000020  //�1� when no match between authorization codes
#define FPGA_GSTS_BIT_RELAY_0             0x00000040
#define FPGA_GSTS_BIT_RELAY_1             0x00000080
#define FPGA_GSTS_BIT_WATCHDOG_RUNNING    0x00000100  //�1� when watchdog is running
#define FPGA_GSTS_BIT_BUSY                0x00000200  //�1� when Busy - This handshake works for: NOR access, Phase measurement and Branding challenge
#define FPGA_GSTS_BIT_GENLOCK_B_REF       0x00000400
#define FPGA_GSTS_BIT_GENLOCK_B_LOCKED    0x00000800
#define FPGA_GSTS_BIT_GENLOCK_B_ALIGNED   0x00001000
#define FPGA_GSTS_BIT_GENLOCK_M_REF       0x00002000
#define FPGA_GSTS_BIT_GENLOCK_M_LOCKED    0x00004000
#define FPGA_GSTS_BIT_GENLOCK_M_ALIGNED   0x00008000
#define FPGA_GSTS_MSK_LINK_STATE          0x000E0000
#define FPGA_GSTS_MSK_LINK_WIDTH          0x00300000
#define FPGA_GSTS_BIT_GEN2_LINK           0x00400000  //�1� when link is running in PCIe Gen 2 Mode
#define FPGA_GSTS_BIT_MTG_B_LOCKED        0x01000000
#define FPGA_GSTS_BIT_MTG_M_LOCKED        0x02000000
#define FPGA_GSTS_BIT_LTC_COMPANION_CARD  0x04000000
#define FPGA_GSTS_BIT_SFP_MOD0_EN         0x08000000
#define FPGA_GSTS_BIT_SFP_MOD1_EN         0x10000000


#define PCIE_LINK_STATE_LO                   0x0   //�000� = L0
#define PCIE_LINK_STATE_PPM_L1               0x1   //�001� = PPM L1
#define PCIE_LINK_STATE_PPM_L2L3             0x2   //�010� = PPM L2 L3 Ready
#define PCIE_LINK_STATE_PM_PME               0x3   //�011� = PM_PME
#define PCIE_LINK_STATE_TRANS_ASPM_L0        0x4   //�100� = in or transitioning to from ASPM L0s
#define PCIE_LINK_STATE_TRANS_PPM_1          0x5   //�101� = transitioning to from PPM L1
#define PCIE_LINK_STATE_TRANS_PPM_L2L3       0x6   //�110� = transition to PPM L2 L3 Ready
#define FPGA_GSTS_GET_BIT_LINK_STATE(reg)    ((reg&FPGA_GSTS_MSK_LINK_STATE)>>17)               

//FPGA_SSR_MM32, FPGA_IMR_SHRMM32, FPGA_ISR_MM32 and FPGA_ISR_MM32
#define FPGA_SR_BIT_TEMP_ALARM         0x00000004  //Interrupt generated when a Temperature Alarm is detected
#define FPGA_SR_BIT_WATCHDOG_TRIG      0x00000008  //Interrupt generated when watchdog timeout
#define FPGA_SR_BIT_UC_INT             0x00000010  //Interrupt generated by the �C
#define FPGA_SR_BIT_ALARM_T_SET        0x00004000  //Interrupt generated when the temperature has reached 85�C
#define FPGA_SR_BIT_EOD                0x00010000  //Interrupt generated when the DMA 0 byte count has been reached
#define FPGA_SR_BIT_EOD1               0x00020000  //Interrupt generated when the DMA 1 byte count has been reached     
#define FPGA_SR_BIT_MB_TX_EMPTY        0x00040000  //Mailbox TX empty / End of �C Command 
#define FPGA_SR_BIT_MB_RX_FULL         0x00080000  //Mailbox RX full
#define FPGA_SR_BIT_UART_TX_EMPTY      0x00100000  //UART TX empty
#define FPGA_SR_BIT_UART_RX_FULL       0x00200000  //UART RX full
#define FPGA_SR_BIT_LTC_TOF            0x00400000  //Interrupt generated when the TCE card signals a top of frame

//MAILBOX_DATA indirect address
//Status
#define MB_CMD_STS              0x000
#define MB_SFP0_REG             0x180
#define MB_SFP1_REG             0x1C0


//Cmd rtrn
#define MB_CMD_RTRN             0x021
#define MB_CMD_PARAM            0x201

//Cmd
#define MB_CMD_TAG              0x200

#define MB_CMD_MAX_PARAM_LEN    128
#define MB_CMD_MAX_RTRN_LEN    MB_CMD_MAX_PARAM_LEN

//Comd Sts 
#define MB_CMD_STS_BIT_DONE                    0x00000001           /* �1� = Command done */
#define MB_CMD_STS_BIT_NOTDEFINED              0x00000002           /* �1� = Command not defined */
#define MB_CMD_STS_BIT_ERROR                   0x00000004           /* �1� = Error occurs during execution */
#define MB_CMD_STS_BIT_DATA                    0x00000008           /* �1� = Data are available (end of command which need answer) -> Data can be read at indirect address 0x21 */
#define MB_CMD_STS_BIT_WRONG_NBOF_PARAM        0x00000010           /* �1� = Number of parameters is wrong */
#define MB_CMD_STS_MSK_CMD_TAG                 0xFFFF0000           /* TAG of command concerned by status */

#define MB_CMD_STS_BIT_CMD_TAG(cmd_tag)        (((cmd_tag)<<16) & MB_CMD_STS_MSK_CMD_TAG)
#define MB_CMD_STS_GET_BIT_CMD_TAG(reg)        ((reg& MB_CMD_STS_MSK_CMD_TAG) >> 16)

//Cmd Tag
#define MB_GET_REVID                      0x00000000   /* Revision ID of ARM Firmware �yymmddvv- */
#define MB_GET_SSN                        0x00000001   /* Unique 96 bits Serial Number of ARM */
#define MB_DISABLE_PROCESS					   0x0000000A
#define MB_ENABLE_PROCESS	               0x0000000F

#define MB_CMDTAG_MSK_ID                  0x0000FFFF
#define MB_CMDTAG_BIT_MSK_ID(cmd_id)      ((cmd_id) & MB_CMDTAG_MSK_ID)
#define MB_CMDTAG_GET_BIT_MSK_ID(reg)     (((reg)&MB_CMDTAG_MSK_ID))



/** @name BRD_DMARQST_USRDATA_IDX Definition of the index for DMA request user data table *///@{
#define BRD_DMARQST_USRDATA_IDX_FLAGS       0   ///< This user data attached to a DMA request gives some flags specific to the board.
#define BRD_DMARQST_USRDATA_IDX_EOD_VALUE   1
//@}

#define BRD_DMARQST_USRDATA_FLAGS_MULTIRQST_FIRST (0x01) ///< This is the first DMA request from a multi-request data transfer     
#define BRD_DMARQST_USRDATA_FLAGS_MULTIRQST_LAST  (0x02) ///< This is the last DMA request from a multi-request data transfer   

#define DMA_PADDING_SIZE 0x80

//FPGA_WATCHDOG_CTRL_MM32
#define FPGA_WATCHDOG_MSK_VALUE     0x3FFFFFFF
#define FPGA_WATCHDOG_BIT_ENABLE    0x80000000
#define FPGA_WATCHDOG_BIT_REARM     0x40000000

#define FPGA_WATCHDOG_BIT_VALUE(val)   (val&FPGA_WATCHDOG_MSK_VALUE)

// -- FPGA_TCE_STS_MM32 : TCE Status Register R -------------------------------------------------------------

#define FPGA_TCE_STS_LOCKED            0x00000001  // TCE locked status: �0�: unlocked - �1�: locked
#define FPGA_TCE_STS_FRAMERATE         0x00000006  // LTC frame rate: �01�: 30 - �10�: 25 - �11� : 24 FPS

#define FPGA_GET_BIT_TCE_STS_LOCKED(reg)       ((reg)&FPGA_TCE_STS_LOCKED)
#define FPGA_GET_BIT_STS_FRAMERATE(reg)       (((reg)&FPGA_TCE_STS_FRAMERATE)>>1)

#define FPGA_GET_BIT_LTC_VALUE_FRAMES_UNIT(reg)  ((reg)&FPGA_LTC_VALUE_FRAMES_UNIT_MSK)
#define FPGA_GET_BIT_LTC_VALUE_FRAMES_TENS_UNIT(reg)  (((reg)&FPGA_LTC_VALUE_FRAMES_TENS_MSK)>>FPGA_LTC_POS_FRAMES_TENS)

#define FPGA_GET_BIT_LTC_VALUE_SECONDS_UNIT(reg)  (((reg)&FPGA_LTC_VALUE_SECONDS_UNIT_MSK)>>FPGA_LTC_POS_SECONDS_UNIT)
#define FPGA_GET_BIT_LTC_VALUE_SECONDS_TENS_UNIT(reg)  (((reg)&FPGA_LTC_VALUE_SECONDS_TENS_MSK)>>FPGA_LTC_POS_SECONDS_TENS)

#define FPGA_GET_BIT_LTC_VALUE_MINUTES_UNIT(reg)  (((reg)&FPGA_LTC_VALUE_MINUTES_UNIT_MSK)>>FPGA_LTC_POS_MINUTES_UNIT)
#define FPGA_GET_BIT_LTC_VALUE_MINUTES_TENS_UNIT(reg)  (((reg)&FPGA_LTC_VALUE_MINUTES_TENS_MSK)>>FPGA_LTC_POS_MINUTES_TENS)

#define FPGA_GET_BIT_LTC_VALUE_HOURS_UNIT(reg)  (((reg)&FPGA_LTC_VALUE_HOURS_UNIT_MSK)>>FPGA_LTC_POS_HOURS_UNIT)
#define FPGA_GET_BIT_LTC_VALUE_HOURS_TENS_UNIT(reg)  (((reg)&FPGA_LTC_VALUE_HOURS_TENS_MSK)>>FPGA_LTC_POS_HOURS_TENS)

#define FPGA_GET_BIT_LTC_VALUE_1_BINARY_GROUP(reg)  (((reg)&FPGA_LTC_VALUE_1_BINARY_GROUP_MSK)>>FPGA_LTC_POS_BINARY_GROUP1)
#define FPGA_GET_BIT_LTC_VALUE_2_BINARY_GROUP(reg)  (((reg)&FPGA_LTC_VALUE_2_BINARY_GROUP_MSK)>>FPGA_LTC_POS_BINARY_GROUP2)
#define FPGA_GET_BIT_LTC_VALUE_3_BINARY_GROUP(reg)  (((reg)&FPGA_LTC_VALUE_3_BINARY_GROUP_MSK)>>FPGA_LTC_POS_BINARY_GROUP3)
#define FPGA_GET_BIT_LTC_VALUE_4_BINARY_GROUP(reg)  (((reg)&FPGA_LTC_VALUE_4_BINARY_GROUP_MSK)>>FPGA_LTC_POS_BINARY_GROUP4)
#define FPGA_GET_BIT_LTC_VALUE_5_BINARY_GROUP(reg)  (((reg)&FPGA_LTC_VALUE_5_BINARY_GROUP_MSK)>>FPGA_LTC_POS_BINARY_GROUP5)
#define FPGA_GET_BIT_LTC_VALUE_6_BINARY_GROUP(reg)  (((reg)&FPGA_LTC_VALUE_6_BINARY_GROUP_MSK)>>FPGA_LTC_POS_BINARY_GROUP6)
#define FPGA_GET_BIT_LTC_VALUE_7_BINARY_GROUP(reg)  (((reg)&FPGA_LTC_VALUE_7_BINARY_GROUP_MSK)>>FPGA_LTC_POS_BINARY_GROUP7)
#define FPGA_GET_BIT_LTC_VALUE_8_BINARY_GROUP(reg)  (((reg)&FPGA_LTC_VALUE_8_BINARY_GROUP_MSK)>>FPGA_LTC_POS_BINARY_GROUP8)

#define FPGA_GET_BIT_LTC_VALUE_DROP_FRAME_FLAG(reg)  (((reg)&FPGA_LTC_VALUE_DROP_FRAME_FLAG)>>FPGA_LTC_POS_DROP_FRAME_FLAG)
#define FPGA_GET_BIT_LTC_VALUE_COLOR_FRAME_FLAG(reg)  (((reg)&FPGA_LTC_VALUE_COLOR_FRAME_FLAG)>>FPGA_LTC_POS_COLOR_FRAME_FLAG)

#define FPGA_GET_BIT_LTC_VALUE_POLARITY_CORR_30_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_POLARITY_CORR_30_FRAME)>>FPGA_LTC_POS_POLARITY_CORR_30_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF0_30_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF0_30_FRAME)>>FPGA_LTC_POS_BGF0_30_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF1_30_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF1_30_FRAME)>>FPGA_LTC_POS_BGF1_30_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF2_30_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF2_30_FRAME)>>FPGA_LTC_POS_BGF2_30_FRAME)

#define FPGA_GET_BIT_LTC_VALUE_POLARITY_CORR_25_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_POLARITY_CORR_25_FRAME)>>FPGA_LTC_POS_POLARITY_CORR_25_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF0_25_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF0_25_FRAME)>>FPGA_LTC_POS_BGF0_25_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF1_25_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF1_25_FRAME)>>FPGA_LTC_POS_BGF1_25_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF2_25_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF2_25_FRAME)>>FPGA_LTC_POS_BGF2_25_FRAME)

#define FPGA_GET_BIT_LTC_VALUE_POLARITY_CORR_24_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_POLARITY_CORR_24_FRAME)>>FPGA_LTC_POS_POLARITY_CORR_24_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF0_24_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF0_24_FRAME)>>FPGA_LTC_POS_BGF0_24_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF1_24_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF1_24_FRAME)>>FPGA_LTC_POS_BGF1_24_FRAME)
#define FPGA_GET_BIT_LTC_VALUE_BGF2_24_FRAME(reg)  (((reg)&FPGA_LTC_VALUE_BGF2_24_FRAME)>>FPGA_LTC_POS_BGF2_24_FRAME)
// -- FPGA_LTC_CHANNEL_MM32 : TCE Channel Access Parameters W -------------------------------------------------------------

#define FPGA_TCE_CHANNEL_LTC_CHOICE    0x0000000F  // LTC signal read choice
#define FPGA_TCE_CHANNEL_LTC_GLOBAL_LSB      0x00
#define FPGA_TCE_CHANNEL_LTC_GLOBAL_MSB      0x01
#define FPGA_TCE_CHANNEL_LTC_RX0_LSB         0x02
#define FPGA_TCE_CHANNEL_LTC_RX0_MSB         0x03
#define FPGA_TCE_CHANNEL_LTC_RX1_LSB         0x04
#define FPGA_TCE_CHANNEL_LTC_RX1_MSB         0x05
#define FPGA_TCE_CHANNEL_LTC_RX2_LSB         0x06
#define FPGA_TCE_CHANNEL_LTC_RX2_MSB         0x07
#define FPGA_TCE_CHANNEL_LTC_RX3_LSB         0x08
#define FPGA_TCE_CHANNEL_LTC_RX3_MSB         0x09
#define FPGA_TCE_CHANNEL_LTC_RX4_LSB         0x0A
#define FPGA_TCE_CHANNEL_LTC_RX4_MSB         0x0B
#define FPGA_TCE_CHANNEL_LTC_RX5_LSB         0x0C
#define FPGA_TCE_CHANNEL_LTC_RX5_MSB         0x0D
#define FPGA_TCE_CHANNEL_LTC_RX6_LSB         0x0E
#define FPGA_TCE_CHANNEL_LTC_RX6_MSB         0x0F
#define FPGA_TCE_CHANNEL_LTC_RX7_LSB         0x10
#define FPGA_TCE_CHANNEL_LTC_RX7_MSB         0x11

// -- LTC description masks and position bits -------------------------------------------------------------

#define FPGA_LTC_VALUE_FRAMES_UNIT_MSK           0x000000000000000F
#define FPGA_LTC_VALUE_FRAMES_TENS_MSK           0x0000000000000300
#define FPGA_LTC_VALUE_SECONDS_UNIT_MSK          0x00000000000F0000
#define FPGA_LTC_VALUE_SECONDS_TENS_MSK          0x0000000007000000
#define FPGA_LTC_VALUE_MINUTES_UNIT_MSK          0x0000000F00000000
#define FPGA_LTC_VALUE_MINUTES_TENS_MSK          0x0000070000000000
#define FPGA_LTC_VALUE_HOURS_UNIT_MSK            0x000F000000000000
#define FPGA_LTC_VALUE_HOURS_TENS_MSK            0x0300000000000000


#define FPGA_LTC_VALUE_1_BINARY_GROUP_MSK        0x00000000000000F0
#define FPGA_LTC_VALUE_2_BINARY_GROUP_MSK        0x000000000000F000
#define FPGA_LTC_VALUE_3_BINARY_GROUP_MSK        0x0000000000F00000
#define FPGA_LTC_VALUE_4_BINARY_GROUP_MSK        0x00000000F0000000
#define FPGA_LTC_VALUE_5_BINARY_GROUP_MSK        0x000000F000000000
#define FPGA_LTC_VALUE_6_BINARY_GROUP_MSK        0x0000F00000000000
#define FPGA_LTC_VALUE_7_BINARY_GROUP_MSK        0x00F0000000000000
#define FPGA_LTC_VALUE_8_BINARY_GROUP_MSK        0xF000000000000000

#define FPGA_LTC_VALUE_DROP_FRAME_FLAG           0x0000000000000200
#define FPGA_LTC_VALUE_COLOR_FRAME_FLAG          0x0000000000000400
#define FPGA_LTC_VALUE_POLARITY_CORR_30_FRAME    0x0000000004000000
#define FPGA_LTC_VALUE_POLARITY_CORR_25_FRAME    0x0800000000000000
#define FPGA_LTC_VALUE_POLARITY_CORR_24_FRAME    0x0000000004000000
#define FPGA_LTC_VALUE_BGF0_30_FRAME             0x0000040000000000
#define FPGA_LTC_VALUE_BGF0_25_FRAME             0x0000000004000000
#define FPGA_LTC_VALUE_BGF0_24_FRAME             0x0000040000000000
#define FPGA_LTC_VALUE_BGF1_30_FRAME             0x0400000000000000
#define FPGA_LTC_VALUE_BGF1_25_FRAME             0x0400000000000000
#define FPGA_LTC_VALUE_BGF1_24_FRAME             0x0400000000000000
#define FPGA_LTC_VALUE_BGF2_30_FRAME             0x0800000000000000
#define FPGA_LTC_VALUE_BGF2_25_FRAME             0x0000040000000000
#define FPGA_LTC_VALUE_BGF2_24_FRAME             0x0800000000000000

#define FPGA_LTC_POS_DROP_FRAME_FLAG            10
#define FPGA_LTC_POS_COLOR_FRAME_FLAG           11
#define FPGA_LTC_POS_POLARITY_CORR_30_FRAME     27
#define FPGA_LTC_POS_POLARITY_CORR_25_FRAME     59
#define FPGA_LTC_POS_POLARITY_CORR_24_FRAME     27
#define FPGA_LTC_POS_BGF0_30_FRAME              43
#define FPGA_LTC_POS_BGF0_25_FRAME              27
#define FPGA_LTC_POS_BGF0_24_FRAME              43
#define FPGA_LTC_POS_BGF1_30_FRAME              58
#define FPGA_LTC_POS_BGF1_25_FRAME              58
#define FPGA_LTC_POS_BGF1_24_FRAME              58
#define FPGA_LTC_POS_BGF2_30_FRAME              59
#define FPGA_LTC_POS_BGF2_25_FRAME              43
#define FPGA_LTC_POS_BGF2_24_FRAME              59

#define FPGA_LTC_POS_FRAMES_UNIT           0
#define FPGA_LTC_POS_FRAMES_TENS           8
#define FPGA_LTC_POS_SECONDS_UNIT          16
#define FPGA_LTC_POS_SECONDS_TENS          24
#define FPGA_LTC_POS_MINUTES_UNIT          32
#define FPGA_LTC_POS_MINUTES_TENS          40
#define FPGA_LTC_POS_HOURS_UNIT            48
#define FPGA_LTC_POS_HOURS_TENS            56


#define FPGA_LTC_POS_BINARY_GROUP1        4
#define FPGA_LTC_POS_BINARY_GROUP2        12
#define FPGA_LTC_POS_BINARY_GROUP3        20
#define FPGA_LTC_POS_BINARY_GROUP4        28
#define FPGA_LTC_POS_BINARY_GROUP5        36
#define FPGA_LTC_POS_BINARY_GROUP6        44
#define FPGA_LTC_POS_BINARY_GROUP7        52
#define FPGA_LTC_POS_BINARY_GROUP8        60

#endif //_X3XXDRV_REGISTERSMAP_H_
