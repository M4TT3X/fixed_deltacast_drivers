#ifndef _CBOARDUPGRADE_H_
#define _CBOARDUPGRADE_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "IBoardUpgrade.h"
#include "CX330DrvIF.h"

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

using namespace CX330DrvIF_NS;

class CX330BoardUpgrade : public CX330DrvIF, public IBoardUpgrade
{
public:
   
   CX330BoardUpgrade(ULONG BoardIdx_UL);
   ~CX330BoardUpgrade(){};
   
   char mpBoardName_c[64];
   
   virtual ULONG GetNbUpgradableStuff();
   virtual CDRVIF_STATUS GetUpgradeInfo(ULONG StuffIndex_UL, ULONG* pCurrentFirmware_UL, ULONG* pRequestedFirmware_UL);
   virtual CDRVIF_STATUS UpgradeFirmware(ULONG StuffIndex_UL, INotifyProgressFunctor *pSignalProgress_fct);
   virtual char * GetBoardName();
};



#endif