/********************************************************************************************************************//**
 * @internal
 * @file   	X320BoardTypes.h
 * @date   	2013/04/05
 * @author 	ja
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2013/04/05  v00.01.0000    ja       Creation of this file

 **********************************************************************************************************************/

#ifndef _X320BOARDTYPES_H_
#define _X320BOARDTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define X320BRD_PARTITION_WORD_SIZE       (2<<20)    
#define X320BRD_SECURED_DATA_ADDR         0x3F0000
#define X320BRD_PCB_VER_BATCH_DATA_ADDR   0x3E0000
#define X320BRD_NB_ONBRD_BUF   8 ///< Defines the number of on board buffers per data type.

typedef enum _X320_FW_ID{
   X320_FPGA_FW_ID_FAILSAFE,
   X320_FPGA_FW_ID_HDMI,
   X320_ARM_FW_ID_HDMI,
   NBOF_X320_FW_ID
}X320_FW_ID;

typedef enum{
   X320BRD_CHN_DATA_TYPE_VIDEO=0,
	X320BRD_CHN_DATA_TYPE_AUDIO,
	X320BRD_CHN_DATA_TYPE_VIDEO_PLANAR_V,
	X320BRD_CHN_DATA_TYPE_VIDEO_PLANAR_U,
   X320BRD_CHN_DATA_TYPE_THUMBNAIL
}X320BRD_CHN_DATA_TYPE;

typedef enum{
   X320BRD_BUFQ_DATA_TYPE_VIDEO,
	X320BRD_BUFQ_DATA_TYPE_AUDIO,
   X320BRD_BUFQ_DATA_TYPE_THUMBNAIL
}X320BRD_BUFQ_DATA_TYPE;

#define X320_ADVLP_VIDEO_ADDR_MAX 7 //Nb of secondary addresses for advanced line padding
#define X320_ADVLP_PARAM_MAX 7  //Nb of param for advanced line padding

#define X320BRD_BUFQUEUE_USRPARAM_IDX_DATACONTENT									0
#define X320BRD_BUFQUEUE_USRPARAM_IDX_CHANNEL_INDEX								1
#define X320BRD_BUFQUEUE_USRPARAM_IDX_EOD_VALUE										2
#define X320BRD_BUFQUEUE_USRPARAM_IDX_CURRENT_ONBRD_BUF_IDX						3
#define X320BRD_BUFQUEUE_USRPARAM_IDX_NB_ON_BOARD_BUFFER							4
#define X320BRD_BUFQUEUE_USRPARAM_IDX_VIDEO_ODD_SIZE								5
#define X320BRD_BUFQUEUE_USRPARAM_IDX_VIDEO_EVEN_SIZE								6
#define X320BRD_BUFQUEUE_USRPARAM_IDX_LB_ODD_SIZE									7
#define X320BRD_BUFQUEUE_USRPARAM_IDX_LB_EVEN_SIZE									8
#define X320BRD_BUFQUEUE_USRPARAM_IDX_ADVLP_PARAM									9
#define X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_ADVLP				(X320BRD_BUFQUEUE_USRPARAM_IDX_ADVLP_PARAM+X320_ADVLP_PARAM_MAX)
#define X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE						(X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_ADVLP+(X320_ADVLP_VIDEO_ADDR_MAX*X320BRD_NB_ONBRD_BUF))
#define X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_BASE				(X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X320BRD_BUFQ_DATA_TYPE_VIDEO*X320BRD_NB_ONBRD_BUF)
#define X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_AUDIO_BASE				(X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X320BRD_BUFQ_DATA_TYPE_AUDIO*X320BRD_NB_ONBRD_BUF)
#define X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_THUMBNAIL_BASE  		(X320BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X320BRD_BUFQ_DATA_TYPE_THUMBNAIL*X320BRD_NB_ONBRD_BUF)
//When adding new user param, you should update MAX_BUFFER_QUEUE_USER_PARAM 



#define X320BRD_BUFQUEUE_DATACONTENT_VIDEO			(1<<X320BRD_BUFQ_DATA_TYPE_VIDEO)
#define X320BRD_BUFQUEUE_DATACONTENT_AUDIO			(1<<X320BRD_BUFQ_DATA_TYPE_AUDIO)
#define X320BRD_BUFQUEUE_DATACONTENT_THUMBNAIL   	(1<<X320BRD_BUFQ_DATA_TYPE_THUMBNAIL)


#define X320BRD_BUFHDR_USRIDX_TIMESTAMP            0x0
#define X320BRD_BUFHDR_USRIDX_EVEN_PARITY          0x1
#define X320BRD_BUFHDR_USRIDX_AUDIO_TYPE           0x2
#define X320BRD_BUFHDR_USRIDX_AUDIO_INFOFRAME_0    0x3
#define X320BRD_BUFHDR_USRIDX_AUDIO_INFOFRAME_1    0x4
#define X320BRD_BUFHDR_USRIDX_AUDIO_CHANNEL_STS_0  0x5
#define X320BRD_BUFHDR_USRIDX_AUDIO_CHANNEL_STS_1  0x6


#define BUFHDR_STS_FRAMING_ERROR       0x1


#define X320BRD_DMA_PADDING_SIZE   0x200
#define X320BRD_DMA_ADDR_ALIGN     0x1000

#define X320BRD_ROUND_TO_x(Size, x)  (((ULONG)(Size) + x - 1) & ~(x - 1))
#define X320BRD_PAD_DMA_SIZE(Size)    X320BRD_ROUND_TO_x(Size, X320BRD_DMA_PADDING_SIZE)
#define X320BRD_ALIGN_DMA_ADDR(Size)  X320BRD_ROUND_TO_x(Size, X320BRD_DMA_ADDR_ALIGN)


/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _X320BOARDTYPES_H_
