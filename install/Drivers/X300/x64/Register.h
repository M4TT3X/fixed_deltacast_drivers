/********************************************************************************************************************//**
 * @file   Register.h
 * @date   2007/11/30
 * @Author cs
 * @brief  Contains macro to define and access to registers.
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/11/30  v00.01.0000    cs       Creation of this file
   2010/08/24  v00.01.0001    cs/ja    Add parenthesis around the definition of several macros (DEFINE_MMREG32, ...) to
                                       avoid strange behavior with operation ordering with things like :
                                       if (FPGA_GSTS_MM32&0xFF00).
   2013/07/08  v00.01.1000    jj       Modified the register declaration to fit the new model

 **********************************************************************************************************************/

#ifndef _REGISTER_H_
#define _REGISTER_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/**********************************************************************************************************************//**
 * @def REGISTER_ACCESS_PCI_RESOURCES_PTR 
 * @brief Defines a pointer to PCI resources array for memory-mapped register.
 *
 * All memory-mapped registers are defined based on a pointer to a PCI_RESOURCE array (for the 6 possible PCI BARs). To 
 * allow user to use its own pointer to resources, he can define this symbol before including the registers.h file
 * @code
 *	
 * #define REGISTER_ACCESS_PCI_RESOURCES_PTR GL_pPCIResouces_X
 * #include "registers.h"
 *
 *  #define MYREGISTER_MM32 DEFINE_MMREG32(0,0x100); // Defines a register on PCI Bar 0 and offset 100h
 *
 *	PCI_RESOURCE GL_pPCIResouces_X[6];
 *
 *  int main(void)
 *  {
 *		// Here, use IO Control to fill the GL_pPCIResouces_X array 
 *		...
 *      MYREGISTER_MM32 = 0x18;
 *  
 *
 * @endcode
 *
 * @remark 
 *************************************************************************************************************************/
#ifdef DRIVER

#include "DrvTypes.h"

#define REGISTER_ACCESS_PCI_RESOURCES_PTR pdx->pPCIResources_X

typedef volatile void*        PREGISTER;
typedef volatile ULONG*       PMM32_REGISTER;
typedef volatile UWORD*       PMM16_REGISTER;
typedef volatile UBYTE*       PMM8_REGISTER;
typedef volatile ULONGLONG*   PMM64_REGISTER;

#define DEFINE_MMREG64(bar,offset) (*(volatile ULONGLONG*)&REGISTER_ACCESS_PCI_RESOURCES_PTR[bar].pKernelAddr_UB[offset])
#define DEFINE_MMREG32(bar,offset) (*(volatile ULONG*)  &REGISTER_ACCESS_PCI_RESOURCES_PTR[bar].pKernelAddr_UB[offset])
#define DEFINE_MMREG16(bar,offset) (*(volatile UWORD*)  &REGISTER_ACCESS_PCI_RESOURCES_PTR[bar].pKernelAddr_UB[offset])
#define DEFINE_MMREG8(bar,offset)  (*(volatile UBYTE*)  &REGISTER_ACCESS_PCI_RESOURCES_PTR[bar].pKernelAddr_UB[offset])
#define DEFINE_IOREG(bar,offset)    (REGISTER_ACCESS_PCI_RESOURCES_PTR[bar].IOAddr_UL+offset)

#else

class ISkelDrvIF;

/* This should be a POD type */
typedef struct _MM32_REGISTER
{
   /* Fields are public to allow POD */
public:
   ISkelDrvIF*    mpDrvIf_O;
   ULONG          mBar_UL : 8;
   ULONG          mOffset_UL : 24;

   ULONG operator=(ULONG Value_UL);
   ULONG operator&=(ULONG Value_UL);
   ULONG operator|=(ULONG Value_UL);
   ULONG operator^=(ULONG Value_UL);
   operator ULONG();

} MM32_REGISTER, *PMM32_REGISTER;

#define DEFINE_MMREG32(bar, offset)    (*(DRVIF_ACCESS)->Register_Factory((bar), (offset)))
#define DEFINE_MMREG8(bar, offset)    (*(DRVIF_ACCESS)->Register_Factory((bar), (offset)))

#endif

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/*********************************************************************************************************************//**
 * @enum PCI_RESOURCE_TYPE
 * @brief Available value to define PCI hardware resources
 *************************************************************************************************************************/
typedef enum
{
	PCI_RESOURCE_TYPE_NOT_USED,	///< PCI BAR not used
	PCI_RESOURCE_TYPE_MEMORY,	///< Defines a memory-mapped PCI BAR
	PCI_RESOURCE_TYPE_IO,		///< Defines an I/O mapped PCI BAR

} PCI_RESOURCE_TYPE;



/**********************************************************************************************************************//**
 * @struct PCI_RESOURCE
 * @brief This structure contains formation to define and access to a PCI hardware resource 
 *
 * It will be filled by the driver with the GET_PCI_RESOURCES I/O control command.
 *************************************************************************************************************************/
#pragma pack(push, 4)
typedef struct
{
	PCI_RESOURCE_TYPE Type_E;			   ///< Specifies the PCI Hardware resource type
	ULONG Size_UL;						      ///< Specifies the size of the resources
	PHYSICAL_ADDRESS PhysicalAddr_PA;	///< Specifies the PCI physical address (relevant for memory-mapped resources)

	union
	{
		DELTA_VOID  pKernelAddr_UB;			   ///< Keeps an user mapped address for kernel mode
		DELTA_VOID  pUserAddr_UB;			      ///< Keeps an user mapped address for user-mode
		ULONG       IOAddr_UL;				      ///< Keeps the IO address.
	};

} PCI_RESOURCE_IOCTL;

typedef struct
{
	PCI_RESOURCE_TYPE Type_E;			   ///< Specifies the PCI Hardware resource type
	ULONG Size_UL;						      ///< Specifies the size of the resources
	PHYSICAL_ADDRESS PhysicalAddr_PA;	///< Specifies the PCI physical address (relevant for memory-mapped resources)

	union
	{
		UBYTE*   pKernelAddr_UB;			   ///< Keeps an user mapped address for kernel mode
		UBYTE*   pUserAddr_UB;			      ///< Keeps an user mapped address for user-mode
		ULONG    IOAddr_UL;				      ///< Keeps the IO address.
	};

} PCI_RESOURCE;

#pragma pack(pop)


#define DEFINE_SHRMMREG32(bar,offset)  bar,offset



/*
enum
{
   IDX_FPGA_TOTO_SHRMM32,
   NB_IDX
};

ULONG Val[NB_IDX];


#define FPGA_TOTO_SHRMM32 DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x00)

ShadowReg_Write32(FPGA_TOTO_SHRMM32,55);
Unsafe_ShadowReg_Write32(FPGA_TOTO_SHRMM32,55);

#define ShadowReg_Write32(reg,val) MyFct(IDX_##reg,reg,val,TRUE)
*/

#ifdef DRIVER

/* Macros to access to shadow registers */
#	define SHREG32_WR(reg) reg=pdx->pSHREG32_UL[SHREG32_##reg]
#	define SHREG32_RD(reg) pdx->pSHREG32_UL[SHREG32_##reg]

#	define MM64_TRACE_RD(reg) DbgOutput(TRACE #reg"=%016llxh\n",reg)
#	define MM32_TRACE_RD(reg) DbgOutput(TRACE #reg"=%08xh\n",reg)

#	ifdef __linux__
#		define IO_READ8(Offset)            IO_Read8(Offset)
#		define IO_READ16(Offset)           IO_Read16(Offset)
#		define IO_READ32(Offset)           IO_Read32(Offset)
#		define IO_WRITE8(Offset,Value)     IO_Write8(Offset,Value)
#		define IO_WRITE16(Offset,Value)    IO_Write16(Offset,Value)
#		define IO_WRITE32(Offset,Value)    IO_Write32(Offset,Value)
#	else
#		define IO_READ8(Offset)            READ_PORT_UCHAR((PUCHAR)Offset)
#		define IO_READ16(Offset)           READ_PORT_USHORT((PUSHORT)Offset)
#		define IO_READ32(Offset)           READ_PORT_ULONG((PULONG)Offset)
#		define IO_WRITE8(Offset,Value)     WRITE_PORT_UCHAR((PUCHAR)Offset,Value)
#		define IO_WRITE16(Offset,Value)    WRITE_PORT_USHORT((PUSHORT)Offset,Value)
#		define IO_WRITE32(Offset,Value)    WRITE_PORT_ULONG((PULONG)Offset,Value)
#	endif
#else //DRIVER
#	ifndef OUTPUT_FCT
#	  define OUTPUT_FCT printf
#	endif
#	define MM32_TRACE_WR(reg,Val) do{OUTPUT_FCT(#reg"=%08xh\n",Val); reg=Val;} while(0)
#	define MM32_TRACE_RD(reg) OUTPUT_FCT(#reg"=%08xh\n",reg)
#	define MM64_TRACE_RD(reg) OUTPUT_FCT(#reg"=%016I64xh\n",reg)
#endif //DRIVER


#endif // _REGISTER_H_
