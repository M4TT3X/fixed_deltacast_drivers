/********************************************************************************************************************//**
 * @internal
 * @file   	X320Drv_RegistersMap.h
 * @date   	2013/03/26
 * @author 	ja
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2013/03/26  v00.01.0000    ja       Creation of this file

 **********************************************************************************************************************/

#ifndef _X320DRV_REGISTERSMAP_H_
#define _X320DRV_REGISTERSMAP_H_

#include "Register.h"

#define X320_DMA_PADDING_SIZE 0x200

#ifdef DRIVER  /* Ensure that this is only used from driver side */
#define FPGA_MAILBOX_DI_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x90)
#define FPGA_MAILBOX_ADDR_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x98)
#endif 

#define FPGA_GCMD_MSK_GENLOCK_B_SOURCE                0x00007000
#define FPGA_GCMD_BIT_GENLOCK_B_ENABLE                0x00008000 //�1� to activate the Genlock feature
#define FPGA_GCMD_BIT_MTG_B_RESET                     0x00010000 //�1� to reset the Master Timing Generator 0
#define FPGA_GCMD_BIT_MTG_B_START                     0x00020000 //�1� to start the Master Timing Generator 0
#define FPGA_GCMD_MSK_GENLOCK_M_SOURCE                0x001C0000
#define FPGA_GCMD_BIT_GENLOCK_M_ENABLE                0x00200000 //�1� to activate the Genlock feature
#define FPGA_GCMD_BIT_MTG_M_RESET                     0x00400000 //�1� to reset the Master Timing Generator 0
#define FPGA_GCMD_BIT_MTG_M_START                     0x00800000 //�1� to start the Master Timing Generator 0

#define FPGA_GCMD_BIT_GENLOCK_B_SOURCE(genlock_source)      ((genlock_source<<12)&FPGA_GCMD_MSK_GENLOCK_B_SOURCE)
#define FPGA_GCMD_BIT_GENLOCK_M_SOURCE(genlock_source)      ((genlock_source<<18)&FPGA_GCMD_MSK_GENLOCK_M_SOURCE)

#define PCIE_LINK_WIDTH_UNKNOWN              0x0   
#define PCIE_LINK_WIDTH_1                    0x1   //'1' = 1x PCIe
#define PCIE_LINK_WIDTH_2                    0x2   //'2' = 2x PCIe
#define PCIE_LINK_WIDTH_4                    0x3   //'3' = 4x PCIe
#define PCIE_LINK_WIDTH_8                    0x4   //'4' = 8x PCIe
#define FPGA_GSTS_GET_BIT_LINK_WIDTH(reg)    ((reg&FPGA_GSTS_MSK_LINK_WIDTH) >>20)

#define FPGA_PARAM_ROM_A_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0xA0)
#define FPGA_PARAM_ROM_D_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0xA8)
#define FPGA_SSR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0xB0)
#define FPGA_IMR_RX_SHRMM32      DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xB8)
#define FPGA_ICR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0xC0)
#define FPGA_RX0_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xC8) 
#define FPGA_RX1_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xD0)
#define FPGA_CH0_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0xD8)
#define FPGA_CH1_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0xE0)
#define FPGA_CSC_A_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xE8)
#define FPGA_CSC_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xF0)

//Bar 1 register map (RD)
#define FPGA_SR_MM32             DEFINE_MMREG32(FPGA_PCI_BAR,0x08)
#define FPGA_ISR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x10)
#define FPGA_REVISION_ID_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x18)
#define FPGA_DMADATABC0_L_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x28)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x30)
#define FPGA_DMADATABC1_L_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x38)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x40)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x48)
//#define FPGA_VCXO_CTL_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x20)


#ifdef DRIVER  /* Ensure that this is only used from driver side */
#define FPGA_MAILBOX_DO_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x88)
#endif

#define FPGA_SR_RX_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x90)
#define FPGA_ISR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x98)
#define FPGA_RXBUFID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xA0)
#define FPGA_RX0_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xA8) 
#define FPGA_RX1_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xB0) 
#define FPGA_RX0_AUDIO_BC_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0xB8) 
#define FPGA_RX1_AUDIO_BC_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0xC0) 

#define FPGA_DMAADVRX_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x1E0)  /** Advanced DMA Settings � PC to FPGA */
#define FPGA_DMAADVTX_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x1E8)  /** Advanced DMA Settings � FPGA to PC */


//FPGA_GCMD_SHRMM32
// -> see Board.h


//FPGA_GSTS_MM32
// -> see Board.h



//FPGA_END_OF_DMA_MM32
#define FPGA_EOD_RXx(i)                (1<<i)
#define FPGA_EOD_TXx(i)                (1<<(8+i))

//FPGA_SSR_MM32, FPGA_IMR_SHRMM32, FPGA_ISR_MM32 and FPGA_ISR_MM32
// -> see Board.h

//FPGA_SSR_RX_MM32, FPGA_IMR_RX_SHRMM32, FPGA_ISR_RX_MM32 and FPGA_ISR_RX_MM32
#define FPGA_SR_RX_BIT_RXi_PARITY(i)        (0x1<<(i*4))
#define FPGA_SR_RX_BIT_RXi_OVERRUN(i)       (0x2<<(i*4))
#define FPGA_SR_RX_BIT_RXi_STS_CHANGE(i)    (0x4<<(i*4))
#define FPGA_SR_RX_BIT_RXi(i)               (0x8<<(i*4))


//FPGA_RXx_MM32
#define FPGA_RXx_BIT_CAPTURE_ENABLE          0x00000001  //�0� = Stop video capture - �1� = Start video capture
#define FPGA_RXx_MSK_INPUT_STYLE             0x00000006
#define FPGA_RXx_BIT_INTERLACED_MODE         0x00000008  //�0� = Progressive Stream - �1� = Interlaced Stream
#define FPGA_RXx_MSK_PACKING                 0x000000F0
#define FPGA_RXx_BIT_CSC_ENABLE              0x00000100  //�0� = Disable Color Space Converter - �1� = Enable Color Space Converter
#define FPGA_RXx_BIT_SHRINKER_ENABLE         0x00000200  //Reserved for future use
#define FPGA_RXx_BIT_FIELD_MODE              0x00000400  //�0� = Frame Mode - �1� = Field Mode
#define FPGA_RXx_BIT_AUDIO_CAPTURE_ENABLE    0x00000800  //�0� = Stop audio capture - �1� = Start audio capture
#define FPGA_RXx_BIT_FRAME_DEC_ENABLE        0x00001000  //�0� = No frame/field decimation - �1� = Activate frame/field decimation
#define FPGA_RXx_BIT_WINDOW_CROPPING         0x00002000  //�0� = Capture all frame/field - �1� = Capture window delimited by user.
#define FPGA_RXx_BIT_MSK_THUMBNAIL           0x0000C000  //Thumbnail capture
#define FPGA_RXx_MSK_FRAME_DEC_DEN           0x00FF0000  //Denominator of frame rate ratio. Must be bigger than numerator
#define FPGA_RXx_MSK_FRAME_DEC_NUM           0xFF000000  //Numerator of frame rate ratio. Must be smaller than denominator.

#define INPUT_STYLE_1X_444_12BIT    0x0		//�00� = 1x 4:4:4 � 12-bits/channel
#define INPUT_STYLE_2X_444_8BIT     0x1		//�01� = 2x 4:4:4 � 8-bits/channel
#define INPUT_STYLE_2X_422_12BIT    0x2		//�10� = 2x 4:2:2 � 12-bits/channel (For future use)
#define INPUT_STYLE_4X_420_8BIT     0x3		//�11� = 4x 4:2:0 � 8-bits/channel
#define FPGA_RXx_BIT_INPUT_STYLE(style)      ((style<<1)&FPGA_RXx_MSK_INPUT_STYLE)

#define PACKING_4444_8BIT  0x0
#define PACKING_444_8BIT  0x1
#define PACKING_422_8BIT  0x2
#define PACKING_444_10BIT 0x3
#define PACKING_422_10BIT 0x4
#define PACKING_444_12BIT 0x5
#define PACKING_422_12BIT 0x6
#define PACKING_PLANAR_8BIT 0x8
#define FPGA_RXx_BIT_PACKING(packing)     ((packing<<4)&FPGA_RXx_MSK_PACKING)

#define THUMBNAILS_DISABLE 0x0
#define THUMBNAILS_YUV     0x1
#define THUMBNAILS_RGB_24  0x2
#define THUMBNAILS_RGB_32  0x3
#define FPGA_RXx_BIT_THUMBNAIL(thumb)     ((thumb<<14)&FPGA_RXx_BIT_MSK_THUMBNAIL)

#define FPGA_RXx_BIT_FRAME_DEC_DEN(denominator)     ((denominator<<16) & FPGA_RXx_MSK_FRAME_DEC_DEN)
#define FPGA_RXx_BIT_FRAME_DEC_NUM(numerator)       ((numerator<<24) & FPGA_RXx_MSK_FRAME_DEC_NUM)


//FPGA_RXBUFID_MM32
#define FPGA_RXBUFID_MSK_RXi(i)                 (0x7<<(i*4))       
#define FPGA_RXBUFID_GET_BIT_RXi(reg,i)         ((reg>>(i*4))&0x7)
#define FPGA_RXBUFCORRUPTED_GET_BIT_RXi(reg,i)  ((reg>>(i*4))&0x8)


//FPGA_TEMP_STS_MM32
#define FPGA_TEMP_STS_MSK_DIE_TEMP  0x000003FF  //Current Die Temperature

#define FPGA_TEMP_STS_BIT_DIE_TEMP(reg) (reg&FPGA_TEMP_STS_MSK_DIE_TEMP)


//FPGA_CSC_A_MM32
#define FPGA_CSC_A_MSK_COEF_SEL     0x0000000F
#define FPGA_CSC_A_BIT_CSC_SEL      0x00000010  //�0� = RX0 CSC - �1� = RX1 CSC

#define CSC_COEF_K11    0x0
#define CSC_COEF_K12    0x1
#define CSC_COEF_K13    0x2
#define CSC_COEF_K21    0x3
#define CSC_COEF_K22    0x4
#define CSC_COEF_K23    0x5
#define CSC_COEF_K31    0x6
#define CSC_COEF_K32    0x7
#define CSC_COEF_K33    0x8
#define CSC_COEF_O1     0x9
#define CSC_COEF_O2     0xA
#define CSC_COEF_O3     0xB
#define CSC_COEF_C1     0xC
#define CSC_COEF_C2     0xD
#define CSC_COEF_C3     0xE

#define FPGA_CSC_A_BIT_COEF_SEL(coef)  (coef & FPGA_CSC_A_MSK_COEF_SEL)


//FPGA_CSC_D_MM32
#define FPGA_CSC_D_MSK_VALUE   0x0003FFFF //Coefficient value


//MAILBOX_DATA indirect address
//see Board.h

//Cmd Tag
//->see also Board.h
#define MB_WRITE_EDID_0                   0x00800002  // Program E-EDID for RX0
#define MB_WRITE_EDID_1                   0x00800003  // Program E-EDID for RX1
#define MB_READ_EDID_0                    0x00000004  // Read E-EDID for RX0
#define MB_READ_EDID_1                    0x00000005  // Read E-EDID for RX1
#define MB_START_RX0                      0x00040006  // Configure Front End for video capture
#define MB_START_RX1                      0x00040007  // Configure Front End for video capture
#define MB_STOP_RX0                       0x00000008  // Stop capture with Front End
#define MB_STOP_RX1                       0x00000009  // Stop capture with Front End
#define MB_HPD_RX0                        0x0000000b  // Create a Hot Plug Detect Event on RX0
#define MB_HPD_RX1                        0x0000000c  // Create a Hot Plug Detect Event on RX1
#define MB_RESET_RX0                      0x0000000d  // Start Reset sequence for RX0 input
#define MB_RESET_RX1                      0x0000000e  // Start Reset sequence for RX1 input
#define MB_CLEAR_I2C_ERROR                0x0000000f  // Clear the I2C error flag
#define MB_GET_AUDIOCHANNELSTS_0          0x00000010  // Get Audio Channel Status for RX0
#define MB_GET_AUDIOCHANNELSTS_1          0x00000011  // Get Audio Channel Status for RX1
#define MB_GET_AUDIOINFOFRAME_0           0x00000012  // Get Audio InfoFrame for RX0
#define MB_GET_AUDIOINFOFRAME_1           0x00000013  // Get Audio InfoFrame for RX1


//Status
#define MB_RX0_VIDEO_STS                  0x00000001
#define MB_RX0_TMDS_FREQ_STS              0x00000002
#define MB_RX0_LINE_WIDTH                 0x00000003
#define MB_RX0_FIELD_HEIGHT               0x00000004
#define MB_RX0_LINE_TOTAL_WIDTH           0x00000005
#define MB_RX0_FIELD_TOTAL_HEIGHT         0x00000006
#define MB_RX0_AUDIO_AUDIO_CHANNEL_STS_0  0x00000007
#define MB_RX0_AUDIO_AUDIO_CHANNEL_STS_1  0x00000008
#define MB_RX0_AUDIO_INFOFRAME_STS_0      0x00000009
#define MB_RX0_AUDIO_INFOFRAME_STS_1      0x0000000A
#define MB_RX1_VIDEO_STS                  0x00000011
#define MB_RX1_TMDS_FREQ_STS              0x00000012
#define MB_RX1_LINE_WIDTH                 0x00000013
#define MB_RX1_FIELD_HEIGHT               0x00000014
#define MB_RX1_LINE_TOTAL_WIDTH           0x00000015
#define MB_RX1_FIELD_TOTAL_HEIGHT         0x00000016
#define MB_RX1_AUDIO_AUDIO_CHANNEL_STS_0  0x00000017
#define MB_RX1_AUDIO_AUDIO_CHANNEL_STS_1  0x00000018
#define MB_RX1_AUDIO_INFOFRAME_STS_0      0x00000019
#define MB_RX1_AUDIO_INFOFRAME_STS_1      0x0000001A

// --MB_RXx_VIDEO_STS--
#define MB_RXx_VIDEO_STS_BIT_CLK_DETECTED             0x00000001  // �1� = HDMI Clock detected. There is an electrical activity on the dedicated pins of the Front-End.
#define MB_RXx_VIDEO_STS_BIT_CLK_LOCKED               0x00000002  // �1� = HDMI Clock locked. The clock is consistent and its frequency has been measured.
#define MB_RXx_VIDEO_STS_BIT_DE_FILTER_LOCKED         0x00000004  // �1� = HDMI Horizontal Filter Locked. This bit validates horizontal measures: Active Line Width & Total Line Width.
#define MB_RXx_VIDEO_STS_BIT_VER_FILTER_LOCKED        0x00000008  // �1� = HDMI Vertical Filter Locked. This bit validates vertical measures: Active Lines in Field, Total Lines in Field.
#define MB_RXx_VIDEO_STS_BIT_INTERLACED_MODE          0x00000010  // �0� = Input video with progressive timing - �1� = Input video with interlaced timing
#define MB_RXx_VIDEO_STS_BIT_HDMI_MODE                0x00000020  // �0� = DVI - �1� = HDMI
#define MB_RXx_VIDEO_STS_MSK_DEEP_COLOR_MODE          0x000000c0
#define MB_RXx_VIDEO_STS_MSK_INPUT_COLOR_SPACE        0x00000f00
#define MB_RXx_VIDEO_STS_BIT_3D_MODE_DETECTED         0x00001000  // �0� = Not 3D - �1� = 3D
#define MB_RXx_VIDEO_STS_BIT_ENCRYPTED_CONTENT        0x00002000  // �0� = The input stream is not HDCP encrypted. - �1� = The input stream is HDCP encrypted.
#define MB_RXx_VIDEO_STS_MSK_FRONT_END_OUTPUT_STYLE   0x0000c000
#define MB_RXx_VIDEO_STS_BIT_VIDEO_LOCKED             0x00010000  // �1� = Valid Video Data (Main global status). The capture is enabled. There is correspondence between the programmed format and the received format. 
#define MB_RXx_VIDEO_STS_BIT_AUDIO_LOCKED             0x00020000  // �1� = Audio Clock locked.
#define MB_RXx_VIDEO_STS_MSK_AUDIO_PACKET_TYPE        0x001c0000
#define MB_RXx_VIDEO_STS_MSK_PXL_REPETITION           0x01e00000
#define MB_RXx_VIDEO_STS_MSK_3D_PACKING_MODE          0x06000000
#define MB_RXx_VIDEO_STS_MSK_SAMPLING_STRUCT          0x08000000
#define MB_RXx_VIDEO_STS_BIT_I2C_ERROR_FLAG           0x80000000  // �1� = Error during I2C access (status can be corrupted)Must be cleared by right Mailbox command

#define MB_RXx_VIDEO_STS_GET_BIT_DEEP_COLOR_MODE(reg)    ((reg&MB_RXx_VIDEO_STS_MSK_DEEP_COLOR_MODE)>>6)
#define DEEP_COLOR_8_BIT      0x0  // �00� = 8-bit per channel
#define DEEP_COLOR_10_BIT     0x1  // �01� = 10-bit per channel
#define DEEP_COLOR_12_BIT     0x2  // �10� = 12-bit per channel

#define MB_RXx_VIDEO_STS_GET_BIT_INPUT_COLOR_SPACE(reg)  ((reg&MB_RXx_VIDEO_STS_MSK_INPUT_COLOR_SPACE)>>8)
#define INPUT_COLOR_SPACE_RGB_LIMITED        0x0   // '0000' = RGB limited
#define INPUT_COLOR_SPACE_RGB_FULL           0x1   // '0001' = RGB full
#define INPUT_COLOR_SPACE_YUV_601_LIMITED    0x2   // '0010' = YUV_601 limited
#define INPUT_COLOR_SPACE_YUV_709_LIMITED    0x3   // '0011' = YUV_709 limited
#define INPUT_COLOR_SPACE_XVYCC_601          0x4   // '0100' = XVYCC_601
#define INPUT_COLOR_SPACE_XVYCC_709          0x5   // '0101' = XVYCC_709
#define INPUT_COLOR_SPACE_YUV_601_FULL       0x6   // '0110' = YUV_601 full
#define INPUT_COLOR_SPACE_YUV_709_FULL       0x7   // '0111' = YUV_709 full
#define INPUT_COLOR_SPACE_SYCC_601           0x8   // '1000' = sYCC 601
#define INPUT_COLOR_SPACE_ADOBE_YCC_601      0x9   // '1001' = Adobe YCC 601
#define INPUT_COLOR_SPACE_ADOBE_RGB          0xa   // '1010' = Adobe RGB

#define MB_RXx_VIDEO_STS_GET_BIT_FRONT_END_OUTPUT_STYLE(reg) ((reg&MB_RXx_VIDEO_STS_MSK_FRONT_END_OUTPUT_STYLE)>>14)
#define FRONT_END_OUTPUT_STYLE_1X444_12_BIT  0x0  // �00� = 1x 4:4:4 � 12-bits/channel
#define FRONT_END_OUTPUT_STYLE_2X444_8_BIT   0x1  // �01� = 2x 4:4:4 � 8-bits/channel
#define FRONT_END_OUTPUT_STYLE_2X422_12_BIT  0x2  // �10� = 2x 4:2:2 � 12-bits/channel  (For future use)
#define FRONT_END_OUTPUT_STYLE_4X420_8_BIT   0x3  // �11� = 4x 4:2:0 � 8-bits/channel

#define MB_RXx_VIDEO_STS_GET_BIT_AUDIO_PACKET_TYPE(reg)  ((reg&MB_RXx_VIDEO_STS_MSK_AUDIO_PACKET_TYPE)>>18)
#define AUDIO_PACKET_TYPE_NONE            0x0 // �000� = No audio packet
#define AUDIO_PACKET_TYPE_AUDIO_SAMPLES   0x1 // �001� = Audio samples
#define AUDIO_PACKET_TYPE_HBR_PACKETS     0x2 // �010� = HBR packets
#define AUDIO_PACKET_TYPE_DSD_PACKETS     0x3 // �011� = DSD packets
#define AUDIO_PACKET_TYPE_DST_PACKETS     0x4 // �100� = DST packets

#define MB_RXx_VIDEO_STS_GET_BIT_PXL_REPETITION(reg)  ((reg&MB_RXx_VIDEO_STS_MSK_PXL_REPETITION)>>21)

#define MB_RXx_VIDEO_STS_GET_BIT_3D_PACKING_MODE(reg) ((reg&MB_RXx_VIDEO_STS_MSK_3D_PACKING_MODE)>>25)
#define PACKING_MODE_3D_FRAME          0x0  // �00� = Frame Packing 
#define PACKING_MODE_3D_TOP_BOTTOM     0x1  // �01� = Top-and-Bottom
#define PACKING_MODE_3D_SIDE_BY_SIDE   0x2  // �10� = Side-by-Side

#define MB_RXx_VIDEO_STS_GET_BIT_SAMPLING_STRUCT(reg) ((reg&MB_RXx_VIDEO_STS_MSK_SAMPLING_STRUCT)>>27)
#define SAMPLING_STRUCT_444   0x0   // �00� = 4:4:4
#define SAMPLING_STRUCT_422   0x1   // �01� = 4:2:2
#define SAMPLING_STRUCT_420   0x2   // �10� = 4:2:0

// --MB_RXx_TMDS_FREQ_STS--
#define MB_RXx_TMDS_FREQ_STS_MSK_TMDS_FREQ      0x0000ffff  // TMDS frequency measurement in MHz
#define MB_RXx_TMDS_FREQ_STS_MSK_TMDS_FREQFRAC  0x00ff0000  // TMDS fractional frequency measurement in 1/128 MHz

#define MB_RXx_TMDS_FREQ_STS_GET_BIT_TMDS_FREQ(reg)      (reg&MB_RXx_TMDS_FREQ_STS_MSK_TMDS_FREQ)
#define MB_RXx_TMDS_FREQ_STS_GET_BIT_TMDS_FREQFRAC(reg)  ((reg&MB_RXx_TMDS_FREQ_STS_MSK_TMDS_FREQFRAC)>>16)


// --MB_RXx_LINE_WIDTH--
#define MB_RXx_LINE_WIDTH_MSK_LINE_WIDTH     0x0000ffff  // Total number of active pixels per line

#define MB_RXx_LINE_WIDTH_GET_BIT_LINE_WIDTH(reg)  (reg&MB_RXx_LINE_WIDTH_MSK_LINE_WIDTH)


// --MB_RXx_FIELD_HEIGHT--
#define MB_RXx_FIELD_HEIGHT_MSK_FIELD0_HEIGHT   0x0000ffff  // The number of active lines in Field 0
#define MB_RXx_FIELD_HEIGHT_MSK_FIELD1_HEIGHT   0xffff0000  // The number of active lines in Field 1

#define MB_RXx_FIELD_HEIGHT_GET_BIT_FIELD0_HEIGHT(reg) (reg&MB_RXx_FIELD_HEIGHT_MSK_FIELD0_HEIGHT)
#define MB_RXx_FIELD_HEIGHT_GET_BIT_FIELD1_HEIGHT(reg) ((reg&MB_RXx_FIELD_HEIGHT_MSK_FIELD1_HEIGHT)>>16)

// --MB_RXx_LINE_TOTAL_WIDTH--
#define MB_RXx_LINE_TOTAL_WIDTH_MSK_LINE_TOTAL_WIDTH     0x0000ffff  // Total number of total pixels per line

#define MB_RXx_LINE_TOTAL_WIDTH_GET_BIT_LINE_TOTAL_WIDTH(reg)  (reg&MB_RXx_LINE_TOTAL_WIDTH_MSK_LINE_TOTAL_WIDTH)


// --MB_RXx_FIELD_TOTAL_HEIGHT--
#define MB_RXx_FIELD_TOTAL_HEIGHT_MSK_FIELD0_TOTAL_HEIGHT   0x0000ffff  // The number of total lines in Field 0
#define MB_RXx_FIELD_TOTAL_HEIGHT_MSK_FIELD1_TOTAL_HEIGHT   0xffff0000  // The number of total lines in Field 1

#define MB_RXx_FIELD_TOTAL_HEIGHT_GET_BIT_FIELD0_TOTAL_HEIGHT(reg) (reg&MB_RXx_FIELD_TOTAL_HEIGHT_MSK_FIELD0_TOTAL_HEIGHT)
#define MB_RXx_FIELD_TOTAL_HEIGHT_GET_BIT_FIELD1_TOTAL_HEIGHT(reg) ((reg&MB_RXx_FIELD_TOTAL_HEIGHT_MSK_FIELD1_TOTAL_HEIGHT)>>16)

//SW Board ID
#define SW_BRDID_MSK_SUB_PRODUCT_ID              0x000000ff
#define SW_BRDID_MSK_PRODUCT_ID                  0x0000ff00
#define SW_BRDID_MSK_NB_RX_CHN                   0x000f0000
#define SW_BRDID_MSK_NB_TX_CHN                   0x00f00000
#define SW_BRDID_MSK_PRODUCT_FAMILY              0xff000000

#define SW_BRDID_BIT_SUB_PRODUCT_ID(SubProductId)  ((SubProductId)&SW_BRDID_MSK_SUB_PRODUCT_ID)
#define SW_BRDID_GET_BIT_SUB_PRODUCT_ID(reg)       ((reg)&SW_BRDID_MSK_SUB_PRODUCT_ID)
#define SW_BRDID_BIT_PRODUCT_ID(ProductId)         (((ProductId)<<8)&SW_BRDID_MSK_PRODUCT_ID)
#define SW_BRDID_GET_BIT_PRODUCT_ID(reg)           (((reg)&SW_BRDID_MSK_PRODUCT_ID)>>8)
#define SW_BRDID_BIT_NB_RX_CHN(NbRxChn)            (((NbRxChn)<<16)&SW_BRDID_MSK_NB_RX_CHN)
#define SW_BRDID_GET_BIT_NB_RX_CHN(reg)            (((reg)&SW_BRDID_MSK_NB_RX_CHN)>>16)
#define SW_BRDID_BIT_NB_TX_CHN(NbTxChn)            (((NbTxChn)<<20)&SW_BRDID_MSK_NB_TX_CHN)
#define SW_BRDID_GET_BIT_NB_TX_CHN(reg)            (((reg)&SW_BRDID_MSK_NB_TX_CHN)>>20)
#define SW_BRDID_BIT_PRODUCT_FAMILY(ProductFamily) (((ProductFamily)<<24)&SW_BRDID_MSK_PRODUCT_FAMILY)
#define SW_BRDID_GET_BIT_PRODUCT_FAMILY(reg)       (((reg)&SW_BRDID_MSK_PRODUCT_FAMILY)>>24)


//HW Board ID
#define HW_BRDID_MSK_CHN_RX_ENABLE               0x00000003

#define HW_BRDID_BIT_CHN_RX_ENABLE(ChnRxEnable)  ((ChnRxEnable)&HW_BRDID_MSK_CHN_RX_ENABLE)


#define AUT0RIZATION_DODE        0x4443
#define BRANDING_DC              0x0

#endif //_X320DRV_REGISTERSMAP_H_
