/**********************************************************************************************************************
 
   Module   : CTypes
   File     : CTypes.h
   Created  : 2007/06/19
   Author   : cs

   Description : 

   
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/06/19  v00.01.0000    cs       Creation of this file


 **********************************************************************************************************************/

#ifndef _CTYPES_H_
#define _CTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

#ifndef SAFE_DELETE
#define SAFE_DELETE(pObj) do{if (pObj) {delete pObj; pObj=NULL;} }while(0)
#endif

#ifndef SAFE_DELETA
#define SAFE_DELETA(pObj) do{if (pObj) {delete [] pObj; pObj=NULL;} }while(0)
#endif

#ifndef SAFE_FREE
#define SAFE_FREE(Pointer_X) if (Pointer_X) { free(Pointer_X); Pointer_X = NULL; }
#endif

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

/* typedef used to define all time a 64 bit pointers */
#ifndef __cplusplus
typedef struct  
{
   union
   {
       unsigned long long Address_UH;
       void* pAddress_v;
   } Address_U; 
} DELTA_VOID;
#else
struct DELTA_VOID
{
   void operator=(void* pAddress_v)
   {
      this->Address_U.Address_UH = 0;
      this->Address_U.pAddress_v = pAddress_v;
   }

   void operator=(unsigned long long Address_UH)
   {
      this->Address_U.Address_UH = Address_UH;
   }

   union
   {
       unsigned long long Address_UH;
       void* pAddress_v;
   } Address_U; 
};

#endif


#if defined(__linux__)

#  ifndef __KERNEL__
#     include <stdint.h>
#     include <wchar.h>
      typedef unsigned char  BYTE;
      typedef uint8_t UBYTE;
      typedef unsigned short WORD;
      typedef uint16_t UWORD;
      typedef int32_t DWORD;
      typedef uint32_t UDWORD;
      typedef wchar_t WCHAR;
#  else                    /* __KERNEL__ */
#     include <linux/types.h>
      typedef u8      BYTE;
      typedef u8      UBYTE;
      typedef unsigned short     WORD;
      typedef u16     UWORD;
      typedef s32     DWORD;
      typedef u32     UDWORD;
      typedef unsigned char WCHAR;
#  endif							/* __KERNEL__ */

   typedef uint64_t __int64 ;
   typedef uint32_t ULONG;  //typedef unsigned long ULONG;
   typedef unsigned int BOOLEAN;
   typedef int BOOL32;
   typedef unsigned char UCHAR;
   typedef unsigned short USHORT;
   typedef int32_t    LONG;    //typedef long    LONG;
   typedef long long LONGLONG;
   typedef unsigned long long ULONGLONG;
   typedef int32_t SLONG;   //typedef signed long SLONG;
   typedef void * HANDLE;

   /*! Union used for 64 bits integer*/
   #ifndef LARGE_INTEGER_DEF
   #define LARGE_INTEGER_DEF
   typedef union
   {
      struct
      {
         DWORD           LowPart; /*!<LowPart*/
         LONG            HighPart;/*!<HighPart*/
      };
      struct
      {
         DWORD           LowPart;/*!<u.LowPart*/
         LONG            HighPart;/*!<u.HighPart*/
      } u;/*!<structure u.*/
      LONGLONG        QuadPart;/*!<QuadPart*/
   } LARGE_INTEGER;
   #  endif

   typedef LARGE_INTEGER PHYSICAL_ADDRESS;

#  ifndef TRUE
#  define TRUE               1	/*!<boolean TRUE value */
#  endif
#  ifndef FALSE
#  define FALSE              0	/*!<boolean FALSE value */
#  endif



#  ifndef NULL
#  define NULL               0              
#  endif

#define INFINITE 0xFFFFFFFF 
#define INVALID_HANDLE_VALUE 0xFFFFFFFF

typedef ULONG PID_TYPE;
#define INVALID_PID_VALUE -1;
#define PID_FORMAT "%08xh"

#elif defined(__APPLE__)
/******************************************* Mac OS X ***********************************************/

#include <libkern/OSTypes.h>
#include <mach/task.h>

#ifdef DRIVER
#include <libkern/libkern.h>
#else
#include <wchar.h>
#endif

typedef UInt8 UBYTE;
typedef UInt8 BYTE;
typedef UInt16 UWORD;
typedef UInt16 USHORT;
typedef UInt32 ULONG;
typedef UInt64 ULONGLONG;

typedef SInt8 SBYTE;
typedef UInt16 WORD;
typedef SInt32 DWORD;
typedef SInt32 LONG;
typedef SInt32 SLONG;
typedef SInt64 LONGLONG;

//#ifdef DRIVER
typedef SInt32 BOOL32;
//#else
//typedef SInt8 BOOL32;
//#endif

typedef wchar_t WCHAR;

#ifndef LARGE_INTEGER_DEF
#define LARGE_INTEGER_DEF
typedef union
{
   struct
   {
      DWORD           LowPart;
      LONG            HighPart;
   };
   struct
   {
      DWORD           LowPart;
      LONG            HighPart;
   } u;
   LONGLONG        QuadPart;
} LARGE_INTEGER;
#  endif

typedef LARGE_INTEGER PHYSICAL_ADDRESS;

typedef void* PVOID;

#  ifndef TRUE
#  define TRUE true
#  endif
#  ifndef FALSE
#  define FALSE false
#  endif

#define INFINITE 0xFFFFFFFF 
#define INVALID_HANDLE_VALUE 0xFFFFFFFF

#define INVALID_PID_VALUE TASK_NULL;

#ifdef DRIVER
typedef task_t PID_TYPE;
#define PID_FORMAT "%p"
#else
typedef UInt64 PID_TYPE;
#define PID_FORMAT "%ull"
#endif



/****************************************************************************************************/

#else /* defined(__linux__) */

   typedef unsigned char UBYTE;
   typedef unsigned long ULONG;
   typedef unsigned short UWORD;
   typedef signed long SLONG;
   typedef int BOOL32;

#  ifndef NULL
#  define NULL               0              
#  endif

#  ifndef DRIVER

#  include <windows.h>

   typedef LARGE_INTEGER PHYSICAL_ADDRESS;
#  endif /*DRIVER*/ 

typedef unsigned long PID_TYPE;
#define INVALID_PID_VALUE -1;
#define PID_FORMAT "%08xh"

#endif /*else defined(__linux__)*/

   /** @name TODO statement through #pragma 
 * This set of macro allows specifying a TODO that appears at compilation level
 * Statements like: 
 * @code 
 *  #pragma message(TODO "Fix this problem!") 
 * @endcode
 * Which will cause messages like: 
 * @code
 *   C:\Source\Project\main.cpp(47): TODO : Fix this problem! 
 * @endcode
 *
 *  to show up during compiles. Note that you can NOT use the  words "error" or "warning" in your reminders, since it will 
 *  make the IDE think it should abort execution. You can double click on these messages and jump to the line in question. 
 **///@{
//#define Stringize( L ) #L 
//#define MakeString( M, L ) M(L) 
//#define $Line MakeString( Stringize, __LINE__ ) 
//#define TODO __FILE__ "(" $Line ") : TODO : " 
//@}

#ifndef __APPLE__

#ifndef min
#define min(a,b) (((a)<(b))?(a):(b))
#endif

#endif

#define ROUND_TO_x(Size, x)  (((ULONG)(Size) + x - 1) & ~(x - 1))

#endif // _CTypes_H_
