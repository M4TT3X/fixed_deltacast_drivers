/********************************************************************************************************************//**
 * @internal
 * @file   	BoardData.h
 * @date   	2010/07/15
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/15  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _BOARDDATA_H_
#define _BOARDDATA_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "KernelObjects.h"
#include "DMAMngr.h"
#include "KernelSysCall.h"
#include "FPGA_DMAController_ADVLP.h"
#include "MailboxMngr.h"
#include "NorFlash.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct
{
 
   BOOL32                  pDMADoneIntFromPCI_B[MAX_DMA_CHANNELS];
   BOOL32                  pDMADoneIntFromFPGA_B[MAX_DMA_CHANNELS];
   ULONG                   pLastRxBufID_UL[8];     // Keeps the last RX Buffer IS cought during ISR
   SYSTEM_TIME             pLastRxSystemTime_X[8];  


   FPGA_DMACTRL_PARAM DMACtrlParam_X;

   MAILBOX_DATA   MailboxData_X;

   KOBJ_FAST_MUTEX         ArmCmdLock_X;        /* A lock protecting the �C from concurrent command */
   EVENT_ID                ArmTxEmptyEvent_e;

   NOR_FLASH_REG           NorFlashReg_X;

} BOARD_DATA;

#endif // _BOARDDATA_H_

