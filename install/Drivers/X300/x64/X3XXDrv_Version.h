/********************************************************************************************************************//**
 * @internal
 * @file   	X300Drv_Version.h
 * @date   	2010/08/26
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2012/02/15  v1.00.0000    gt       Creation of this driver
   2012/08/14  v1.00.0001    gt       Under Windows, map in kernel space only the first page of each buffer to avoid VHDERR_OPERATIONFAILED on VHD_StartStream with tiny kernel address space
   2012/05/22  v1.00.0002    ja       Add support for TX (HD44)
   2012/10/11  v1.00.0003    gt       Modify kernel memory allocation to reserve space only for headers
                             ja       Increase DMA max size to support 4096x2160
                                      Add support for field mode
                                      Add support for 4K mode
                                      Add handshake protection in arm uart
                             gt       New flash_read function
                             ja       Add flash_write_256 function that support 256k bloc size
                                      new FPGA (HD80=0x12092401, 3G40=0x12092404, 3G22=0x12101201, HD44=0x12100503, 3G04=0x12100505)
                                      new ARM (HD3G=0x12091901)
                                      modify KOBJ_CreateMemPool Tag to be Windows Drivers Verifier compliant
                                      Add OBJ_KERNEL_HANDLE attribute in WDMHLP_MapPhysicalMemory to be Windows Drivers Verifier compliant
                                      SkelDrv_Close in DispatchClose is not anymore into SAFEAREA to be Windows Drivers Verifier compliant
                                      new EventMngr_CloseByHandle function  that dereferences object (nonpaged memory leak fix)
   2012/10/11  v1.00.0004     ja      Add EXTRA_CFLAGS += -fno-stack-protector in Makefile and Makefile_client
   2012/10/23  v1.00.0005     ja      Add protection in EventMngr_DPCHandler to avoid KOBJ_SetSyncEvent call if the event has been closed meanwhile
   2012/10/25  v1.00.0006     cs      New fw (all : 1210250x):Wait end of SG list bm write in sdram before first sg desc prefetch from sdram
   2012/11/19  v1.00.0007     ja      Bug fix: error in Board_ARMUpgrade
   2012/11/29  v1.00.0008     ja      New FPGA (HD80=0x12112901): Field mode issue (relative to thumbnails)
   2013/01/07  v1.00.0009     ja      New FPGA (HD80=0x13010302): First trame/thumbnail corrupted
   2012/12/13  v1.00.0010     cs      Bug Fix: Add a missing pointer check after allocation before using it
   2012/10/30  v1.00.0011    cs       Includes an update in the PCI skel driver to get rid off the windows power management part.
   2012/11/12  v1.00.0012    cs/ja    Linux KOBJ_CreateMemPool allocation limited to 128K in old kernel. kmalloc has been replaced by vmalloc
   2012/11/12  v1.00.0013    cs/ja    MainDevice_Open/DispatchCreate and MainDevice_Release/DispatchClose functions have bad level of execution
   2012/12/06  v1.00.0014     ja      New FW (hd44: 12113003): Dual Link 1080p50/60 TX support
   2012/11/26  v1.00.0015     gt      New : DELTA-hd-elp-d 62, DELTA-sd-elp-d 80 and DELTA-3G-elp 40 integration
   2012/11/26  v1.00.0016     gt      New : thumbnails support on DELTA-3G-elp(-d) 40 
   2013/01/15  v1.00.0017    gt       Under Windows, map in kernel space only the first page of each buffer to avoid VHDERR_OPERATIONFAILED on VHD_StartStream with tiny kernel address space
   2013/01/21  v1.00.0018    gt       New ARM (3G=0x12113001 - HD62=0x12113011)
                                      New FPGA (HD80=0x13010302 - 3G40=0x13010401 - HD44=0x13012104 - 3G04=0x13011003 - 3G22=0x13012102 - HD62=0x13012101)
                                       - new : SD only protection
                                       - new : RX and TX dual-link 1080p50/60 support
                                       - bug fix : transmission may be corrupted due to a bad internal fifo (tx raw 1080p24 issue)
                                       - bug fix : field mode
                                       - bug fix : potential first frame (or thumbnail) corruption (due to pending interrupt of last rx)
   2013/01/23  v1.00.0019     ja      New FPGA (HD80=0x13012201 - 3G40=0x13012301)
                                       -bug fix: VBI did not work with thumbnail
   2013/01/24  v1.00.0020     ja      New FPGA (3G04=0x13012403 - 3G22=0x13012303)
                                       -bug fix: TX raw
   2013/01/28  v1.00.0021     gt      bug fix : PCI core interruption of the second DMA channel were not enabled
   2013/01/30  v1.00.0022     gt      bug fix : code was not locked
   2013/02/11  v1.00.0023     ja      New FPGA (3G22=0x13012506) Bug fix: YUVK transmission
   2013/02/14  v1.00.0024     ja      bug fix: 4K RX4567 onboard buffer addresses
   2013/02/25  v1.00.0025     ja      New FPGA (HD62=13022101) Bug fix: Improve robustness of SPI communication (ARM communication)
   2013/03/04  v1.00.0026     ja      New FPGA (HD44=13022802 - 3G04=13022701 - 3G22=13022703) Bug fix: Improve robustness of SPI communication (ARM communication)
   2013/03/05  v1.00.0027     ja      New FPGA (HD44=1301240A - 3G04=13012407 - 3G22=13012507) Patched 5.12 FPGA. Previous introduced 5.13 features and that wasn't acceptable.
   2013/01/29  v1.00.0028     ja      new: R3B/L3B support
                                      New FPGA (3G22=0x13012801)
                                       - new: R3B/L3B support
   2013/02/13  v1.00.0029     ja      bug fix: 3G dual is now configured as 3G11 at driver loading
   2013/03/11  V1.00.0030     JA      New: R3B support 3G formats
   2013/03/14  v1.00.0031     ja      New FPGA (3g22=0x13031102): data corrupted when bidir config as 3G20
   2013/02/11  v1.00.0032     ja      Bug fix: add prereset ARM cmd before ARM upgrading in the driver loading
   2013/02/26  v1.00.0033     ja      New ARM (3G22=0x13022601): HDMI monitoring
                                      New FPGA (3G22=0x13022503): HDMI monitoring
                                      New FPGA (3G04=0x13021501): HDMI monitoring
   2013/03/07  v1.00.0034     ja      New ARM (3G=0x13030701): HDMI new ship revision (new batch) support
   2013/03/04  v1.00.0035     ja      New FPGA: (HD80=0x13012801) 
                                      New FPGA: (3G40=0x13022702)
                                      New FPGA: (3G22=0x13032002)
                                      New FPGA: (HD44=0x13022802)
                                      New FPGA: (3G04=0x13022701)
                                      New FPGA: (HD62=0x13022805) : no chroma for dual packing
                                      New: Add support for RX YUVK 4224_8
   2013/03/28  v1.00.0036     ja      Bug fix: DELTA-3G-04 linux FW used 40 FW
   2013/03/13  v1.00.0037     ja      New FPGA (3G22=0x13031102)
                                               (3G40=0x13022702)
                                               (HD44=0x13031202)
                                               (HD62=0x13031205)
                                               (HD80=0x13012801): Change new_frame gestion to be compliant with RP168 in Raw Mode
   2013/04/10 v1.00.0038      ja      Bug fix: R3B and L3B HDMI monitoring support (Board_Init function)
   2013/04/11 v1.00.0039      ja      New FPGA  (3G04=0x13041102)
                                                (3G22=0x13041101)
                                                (HD80=0x13040301)
                                                (HD62=0x13041205)
                                                (HD44=0x13041202)
                                                (3G40=0x13041104): PCI core error fixed (HP compliance)
   2013/04/12 v1.00.0040      ja       Bug fix: Arm reset and uart init are only done if arm is present
   2013/04/16 v1.00.0041      ja       Bug fix: PCIeBridge_ISR_CatchInt(): DMACSR interruption was clear for the second DMA when the two END of DMA was received at the same time. That caused TX drops.
   2013/04/18 v1.00.0042      ja       New ARM (HD3G=13041801)
                                               (HD62=13041811): Gnlk getinputformat return 0x3f (invalid format) if the gnlk in not locked
   2013/04/18 v1.00.0043      ja       Bug fix: in linux makefile, need restart check did not work with more than one board
   2013/04/19 v1.00.0044      ja       New FW (3G22=0x13042503)
                                              (3G04=0x13042601)
                                              (3G40=0x13042504)
                                              (HD44=0x13042502)
                                              (HD62=0x13042501)
                                              (HD80=0x13042506):freeze bug fix
   2013/05/17 v1.00.0045      ja       Bug fix: arm upgrade: New reset to avoid loop in ARM Prereset while the ARM was empty or not running
   2013/05/17 v1.00.0046      bc       Add support Mac OSX + change KOBJ_SetSyncEvent mechanism  
   2013/05/27 v1.00.0047      ja       Bug fix: increase wait time in arm upgrade between the 2 reset (to avoid prereset loop)
   2013/05/29 v1.00.0048      ja      Mempool for DMA descriptors is now allocated dynamically in USBufMngr_RegisterUSBuf and freed in USBufMngr_UnregisterUSBuf
   2013/06/11 v1.00.0049      ja       DELTA-h4k support
   2013/06/19 v1.00.0050      ja       FPGA_RXx_CMD is now a shadow register and the DELTA-h4k CSC is now ONLINE
   2013/06/26 v1.00.0051      ja       Windows USBufMngr allocate several MDL to sustain size buffer limit
   2013/06/27 v1.00.0052      ja       FPGA_TXx_CMD_SHRMM32 was not defined in X300 board init
   2013/07/05 v1.00.0053      ja       ARM is not anymore reset at board init. Reset pin is just set to avoid reset when the arm is already running but still start the arm is it wasn't running.
   2013/07/05 v1.00.0054      gt       X320Drv_GetFirmwareRequest was called instead of X300Drv_GetFirmwareRequest function for a X300 board
   2013/07/16 v1.00.0055      ja       Bug fix: add HDMI monitoring support for DELTA-3G-elp 11/10/20/01/02/2c + DELTA-3G-elp 11 (R3B)
   2013/07/24 v1.00.0056      ja       Bug fix: Remove clearing of EndOfDMA SR bit in Board_CleanUp function
                                       Bug fix: add RX and TX cmd register clean up in Board_X300_Close and Board_X320_Close to avoid last frame repeat when killing TX streaming process
   2013/08/21 v1.00.0057      ja       Bug Fix: add relay control with registry key
   2013/05/17 v1.00.0058      ja       Bug fix: arm upgrade: New reset to avoid loop in ARM Prereset while the ARM was empty or not running
   2013/08/28 v1.00.0059      bc       DELTA-h4k support for OSX 
   2013/07/15 v1.00.0060      ja       New FPGA (3G40=0x13082005)
                                                (3G04=0x13082201) 
                                                (3G22=0x13082302): 3G level B support
   2013/09/02 v1.00.0061      ja       New FPGA (3G40=0x13083005)
                                                (3G22=0x13083002): Flag dual link was not raised for 3G Level B
   2013/09/09 v1.00.0062      ja       New FPGA (HDMI=0x13090901): Audio first frame corruption
   2013/09/13 v1.00.0063      ja       Bug: ANC-C2 et ANC-Y2 size was not correct for 3G levelB (Board_ISR)
   2013/09/17 v1.00.0064      ja       New FPGA (3G22=0x13091701): strange freezes issues since 3G LevelB support fix
   2013/09/18 v1.00.0065      ja       New FPGA (3g40=0x13091702): strange freezes issues since 3G LevelB support fix
   2013/09/25 v1.00.0066      ja       New ARM (HDMI=0x13090901): Mailbox is now only updated when its content changes and equalizer configuration improved for 27 MHz boundary
                                       New Mailbox kernel space access
   2013/09/25 v1.00.0067      ja       Bug: delta-x3xx.ko linux driver has been rename to delta-x300.ko for backward compatibility
   2013/09/25 v1.00.0068      ja       New FPGA (3G04=0x13092502): New Fitting to solve strange freeze issue
   2013/09/26 v1.00.0069      ja       New FPGA (3G04=0x13092602): New version of "Solve NMI errors and free in HP platforms" patch
   2013/09/26 v1.00.0070      gt       Bug fix : BoardDataMemPool_X was freed before disabling interrupt causing BSOD in ISR when an interrupt occurs after freeing BoardDataMemPool_X
   2013/09/30 v1.00.0071      bc       Move Board_GetArmID before FPGA version check in Board_X320_Init for Mac compatibility.
   2013/10/17 v1.00.0072      gt       Bug fix : SkelDrv_AllocDrvData was called after SkelDrv_DeviceAdd. Move SkelDrv_AllocDrvData from WDFEvtPrepareHW to WDFEvtDeviceAdd
   2013/10/17 v1.00.0073      gt       Bug fix : SkelDrv_FreeDrvData was called to soon. Move SkelDrv_FreeDrvData from WDFEvtReleaseHW to WDFEvtDeviceContextCleanup
   2013/10/21 v1.00.0074      gt       Bug fix : In Stream_Wakeup, CallBack_Wakeup_fct returned status is overwritten by MultiChnDMAMngr_Wakeup returned status. 
   2013/10/21 v1.00.0075     gt       Bug fix : In Stream_Wakeup, MultiChnDMAMngr_Wakeup is called before CallBack_Wakeup_fct
   2013/10/21 v1.00.0076     gt       Bug fix : In Board_X320_Sleep, an undefined shadow register was cleared   
   2013/11/12 v1.00.0077      ja       New FPGA (3G04=0x13110801): Correction of Thunderbolt
                                       New�FPGA (HD44=0x13111201): Correction of thunderbolt
                                       New FPGA (HD62=0x13111202): Correction of thunderbolt
                                       New FPGA (3G22=0x13111101): Correction of thunderbolt
   2013/11/18 v1.00.0078      gt       Bug fix : BSOD due to an access to IDX_FPGA_TXi_CMD_SHRMM32 shadow register in Board_X320_Sleep
   2013/11/22 v1.00.0079      ja       Bug fix: Board ref count mismatch (no-wakeup ref count)
   2013/11/13 v1.00.0080      gt       New FPGA (3G11=0x13111301): New : Add preliminary support of DELTA-3G-elp-key
                                       Bug fix : genlock 3G (TODO : REPORT CORRECTION ON OTHER BOARD!!!)
   2013/12/11 v1.00.0081      ja       New FPGA (3G22=0x13101001): Remove gtx_double_reset and create a tx_clk_valid signal to ensure right alignment of YUVK stream without genlock
   2014/01/03 v1.00.0082      ja       Bug fix: add CDRVIF_STATUS_WAKEUP_ACK and CDRVIF_STATUS_REMOVED error detection to protect direct register accesses in CX300DrvIF and CX320DrvIF
   2014/01/03 v1.00.0083      gt       Bug fix : Windows device manager returned code 10 after updating firmware
                                       Bug fix : the driver try to access the ARM from the failsafe firmware
                                       Bug fix : X300DRV_RELAYx_* driver parameters were not correctly handled
                                       Bug fix : Board_Close(pdx,0,0) is called in Board_Remove. Interruption are not enabled at this level. The Board_X300_ARMStopHDMI function timeouts. => move the call to Board_Sleep
                                       Bug fix : Board_X300_ARMStopHDMI returns FALSE even if the function succeed (MB_CMD_STS is not compared to the correct value)
   2013/09/18 v1.00.0084      gt       Bug fix : add FLASH_ResetSecuredCnt call in Board_ReadSecuredData to avoid bad config reading
   2013/09/18 v1.00.0085      gt       Bug fix : add FLASH_Sleep call in FLASH_WriteDataSq and FLASH_ReadDataSq to avoid bad config reading
   2013/09/18 v1.00.0086      gt       Bug fix : in FLASH_ResetSecuredCnt CE_N, OE_N and WE_N were not set
   2013/11/29 v1.00.0087      ja       New FPGA (3G22=0x1311130A): Advanced Line Padding (ADDR PC -> FPGA)
   2013/11/29 v1.00.0088      gt       New FPGA (3G22=0x13121201): Change ordering to "strict" at DDR level
   2013/11/29 v1.00.0089      gt       New FPGA (HD44=0x13121301): Change ordering to "strict" at DDR level
   2014/01/09 v1.00.0090      gt       New ARM (3GHD=0x14011001 - HD62=0x14011011) : Improved genlock support
   2014/01/22 v1.00.0091      ja       New FPGA (3G22=0x14012202): Correction of Advanced DMA (PC > FPGA : Positive Padding)
   2014/01/23 v1.00.0092      ja       New FPGA (HD44=0x14012203): Correction of Advanced DMA (PC > FPGA : Positive Padding)
   2014/01/23 v1.00.0093      ja       New ARM (HDMI=0x14012301): Back to the previous system : the mailbox is updated on every main loop. This isn't really a problem since this doesn't generate an interrupt.
   2014/01/24 v1.00.0094      ja       New FPGA (3G22=0x14012401): Correction of Rx Graphic Padding V210
   2014/01/28 v1.00.0095      ja       New FPGA (3G22=14012802): Correction of Advanced DMA (PC > FPGA : Reset state)
   2014/01/28 v1.00.0096      ja       Modify MailboxMngr_Ioctl to fit with IOCT cascade style to manage IOCT different than IOCTL_MAILBOX_ACCESS (CS request)
   2014/01/29 v1.00.0097      ja       New FPGA (3G22=14012902): Replace cpl_nb_distram by a vector at dma_rxpipe_advanced level
   2014/01/29 v1.00.0098      ja       New FPGA (HD44=0x14012903): ADVLP fix report
   2014/02/05 v1.00.0099      ja       New FPGA (3G40=0x14012901): ADVLP support
   2014/02/06 v1.00.0100      ja       Bug fix: dissociate PCIeBridge_ADVLP from PCIeBridge
   2014/02/07 v1.00.0101      ja       New FPGA (HD80=0x14020604): ADVLP support
   2014/02/13 v1.00.0102      gt       Bug fix: OS doesn't ask to reboot after firmware upgrade. Board_AddDevice returns always OK.
   2014/02/13 v1.00.0103      gt       Bug fix: cold-unplug with a running app result in a call to DeviceClose after DeviceReleaseHW. A BSOD occurs because BoardData is accessed after unallocation.
                                       => move SkelDrv_AllocDrvData in WdfDeviceAdd and SkelDrv_FreeDrvData in WDFDeviceContextCleanup. 
   2014/02/18 v1.00.0104      ja       New FPGA (HD80=0x14021704): New gestion of DDR read ordering at sd_manager level
   2014/02/19 v1.00.0105      ja       New FPGA (HD44=0x14021803): New gestion of DDR read ordering at sd_manager level
                                       New FPGA (HD62=0x14021902): New gestion of DDR read ordering at sd_manager level                          
                                       New FPGA (3G22=0x14021802): New gestion of DDR read ordering at sd_manager level                          
                                       New FPGA (3G40=0x14021801): New gestion of DDR read ordering at sd_manager level
   2014/02/19 v1.00.0106      bc       Bug fix : Bad sync event init under OSX
   2014/02/20 v1.00.0107      ja       New FPGA (3G11=0x14022001): Customisable clipping values at keyer level
                                                                   Support of ANC freeze in case of underrun when VBI/ANC mixer is enabled
                                                                   Change generation of mtg_ce in SD case to avoid deadlock	
                                                                   Advanced DMA Support
                                                                   New gestion of DDR read ordering at sd_manager level
                                                                   Remove current version of Video Line Padding
                                                                   Graphic Padding Support
                                                                   Full support of YUVK reception + remove of cd_n for SFP
                                                                   Remove gtx_double_reset and create a tx_clk_valid signal
                                                                   Update genlock_mtg entity to support genlock on 3G stream 
                                                                   Phase measurement on MTG references (can be BB)
   2014/02/20 v1.00.0108      ja       New FPGA (3G04=0x14021901): Advanced DMA Support
                                                                   Remove current version of Video Line Padding
                                                                   Graphic Padding Support
                                                                   Remove gtx_double_reset and create a tx_clk_valid signal
                                                                   Update genlock_mtg entity to support genlock on 3G stream 
                                                                   Change generation of mtg_ce in SD case to avoid deadlock
                                                                   New gestion of DDR read ordering at sd_manager level
   2014/02/21 v1.00.0109      ja       New FPGA (HDMI=0x14022002): New version of "Solve NMI errors and free in HP platforms" patch
                                                                   Add PLL on refclk (50MHz) to ensure 50% Duty Cycle + DNA readback improvement
                                                                   Support of Advanced DMA
                                                                   New gestion of DDR read ordering at sd_manager level
   2014/02/25 v1.00.0110      ja       New FPGA (HD80=0x14022504): Correction of g_link_A_B_n (1080p DL RX issues)
                                       New FPGA (HD44=0x14022503): Correction of g_link_A_B_n (1080p DL RX issues)
                                       New FPGA (HD62=0x14022502): Correction of g_link_A_B_n (1080p DL RX issues)
   2014/02/27 v1.00.0111      ja       New ARM (HDMI=0x14022701): return to a 100kHz I�C interface speed. 
   2014/02/28 v1.00.0112      ja       New FPGA (HD80=0x14022804): define range (1 down to 0) for g_channel_selection_0/g_channel_selection_1/g_link_A_B_n parameters
                                       New FPGA (HD44=0x14022803): define range (1 down to 0) for g_channel_selection_0/g_channel_selection_1/g_link_A_B_n parameters
                                       New FPGA (HD62=0x14022802): define range (1 down to 0) for g_channel_selection_0/g_channel_selection_1/g_link_A_B_n parameters
   2014/03/04 v1.00.0113      ja       New FPGA (3G11=0x14022801): Map sw_line_p into mtg_struct to allow output selection at keyer level 
   2014/03/05 v1.00.0114      gt       Bug fix : LinuxAllocator doesn't compile with newer kernel : kmalloc is undefined. Add the include of <linux/slab.h>)
   2014/03/06 v1.00.0115      ja       New ARM (HDMI=0x14030601): I2C initialization bug fix
   2014/03/06 v1.00.0116      ja       Bug fix: ADVLP registers were not defined for x320 boards
   2014/03/10 v1.00.0117      ja       Bug fix: DMA split engine was not working due to ADVLP -> DMA split has been patched but no ADVLP functions are available with DMA split (DMA size > 0x01F00000 )
   2014/03/11 v1.00.0118      ja       Bug fix: bug in the previous fix (DMA split engine)
   2014/03/13 v1.00.0119      ja       Improvement : reduce the flash upgrade duration 
   2014/03/13 v1.00.0120      ja       Bug fix:  PCI size and local bus size were increased in software for ADVLP scenario fix but now it is done in FPGA
                                       New FPGA (3G22=0x14031302): Change data_ready generation at dma_rxpipe level to help DMA flush
   2014/03/14 v1.00.0121      ja       New FPGA (HD44=0x14031403): Change data_ready generation at dma_rxpipe level to help DMA flush
                                       New FPGA (HD80=0x14031404): Change data_ready generation at dma_rxpipe level to help DMA flush
                                       New FPGA (HD62=0x14031502): Change data_ready generation at dma_rxpipe level to help DMA flush
                                       New FPGA (HDMI=0x14031101): Change data_ready generation at dma_rxpipe level to help DMA flush
                                       New FPGA (3G40=0x14031401): Change data_ready generation at dma_rxpipe level to help DMA flush
                                       New FPGA (3G04=0x14031501): Change data_ready generation at dma_rxpipe level to help DMA flush
   2014/03/17 v1.00.0122      ja       New FPGA (HD44=0x14031703): TX2 timeout bug on the previous FPGA (0x14031403)
   2014/03/17 v1.00.0123      ja       New FPGA (3G11=0x14031701): Define range (1 downto 0) for g_channel_selection_0/g_channel_selection_1/g_link_A_B_n parameters
                                                                   Change data_ready generation at dma_rxpipe level to help DMA flush
                                                                   Map "sd_a_rom_A_i" on darb port instead of "sd_a_rom_A" (RX 3G.B issue)
   2014/03/18 v1.00.0124      ja       Bug fix: X300_FPGA_FW_ID_3G11 was not integrated in Linux files
   2014/03/19 v1.00.0125      gt       Bug fix: all channel of the card are disabled after recovering from hibernate because the config was not read on wakeup
   2014/03/31 v1.00.0126      ja       New FPGA(3G40:14033101): 3G-B LinkB is not anymore detected from SMPTE352M but from the bit 13 of the RX CMD register
   2014/04/01 v1.00.0127      ja       Bug fix: Field mode underrun support: because in field mode, underrun interrupts are triggered one out of two, we need to increase STREAM_PROPERTIES_TX0_CHN_ONBOARD_FILLED_BUFFER_CNT on underrun (in field mode) to maintain correct value (see Stream_ISR() )
   2014/04/11 v1.00.0128      ja       New FPGA(3G04:14041101): Limit number of pending read at sd_manager level to avoid read buffer wrapping
   2014/04/14 v1.00.0129      gt       Bug fix : Under Linux, boardData structure is not cleared before enabling interrupt => kernel panic in case of shared IRQ
   2014/04/15 v1.00.0130      gt       Improvement : Improve FPGA firmware upgrade in user mode.
   2014/04/25 v1.00.0131      ja       InterlockedOp.h linux macro transformed to function to avoid to be precompiled (unknown symbol pv_cpu_ops)
   2014/05/05 v1.00.0132      ja       New FPGA (HDMI=0x14041401)
													New ARM (HDMI=0x14041001): 4K 4:2:0 support
   2014/05/02 v1.00.0133      ja       Bug fix: spinlock_t in KernelObject become raw_spinlock_t to avoid Linux RT interrupts freeze
   2014/05/20 v1.00.0134      ja       New FPGA(3G04=0x14051501):											
													New FPGA(3G22=0x14051502):	
													New FPGA(3G40=0x14051901):	
													New FPGA(HD44=0x14052003):	
													New FPGA(HD62=0x14052002):
													New FPGA(HD80=0x14051904):	
													New FPGA(3G11=0x14052004): Limit number of pending read at sd_manager level to avoid read buffer wrapping
													Improve robustness of read_buffer readback fsm at sd_manager level
	2014/03/19 v1.00.0135      gt       Imp : genlock HD62 uniformisation		
	2014/05/20 v1.00.0136      ja       New FPGA(HDMI=0x14052103): Limit number of pending read at sd_manager level to avoid read buffer wrapping
  2014/05/28 v1.00.0137      gt       New : SFP card support
  2014/06/04 v1.00.0138		ja			Bug fix: ULONG pointer in DMA_REQUEST struct has been replaced by DELTA_VOID type to maintain x86/x64 support

																						Improve robustness of read_buffer readback fsm at sd_manager level
   2014/08/27 v1.00.0142      ja				Bug fix: to avoid fw live upgrade on driver loading after a previous fw upgrade without reboot (cause crash), don't clear FPGA_GCMD_BIT_FW_WARM_LOAD on SHREG32_DEFINE if it has been set (Board_X300_DeviceAdd and Board_X320_DeviceAdd)
   2014/08/28 v1.00.0143      ja			Bug fix:  Call the entire remove() function execpt free_irq if request_irq() linux function fails in DriverEntre::probe

   2014/11/05 v1.00.0144      gt				New ARM(HD_3G : 0x14110501) : authorize new modules ("EB59HD2R-SN     ", "EB59HD2T-SN     ", "SPS-4110VW-2TG  ", "SPS-4110VW-2RG  " and "EB15HDRT-LN-PL  ")
   2014/06/17 v1.00.0145      bc       New board DELTA3g40hd40 with first FPGA (3G40HD40=0x14061701)
   2014/06/25 v1.00.0146      bc       New FPGA(3G40HD40=0x14062302):	
   2014/11/13 v1.00.0147      gt       New FPGA(HD44=0x14111203):	
                                             Round rather than truncate when converting from 10b to 8b
                                             Generate checksum error flags for ANC packets
                                             Pipeline TX_new_buffer to ensure sync with underun (same rising edge)
   2014/11/26 v1.00.148       bc       New FPGA(HD80=0x14112604)
                                       New FPGA(HD62=0x14112602)
                                       New FPGA(HD44=0x14112503)
                                       New FPGA(3G40HD40=0x14112601)
                                       New FPGA(3G22=0x14112502)
                                       New FPGA(3G04=0x14081401)
                                             Round rather than truncate when converting from 10b to 8b
                                             Generate checksum error flags for ANC packets
                                             Pipeline TX_new_buffer to ensure sync with underun (same rising edge)  
   2014/12/01 v1.00.149       bc       New FPGA(3G40=0x14112701)
                                       New FPGA(sfp22=0x14112703)
                                       New FPGA(3G11=0x14112702)
                                             Round rather than truncate when converting from 10b to 8b
                                             Generate checksum error flags for ANC packets
                                             Pipeline TX_new_buffer to ensure sync with underun (same rising edge)  
   2014/12/04 v1.00.150       bc       New FPGA(3G40HD40=0x14081801)
   2014/12/04 v1.00.151       bc       New FPGA(3G11=0x14120302)
                                       Bug fix : reset ARM HDMI in wake up function
- SDK 5.17
   2015/03/12 v1.00.152       ja       New FPGA(3G40=0x15031001): Change management of 4k flags, interrupts and locked bits to sync RX in multi-mode (3G.B 4K DL included)
                                       New FPGA(3G04=0x15031103): New port added to triple_rate_sdi_tx_interface.vhd to ensure sync in 3G.B 4K DL 
   2015/04/24 v1.00.153       ms       New FPGA(3G40=0x15033101)
                                       New FPGA(HD80=0x15030601)
                                       New : add LTC support
   2015/04/02 v1.00.154       bc       Bug fix : Bad initialization for USBufMngrLock mutex and move it to pdx. 
                                                 DMAMngr_Abort never ends if DMA is in progress
   2015/04/01 v1.00.155      gt       New : add IOCTL_MULTICHANNEL_ABORT_DMA 
                                       Bug fix : DMAMngr_Abort didn't unlock the user buffer in case of timeout
				       Bug fix : DMAMngr_Abort didn't unlock the page list buffer 
                                       Bug fix : DMAMngr_Abort didn't call BufMngr_DMADoneHandler associated with the DMA
   2014/10/09 v1.00.0156       bc      New: DELTA-3g-8c support FPGA (3G 8c=0x15022601)
   2014/11/28 v1.00.0157       bc      New support RAW and Video simultaneous capture on 3G40HD40. new FPGA (3G40HD40=0x14120304)      
   2014/11/28 v1.00.0158       bc      New FPGA (3G40HD40=0x14122201)     
   2015/03/23 v1.00.0159       bc      New FPGA (HD80=0x15030601)(3G40=0x15032301)(3G22=0x15030401)(HD44=0x15032303)
                                                (3G04=0x15031103)(HD62=0x15032302)(3G11=0x15032306)(SFP22=0x15032305): 
                                                Add overrun mechanism. Change capture mode value and latch in FABC register.
   2015/03/26 v1.00.0160       bc      New : Add low power state support 
   2015/04/24 v1.00.0161       bc      New : DELTA-3g-4c FPGA (3G 4c=0x15042201)
                                       New FPGA (3G 8c=0x15042202)
   2015/05/04 v1.00.0162       bc      New : Move low power state at driver level     
   2015/05/04 v1.00.0163       bc      New : DELTA-3g-4c FPGA (3G 4c=15043001) : 
                                          DDR @ 1400 MHz with Extended Bank Gestion
                                          Wait for all clk to be valid when multi link cases (TX) and Change order of muxs
                                          Overrun robustness improve : Add tag in ext_dout (toggle every new frame) 
                                          Add "thumbnail only" mode
                                          Add low_power_mode reset on channel_on bit of RX/TX CMD 
                                          Modify TX interrupt condition 
   2015/05/04 v1.00.0164       ja      Fix: Board_SetArmProcess function is not anymore called without arm presence in the Board_X300_Wakeup function to avoid arm access timeout
   2015/05/07 v1.00.0165       bc      New : DELTA-3g-4c FPGA (3G 4c=150050601) : Correction bug video packer planar
   2015/05/08 v1.00.0166       bc      New : add support for 27MHz time stamp on X330.
                                             DELTA-3g-4c FPGA (3G 4c=150050801) :
                                                Reset TX interruptions (SR_TX) when TX channel off 
                                                Time Stamp counter 27 MHz
                                                Phase Counter (RX + Genlock)
                                                Thumbnail inputs clipping
   2015/05/11 v1.00.0167       gt      New ARM(HDMI=0x15050801) : Changing RX0 signal source disrupts RX1 signal detection
   2015/05/07 v1.00.0168       bc      New : DELTA-3g-4c FPGA (3G 4c=15051301) : 
                                                Correction handle rx_int with dual coupled streams
                                                Stream reset based on signal stream_on and not tx_cmd anymore
   2015/03/23 v1.00.0169       bc      New FPGA (HD80=0x15051806)(HD44=0x15051801)(3G40HD40=0x15051902)(HD62=0x15051903):
                                          Correction of pcie_last_request construction to avoid erroneous Completion Timeout generation
                                          DMA Performance
                                          Revamp of sd_manager/sd_arbiter to enhance performances and ensure stability
                                          Add Timing Constraints for all pulse synchronizers
                                          Change behavior of sd_fifo_rst at line_in level to avoid erroneous write into DDR
   2015/03/23 v1.00.0170       bc      New FPGA (HD_80 0x15052201)(3G_40 0x15052205)(3G_22 0x15052203)(HD_44 0x15051801)
                                                (3G_04 0x15052202)(HD_62 0x15052101)(3G_11 0x15052102)(SFP22 0x15052104)
                                                (3G_40_HD40 0x1505220A) : 
                                                Add timing constraints for time stamp counters and phase counters
                                                Add Timing Constraints for all pulse synchronizers
                                                Change behavior of sd_fifo_rst at line_in level to avoid erroneous write into DDR
                                                Correction of g_max_ddr_read_request_nbr parameter
                                                Add Post Mortem Debug
   2015/05/27 v1.00.0172       bc      New FPGA (3G_04 0x15052301)
   2015/05/27 v1.00.0173       bc      New FPGA (3G 4c 0x15052801) change generation of req signals, Reset vtc when stream off + add one reset signal (tx_cfg_rst) to mtg_lmh
                                                (3G 8c 0x15052802) change generation of req signals, Reset vtc when stream off + add one reset signal (tx_cfg_rst) to mtg_lmh
   2015/06/05 v1.00.0174       bc      New FPGA (3G 4c 0x15060401) DDR @ 1200 MHz
   2015/06/08 v1.00.0175       bc      New FPGA (HD_80 0x15060505)(3G_40 0x15060502)(3G_04 0x15060802)(3G_11 0x15060501)
                                                (3G_40_HD40 0x15060801): Correction of sd_manager reset behaviour
   2015/06/08 v1.00.0176       gt      In ARMUART_FlashUpgrade, ARMUART_CMD_BL_INIT is send once before waiting for ACK
   2015/06/08 v1.00.0177       bc      New FPGA (3G_22 0x15060805)(HD_44 0x15060803)(HD_62 0x15060804)(3G_11 0x15060501)
                                                (SFP22 0x15060806)(HDMI 0x15060807) : Correction of sd_manager reset behaviour
   2015/06/10 v1.00.0178       gt      Bug fix : card configuration is changed by debuggers
   2015/06/08 v1.00.0179       bc      New FPGA (3G_8c 0x15061001) Improve timing score, DDR @ 1200 MHz, change generation of req signals, Reset vtc when stream off + add one reset signal (tx_cfg_rst) to mtg_lmh
   2015/06/23 v1.00.0180       bc      New FPGA (3G_8c 0x15062302) change generation of sdarb_ready signal, change generation of new_buffer
   2015/06/24 v1.00.0181       bc      New FPGA (3G_4c 0x15062301) change generation of sdarb_ready signal, change generation of new_buffer
   2015/06/25 v1.00.0182       bc      New : Disable temperature protection
   2015/06/26 v1.00.0183       bc      New : Device Pci ID for 4c
   2015/06/26 v1.00.0184       bc      New FPGA (3G_8c 0x15062501) Change MIG sys reset, Change Device ID
                                                (3G_4c 0x15062601) Change MIG sys reset
   2015/06/26 v1.00.0185       bc      New FPGA (3G_8c 0x15063001) Add generics parameters (remove thumbnail, planar and csc modules)
   2015/06/26 v1.00.0186       bc      New FPGA (3G_8c 0x15070201) Change RX_CDR_CFG, Change fifo level count, Line buffer more robust in relation to odd number of pixel (chroma swap).
                                                (3G_4c 0x15070101) Change RX_CDR_CFG, Change fifo level count, Line buffer more robust in relation to odd number of pixel (chroma swap).
   2015/07/06 v1.00.0187       bc      New FPGA (3G_8c 0x15070304) Correction new buffer generation for thumbnail
                                       New FPGA (3G_4c 0x15070303) Correction new buffer generation for thumbnail
   2015/07/24 v1.00.0188       gt      New FPGA (HD44 ) : new reset control
                                       New : add a workaround to retrieve timeout issue
   2015/08/11 v1.00.0189       ja      Bug fix: When setting PCIEBRIDGE_DMACSR_MM32, we must remove bit PCIEBRIDGE_DMACSR_BIT_RESET from what have been read in PCIEBRIDGE_DMACSR_MM32 because read and write bit 31 have not the same meaning
                                       New FPGA (HD44 0x15080305 ):Correction of Auto Cleared bits for Bar 0
   2015/08/17 v1.00.0190       ja      New FPGA (HD44 0x15081303): Auto Recovery Implementation Enhancement + Emulation of DMA/DDR timeout
                                       New: integration of the new DDR and DMA interrupt for autorecovery
   2015/08/19 v1.00.0191       gt      Bug fix : add a spinlock to avoid autorecovery feature to reset DMA state machine in the middle of DMA start. 
   2015/08/21 v1.00.0192       gt      Bug fix : autorecovery feature now restarts a DMA by writing all involved registers (because local bus address have been modified by the broken DMA)
   2015/08/21 v1.00.0193       ja      New:  Add driver property that contains the number of DDR and DMA recovery occurence
                                             Add registry keys to enable/disable DDR and DMA recovery
   2015/08/22 v1.00.0194       gt      New:  Add a counter in the buffer queue status to count corrupted buffers
   2015/08/22 v1.00.0195       gt      New FPGA (V6_HD_80_FW_VERSION=0x15082102, V6_3G_40_FW_VERSION=0x15082106, V6_3G_22_FW_VERSION=0x150825A4, 
                                       V6_HD_44_FW_VERSION=0x15082501, V6_3G_04_FW_VERSION=0x15082507, V6_HD_62_FW_VERSION=0x15082103, 
                                       V6_3G_11_FW_VERSION=0x15082105, V6_SFP22_FW_VERSION 0x15082508, V6_3G_40_HD40_FW_VERSION 0x150821A9) : autorecovery FPGA implementation
   2015/06/10 v1.00.0196      gt       Bug fix : do_gettimeofday is subject to NTP server time adjustement => replace by getrawmonotonic
   2015/09/01 v1.00.0197      bc       New FPGA (V6_HD_44_FW_VERSION=0x15090102) : Timing score improved	
   2015/09/01 v1.00.0198      bc       New FPGA (V6_HD_80_FW_VERSION=0x150903060, V6_3G_40_FW_VERSION=0x15082106, V6_3G_22_FW_VERSION=0x150903B7, V6_HD_44_FW_VERSION=0x150903A4, 
                                                 V6_3G_04_FW_VERSION=0x15082507, V6_HD_62_FW_VERSION=0x150903B3) : Timing score improved	
   2015/09/07 v1.00.0199      bc       Remove the constraint on bit 7 for rev ID in Board_X300_HWFirmwareCheck
   2015/09/07 v1.00.0200      bc       New FPGA (V6_3G_11_FW_VERSION 0x150904A8) : Management of corrupted_ddr bit
   2015/09/08 v1.00.0201      bc       New FPGA (3G 8c 0x15090702)(3G 4c 0x15090701) : Debug post-mortem, Distribution of CSC coefficients is function of scenario.
   2015/09/28 v1.00.0202      bc       New FPGA (3G 4c 0x15092801) :
                                           Change FSM for r_state (assert gtx reset when fail) (timeout en 3G Level-B)
                                           rst_n of RX asserted when flush (in case of switching) + monitoring flush
   2015/10/08 v1.00.0203      bc       New FPGA (3G 4c 0x15100603) :remove rst_n of RX when flush + robustess when overrun 
   2015/10/14 v1.00.0204      bc       New FPGA (3G 4c 0x15100901) :remove rst_n of RX when flush + robustess when overrun 
   2015/10/14 v1.00.0205      ja       New: remove DDR and DMA auto recovery patch.
   
   2015/10/20 v1.00.0207      bc       New FPGA (3G 4c 0x15101901)(3G 8c 0x15101903) : Add planar 10 bits support + activate planar 8 bits on SMPTE 425-5 
   2015/10/20 v1.00.0208      bc       New FPGA (3G 4c 0x15102701) : Correction of generation of new_line signal in multi-link scenarios (Bug planar 4K and thumbnails 4K)
   2015/11/10 v1.00.0209      bc       New FPGA (3G_40 0x15102805)(HD_44 0x15110202)(3G_40_HD40 0x15102102)(HD_80 0x15102802) : Revamp V6. 
   2015/11/12 v1.00.0210      ms       New : add LTC support for 4c and 8c
                                       Bug fix : no more LTC register reads for all X300, only for the supported ones
                                       Bug fix : Add check of bit busy to avoid LTC Timecode error on register reads.
   2015/11/18 v1.00.0211      bc       New FPGA (3G 4c 0x15110601)(3G 8c 0x15111802) : improve DDR efficiency to increase bandwidth
   2015/11/19 v1.00.0212      gt       Bug fix : In BufMngr, every RX user unlock try to a arm a transfer on buffer queue 0. 
                                       It can cause a drop on the TX channel associated to the buffer queue 0.
   2015/11/19 v1.00.0213      bc       New FPGA (3G 4c 0x15111901)(3G 8c 0x15111902) : Correction deadlock in FSM of local arbiter when reset of one RX/TX (Timeout RX)
   2015/11/25 v1.00.0214      bc       New FPGA (3G 4c 0x15112503)(3G 8c 0x15112504) : Correction Planar 4K Quadrant, small playlist ANC, TX RAW Timeout and 64 bytes erroneous.
   2015/11/10 v1.00.0215      bc       New FPGA (3G_22 0x15110201)(3G_04 0x15110203)(HD_62 0x15103001)(3G_11 0x15110205)(SFP22 0x15110204) : Revamp V6
   2015/11/14 v1.00.0216      gt       New : add a workaround to recover crashed flash with a reboot instead of a shutdown
   2015/11/18 v1.00.0217      gt       Bug fix : In BufMngr, every RX user unlock try to a arm a transfer on buffer queue 0. 
   2015/07/08 v1.00.0218      bc       New : add support for thumbnails on h4k (ARM HDMI 0x15111202)(FW HDMI 0x15111001)
   2015/11/19 v1.00.0219      bc       New FPGA (FW HDMI 0x15111803) : Revamp v6 + Misalignment between Data and Qualifier at input stage on DELTA-h4k
   2015/12/07 v1.00.0220      bc       New FPGA (3G_22 0x15111803)(HD_44 0x15111807)(3G_04 0x15111801)(HD_62 0x15111808)(3G_11 0x15111802)(SFP22 0x15111804): Correction of TX genlocked Field Mode
   2015/12/09 v1.00.0221      bc       New FPGA (3G_11 0x15120801): relay was commented
   2015/12/17 v1.00.0222      bc       New FPGA (H4K 0x15121102) ARM (H4K 0x15120802): Revamping + Read_done pulse in mailbox
                                       New FPGA (3G 4c 0x15120902) : planar 10bits + planar NTSC
   2015/12/18 v1.00.0223      gt       New ARM (K7: 0x15121801) : Previous version were built on a old version of the V6 project (0x14011010).
                                       The K7 project is rebuilt on the last V6 version (0x14110501) to include last modifications (new EmbeddedLib version among other things).
   2015/12/21 v1.00.0224      gt       SkelDrv_Sleep was not called in removeEx (Linux)
   2015/12/22 v1.00.0225      bc       New FPGA (3G 11 0x15122102) : Correction of input CSC mapping
   2015/12/17 v1.00.0226      bc       New FPGA (H4K 0x16010501) ARM (H4K 0x16010509): bug fix planar 4:2:0
   2016/01/08 v1.00.0227      bc       New ARM (K7 0x16010701) : PB1, PB5, PB8, PB9 and PB12 IO were not correctly configured
                                       New FPGA (3G 8c 0x15121703) : planar 10bits + planar NTSC + Correction bug timeout TX + Report carrier detect 
   2016/01/08 v1.00.0228      bc       New FPGA (3G40HD40 0x151207AD) : Basic check of active line size (corrupted buffer detection)
   2016/01/13 v1.00.0229      gt       New FPGA (4c 0x16010701) : PCIe negociation failure with different host devices (PCB 2.0 only)
                                       New FPGA (3G 11 0x16011302) : Correction of RX Line Padding
   2016/01/14 v1.00.0230      bc       New FPGA (8c 0x16011301) : PCIe negociation failure with different host devices (PCB 2.0 only)
- SDK 5.19
   2016/01/26 v1.00.0231      gt       New : compatibility with Linux kernel >= 4.1 (remove IRQF_DISABLED usage)
   2016/02/09 v1.00.0232      bc       New FPGA (HD_80 0x16020809)(3G_40 0x16020805)(3G_22 0x16020803)(HD_44 0x16020901)(3G_04 0x16020801)(HD_62 0x16020902)(3G_11 0x16020802)(SFP22 0x16020804)(3G_40_HD40 0x16020806)
                                             Waiting for version history from HW
   2016/01/26 v1.00.0233      ja       New FPGA (3G22 0x16012601): Relays support gets back to support DELTA-3G-elp 11 (R3B)
   2016/01/29 v1.00.0234      gt       New FPGA (hd44 0x16020102) : Bug fix : US transmission timeouts on TX0 and TX1 if started directly after powerup without a previous EUR transmission
   2016/02/16 v1.00.0235      ot       New FPGA (3G40HD40 0x16021102) : Bugfix in the case of 4K switching
   2015/11/18 v1.00.0236      ot       New FPGA (3G_04 0x15111801) to fix phase flip issue with genlocked field mode transmission
   2016/02/16 v1.00.0237      ja       Bug fix: some IOCTL used user space memory without KSYSCALL_CopyToUserSpace. That caused crash on linux with Z170 chipset.
   2016/03/02 v1.00.0238      bc       New FPGA (HD_80 0x16022502)(3G_40 0x16022406)(3G_22 0x16022404)(HD_44 0x16022407)(3G_04 0x16022301)(HD_62 0x16022501)(3G_11 0x16022403)(SFP22 0x16022405)(3G_40_HD40 0x16022402)
                                          Correction of TX 3G RAW issue + Cleaning mtg_lmh initialization + Improve robustness of multi-link resynchronization when switching + Replace vertical alignement by alignement after new_buffer flush
   2016/02/08 v1.00.0239      ms       Bug fix : LTC registers changed on V6 revamp
   2016/03/08 v1.00.0240      bc       New : add support LTC for DELTA-hd-elp-d 44 et DELTA-hd-elp-d 62
   2016/03/01 v1.00.0241      bc       Bug fix : To access second ANC stream in HD DUAL, ANC B was used on X300 versus ANC 2 on X330. Now, both ANC 2 and ANC B are allowed.
   2016/03/10 v1.00.0242      bc       New FPGA :  (3G_8C 0x16030804)(3G_4C 0x16030701):Planar 10 bits + DMA Master Revamp
                                                   (HD_80 0x16022502)(3G_40 0x16022406)(3G_22 0x16030703)(HD_44 0x16030705)(3G_04 0x16030701)(HD_62 0x16030706)(3G_11 0x16030702)(SFP22 0x16030704)(3G_40_HD40 0x16031002):Suppress TX dfifo increasing (4x3G.B broken)
   2015/08/10 v1.00.0243      ms       New Add support for DELTA-3G-elp-key-d-4K card
                                                            DELTA-3G-elp-key-d-2K card
   2016/02/18 v1.00.0244      bc       Bug fix : genlock on 1080psf24 video standard reference
   2016/03/03 v1.00.0245      ms       New FPGA (3G KEY 0xXXXXXXXX)
   2016/03/15 v1.00.0246      ms       New FPGA (3G KEY 0x16031421) (3G 11 0x116031101)
   2016/03/15 v1.00.0247      bc       New FPGA (3G 04 0x16031603) : Increase oc fifo depth : bug 4K 3G LevelB
   2016/03/22 v1.00.0248      ms       Add Linux driver support for keyer 4K and 2K 
   2016/03/23 v1.00.0249      bc       Bug fix : check device presence before calling SkelDrv_CheckDevicePresence to avoid blue screen if device is removed
   2016/03/31 v1.00.0250      ms       New FPGA (3G KEY 0x16033003) (8c 0x16033001) (4c 0x16032912) : Bug fix switcher 4K 425_5
   2016/04/11 v1.00.0251      bc       New : allow 425-3 interface on DELTA-3G-elp-key-d 2K

**********************************************************************************************************************/

#ifndef _X3XXDRV_VERSION_H_
#define _X3XXDRV_VERSION_H_

//#pragma message("====================================================================================================================")
//#pragma message("=  TODO List")
//#pragma message("====================================================================================================================")
//#pragma message(" > Add a way to trig a board shutdown during a 'shutown operation' the the power management sequence to properly handle relays behaviour")
//#pragma message("====================================================================================================================")


/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define X3XXDRV_VERMAJOR 1
#define X3XXDRV_VERMINOR 0
#define X3XXDRV_BUILD 251
#define X3XXDRV_PRODVER "1.00.0251\0"  

/** @def VALIDATE_ACCESS_CODE
 * The validate access code allows to check if the interface between driver and interface object are compatible.
 * If the object driver interface is linked to a greater minor code, it could inform that a newer driver is available, but, it
 * can work with the current one.
 * If the major access code mismatches, the interface object is not able to work  with the current driver.
 **/

#define X3XX_VALIDATE_ACCESS_CODE_MAJOR 0x0008 ///< Gives a version number of the interface. This number must MATCH !
#define X3XX_VALIDATE_ACCESS_CODE_MINOR 0x0001
#define X3XX_VALIDATE_ACCESS_CODE ((X3XX_VALIDATE_ACCESS_CODE_MAJOR<<16) | X3XX_VALIDATE_ACCESS_CODE_MINOR)


/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _X300DRV_VERSION_H_
