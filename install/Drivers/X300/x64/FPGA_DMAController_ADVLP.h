/********************************************************************************************************************//**
 * @file   	FPGA_DMAController_ADVLP.h
 * @date   	2013/12/04
 * @author 	ja
 * @version v00.01.0000 
 * @brief   This file describes the new behavior of the deltacast DMA controller on the FPGA side that support the advanced line padding.
 *
 * Some operations are required to perform either BLOCK MODE transfer or SCATTER/GATHER transfer. The scatter/gather
 * transfer works with a list of pages (physical addresses) given to the controller to achieve a tranfser to/from a 
 * spreaded in the physical memory.
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2013/12/04  v00.01.0000    ja       Creation of this file

 **********************************************************************************************************************/

#ifndef _FPGA_DMACONTROLLER_ADVLP_H_
#define _FPGA_DMACONTROLLER_ADVLP_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "PCIeBridge_ADVLP.h"
#include "FPGA_DMAController_ADVLP_Registry.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct  
{
   BOOL32  FullDuplexSupport_B;
   BOOL32  ScatterGatherSupport_B;
   PMM32_REGISTER pDMARegisterList_MM32[NB_DMACTRL_REGISTERS];
   ULONG pSGDescrTargetLBAddr_UL[2];

} FPGA_DMACTRL_PARAM;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

BOOL32 FPGADMACtrl_DMAMngrDMAStart(PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL, TRANSLATED_DMA_REQUEST * pRequest_X);
BOOL32 FPGADMACtrl_DMAAddRequest( PDEVICE_EXTENSION pdx,ULONG *pDMAChannel_UL, DMA_REQUEST *pDMARequ_X );
void FPGADMACtrl_Init( PDEVICE_EXTENSION pdx,FPGA_DMACTRL_PARAM * pParam_X );

#endif // _FPGA_DMACONTROLLER_ADVLP_H_
