/********************************************************************************************************************//**
 * @file   CSkelDrvIF.h
 * @date   2008/01/04
 * @Author Christophe Seyler
 * @brief  Driver abstraction class Windows definition
 *
 * Package : Low-level API
 * Company : DELTACAST
 *
 * @par Copyright notice 
 * Copyright 2007 by DELTACAST.  All rights reserved.
 * All information and source code contained herein is proprietary and confidential to DELTACAST.
 * Any use, reproduction, or disclosure without the written permission of DELTACAST is prohibited.
 *
 **********************************************************************************************************************//*

 ====================================================================================================================
 =  Version History                                                                                                 =
 ====================================================================================================================
   Date     	Version     	Author   Description
 ====================================================================================================================
   2006/11/09  v00.01.0000    cs       Creation of this file
   2007/07/11  v00.02.0000    cs/ot    Add a SAFE_CLOSE in CSkelDrvIF class destructor   
   2007/11/21  v00.03.0000    cs       Add 64-Bit compliance
   2008/01/07  v00.03.0001    cs       Documentation update
   2008/01/09  v00.04.0000    cs       Change the error constants to positive value to avoid conversion types warnings.
   2008/03/17  v00.05.0000    cs       Clear the memory area allocated by AllocateMemory() method
   2009/06/11  v00.06.0000    cs       add a #pragma comment (lib,"setupapi.lib") to avoid the need to manually add the library to
                                       use the class.
   2010/02/11  v00.07.0000    jm       new : Support of memory pool preallocation
                                       Add AllocateCommonBufferPool(), FreeCommonBufferPool() and GetMemMngrInfo()
   2011/02/07  v00.08.0000    cs       Add support for DMABuf link buffer in any cases, using the new DMABufMngr IOCTL.
   2011/03/07    v00.08.0001  cs       Fix a bug in the Windows implementation of CreateDMABuffer(). The function always increased
                                       the process working size whatever the allocation requires it or not.
   2011/03/11  v00.08.0002    cs       Fix a bug in the windows version of DestroyUSBuf() ... IN the call on VirtualFree, 
                                       the size was provided as parameter instead of flags. That was leading to a leak in the
                                       page count usage.
   2011/04/07  v00.08.0003    cs       Get rid off the Memory VirtualLock/Unlock out of the interface. These operation are now
                                       only done in the driver.
   2013/01/25  v00.08.0004    jj       Added parenthesis around the CDRVIF_SUCCEEDED's argument
   2013/12/10  v00.08.0005    ja       Add GetBoardNoAwakeRefCount() function at the end of the declaration table to respect retro compatibility
 ====================================================================================================================
 =  TODO List                                                                                                       =
 ====================================================================================================================
 - Split the CSkelDrvIF object into 2 objects because these is only a few number of method really OS-dependant. 
 Then, there are too much redoubling code shared between the Windows and the Linux CSkelDrvIF object.
 To do this, a CDrvInterfaceBaseObj symbol will be conditionally defined to either CDrvInterfaceWindows or 
 CDrvInterfaceLinux.
 By this way, all methods not OS-dependent will be highlighted.

 **********************************************************************************************************************/

#ifndef _IDRVINTERFACE_H_
#define _IDRVINTERFACE_H_

#define CDRVIF_VERSION 0x00070000

/***** INCLUDES SECTION ***********************************************************************************************/

#ifdef __linux__
#include <asm/ioctl.h>

typedef const char * DRIVER_ID_TYPE;
typedef int BOARD_HANDLE;

#elif defined(__APPLE__)

#include <IOKit/IOKitLib.h>

typedef io_service_t BOARD_HANDLE;
typedef CFMutableDictionaryRef DRIVER_ID_TYPE;

#else

#include <windows.h>
#include <winioctl.h>
#define _SYS_GUID_OPERATORS_  
#include <objbase.h>
#include <initguid.h>
#include "setupapi.h"

typedef GUID DRIVER_ID_TYPE;
typedef HANDLE BOARD_HANDLE;

#endif

#include "Register.h"
#include "SkelIoctls.h"
#include "EventMngrTypes.h"
#include "CommonBufMngrTypes.h"
#include "CTypes.h"
#include "DMABufMngrTypes.h"
#include "KThreadMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/** @defgroup CDRVIF_STATUS_GROUP CSkelDrvIF Status */
//@{
typedef ULONG CDRVIF_STATUS;

#define CDRVIF_STATUS_SUCCESS                    0  //!< Success Status
#define CDRVIF_STATUS_TIMEOUT                    1  //!< Timeout occurs during processing
#define CDRVIF_STATUS_IO_ERROR                   2  //!< An error occurs during driver access
#define CDRVIF_STATUS_BAD_ARG                    3  //!< Bad argument given to a method
#define CDRVIF_STATUS_NO_MEM                     4  //!< No more memory to complete the operation 
#define CDRVIF_STATUS_NOT_IMPLEMENTED_YET        5  //!< This feature is not implemented yet
#define CDRVIF_STATUS_RESOURCE_NOT_AVAILABLE     6  //!< This feature is not available due to resource restriction
#define CDRVIF_STATUS_FEATURE_NOT_AVAILABLE      7  //!< This feature is not available 
#define CDRVIF_STATUS_PREALLOCATION_TOO_SMALL    8  //!< The preallocated pool is too small 
#define CDRVIF_STATUS_CMD_STS_BIT_ERROR          9  //!< Command status bit error
#define CDRVIF_STATUS_FAILED                    10  ///< The operation has failed
#define CDRVIF_STATUS_PARAMETERS_MISMATCH       11  ///< Given parameters have incompatible values
#define CDRVIF_STATUS_NO_MORE_SPACE             12  ///< There is no more space available for the current operation
#define CDRVIF_STATUS_BAD_STATE                 13  ///< The request cannot be processed in the current state.
#define CDRVIF_STATUS_REMOVED                   14  ///< The device has been removed
#define CDRVIF_STATUS_WAKEUP_ACK                15  ///< The wakeup needs to be acknowledged before continuing
#define CDRVIF_STATUS_LTC_SOURCE_UNLOCKED       16  ///< LTC source unlocked
#define CDRVIF_STATUS_LOW_POWER                 17  ///< The device is in low power mode
//@}
//@}


/***** MACROS DEFINITION **********************************************************************************************/

/** @defgroup CDRV_STATUS_MACRO_GROUP CSkelDrvIF test status macros*/
/* These macros help user to test returning values.*/
//@{
#define CDRVIF_SUCCEEDED(Sts) (!(Sts))   /*!< Macro that test the given value to return a non-zero value in success case */
#define CDRVIF_FAILED(Sts)    (Sts)    	 /*!< Macro that test the given value to return a non-zero value in failed case */
//@}

/***** STRUCUTRES DEFINITION ******************************************************************************************/

/**********************************************************************************************************************//**
* @struct BOARD
* @brief Internal data to keep pointer to opened driver and all board resources mapped into user space.
*
* The access to these information could be done by CSkelDrvIF::GetPCIResourcesPtr() to allow user to
* access to board registers directly.
*
* For more information refer to ref CDRV_DOC_BOARD_RES_PAGE "Board resources and registers access".
*
* @remark Due to the presence of Windows HANDLE inside the structure, this one is OS-dependent
*************************************************************************************************************************/
typedef struct
{
   BOARD_HANDLE       Board_H;               ///< Keeps Windows HANDLE to driver
   PCI_RESOURCE       pPCIResources_X[6];    ///< Keeps user space mapped resources
#ifdef __APPLE__
   io_connect_t       UserClient_H;          ///< Keeps user client handle
#endif
} BOARD;

/**********************************************************************************************************************//**
* @struct DRVEVENT
* @brief This structure keeps all needed data to managed user space / driver synchronization event.
*
* It abstracts OS dependent object at this level. To allow Linux/Windows application portability,
* user should use event through this object.
* Windows users are also allowed to retrieve Windows Event handle (CSkelDrvIF::RetrieveEventHandle() method)
* to use it with specific Windows API functions like WaitForMultipleObject.
*
* This structure is filled by the CSkelDrvIF::CreateDrvEvent() method, and the event is released by CSkelDrvIF::CloseDrvEvent()
*
* For more information about user space / driver synchronization event, please refer to ref CDRV_DOC_DRVEVENT_PAGE "Driver Events page".
*
* @remark Driver event creation allocates limited resources at driver level. These resources are released by CSkelDrvIF::CloseDrvEvent(),
*         or implicitly when driver access is closed by the application. Note that the driver close also occurs when application crashes.
* @remark All fields of this structure are filled by the CSkelDrvIF::AllocateMemory() method. User should use this structure
*         like a kind of handle. 
*
* @see CSkelDrvIF::CreateDrvEvent()
* @see CSkelDrvIF::WaitOnDrvEvent()
* @see CSkelDrvIF::CloseDrvEvent()
* @see CSkelDrvIF::SetDrvEvent()
* @see CSkelDrvIF::ResetDrvEvent()
*************************************************************************************************************************/

struct DRVEVENT
{
#ifdef WIN32
    HANDLE   Event_H;                   //!< Keeps Windows HANDLE to created event
#endif
    EVENT_ID EventID_e;                 //!< Keeps the EVENT ID returned by driver
    ULONG    IntSrc_UL;                 //!< Keeps the associated interrupt sources
} ;


/**********************************************************************************************************************//**
* @struct ALLOC_MEM_DESCR
* @brief  This structure keeps data to handle DMA related memory allocation.
*
* The driver interface allows user to allocate contiguous and non-pageable memory for DMA operation. This structure
* is used in ref CDRV_DOC_MEM_ALLOC_PAGE "the driver memory allocation mechanism".
* In fact, all methods that use this structure abstract several low level driver access, to allocate and map into user space
* the needed memory resources.
* 
* @see CSkelDrvIF::AllocateMemory()
* @see CSkelDrvIF::FreeMemory()
*************************************************************************************************************************/
typedef struct 
{
   ULONG     Size_UL;            ///< Keeps the size of the memory bloc
   ULONGLONG PhysAddr_ULL;       ///< Keeps the PCI physical address of the allocated memory bloc
   UBYTE    *pAddr_UB;           ///< Keeps the user-space mapped virtual address
   ULONG     CommonBufID_UL;     ///< Keeps the common buffer index returned by the IOCTL_ALLOCATE_COMMON_BUFFER I/O Control
   COMMON_BUFFER_DESCR Descr_X;  // Keeps the information returned by the the IOCTL_GET_COMMON_BUFFER I/O Control 
} ALLOC_MEM_DESCR;


/***** CLASSES DECLARATIONS *******************************************************************************************/


class IDMABuffer
{
public:
   enum TYPE
   {
      TYPE_AUTO,
      TYPE_COMMON_BUFFER,
      TYPE_US_BUFFER,
      TYPE_PREALLOCATE_CB_0_0,
      TYPE_PREALLOCATE_CB_0_1,
      TYPE_PREALLOCATE_CB_1_0,
      TYPE_PREALLOCATE_CB_1_1,
      TYPE_PREALLOCATE_CB_2_0,
      TYPE_PREALLOCATE_CB_2_1,
      TYPE_PREALLOCATE_CB_3_0,
      TYPE_PREALLOCATE_CB_3_1
   };

   virtual ~IDMABuffer(){};
   virtual TYPE GetType()=0;
   virtual ULONG GetSize()=0;
   virtual UBYTE * GetBuffer()=0;
   virtual DMABUFID GetDMABufID()=0;
};
/*********************************************************************************************************************//**
* @class CSkelDrvIF
* @brief This is the main low level interface to the driver.
*
* It provides a set of methods to interface the low level behavior of the driver. Some OS dependent objects
* and OS functions calls are involved in this class and abstracted at this level.
*
* @remark This class is OS dependent.  
*************************************************************************************************************************/
class ISkelDrvIF
{
public:  
   virtual ~ISkelDrvIF(){};

   virtual CDRVIF_STATUS GetDriverVersion(ULONG *pVersion_UL)=0;
   virtual CDRVIF_STATUS GetBoardRefCount(ULONG *pRefCount_UL)=0;  

   virtual BOOL32     BoardIsOpened()=0;

   virtual CDRVIF_STATUS    CreateDrvEvent(DRVEVENT *pDrvEvent_X, BOOL32 ManualReset_B=FALSE,ULONG AssociatedIntSrc_UL=0, BOOL32 EnableIntSrcInDPC_B=TRUE, void * pParam_v=NULL)=0;
   virtual CDRVIF_STATUS    WaitOnDrvEvent(DRVEVENT *pDrvEvent_X,ULONG Timeout_UL=INFINITE)=0;
   virtual CDRVIF_STATUS    CloseDrvEvent(DRVEVENT *pDrvEvent_X)=0;
   virtual CDRVIF_STATUS    SetDrvEvent(DRVEVENT *pDrvEvent_X)=0;
   virtual BOOL32           ResetDrvEvent(DRVEVENT *pDrvEvent_X)=0;
   virtual CDRVIF_STATUS    SetDrvEventAssociatedIntSrc(DRVEVENT *pDrvEvent_X,ULONG AssociatedIntSrc_UL=0, BOOL32 EnableIntSrcInDPC_B=TRUE)=0;


   virtual CDRVIF_STATUS    GetMemMngrInfo(IOCTL_GETPREALLOCINFO_COMMON_BUFFER_PARAM *pMemMngrInfo_X)=0;
   virtual CDRVIF_STATUS    ReservePoolSide(ULONG PoolId_UL, ULONG PoolSide_UL, ULONG NbOfBuffers_UL, ULONG pBuffersSize_UL[COMMON_BUFFER_MAX_SLAVE+1])=0;
   virtual CDRVIF_STATUS    FreePoolSide(ULONG PoolId_UL, ULONG PoolSide_UL)=0;
   
   virtual CDRVIF_STATUS    ReservePreallocatedMemory(ULONG Size_UL, ULONG PoolId_UL, ULONG PoolSide_UL, ALLOC_MEM_DESCR *pDescr_X)=0;    //Reserve a preallocated buffer + map buffer
   virtual CDRVIF_STATUS    AllocateMemory(ULONG Size_UL,ALLOC_MEM_DESCR *pDescr_X)=0;     //Allocate a common buffer + map buffer. 
   virtual CDRVIF_STATUS    FreeMemory(ALLOC_MEM_DESCR *pDescr_X)=0;                       //Unmap buffer + free common buffer      

   virtual CDRVIF_STATUS    ReservePreallocatedBuffer(ULONG Size_UL, ULONG PoolId_UL, ULONG PoolSide_UL, ULONG *pBufId_UL)=0;
   virtual CDRVIF_STATUS    AllocateCommonBuffer(ULONG Size_UL, ULONG *pBufId_UL)=0;    
   virtual CDRVIF_STATUS    FreeCommonBuffer( ULONG BufferID_UL )=0;                    

   virtual CDRVIF_STATUS    GetCommonBuffer(ULONG BufID_UL, COMMON_BUFFER_DESCR *pDescr_X)=0;          //Map buffer   TODO : rename : MapCommonBuffer?
   virtual CDRVIF_STATUS    ReleaseCommonBuffer( ULONG BufferID_UL,COMMON_BUFFER_DESCR *pDescr_X)=0;   //Unmap buffer TODO : rename : UnmapCommonBuffer?
   virtual CDRVIF_STATUS    LinkCommonBuffer( IOCTL_LINK_COMMON_BUFFER_PARAM *pLink_X )=0;
      
   virtual IDMABuffer *  CreateDMABuffer(ULONG Size_UL, IDMABuffer::TYPE Type_e = IDMABuffer::TYPE_AUTO)=0;
   virtual void		     DestroyDMABuffer(IDMABuffer * pBuffer_O)=0;
   virtual CDRVIF_STATUS LinkDMABuffer(IDMABuffer * pMaster_O, IDMABuffer * pSlave_O)=0;

   virtual CDRVIF_STATUS  DMABuf_GetFlags(DMABUFID BufID, ULONG *pFlags_UL)=0;
   virtual CDRVIF_STATUS  DMABuf_SetBitFlags(DMABUFID BufID, ULONG *pFlags_UL)=0;
   virtual CDRVIF_STATUS  DMABuf_ClrBitFlags(DMABUFID BufID, ULONG *pFlags_UL)=0;

   virtual CDRVIF_STATUS    SetDPCIntMask(ULONG Mask_UL)=0;
   virtual CDRVIF_STATUS    ClrDPCIntMask(ULONG Mask_UL)=0;
   virtual CDRVIF_STATUS    GetIntSrc(ULONG *pIntSrc_UL)=0;

   virtual BOARD *  GetBoardPtr()=0;
   virtual PCI_RESOURCE * GetPCIResourcesPtr()=0;
   virtual CDRVIF_STATUS GetRegisterInfo(PMM32_REGISTER pRegPtr, UBYTE *pPCIBar_UB, ULONG *pOffset_UL)=0;

   virtual CDRVIF_STATUS DriverProperty_GetValue(ULONG Property_UL, ULONG *pValue_UL)=0;  
   virtual CDRVIF_STATUS DriverProperty_SetValue(ULONG Property_UL, ULONG Value_UL)=0;    
   virtual CDRVIF_STATUS DriverProperty_SetBit(ULONG Property_UL, ULONG Bit_UL)=0;  
   virtual CDRVIF_STATUS DriverProperty_ClrBit(ULONG Property_UL, ULONG Bit_UL)=0;  
 
   virtual CDRVIF_STATUS ShadowReg32_SetBit(ULONG Idx_UL, ULONG Bit_UL, ULONG *pValue_UL=NULL)=0;
   virtual CDRVIF_STATUS ShadowReg32_SetMsk(ULONG Idx_UL, ULONG Bit_UL, ULONG Msk_UL, ULONG *pValue_UL=NULL)=0;
   virtual CDRVIF_STATUS ShadowReg32_SetAutoClearBit(ULONG Idx_UL,ULONG Bit_UL, ULONG *pValue_UL=NULL)=0;
   virtual CDRVIF_STATUS ShadowReg32_ClrBit(ULONG Idx_UL, ULONG Bit_UL, ULONG *pValue_UL=NULL)=0;
   virtual CDRVIF_STATUS ShadowReg32_Get(ULONG Idx_UL, ULONG *pValue_UL)=0;
   virtual CDRVIF_STATUS ShadowReg32_Set(ULONG Idx_UL, ULONG Value_UL)=0;

   virtual CDRVIF_STATUS KThread_Wakeup(KTHREADID KThreadID, ULONG SyncParam_UL)=0;
   virtual CDRVIF_STATUS KThread_Stop(KTHREADID KThreadID)=0;
   virtual CDRVIF_STATUS KThread_SetPeriod(KTHREADID KThreadID, ULONG Timeout_UL)=0;

   virtual CDRVIF_STATUS SetDbgMask(ULONG ModuleID_UL, ULONG Mask_UL)=0;

   virtual CDRVIF_STATUS GetBoardNoAwakeRefCount(ULONG *pRefCount_UL)=0;

   virtual CDRVIF_STATUS Register_Get(ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL)=0;
   virtual CDRVIF_STATUS Register_Set(ULONG Bar_UL, ULONG Offset_UL, ULONG  Value_UL)=0;
   virtual CDRVIF_STATUS Register_Set(ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL)=0;
   virtual CDRVIF_STATUS Register_And(ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL)=0;
   virtual CDRVIF_STATUS Register_And(ULONG Bar_UL, ULONG Offset_UL, ULONG Value_UL)=0;
   virtual CDRVIF_STATUS Register_Or (ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL)=0;
   virtual CDRVIF_STATUS Register_Or (ULONG Bar_UL, ULONG Offset_UL, ULONG Value_UL)=0;
   virtual CDRVIF_STATUS Register_Xor(ULONG Bar_UL, ULONG Offset_UL, ULONG* pValue_UL)=0;
   virtual CDRVIF_STATUS Register_Xor(ULONG Bar_UL, ULONG Offset_UL, ULONG Value_UL)=0;
};

#endif // _IDRVINTERFACE_H_
