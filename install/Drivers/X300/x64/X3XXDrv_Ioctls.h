/********************************************************************************************************************//**
 * @internal
 * @file   	TstDrvIoctls.h
 * @date   	2010/08/10
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/10  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _X3XXDRVIOCTLS_H_
#define _X3XXDRVIOCTLS_H_

#include "StreamIoctls.h"
#include "MailboxMngrIF.h"



typedef enum
{
   IOCTL_ARM_ACCESS_TYPE_CMD
} IOCTL_ARM_ACCESS_TYPE;

typedef struct  
{
   ULONG                      StructSize_UL;
   IOCTL_ARM_ACCESS_TYPE      AccessType_E;

   struct 
   {
      union{
         ULONG       CmdTag_UL;
         ULONG       CmdSts_UL;
      };
      DELTA_VOID     pParam_v;
      DELTA_VOID     pRtrn_v;
      ULONG          ParamSize_UL;
      ULONG          RtrnSize_UL;
   } ;
} IOCTL_ARM_ACCESS_PARAM;

typedef enum
{
   IOCTL_FLASH_ACCESS_TYPE_WRITE_AND_CHECK_BLOCK,
	IOCTL_FLASH_ACCESS_TYPE_CHECK_BLOCK,
	IOCTL_FLASH_ACCESS_TYPE_READ
} IOCTL_FLASH_ACCESS_TYPE;

typedef struct 
{
   ULONG                      StructSize_UL;
   IOCTL_FLASH_ACCESS_TYPE    Type_e;
   ULONG                      FlashAddress_UL;
   DELTA_VOID                 pBuffer_v;
   ULONG                      BufferWordSize_UL;      
} IOCTL_FLASH_ACCESS_PARAM;

typedef enum
{
   IOCTL_LTC_ACCESS_TYPE_GET_LTC_VALUE
} IOCTL_LTC_ACCESS_TYPE;

typedef struct  
{
   ULONG                   StructSize_UL;
   IOCTL_LTC_ACCESS_TYPE   Type_E;
   union
   {
      /* IOCTL_LTC_ACCESS_TYPE_GET_VALUE */
      struct{
         ULONGLONG     LtcValue_ULL;
         UBYTE          Sts_UB;
      }GetLtcValue;
   };
} IOCTL_LTC_ACCESS_PARAM;

#define X3XXDRV_IOC(code) (STREAM_IOC_USER(code))

#define IOCTL_ARM_ACCESS         DEFINE_IOCTL_RDWR(X3XXDRV_IOC(0x10), IOCTL_ARM_ACCESS_PARAM)
#define IOCTL_MAILBOX_ACCESS     DEFINE_IOCTL_RDWR(X3XXDRV_IOC(0x11), IOCTL_MAILBOX_ACCESS_PARAM)
#define IOCTL_FLASH_ACCESS      DEFINE_IOCTL_RDWR(X3XXDRV_IOC(0x12), IOCTL_FLASH_ACCESS_PARAM)


#define X3XXDRV_IOC_USER(code) (X3XXDRV_IOC(code)+0x13)  

#endif //_X3XXDRVIOCTLS_H_
