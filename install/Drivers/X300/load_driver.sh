#!/bin/bash
drivername="delta-x300"
drivernamex="delta-x3xx"

echo "Loading the module (this must succeed). This can takes a few tens of seconds."
/sbin/modprobe $drivername

if [ "$#" != "1" ]; then
	echo "Usage: $0 num-devices"
	echo
	exit
fi

major=$(cat /proc/devices | grep -i ${drivernamex} | cut -d ' ' -f1)
echo "Major number: $major"

echo "Deleting old device nodes..."
rm /dev/${drivername}*

echo "Creating device nodes..."
n_devices=0
while [ "$n_devices" != "$1" ]; do
	mknod /dev/${drivername}${n_devices} c $major $((${n_devices}*10+0))
	cat /dev/${drivername}${n_devices}
	n_devices=$((${n_devices}+1))
done
chmod 666 /dev/${drivername}*

echo Done
echo
