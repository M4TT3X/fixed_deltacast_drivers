/**********************************************************************************************************************
 
   Module   : chdev_main
   File     : chdev_main.c
   Created  : 2006/11/20
   Author   : cs

 **********************************************************************************************************************/

/***** INCLUDES *******************************************************************************************************/

#define MODID MID_SKELDRV

#include "Driver.h"
#include "EventMngr.h"
#include "LinuxAllocator.h"
#include "DrvDbg.h"
#include "SkelIoctls.h"
#include "MMapCommandId.h"
#include "CommonBufMngr.h"
#include "LinuxDrvHlp.h"
#include "DrvDbg.h"
#include "OSDrvData.h"
#include "DriverAccess.h"
#include "LinuxDrvInfo.h"
#include "SkelProperties.h"
#include "KThreadMngr.h"
#include "InterlockedOp.h"

/***** EXTERNAL VARIABLES *********************************************************************************************/

#define TraceISROutput(pFmt_c,...)       xxxOutput(SKELDRV_TRACE_MSG_ISR, "TRACEISR: ", pFmt_c, ##__VA_ARGS__)

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,12))
extern struct class_simple *pDeltaClass_X;
#define DEVICE_CREATE(a,b,c,d,e,f) class_simple_device_add(a,c,d,e)
#define device_destroy(a,b) class_simple_device_remove(b)
#elif (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27))
extern struct class *pDeltaClass_X;
#define DEVICE_CREATE(a,b,c,d,e,f) device_create(a,b,c,e,f)
#else
extern struct class *pDeltaClass_X;
#define DEVICE_CREATE(a,b,c,d,e,f) device_create(a,b,c,d,e,f)
#endif

/***** GLOBAL VARIABLES ***********************************************************************************************/

typedef struct _PROCESS_EXTENSION
{
	DEVICE_EXTENSION *pdx;
	ULONG PID_UL;
}PROCESS_EXTENSION;


extern DEVICE_EXTENSION GL_pDeviceExtension_X[];
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,11))
int MainDevice_Ioctl(struct inode *inode, struct file *filp,unsigned int cmd, unsigned long arg);
#else
int MainDevice_Ioctl(struct file *filp,unsigned int cmd, unsigned long arg);
#endif
int MainDevice_Open(struct inode *inode, struct file *filp);
ssize_t MainDevice_Read(struct file *filp,char *pUserBuffer_c,size_t length,loff_t *offset);
int MainDevice_Release(struct inode *inode, struct file *filp);
int MainDevice_MMap(struct file *filp, struct vm_area_struct *vma);

/***** inode2pdx *********************************************************************************************

  Description :

	This function helps to retrieve the PDEVICE_EXTENSION pointer from the file object context.

  Parameters :

	struct inode *inode	:
 
  Returned Value : DEVICE_EXTENSION *
  Remark : None
 
 **********************************************************************************************************************/
DEVICE_EXTENSION *inode2pdx(struct inode *inode)
{
   int dev_index = (iminor(inode)) / NBMINORS;
   return &GL_pDeviceExtension_X[dev_index];
} 

static struct file_operations GL_MainDeviceOps_X =
{
   .owner=THIS_MODULE,
   .llseek=NULL,
   .read=MainDevice_Read,
   .write=NULL,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,11))
   .ioctl=MainDevice_Ioctl,
#else
   .unlocked_ioctl=MainDevice_Ioctl,
   .compat_ioctl=MainDevice_Ioctl,
#endif
   .mmap=MainDevice_MMap,
   .open=MainDevice_Open,/*a Open (and a Release) function is called automatically when using cat /dev/sdiX*/
   .release=MainDevice_Release, /*Open is at least mandatory to fill filp->private_data structure*/
};



#if (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 11))
int MainDevice_Ioctl(struct inode* pNode_X, struct pFile_X, unsigned int Command_UL, unsigned long Arg_UL)
#else
int MainDevice_Ioctl(struct file* pFile_X, unsigned int Command_UL, unsigned long Arg_UL)
#endif
{
	PROCESS_EXTENSION *pProcessExtension = pFile_X->private_data;
   PDEVICE_EXTENSION pdx = pProcessExtension->pdx;
   int               Result_i = 0;
   PID_TYPE          Process_pid = pProcessExtension->PID_UL;
   IOCTL_STATUS      Status_E;

   EnterOutput("Process: " PID_FORMAT "\n", Process_pid);

   if ((_IOC_TYPE(Command_UL) == MYDRV_IOC_MAGIC)  && (_IOC_NR(Command_UL) <= MYDRV_IOC_MAXNR))
   {
      if (_IOC_DIR(Command_UL) & _IOC_READ)  
         Result_i = !access_ok(VERIFY_WRITE, (void __user *)Arg_UL, _IOC_SIZE(Command_UL));

      if (_IOC_DIR(Command_UL) & _IOC_WRITE) 
         Result_i = !access_ok(VERIFY_READ, (void __user *)Arg_UL, _IOC_SIZE(Command_UL));

      if (!Result_i)
      {
         Status_E = SkelDrv_IoCtl(pdx, Command_UL, (UBYTE*)Arg_UL, _IOC_SIZE(Command_UL), Process_pid, NULL);

         switch (Status_E)
         {
         case IOCTL_STATUS_OK:                  Result_i = 0; break; 
         case IOCTL_STATUS_INVALID_PARAMETER:   Result_i = -EINVAL; break;
         case IOCTL_STATUS_UNKNOWN_IOCTL:       Result_i = -ENOTTY; break;
         case IOCTL_STATUS_NOT_IMPLEMENTED:     Result_i = -ENOSYS; break;
         case IOCTL_STATUS_WAKEUP_ACK:          Result_i = -ECONNRESET; break;
         case IOCTL_STATUS_MEM_FAULT:           Result_i = -EFAULT; break;
         case IOCTL_STATUS_DEVICE_REMOVED:      Result_i = -ENODEV; break;
         case IOCTL_STATUS_LOW_POWER_STATE:     Result_i = -EBUSY; break;
         default:                               Result_i = -EIO; break;  
         }
      }
      else
      {
         ErrOutput("Invalid IOCTL buffer\n");
         Result_i = -EFAULT;
      }
   }
   else
   {
      ErrOutput("Corrupted IOCTL command: %d (0x%x)\n", Command_UL, Command_UL);
      Result_i = -ENOTTY;
   }

   ExitOutput( "Status: %d\n", Result_i);
   return Result_i;
}

void RemoveCharDevices(DEVICE_EXTENSION *pdx, int major_number_i, int index_i)
{
   EnterOutput("\n");
   cdev_del(&(pdx->pOsDrvData_X->MainCharDevice_X));
   device_destroy(pDeltaClass_X, MKDEV(major_number_i, index_i*NBMINORS));
   ExitOutput("\n");
}

int CreateCharDevices(DEVICE_EXTENSION *pdx, int major_number_i, int index_i)
{
int err_i;
int devno_i;
void *pErr_v;

   EnterOutput("\n");

   devno_i = MKDEV(major_number_i, index_i*NBMINORS);
   cdev_init(&pdx->pOsDrvData_X->MainCharDevice_X, &GL_MainDeviceOps_X);
   err_i = cdev_add(&pdx->pOsDrvData_X->MainCharDevice_X,devno_i,1);
   if (err_i)
   {
      ErrOutput("Cannot add new character device %d (err=%d)\n", index_i,err_i);
   }
   else
   {
      char DriverName_c[50];
      strcpy(DriverName_c,pdx->DrvData_X.CustomizationTable_X.pDriverName_c);
      strcat(DriverName_c,"%d");

      pErr_v = DEVICE_CREATE(pDeltaClass_X, NULL, devno_i, NULL, DriverName_c, pdx->DrvData_X.CustomizationTable_X.LocalBoardNb_i); 
      if(IS_ERR(pErr_v))
         InitErrOutput("Device node %d not exported to sysfs. Udev will not detect it\n", pdx->DrvData_X.CustomizationTable_X.LocalBoardNb_i);
   }
   ExitOutput("\n");
   return err_i;
}

/***** MainDevice_Open *********************************************************************************************

  Description :

	

  Parameters :

	struct inode *inode	:
	struct file *filp	:
 
  Returned Value : int
  Remark : None
 
 **********************************************************************************************************************/
int MainDevice_Open(struct inode *inode, struct file *filp)
{
   int sts = 0;
   DEVICE_EXTENSION *pdx;
   ULONG PID_UL=current->tgid; //current->tgid returns the pid of the first thread while current->pid returns the pid of the current thread
	PROCESS_EXTENSION *pProcessExtension;
	pProcessExtension = kmalloc(sizeof(PROCESS_EXTENSION),GFP_KERNEL);
   pdx = inode2pdx(inode);
	pProcessExtension->pdx = pdx;
	pProcessExtension->PID_UL = PID_UL;
   filp->private_data = pProcessExtension;
   EnterOutput( "(PID:%08xh)\n",PID_UL);
   
   if (!SkelDrv_Open(pdx,PID_UL))
   {
      ErrOutput("SkelDrv_Open failed, decrementing the ref counts\n");

      sts = -ENODEV;
   }

   ExitOutput("\n");
   return sts;
}

/***** MainDevice_Release *********************************************************************************************

  Description :

	

  Parameters :

	struct inode *inode	:
	struct file *filp	:
 
  Returned Value : int
  Remark : None
 
 **********************************************************************************************************************/
int MainDevice_Release(struct inode *inode, struct file *filp)
{
int sts;
PROCESS_EXTENSION *pProcessExtension = filp->private_data;
DEVICE_EXTENSION *pdx = pProcessExtension->pdx;
ULONG PID_UL=pProcessExtension->PID_UL;
int MainDevRefCount_i;

   EnterOutput( "(PID:%08xh)\n",PID_UL);

   sts=SkelDrv_Close(pdx,PID_UL)?0:-1; //sts=Board_Close(pdx,MainDevRefCount_i,PID_UL); 

	kfree(pProcessExtension);

   ExitOutput("\n");
   return (sts);
}

/***** MainDevice_Read *********************************************************************************************

Description :



Parameters :

struct file *filp	:
char *pUserBuffer_c	:
size_t length	:
loff_t *offset	:

Returned Value : ssize_t
Remark : None

**********************************************************************************************************************/
ssize_t MainDevice_Read(struct file *filp,char *pUserBuffer_c,size_t length, loff_t *offset)  
{
	PROCESS_EXTENSION *pProcessExtension = filp->private_data;
   DEVICE_EXTENSION *pdx = pProcessExtension->pdx;
   ssize_t rtrn;
   char *pString_c;



   switch(pdx->BoardInitStatus_E)
   {
   case BOARD_INIT_NEED_RESTART:
      pString_c="\nFirmware has been upgraded.\nYour computer needs to restart.\n\nPlease restart your computer.\n";
      break;
   case BOARD_INIT_NEED_SHUTDOWN:
      pString_c="\nFirmware has been upgraded.\nYour computer needs to be powered off for a couple of seconds.\n\nPlease turned off, wait of a few seconds, and turn on.\n";
      break;
   default:
      pString_c="";
      break;
   }

   rtrn=min(length, strlen(pString_c)- (size_t)*offset);
   if(rtrn)
   {
      memcpy(pUserBuffer_c, pString_c + *offset, rtrn);
      *offset += rtrn;
   }

   return rtrn;
}

/***** MainDevice_MMap *********************************************************************************************

Description :

 This function is used to map memory area. There are different kind of memory to map (PCI memory mapped register, or
 common buffer, ...). The kind parameter is given through the offset parameter.

Parameters :

struct file *filp	         : Pointer to file context
struct vm_area_struct *vma	: Pointer to Virtual Memory area information structure.

Returned Value : int
Remark : none  

**********************************************************************************************************************/
int MainDevice_MMap(struct file *filp, struct vm_area_struct *vma)
{
int Sts_i=0;
PROCESS_EXTENSION *pProcessExtension = filp->private_data;
DEVICE_EXTENSION *pdx= pProcessExtension->pdx;

ULONG MMapCmd_UL;

   EnterOutput( "(PID:%08xh)\n",current->tgid); //current->tgid returns the pid of the first thread while current->pid returns the pid of the current thread

   MMapCmd_UL = vma->vm_pgoff;   // Get the MMAP command given through offset parameter ...
   vma->vm_pgoff=0;              // ... and restore the offset to 0 for futur use for the VMA structure
   
   switch(MMapCmd_UL&MMAP_CMD_MASK)
   {
   case MMAP_CMD_MAP_PCI_RESOURCES: DbgOutput("MMAP PCI Resources\n");
      Sts_i = LinuxDrvHlp_MMap_PCIResource(pdx, vma,MMapCmd_UL & MMAP_CMD_PARAM_MASK);
      break;

   case MMAP_CMD_MAP_COMMON_BUFFER:DbgOutput("MMAP Common Buffer\n");
      Sts_i=CommonBufMngr_MMap(pdx,vma,MMapCmd_UL&MMAP_CMD_PARAM_MASK);
      break;
   }

   ExitOutput("\n");
   return Sts_i;
}







