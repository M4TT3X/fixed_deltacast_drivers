/**********************************************************************************************************************
 
   Module   : EventMngrTypes
   File     : EventMngrTypes.h
   Created  : 2007/06/19
   Author   : cs

   Description : 

   
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/06/19  v00.01.0000    cs       Creation of this file


 **********************************************************************************************************************/

#ifndef _EventMngrTypes_H_
#define _EventMngrTypes_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define INVALID_EVENT_ID 0

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef ULONG EVENT_ID;

typedef enum 
{
   IOCTL_EVENT_ACCESS_TYPE_OPEN,
   IOCTL_EVENT_ACCESS_TYPE_CLOSE,
   IOCTL_EVENT_ACCESS_TYPE_SET,
   IOCTL_EVENT_ACCESS_TYPE_RESET,
   IOCTL_EVENT_ACCESS_TYPE_WAITON,
   IOCTL_EVENT_ACCESS_TYPE_WAITON_WITH_TIMEOUT,
   IOCTL_EVENT_ACCESS_TYPE_REG_INT_SRC
}IOCTL_EVENT_ACCESS_TYPE;

#pragma pack(push, 4)

typedef struct
{
   ULONG                      StructSize_UL;
   IOCTL_EVENT_ACCESS_TYPE    AccessType_E;
   EVENT_ID                   EventID_e;
   union
   {
      DELTA_VOID                 Event_H;
      ULONG                      TimeOut_UL;
      ULONG                      IntSrc_UL;
   };
}IOCTL_EVENT_ACCESS_PARAM;

#pragma pack(pop)


#ifdef __linux__

typedef struct
{
   EVENT_ID EventID_e;
   ULONG    Timeout_UL;
} WAIT_ON_EVENT_PARAM;

#endif

#endif // _EventMngrTypes_H_
