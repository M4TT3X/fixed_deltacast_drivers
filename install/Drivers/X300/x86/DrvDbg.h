/********************************************************************************************************************//**
 * @internal
 * @file   	DrvDbg.h
 * @date   	2010/06/17
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/17  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _DRVDBG_H_
#define _DRVDBG_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#ifdef __linux__
#include "LinuxDrvInfo.h"
#endif

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#ifndef MODID
#error "MODID symbol must be defined before including this file !"
#endif

//#define SHORT_DBG_MSG

#ifdef __linux__
#define KernelPrint printk
//#define PRINT_TIME

#elif defined (__APPLE__)

#include "CTypes.h"
#define KernelPrint IOLog

#else
#define KernelPrint DbgPrint 
#undef  PRINT_TIME
#endif

extern ULONG GL_InitDbgMask_UL;

#define DBG_OUTPUT_ALL           0xFFFFFFFF
#define DBG_OUTPUT_NONE          0x00000000


#define DBG_OUTPUT_RETAIL        0x00000001
#define DBG_OUTPUT_ERROR         0x00000002
#define DBG_OUTPUT_WARNING       0x00000004
#define DBG_OUTPUT_INFO          0x00000008
#define DBG_OUTPUT_DEBUG         0x00000010
#define DBG_OUTPUT_TRACE         0x00000020
#define DBG_OUTPUT_ENTER         0x00000040
#define DBG_OUTPUT_EXIT          0x00000080
#define DBG_OUTPUT_IOCTL         0x01000000

#define DBG_OUTPUT_ISR_RETAIL    0x00000100
#define DBG_OUTPUT_ISR_ERROR     0x00000200
#define DBG_OUTPUT_ISR_WARNING   0x00000400
#define DBG_OUTPUT_ISR_INFO      0x00000800
#define DBG_OUTPUT_ISR_DEBUG     0x00001000
#define DBG_OUTPUT_ISR_TRACE     0x00002000
#define DBG_OUTPUT_ISR_ENTER     0x00004000
#define DBG_OUTPUT_ISR_EXIT      0x00008000

#define DBG_OUTPUT_DPC_RETAIL    0x00010000
#define DBG_OUTPUT_DPC_ERROR     0x00020000
#define DBG_OUTPUT_DPC_WARNING   0x00040000
#define DBG_OUTPUT_DPC_INFO      0x00080000
#define DBG_OUTPUT_DPC_DEBUG     0x00100000
#define DBG_OUTPUT_DPC_TRACE     0x00200000
#define DBG_OUTPUT_DPC_ENTER     0x00400000
#define DBG_OUTPUT_DPC_EXIT      0x00800000

#define DBG_OUTPUT_TRACE_ISR     0x02000000
#define DBG_OUTPUT_TRACE_ALLOC   0x04000000
#define DBG_OUTPUT_TRACE_DMA     0x08000000


#define DBG_OUTPUT_USR_MSG(msg)  ((1)<<(28+(msg)))


#define DBG_OUTPUT_ENTER_PREFIX           "--> Entering "
#define DBG_OUTPUT_EXIT_PREFIX            "<-- Exiting "
#define DBG_OUTPUT_DEBUG_PREFIX           ""
#define DBG_OUTPUT_INFO_PREFIX            "==NFO: " 
#define DBG_OUTPUT_WARNING_PREFIX         "--WRN: " 
#define DBG_OUTPUT_ERROR_PREFIX           "**ERR: "
#define DBG_OUTPUT_RETAIL_PREFIX          ""
#define DBG_OUTPUT_TRACE_PREFIX           ">>TRC: " 
#define DBG_OUTPUT_IOCTL_PREFIX           ">>IOCTL: " 

#ifndef DBG_OUTPUT_USRMSG_PREFIX
#define DBG_OUTPUT_USRMSG_PREFIX          ""
#endif
#ifndef DBG_OUTPUT_USRMSG0_PREFIX
#define DBG_OUTPUT_USRMSG0_PREFIX         DBG_OUTPUT_USRMSG_PREFIX
#endif                   
#ifndef DBG_OUTPUT_USRMSG1_PREFIX
#define DBG_OUTPUT_USRMSG1_PREFIX         DBG_OUTPUT_USRMSG_PREFIX
#endif                   
#ifndef DBG_OUTPUT_USRMSG2_PREFIX
#define DBG_OUTPUT_USRMSG2_PREFIX         DBG_OUTPUT_USRMSG_PREFIX
#endif                   
#ifndef DBG_OUTPUT_USRMSG3_PREFIX
#define DBG_OUTPUT_USRMSG3_PREFIX         DBG_OUTPUT_USRMSG_PREFIX
#endif


#ifdef PRINT_TIME
#define TIME_GET                      struct timeval tv; do_gettimeofday(&tv);
#define TIME_FMT                       "[%ld.%06ld] "
#define TIME_USE                       ,tv.tv_sec, tv.tv_usec
#else
#define TIME_GET                      
#define TIME_FMT                       
#define TIME_USE                       
#endif

#ifndef DEFAULT_DBG_MASK
#if (DBG==1)
#define DEFAULT_DBG_MASK (DBG_OUTPUT_RETAIL|DBG_OUTPUT_ERROR)
#else
#define DEFAULT_DBG_MASK (DBG_OUTPUT_RETAIL)
#endif
#endif //ifndef DEFAULT_DBG_MASK


/***** MACROS DEFINITIONS *********************************************************************************************/

#define InitDbgOutput(pFmt_c,...)      InitxxxOutput(DBG_OUTPUT_DEBUG, DBG_OUTPUT_DEBUG_PREFIX, pFmt_c, ##__VA_ARGS__)
#define InitErrOutput(pFmt_c,...)      InitxxxOutput(DBG_OUTPUT_ERROR, DBG_OUTPUT_ERROR_PREFIX, pFmt_c, ##__VA_ARGS__)
#define InitRetailOutput(pFmt_c,...)   InitxxxOutput(DBG_OUTPUT_RETAIL, DBG_OUTPUT_RETAIL_PREFIX, pFmt_c, ##__VA_ARGS__)

#define EnterOutput(pFmt_c,...)        xxxOutput(DBG_OUTPUT_ENTER, DBG_OUTPUT_ENTER_PREFIX, pFmt_c, ##__VA_ARGS__)
#define ExitOutput(pFmt_c,...)         xxxOutput(DBG_OUTPUT_EXIT, DBG_OUTPUT_EXIT_PREFIX, pFmt_c, ##__VA_ARGS__)
#define DbgOutput(pFmt_c,...)          xxxOutput(DBG_OUTPUT_DEBUG, DBG_OUTPUT_DEBUG_PREFIX, pFmt_c, ##__VA_ARGS__)
#define InfoOutput(pFmt_c,...)         xxxOutput(DBG_OUTPUT_INFO, DBG_OUTPUT_INFO_PREFIX, pFmt_c, ##__VA_ARGS__)
#define WrnOutput(pFmt_c,...)          xxxOutput(DBG_OUTPUT_WARNING, DBG_OUTPUT_WARNING_PREFIX, pFmt_c, ##__VA_ARGS__)
#define ErrOutput(pFmt_c,...)          xxxOutput(DBG_OUTPUT_ERROR, DBG_OUTPUT_ERROR_PREFIX, pFmt_c, ##__VA_ARGS__)
#define RetailOutput(pFmt_c,...)       xxxOutput(DBG_OUTPUT_RETAIL, DBG_OUTPUT_RETAIL_PREFIX, pFmt_c, ##__VA_ARGS__)
#define TraceOutput(pFmt_c,...)        xxxOutput(DBG_OUTPUT_TRACE, DBG_OUTPUT_TRACE_PREFIX, pFmt_c, ##__VA_ARGS__)
#define IoctlOutput(pFmt_c,...)        xxxOutput(DBG_OUTPUT_IOCTL, DBG_OUTPUT_IOCTL_PREFIX, pFmt_c, ##__VA_ARGS__)


#define IsrDbgOutput(pFmt_c,...)       xxxOutput(DBG_OUTPUT_ISR_DEBUG, DBG_OUTPUT_DEBUG_PREFIX, pFmt_c, ##__VA_ARGS__)
#define IsrInfoOutput(pFmt_c,...)      xxxOutput(DBG_OUTPUT_ISR_INFO, DBG_OUTPUT_INFO_PREFIX, pFmt_c, ##__VA_ARGS__)
#define IsrWrnOutput(pFmt_c,...)       xxxOutput(DBG_OUTPUT_ISR_WARNING, DBG_OUTPUT_WARNING_PREFIX, pFmt_c, ##__VA_ARGS__)
#define IsrErrOutput(pFmt_c,...)       xxxOutput(DBG_OUTPUT_ISR_ERROR, DBG_OUTPUT_ERROR_PREFIX, pFmt_c, ##__VA_ARGS__)
#define DpcDbgOutput(pFmt_c,...)       xxxOutput(DBG_OUTPUT_DPC_DEBUG, DBG_OUTPUT_DEBUG_PREFIX, pFmt_c, ##__VA_ARGS__)
#define DpcInfoOutput(pFmt_c,...)      xxxOutput(DBG_OUTPUT_DPC_INFO, DBG_OUTPUT_INFO_PREFIX, pFmt_c, ##__VA_ARGS__)
#define DpcWrnOutput(pFmt_c,...)       xxxOutput(DBG_OUTPUT_DPC_WARNING, DBG_OUTPUT_WARNING_PREFIX, pFmt_c, ##__VA_ARGS__)
#define DpcErrOutput(pFmt_c,...)       xxxOutput(DBG_OUTPUT_DPC_ERROR, DBG_OUTPUT_ERROR_PREFIX, pFmt_c, ##__VA_ARGS__)


#define TraceISROutput(pFmt_c,...)     xxxOutput(DBG_OUTPUT_TRACE_ISR,    "TRACEISR: ", pFmt_c, ##__VA_ARGS__)
#define TraceAllocOutput(pFmt_c,...)   xxxOutput(DBG_OUTPUT_TRACE_ALLOC,  "TRACEALLOC: ", pFmt_c, ##__VA_ARGS__)
#define TraceDMAOutput(pFmt_c,...)     xxxOutput(DBG_OUTPUT_TRACE_DMA,  "TRACEDMA: ", pFmt_c, ##__VA_ARGS__)

#define Usr0Output(pFmt_c,...)     xxxOutput(DBG_OUTPUT_USR_MSG(0),  DBG_OUTPUT_USRMSG0_PREFIX, pFmt_c, ##__VA_ARGS__)
#define Usr1Output(pFmt_c,...)     xxxOutput(DBG_OUTPUT_USR_MSG(1),  DBG_OUTPUT_USRMSG1_PREFIX, pFmt_c, ##__VA_ARGS__)
#define Usr2Output(pFmt_c,...)     xxxOutput(DBG_OUTPUT_USR_MSG(2),  DBG_OUTPUT_USRMSG2_PREFIX, pFmt_c, ##__VA_ARGS__)
#define Usr3Output(pFmt_c,...)     xxxOutput(DBG_OUTPUT_USR_MSG(3),  DBG_OUTPUT_USRMSG3_PREFIX, pFmt_c, ##__VA_ARGS__)




#define Output(mask,pFmt_c,...)        xxxOutput(mask, DBG_OUTPUT_USRMSG_PREFIX, pFmt_c, ##__VA_ARGS__)



#define InitxxxOutput(Level_UL,pPrefix_c,pFmt_c,...)  do{TIME_GET  if((Level_UL)&GL_InitDbgMask_UL) \
   KernelPrint( TIME_FMT DRIVERNAME "[%s] "pPrefix_c pFmt_c TIME_USE, __FUNCTION__, ##__VA_ARGS__);}while(0)


#ifndef SHORT_DBG_MSG
#define xxxOutput(Level_UL,pPrefix_c,pFmt_c,...)  do{TIME_GET  if((Level_UL)&pdx->pDbgMsk_UL[MODID]) \
  KernelPrint( TIME_FMT "%s %d [%s] "pPrefix_c pFmt_c TIME_USE,pdx->DrvData_X.CustomizationTable_X.pDriverName_c, pdx->BoardIdx_i, __FUNCTION__, ##__VA_ARGS__);}while(0)
#else
#define xxxOutput(Level_UL,pPrefix_c,pFmt_c,...)  do{if((Level_UL)&pdx->pDbgMsk_UL[MODID]) \
   KernelPrint(pFmt_c, ##__VA_ARGS__);}while(0)
#endif

#define SetDbgMask(val) do {pdx->pDbgMsk_UL[MODID] = val; }while(0)
#define GetDbgMask() (pdx->pDbgMsk_UL[MODID])

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/
void SetInitDbgMask(ULONG InitDbgMask_UL);

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _DRVDBG_H_


