/********************************************************************************************************************//**
 * @internal
 * @file   	CX330DrvIF.h
 * @date   	2014/09/02
 * @author 	bc
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	c     	Author   Description
   ====================================================================================================================
   2014/09/02  v00.01.0000    bc       Creation of this file
                                       
 **********************************************************************************************************************/

#ifndef _CX330DRVIF_H_
#define _CX330DRVIF_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CX3XXDrvIF.h"
#include "CStreamDrvIF.h"
#include "X330Drv_Properties.h"
#include "X3XXDrv_Types.h"
#include "X330Drv_Types.h"
#include "X330Drv_Version.h"
#include "IFwUpdate.h"
#include "SDI.h"
#include "NorFlash.h"
#include "CMailboxIF.h"

namespace CX330DrvIF_NS
{




/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define RXCHANNEL_ADDITIONAL_FEATURES_COUPLED_2                   0x000000004
#define RXCHANNEL_ADDITIONAL_FEATURES_THUMBNAIL                   0x000000008
#define RXCHANNEL_ADDITIONAL_FEATURES_VBI10BITS                   0x000000010
#define RXCHANNEL_ADDITIONAL_FEATURES_FIELD_MODE                  0x000000020
#define RXCHANNEL_ADDITIONAL_FEATURES_COUPLED_4                   0x000000040
#define RXCHANNEL_ADDITIONAL_FEATURES_3G_LEVELB                   0x000000100
#define RXCHANNEL_ADDITIONAL_FEATURES_GRAPHIC_PADDING             0x000000200
#define RXCHANNEL_ADDITIONAL_FEATURES_CHECK_CRC_SWITCHING_LINE    0x000000400

#define TXCHANNEL_ADDITIONAL_FEATURES_CRC_INSERT_N          0x000000001
#define TXCHANNEL_ADDITIONAL_FEATURES_ENABLE_SMPTE352M      0x000000002
#define TXCHANNEL_ADDITIONAL_FEATURES_COUPLED_2             0x000000004
#define TXCHANNEL_ADDITIONAL_FEATURES_FIELD_MODE            0x000000020 
#define TXCHANNEL_ADDITIONAL_FEATURES_RXONTX_ENABLE         0x000000080 
#define TXCHANNEL_ADDITIONAL_FEATURES_RXONTX_DELAY_MSK      0x000000070 
#define TXCHANNEL_ADDITIONAL_FEATURES_DISABLE_CLAMPING      0x000000100 
#define TXCHANNEL_ADDITIONAL_FEATURES_NO_CHROMA_ON_KEY      0x000000800 
#define TXCHANNEL_ADDITIONAL_FEATURES_COUPLED_4             0x000002000

#define TXCHANNEL_ADDITIONAL_FEATURES_BIT_RXONTX_DELAY(delay)        ((delay<<4) & TXCHANNEL_ADDITIONAL_FEATURES_RXONTX_DELAY_MSK)
#define TXCHANNEL_ADDITIONAL_FEATURES_GET_BIT_RXONTX_DELAY(add_feat) ((add_feat&TXCHANNEL_ADDITIONAL_FEATURES_RXONTX_DELAY_MSK) >> 4)


/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
   typedef struct 
   {
      BOOL32 CarrierUndetected_B;
      BOOL32 Locked_B;
      SCENARIO Scenario_E;
      SDI_VIDEOSTD VidStd_e;
      BOOL32 ValidVPID_B;
      int LinkAssignement_i;
      BOOL32 AlignedPhysicalLink_B;   //only valid when the stream is started
   }X330_SDI_RX_STATUS;

   typedef struct
   {
      BOOL32               AutoDetectInputVidStd_B;
      SDI_VIDEOSTD         VidStd_e;
      SDI_DATA_TYPE        DataType_e;
      SDI_DATA_FORMAT      DataFormat_e;
      ULONG                AdditionalFeatures_UL;
      SCENARIO   ScenarioQuadLink_e;
   } RX_CHANNEL_START_PARAM;

   typedef struct
   {
      ULONG DataTypeMask_UL;
   }RX_CHANNEL_CHANGE_DATA_TYPE;

    typedef struct
   {
      BOOL32 Coupled_B;
      BOOL32 Coupled4K_B;
      BOOL32 Merged4K_B;
      BOOL32 Dual_B;
      BOOL32 LevelB3G_B;
   }CHANNEL_STOP_PARAM;

   typedef struct
   {
      SDI_VIDEOSTD         VidStd_e;
      SDI_DATA_TYPE        DataType_e;
      SDI_DATA_FORMAT      DataFormat_e;
      BOOL32               GenlockEnable_B;
      ULONG                Genlock_VOffset_UL;
      ULONG                Genlock_HOffset_UL;
      ULONG                AdditionalFeatures_UL;
      ULONG                LinePadding_UL;
      ULONG                MTGChoice_UL;
      ULONG                PaddingSize_UL;
      BOOL32               IsKeyerEnable_B;
      ULONG                Preload_UL;
      ULONG                BufQueueDepth_UL;
      SCENARIO             Scenario_e;
   } TX_CHANNEL_START_PARAM;

   typedef struct  
   {
      UBYTE NbRxChannels_UB;
      UBYTE NbTxChannels_UB;
      ULONG FirmwareVersion_UL;
      X3XX_PRODUCT_FAMILY ProductFamily_E;
      LONGLONG SSN_LL;
      ULONG BoardFeatures_UL;
      X330_FW_ID FpgaFwId_E;
      X330_FW_ID ARMFwId_E;
      ULONG RqstFpgaFwVersion_UL;
      ULONG ArmVersion_UL;
      ULONG RqstArmFwVersion_UL;
      ULONG NbUpgradableStuff_UL;
      ULONG NbOfKeyer_UL;

   } BOARD_INFO;

   enum CHANNEL
   {
      CHANNEL_RX,
      CHANNEL_TX
   };   

   /*********************************************************************************************************************//**
    * @enum SDI_REF_CLOCK_SRC
    * @brief This enumeration gives all available reference clock sources.
    * @remark The enumeration value are built with tag on the most significant byte to easily determine the source type for
    *         further parameters checking. Used tags are :
    *         - 0x00 : Local clock
    *         - 0x01 : Analog reference
    *         - 0x02 : External clock
    *         - 0x03 : Reference from input channel (RXx)
    *************************************************************************************************************************/

   enum SDI_REF_CLOCK_SRC
   {
      SDI_REF_CLOCK_SRC_LOCAL      =0x00000000,          ///< local 27Mhz oscillator
      SDI_REF_CLOCK_SRC_PROG_LOCAL =0x00000001,          ///< Programmable local oscillator
      SDI_REF_CLOCK_SRC_BB         =0x01000000,          ///< Black burst
      SDI_REF_CLOCK_SRC_EX0        =0x02000000,          ///< External connector 0
      SDI_REF_CLOCK_SRC_EX1        =0x02000001,          ///< External connector 1
      SDI_REF_CLOCK_SRC_RX0        =0x03000000,          ///< Clock from RX0
      SDI_REF_CLOCK_SRC_RX1        =0x03000001,          ///< Clock from RX1
      SDI_REF_CLOCK_SRC_RX2        =0x04000000,          ///< Clock from RX2
      SDI_REF_CLOCK_SRC_RX3        =0x04000001,          ///< Clock from RX3
      SDI_REF_CLOCK_SRC_RX4        =0x05000000,          ///< Clock from RX4
      SDI_REF_CLOCK_SRC_RX5        =0x05000001,          ///< Clock from RX5
      SDI_REF_CLOCK_SRC_RX6        =0x06000000,          ///< Clock from RX6
      SDI_REF_CLOCK_SRC_RX7        =0x06000001,          ///< Clock from RX7
      SDI_REF_CLOCK_SRC_UNDEFINED  =0xFFFFFFFF
   } ;

   enum CLOCK_DIVIDER
   {
      CLOCK_DIVIDER_1=0,      ///< Specifies a clock divider set to 1.0 to obtain frame rate like 50 or 60Hz
      CLOCK_DIVIDER_1001      ///< Specifies a clock divider set to 1.001 to obtain NTSC like frame rate (59.xx Hz)
   };

   enum HDMI_SOURCE
   {
      HDMI_SOURCE_PHYSICAL_RX0,
      HDMI_SOURCE_PHYSICAL_RX1,
      HDMI_SOURCE_PHYSICAL_TX0,
      HDMI_SOURCE_PHYSICAL_TX1,
      HDMI_SOURCE_PHYSICAL_TX2,
      HDMI_SOURCE_PHYSICAL_TX3
   };

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

class CX330DrvIF : public CX3XXDrvIF
{
public:

   CX330DrvIF(ULONG BoardIdx_UL);
   static ULONG GetNbBoard();

   virtual CBufferQueue * CreateBufferQueue();   

   const BOARD_INFO * Board_GetInfo() {return &m_BoardInfo_X;}

   CDRVIF_STATUS Board_LoadDriverProperties();
   CDRVIF_STATUS Genlock_GetGnlkVideoStandard(int GnlkIdx_i, SDI_VIDEOSTD *pIncomingSdiVidStd_E);
   void          Genlock_GetStatus(BOOL32* pRefPresent_B, BOOL32* pMtgLocked_B, ULONG LogicalGenlockIdx_UL);
   void          Genlock_SetOffset(int LogicalGenlockIdx_UL, ULONG Offset_UL);
   CDRVIF_STATUS Board_GenlockEnable(int GnlkIdx_i, BOOL32 Enable_B, SDI_VIDEOSTD GnlkVidStd_E = SDI_VIDEOSTD_UNKNOWN_STD);
   CDRVIF_STATUS Board_GetInputFormat(int GnlkIdx_i, SDI_VIDEOSTD *pInputFormat_UL);
   CDRVIF_STATUS Board_SetVCXO(int GnlkIdx_i, ULONG VCXOValue_UL);   
   CDRVIF_STATUS Board_GenlockActivate(int GnlkIdx_i, BOOL32 Activate_B);
   CDRVIF_STATUS Board_ARMStopHDMI();
   CDRVIF_STATUS Board_ARMStartHDMI(HDMI_SOURCE HDMISource_E, SDI_DATA_FORMAT HDMIPacking_E, SDI_VIDEOSTD VideoStd_E);
   CDRVIF_STATUS Board_GetHDMISts(ULONG *pHDMISts_UL);
   CDRVIF_STATUS Board_ReadHDMIEDID(BYTE *pEDIDBuffer);
   ULONG Board_GetBypassRelay( int RelayID_i, BOOL32 *pEnable_B );
   CDRVIF_STATUS Board_LatchPhase();
   static ULONG Helper_SoftBrandingUnChallenge (ULONG Challenge_UL, ULONG BrandingKey_UL);
   CDRVIF_STATUS Board_BiDirCfg(X330_BIDIR_CFG BiDirCfg_E);
   CDRVIF_STATUS Board_GetTemperature(ULONG *pTemperature_UL);
   CDRVIF_STATUS Board_Get27MhzTime(LONGLONG *p27MHzTimeStamp);

   CDRVIF_STATUS Channel_ConfigureCSC(UBYTE Channel_UB,SDI_DATA_FORMAT DataFormat_e,BOOL32 IsHD_B, BOOL32 InputCSC_B);
   CDRVIF_STATUS Channel_SdiRx_Start( UBYTE Channel_UB, RX_CHANNEL_START_PARAM * pParam_X);
   CDRVIF_STATUS Channel_SdiRx_EnableDataType( UBYTE Channel_UB, ULONG DataTypeMask_UL);
   CDRVIF_STATUS Channel_SdiRx_EnableDataType( UBYTE Channel_UB, RX_CHANNEL_CHANGE_DATA_TYPE *pParam_X);
   CDRVIF_STATUS Channel_SdiRx_DisableDataType( UBYTE Channel_UB, ULONG DataTypeMask_UL);
   CDRVIF_STATUS Channel_SdiRx_DisableDataType( UBYTE Channel_UB, RX_CHANNEL_CHANGE_DATA_TYPE *pParam_X);
   CDRVIF_STATUS Channel_SdiRx_Stop( UBYTE Channel_UB);
   CDRVIF_STATUS Channel_SdiRx_Stop( UBYTE Channel_UB, CHANNEL_STOP_PARAM *pParam_X );
   CDRVIF_STATUS Channel_SdiRx_GetStatus( UBYTE Channel_UB, X330_SDI_RX_STATUS *pStatus_X);
   ULONG Channel_SdiRx_GetPhase( UBYTE Channel_UB );
   CDRVIF_STATUS Channel_SdiTx_Start( UBYTE Channel_UB, TX_CHANNEL_START_PARAM * pParam_X);
   CDRVIF_STATUS Channel_SdiTx_Stop( UBYTE ChannelIdx_UB);
   CDRVIF_STATUS Channel_SdiTx_Stop( UBYTE ChannelIdx_UB, CHANNEL_STOP_PARAM *pParam_X );
   CDRVIF_STATUS Channel_SdiTx_ArmPreload(UBYTE ChannelIdx_UL, ULONG NbCoupledLinks_UL);
   CDRVIF_STATUS Channel_SdiTx_SetScenario(UBYTE ChannelIdx_UB, SCENARIO Scenario_E, SDI_VIDEOSTD VideoStandard_E);
   CDRVIF_STATUS Board_SetupVTG (int VTGIndex_i, SDI_VIDEOSTD Standard_E, SDI_DATA_FORMAT DataFormat_e, SCENARIO Scenario_E, ULONG NbCoupledLinks_UL);
   CDRVIF_STATUS Board_SetupMasterVTG (SDI_VIDEOSTD MVTGStandard_E, SDI_VIDEOSTD RefStandard_E, CLOCK_DIVIDER ClkDiv_E, int VTGIdx_i = 0);
   CDRVIF_STATUS Board_SetGenlockClkSrc( SDI_REF_CLOCK_SRC GenlockClkSrc_e, int GnlkIdx_i =0  );

   static void Helper_FillOnBoardAddresses(CHANNEL Channel_e, UBYTE ChnIdx_i, BUFFERQUEUE_PARAM *pBQParam_X, ULONG NbLink_UL, BOOL32 Is4K_B, BOOL32 LinkB_B, ULONG NbCoupledLink_UL, BOOL32 Quadrant_B, ULONG LineSize_UL =0, ULONG LineNb_UL =0, BOOL32 FieldMerge_B = FALSE, ULONG OddLineNb_UL = 0, BOOL32 IsDualInterface_B = FALSE);
   static ULONG Helper_GetOnBoardAddress(CHANNEL Channel_e, UBYTE ChnIdx_i, X330BRD_CHN_DATA_TYPE DataType_e, ULONG BufIdx_UL, ULONG NbLink_UL, BOOL32 Is4K_B, ULONG NbCoupledLink_UL, BOOL32 Quadrant_B, BOOL32 IsDualInterface_B);
   void Board_SetOnBoardBufferBaseAddr(CHANNEL Channel_e, UBYTE ChannelIdx_UB, ULONG NbPhysLink_UL, BOOL32 Is4K_B, BOOL32 LinkB_B, ULONG NbCoupledLink_UL, BOOL32 Quadrant_B, ULONG FrameSize_UL, BOOL32 Planar_B, BOOL32 InvertedPlanar_B, BOOL32 Planar10Bit_B, BOOL32 FieldMode_B, BOOL32 IsDualInterface_B);
   void Board_SetOnBoardBufferBaseAddr(CHANNEL Channel_e, UBYTE ChannelIdx_UB, X330BRD_CHN_DATA_TYPE DataType_e, UBYTE BufferIdx_UB, ULONG BufferBaseAddr_UL);
   void Board_SetCSCRegister(ULONG Adress_UL, ULONG Data_UL);

   
   CDRVIF_STATUS FillRXDriverProperties(UBYTE Channel_UB, RX_CHANNEL_START_PARAM * pParam_X);
   CDRVIF_STATUS FillTXDriverProperties(UBYTE ChannelIdx_UB, BOOL32 SWModifyStream_B, BOOL32 FieldMode_B, BOOL32 DL1080p_B, ULONG Preload_UL, ULONG BufQueueDepth_UL);

   CDRVIF_STATUS Board_DetectCompanionCard(X3XX_COMPANION_CARD_TYPE CompanionCardType_E, BOOL32 *pCompanionCard_B);
   CDRVIF_STATUS Board_GetTimeCode(X3XX_TIMECODE_SOURCE TcSource_E, BOOL32 *pLocked_B, float *pFrameRate_f, ULONGLONG *pTimeCode_ULL);
   CDRVIF_STATUS Board_GetLtcPacket( ULONGLONG *pValueLTC_ULL, UBYTE *pStatus_UB);
   void Keyer_KCompressorInit();
   static BOOL32 IsBoardFeatureAvailable(ULONG BoardFeatures_UL, ULONG Feature_UL);

   BOARD_INFO* GetBoardInfo();


private:
   void MapChannelIdx(UBYTE * pChannelIndex_UB);
   BOOL32 IsHDDualMap1( SCENARIO Scenario_e, SDI_VIDEOSTD VideoStandard_E);


protected:
   CDRVIF_STATUS SetRxCmdReg( UBYTE Channel_UB, ULONG RxCmd_UL );
   CDRVIF_STATUS SetBitRxCmdReg( UBYTE Channel_UB, ULONG BIT_UL );
   CDRVIF_STATUS ClrBitRxCmdReg( UBYTE Channel_UB, ULONG BIT_UL );
   CDRVIF_STATUS GetRxCmdReg( UBYTE Channel_UB, ULONG * pRxCmd_UL );
   CDRVIF_STATUS SetTxCmdReg( UBYTE Channel_UB, ULONG TxCmd_UL  );
   CDRVIF_STATUS SetBitTxCmdReg( UBYTE Channel_UB, ULONG BIT_UL );
   CDRVIF_STATUS ClrBitTxCmdReg( UBYTE Channel_UB, ULONG BIT_UL );
   CDRVIF_STATUS SetTxScenarioReg( UBYTE Channel_UB, ULONG Scenario_UL );
   PMM32_REGISTER GetTxCFG_DReg( UBYTE Channel_UB );
   ULONG GetRxStsReg( UBYTE Channel_UB );  
   ULONG GetRxVpidReg( UBYTE Channel_UB );

   BOARD_INFO m_BoardInfo_X;   

   /* We have to translate logical (starting from 0) genlock indices to physical genlock indices. 
    * On HD-62 there is a genlock 1 but no genlock 0, so virtual genlock 0 maps to physical genlock 1. */
   struct  
   {
      ULONG mMailboxGenlockEnableCmd_UL;          /* MB_GENLOCK_x_ENABLE */
      ULONG mMailboxGenlockDisableCmd_UL;         /* MB_GENLOCK_x_DISABLE */
      ULONG mMailboxGenlockGetInputFormatCmd_UL;  /* MB_GENLOCK_x_GET_INPUT_FORMAT */
      ULONG mMailboxGenlockSetVcxoCmd_UL;         /* MB_GENLOCK_x_SET_VCXO_VOLTAGE */
      ULONG mTxCmdMtgChoiceBit_UL;                /* FPGA_TXx_BIT_MTG_CHOICE or 0 */
      ULONG mGstsGenlockRefBit_UL;                /* FPGA_GSTS_BIT_GENLOCK_x_REF */
      ULONG mGstsMtgLockedBit_UL;                 /* FPGA_GSTS_BIT_MTG_x_LOCKED */
      ULONG mGcmdGenlockEnableBit_UL;             /* FPGA_GCMD_BIT_GENLOCK_x_ENABLE */
      ULONG mGcmdStartBit_UL;                     /* FPGA_GCMD_BIT_MTG_x_START */
      ULONG mGcmdResetBit_UL;                     /* FPGA_GCMD_BIT_MTG_x_RESET */
      ULONG mGcmdGenlockSourceShift_UL;           /* FPGA_GCMD_BIT_GENLOCK_x_SOURCE */
      ULONG mGcmdGenlockSourceMask_UL;            /* FPGA_GCMD_MSK_GENLOCK_x_SOURCE */
      ULONG mVPulseOffsetBit_UL;                  /* FPGA_VPULSE_OFFSET_BIT_MTG_1 or 0 */
      PMM32_REGISTER mpMtgConfig_mm32;            /* FPGA_MTGx_CFG_D_MM32 */
   } mpGenlockDesc_X[2];
};

class CX330DrvBufferQueue : public CBufferQueue
{

   friend class CX330DrvIF;

protected:

   CX330DrvBufferQueue(CX330DrvIF * pOwner_O);

private:

   CX330DrvIF * m_pOwner_O;


};

};
#endif // _CX300DRVIF_H_
