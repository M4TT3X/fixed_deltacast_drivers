/********************************************************************************************************************//**
 * @internal
 * @file   	X330Drv_RegistersMap.h
 * @date   	2014/08/29
 * @author 	bc
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2014/08/29  v00.01.0000    bc       Creation of this file

 **********************************************************************************************************************/

#ifndef _X330DRV_REGISTERSMAP_H_
#define _X330DRV_REGISTERSMAP_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "Register.h"

#define X330_DMA_PADDING_SIZE 0x200

#ifdef DRIVER
#define FPGA_MAILBOX_DI_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x90)
#define FPGA_MAILBOX_ADDR_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x98)
#endif

#define FPGA_GCMD_BOARD_TS_LATCH                      0x00000020 // �1� to latch the current value of  Board TS counter (Auto-clear)
#define FPGA_GCMD_MSK_GENLOCK_B_SOURCE                0x00003c00
#define FPGA_GCMD_BIT_GENLOCK_B_ENABLE                0x00004000 //�1� to activate the Genlock feature
#define FPGA_GCMD_BIT_MTG_B_RESET                     0x00008000 //�1� to reset the Master Timing Generator 0
#define FPGA_GCMD_BIT_MTG_B_START                     0x00010000 //�1� to start the Master Timing Generator 0
#define FPGA_GCMD_BIT_LOW_POWER_MODE                  0x80000000 //�0� to set the board in low power mode

#define FPGA_GCMD_BIT_GENLOCK_B_SOURCE(genlock_source)      ((genlock_source<<10)&FPGA_GCMD_MSK_GENLOCK_B_SOURCE)

#define PCIE_LINK_WIDTH_1                    0x0   //'0' = 1x PCIe
#define PCIE_LINK_WIDTH_2                    0x1   //'1' = 2x PCIe
#define PCIE_LINK_WIDTH_4                    0x2   //'2' = 4x PCIe
#define PCIE_LINK_WIDTH_8                    0x3   //'3' = 8x PCIe
#define FPGA_GSTS_GET_BIT_LINK_WIDTH(reg)    ((reg&FPGA_GSTS_MSK_LINK_WIDTH) >>20)

#define FPGA_HDMI_CTL_SHRMM32    DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xA0)
#define FPGA_MTG_B_CFG_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xA8)
#define FPGA_MTG_M_CFG_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xB0)
#define FPGA_VPULSE_OFFSET_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0xB8)
//#define FPGA_BTC                 DEFINE_MMREG32(FPGA_PCI_BAR,0xC0)
#define FPGA_KEYER_CTRL_SHRMM32          DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xC8)
#define FPGA_PP_PARAMS_A       DEFINE_MMREG32(FPGA_PCI_BAR,0xD0)
#define FPGA_PP_PARAMS_D       DEFINE_MMREG32(FPGA_PCI_BAR,0xD8)
#define FPGA_PARAM_ROM_A_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0xE0)
#define FPGA_PARAM_ROM_D_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0xE8)
#define FPGA_SSR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0xF0)
#define FPGA_IMR_RX_SHRMM32      DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xF8)
#define FPGA_ICR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x100)
#define FPGA_RX0_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x108) 
#define FPGA_RX1_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x110) 
#define FPGA_RX2_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x118) 
#define FPGA_RX3_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x120) 
#define FPGA_RX4_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x128) 
#define FPGA_RX5_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x130) 
#define FPGA_RX6_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x138) 
#define FPGA_RX7_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x140) 
#define FPGA_SSR_TX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x148)
#define FPGA_IMR_TX_SHRMM32      DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x150)
#define FPGA_ICR_TX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x158)
#define FPGA_TX0_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x160) 
#define FPGA_TX1_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x168) 
#define FPGA_TX2_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x170) 
#define FPGA_TX3_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x178) 
#define FPGA_TX4_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x180) 
#define FPGA_TX5_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x188) 
#define FPGA_TX6_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x190) 
#define FPGA_TX7_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x198) 
#define FPGA_TX0_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1A0) 
#define FPGA_TX1_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1A8) 
#define FPGA_TX2_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1B0) 
#define FPGA_TX3_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1B8) 
#define FPGA_TX4_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1C0) 
#define FPGA_TX5_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1C8) 
#define FPGA_TX6_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1D0) 
#define FPGA_TX7_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1D8) 
#define FPGA_CH0_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1E0)
#define FPGA_CH1_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1E8)
#define FPGA_CH2_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1F0)
#define FPGA_CH3_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1F8)
#define FPGA_CH4_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x200)
#define FPGA_CH5_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x208)
#define FPGA_CH6_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x210)
#define FPGA_CH7_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x218)
#define FPGA_DMAADVRX_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x1E0)  /** Advanced DMA Settings � PC to FPGA */
#define FPGA_DMAADVTX_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x1E8)  /** Advanced DMA Settings � FPGA to PC */
#define FPGA_CSC_A_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x220)
#define FPGA_CSC_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x228)
#define FPGA_TX_SCEN_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x238) 
#define FPGA_K_COMP_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x240) 
#define FPGA_TCE_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x368)
#define FPGA_LTC_CHANNEL_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x248)
#define FPGA_LTC_VALUE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x360)

//Bar 1 register map (RD)
#define FPGA_SR_MM32             DEFINE_MMREG32(FPGA_PCI_BAR,0x08)
#define FPGA_ISR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x10)
#define FPGA_REVISION_ID_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x18)
//#define FPGA_DMA_BC_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x28)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x30)
//#define FPGA_DMA1_BC_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x38)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x40)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x48)
//#define FPGA_VCXO_CTL_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x50)

#ifdef DRIVER
#define FPGA_MAILBOX_DO_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x88)
#endif

//#define FPGA_MTG_CTL_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x90)
#define FPGA_BTC_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x98)
#define FPGA_SR_RX_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xA0)
#define FPGA_ISR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0xA8)
#define FPGA_RXBUFID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xB0)
#define FPGA_RX0_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xB8) 
#define FPGA_RX1_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xC0) 
#define FPGA_RX2_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xC8) 
#define FPGA_RX3_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xD0) 
#define FPGA_RX4_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xD8) 
#define FPGA_RX5_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xE0) 
#define FPGA_RX6_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xE8) 
#define FPGA_RX7_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xF0) 
#define FPGA_RX0_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0xF8) 
#define FPGA_RX1_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x100) 
#define FPGA_RX2_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x108) 
#define FPGA_RX3_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x110) 
#define FPGA_RX4_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x118) 
#define FPGA_RX5_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x120) 
#define FPGA_RX6_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x128) 
#define FPGA_RX7_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x130) 
#define FPGA_FABC0_A_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x138) 
#define FPGA_FABC0_A_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x140) 
#define FPGA_FABC0_B_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x148) 
#define FPGA_FABC0_B_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x150) 
#define FPGA_FABC1_A_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x158) 
#define FPGA_FABC1_A_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x160) 
#define FPGA_FABC1_B_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x168) 
#define FPGA_FABC1_B_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x170) 
#define FPGA_FABC2_A_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x178) 
#define FPGA_FABC2_A_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x180) 
#define FPGA_FABC2_B_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x188) 
#define FPGA_FABC2_B_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x190) 
#define FPGA_FABC3_A_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x198) 
#define FPGA_FABC3_A_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1A0) 
#define FPGA_FABC3_B_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1A8) 
#define FPGA_FABC3_B_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1B0) 
#define FPGA_FABC4_A_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1B8) 
#define FPGA_FABC4_A_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1C0) 
#define FPGA_FABC4_B_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1C8) 
#define FPGA_FABC4_B_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1D0) 
#define FPGA_FABC5_A_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1D8) 
#define FPGA_FABC5_A_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1E0) 
#define FPGA_FABC5_B_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1E8) 
#define FPGA_FABC5_B_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1F0) 
#define FPGA_FABC6_A_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1F8) 
#define FPGA_FABC6_A_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x200) 
#define FPGA_FABC6_B_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x208) 
#define FPGA_FABC6_B_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x210) 
#define FPGA_FABC7_A_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x218) 
#define FPGA_FABC7_A_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x220) 
#define FPGA_FABC7_B_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x228) 
#define FPGA_FABC7_B_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x230) 

#define FPGA_RX0_VPID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x238) 
#define FPGA_RX1_VPID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x240) 
#define FPGA_RX2_VPID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x248) 
#define FPGA_RX3_VPID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x250) 
#define FPGA_RX4_VPID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x258) 
#define FPGA_RX5_VPID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x260) 
#define FPGA_RX6_VPID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x268) 
#define FPGA_RX7_VPID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x270) 

#define FPGA_SR_TX_MM32           DEFINE_MMREG32(FPGA_PCI_BAR,0x278)
#define FPGA_ISR_TX_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x280)
#define FPGA_CRC_RX_01_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x298)
#define FPGA_CRC_RX_23_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x2A0)
#define FPGA_CRC_RX_45_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x2A8)
#define FPGA_CRC_RX_67_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x2B0)

#define FPGA_RX0_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2B8)
#define FPGA_RX0_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2C0) 
#define FPGA_RX1_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2C8) 
#define FPGA_RX1_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2D0) 
#define FPGA_RX2_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2D8) 
#define FPGA_RX2_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2E0) 
#define FPGA_RX3_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2E8) 
#define FPGA_RX3_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2F0) 
#define FPGA_RX4_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x2F8) 
#define FPGA_RX4_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x300) 
#define FPGA_RX5_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x308) 
#define FPGA_RX5_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x310) 
#define FPGA_RX6_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x318) 
#define FPGA_RX6_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x320) 
#define FPGA_RX7_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x328) 
#define FPGA_RX7_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x330) 
#define FPGA_BRD_TS_MSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x338) 
#define FPGA_BRD_TS_LSB_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x340) 
#define FPGA_BB_TS_MSB_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x348) 
#define FPGA_BB_TS_LSB_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x350) 





// Keyer parameters processing
// -- HWPROCESSING_PARAM_KEYER_ADDRESS
#define HWPROCESSING_PARAM_RX_BLACKOUT                   0x01
#define HWPROCESSING_PARAM_BLENDER_MIN_CLIP              0x02  // Alpha Blender Key Min Clip 10 bits
#define HWPROCESSING_PARAM_BLENDER_MAX_CLIP              0x03  // Alpha Blender Key Max Clip 10 bits
#define HWPROCESSING_PARAM_BLENDER_ONE_OVER_MAX_CLIP     0x04  //(2^20)/(MaxClip - MinClip) 18 bits
#define HWPROCESSING_PARAM_K_PROCESSING                  0x05
#define HWPROCESSING_PARAM_Y_MINMAX_CLIP                 0x06
#define HWPROCESSING_PARAM_C_MINMAX_CLIP                 0x07
#define HWPROCESSING_PARAM_BLENDER_MODE                  0x08  // blender mode register 2 bits
#define HWPROCESSING_PARAM_OUTPUT_SELECTION              0x09

// -- HWPROCESSING_PARAM_BLENDER_MODE
#define HWPROCESSING_PARAM_BLENDER_MODE_BIT_ADDITIVE     0x1   //0 : Alpha blender in multiplicative mode (default mode) - 1 : Alpha blender in additive mode
#define HWPROCESSING_PARAM_BLENDER_MODE_BIT_ALPHA_ON_Y   0x2   //0 : Alpha channel is on alpha path - 1 : Alpha channel is in luminance path

// -- HWPROCESSING_PARAM_OUTPUT_SELECTION
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_MASK                0x03
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_TX                  0x00
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_RX                  0x01
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_BACK                0x02
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_CK                  0x03

#define HWPROCESSING_INPUT_A_VIDEO_SOURCE_MASK               0x01
#define HWPROCESSING_INPUT_A_VIDEO_SOURCE_TX                 0x00
#define HWPROCESSING_INPUT_A_VIDEO_SOURCE_RX                 0x01

#define HWPROCESSING_INPUT_A_VIDEO_SOURCE_BIT(val)     ((val&HWPROCESSING_INPUT_A_VIDEO_SOURCE_MASK)<<2)

#define HWPROCESSING_INPUT_B_VIDEO_SOURCE_MASK               0x01
#define HWPROCESSING_INPUT_B_VIDEO_SOURCE_TX                 0x00
#define HWPROCESSING_INPUT_B_VIDEO_SOURCE_RX                 0x01

#define HWPROCESSING_INPUT_B_VIDEO_SOURCE_BIT(val)     ((val&HWPROCESSING_INPUT_B_VIDEO_SOURCE_MASK)<<3)


#define HWPROCESSING_OUTPUT_ANC_SOURCE_MASK                  0x03
#define HWPROCESSING_OUTPUT_ANC_SOURCE_BACK                  0x02
#define HWPROCESSING_OUTPUT_ANC_SOURCE_RX                    0x01
#define HWPROCESSING_OUTPUT_ANC_SOURCE_TX                    0x00

#define HWPROCESSING_OUTPUT_ANC_PRIORITY_MASK                0x01
#define HWPROCESSING_OUTPUT_ANC_PRIORITY_NONE                0x00
#define HWPROCESSING_OUTPUT_ANC_PRIORITY_TX0                 0x01

#define HWPROCESSING_OUTPUT_SELECTION_BIT_VIDEO_TXi(source)  ((source&0x03))
#define HWPROCESSING_OUTPUT_SELECTION_BIT_ANC_TXi(source)    ((source&0x03)<<2)
#define HWPROCESSING_OUTPUT_PRIORITY_BIT_ANC_TXi(source)     ((source&0x01)<<4)

#define HWPROCESSING_PARAM_K_PROCESSING_INVERT_KEY_MASK       0x800
#define HWPROCESSING_SEL_BIT_PROCESSING_INVERT_KEY(source)    (source<<11)
#define HWPROCESSING_PARAM_K_PROCESSING_ALPHA_BF_MASK         0x7FF

#define HWPROCESSING_PARAMS_A_KEYER_INDEX_MASK                0x03
#define HWPROCESSING_PARAMS_A_MASK                            0x3F
         





//FPGA_CSC_A_MM32
#define FPGA_CSC_A_MSK_COEF_SEL     0x0000000F
#define FPGA_CSC_A_MSK_CSC_SEL      0x000000F0
#define FPGA_CSC_A_BIT_COEF_SEL(coef)  ((coef) & FPGA_CSC_A_MSK_COEF_SEL)
#define FPGA_CSC_A_BIT_CSC_SEL_RX(idx) ((idx<<4) & FPGA_CSC_A_MSK_CSC_SEL) 
#define FPGA_CSC_A_BIT_CSC_SEL_TX(idx) (((idx+8)<<4) & FPGA_CSC_A_MSK_CSC_SEL) 

#define CSC_COEF_K11    0x0
#define CSC_COEF_K12    0x1
#define CSC_COEF_K13    0x2
#define CSC_COEF_K21    0x3
#define CSC_COEF_K22    0x4
#define CSC_COEF_K23    0x5
#define CSC_COEF_K31    0x6
#define CSC_COEF_K32    0x7
#define CSC_COEF_K33    0x8
#define CSC_COEF_O1     0x9
#define CSC_COEF_O2     0xA
#define CSC_COEF_O3     0xB
#define CSC_COEF_C1     0xC
#define CSC_COEF_C2     0xD
#define CSC_COEF_C3     0xE


//FPGA_CSC_D_MM32
#define FPGA_CSC_D_MSK_VALUE   0x0003FFFF //Coefficient value



#define FPGA_CSC_A_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x220)
#define FPGA_CSC_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x228)




#define HWPROCESSING_CK_OUTPUT_FOREGROUND             0x00  ///< Specifies foreground as output of CK module
#define HWPROCESSING_CK_OUTPUT_BACKGROUND             0x01  ///< Specifies background as output of CK module

#define HWPROCESSING_PARAM_CK_ENABLE                  0x00
#define HWPROCESSING_PARAM_CK_MAIN_OUT_SELECT         0x10
#define HWPROCESSING_PARAM_CK_MONITOR_OUT_SELECT      0x15

#define HWPROCESSING_PARAM_ALPHA_TBAR                 0x1A


#define FPGA_HD_TXx_HD                 0x00080000  // Set for HD transmission, clear for SD transmission


//FPGA_GCMD_SHRMM32
// -> see Board.h


//FPGA_HDMI_CTL
#define FPGA_HDMI_CTL_BIT_ENABLE                      0x00000001

#define FPGA_HDMI_CTL_MASK_SEL                        0x00000006
#define FPGA_HDMI_CTL_SEL_RX0                         0x00000000
#define FPGA_HDMI_CTL_SEL_TX0                         0x00000001
#define FPGA_HDMI_CTL_SEL_RX1                         0x00000002
#define FPGA_HDMI_CTL_SEL_TX1                         0x00000003

#define FPGA_HDMI_CTL_SEL_04_TX0                      0x00000000
#define FPGA_HDMI_CTL_SEL_04_TX1                      0x00000001
#define FPGA_HDMI_CTL_SEL_04_TX2                      0x00000002
#define FPGA_HDMI_CTL_SEL_04_TX3                      0x00000003

#define FPGA_HDMI_CTL_BIT_SEL(sel)                ((sel<<1)&FPGA_HDMI_CTL_MASK_SEL)





//FPGA_GSTS_MM32
// -> see Board.h


#define GENLOCK_SOURCE_FREERUN      0x0
#define GENLOCK_SOURCE_RX0          0x1   // RX0 is taken as Genlock source
#define GENLOCK_SOURCE_RX1          0x2   // RX1 is taken as Genlock source     
#define GENLOCK_SOURCE_RX2          0x3   // RX2 is taken as Genlock source     
#define GENLOCK_SOURCE_RX3          0x4   // RX3 is taken as Genlock source     
#define GENLOCK_SOURCE_RX4          0x5   // RX4 is taken as Genlock source     
#define GENLOCK_SOURCE_RX5          0x6   // RX5 is taken as Genlock source  
#define GENLOCK_SOURCE_RX6          0x7   // RX6 is taken as Genlock source     
#define GENLOCK_SOURCE_RX7          0x8   // RX7 is taken as Genlock source  
#define GENLOCK_SOURCE_BLACK_BURST  0x9   // Black burst is taken as Genlock source

//FPGA_SSR_MM32, FPGA_IMR_SHRMM32, FPGA_ISR_MM32 and FPGA_ISR_MM32
// -> see Board.h


//FPGA_VPULSE_OFFSET_MM32
#define FPGA_VPULSE_OFFSET_BIT_MTG_M  0x80000000 /* '0' = MTG B - '1' = MTG M */

//MAILBOX_DATA indirect address
//see Board.h

//Cmd Tag
//->see also Board.h
#define MB_GENLOCK_0_ENABLE               0x00010002   /* Enable the genlock 0 */
#define MB_GENLOCK_1_ENABLE               0x00010003   /* Enable the genlock 1 */
#define MB_GENLOCK_0_DISABLE              0x00000004   /* Disable the genlock 0 (freerun operation) */
#define MB_GENLOCK_1_DISABLE              0x00000005   /* Disable the genlock 1 (freerun operation) */
#define MB_GENLOCK_0_GET_INPUT_FORMAT     0x00000006   /* Video format at the genlock 0�s input */
#define MB_GENLOCK_1_GET_INPUT_FORMAT     0x00000007   /* Video format at the genlock 1�s input */
#define MB_GENLOCK_0_SET_VCXO_VOLTAGE     0x00010008   /* Set the voltage of the VCXO attached to genlock 0  */
#define MB_GENLOCK_1_SET_VCXO_VOLTAGE     0x00010009   /* Set the voltage of the VCXO attached to genlock 1  */
#define MB_START_HDMI                     0x0002000B   /* Start HDMI monitoring*/
#define MB_STOP_HDMI                      0x0000000C   /* Stop HDMI monitoring*/
#define MB_GET_HDMI_STS                   0x0000000D   /* Get HDMI status*/
#define MB_READ_HDMI_EDID                 0x0000000E   /* Read HDMI EDID*/
#define MB_SFP_WRITE(NbReg)               (((2+((NbReg)+sizeof(UWORD)-1)/sizeof(UWORD))<< 16) | 0x0010)
#define MB_SFP_READ	    	               0x00020011
#define MB_SFP_AUTHORIZE            	   0x000A0012
#define MB_SFP_SET_TABLE(NbEntry)         (((2+((NbEntry)*2+sizeof(UWORD)-1)/sizeof(UWORD))<< 16) | 0x0013)
#define MB_SFP_ACK    	                  0x00010014

// HDMI status
#define HDMISTS_HOTPLUG_DETECTED       0x00000100
#define HDMISTS_CORRECTLY_IDENTIFIED   0x00000200

//FPGA_END_OF_DMA_MM32
#define FPGA_EOD_RXx(i)        (1<<(i))
#define FPGA_EOD_TXx(i)        (1<<(8+(i)))

//FPGA_SSR_RX_MM32, FPGA_IMR_RX_SHRMM32, FPGA_ISR_RX_MM32 and FPGA_ISR_RX_MM32
#define FPGA_SR_RX_BIT_RXi_PARITY(i)        (0x1<<((i)*4))
#define FPGA_SR_RX_BIT_RXi_OVERRUN(i)       (0x2<<((i)*4))
#define FPGA_SR_RX_BIT_RXi_STS_CHANGE(i)    (0x4<<((i)*4))
#define FPGA_SR_RX_BIT_RXi(i)               (0x8<<((i)*4))


#define FPGA_SR_TX_BIT_TXi_UNDERRUN(i)      (0x2<<(((i))*4))
#define FPGA_SR_TX_BIT_TXi(i)               (0x8<<(((i))*4))


//FPGA_RXx_MM32
#define FPGA_RXx_MSK_CAPTURE_MODE            0x0000000F
#define FPGA_RXx_BIT_CAPTURE_MODE_ENABLED    0x00000001
#define FPGA_RXx_BIT_CAPTURE_MODE_VIDEO      0x00000002
#define FPGA_RXx_BIT_CAPTURE_MODE_ANC        0x00000004
#define FPGA_RXx_BIT_CAPTURE_MODE_RAW        0x00000008
#define FPGA_RXx_MSK_PACKING_MODE            0x000001f0
#define FPGA_RXx_BIT_SD_MODE                 0x00000200
#define FPGA_RXx_BIT_VBI_10BIT               0x00000400
#define FPGA_RXx_BIT_PLANAR                  0x00000800
#define FPGA_RXx_BIT_FIELD_MODE              0x00001000
#define FPGA_RXx_BIT_LEVELB                  0x00002000
#define FPGA_RXx_BIT_CSC_ENABLE              0x00004000
#define FPGA_RXx_BIT_DISABLE_CHECK_CRC_SWL   0x00008000  
#define FPGA_RXx_MSK_CSC_MODE                0x00010000
#define FPGA_RXx_BIT_GRAPHIC_PADDING         0x00020000  //�1� to enable Graphic Padding (each new line start on a 64B boundary)
#define FPGA_RXx_BIT_VIDEO_SND_PATH          0x00040000 
#define FPGA_RXx_MSK_COUPLED_MODE            0x00C00000 
#define FPGA_RXx_MSK_SCENARIO                0x0F000000  

#define FPGA_RXx_BIT_CSC_MODE(mode)      (((mode)<<16)&FPGA_RXx_MSK_CSC_MODE)
#define CSC_MODE_AVERAGING            0x0 //�0� to convert 4:2:2 to 4:4:4 by averaging
#define CSC_MODE_DUPLICATING          0x1 //�1� to convert 4:2:2 to 4:4:4 by duplicating

#define FPGA_RXx_BIT_PACKING_MODE(pkg)      (((pkg)<<4)&FPGA_RXx_MSK_PACKING_MODE)

#define FPGA_RXx_BIT_QUAD_LINK(quad)      (((quad)<<24)&FPGA_RXx_MSK_SCENARIO)

#define RX_SINGLE_LINK                           0x00000001 
#define RX_SINGLE_LINK_3GB_DL_MAP_I              0x00000002
#define RX_SINGLE_LINK_3GB_DL_MAP_II             0x00000003 
#define RX_DUAL_LINK_HD_MAP_I                    0x00000004 
#define RX_DUAL_LINK_HD_MAP_II                   0x00000005 
#define RX_DUAL_LINK_3GB_DS_4K_QUADRANT          0x00000006 
#define RX_DUAL_LINK_3GB_DS_4K_PIXEL             0x00000007 
#define RX_QUAD_LINK_HD_3GA_4K_QUADRANT          0x00000008 
#define RX_QUAD_LINK_HD_3GA_4K_PIXEL             0x00000009 
#define RX_QUAD_LINK_3GB_DL_4K_QUADRANT          0x0000000A 
#define RX_QUAD_LINK_3GB_DL_4K_PIXEL             0x0000000B 

#define FPGA_RXx_BIT_COUPLED_MODE(mode)      (((mode)<<22)&FPGA_RXx_MSK_COUPLED_MODE)
#define RX_COUPLED_MODE_2             0x1
#define RX_COUPLED_MODE_4             0x2

#define PACKING_4220_8        0x00000000  // 4:2:2:0  8-bit video packing 
#define PACKING_4224_8        0x00000002  // 4:2:2:4  8-bit video packing 
#define PACKING_4220_10       0x00000001  // 4:2:2:0 10-bit video packing 
#define PACKING_4224_10       0x00000003  // 4:2:2:4 10-bit video packing 
#define PACKING_4440_8        0x00000006  // 4:4:4:0  8-bit video packing 
#define PACKING_4444_8        0x00000004  // 4:4:4:4  8-bit video packing 
#define PACKING_4440_10       0x00000007  // 4:4:4:0 10-bit video packing 
#define PACKING_4444_10       0x00000005  // 4:4:4:4 10-bit video packing 
#define PACKING_4200_8        0x00000008  // 4:2:0:0  8-bit video packing 
#define PACKING_4200_10_LSB   0x00000009  // 4:2:0:0 10-bit LSB video packing 
#define PACKING_4200_10_MSB   0x0000000B  // 4:2:0:0 10-bit MSB video packing 

//FPGA_TXx_MM32
#define FPGA_TXx_MSK_OUTPUT_MODE       0x0000000f  // Output Mode
#define FPGA_TXx_BIT_CLAMPING          0x00000010  // Enable clamping
#define FPGA_TXx_ARM_FOR_PRELOAD       0x00000020  // Arm the channel allowing preloading frames on-board
#define FPGA_TXx_BIT_FIELD_MODE        0x00000040  // �1� to use New Field as New Buffer Interrupt source instead of New Picture
#define FPGA_TXx_MSK_COUPLED_MODE      0x00000180  // Coupled mode 
#define FPGA_TXx_MSK_PACKING           0x00001E00  // Packing scheme
#define FPGA_TXx_BIT_KEY_NEUTRAL_CHROMA 0x00002000 // '0' = Chroma values of Key stream are equal to Fill ones - '1' = Chroma values of Key stream are forced to Neutral value (512 on 10-bit)
#define FPGA_TXx_BIT_CRC_INSERT_N      0x00004000  // �0� = The transmitter generates and inserts CRC values on each video line in HD-SDI and 3G-SDI modes. �1� = Disable
#define FPGA_TXx_BIT_PLANAR            0x00008000  // �1� to enable Planar formats
#define FPGA_TXx_MSK_PADDING_SIZE      0x001f0000  // Nbr of pixels into graphic line padding
#define FPGA_TXx_BIT_RX_ON_TX(delay)   ((1<<23)|((delay)<<20))
#define FPGA_TXx_BIT_FILL_KEY          0x00400000  // �1� to enable Fill+Key : Fill on TX(i) and Key on TX(i+4)
#define FPGA_TXx_BIT_INTERNAL_KEYER    0x00800000  // �1� to enable internal keyer : Fill on TX(i) and Key on TX(i+4)
#define FPGA_TXx_BIT_MTG_M             0x01000000  // '0' = MTGB - '1' = MTGM
#define FPGA_TXx_BIT_CSC_ENABLE        0x02000000  // �1� to enable Color Space Converter (RGB to YUV)
#define FPGA_TXx_MSK_SAMPLE_RATE       0x0C000000  // 00 = HD-SDI (including dual link) - 01 = SD-SDI - 10 = 3G-SDI(level A and level B) - 11 = Invalid
#define FPGA_TXx_BIT_SMPTE352M_ENABLE  0x10000000  // Enable automatic SMPTE352M packet insertion on HDe board
#define FPGA_TXx_BIT_GENLOCK_ENABLE    0x20000000  // Enable TXx Genlocking
#define FPGA_TXx_BIT_3G_LEVELB         0x40000000  // Set to 1 to activate level B on TX0
#define FPGA_TXx_BIT_VTG_RST_N         0x80000000  // Reset the address pointer and all counter
#define FPGA_TX0_MSK_SCENARIO          0x0000000F  // scenario TX0
#define FPGA_TX1_MSK_SCENARIO          0x000000F0  // scenario TX1
#define FPGA_TX2_MSK_SCENARIO          0x00000F00  // scenario TX2
#define FPGA_TX3_MSK_SCENARIO          0x0000F000  // scenario TX3
#define FPGA_TX4_MSK_SCENARIO          0x000F0000  // scenario TX4
#define FPGA_TX5_MSK_SCENARIO          0x00F00000  // scenario TX5
#define FPGA_TX6_MSK_SCENARIO          0x0F000000  // scenario TX6
#define FPGA_TX7_MSK_SCENARIO          0xF0000000  // scenario TX7

#define FPGA_TXx_BIT_PADDING_SIZE(size)   (((size)<<16)&FPGA_TXx_MSK_PADDING_SIZE)

#define FPGA_TXx_BIT_OUTPUT_MODE(mode)       ((mode)&FPGA_TXx_MSK_OUTPUT_MODE)
#define FPGA_TXx_GET_BIT_OUTPUT_MODE(reg)   ((reg)&FPGA_TXx_MSK_OUTPUT_MODE)
#define TX_OUTPUT_MODE_DISABLED        0x0
#define TX_OUTPUT_MODE_VIDEO_ANC       0x1
#define TX_OUTPUT_MODE_COLOR_BAR       0x2
#define TX_OUTPUT_MODE_VIDEO           0x3
#define TX_OUTPUT_MODE_ANC             0x5
#define TX_OUTPUT_MODE_RAW             0x7

#define FPGA_TXx_BIT_PACKING(packing_mode)  ((packing_mode)<<9) & FPGA_TXx_MSK_PACKING
#define FPGA_TXx_GET_BIT_PACKING(reg)       (((reg)& FPGA_TXx_MSK_PACKING) >> 9)
#define TX_PACKING_V208     PACKING_V208   // 4:2:2:0  8-bit YUV  video packing 
#define TX_PACKING_AV208    PACKING_AV208  // 4:2:2:4  8-bit YUVA video packing 
#define TX_PACKING_V210     PACKING_V210   // 4:2:2:0 10-bit YUV  video packing 
#define TX_PACKING_AV210    PACKING_AV210  // 4:2:2:4 10-bit YUVA video packing 
#define TX_PACKING_V408     PACKING_V408   // 4:4:4:0  8-bit YUV  video packing 
#define TX_PACKING_AV408    PACKING_AV408  // 4:4:4:4  8-bit YUVA video packing 
#define TX_PACKING_V410     PACKING_V410   // 4:4:4:0 10-bit YUV  video packing 
#define TX_PACKING_AV410    PACKING_AV410  // 4:4:4:4 10-bit YUVA video packing 
#define TX_PACKING_C408     PACKING_C408   // 4:4:4:0  8-bit RGB  video packing 
#define TX_PACKING_AC408    PACKING_AC408  // 4:4:4:4  8-bit RGBA video packing 

#define FPGA_TXx_BIT_SAMPLE_RATE(tx_mode)        ((tx_mode)<<26) & FPGA_TXx_MSK_SAMPLE_RATE
#define FPGA_TXx_GET_BIT_SAMPLE_RATE(reg)        (((reg)& FPGA_TXx_MSK_SAMPLE_RATE) >> 26)
#define TX_MODE_HD                    0x00        // HD stream
#define TX_MODE_SD                    0x01        // SD stream
#define TX_MODE_3G                    0x02        // 3G stream
#define TX_MODE_INVALID               0x03        // Invalid

#define TX_SINGLE_LINK                           0x00000001 
#define TX_SINGLE_LINK_3GB_DL_MAP_I              0x00000002
#define TX_SINGLE_LINK_3GB_DL_MAP_II             0x00000003 
#define TX_DUAL_LINK_HD_MAP_I                    0x00000004 
#define TX_DUAL_LINK_HD_MAP_II                   0x00000005 
#define TX_DUAL_LINK_3GB_DS_4K_QUADRANT          0x00000006 
#define TX_DUAL_LINK_3GB_DS_4K_PIXEL             0x00000007 
#define TX_QUAD_LINK_HD_3GA_4K_QUADRANT          0x00000008 
#define TX_QUAD_LINK_HD_3GA_4K_PIXEL             0x00000009 
#define TX_QUAD_LINK_3GB_DL_4K_QUADRANT          0x0000000A 
#define TX_QUAD_LINK_3GB_DL_4K_PIXEL             0x0000000B 

#define FPGA_TXx_BIT_COUPLED_MODE(mode)      (((mode)<<7)&FPGA_TXx_MSK_COUPLED_MODE)
#define TX_COUPLED_MODE_2             0x1
#define TX_COUPLED_MODE_4             0x2

//FPGA_RXBUFID_MM32
#define FPGA_RXBUFID_MSK_RXi(i)              (0x7<<((i)*3))       
#define FPGA_RXBUFID_GET_BIT_RXi(reg,i)      (((reg)>>((i)*3))&0x7)


//FPGA_RXx_STS_MM32
#define FPGA_RXx_STS_BIT_PROGRESSIVE             0x00000400     //�0� = Interlaced Transport Stream - �1� = Progressive Transport Stream (**)
#define FPGA_RXx_STS_MSK_RATE                    0x00007800     //(**)
#define FPGA_RXx_STS_BIT_LEVELB                  0x00008000     //�1� when the input signal is level B (only valid in 3G) (*)
#define FPGA_RXx_STS_MSK_FAMILY                  0x000f0000     //(**)
#define FPGA_RXx_STS_BIT_CARRIER_DETECT_N        0x00100000     //�0� = Equalizer Carrier Detected - �1� = Equalizer Carrier NOT Detected
#define FPGA_RXx_STS_MSK_MODE                    0x00600000     //(*)
#define FPGA_RXx_STS_BIT_US                      0x00800000     //�0� = Eu - �1� = Us (*)
#define FPGA_RXx_STS_BIT_ALIGNED                 0x01000000     //�1� when RX(i) and RX(i+1) are aligned (with i = 0, 2, 4 or 6)
#define FPGA_RXx_STS_BIT_VPID_VALID_A            0x02000000     //�1� when SMPTE 352M packet found on stream A
#define FPGA_RXx_STS_BIT_CRC_ERR_A               0x04000000     //�1� when a CRC error occurs on stream A
#define FPGA_RXx_STS_BIT_MODE_LOCKED             0x08000000     //�1� when Mode Status are valid (*)
#define FPGA_RXx_STS_BIT_FORMAT_LOCKED           0x10000000     //�1� when Format Status are valid (**)

#define FPGA_RXx_STS_GET_BIT_RATE(reg)   (((reg)&FPGA_RXx_STS_MSK_RATE) >> 11)
#define RATE_23_98_HZ                        0x2
#define RATE_24_HZ                           0x3
#define RATE_47_95_HZ                        0x4
#define RATE_25_HZ                           0x5
#define RATE_29_97_HZ                        0x6
#define RATE_30_HZ                           0x7
#define RATE_48_HZ                           0x8
#define RATE_50_HZ                           0x9
#define RATE_59_94_HZ                        0xA
#define RATE_60_HZ                           0xB

#define FPGA_RXx_STS_GET_BIT_FAMILY(reg)   (((reg)&FPGA_RXx_STS_MSK_FAMILY) >> 16)
#define FAMILY_1920x1080_SMPTE274            0x0
#define FAMILY_1280x720_SMPTE296             0x1
#define FAMILY_2048x1080_SMPTE2048           0x2
#define FAMILY_1920x1080_SMPTE295            0x3
#define FAMILY_NTSC                          0x8
#define FAMILY_PAL                           0x9

#define FPGA_RXx_STS_GET_BIT_MODE(reg)   (((reg)&FPGA_RXx_STS_MSK_MODE) >> 21)
#define MODE_HD                              0x0
#define MODE_SD                              0x1
#define MODE_3G                              0x2

//FPGA_FABCx_x_MM32
#define FPGA_FABCx_x_MSK_ANC_SIZE            0x03FFFFFF
#define FPGA_FABCx_x_BIT_CRC_ERROR           0x04000000
#define FPGA_FABCx_x_MSK_CAPTURED_DATA       0x78000000
#define FPGA_FABCx_x_BIT_VIDEO               0x08000000
#define FPGA_FABCx_x_BIT_ANC                 0x10000000
#define FPGA_FABCx_x_BIT_RAW                 0x20000000
#define FPGA_FABCx_x_BIT_VIDEO_SND_PATH      0x40000000

#define FPGA_FABCx_x_GET_ANC_SIZE(reg)       (((reg)&FPGA_FABCx_x_MSK_ANC_SIZE))
#define FPGA_FABCx_x_GET_CAPTURED_DATA(reg)  (((reg)&FPGA_FABCx_x_MSK_CAPTURED_DATA)>>27)
#define FABC_CAPTURE_MODE_VIDEO              0x1
#define FABC_CAPTURE_MODE_ANC                0x2
#define FABC_CAPTURE_MODE_RAW                0x4
#define FABC_CAPTURE_MODE_VIDEO_SND_PATH     0x8

//FPGA_K_COMP_MM32
#define FPGA_K_COMP_MSK_SLOPE                0x0003FFFF
#define FPGA_K_COMP_BIT_ENABLE               0x00040000
#define FPGA_K_COMP_GET_BITS_SLOPE(coef)     ((coef) & FPGA_K_COMP_MSK_SLOPE)

//SW Board ID
#define SW_BRDID_MSK_SUB_PRODUCT_ID              0x000000ff
#define SW_BRDID_MSK_PRODUCT_ID                  0x0000ff00
#define SW_BRDID_MSK_NB_RX_CHN                   0x000f0000
#define SW_BRDID_MSK_NB_TX_CHN                   0x00f00000
#define SW_BRDID_MSK_PRODUCT_FAMILY              0xff000000

#define SW_BRDID_BIT_SUB_PRODUCT_ID(SubProductId)  ((SubProductId)&SW_BRDID_MSK_SUB_PRODUCT_ID)
#define SW_BRDID_GET_BIT_SUB_PRODUCT_ID(reg)       ((reg)&SW_BRDID_MSK_SUB_PRODUCT_ID)
#define SW_BRDID_BIT_PRODUCT_ID(ProductId)         (((ProductId)<<8)&SW_BRDID_MSK_PRODUCT_ID)
#define SW_BRDID_GET_BIT_PRODUCT_ID(reg)           (((reg)&SW_BRDID_MSK_PRODUCT_ID)>>8)
#define SW_BRDID_BIT_NB_RX_CHN(NbRxChn)            (((NbRxChn)<<16)&SW_BRDID_MSK_NB_RX_CHN)
#define SW_BRDID_GET_BIT_NB_RX_CHN(reg)            (((reg)&SW_BRDID_MSK_NB_RX_CHN)>>16)
#define SW_BRDID_BIT_NB_TX_CHN(NbTxChn)            (((NbTxChn)<<20)&SW_BRDID_MSK_NB_TX_CHN)
#define SW_BRDID_GET_BIT_NB_TX_CHN(reg)            (((reg)&SW_BRDID_MSK_NB_TX_CHN)>>20)
#define SW_BRDID_BIT_PRODUCT_FAMILY(ProductFamily) (((ProductFamily)<<24)&SW_BRDID_MSK_PRODUCT_FAMILY)
#define SW_BRDID_GET_BIT_PRODUCT_FAMILY(reg)       (((reg)&SW_BRDID_MSK_PRODUCT_FAMILY)>>24)

//HW Board ID
#define HW_BRDID_MSK_CHN_RX_ENABLE               0x000000ff
#define HW_BRDID_MSK_CHN_TX_ENABLE               0x0000ff00
#define HW_BRDID_BIT_ALT_DDR                     0x00010000
#define HW_BRDID_BIT_UC                          0x00020000
#define HW_BRDID_BIT_BASEBOARD_LMH               0x00040000
#define HW_BRDID_BIT_MEZZA_LMH                   0x00080000
#define HW_BRDID_BIT_OSC_148                     0x00100000
#define HW_BRDID_BIT_MEZZA_VCXO                  0x00200000
#define HW_BRDID_BIT_FLASH_SIZE                  0x00400000
#define HW_BRDID_BIT_BASEBOARD_BLACKBURST        0x00800000
#define HW_BRDID_BIT_MEZZA_BLACKBURST            0x01000000
#define HW_BRDID_MSK_VIDEO_STANDARD              0x06000000
#define HW_BRDID_BIT_MEZZA                       0x08000000
#define HW_BRDID_BIT_KEYER                       0x10000000


#define HW_BRDID_BIT_CHN_RX_ENABLE(ChnRxEnable)  ((ChnRxEnable)&HW_BRDID_MSK_CHN_RX_ENABLE)
#define HW_BRDID_BIT_CHN_TX_ENABLE(ChnTxEnable)  (((ChnTxEnable)<<8)&HW_BRDID_MSK_CHN_TX_ENABLE)
#define HW_BRDID_BIT_VIDEO_STANDARD(VidStd)      (((VidStd)<<25)&HW_BRDID_MSK_VIDEO_STANDARD)
#define VIDSTD_SD                0x0
#define VIDSTD_HD                0x1
#define VIDSTD_3G                0x3

#define AUT0RIZATION_DODE        0x4443
#define BRANDING_DC              0x0

//VBI config
#define VBICONF_MAX_NBOF_CAPTURE_LINE                    64
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_MSK_LINE      0x07FF
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_BIT_CAPT_C    0x0800
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_BIT_CAPT_Y    0x1000

#define FPGA_SR_BIT_TX_EMPTY     0x00100000 
#define FPGA_SR_BIT_RX_FULL      0x00200000

//I2C address
#define SERIAL_ID_ADDR           0xA0
#define SWITCH_I2C_ADDR          0xE0

//Register address
#define SERIAL_ID_VENDOR_PN_ADDR    40
#define SERIAL_ID_VENDOR_PN_SIZE    16
#define SERIAL_ID_CC_EXT_ADDR       95

#define CHALLENGE_DELAY             2000     //minimum time between 2 challenges (to avoid brut-force attack)

#endif // _HDDRV_REGISTERSMAP_H_
