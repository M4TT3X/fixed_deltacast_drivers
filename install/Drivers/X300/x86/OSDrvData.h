/********************************************************************************************************************//**
 * @internal
 * @file   	OSDrvData.h
 * @date   	2011/04/28
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2011/04/28  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _OSDRVDATA_H_
#define _OSDRVDATA_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvData.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define SPINLOCK_POOL_SIZE 32
#define EVENT_POOL_SIZE    64
#define MUTEX_POOL_SIZE    16

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

struct _OS_DRV_DATA
{
   struct cdev             MainCharDevice_X;
   atomic_t                RefCount;
   atomic_t                MainDev_RefCount;
   struct pci_dev          *pPCIDevice_X;
   spinlock_t              InterruptSpinlock_X;
   struct tasklet_struct	Tasklet_X;
   atomic_t                pAuxDev_RefCount[NB_AUX_CHAR_DEVICES];
   spinlock_t              pSpinlockPool[SPINLOCK_POOL_SIZE];
   ULONG                   pSpinlockFlags_UL[SPINLOCK_POOL_SIZE];
   struct semaphore        pSemaphorePool[MUTEX_POOL_SIZE];
   ULONG                   pMutexFlags_UL[MUTEX_POOL_SIZE];
   wait_queue_head_t       pEventPool[EVENT_POOL_SIZE];
   ULONG                   pEventFlags_UL[EVENT_POOL_SIZE];


} ;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _OSDRVDATA_H_
