#ifndef _CHDEV_MAIN_H_
#define _CHDEV_MAIN_H_


#define NBMINORS 10
int CreateCharDevices(DEVICE_EXTENSION *pdx, int major_number_i, int index_i);
void RemoveCharDevices(DEVICE_EXTENSION *pdx, int major_number_i, int index_i);
#endif //_CHDEV_MAIN_H_
