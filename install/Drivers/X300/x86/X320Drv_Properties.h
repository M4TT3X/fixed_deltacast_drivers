/********************************************************************************************************************//**
 * @internal
 * @file   	X320Drv_Properties.h
 * @date   	2013/04/05
 * @author 	ja
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2013/04/05  v00.01.0000    ja       Creation of this file

 **********************************************************************************************************************/

#ifndef _X320DRV_PROPERTIES_H_
#define _X320DRV_PROPERTIES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "StreamProperties.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define X320DRV_BOARD_FEATURES_GENLOCK0      0x00000001
#define X320DRV_BOARD_FEATURES_GENLOCK1      0x00000002
#define X320DRV_BOARD_FEATURES_KEYER         0x00000004
#define X320DRV_BOARD_FEATURES_2048          0x00000008
#define X320DRV_BOARD_FEATURES_LINE_PADDING  0x00000010
#define X320DRV_BOARD_FEATURES_FIELD_MODE    0x00000020
#define X320DRV_BOARD_FEATURES_UNUSED        0x00000040
#define X320DRV_BOARD_FEATURES_4K            0x00000100
#define X320DRV_BOARD_FEATURES_THUMBNAIL     0x00000200
#define X320DRV_BOARD_FEATURES_DL1080P       0x00000400
#define X320DRV_BOARD_FEATURES_BIDIR         0x00000800
#define X320DRV_BOARD_FEATURES_RELAY_0       0x00001000
#define X320DRV_BOARD_FEATURES_RELAY_1       0x00002000
#define X320DRV_BOARD_FEATURES_RELAY_2       0x00004000
#define X320DRV_BOARD_FEATURES_RELAY_3       0x00008000
#define X320DRV_BOARD_FEATURES_HDMI_MONITORING  0x00010000



#define X320DRV_RELAYx_DISABLE_VAL_BYUSER       0
#define X320DRV_RELAYx_DISABLE_VAL_FGPALOADED   1
#define X320DRV_RELAYx_DISABLE_VAL_BOARDOPEN    2
#define X320DRV_RELAYx_ENABLE_VAL_BYUSER        0
#define X320DRV_RELAYx_ENABLE_VAL_DRIVERUNLOAD  1
#define X320DRV_RELAYx_ENABLE_VAL_BOARDCLOSED   2

#define X320DRV_RX_CHN_STATE_BIT_FIELD_MODE      0x00000002

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
   X320DRV_EXTBOARD_TYPE_NONE,
   X320DRV_EXTBOARD_TYPE_40,
   X320DRV_EXTBOARD_TYPE_3G,

} X320DRV_EXTBOARD_TYPE;


typedef enum
{
   X320DRV_PROPERTIES_FIRST=NB_STREAM_PROPERTIES,
   X320DRV_PROPERTIES_PRODUCT_FAMILY=X320DRV_PROPERTIES_FIRST,
   X320DRV_PROPERTIES_BOARD_FIRMWARE_VERSION,
   X320DRV_PROPERTIES_BOARD_ARM_VERSION,
   X320DRV_PROPERTIES_BOARD_SSN_MSB,
   X320DRV_PROPERTIES_BOARD_SSN_LSB,


   X320DRV_PROPERTIES_EXTBOARD_TYPE,
   X320DRV_PROPERTIES_BOARD_FIRMWAREID,
   X320DRV_PROPERTIES_RELAY0_DISABLE,
   X320DRV_PROPERTIES_RELAY0_ENABLE,
   X320DRV_PROPERTIES_RELAY1_DISABLE,
   X320DRV_PROPERTIES_RELAY1_ENABLE,

   X320DRV_PROPERTIES_BOARD_FEATURES,
   X320DRV_PROPERTIES_BOARD_FPGA_FW_ID,
   
   X320DRV_PROPERTIES_BOARD_NB_UPGRADABLE_STUFF,
   X320DRV_PROPERTIES_BOARD_RQST_ARM_VERSION,
   X320DRV_PROPERTIES_BOARD_RQST_FIRMWARE_VERSION,
   X320DRV_PROPERTIES_BOARD_ARM_FW_ID,

   NB_X320DRV_PROPERTIES
} X320DRV_PROPERTIES;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _X320DRV_PROPERTIES_H_
