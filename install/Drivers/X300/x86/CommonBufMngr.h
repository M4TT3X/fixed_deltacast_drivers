/********************************************************************************************************************//**
   * @file   CommonBufMngr.h
   * @date   2008/01/07
   * @Author cs
   * @brief  This file describes all functions related to Common Buffer Manager.
   *
   * The Common Buffer Manager handles creation, access and release of buffers for DMA operations. These buffers are 
   * unpageable and physically contigious.
   * 
   **********************************************************************************************************************//*

      ====================================================================================================================
      =  Version History                                                                                                 =
      ====================================================================================================================
      Date     	Version     	Author   Description
      ====================================================================================================================
      2006/11/21  v00.01.0000    cs       Creation of this file
      2006/11/30  v00.02.0000    cs       - Adding feature to allow system to lock buffer to avoid freeing during DMA 
                                          operation. Now the used buffer is not free anymore, but just marked. When the 
                                          system will unlock it, it will be freed automatically. 
                                          - Add check on the buffer ID in the CommonBufMngr_Free() function.
      2008/01/07  v00.03.0000    cs       Documentation update.
      2010/02/10  v00.04.0000    jm       new preallocation management:

                                          COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS pools of memory can be preallocated during 
                                          driver loading if some key register values are set. Those pools, when used, are the
                                          COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS first common buffers in the common buffer management.
                                          Then, the buffers allocated inside those preallocated pools are indexed this way:
                                          0 to 3 : memory pools
                                          4 to 4+ -1 : buffers in pool 0
                                          4+ to 4+(2 * MAX_NBOFBUFINPOOL) -1 : buffers in pool 1
                                          4+(2 * MAX_NBOFBUFINPOOL) to 4+(3 * MAX_NBOFBUFINPOOL)-1 : buffers in pool 2
                                          4+(3 * MAX_NBOFBUFINPOOL) to 4+(4 * MAX_NBOFBUFINPOOL)-1 : buffers in pool 3

                                          When preallocation is used, the buffers follow each other.
                                          Their size must be a multiple of 4k bytes.

                                          new functions :
                                          CommonBufMngr_AllocateBufPool(...)
                                          CommonBufMngr_FreeBufPool(...)
                                          CommonBufMngr_PreallocateBuffers_GetInfo()

      2010/04/28  v00.01.0002    jm       preallocation pool management modification
                                          - ability to access the pool when ANC disjoined mode
                                          at the same time than the normal access.
                                          - the number MAX_NBOFBUFINPOOL is now 2*32 (it was 3*8 before) 
      2011/02/14  v00.01.0003    cs       Add a check in the CommmonBufMngr_Check() function to avoid wrong "Still allocatected"
                                          error on "Keep Alive" buffer
      2011/03/31  v00.01.0004    cs       Bug fix : The preallocation implementation didn't care of 'Locked by system' status.
      2011/08/07  v00.02.0000    cs       Windows/Linux merging ... All common parts aren't duplicated anymore.
      2012/01/02  v00.03.0000    cs       Change the way the buffers are freed when they are involved in a DMA. Before the update, 
                                          the free was done during the "unlock by system", so during the DMA done DPC handler.
                                          In some cases (linux and MEM= method) the freeing process can't be done at high IRQL.
                                          In the current implementation, the userpace free buffer now waits for unlocked buffer
                                          before freeing them at low IRQL.

**********************************************************************************************************************/

#ifndef _COMMONBUFMNGR_H_
#define _COMMONBUFMNGR_H_
// 
// 
// #ifndef WINDOWS_WDM_DRIVER
// #error "This file must be used in Windows WDM environment"
// #endif

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvTypes.h"
#include "KernelObjects.h"
#include "CommonBufMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define COMMON_BUFFER_MAX_SLAVE     DMABUF_MAX_NB_SLAVES
#define COMMON_BUFFER_MAX_NB        MAX_DMABUF


#define COMMON_BUFFER_FLAGS_ALLOCATED         DMABUF_FLAGS_USED // This flags marks the buffer as allocated
#define COMMON_BUFFER_FLAGS_KEEP_ALIVE        0x20000000 // The flag is the same than COMMON_BUFFER_FLAGS_PREALLOCATED, because they are handle by the same way

#define COMMON_BUFFER_FLAGS_POOLSIDE_MSK         0x08000000    
#define COMMON_BUFFER_FLAGS_POOLID_MSK           0x03000000
#define COMMON_BUFFER_FLAGS_PREALLOCATED         0x40000000 // This flags marks the buffer as pre-allocated
#define COMMON_BUFFER_FLAGS_PREALLOCATED_USED    0x10000000 // This flags marks the pre-allocated buffer as used

#define COMMON_BUFFER_FLAGS_POOLSIDE(PoolSide)     ((PoolSide<<27) & COMMON_BUFFER_FLAGS_POOLSIDE_MSK)
#define COMMON_BUFFER_FLAGS_GET_POOLSIDE(CB_flags) ((CB_flags&COMMON_BUFFER_FLAGS_POOLSIDE_MSK)>>27)
#define COMMON_BUFFER_FLAGS_POOLID(PoolId)         ((PoolId<<24) & COMMON_BUFFER_FLAGS_POOLID_MSK)
#define COMMON_BUFFER_FLAGS_GET_POOLID(CB_flags)   ((CB_flags&COMMON_BUFFER_FLAGS_POOLID_MSK)>>24)

#define COMMON_PREALLOC_POOLSIDE0_USED_FLAGS            0x00000001 // The side 0 of the pre-alloctaed pool is currently used
#define COMMON_PREALLOC_POOLSIDE1_USED_FLAGS            0x00000002 // The side 1 of the pre-alloctaed pool is currently used
#define COMMON_PREALLOC_POOLSIDE_FLAGS_MARKED_TO_FREE   0x80000000



/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct 
{
   KOBJ_MEMPOOL            MemPoolForDescr_KOBJ;
   COMMON_BUFFER_DESCR     *pCommonBufferDescr_X;//[COMMON_BUFFER_MAX_NB]; /// Keeps a list of allocated buffer descriptors
   ULONG                   pBufferFlags_UL[COMMON_BUFFER_MAX_NB];          /// Keeps a set of flags associated to the buffers (Locked by system, marked to free, ...)
   KOBJ_SPINLOCK           SpinLock_KO;                                    /// Spin lock to give exclusive access to buffer allocation module
#ifndef __linux__
   /* Linux implementation doesn't support preallocated buffer through buffers pool */
   ULONG                   ppPreallocPoolCbId_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS];
   ULONG                   ppPreallocPoolSideSize_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS][COMMON_BUFFER_NBOF_POOLSIDE];
   PID_TYPE                ppPreallocPoolSidePid_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS][COMMON_BUFFER_NBOF_POOLSIDE];
   ULONG                   ppPreallocPoolSideFlags_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS][COMMON_BUFFER_NBOF_POOLSIDE];
#endif
} COMMON_BUF_MNGR_DATA;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void           CommonBufMngr_DeviceAdd( PDEVICE_EXTENSION pdx,ULONG pPreallocPoolSize_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS]);
void           CommonBufMngr_ReleasePermanentResources(PDEVICE_EXTENSION pdx);
ULONG          CommonBufMngr_AllocateBuf(PDEVICE_EXTENSION pdx,ULONG Size_UL,PID_TYPE PID_UL);
void           CommonBufMngr_FreeBuf(PDEVICE_EXTENSION pdx, ULONG BufID_UL,PID_TYPE PID_UL);
BOOL32         CommonBufMngr_GetBuf(PDEVICE_EXTENSION pdx, COMMON_BUFFER_DESCR *pDescr_X);
BOOL32         CommonBufMngr_ReleaseBuf(PDEVICE_EXTENSION pdx, COMMON_BUFFER_DESCR *pDescr_X);
IOCTL_STATUS   CommonBufMngr_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferSize_UL, PID_TYPE PID_UL);
void           CommonBufMngr_CleanUp(PDEVICE_EXTENSION pdx,PID_TYPE PID_UL, BOOL32 DevicePresent_B);
BOOL32         CommonBufMngr_LockBySystem(PDEVICE_EXTENSION pdx, ULONG BufID_UL, BOOL32 LockSlaves_B);
BOOL32         CommonBufMngr_UnlockBySystem(PDEVICE_EXTENSION pdx, ULONG BufID_UL, BOOL32 UnlockSlaves_B);
void           CommonBufMngr_Check(PDEVICE_EXTENSION pdx);
BOOL32         CommonBufMngr_LinkBuffer(PDEVICE_EXTENSION pdx, ULONG BufID_UL, ULONG MasterBufID_UL);
BOOL32         CommonBufMngr_IsAllocated(PDEVICE_EXTENSION pdx, ULONG BufID_UL);
BOOL32         CommonBufMngr_KeepAlive(PDEVICE_EXTENSION pdx, ULONG BufID_UL);
BUFID          CommonBufMngr_GetSlaveBufID(PDEVICE_EXTENSION pdx, BUFID MasterBufID, ULONG SlaveIdx_UL);
#ifdef __linux__
int            CommonBufMngr_MMap(PDEVICE_EXTENSION pdx, struct vm_area_struct *vma, ULONG Cmd_UL);
#endif

/************************* Preallocated buffer stuff ********************************/
#ifndef __linux__
void           CommonBufMngr_PreallocateBuffers_DeviceAdd( PDEVICE_EXTENSION pdx,ULONG pPreallocPoolSize_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS]);
void           CommonBufMngr_PreallocateBuffers_ReleasePermanentResources( PDEVICE_EXTENSION pdx );
void           CommonBufMngr_PreallocateBuffers_CleanUp( PDEVICE_EXTENSION pdx, PID_TYPE PID_UL, BOOL32 DevicePresent_B);
void           CommonBufMngr_PreallocateBuffers_GetInfo(PDEVICE_EXTENSION pdx , IOCTL_GETPREALLOCINFO_COMMON_BUFFER_PARAM *pInfo_X);
#ifndef __APPLE__
void           CommonBufMngr_DisablePrealloc(PDEVICE_EXTENSION pdx);
#endif
void           CommonBufMngr_FreePreallocatedBuf(PDEVICE_EXTENSION pdx, BUFID BufID_UL , PID_TYPE PID_UL);
BOOL32         CommonBufMngr_ReserveBufPoolSide( PDEVICE_EXTENSION pdx, ULONG PoolId_UL, ULONG PoolSide_UL, ULONG pBufferSize_UL[COMMON_BUFFER_MAX_SLAVE+1], ULONG NbOfBuffers_UL, PID_TYPE PID_UL );
void           CommonBufMngr_FreeBufPoolSide(PDEVICE_EXTENSION pdx, ULONG PoolId_UL, ULONG PoolSide_UL, PID_TYPE PID_UL, BOOL32 Safe_B);
ULONG          CommonBufMngr_ReservePreallocatedBuffer( PDEVICE_EXTENSION pdx, ULONG PoolId_UL, ULONG PoolSide_UL, ULONG Size_UL, PID_TYPE PID_UL );
#endif

/************************* OS abstraction stuff ************************************/
BOOL32         CommonBufMngr_OSAllocateBuf(PDEVICE_EXTENSION pdx, COMMON_BUFFER_DESCR *pCommonBufferDescr_X);
void           CommonBufMngr_OSFreeBuf(PDEVICE_EXTENSION pdx, COMMON_BUFFER_DESCR *pCommonBufferDescr_X);

#endif // _COMMONBUFMNGR_H_
