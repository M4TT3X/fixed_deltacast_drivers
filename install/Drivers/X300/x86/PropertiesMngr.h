/********************************************************************************************************************//**
 * @file   PropertiesMngr.h
 * @date   2007/12/10
 * @Author cs
 * @brief  
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2006/12/10  v00.01.0000    cs       Creation of this file
   2007/12/10  v00.02.0000    cs       Add Test'n'Set and Test'n'Clear features.
   2011/01/26  v00.02.0001    cs       Add PropertiesMngr_UnsafeGetValue(propid) macro

 **********************************************************************************************************************/

#ifndef _PROPERTIESMNGR_H_
#define _PROPERTIESMNGR_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "SkelDrv.h"
#include "KernelObjects.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define MAX_PROPERTIES 256	///< Defines the maximum number of properties handled by the Property Manager.

/***** MACROS DEFINITIONS *********************************************************************************************/

#define GetPropMngrData() (&pdx->PropertiesMngrData_X)

#ifdef OLD_WAY
#define PROPMNGR_INIT_SAFEAREA()  KOBJ_InitializeSpinLock(pdx,&GetPropMngrData()->SpinLock_KO)
#define PROPMNGR_ENTER_SAFEAREA() KOBJ_AcquireSpinLock(pdx,&GetPropMngrData()->SpinLock_KO)
#define PROPMNGR_LEAVE_SAFEAREA() KOBJ_ReleaseSpinLock(pdx,&GetPropMngrData()->SpinLock_KO)
#endif


/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct
{
   ULONG pProperties_UL[MAX_PROPERTIES];
   ULONG pPropertiesDefaultValue_UL[MAX_PROPERTIES];
   ULONG NbProperties_UL;
#ifdef OLD_WAY
   KOBJ_SPINLOCK SpinLock_KO;
#endif
} PROPERTIES_MNGR_DATA;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void PropertiesMngr_DeviceAdd (PDEVICE_EXTENSION pdx);
BOOL32 PropertiesMngr_SetValue (PDEVICE_EXTENSION pdx, ULONG PropIdx_UL,ULONG Value_UB);
BOOL32 PropertiesMngr_GetValue (PDEVICE_EXTENSION pdx, ULONG PropIdx_UL,ULONG *pValue_UB);
BOOL32 PropertiesMngr_SetBit (PDEVICE_EXTENSION pdx, ULONG PropIdx_UL,ULONG Value_UB);
BOOL32 PropertiesMngr_ClrBit (PDEVICE_EXTENSION pdx, ULONG PropIdx_UL,ULONG Value_UB);
BOOL32 PropertiesMngr_Reset(PDEVICE_EXTENSION pdx, ULONG PropIdx_UL, ULONG *pVal_UL);
IOCTL_STATUS PropertiesMngr_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,PID_TYPE PID_UL);
BOOL32 PropertiesMngr_TestAndSetBit(PDEVICE_EXTENSION pdx, ULONG PropIdx_UL,ULONG *pValue_UB);
BOOL32 PropertiesMngr_TestAndClearBit(PDEVICE_EXTENSION pdx, ULONG PropIdx_UL,ULONG *pValue_UB);
BOOL32 PropertiesMngr_TestBits(PDEVICE_EXTENSION pdx, ULONG PropIdx_UL,ULONG Val_UL);
BOOL32 PropertiesMngr_AddValue (PDEVICE_EXTENSION pdx, ULONG PropIdx_UL,int Value_i);

#endif // _PROPERTIESMNGR_H_
