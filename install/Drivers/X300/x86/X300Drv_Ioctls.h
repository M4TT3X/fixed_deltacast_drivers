
#ifndef _X300DRVIOCTLS_H_
#define _X300DRVIOCTLS_H_

#include "X3XXDrv_Ioctls.h"

typedef enum
{
   IOCTL_SFP_ACCESS_TYPE_GET_SERIALID,
   IOCTL_SFP_ACCESS_TYPE_SET_MON_TBL,
   IOCTL_SFP_ACCESS_TYPE_GET_MON_TBL,
   IOCTL_SFP_ACCESS_TYPE_GET_MON_VAL,
   IOCTL_SFP_ACCESS_TYPE_AUTHORIZE
} IOCTL_SFP_ACCESS_TYPE;

typedef struct  
{
   ULONG                   StructSize_UL;
   IOCTL_SFP_ACCESS_TYPE   Type_E;
   UBYTE                   ModIdx_UB;
   union
   {
      /* IOCTL_SFP_ACCESS_TYPE_GET_SERIALID */
      struct{
         SFP_STATUS     Sts_E;
         DELTA_VOID     pBuffer_v;
      }GetId;

      /* IOCTL_SFP_ACCESS_TYPE_*ET_MON_TBL */
      struct{
         DELTA_VOID  pBuffer_v;
         ULONG       Size_UL;
         ULONG       MinRefreshTime_UL;
      }RegTbl;

      /*  IOCTL_SFP_ACCESS_TYPE_GET_MON_VAL */
      union{
         UBYTE TableEntry_UB;
         struct{
            UBYTE    RegVal_UB;
            SFP_STATUS   Sts_E;
         };
      }GetVal;

      /*  IOCTL_SFP_ACCESS_TYPE_AUTHORIZE */
      union{
         DELTA_VOID  pKey_v;
         SFP_STATUS  Sts_E;
      }AuthKey;
   };
} IOCTL_SFP_ACCESS_PARAM;

#define X300DRV_IOC(code) (X3XXDRV_IOC_USER(code))

#define IOCTL_SFP_ACCESS    DEFINE_IOCTL_RDWR(X300DRV_IOC(0x00), IOCTL_SFP_ACCESS_PARAM)
#define IOCTL_LTC_ACCESS    DEFINE_IOCTL_RDWR(X300DRV_IOC(0x01), IOCTL_LTC_ACCESS_PARAM)

#endif //_X300DRVIOCTLS_H_
