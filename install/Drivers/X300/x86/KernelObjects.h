/********************************************************************************************************************//**
 * @file   	KernelObjects.h
 * @date   	2011/02/17
 * @author 	cs
 * @version v00.04.0000 
 * @brief   This the Windows version of the implementation of the Portable Kernel Objects.
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2006/10/19  v00.01.0000    cs       Creation of this file
   2009/04/24  v00.02.0000    gt       Add KOBJ_WaitOnSyncEvent, KOBJ_WaitOnSyncEvent_Timeout and LocalKEvent_X in KOBJ_SYNC_EVENT
   2011/02/22  v00.03.0000    cs       Add KThread Kernel Object
   2011/02/24  v00.03.0001    cs       Fix a bug in KOBJ_WaitOnSyncEvent_Timeout(). Now, it can handle the INFINITE timeout
                                       properly.
   2011/02/25  v00.03.0002    cs       Fix the returned value of the SetEvent and ResetEvent function.
   2011/04/21  v00.04.0000    cs       Add KOBJ_MEMPOOL kernel object.
   2013/03/29  v00.05.0000    bc       Add KOBJ_Init, KOBJ_SetSyncEventByDPC, KOBJ_ProcessSetSyncEventByDPC, KOBJ_ResetSyncEventByDPC
                                       and KOBJ_ProcessResetSyncEventByDPC functions

 **********************************************************************************************************************/

#ifndef _KERNELOBJECTS_H_
#define _KERNELOBJECTS_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvTypes.h"
#include "CTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** KOBJ_SPINLOCK **************************************************************************************************

   This object allows an inter-CPU IRQ Safe synchronization way.   

 **********************************************************************************************************************/

typedef struct
{
#ifdef __linux__
#ifdef CONFIG_PREEMPT_RT
   raw_spinlock_t *   pSpinlock_X;
#else
	spinlock_t *   pSpinlock_X;
#endif
   unsigned long  Flags_UL;
#elif defined(__APPLE__)
    int Index_i;
#else
   KSPIN_LOCK SpinLock_X;
   KIRQL      OldIrql_X;
#endif
} KOBJ_SPINLOCK;

void KOBJ_InitializeSpinLock(PDEVICE_EXTENSION pdx, KOBJ_SPINLOCK * pSpinLock_KO);
void KOBJ_AcquireSpinLock(PDEVICE_EXTENSION pdx, KOBJ_SPINLOCK * pSpinLock_KO);
void KOBJ_ReleaseSpinLock(PDEVICE_EXTENSION pdx, KOBJ_SPINLOCK * pSpinLock_KO);

#if defined(__linux__) || defined(__APPLE__)
#define KOBJ_AcquireSpinLockAtDPCLevel KOBJ_AcquireSpinLock
#define KOBJ_ReleaseSpinLockAtDPCLevel KOBJ_ReleaseSpinLock
#else
void KOBJ_AcquireSpinLockAtDPCLevel(PDEVICE_EXTENSION pdx, KOBJ_SPINLOCK * pSpinLock_KO);
void KOBJ_ReleaseSpinLockAtDPCLevel(PDEVICE_EXTENSION pdx, KOBJ_SPINLOCK * pSpinLock_KO);
#endif

/***** KOBJ_FAST_MUTEX ************************************************************************************************
   
   This object gives the MUTEX feature but instead of classic mutex, these objects may be not reentrant.

 **********************************************************************************************************************/

typedef struct 
{
#ifdef __linux__
   struct semaphore *pSemaphore_X;
#elif defined(__APPLE__)
   int Index_i;
#else
   FAST_MUTEX FastMutex_X;
#endif
} KOBJ_FAST_MUTEX;

void KOBJ_InitializeFastMutex(PDEVICE_EXTENSION pdx, KOBJ_FAST_MUTEX * pFastMutex_KO);
void KOBJ_AcquireFastMutex(PDEVICE_EXTENSION pdx, KOBJ_FAST_MUTEX * pFastMutex_KO);
void KOBJ_ReleaseFastMutex(PDEVICE_EXTENSION pdx, KOBJ_FAST_MUTEX * pFastMutex_KO);

/***** KOBJ_SYNC_EVENT ************************************************************************************************

 **********************************************************************************************************************/

#define KOBJ_EVENT_TABLE_SIZE 64

typedef struct
{
#ifdef __linux__
   wait_queue_head_t *pEvent_X;
   int Flags_i; 
#elif defined(__APPLE__)
   int Index_i;
#else
   KEVENT  LocalKEvent_X;
   PKEVENT KEvent_X;
#endif   
} KOBJ_SYNC_EVENT;

typedef struct
{
   KOBJ_SYNC_EVENT *ppSetSyncEventTable_X[KOBJ_EVENT_TABLE_SIZE];      // Table for setting sync events
   KOBJ_SYNC_EVENT *ppResetSyncEventTable_X[KOBJ_EVENT_TABLE_SIZE];    // Table for resetting sync events
   KOBJ_SPINLOCK SyncEventSpinLock_X;                                // Spin lock to protect events access
} KOBJ_SYNC_EVENT_DATA;

void KOBJ_InitializeSyncEvent(PDEVICE_EXTENSION pdx, KOBJ_SYNC_EVENT * pSyncEvent_KO, BOOL32 State_B);
BOOL32 KOBJ_SetSyncEvent(PDEVICE_EXTENSION pdx, KOBJ_SYNC_EVENT *pSyncEvent_KO);
BOOL32 KOBJ_ResetSyncEvent(PDEVICE_EXTENSION pdx, KOBJ_SYNC_EVENT *pSyncEvent_KO);
void KOBJ_WaitOnSyncEvent(PDEVICE_EXTENSION pdx, KOBJ_SYNC_EVENT *pSyncEvent_KO);
BOOL32 KOBJ_WaitOnSyncEvent_Timeout(PDEVICE_EXTENSION pdx, KOBJ_SYNC_EVENT *pSyncEvent_KO,ULONG Timeout_UL);
BOOL32 KOBJ_SetSyncEventByDPC(PDEVICE_EXTENSION pdx, KOBJ_SYNC_EVENT *pSyncEvent_KO);
BOOL32 KOBJ_ProcessSetSyncEventByDPC(PDEVICE_EXTENSION pdx);
BOOL32 KOBJ_ResetSyncEventByDPC(PDEVICE_EXTENSION pdx, KOBJ_SYNC_EVENT *pSyncEvent_KO);
BOOL32 KOBJ_ProcessResetSyncEventByDPC(PDEVICE_EXTENSION pdx);

/***** KOBJ_Initialize ************************************************************************************************

 **********************************************************************************************************************/

void KOBJ_DeviceAdd(PDEVICE_EXTENSION pdx);

/***** KOBJ_KTHREAD ***************************************************************************************************


 **********************************************************************************************************************/

/** @page KERNEL_OBJECT_KTHREAD_PAGE  Kernel Threads
 
 @section KERNEL_OBJECT_KTHREAD_OVERVIEW Overview

 A Kernel Thread (or System thread) is a thread created at driver level, which is running outside of a userspace context. The system thread is mainly
 used to provide a way to handle devices that needs polling operations.

 @section KERNEL_OBJECT_KTHREAD_IMPLEMENTATION Implementation

 The implementation provides a set of functions that allows creating a thread and interact with it. Basically, a thread is defined by a procedure and an optionnal parameter.
 The prototype of the thread procedure is <i> int ThreadProc(KOBJ_KTHREAD * pKThreadOBJ_X) </i>.

 The thread <i>handle</i> (KOBJ_KTHREAD) embeds a syncronization object allowing sending command to thread and stopping it.

 @subsection KERNEL_OBJECT_KTHREAD_IMPLEMENTATION_CREATION Thread Creation

 The creation of the thread is done by calling the KOBJ_CreateKThread() function. This call will create the thread and start it.

 @code

 // in the device extension definition


 typedef struct
 {
   ULONG Parameters_UL;
   
 } THREAD_PARAM;

 typdef struct
 {

   //...


   KOBJ_KTHREAD KThread_OBJ;
   THREAD_PARAM ThreadParam_X;

   //...

 } DEVICE_EXTENSION;


 int MyThreadProc(KOBJ_KTHREAD * pKThreadOBJ_X);

 //... somewhere, by example, in the start device 

   BOOL32 Sts_B = KOBJ_CreateKThread(&pdx->Thread_KOBJ, MyThreadProc, &pdx->ThreadParam_X);

   if (Sts_B) 
   {
      // The thread is created and started.
   }



 //... somewhere, by example in stop device

 KOBJ_StopKThread(&pdx->Thread_KOBJ);

 @endcode

 Note that the kernel object as well as the thread parameter must stay in a permanent area (the device extension is the best choise).

 @subsection KERNEL_OBJECT_KTHREAD_IMPLEMENTATION_THREAD_PROC Thread Procedure


 The kernel thread procedure must use provided function to ensure a right behaviour. The first function is a way to retrieve thread parameter. 
 The function KOBJKT_GetKThreadParam() returns the thread parameter out of the kernel object.
 The second function is the KOBJKT_WaitForKThreadSyncCmd(). This function puts the thread in sleep mode until either the timeout occurs or the synchronization object is set.
 In this last case, the procedure retrieves the synchronisation command through the ULONG *pSyncCmd_UL parameter. The procedure must, at least handle one command : the stop command
 (0xFFFFFFFF). Once the command received, the procedure must calls the KOBJKT_ExitKThread() function,and exits.

 Here is a sample of a thread procedure that puts itself in a sleep with a timeout of 2000ms.

 @code

 int MyThreadProc(KOBJ_KTHREAD * pKThreadOBJ_X)
 {
   THREAD_PARAM * pThreadParam_X = KOBJKT_GetKThreadParam(pKThreadOBJ_X);
   BOOL32 Finished_B = FALSE;
   ULONG Cmd_UL;

   do
   {
     if (KOBJKT_WaitForKThreadSyncCmd(pKThreadOBJ_X,&Cmd_UL,2000)) 
     {
         if (Cmd_UL==KTHREAD_SYNCMD_STOP)
         {
            Finished_B = TRUE; // Stop command received
         }
         else
         {
            // User wake-up! 
         }
     }
     else
     {
         // Timeout !
     }


   } while (!Finished_B);

   KOBJKT_ExitKThread(pKThreadOBJ_X);
   return 0;
 }

 @endcode

 @subsection KERNEL_OBJECT_KTHREAD_IMPLEMENTATION_THREAD_SYNC Thread Synchronisation

 The kernel object embeds a sync object allowing interacting with it. The mandatory command is the STOP command (0xFFFFFFFF). The synchronisation command acts as bit map value.
 (the 0x80000000 bit is reserved to ensure that the stop command is unique).  Outside the thread, it is possible to send command by calling the KOBJ_SendKThreadSyncCmd(). This call
 will set the specified bit the in the embedded command value and set the sync object. Once woken-up, the thread will retrieve the set bit and clear the value.
 Note that the 'set value operation' is thread-safe.

**/ 


#define KTHREAD_SYNCMD_STOP 0xFFFFFFFF

struct _KOBJ_KTHREAD;

struct _KOBJ_KTHREAD;// KOBJ_KTHREAD;

typedef int (*KTHREADPROC) (struct _KOBJ_KTHREAD * pKThreadOBJ_X);

typedef enum
{
   KTHREAD_STATE_NOT_INIT=0,
   KTHREAD_STATE_STARTED=0,
   KTHREAD_STATE_STOPPED=0,

} KTHREAD_STATE;

typedef struct _KOBJ_KTHREAD
{   
   KTHREAD_STATE State_e;
   KOBJ_SYNC_EVENT SyncObject_KOBJ;
   void * pContext_v;
   volatile LONG SyncCmd_UL;
   KTHREADPROC ThreadProc_fct;

#ifdef __linux__
   struct task_struct *ts;
#elif defined(__APPLE__)
    thread_t Thread_H;
    bool ShouldStop_B; 
#else
   PKTHREAD pThread_X;
#endif
} KOBJ_KTHREAD;


BOOL32 KOBJ_CreateKThread(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ, KTHREADPROC fct, void * pThreadParam_v);
void KOBJ_StopKThread(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ);
BOOL32 KOBJ_SendKThreadSyncCmd(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ, ULONG SyncCmd_UL);

/** @name These functions must be called from the KThread procdure only *///@{
void * KOBJKT_GetKThreadParam(KOBJ_KTHREAD *pKThread_KOBJ);
BOOL32   KOBJKT_WaitForKThreadSyncCmd(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ, ULONG *pSyncCmd_UL, ULONG Timeout_UL);
void   KOBJKT_ExitKThread(PDEVICE_EXTENSION pdx, KOBJ_KTHREAD *pKThread_KOBJ);
//@}


/************************************************************************/
/* KOBJ_MEMPOOL                                                         */
/************************************************************************/

/**@page KOBJ_MEMPOOL_PAGE Kernel Memory Pool

   The KOBJ_MEMPOOL provides a way a allocate a non-page memory pool at kernel level.

*/

typedef struct  
{
   void * pPtr_v;
   ULONG Size_UL;
   ULONG Tag_UL;
} KOBJ_MEMPOOL;

BOOL32 KOBJ_CreateMemPool(PDEVICE_EXTENSION pdx, KOBJ_MEMPOOL * pMemPool_KOBJ, ULONG Size_UL, ULONG Tag_UL);
void KOBJ_DestroyMemPool(PDEVICE_EXTENSION pdx, KOBJ_MEMPOOL * pMemPool_KOBJ);
void * KOBJ_MemPoolGetDataPtr(PDEVICE_EXTENSION pdx, KOBJ_MEMPOOL * pMemPool_KOBJ);



#endif // _KERNELOBJECTS_H_

