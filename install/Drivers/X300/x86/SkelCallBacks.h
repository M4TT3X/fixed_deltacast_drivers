/********************************************************************************************************************//**
 * @file   	SkelCallBacks.h
 * @date   	2010/06/10
 * @author 	cs/gt
 * @version v00.01.1000 
 * @brief   This file defines the main customization callbacks table of the skeleton driver.
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version        Author   Description
   ====================================================================================================================
   2010/06/10   v00.01.0000   cs/gt    Creation of this file
   2011/02/23   v00.01.0001   cs       Add a new Clean-up callback (for an efficient per-process or global clean-up)       
   2013/06/21   v00.01.1000   jj       Added power management callbacks
                                       Renamed xxx_Init to xxx_DeviceAdd
                                       Renamed xxx_Shutdown to xxx_DeviceRemove
                                       Removed the previous AddDevice which was unused in HD, DVI, SD and X300 drivers

 **********************************************************************************************************************/

#ifndef _SKELCALLBACKS_H_
#define _SKELCALLBACKS_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvTypes.h"
#include "CommonBufMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

	/*********************************************************************************************************************//**
	 * @enum  BOARD_INIT_STATUS
	 * @brief This enumeration gives the available value for the board initialization state.
	 *************************************************************************************************************************/
	typedef enum
	{
		BOARD_INIT_KO,				///< The board isn't correctly initialized                  
		BOARD_INIT_OK,				///< The board is successfully initialized
		BOARD_INIT_NEED_SHUTDOWN,	///< The board initialization requires a shutdown      
		BOARD_INIT_NEED_RESTART     ///< The board initialization requires a restart   
	} BOARD_INIT_STATUS;

	/*********************************************************************************************************************//**
	 * @enum  BOARD_POWER_STATE
	 * @brief This enumeration defines the available power state for a device.
	 *************************************************************************************************************************/
	typedef enum
	{
		BOARD_POWER_STATE_IDLE,					///< Normal working mode
		BOARD_POWER_STATE_SLEEP,				///< First level of sleep mode
		BOARD_POWER_STATE_SUSPEND_TO_RAM,		///< System is in a sleep mode but powering is still ensured
		BOARD_POWER_STATE_SUSPEND_TO_DISK,		///< System is in a sleep mode but the powering is no more ensured
		BOARD_POWER_STATE_SUSPEND_TO_SHUTDOWN	///< The system will shutdown
	} BOARD_POWER_STATE;

  
	/**********************************************************************************************************************//**
	 * @struct SKEL_CALLBACKS
	 * @brief  This structure is a callbacks table allowing to insert specific operation at different driver location.
	 *
	 * This structure is filled by the final target driver during the SkelCallBack_CustomizeDrv() function call.
	 *
	 * @remark For more information about the skeleton driver customization, please refer to @ref SKELDRV_CUSTOMIZATION "this chapter",
	 * of the the @ref SKELDRV_WORKFLOW "workflow chapter" for more information about when these callback are exactly called in the global driver workflow.
	 *************************************************************************************************************************/
	typedef struct {
		BOARD_INIT_STATUS (*CallBack_DeviceAdd_fct)(PDEVICE_EXTENSION pdx);			         ///< Callback called at the driver load (device create or probe)
      BOARD_INIT_STATUS (*CallBack_Wakeup_fct)(PDEVICE_EXTENSION pdx);
		BOOL32            (*CallBack_CheckResources_fct)(PDEVICE_EXTENSION pdx);
      void              (*CallBack_Sleep_fct)(PDEVICE_EXTENSION pdx, BOOL32 PowerOff_B);
		void              (*CallBack_DeviceRemove_fct)(PDEVICE_EXTENSION pdx);					///< Callback called at the driver unload (device unload or remove)
		BOOL32            (*CallBack_ISR_fct)(PDEVICE_EXTENSION pdx,ULONG *pIntSources_UL);	///< Callback called by the interrupt handler (OnInterrupt())
		void              (*CallBack_DPCforISR_fct)(PDEVICE_EXTENSION pdx, ULONG DPCIntSource_UL); ///< Callback called by the DPC attached to the ISR (DpcForIsr())
		BOOL32            (*CallBack_Open_fct)(PDEVICE_EXTENSION pdx, ULONG RefCount_UL,PID_TYPE PID_UL);///< Callback called when the driver is opened (CreateFile() or open())
		BOOL32            (*CallBack_Close_fct)(PDEVICE_EXTENSION pdx, ULONG RefCount_UL,PID_TYPE PID_UL);///< Callback called when the driver is opened (CloseHandle() or close())
		IOCTL_STATUS      (*CallBack_IoCtl_fct)(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL, PID_TYPE PID_UL); ///< Callback called during the processing of an IO Control
      void              (*CallBack_CleanUp_fct)(PDEVICE_EXTENSION pdx,PID_TYPE PID_UL, BOOL32 DevicePresent_B); ///< Callback called during cleanup (pre/process and general)
		void              (*CallBack_ReleasePermanentResources_fct)(PDEVICE_EXTENSION pdx);
      void              (*CallBack_PropMngrPropertiesInit)(PDEVICE_EXTENSION pdx,ULONG *pDefaultProperties_UL);
      BOOL32            (*CallBack_SetDrvPowerState_fct)(PDEVICE_EXTENSION pdx, DRV_POWER_STATE PowerState_E); ///< Callback called by the DPC attached to the power state function)
      
      /* Called when the DrvIF acks the wake up */
      void              (*CallBack_AckWakeup_fct)(PDEVICE_EXTENSION pdx, PID_TYPE Process_pid);
      /* Called when the OS detects a device removal */
      void              (*CallBack_DeviceRemoval_fct)(PDEVICE_EXTENSION pdx);
      /* Board must provide this function to inform skel about the device's presence */
      BOOL32            (*CallBack_IsDevicePresent_fct)(PDEVICE_EXTENSION pdx);
     void  (*CallBack_AllocDrvData_fct) (PDEVICE_EXTENSION pdx);				
      void  (*CallBack_FreeDrvData_fct) (PDEVICE_EXTENSION pdx);				

      ULONG pPreallocPoolSize_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS]; ///< Sizes of the preallocated buffer pools
      ULONG DriverVersion_UL;
      ULONG DefaultDbgMask_UL;
      ULONG DriverSubtype_UL;
      ULONG SleepContextSize_UL;    /* Size of the memory required to retain a board working state (active channels etc.), 0 = unused */

	} SKEL_CUSTOMIZATION;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/**********************************************************************************************************************//**
 * @fn extern "C" void SkelCallBack_EntryPoint(  )
 * @brief This function is called during the driver start.
 *
 * This function is defined in the skeleton driver but not implemented. It is mandatory to the final driver to implement this function.
 * @remark For more information about the driver workflow, please refer to @ref SKELDRV_WORKFLOW "this chapter".
 **********************************************************************************************************************/
#ifdef  __cplusplus
extern "C" 
{
#endif

void SkelCallBack_EntryPoint(void);

#ifdef  __cplusplus
};
#endif

/**********************************************************************************************************************//**
 * @fn extern "C" void SkelCallBack_CustomizeDrv( SKEL_CALLBACKS * pCallbacksList_X )
 * @brief This function, called by the skeleton at the beginning of the driver load allows customization of the 
 *        driver behavior be setting a table of callbacks.
 *
 * This function is defined in the skeleton driver but not implemented. It is mandatory to the final driver to implement this function.
 *
 * @param [out] pCallbacksList_X Pointer to callbacks tables
 *
 * @remark The callbacks table is already filled by NULL value by the called.
 * @remark For more information about the driver workflow, please refer to @ref SKELDRV_WORKFLOW "this chapter".
 **********************************************************************************************************************/
#ifdef  __cplusplus
extern "C" 
{
#endif

void SkelCallBack_CustomizeDrv(PDEVICE_EXTENSION pdx, SKEL_CUSTOMIZATION * pCallbacksList_X);

#ifdef  __cplusplus
};
#endif

#endif // _SKELCALLBACKS_H_










