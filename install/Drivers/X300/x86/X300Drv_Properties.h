/********************************************************************************************************************//**
 * @internal
 * @file   	X300Drv_Properties.h
 * @date   	2010/08/26
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/26  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _X300DRV_PROPERTIES_H_
#define _X300DRV_PROPERTIES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "StreamProperties.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define X300DRV_BOARD_FEATURES_GENLOCK0               0x00000001
#define X300DRV_BOARD_FEATURES_GENLOCK1               0x00000002
#define X300DRV_BOARD_FEATURES_KEYER                  0x00000004
#define X300DRV_BOARD_FEATURES_2048                   0x00000008
#define X300DRV_BOARD_FEATURES_ADVLP                  0x00000010
#define X300DRV_BOARD_FEATURES_FIELD_MODE             0x00000020
#define X300DRV_BOARD_FEATURES_UNUSED                 0x00000040
#define X300DRV_BOARD_FEATURES_4K                     0x00000100
#define X300DRV_BOARD_FEATURES_THUMBNAIL              0x00000200
#define X300DRV_BOARD_FEATURES_DL1080P                0x00000400
#define X300DRV_BOARD_FEATURES_BIDIR                  0x00000800
#define X300DRV_BOARD_FEATURES_RELAY_0                0x00001000
#define X300DRV_BOARD_FEATURES_RELAY_1                0x00002000
#define X300DRV_BOARD_FEATURES_RELAY_2                0x00004000
#define X300DRV_BOARD_FEATURES_RELAY_3                0x00008000
#define X300DRV_BOARD_FEATURES_HDMI_MONITORING        0x00010000
#define X300DRV_BOARD_FEATURES_CSCINPUT               0x00020000
#define X300DRV_BOARD_FEATURES_SIM_RAW_VIDEO_CAPTURE  0x00040000
#define X300DRV_BOARD_FEATURES_LTC                    0x00080000



#define X300DRV_RELAYx_DISABLE_VAL_BYUSER       0
#define X300DRV_RELAYx_DISABLE_VAL_FGPALOADED   1
#define X300DRV_RELAYx_DISABLE_VAL_BOARDOPEN    2
#define X300DRV_RELAYx_ENABLE_VAL_BYUSER        0
#define X300DRV_RELAYx_ENABLE_VAL_DRIVERUNLOAD  1
#define X300DRV_RELAYx_ENABLE_VAL_BOARDCLOSED   2

#define X300DRV_RX_CHN_STATE_BIT_COUPLED         0x00000001   
#define X300DRV_RX_CHN_STATE_BIT_FIELD_MODE      0x00000002
#define X300DRV_RX_CHN_STATE_BIT_4K              0x00000004
#define X300DRV_RX_CHN_STATE_BIT_DUAL_LINK       0x00000008
#define X300DRV_RX_CHN_STATE_BIT_3G_LEVELB       0x00000010


/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
   X300DRV_EXTBOARD_TYPE_NONE,
   X300DRV_EXTBOARD_TYPE_40,
   X300DRV_EXTBOARD_TYPE_3G,

} X300DRV_EXTBOARD_TYPE;


typedef enum
{
   X300DRV_PROPERTIES_FIRST=NB_STREAM_PROPERTIES,
   X300DRV_PROPERTIES_PRODUCT_FAMILY=X300DRV_PROPERTIES_FIRST,
   X300DRV_PROPERTIES_BOARD_FIRMWARE_VERSION,
   X300DRV_PROPERTIES_BOARD_ARM_VERSION,
   X300DRV_PROPERTIES_BOARD_SSN_MSB,
   X300DRV_PROPERTIES_BOARD_SSN_LSB,


   X300DRV_PROPERTIES_EXTBOARD_TYPE,
   X300DRV_PROPERTIES_BOARD_FIRMWAREID,
   X300DRV_PROPERTIES_RELAY0_DISABLE,
   X300DRV_PROPERTIES_RELAY0_ENABLE,
   X300DRV_PROPERTIES_RELAY1_DISABLE,
   X300DRV_PROPERTIES_RELAY1_ENABLE,

   X300DRV_PROPERTIES_BOARD_FEATURES,
   X300DRV_PROPERTIES_BOARD_FPGA_FW_ID,
   X300DRV_PROPERTIES_BOARD_ARM_FW_ID,
   X300DRV_PROPERTIES_BOARD_RQST_FIRMWARE_VERSION,
   X300DRV_PROPERTIES_BOARD_RQST_ARM_VERSION,
   X300DRV_PROPERTIES_BOARD_NB_UPGRADABLE_STUFF, 

   X300DRV_PROPERTIES_RELAY2_DISABLE,
   X300DRV_PROPERTIES_RELAY2_ENABLE,
   X300DRV_PROPERTIES_RELAY3_DISABLE,
   X300DRV_PROPERTIES_RELAY3_ENABLE,

   NB_X300DRV_PROPERTIES
} X300DRV_PROPERTIES;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _X300DRV_PROPERTIES_H_
