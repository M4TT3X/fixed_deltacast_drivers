/********************************************************************************************************************//**
 * @internal
 * @file   	CX3XXDrvIF.h
 * @date   	2014/08/27
 * @author 	bc
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	c     	Author   Description
   ====================================================================================================================
   2014/08/27  v00.01.0000    bc       Creation of this file
                                       
 **********************************************************************************************************************/

#ifndef _CX3XXDRVIF_H_
#define _CX3XXDRVIF_H_

/***** INCLUDES SECTION ***********************************************************************************************/


#include "CStreamDrvIF.h"
#include "X300Drv_Properties.h"
#include "X3XXDrv_Types.h"
#include "X300Drv_Types.h"
#include "X300Drv_Version.h"
#include "IFwUpdate.h"
#include "IFabInfo.h"
#include "SDI.h"
#include "NorFlash.h"
#include "CMailboxIF.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
typedef union 
{  
   UWORD pWord_W[8];
   struct{
      ULONG HwBrdId_UL;
      ULONG SwBrdId_UL;
      ULONG BrandingId_UL;
      WORD AuthorizationCode_W;
      WORD Reserved_W;         
   };
}SECURED_DATA;
/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

class CX3XXDrvIF : public CStreamDrvIF, public IFwUpdate, public INorFlashInterface, public IFabInfo
{
public:

   CX3XXDrvIF(DRIVER_ID_TYPE DriverId, ULONG BoardIdx_UL, ULONG DrvSubType_UL);

   virtual CDRVIF_STATUS Board_IsOK();

   CDRVIF_STATUS Board_ReadSSN(LONGLONG *pSerialNumber_LL);
   CDRVIF_STATUS Board_SetAsynchronous();
   CDRVIF_STATUS Board_ReadSecuredData();
   CDRVIF_STATUS Board_WriteSecuredData(SECURED_DATA *pSecuredData_X);
   CDRVIF_STATUS Board_ReadFabInfo(ULONG *pFabInfo_UL, ULONG *pSize_UL);
   CDRVIF_STATUS Board_WriteFabInfo(ULONG *pFabInfo_UL, ULONG *pSize_UL);
   CDRVIF_STATUS Board_WriteBiDirInfo(ULONG NbRx_UL, ULONG NbTx_UL);
   CDRVIF_STATUS Board_FWPROMUpdate( UBYTE FirmwareID_UB, UBYTE *pBuffer_UB, ULONG BufferSize_UL,BOOL32 CheckFirmware_B=FALSE, INotifyProgressFunctor *pSignalProgress_fct=NULL );
   CDRVIF_STATUS Board_FWPROMRead( UBYTE FirmwareID_UB, UBYTE *pBuffer_UB, ULONG BufferSize_UL, INotifyProgressFunctor *pSignalProgress_fct=NULL );
   CDRVIF_STATUS Board_ARMUpgrade( UBYTE *pBuffer_UB, ULONG BufferSize_UL );
   CDRVIF_STATUS Board_ARMRead( UBYTE *pBuffer_UB, ULONG BufferSize_UL );
   CDRVIF_STATUS Board_GetArmRevID(ULONG *pArmRevId_UL);
   CDRVIF_STATUS Board_GetArmSSN(ULONG *pArmSSNLS_UL,ULONG *pArmSSNIS_UL,ULONG *pArmSSNMS_UL);
   ULONG Board_BrandingChallenge( ULONG Challenge_UL );
   CDRVIF_STATUS Board_WatchdogArm();    
   CDRVIF_STATUS Board_WatchdogDisable();
   CDRVIF_STATUS Board_WatchdogEnable(ULONG TimeInMS_UL);  

   BOOL32 FLASH_BlockUnlock(ULONG StartAddr_UL, ULONG NbBlock_UL, INotifyProgressFunctor* pSignalProgress_fct=NULL );
   BOOL32 FLASH_BlockLock(ULONG StartAddr_UL, ULONG NbBlock_UL, INotifyProgressFunctor* pSignalProgress_fct=NULL );
   BOOL32 FLASH_BlockErase(ULONG StartAddr_UL, ULONG NbBlock_UL, INotifyProgressFunctor* pSignalProgress_fct=NULL);
   BOOL32 FLASH_BlockBlankCheck(ULONG StartAddr_UL, ULONG NbBlock_UL, INotifyProgressFunctor* pSignalProgress_fct=NULL );
   BOOL32 FLASH_Write(ULONG Addr_UL, UWORD *pBuffer_UW, ULONG BufferSize_UL, INotifyProgressFunctor* pSignalProgress_fct=NULL );
   BOOL32 FLASH_Write_256(ULONG Addr_UL, UWORD *pBuffer_UW, ULONG BufferSize_UL, INotifyProgressFunctor* pSignalProgress_fct=NULL );
   BOOL32 FLASH_Read(ULONG Addr_UL, UWORD *pBuffer_UW, ULONG BufferSize_UL, INotifyProgressFunctor* pSignalProgress_fct=NULL );
   BOOL32 FLASH_Check(ULONG Addr_UL, UWORD *pBuffer_UW, ULONG BufferSize_UL, INotifyProgressFunctor* pSignalProgress_fct=NULL );
   void FLASH_WriteWord(ULONG Addr_UL, WORD Val_W);
   WORD FLASH_ReadWord(ULONG Addr_UL);
   void FLASH_ResetSecuredCnt();
   static void MonitorFlashProgress(ULONG ProgressInPrc_UL, void *pUser);
   static ULONG Helper_SoftBrandingChallenge (ULONG Challenge_UL, ULONG BrandingKey_UL);  
   static ULONG Helper_SoftBrandingUnChallenge (ULONG Challenge_UL, ULONG BrandingKey_UL); 

protected:
   virtual CDRVIF_STATUS Board_ArmCmd(ULONG Tag, const ULONG* pBuffer_UL=NULL, ULONG ParamSize_UL=0, ULONG *pRtrn_UL=NULL, ULONG RtrnSize_UL=0, ULONG *pSts_UL=NULL);
   CDRVIF_STATUS Board_SetArmProcess(BOOL32 Enable_B);
   virtual BOOL32 CheckValidateAccessCode(ULONG ValidateAccessCode_UL);
   void SetFlashPartitionWordSize(ULONG FlashPartitionWordSize_UL);
   void SetSecuredDataAddr(ULONG SecuredDataAddr_UL);
   void SetFabInfoDataAddr(ULONG FabInfoDataAddr_UL);
   void SetBiDirInfoDataAddr(ULONG mBiDirInfoDataAddr_UL);

   NOR_FLASH_REG m_NorFlashReg_X;
   CMailboxIF    mMailboxIf_O;

private:
   ULONG mFlashPartitionWordSize_UL;
   ULONG mSecuredDataAddr_UL;
   ULONG mFabInfoDataAddr_UL;
   ULONG mBiDirInfoDataAddr_UL;

};

#endif // _CX300DRVIF_H_
