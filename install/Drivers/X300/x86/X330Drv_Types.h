/********************************************************************************************************************//**
 * @internal
 * @file   	BoardTypes.h
 * @date   	2014/08/29
 * @author 	bc
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2014/08/29  v00.01.0000    bc       Creation of this file

 **********************************************************************************************************************/

#ifndef _X330BOARDTYPES_H_
#define _X330BOARDTYPES_H_

#include "CTypes.h"
/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define X330BRD_PARTITION_WORD_SIZE     (2<<22)   
#define X330BRD_SECURED_DATA_ADDR       0x1FF0000
#define X330BRD_FAB_INFO_DATA_ADDR      0x1FE0000
#define X330BRD_BIDIR_CFG_DATA_ADDR     0x1FD0000
#define X330BRD_NB_ONBRD_BUF   8 ///< Defines the number of on board buffers per data type.

typedef enum{
   X330BRD_CHN_DATA_TYPE_VIDEO             =0,
   X330BRD_CHN_DATA_TYPE_YANC              =1,
   X330BRD_CHN_DATA_TYPE_CANC              =2,
   X330BRD_CHN_DATA_TYPE_VIDEO_SND_PATH    =3,
   X330BRD_CHN_DATA_TYPE_PLANAR1           =4,
   X330BRD_CHN_DATA_TYPE_PLANAR2           =5,
   X330BRD_CHN_DATA_TYPE_YANCB             =6,
   X330BRD_CHN_DATA_TYPE_CANCB             =7,
}X330BRD_CHN_DATA_TYPE;


typedef enum{
   X330BRD_BUFQ_DATA_TYPE_VIDEO,
   X330BRD_BUFQ_DATA_TYPE_YANC,
   X330BRD_BUFQ_DATA_TYPE_CANC,
   X330BRD_BUFQ_DATA_TYPE_VIDEO2,
   X330BRD_BUFQ_DATA_TYPE_YANC2,
   X330BRD_BUFQ_DATA_TYPE_CANC2,
   X330BRD_BUFQ_DATA_TYPE_VIDEO3,
   X330BRD_BUFQ_DATA_TYPE_YANC3,
   X330BRD_BUFQ_DATA_TYPE_CANC3,
   X330BRD_BUFQ_DATA_TYPE_VIDEO4,
   X330BRD_BUFQ_DATA_TYPE_YANC4,
   X330BRD_BUFQ_DATA_TYPE_CANC4,
   X330BRD_BUFQ_DATA_TYPE_VIDEO_B,
   X330BRD_BUFQ_DATA_TYPE_YANC_B,
   X330BRD_BUFQ_DATA_TYPE_CANC_B,
   X330BRD_BUFQ_DATA_TYPE_VIDEO2_B,
   X330BRD_BUFQ_DATA_TYPE_YANC2_B,
   X330BRD_BUFQ_DATA_TYPE_CANC2_B,
   X330BRD_BUFQ_DATA_TYPE_VIDEO3_B,
   X330BRD_BUFQ_DATA_TYPE_YANC3_B,
   X330BRD_BUFQ_DATA_TYPE_CANC3_B,
   X330BRD_BUFQ_DATA_TYPE_VIDEO4_B,
   X330BRD_BUFQ_DATA_TYPE_YANC4_B,
   X330BRD_BUFQ_DATA_TYPE_CANC4_B,
   X330BRD_BUFQ_DATA_TYPE_VIDEO_SECOND_PATH,
   X330BRD_BUFQ_DATA_TYPE_VIDEO2_SECOND_PATH,
   X330BRD_BUFQ_DATA_TYPE_VIDEO3_SECOND_PATH,
   X330BRD_BUFQ_DATA_TYPE_VIDEO4_SECOND_PATH
}X330BRD_BUFQ_DATA_TYPE;
//When adding new data type, you should update MAX_BUFFER_QUEUE_DATA_TYPES 

#define X330_ADVLP_VIDEO_ADDR_MAX 7 //Nb of secondary addresses for advanced line padding
#define X330_ADVLP_PARAM_MAX 7  //Nb of param for advanced line padding

#define X330BRD_BUFQUEUE_USRPARAM_IDX_DATACONTENT                  0
#define X330BRD_BUFQUEUE_USRPARAM_IDX_CHANNEL_INDEX                1
#define X330BRD_BUFQUEUE_USRPARAM_IDX_EOD_VALUE                    2
#define X330BRD_BUFQUEUE_USRPARAM_IDX_CURRENT_ONBRD_BUF_IDX        3
#define X330BRD_BUFQUEUE_USRPARAM_IDX_NB_ON_BOARD_BUFFER           4
#define X330BRD_BUFQUEUE_USRPARAM_IDX_VIDEO_ODD_SIZE               5
#define X330BRD_BUFQUEUE_USRPARAM_IDX_VIDEO_EVEN_SIZE              6
#define X330BRD_BUFQUEUE_USRPARAM_IDX_LB_ODD_SIZE                  7
#define X330BRD_BUFQUEUE_USRPARAM_IDX_LB_EVEN_SIZE                 8
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ADVLP_PARAM                  9
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_ADVLP   (X330BRD_BUFQUEUE_USRPARAM_IDX_ADVLP_PARAM+X330_ADVLP_PARAM_MAX)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE          (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_ADVLP+(X330_ADVLP_VIDEO_ADDR_MAX*X330BRD_NB_ONBRD_BUF))
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC_BASE     (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_YANC*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC_BASE     (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_CANC*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO2_BASE   (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO2*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC2_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_YANC2*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC2_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_CANC2*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO3_BASE   (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO3*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC3_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_YANC3*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC3_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_CANC3*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO4_BASE   (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO4*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC4_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_YANC4*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC4_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_CANC4*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_B_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC_B_BASE     (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_YANC_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC_B_BASE     (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_CANC_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO2_B_BASE   (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO2_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC2_B_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_YANC2_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC2_B_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_CANC2_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO3_B_BASE   (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO3_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC3_B_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_YANC3_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC3_B_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_CANC3_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO4_B_BASE   (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO4_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_YANC4_B_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_YANC4_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_CANC4_B_BASE    (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_CANC4_B*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO_SECOND_PATH_BASE   (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO_SECOND_PATH*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO2_SECOND_PATH_BASE  (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO2_SECOND_PATH*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO3_SECOND_PATH_BASE  (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO3_SECOND_PATH*X330BRD_NB_ONBRD_BUF)
#define X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_VIDEO4_SECOND_PATH_BASE  (X330BRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE+X330BRD_BUFQ_DATA_TYPE_VIDEO4_SECOND_PATH*X330BRD_NB_ONBRD_BUF)

//When adding new user param, you should update MAX_BUFFER_QUEUE_USER_PARAM 



#define X330BRD_BUFQUEUE_DATACONTENT_VIDEO                 (1<<X330BRD_BUFQ_DATA_TYPE_VIDEO)
#define X330BRD_BUFQUEUE_DATACONTENT_YANC                  (1<<X330BRD_BUFQ_DATA_TYPE_YANC)
#define X330BRD_BUFQUEUE_DATACONTENT_CANC                  (1<<X330BRD_BUFQ_DATA_TYPE_CANC)
#define X330BRD_BUFQUEUE_DATACONTENT_VIDEO2                (1<<X330BRD_BUFQ_DATA_TYPE_VIDEO2)
#define X330BRD_BUFQUEUE_DATACONTENT_YANC2                 (1<<X330BRD_BUFQ_DATA_TYPE_YANC2)
#define X330BRD_BUFQUEUE_DATACONTENT_CANC2                 (1<<X330BRD_BUFQ_DATA_TYPE_CANC2)
#define X330BRD_BUFQUEUE_DATACONTENT_VIDEO3                (1<<X330BRD_BUFQ_DATA_TYPE_VIDEO3)
#define X330BRD_BUFQUEUE_DATACONTENT_YANC3                 (1<<X330BRD_BUFQ_DATA_TYPE_YANC3)
#define X330BRD_BUFQUEUE_DATACONTENT_CANC3                 (1<<X330BRD_BUFQ_DATA_TYPE_CANC3)
#define X330BRD_BUFQUEUE_DATACONTENT_VIDEO4                (1<<X330BRD_BUFQ_DATA_TYPE_VIDEO4)
#define X330BRD_BUFQUEUE_DATACONTENT_YANC4                 (1<<X330BRD_BUFQ_DATA_TYPE_YANC4)
#define X330BRD_BUFQUEUE_DATACONTENT_CANC4                 (1<<X330BRD_BUFQ_DATA_TYPE_CANC4)
#define X330BRD_BUFQUEUE_DATACONTENT_VIDEO_SECOND_PATH     (1<<X330BRD_BUFQ_DATA_TYPE_VIDEO_SECOND_PATH)
#define X330BRD_BUFQUEUE_DATACONTENT_VIDEO2_SECOND_PATH    (1<<X330BRD_BUFQ_DATA_TYPE_VIDEO2_SECOND_PATH)
#define X330BRD_BUFQUEUE_DATACONTENT_VIDEO3_SECOND_PATH    (1<<X330BRD_BUFQ_DATA_TYPE_VIDEO3_SECOND_PATH)
#define X330BRD_BUFQUEUE_DATACONTENT_VIDEO4_SECOND_PATH    (1<<X330BRD_BUFQ_DATA_TYPE_VIDEO4_SECOND_PATH)
#define X330BRD_BUFQUEUE_DATACONTENT_YANC_B                (1<<X330BRD_BUFQ_DATA_TYPE_YANC_B)
#define X330BRD_BUFQUEUE_DATACONTENT_CANC_B                (1<<X330BRD_BUFQ_DATA_TYPE_CANC_B)
#define X330BRD_BUFQUEUE_DATACONTENT_YANC2_B               (1<<X330BRD_BUFQ_DATA_TYPE_YANC2_B)
#define X330BRD_BUFQUEUE_DATACONTENT_CANC2_B               (1<<X330BRD_BUFQ_DATA_TYPE_CANC2_B)
#define X330BRD_BUFQUEUE_DATACONTENT_YANC3_B               (1<<X330BRD_BUFQ_DATA_TYPE_YANC3_B)
#define X330BRD_BUFQUEUE_DATACONTENT_CANC3_B               (1<<X330BRD_BUFQ_DATA_TYPE_CANC3_B)
#define X330BRD_BUFQUEUE_DATACONTENT_YANC4_B               (1<<X330BRD_BUFQ_DATA_TYPE_YANC4_B)
#define X330BRD_BUFQUEUE_DATACONTENT_CANC4_B               (1<<X330BRD_BUFQ_DATA_TYPE_CANC4_B)


#define X330BRD_BUFHDR_USRIDX_TIMESTAMP      0x0
#define X330BRD_BUFHDR_USRIDX_EVEN_PARITY    0x1
#define X330BRD_BUFHDR_27MHZ_TIMESTAMP_LOW   0x2  ///< 27MHz Time Stamp value associated with the buffer (RX-only)
#define X330BRD_BUFHDR_27MHZ_TIMESTAMP_HIGH  0x3
#define X330BRD_BUFHDR_NB_CRC_LINE_ERROR     0x4

#define X330BRD_BUFHDR_USRIDX_LTC_MSB        0x5
#define X330BRD_BUFHDR_USRIDX_LTC_LSB        0x6
#define X330BRD_BUFHDR_USRIDX_LTC_STATUS     0x7

#define BUFHDR_STS_FRAMING_ERROR       0x1


#define X330BRD_DMA_PADDING_SIZE   DMA_PADDING_SIZE
#define X330BRD_DMA_ADDR_ALIGN     0x1000

#define X330BRD_ROUND_TO_x(Size, x)  (((ULONG)(Size) + x - 1) & ~(x - 1))
#define X330BRD_PAD_DMA_SIZE(Size)    X330BRD_ROUND_TO_x(Size, X330BRD_DMA_PADDING_SIZE)
#define X330BRD_ALIGN_DMA_ADDR(Size)  X330BRD_ROUND_TO_x(Size, X330BRD_DMA_ADDR_ALIGN)
   

typedef enum _X330_FW_ID{
   X330_FPGA_FW_ID_FAILSAFE,
   X330_FPGA_FW_ID_3G8C,
   X330_ARM_FW_ID_3G8C,
   X330_FPGA_FW_ID_3G4C,
   X330_FPGA_FW_ID_3GKEY,
   NBOF_X330_FW_ID
}X330_FW_ID;

typedef enum _X330_BIDIR_CFG
{
   X330_BIDIR_CFG_40,
   X330_BIDIR_CFG_31,
   X330_BIDIR_CFG_22,
   X330_BIDIR_CFG_13,
   X330_BIDIR_CFG_04,
   X330_BIDIR_CFG_80,
   X330_BIDIR_CFG_71,
   X330_BIDIR_CFG_62,
   X330_BIDIR_CFG_53,
   X330_BIDIR_CFG_44,
   X330_BIDIR_CFG_35,
   X330_BIDIR_CFG_26,
   X330_BIDIR_CFG_17,
   X330_BIDIR_CFG_08,
   X330_BIDIR_CFG_88,
   NBOF_X330_BIDIR_CFG
}X330_BIDIR_CFG;

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _X300BOARDTYPES_H_
