/********************************************************************************************************************//**
 * @file   PCIeBridge_ADVLP.h
 * @date   2008/04/01
 * @Author cs
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2008/04/01  v00.01.0000    cs       Creation of this file
   2008/12/02  v00.01.0001    cs       Replace some Read-Modify-Write access to PCIEBRIDGE_DMACSR_MM32 register to simple
                                       write accesses. The involved bits are auto-clear, so it is not necessary to read 
                                       the value to just set an auto-clear bit. Furthermore, there was dangerous
                                       case because ISR routine access by a read-modify-write access when a DPC routine is
                                       potentially doing the same during a DMA start. That could lead to a race condition
   2011/01/26  v00.02.0000    cs       Add support of full duplex DMA transfer.
 **********************************************************************************************************************/

#ifndef _PCIEBRIDGE_ADVLP_H_
#define _PCIEBRIDGE_ADVLP_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvTypes.h"
#include "CommonBufMngrTypes.h"
#include "DMAMngr.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

BOOL32 PCIeBridge_Init(PDEVICE_EXTENSION pdx);
BOOL32 PCIeBridge_DownloadFPGA(PDEVICE_EXTENSION pdx, UBYTE *pBuffer_UB,ULONG Size_UL);

void PCIeBridge_EnableUserInterrupt(PDEVICE_EXTENSION pdx,BOOL32 Enable_B);
BOOL32 PCIeBridge_IsUserInterruptIsEnable(PDEVICE_EXTENSION pdx);

BOOL32 PCIeBridge_DMA_Init(PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL,  COMMON_BUFFER_DESCR *pPtr_X);
BOOL32 PCIeBridge_DMA_Start(PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL, TRANSLATED_DMA_REQUEST *,BOOL32 EnableInterrupt_B);
void PCIeBridge_DMA_Abort(PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL);

BOOL32 PCIeBridge_ISR_CatchInt(PDEVICE_EXTENSION, ULONG *pDMADoneInt_UL, ULONG *pUserInterrupt_UL);
void PCIeBridge_ISR_ClearInt(PDEVICE_EXTENSION);

#endif // _PCIEBRIDGE_ADVLP_H_
