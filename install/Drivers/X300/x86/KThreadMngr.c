/********************************************************************************************************************//**
 * @file   	KThreadMngr.cpp
 * @date   	2011/02/16
 * @author 	cs
 * @brief  
 **********************************************************************************************************************/
  
/***** INCLUDES SECTION ***********************************************************************************************/

#define MODID MID_KTHREADMNGR

#include "Driver.h"
#include "KThreadMngr.h"
#include "DrvDbg.h"
#include "InterlockedOp.h"
#include "KernelSysCall.h"

/***** LOCAL SYMBOLS DEFINITIONS **************************************************************************************/

/***** LOCAL MACROS DEFINITIONS ***************************************************************************************/

#define TraceThreadOutput(pFmt_c,...)    xxxOutput(DBG_OUTPUT_USR_MSG(0), "TRACETHREAD: ", pFmt_c, ##__VA_ARGS__)
#define TraceThreadMsgOutput(pFmt_c,...) xxxOutput(DBG_OUTPUT_USR_MSG(0), "TRACETHREADMSG: ", pFmt_c, ##__VA_ARGS__)

#define GetKThreadMngrData() (&pdx->KThreadMngrData_X) ///< Macro to retrieve Event Manager data from the device context.

/** @name Event Manager safe area
 The Event manager shares data between different execution context (multi-user, DPC, ...). The access to these shared data
 can be safe using a local safe area that can be used through this set of macros.
*///@{

#define KTHREADMNGR_INIT_SAFEAREA()            KOBJ_InitializeSpinLock(pdx,&GetKThreadMngrData()->SpinLock_KO);
#define KTHREADMNGR_ENTER_SAFEAREA()           KOBJ_AcquireSpinLock(pdx,&GetKThreadMngrData()->SpinLock_KO)
#define KTHREADMNGR_LEAVE_SAFEAREA()           KOBJ_ReleaseSpinLock(pdx,&GetKThreadMngrData()->SpinLock_KO);
#define KTHREADMNGR_ENTER_SAFEAREA_DPC()       KOBJ_AcquireSpinLockAtDPCLevel(pdx,&GetKThreadMngrData()->SpinLock_KO)
#define KTHREADMNGR_LEAVE_SAFEAREA_DPC()       KOBJ_ReleaseSpinLockAtDPCLevel(pdx,&GetKThreadMngrData()->SpinLock_KO);

//@}

/***** LOCAL TYPES AND STRUCTURES DEFINITIONS *************************************************************************/

/***** EXTERNAL VARIABLES *********************************************************************************************/

/***** GLOBAL VARIABLES ***********************************************************************************************/

/***** FUNCTIONS DEFINITION *******************************************************************************************/
#pragma LOCKEDCODE

int KThreadMngr_KThreadProcWrapper(KOBJ_KTHREAD * pKThreadOBJ_X);

/***** CLASSES DEFINITION *********************************************************************************************/

void KThreadMngr_DeviceAdd( PDEVICE_EXTENSION pdx )
{
int i;

   EnterOutput("\n");

   KTHREADMNGR_INIT_SAFEAREA();

   for (i=0; i<MAX_KTHREADS; i++)
   {
      GetKThreadMngrData()->pKThreadList_X[i].KThreadID=INVALID_KTHREADID;
      GetKThreadMngrData()->pKThreadList_X[i].Flags_UL =0;
      GetKThreadMngrData()->pKThreadList_X[i].pdxcopy=pdx;
   }

   ExitOutput("\n");
}


KTHREADID KThreadMngr_CreateKThread( PDEVICE_EXTENSION pdx, KTHREADMNGR_FCT KThreadProc_fct, void * pContext_v, ULONG KThreadPeriod_UL )
{
KTHREADID KThreadID = INVALID_KTHREADID;
KTHREAD_DESCR *pKThreadDescr_X=NULL;
int i;

   EnterOutput("(Context:%08p / Period:%08lxh)\n",pContext_v,KThreadPeriod_UL);

   /* Find a not used KThread Descriptor within a safe area... If found, mark ti as "used" */

   KTHREADMNGR_ENTER_SAFEAREA();
   
      for (i=0; i<MAX_KTHREADS; i++)
      {
         if (!(GetKThreadMngrData()->pKThreadList_X[i].Flags_UL&KTHREAD_FLAGS_USED))
         {
            KThreadID = i;
            pKThreadDescr_X = &GetKThreadMngrData()->pKThreadList_X[i];
            GetKThreadMngrData()->pKThreadList_X[i].Flags_UL|=KTHREAD_FLAGS_USED;
            break;
         }
      }

   KTHREADMNGR_LEAVE_SAFEAREA();


   if (KThreadID!=INVALID_KTHREADID)
   {
      pKThreadDescr_X->KThreadID = KThreadID;
      pKThreadDescr_X->pdxcopy=pdx;
      pKThreadDescr_X->pContext_v=pContext_v;
      pKThreadDescr_X->KThreadFunction_fct=KThreadProc_fct;
      pKThreadDescr_X->KThreadPeriod_UL=KThreadPeriod_UL;

      if (KOBJ_CreateKThread(pdx,&pKThreadDescr_X->KThread_KOBJ,KThreadMngr_KThreadProcWrapper, pKThreadDescr_X))
      {
         TraceThreadOutput("KThread (KTHREADID=%08lxh) Created \n",KThreadID);
      }
      else ErrOutput("Unable to create thread\n");
   }
   else ErrOutput("No more KTHREADID available\n");


   ExitOutput("(KThreadID:%08lxh)\n",KThreadID);
   return KThreadID;
}



int KThreadMngr_KThreadProcWrapper(KOBJ_KTHREAD * pKThreadOBJ_X)
{
   KTHREAD_DESCR *pKThreadDescr_X = (KTHREAD_DESCR *) KOBJKT_GetKThreadParam(pKThreadOBJ_X);
   BOOL32 Finished_B = FALSE;
   BOOL32 Timeout_B;
   ULONG SyncCmd_UL;
   KTHREAD_EVENT Event_e;
   ULONG EventParam_UL;
   PDEVICE_EXTENSION pdx = pKThreadDescr_X->pdxcopy;

   EnterOutput("\n");

   if (pKThreadDescr_X)
   {
      /* Call a first time before the loop allowing creation initialization */
      
      TraceThreadOutput("KThreadProc (KTHREADID=%08lxh) started with period set to %08lxh\n",pKThreadDescr_X->KThreadID,pKThreadDescr_X->KThreadPeriod_UL);
      TraceThreadMsgOutput("KThreadProc (KTHREADID=%08lxh) Event : KTHREAD_EVENT_INIT\n",pKThreadDescr_X->KThreadID);
      pKThreadDescr_X->KThreadFunction_fct(pKThreadDescr_X->pdxcopy, pKThreadDescr_X->KThreadID,pKThreadDescr_X->pContext_v,KTHREAD_EVENT_INIT, 0);

      do 
      {
         EventParam_UL = 0;

         TraceThreadOutput("KThreadProc (KTHREADID=%08lxh) is waiting for %08lxh ms\n",pKThreadDescr_X->KThreadID,pKThreadDescr_X->KThreadPeriod_UL);
         Timeout_B = KOBJKT_WaitForKThreadSyncCmd(pdx,pKThreadOBJ_X,&SyncCmd_UL, pKThreadDescr_X->KThreadPeriod_UL);

         if (!Timeout_B) 
         {
            Event_e = KTHREAD_EVENT_PERIODIC;
            TraceThreadMsgOutput("KThreadProc (KTHREADID=%08lxh) Event : KTHREAD_EVENT_PERIODIC\n",pKThreadDescr_X->KThreadID);
         }
         else
         {
            if (SyncCmd_UL==KTHREAD_SYNCMD_STOP) 
            {
               Event_e = KTHREAD_EVENT_STOP;
               TraceThreadMsgOutput("KThreadProc (KTHREADID=%08lxh) Event : KTHREAD_EVENT_STOP\n",pKThreadDescr_X->KThreadID);
               Finished_B=TRUE;
            }
            else 
            {
               if (SyncCmd_UL==KTHREAD_SYNC_PERIOD_UPDATE)
               {
                  Event_e = KTHREAD_EVENT_PERIOD_UPDATE;
                  TraceThreadMsgOutput("KThreadProc (KTHREADID=%08lxh) Event : KTHREAD_EVENT_PERIOD_UPDATE\n",pKThreadDescr_X->KThreadID);
               }
               else
               {
                  Event_e = KTHREAD_EVENT_WAKEUP;
                  EventParam_UL = SyncCmd_UL;
                  TraceThreadMsgOutput("KThreadProc (KTHREADID=%08lxh) Event : KTHREAD_EVENT_WAKEUP\n",pKThreadDescr_X->KThreadID);
               }
            }
         }

         pKThreadDescr_X->KThreadFunction_fct(pKThreadDescr_X->pdxcopy, pKThreadDescr_X->KThreadID,pKThreadDescr_X->pContext_v,Event_e, EventParam_UL);

      } while(!Finished_B);
   }
   else ErrOutput("Unable to retrieve KThread Context\n");

   TraceThreadOutput("KThreadProc (KTHREADID=%08lxh) exits\n",pKThreadDescr_X->KThreadID);


   ExitOutput("\n");
   KOBJKT_ExitKThread(pdx,pKThreadOBJ_X);
   return 0;

}


KTHREAD_DESCR * KThreadMngr_GetKThreadDescr(PDEVICE_EXTENSION pdx, KTHREADID KThreadID )
{
KTHREAD_DESCR * pTmp_X = NULL;
   if ((KThreadID<MAX_KTHREADS) && (GetKThreadMngrData()->pKThreadList_X[KThreadID].Flags_UL&KTHREAD_FLAGS_USED))
   {
      pTmp_X = &GetKThreadMngrData()->pKThreadList_X[KThreadID];
   }

   return pTmp_X;
}

BOOL32 KThreadMngr_StopKThread( PDEVICE_EXTENSION pdx, KTHREADID KThreadID )
{
   KTHREAD_DESCR * pKThreadDescr_X = KThreadMngr_GetKThreadDescr( pdx,  KThreadID);

   if (pKThreadDescr_X)
   {
      KOBJ_StopKThread(pdx,&pKThreadDescr_X->KThread_KOBJ);
      pKThreadDescr_X->KThreadID=INVALID_KTHREADID;
      pKThreadDescr_X->Flags_UL =0;
      TraceThreadOutput("KThread (KTHREADID=%08lxh) is stopped\n",KThreadID);
      return TRUE;
   }
   else ErrOutput("Invalid KThreadID (%08lx)\n",KThreadID);
   return FALSE;
}

BOOL32 KThreadMngr_SignalKThread( PDEVICE_EXTENSION pdx,KTHREADID KThreadID,ULONG Param_UL )
{
   KTHREAD_DESCR * pKThreadDescr_X = KThreadMngr_GetKThreadDescr( pdx,  KThreadID);

   if (pKThreadDescr_X)
   {
      if (!(Param_UL&0x80000000))
      {
         return KOBJ_SendKThreadSyncCmd(pdx,&pKThreadDescr_X->KThread_KOBJ, Param_UL);
      }
      else ErrOutput("Invalid KThread Command Word (%08lx)\n",Param_UL);
   }
   else ErrOutput("Invalid KThreadID (%08lx)\n",KThreadID);
   return FALSE;
}

void KThreadMngr_ReleasePermanentResources( PDEVICE_EXTENSION pdx )
{
int i;
   for (i=0; i<MAX_KTHREADS; i++)
   {
      if (GetKThreadMngrData()->pKThreadList_X[i].Flags_UL&KTHREAD_FLAGS_USED)
      {
         KThreadMngr_StopKThread(pdx, i);
      }
   }
}

/**********************************************************************************************************************//**
 * @fn BOOL32 KThreadMngr_SetKThreadPeriod( PDEVICE_EXTENSION pdx,KTHREADID KThreadID,ULONG Timeout_UL, BOOL32 Signal_B )
 * @brief This method allows changing a KThread period.
 *
 * Note that the new period will only take care on the next wake-up. To force a wake-up for an immediate period change,
 * set the Signal_B parameter to TRUE. In this case, the thread will wake-up by calling the thread procedure with a 
 * KTHREAD_EVENT_PERIOD_UPDATE event and will go to a sleep corresponding to the new period.
 *
 * Note that in the case when the function is called from the thread procedure itself, do not set the Signal_B to TRUE.
 * The new period will be immediately taken care about because the thread is already woken up.
 *
 * @param pdx Pointer to a device context
 * @param KThreadID Specifies the KThread ID
 * @param Timeout_UL Specifies the new period
 * @param Signal_B Wake-up the thread for an immediate update (see remark)
 *
 * @return TRUE in success case, FALSE otherwise.
 * @remark Do not set to TRUE the Signal_B parameter if called from the thread procedure itself.
 **********************************************************************************************************************/
BOOL32 KThreadMngr_SetKThreadPeriod( PDEVICE_EXTENSION pdx,KTHREADID KThreadID,ULONG Timeout_UL, BOOL32 Signal_B )
{
KTHREAD_DESCR * pKThreadDescr_X = KThreadMngr_GetKThreadDescr( pdx,  KThreadID);

   if (pKThreadDescr_X)
   {
      InterlockedExchange((volatile LONG*)&pKThreadDescr_X->KThreadPeriod_UL,Timeout_UL);


      TraceThreadOutput("KThread (KTHREADID=%08lxh) Set Period to %08lxh ms (Signal_B:%08lx)\n",KThreadID,Timeout_UL,Signal_B);

      if (Signal_B)
      {
         return KOBJ_SendKThreadSyncCmd(pdx,&pKThreadDescr_X->KThread_KOBJ, KTHREAD_SYNC_PERIOD_UPDATE);
      }
   }
   else ErrOutput("Invalid KThreadID (%08lx)\n",KThreadID);
   return FALSE;

}

IOCTL_STATUS KThreadMngr_IoCtl( PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,PID_TYPE PID_UL )
{
IOCTL_STATUS Sts_e = IOCTL_STATUS_OK;
IOCTL_KTHREAD_ACCESS_PARAM Param_X;
   switch(Cmd_UL)
   {
   case IOCTL_KTHREAD_ACCESS:
      if (!KSYSCALL_CopyFromUserSpace(&Param_X,pBuffer_UB,sizeof(IOCTL_KTHREAD_ACCESS_PARAM)))
      {
         switch (Param_X.Type_e)
         {
         case KTHREAD_ACCESS_TYPE_WAKEUP: IoctlOutput("KTHREAD_ACCESS_TYPE_WAKEUP\n");
            if (!KThreadMngr_SignalKThread(pdx, Param_X.KThreadID,Param_X.SyncCmd_UL)) Sts_e = IOCTL_STATUS_FAILED;
            break;
         case KTHREAD_ACCESS_TYPE_STOP: IoctlOutput("KTHREAD_ACCESS_TYPE_STOP\n");
            if (!KThreadMngr_StopKThread(pdx, Param_X.KThreadID)) Sts_e = IOCTL_STATUS_FAILED;
            break;
         case KTHREAD_ACCESS_TYPE_SET_PERIOD: IoctlOutput("KTHREAD_ACCESS_TYPE_SET_PERIOD\n");
            if (!KThreadMngr_SetKThreadPeriod(pdx, Param_X.KThreadID,Param_X.ThreadPeriod_UL,TRUE)) Sts_e = IOCTL_STATUS_FAILED;
            break;
         }
      }
      else  
      {
         ErrOutput("KSYSCALL_CopyFromUserSpace fails\n");
         Sts_e = IOCTL_STATUS_INVALID_PARAMETER;
      }

      break;

   default:
      Sts_e = IOCTL_STATUS_UNKNOWN_IOCTL;
   }

   return Sts_e;
}
