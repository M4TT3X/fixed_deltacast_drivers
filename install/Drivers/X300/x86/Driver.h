/********************************************************************************************************************//**
 * @file   	Driver.h
 * @date   	2010/06/10
 * @author 	cs
 * @version v00.01.0000 
 * @brief   This file contains the definition of the device extension data structure (DEVICE_EXTENSION).
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/10  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _DRIVER_H_
#define _DRIVER_H_

/***** INCLUDES SECTION ***********************************************************************************************/

//#define DRIVER	// Note : Must be placed before #include "DrvTypes.h"
#include "DrvTypes.h"
#include "Register.h"
#include "SkelDrv.h"
#include "CommonBufMngr.h"
#include "USBufMngr.h"
#include "DMABufMngr.h"
#include "EventMngr.h"
#include "PropertiesMngr.h"
#include "ShadowRegisters.h"
#include "KThreadMngr.h"
#include "DrvData.h"
#include "SkelCallBacks.h"
#include "ModulesID.h"
//#include "MailboxMngr.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

//#define NB_PCI_BARS 6
//#define MAX_PROCESSES   12 //TODO_THUMB : should be increased?  ///< Defines the maximum number of processes that can be connected to a board at the same time.

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/**********************************************************************************************************************//**
 * @internal
 * @struct PROCESS_DATA
 * @brief  This data structure stores reference count by process ID.
 * @remark The ProcessGUID field was introduced because when a process dies, its PID can be reused. A race condition could 
 *         occurs when a process is closed quickly followed by a reopen.
 *************************************************************************************************************************/
typedef struct  
{
   PID_TYPE PID_UL;
   ULONG RefCount_UL;
   ULONG ProcessGUID_UL;
   BOOL32 NeedAckWakeup_B;   
} PROCESS_DATA;

/**********************************************************************************************************************//**
 * @struct _DEVICE_EXTENSION
 * @brief  This structure defines the device context, that will given to every driver call. 
 *
 * It contains all needed data attached to one board, and allocated in a such a way to ensure that data are always available
 * even in interruption case.
 *
 * The target driver has to provide a DrvData.h file that contains the definition of the DRV_DATA data structure to add 
 * its own data to the device context.
 *
 * @remark 
 *************************************************************************************************************************/

struct _DEVICE_EXTENSION {

	OS_DRV_DATA             *pOsDrvData_X;	///< OS Dependent data

	int                  	BoardIdx_i;		///< Contains the board index in the case of multi-boards configuration.
	volatile ULONG          IntSource_UL;	///< Interrupt source identification (Please refer to the @ref SKELDRV_INTERRUPT_MANAGEMENT "Interrupt Management chapter")
	volatile ULONG          DPCIntMask_UL;	///< Interrupt mask for DPC calling from ISR (Please refer to the @ref SKELDRV_INTERRUPT_MANAGEMENT "Interrupt Management chapter")
	KOBJ_SPINLOCK           SpinLock_KO;

   PROCESS_DATA            pProcess_X[MAX_PROCESSES];    ///< Keeps information about processes attached to a driver instance.
   ULONG                   ProcessGuidGenerator_UL;

	BOARD_INIT_STATUS       BoardInitStatus_E;            ///< Keeps the board status returned by the 
   SKEL_CUSTOMIZATION      CustomizationTable_X;

   void*                   pSleepContext_v;              /* Additional memory to retain the operating state of the device during sleep */
   KOBJ_SPINLOCK           SleepContextLock_KO;          /* A spinlock protecting the sleep context */
   BOOL32                  DeviceRemoved_B;              /* True when we know the device has been removed (no come back) */
   BOOL32                  IsSleeping_B;                 /* True after calling SkelDrv_Sleep and FALSE after SkelDrv_Wakeup */

   ULONG                   pDbgMsk_UL[MAX_MODULES_ID];   ///< Defines the allowed dbg mask for available modules (modules ID are defined in ModulesID.h provided by target driver).

   PCI_RESOURCE            pPCIResources_X[NB_PCI_BARS];	///< Keeps PCI resources information about each PCI BARs
	COMMON_BUF_MNGR_DATA    CommonBufMngrData_X;    		///< Needed data for the common buffer manager module
   US_BUF_MNGR_DATA        USBufMngrData_X;              ///< Needed data for the Userspace DMA buffer manager
   DMABUFMNGR_DATA         DMABufMngrData_X;             ///< Needed data for DMA buffer manager.
	EVENT_MNGR_DATA         EventMngrData_X;        		///< Needed data for Events Manager module
 	PROPERTIES_MNGR_DATA    PropertiesMngrData_X;   		///< Needed data for Properties Manager
 	SHADOWREG_DATA          ShadowRegData_X;				   ///< Needed data for User Shadow Registers 
   KTHREAD_MNGR_DATA       KThreadMngrData_X;            ///< Needed data for KThreads manager
	DRV_DATA                DrvData_X;						   ///< Needed data for the PCI board application
   KOBJ_SYNC_EVENT_DATA    KObjSyncEventData_X;          ///< Needed data for the KObjs events
   DRV_POWER_STATE         PowerState_E;                 ///< Needed data for low power state
};

#ifdef WDF_DRIVER
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_EXTENSION, WDFGetDeviceContext)

#endif

#endif // _DRIVER_H_
