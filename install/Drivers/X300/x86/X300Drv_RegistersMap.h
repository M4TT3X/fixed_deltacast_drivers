/********************************************************************************************************************//**
 * @internal
 * @file   	X300Drv_RegistersMap.h
 * @date   	2012/03/15
 * @author 	gt
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2011/01/25  v00.01.0000    cs       Creation of this file
   2011/01/25  v00.01.0001    cs       Add new registers related to DMA Channel 1 (suppored by HD 3D board)

 **********************************************************************************************************************/

#ifndef _X300_REGISTERSMAP_H_
#define _X300_REGISTERSMAP_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "Register.h"

#define X300_DMA_PADDING_SIZE 0x200


#define PCIEBRIDGE_DMA_DBG0_MM32         DEFINE_MMREG32(0,0xB0)  /** DMA Command and Status register */
#define PCIEBRIDGE_DMA_DBG1_MM32         DEFINE_MMREG32(0,0xB8)  /** DMA Command and Status register */
#define PCIEBRIDGE_DMA_DBG2_MM32         DEFINE_MMREG32(0,0xC0)  /** DMA Command and Status register */
#define FPGA_DBG_RD_MM32                 DEFINE_MMREG32(FPGA_PCI_BAR,0x208)


#ifdef DRIVER
#define FPGA_MAILBOX_DI_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x90)
#define FPGA_MAILBOX_ADDR_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x98)
#endif

#define FPGA_GCMD_BOARD_TS_LATCH                      0x00000080 // �1� to latch the current value of  Board TS counter (Auto-clear)
#define FPGA_GCMD_MSK_GENLOCK_B_SOURCE                0x00007000
#define FPGA_GCMD_BIT_GENLOCK_B_ENABLE                0x00008000 //�1� to activate the Genlock feature
#define FPGA_GCMD_BIT_MTG_B_RESET                     0x00010000 //�1� to reset the Master Timing Generator 0
#define FPGA_GCMD_BIT_MTG_B_START                     0x00020000 //�1� to start the Master Timing Generator 0
#define FPGA_GCMD_MSK_GENLOCK_M_SOURCE                0x001C0000
#define FPGA_GCMD_BIT_GENLOCK_M_ENABLE                0x00200000 //�1� to activate the Genlock feature
#define FPGA_GCMD_BIT_MTG_M_RESET                     0x00400000 //�1� to reset the Master Timing Generator 0
#define FPGA_GCMD_BIT_MTG_M_START                     0x00800000 //�1� to start the Master Timing Generator 0

#define FPGA_GCMD_BIT_GENLOCK_B_SOURCE(genlock_source)      ((genlock_source<<12)&FPGA_GCMD_MSK_GENLOCK_B_SOURCE)
#define FPGA_GCMD_BIT_GENLOCK_M_SOURCE(genlock_source)      ((genlock_source<<18)&FPGA_GCMD_MSK_GENLOCK_M_SOURCE)

#define PCIE_LINK_WIDTH_UNKNOWN              0x0   
#define PCIE_LINK_WIDTH_1                    0x1   //'1' = 1x PCIe
#define PCIE_LINK_WIDTH_2                    0x2   //'2' = 2x PCIe
#define PCIE_LINK_WIDTH_4                    0x3   //'3' = 4x PCIe
#define PCIE_LINK_WIDTH_8                    0x4   //'4' = 8x PCIe
#define FPGA_GSTS_GET_BIT_LINK_WIDTH(reg)    ((reg&FPGA_GSTS_MSK_LINK_WIDTH) >>20)

#define FPGA_HDMI_CTL_SHRMM32    DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xA0)
#define FPGA_MTG_B_CFG_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xA8)
#define FPGA_MTG_M_CFG_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xB0)
#define FPGA_VPULSE_OFFSET_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0xB8)
//#define FPGA_BTC                 DEFINE_MMREG32(FPGA_PCI_BAR,0xC0)
#define FPGA_KEYER_CTRL_SHRMM32          DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xC8)
#define FPGA_PP_PARAMS_A       DEFINE_MMREG32(FPGA_PCI_BAR,0xD0)
#define FPGA_PP_PARAMS_D       DEFINE_MMREG32(FPGA_PCI_BAR,0xD8)
#define FPGA_PARAM_ROM_A_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0xE0)
#define FPGA_PARAM_ROM_D_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0xE8)
#define FPGA_SSR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0xF0)
#define FPGA_IMR_RX_SHRMM32      DEFINE_SHRMMREG32(FPGA_PCI_BAR,0xF8)
#define FPGA_ICR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x100)
#define FPGA_RX0_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x108) 
#define FPGA_RX1_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x110) 
#define FPGA_RX2_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x118) 
#define FPGA_RX3_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x120) 
#define FPGA_RX4_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x128) 
#define FPGA_RX5_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x130) 
#define FPGA_RX6_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x138) 
#define FPGA_RX7_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x140) 
#define FPGA_SSR_TX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x148)
#define FPGA_IMR_TX_SHRMM32      DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x150)
#define FPGA_ICR_TX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x158)
#define FPGA_TX0_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x160) 
#define FPGA_TX1_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x168) 
#define FPGA_TX2_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x170) 
#define FPGA_TX3_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x178) 
#define FPGA_TX4_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x180) 
#define FPGA_TX5_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x188) 
#define FPGA_TX6_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x190) 
#define FPGA_TX7_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x198) 
#define FPGA_TX0_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1A0) 
#define FPGA_TX1_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1A8) 
#define FPGA_TX2_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1B0) 
#define FPGA_TX3_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1B8) 
#define FPGA_TX4_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1C0) 
#define FPGA_TX5_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1C8) 
#define FPGA_TX6_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1D0) 
#define FPGA_TX7_CFG_D_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x1D8) 
#define FPGA_CH0_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1E0)
#define FPGA_CH1_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1E8)
#define FPGA_CH2_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1F0)
#define FPGA_CH3_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x1F8)
#define FPGA_CH4_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x200)
#define FPGA_CH5_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x208)
#define FPGA_CH6_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x210)
#define FPGA_CH7_LINE_PAD_MM32   DEFINE_MMREG32(FPGA_PCI_BAR,0x218)
#define FPGA_DMAADVRX_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x1E0)  /** Advanced DMA Settings � PC to FPGA */
#define FPGA_DMAADVTX_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x1E8)  /** Advanced DMA Settings � FPGA to PC */
#define FPGA_CSC_A_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x220)
#define FPGA_CSC_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x228)

//Bar 1 register map (RD)
#define FPGA_SR_MM32             DEFINE_MMREG32(FPGA_PCI_BAR,0x08)
#define FPGA_ISR_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x10)
#define FPGA_REVISION_ID_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x18)
//#define FPGA_DMA_BC_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x28)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x30)
//#define FPGA_DMA1_BC_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x38)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x40)
//#define FPGA_RESERVED_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x48)
//#define FPGA_VCXO_CTL_MM32       DEFINE_MMREG32(FPGA_PCI_BAR,0x50)

#define FPGA_TCE_CMD_SHRMM32     DEFINE_SHRMMREG32(FPGA_PCI_BAR,0x238)
#define FPGA_TCE_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x210)
#define FPGA_LTC_CHANNEL_MM32    DEFINE_MMREG32(FPGA_PCI_BAR,0x238)
#define FPGA_LTC_VALUE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x218)


#ifdef DRIVER
#define FPGA_MAILBOX_DO_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x88)
#endif

//#define FPGA_MTG_CTL_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x90)
#define FPGA_BTC_MM32            DEFINE_MMREG32(FPGA_PCI_BAR,0x98)
#define FPGA_SR_RX_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0xA0)
#define FPGA_ISR_RX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0xA8)
#define FPGA_RXBUFID_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xB0)
#define FPGA_RX0_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xB8) 
#define FPGA_RX1_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xC0) 
#define FPGA_RX2_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xC8) 
#define FPGA_RX3_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xD0) 
#define FPGA_RX4_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xD8) 
#define FPGA_RX5_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xE0) 
#define FPGA_RX6_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xE8) 
#define FPGA_RX7_STS_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0xF0) 
#define FPGA_RX0_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0xF8) 
#define FPGA_RX1_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x100) 
#define FPGA_RX2_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x108) 
#define FPGA_RX3_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x110) 
#define FPGA_RX4_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x118) 
#define FPGA_RX5_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x120) 
#define FPGA_RX6_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x128) 
#define FPGA_RX7_PHASE_MM32      DEFINE_MMREG32(FPGA_PCI_BAR,0x130) 
#define FPGA_FABC0_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x138) 
#define FPGA_FABC0_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x140) 
#define FPGA_FABC1_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x148) 
#define FPGA_FABC1_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x150) 
#define FPGA_FABC2_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x158) 
#define FPGA_FABC2_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x160) 
#define FPGA_FABC3_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x168) 
#define FPGA_FABC3_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x170) 
#define FPGA_FABC4_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x178) 
#define FPGA_FABC4_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x180) 
#define FPGA_FABC5_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x188) 
#define FPGA_FABC5_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x190) 
#define FPGA_FABC6_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x198) 
#define FPGA_FABC6_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1A0) 
#define FPGA_FABC7_C_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1A8) 
#define FPGA_FABC7_Y_MM32        DEFINE_MMREG32(FPGA_PCI_BAR,0x1B0) 

#define FPGA_SR_TX_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x1F8)
#define FPGA_ISR_TX_MM32         DEFINE_MMREG32(FPGA_PCI_BAR,0x200)
#define FPGA_RX0_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x220)
#define FPGA_RX0_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x228) 
#define FPGA_RX1_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x230) 
#define FPGA_RX1_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x238) 
#define FPGA_RX2_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x240) 
#define FPGA_RX2_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x248) 
#define FPGA_RX3_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x250) 
#define FPGA_RX3_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x258) 
#define FPGA_RX4_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x260) 
#define FPGA_RX4_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x268) 
#define FPGA_RX5_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x270) 
#define FPGA_RX5_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x278) 
#define FPGA_RX6_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x280) 
#define FPGA_RX6_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x288) 
#define FPGA_RX7_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x290) 
#define FPGA_RX7_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x298) 
#define FPGA_BRD_TS_MSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x2A0) 
#define FPGA_BRD_TS_LSB_MM32     DEFINE_MMREG32(FPGA_PCI_BAR,0x2A8) 

// -- FPGA_KEYERCTL_MM32 : Chromakeyer control (WO) -------------------------------------------------------------

#define FPGA_KEYERCTL_BIT_CHROMAKEY_EN       0x00000001  /* Set this bit to '1' to activate the Video Processing */
#define FPGA_KEYERCTL_BIT_RX_RGBA2YUV        0x00000002  /* Set this bit to '1' when RXs are in dual link (4:4:4:4; RGBA). Active automatic RGB->YUV */
#define FPGA_KEYERCTL_MSK_RX_ENABLE          0x0000000C
#define FPGA_KEYERCTL_BIT_RX0_ENABLE         0x00000004
#define FPGA_KEYERCTL_BIT_TX_RGBA2YUV        0x00000010  /* Set this bit to '1' to activate RGB->YUV conversion on TX0 (PC) channels */
#define FPGA_KEYERCTL_MSK_FGINPUT_MUX        0x00000060  /* Foreground input mux selection */
#define FPGA_KEYERCTL_BIT_FGINPUT_MUX_RX0    0x00000000  /*    00 : RX0 or coupled RX */
#define FPGA_KEYERCTL_BIT_FGINPUT_MUX_RX1    0x00000020  /*    01 : RX1 */
#define FPGA_KEYERCTL_BIT_FGINPUT_MUX_TX0    0x00000040  /*    10 : TX0 or coupled TX */
#define FPGA_KEYERCTL_BIT_FGINPUT_MUX_TX1    0x00000060  /*    11 : TX1 */
#define FPGA_KEYERCTL_MSK_BGINPUT_MUX        0x00000180  /* Background input mux selection */
#define FPGA_KEYERCTL_BIT_BGINPUT_MUX_RX0    0x00000000  /*    00 : RX0 or coupled RX */
#define FPGA_KEYERCTL_BIT_BGINPUT_MUX_TX0    0x00000100  /*    10 : TX0 or coupled TX */
#define FPGA_KEYERCTL_MSK_KEYINPUT_MUX       0x00000600  /* Key input mux selection */
#define FPGA_KEYERCTL_BIT_KEYINPUT_MUX_RX0   0x00000000  /*    00 : RX0 or coupled RX */
#define FPGA_KEYERCTL_BIT_KEYINPUT_MUX_TX0   0x00000400  /*    10 : TX0 or coupled TX */
#define FPGA_KEYERCTL_BIT_KEY_SHRINK         0x00000800  /* Set this bit to '1' to use key shrink */
#define FPGA_KEYERCTL_MSK_ALPHA_B1_MUX       0x00003000  /* Alpha blender B input mux selection */
#define FPGA_KEYERCTL_BIT_ALPHA_B1_MUX_RX0   0x00000000  /*    00 : RX0 or coupled RX */
#define FPGA_KEYERCTL_BIT_ALPHA_B1_MUX_TX0   0x00002000  /*    10 : TX0 or coupled TX */
#define FPGA_KEYERCTL_BIT_RX_PROGRESSIVE     0x00004000  /* Set this bit to '1' when RX are fed with progressive video */
#define FPGA_KEYERCTL_BIT_INV_KEY            0x00008000  /* Set this bit to '1' to complement the alpha blending key */
#define FPGA_KEYERCTL_BIT_HD                 0x00010000  /* Set this bit to '1' if working in HD, or '0' when working in SD */
#define FPGA_KEYERCTL_BIT_TX0_KEYOUT_EN      0x00020000  /* Set when user is going to transmit key on tx0 after RGBA to YUVK conversion */
#define FPGA_KEYERCTL_BIT_TX0_DUALLINK       0x00080000  /* Set this bit to 1 when tx0 PC stream works in dual link 4:4:4:4; YUVA or RGBA. Keep it reset for 4224 dual link format */
#define FPGA_KEYERCTL_BIT_TX0_4444_KEY       0x00100000  /* Only relevant when FPGA_KEYERCTL_BIT_TX0_KEYOUT_EN=1. Set this bit to 1 when physical tx0 must output key signal of a 4:4:4:4 stream, reset to 0 if tx0 must output key signal of a 4:2:2:4 stream */
#define FPGA_KEYERCTL_MSK_TRS_MUX            0x00C00000  /* TRS source mux selection */
#define FPGA_KEYERCTL_BIT_TRS_MUX_MTG        0x00000000  /*    00 : MTG */
#define FPGA_KEYERCTL_BIT_TRS_MUX_TX0        0x00400000  /*    01 : TX0 */
#define FPGA_KEYERCTL_BIT_RX0_BLACKOUT       0x01000000  /* Set this bit to '1' for blackout forced mode, or '0' for normal mode on RX0 */
#define FPGA_KEYERCTL_BIT_3G                 0x04000000  /* Set this bit to '1' if working in 3G, or '0' when working in HD/SD */
#define FPGA_KEYERCTL_CHROMAKEY_RESET_B      0x80000000  /* Video Processing async Reset (reset when 0) Maintain all process in reset state */

// -- HWPROCESSING_PARAM_BLENDER_MODE
#define HWPROCESSING_PARAM_BLENDER_MODE_BIT_ADDITIVE     0x1   //0 : Alpha blender in multiplicative mode (default mode) - 1 : Alpha blender in additive mode
#define HWPROCESSING_PARAM_BLENDER_MODE_BIT_ALPHA_ON_Y   0x2   //0 : Alpha channel is on alpha path - 1 : Alpha channel is in luminance path
#define HWPROCESSING_PARAM_BLENDER_MIN_CLIP           0x1B
#define HWPROCESSING_PARAM_BLENDER_MAX_CLIP           0x1C
#define HWPROCESSING_PARAM_BLENDER_ONE_OVER_MAX_CLIP  0x1D
#define HWPROCESSING_PARAM_BLENDER_MODE               0x1E

//FPGA_CSC_A_MM32
#define FPGA_CSC_A_MSK_COEF_SEL     0x0000000F
#define FPGA_CSC_A_BIT_CSC_SEL      0x00000010  //�0� = RX0 CSC - �1� = RX1 CSC
#define FPGA_CSC_A_BIT_CSC_SEL2     0x00000020  //�1� = TX0 CSC
#define FPGA_CSC_A_BIT_COEF_SEL(coef)  ((coef) & FPGA_CSC_A_MSK_COEF_SEL)

#define CSC_COEF_K11    0x0
#define CSC_COEF_K12    0x1
#define CSC_COEF_K13    0x2
#define CSC_COEF_K21    0x3
#define CSC_COEF_K22    0x4
#define CSC_COEF_K23    0x5
#define CSC_COEF_K31    0x6
#define CSC_COEF_K32    0x7
#define CSC_COEF_K33    0x8
#define CSC_COEF_O1     0x9
#define CSC_COEF_O2     0xA
#define CSC_COEF_O3     0xB
#define CSC_COEF_C1     0xC
#define CSC_COEF_C2     0xD
#define CSC_COEF_C3     0xE


//FPGA_CSC_D_MM32
#define FPGA_CSC_D_MSK_VALUE   0x0003FFFF //Coefficient value



#define FPGA_CSC_A_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x220)
#define FPGA_CSC_D_MM32          DEFINE_MMREG32(FPGA_PCI_BAR,0x228)



// -- HWPROCESSING_PARAM_OUTPUT_SELECTION
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_MASK                0x07
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_BACK                0x00
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_RX0                 0x01
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_RX1                 0x02
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_CK_MAIN             0x03
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_CK_BLENDED_MAIN     0x04
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_TX0                 0x05
#define HWPROCESSING_OUTPUT_VIDEO_SOURCE_TX1                 0x06

#define HWPROCESSING_OUTPUT_ANC_SOURCE_MASK                  0x07
#define HWPROCESSING_OUTPUT_ANC_SOURCE_BACK                  0x00
#define HWPROCESSING_OUTPUT_ANC_SOURCE_RX0                   0x01
#define HWPROCESSING_OUTPUT_ANC_SOURCE_RX1                   0x02
#define HWPROCESSING_OUTPUT_ANC_SOURCE_TX0                   0x03
#define HWPROCESSING_OUTPUT_ANC_SOURCE_TX1                   0x04

#define HWPROCESSING_OUTPUT_SELECTION_BIT_VIDEO_TXi(idx,source)  (((source)&0x07)<<(8*(idx))) 
#define HWPROCESSING_OUTPUT_SELECTION_BIT_ANC_TXi(idx,source)    (((source)&0x07)<<(3+(8*(idx)))) 

#define HWPROCESSING_OUTPUT_ANC_PRIORITY_MASK                0x03
#define HWPROCESSING_OUTPUT_ANC_PRIORITY_NONE                0x00
#define HWPROCESSING_OUTPUT_ANC_PRIORITY_TX0                 0x01

#define HWPROCESSING_OUTPUT_SELECTION_BIT_VIDEO_TXi(idx,source)  (((source)&0x07)<<(8*(idx)))
#define HWPROCESSING_OUTPUT_SELECTION_BIT_ANC_TXi(idx,source)    (((source)&0x07)<<(3+(8*(idx))))
#define HWPROCESSING_OUTPUT_PRIORITY_BIT_ANC_TXi(idx,source)     (((source)&0x03)<<(6+(8*(idx))))

#define HWPROCESSING_CK_OUTPUT_FOREGROUND             0x00  ///< Specifies foreground as output of CK module
#define HWPROCESSING_CK_OUTPUT_BACKGROUND             0x01  ///< Specifies background as output of CK module

#define HWPROCESSING_PARAM_CK_ENABLE                  0x00
#define HWPROCESSING_PARAM_CK_MAIN_OUT_SELECT         0x10
#define HWPROCESSING_PARAM_CK_MONITOR_OUT_SELECT      0x15

#define HWPROCESSING_PARAM_KCOMPRESSOR                0x19 //???
#define HWPROCESSING_PARAM_ALPHA_TBAR                 0x1A
#define HWPROCESSING_PARAM_OUTPUT_SELECTION           0x1F

#define HWPROCESSING_PARAM_Y_MINMAX_CLIP              0x17
#define HWPROCESSING_PARAM_C_MINMAX_CLIP              0x18


#define FPGA_HD_TXx_HD                 0x00080000  // Set for HD transmission, clear for SD transmission

//FPGA_GCMD_SHRMM32
// -> see Board.h


//FPGA_HDMI_CTL
#define FPGA_HDMI_CTL_BIT_ENABLE                      0x00000001

#define FPGA_HDMI_CTL_MASK_SEL                        0x00000006
#define FPGA_HDMI_CTL_SEL_RX0                         0x00000000
#define FPGA_HDMI_CTL_SEL_TX0                         0x00000001
#define FPGA_HDMI_CTL_SEL_RX1                         0x00000002
#define FPGA_HDMI_CTL_SEL_TX1                         0x00000003

#define FPGA_HDMI_CTL_SEL_04_TX0                      0x00000000
#define FPGA_HDMI_CTL_SEL_04_TX1                      0x00000001
#define FPGA_HDMI_CTL_SEL_04_TX2                      0x00000002
#define FPGA_HDMI_CTL_SEL_04_TX3                      0x00000003

#define FPGA_HDMI_CTL_BIT_SEL(sel)                ((sel<<1)&FPGA_HDMI_CTL_MASK_SEL)





//FPGA_GSTS_MM32
// -> see Board.h


#define GENLOCK_SOURCE_FREERUN      0x0
#define GENLOCK_SOURCE_RX0          0x1   // RX0 is taken as Genlock source
#define GENLOCK_SOURCE_RX1          0x2   // RX1 is taken as Genlock source     
#define GENLOCK_SOURCE_RX2          0x3   // RX2 is taken as Genlock source     
#define GENLOCK_SOURCE_RX3          0x4   // RX3 is taken as Genlock source     
#define GENLOCK_SOURCE_BLACK_BURST  0x5   // Black burst is taken as Genlock source
#define GENLOCK_SOURCE_RX4          0x6   // RX4 is taken as Genlock source     
#define GENLOCK_SOURCE_RX5          0x7   // RX5 is taken as Genlock source     

//FPGA_SSR_MM32, FPGA_IMR_SHRMM32, FPGA_ISR_MM32 and FPGA_ISR_MM32
// -> see Board.h


//FPGA_VPULSE_OFFSET_MM32
#define FPGA_VPULSE_OFFSET_BIT_MTG_M  0x80000000 /* '0' = MTG B - '1' = MTG M */

//MAILBOX_DATA indirect address
//see Board.h

//Cmd Tag
//->see also Board.h
#define MB_GENLOCK_0_ENABLE               0x00010002   /* Enable the genlock 0 */
#define MB_GENLOCK_1_ENABLE               0x00010003   /* Enable the genlock 1 */
#define MB_GENLOCK_0_DISABLE              0x00000004   /* Disable the genlock 0 (freerun operation) */
#define MB_GENLOCK_1_DISABLE              0x00000005   /* Disable the genlock 1 (freerun operation) */
#define MB_GENLOCK_0_GET_INPUT_FORMAT     0x00000006   /* Video format at the genlock 0�s input */
#define MB_GENLOCK_1_GET_INPUT_FORMAT     0x00000007   /* Video format at the genlock 1�s input */
#define MB_GENLOCK_0_SET_VCXO_VOLTAGE     0x00010008   /* Set the voltage of the VCXO attached to genlock 0  */
#define MB_GENLOCK_1_SET_VCXO_VOLTAGE     0x00010009   /* Set the voltage of the VCXO attached to genlock 1  */
#define MB_START_HDMI                     0x0002000B   /* Start HDMI monitoring*/
#define MB_STOP_HDMI                      0x0000000C   /* Stop HDMI monitoring*/
#define MB_GET_HDMI_STS                   0x0000000D   /* Get HDMI status*/
#define MB_READ_HDMI_EDID                 0x0000000E   /* Read HDMI EDID*/
#define MB_SFP_WRITE(NbReg)               (((2+((NbReg)+sizeof(UWORD)-1)/sizeof(UWORD))<< 16) | 0x0010)
#define MB_SFP_READ	    	               0x00020011
#define MB_SFP_AUTHORIZE            	   0x000A0012
#define MB_SFP_SET_TABLE(NbEntry)         (((2+((NbEntry)*2+sizeof(UWORD)-1)/sizeof(UWORD))<< 16) | 0x0013)
#define MB_SFP_ACK    	                  0x00010014

// HDMI status
#define HDMISTS_HOTPLUG_DETECTED       0x00000100
#define HDMISTS_CORRECTLY_IDENTIFIED   0x00000200

//FPGA_END_OF_DMA_MM32
#define FPGA_EOD_RXx(i)                (1<<(i))
#define FPGA_EOD_TXx(i)                (1<<(8+(i)))

//FPGA_SSR_RX_MM32, FPGA_IMR_RX_SHRMM32, FPGA_ISR_RX_MM32 and FPGA_ISR_RX_MM32
#define FPGA_SR_RX_BIT_RXi_PARITY(i)        (0x1<<((i)*4))
#define FPGA_SR_RX_BIT_RXi_OVERRUN(i)       (0x2<<((i)*4))
#define FPGA_SR_RX_BIT_RXi_STS_CHANGE(i)    (0x4<<((i)*4))
#define FPGA_SR_RX_BIT_RXi(i)               (0x8<<((i)*4))

#define FPGA_SR_TX_BIT_TXi_UNDERRUN(i)      (0x2<<((i)*4))
#define FPGA_SR_TX_BIT_TXi(i)               (0x8<<((i)*4))


//FPGA_RXx_MM32
#define FPGA_RXx_MSK_CAPTURE_MODE            0x0000000F
#define FPGA_RXx_BIT_CAPTURE_MODE_ENABLED    0x00000001
#define FPGA_RXx_BIT_CAPTURE_MODE_VIDEO      0x00000002
#define FPGA_RXx_BIT_CAPTURE_MODE_ANC        0x00000004
#define FPGA_RXx_BIT_CAPTURE_MODE_RAW        0x00000008
#define FPGA_RXx_MSK_PACKING_MODE            0x000001f0
#define FPGA_RXx_BIT_SD_MODE                 0x00000200
#define FPGA_RXx_BIT_VBI_10BIT               0x00000400
#define FPGA_RXx_BIT_1080P_DL                0x00000800
#define FPGA_RXx_BIT_FIELD_MODE              0x00001000
#define FPGA_RXx_BIT_LINK_B                  0x00002000
#define FPGA_RXx_BIT_CSC_ENABLE              0x00004000
#define FPGA_RXx_BIT_GRAPHIC_PADDING         0x00008000  //�1� to enable Graphic Padding (each new line start on a 64B boundary)
#define FPGA_RXx_MSK_CSC_MODE                0x00010000
#define FPGA_RXx_BIT_VIDEO_SND_PATH          0x00020000 
#define FPGA_RXx_BIT_MULTI_MODE              0x00400000
#define FPGA_RXx_BIT_MULTI_MODE_SEL          0x00800000

#define FPGA_RXx_BIT_CSC_MODE(mode)      (((mode)<<16)&FPGA_RXx_MSK_CSC_MODE)
#define CSC_MODE_AVERAGING            0x0 //�0� to convert 4:2:2 to 4:4:4 by averaging
#define CSC_MODE_DUPLICATING          0x1 //�1� to convert 4:2:2 to 4:4:4 by duplicating

#define FPGA_RXx_BIT_PACKING_MODE(pkg)      (((pkg)<<4)&FPGA_RXx_MSK_PACKING_MODE)

#define PACKING_V208     0x00000000  // 4:2:2:0  8-bit YUV  video packing 
#define PACKING_AV208    0x00000002  // 4:2:2:4  8-bit YUVA video packing 
#define PACKING_V210     0x00000001  // 4:2:2:0 10-bit YUV  video packing 
#define PACKING_AV210    0x00000003  // 4:2:2:4 10-bit YUVA video packing 
#define PACKING_V408     0x00000004  // 4:4:4:0  8-bit YUV  video packing 
#define PACKING_AV408    0x00000006  // 4:4:4:4  8-bit YUVA video packing 
#define PACKING_V410     0x00000005  // 4:4:4:0 10-bit YUV  video packing 
#define PACKING_AV410    0x00000007  // 4:4:4:4 10-bit YUVA video packing 
#define PACKING_C408     0x0000000C  // 4:4:4:0  8-bit RGB  video packing 
#define PACKING_AC408    0x0000000E  // 4:4:4:4  8-bit RGBA video packing 

//FPGA_TXx_MM32
#define FPGA_TXx_MSK_OUTPUT_MODE       0x0000000f  // Output Mode
#define FPGA_TXx_BIT_CLAMPING          0x00000010  // Enable clamping
#define FPGA_TXx_ARM_FOR_PRELOAD       0x00000020  // Arm the channel allowing preloading frames on-board
#define FPGA_TXx_BIT_FIELD_MODE        0x00000040  // �1� to use New Field as New Buffer Interrupt source instead of New Picture
#define FPGA_TXx_BIT_MULTI_MODE_SEL     0x00000080  // '0' = 3D - '1' = 4K
#define FPGA_TXx_MSK_PACKING           0x00000F00  // Packing scheme
#define FPGA_TXx_BIT_1080P_DL          0x00001000  // �1� to enable stream of 1080p Dual Link Stream
#define FPGA_TXx_BIT_KEY_NEUTRAL_CHROMA 0x00002000 // '0' = Chroma values of Key stream are equal to Fill ones - '1' = Chroma values of Key stream are forced to Neutral value (512 on 10-bit)
#define FPGA_TXx_BIT_IOPROC_DISABLE    0x00004000  // Disable Serializer IO Processing
#define FPGA_TXx_MSK_PADDING_SIZE      0x000f8000  // Nbr of pixels into graphic line padding
#define FPGA_TXx_BIT_RX_ON_TX(delay)   ((1<<23)|((delay)<<20))
#define FPGA_TXx_BIT_MTG_M              0x01000000  // '0' = MTGB - '1' = MTGM
#define FPGA_TXx_BIT_MULTI_MODE        0x02000000  // '0' = Normal mode - '1'= 3D mode/4k
#define FPGA_TXx_MSK_SAMPLE_RATE       0x0C000000  // 00 = HD-SDI (including dual link) - 01 = SD-SDI - 10 = 3G-SDI(level A and level B) - 11 = Invalid
#define FPGA_TXx_BIT_SMPTE352M_ENABLE  0x10000000  // Enable automatic SMPTE352M packet insertion on HDe board
#define FPGA_TXx_BIT_GENLOCK_ENABLE    0x20000000  // Enable TXx Genlocking
#define FPGA_TXx_BIT_3G_LEVELB         0x40000000  // Set to 1 to activate level B on TX0
#define FPGA_TXx_BIT_VTG_RST_N         0x80000000  // Reset the address pointer and all counter

#define FPGA_TXx_BIT_PADDING_SIZE(size)   (((size)<<15)&FPGA_TXx_MSK_PADDING_SIZE)

#define FPGA_TXx_BIT_OUTPUT_MODE(mode)       ((mode)&FPGA_TXx_MSK_OUTPUT_MODE)
#define FPGA_TXx_GET_BIT_OUTPUT_MODE(reg)   ((reg)&FPGA_TXx_MSK_OUTPUT_MODE)
#define TX_OUTPUT_MODE_DISABLED        0x0
#define TX_OUTPUT_MODE_VIDEO_ANC       0x1
#define TX_OUTPUT_MODE_COLOR_BAR       0x2
#define TX_OUTPUT_MODE_VIDEO           0x3
#define TX_OUTPUT_MODE_ANC             0x5
#define TX_OUTPUT_MODE_RAW             0x7

#define FPGA_TXx_BIT_PACKING(packing_mode)  ((packing_mode)<<8) & FPGA_TXx_MSK_PACKING
#define FPGA_TXx_GET_BIT_PACKING(reg)       (((reg)& FPGA_TXx_MSK_PACKING) >> 8)
#define TX_PACKING_V208     PACKING_V208   // 4:2:2:0  8-bit YUV  video packing 
#define TX_PACKING_AV208    PACKING_AV208  // 4:2:2:4  8-bit YUVA video packing 
#define TX_PACKING_V210     PACKING_V210   // 4:2:2:0 10-bit YUV  video packing 
#define TX_PACKING_AV210    PACKING_AV210  // 4:2:2:4 10-bit YUVA video packing 
#define TX_PACKING_V408     PACKING_V408   // 4:4:4:0  8-bit YUV  video packing 
#define TX_PACKING_AV408    PACKING_AV408  // 4:4:4:4  8-bit YUVA video packing 
#define TX_PACKING_V410     PACKING_V410   // 4:4:4:0 10-bit YUV  video packing 
#define TX_PACKING_AV410    PACKING_AV410  // 4:4:4:4 10-bit YUVA video packing 
#define TX_PACKING_C408     PACKING_C408   // 4:4:4:0  8-bit RGB  video packing 
#define TX_PACKING_AC408    PACKING_AC408  // 4:4:4:4  8-bit RGBA video packing 

#define FPGA_TXx_BIT_SAMPLE_RATE(tx_mode)        ((tx_mode)<<26) & FPGA_TXx_MSK_SAMPLE_RATE
#define FPGA_TXx_GET_BIT_SAMPLE_RATE(reg)        (((reg)& FPGA_TXx_MSK_SAMPLE_RATE) >> 26)
#define TX_MODE_HD                    0x00        // HD stream
#define TX_MODE_SD                    0x01        // SD stream
#define TX_MODE_3G                    0x02        // 3G stream
#define TX_MODE_INVALID               0x03        // Invalid

#define PCIE_LINK_WIDTH_UNKNOWN        0x0   
#define PCIE_LINK_WIDTH_1              0x1
#define PCIE_LINK_WIDTH_2              0x2
#define PCIE_LINK_WIDTH_4              0x3


//FPGA_RXBUFID_MM32
#define FPGA_RXBUFID_MSK_RXi(i)              (0x7<<((i)*3))       
#define FPGA_RXBUFID_GET_BIT_RXi(reg,i)      (((reg)>>((i)*3))&0x7)


//FPGA_RXx_STS_MM32
#define FPGA_RXx_STS_MSK_SAMPLING_STRUCT         0x0000000F     
#define FPGA_RXx_STS_BIT_ASPECT_RATIO            0x00000010     //'0' = 4/3 - '1' = 16/9
#define FPGA_RXx_STS_MSK_BIT_DEPTH               0x00000060     
#define FPGA_RXx_STS_BIT_PsF                     0x00000080     //�0� = Normal Stream - �1� = Progressive Segmented-Frame Stream
#define FPGA_RXx_STS_BIT_VPID_VALID_B            0x00000100     //�1� when SMPTE 352M packet found on stream B
#define FPGA_RXx_STS_BIT_CRC_ERR_B               0x00000200     //�1� when a CRC error occurs on stream B
#define FPGA_RXx_STS_BIT_PROGRESSIVE             0x00000400     //�0� = Interlaced Transport Stream - �1� = Progressive Transport Stream (**)
#define FPGA_RXx_STS_MSK_RATE                    0x00007800     //(**)
#define FPGA_RXx_STS_BIT_LEVELB                  0x00008000     //�1� when the input signal is level B (only valid in 3G) (*)
#define FPGA_RXx_STS_MSK_FAMILY                  0x000f0000     //(**)
#define FPGA_RXx_STS_BIT_CARRIER_DETECT_N        0x00100000     //�0� = Equalizer Carrier Detected - �1� = Equalizer Carrier NOT Detected
#define FPGA_RXx_STS_MSK_MODE                    0x00600000     //(*)
#define FPGA_RXx_STS_BIT_US                      0x00800000     //�0� = Eu - �1� = Us (*)
#define FPGA_RXx_STS_BIT_ALIGNED                 0x01000000     //�1� when RX(i) and RX(i+1) are aligned (with i = 0, 2, 4 or 6)
#define FPGA_RXx_STS_BIT_VPID_VALID_A            0x02000000     //�1� when SMPTE 352M packet found on stream A
#define FPGA_RXx_STS_BIT_CRC_ERR_A               0x04000000     //�1� when a CRC error occurs on stream A
#define FPGA_RXx_STS_BIT_MODE_LOCKED             0x08000000     //�1� when Mode Status are valid (*)
#define FPGA_RXx_STS_BIT_FORMAT_LOCKED           0x10000000     //�1� when Format Status are valid (**)
#define FPGA_RXx_STS_BIT_SMPTE_STREAM            0x20000000     //�1� when stream is compliant with SMPTE standard (Hardcoded)
#define FPGA_RXx_STS_BIT_DUAL_LINK               0x40000000     //�1� when stream is a 1080p on a Dual Link
#define FPGA_RXx_STS_BIT_ASSIGNMENT              0x80000000     //�0� for the first link (A) and to �1� for the second link (B) in a DL stream

#define FPGA_RXx_STS_GET_SAMPLING_STRUCT(reg)   ((reg)&FPGA_RXx_STS_MSK_SAMPLING_STRUCT)
#define SS_YCbCr_4220                       0x0
#define SS_YCbCr_4440                       0x1
#define SS_RGB_4440                         0x2
#define SS_YCbCr_4200                       0x3
#define SS_YCbCrK_4224                      0x4
#define SS_YCbCrK_4444                      0x5
#define SS_RGBA_4444                        0x6

#define FPGA_RXx_STS_GET_BIT_DEPTH(reg)   (((reg)&FPGA_RXx_STS_MSK_BIT_DEPTH) >> 5)
#define BIT_DEPTH_8                         0x0
#define BIT_DEPTH_10                        0x1
#define BIT_DEPTH_12                        0x2

#define FPGA_RXx_STS_GET_BIT_RATE(reg)   (((reg)&FPGA_RXx_STS_MSK_RATE) >> 11)
#define RATE_23_98_HZ                        0x2
#define RATE_24_HZ                           0x3
#define RATE_47_95_HZ                        0x4
#define RATE_25_HZ                           0x5
#define RATE_29_97_HZ                        0x6
#define RATE_30_HZ                           0x7
#define RATE_48_HZ                           0x8
#define RATE_50_HZ                           0x9
#define RATE_59_94_HZ                        0xA
#define RATE_60_HZ                           0xB

#define FPGA_RXx_STS_GET_BIT_FAMILY(reg)   (((reg)&FPGA_RXx_STS_MSK_FAMILY) >> 16)
#define FAMILY_1920x1080_SMPTE274            0x0
#define FAMILY_1280x720_SMPTE296             0x1
#define FAMILY_2048x1080_SMPTE2048           0x2
#define FAMILY_1920x1080_SMPTE295            0x3
#define FAMILY_NTSC                          0x8
#define FAMILY_PAL                           0x9

#define FPGA_RXx_STS_GET_BIT_MODE(reg)   (((reg)&FPGA_RXx_STS_MSK_MODE) >> 21)
#define MODE_HD                              0x0
#define MODE_SD                              0x1
#define MODE_3G                              0x2

//FPGA_FABCx_x_MM32
#define FPGA_FABCx_x_MSK_ANC_SIZE            0x87FFFFFF
#define FPGA_FABCx_x_MSK_CAPTURED_DATA       0x78000000
#define FPGA_FABCx_x_BIT_VIDEO               0x08000000
#define FPGA_FABCx_x_BIT_ANC                 0x10000000
#define FPGA_FABCx_x_BIT_RAW                 0x20000000
#define FPGA_FABCx_x_BIT_VIDEO_SND_PATH      0x40000000

#define FPGA_FABCx_x_GET_ANC_SIZE(reg)       (((reg)&FPGA_FABCx_x_MSK_ANC_SIZE))
#define FPGA_FABCx_x_GET_CAPTURED_DATA(reg)  (((reg)&FPGA_FABCx_x_MSK_CAPTURED_DATA)>>27)
#define FABC_CAPTURE_MODE_VIDEO              0x1
#define FABC_CAPTURE_MODE_ANC                0x2
#define FABC_CAPTURE_MODE_RAW                0x4
#define FABC_CAPTURE_MODE_VIDEO_SND_PATH     0x8
//#define RX_CAPTURE_MODE_DISABLED             0x0

//SW Board ID
#define SW_BRDID_MSK_SUB_PRODUCT_ID              0x000000ff
#define SW_BRDID_MSK_PRODUCT_ID                  0x0000ff00
#define SW_BRDID_MSK_NB_RX_CHN                   0x000f0000
#define SW_BRDID_MSK_NB_TX_CHN                   0x00f00000
#define SW_BRDID_MSK_PRODUCT_FAMILY              0xff000000

#define SW_BRDID_BIT_SUB_PRODUCT_ID(SubProductId)  ((SubProductId)&SW_BRDID_MSK_SUB_PRODUCT_ID)
#define SW_BRDID_GET_BIT_SUB_PRODUCT_ID(reg)       ((reg)&SW_BRDID_MSK_SUB_PRODUCT_ID)
#define SW_BRDID_BIT_PRODUCT_ID(ProductId)         (((ProductId)<<8)&SW_BRDID_MSK_PRODUCT_ID)
#define SW_BRDID_GET_BIT_PRODUCT_ID(reg)           (((reg)&SW_BRDID_MSK_PRODUCT_ID)>>8)
#define SW_BRDID_BIT_NB_RX_CHN(NbRxChn)            (((NbRxChn)<<16)&SW_BRDID_MSK_NB_RX_CHN)
#define SW_BRDID_GET_BIT_NB_RX_CHN(reg)            (((reg)&SW_BRDID_MSK_NB_RX_CHN)>>16)
#define SW_BRDID_BIT_NB_TX_CHN(NbTxChn)            (((NbTxChn)<<20)&SW_BRDID_MSK_NB_TX_CHN)
#define SW_BRDID_GET_BIT_NB_TX_CHN(reg)            (((reg)&SW_BRDID_MSK_NB_TX_CHN)>>20)
#define SW_BRDID_BIT_PRODUCT_FAMILY(ProductFamily) (((ProductFamily)<<24)&SW_BRDID_MSK_PRODUCT_FAMILY)
#define SW_BRDID_GET_BIT_PRODUCT_FAMILY(reg)       (((reg)&SW_BRDID_MSK_PRODUCT_FAMILY)>>24)



//HW Board ID
#define HW_BRDID_MSK_CHN_RX_ENABLE               0x000000ff
#define HW_BRDID_MSK_CHN_TX_ENABLE               0x0000ff00
#define HW_BRDID_BIT_HDMI                        0x00010000
#define HW_BRDID_BIT_ARM                         0x00020000
#define HW_BRDID_BIT_BASEBOARD_GNLK              0x00040000
#define HW_BRDID_BIT_MEZZA_GNLK                  0x00080000
#define HW_BRDID_BIT_OSC_148                     0x00100000
#define HW_BRDID_BIT_BIDIR                       0x00200000
#define HW_BRDID_BIT_FLASH_SIZE                  0x00400000
#define HW_BRDID_BIT_BASEBOARD_BLACKBURST        0x00800000
#define HW_BRDID_BIT_MEZZA_BLACKBURST            0x01000000
#define HW_BRDID_MSK_VIDEO_STANDARD              0x06000000
#define HW_BRDID_BIT_MEZZA                       0x08000000
#define HW_BRDID_BIT_KEYER0                      0x10000000
#define HW_BRDID_BIT_KEYER1                      0x20000000
#define HW_BRDID_BIT_RELAY                       0x40000000

#define HW_BRDID_BIT_CHN_RX_ENABLE(ChnRxEnable)  ((ChnRxEnable)&HW_BRDID_MSK_CHN_RX_ENABLE)
#define HW_BRDID_BIT_CHN_TX_ENABLE(ChnTxEnable)  (((ChnTxEnable)<<8)&HW_BRDID_MSK_CHN_TX_ENABLE)
#define HW_BRDID_BIT_VIDEO_STANDARD(VidStd)      (((VidStd)<<25)&HW_BRDID_MSK_VIDEO_STANDARD)
#define VIDSTD_SD                0x0
#define VIDSTD_HD                0x1
#define VIDSTD_3G                0x3

#define AUT0RIZATION_DODE        0x4443
#define BRANDING_DC              0x0

//VBI config
#define VBICONF_MAX_NBOF_CAPTURE_LINE                    64
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_MSK_LINE      0x07FF
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_BIT_CAPT_C    0x0800
#define FPGA_PARAM_ROM_D_VALUE_RXx_VBICONF_BIT_CAPT_Y    0x1000

#define FPGA_SR_BIT_TX_EMPTY     0x00100000 
#define FPGA_SR_BIT_RX_FULL      0x00200000


//SFP : private types
#define SFP_MAX_NBOF_MODULE                  2
#define SFP_MAX_NBOF_MON_REG                 248
#define SFP_REFRESH_TIME_MIN_MAX             0xffff
#define SFP_SERIALID_SIZE                    128

#define SFP_NO_MOD                           ((ULONGLONG)1<<63)
#define SFP_NEW_MOD                          ((ULONGLONG)1<<62)
#define SFP_AUTH_BY_DFLT                     0x1
#define SFP_VALID_AUTH_KEY                   0x2
#define SFP_NEW_VAL                          (((ULONGLONG)1<<(SFP_MAX_NBOF_MON_REG/4))-1)

#define SFP_MB_SFPx_ENABLE(x)                (((x)==0)?0x17C:0x17E)
#define SFP_MB_SFPx_DISABLE(x)               (((x)==0)?0x17D:0x17F)
#define SFP_MB_SFPx_REG_VAL(x)               (((x)==0)?0x180:0x1C0)
#define SFP_MB_SFPx_SERIAL_ID(x)             SFP_MB_SFPx_REG_VAL(x)
#define SFP_MB_SFPx_CHG_MSK(x)               (SFP_MB_SFPx_REG_VAL(x)+SFP_MAX_NBOF_MON_REG/4)


//I2C address
#define SERIAL_ID_ADDR           0xA0
#define SWITCH_I2C_ADDR          0xE0

//Register address
#define SERIAL_ID_VENDOR_PN_ADDR    40
#define SERIAL_ID_VENDOR_PN_SIZE    16
#define SERIAL_ID_CC_EXT_ADDR       95

#define CHALLENGE_DELAY             2000     //minimum time between 2 challenges (to avoid brut-force attack)

// -- FPGA_TCE_CMD_MM32 : TCE Command Register W -------------------------------------------------------------

#define FPGA_TCE_CMD_GPIO_DIRECTION             0x0000000F  // Gpios direction (1 bit per Gpio) : �0�: input - �1�: output
#define FPGA_TCE_CMD_GPIO_DIRECTION_GET_BIT(i)  (FPGA_TCE_CMD_GPIO_DIRECTION&(0x01<<i))

#define FPGA_TCE_CMD_GPIO_LEVEL                 0x000000F0  // Gpios level (1 bit per Gpio): �0�: low - �1�: high (valid only if the concerned Gpios direction is output)
#define FPGA_TCE_CMD_GPIO_LEVEL_GET_BIT(i)      ((FPGA_TCE_CMD_GPIO_LEVEL)&(0x10<<i))

#define FPGA_TCE_CMD_GPIO_TRISTATE              0x00000100  // �1� to put all the Gpios in tristate mode
#define FPGA_TCE_CMD_RELAY_CONTROL              0x00000200  // Relay control of the A-LTC card:�0�: IO(4) is looped to IO(5) - �1� IO(4) is looped to IO(6)

// -- FPGA_TCE_STS_MM32 : TCE Status Register R -------------------------------------------------------------

#define FPGA_TCE_STS_GPIO_LEVEL                 0x0000000F  // Gpios level (1 bit per Gpio): �0�: low - �1�: high (valid only if the concerned Gpios direction is input)
#define FPGA_TCE_STS_GPIO_LEVEL_GET_BIT(i)      (FPGA_TCE_STS_GPIO_LEVEL&(0x1<<i))

#define FPGA_TCE_STS_RELAY_STATUS      0x00000001  // Relay status of the A-LTC card:�0�: IO(4) is looped to IO(5) - �1� IO(4) is looped to IO(6)


#endif // _X300DRV_REGISTERSMAP_H_

