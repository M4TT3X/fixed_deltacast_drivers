/********************************************************************************************************************//**
 * @internal
 * @file   	X3XXBoardTypes.h
 * @date   	2013/04/05
 * @author 	ja
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2013/04/05  v00.01.0000    ja       Creation of this file

 **********************************************************************************************************************/

#ifndef _X3XXBOARDTYPES_H_
#define _X3XXBOARDTYPES_H_

typedef enum _X3XX_PCIDEVID{
   X3XX_PCIDEVID_FAILSAFE=0x0001,
   X3XX_PCIDEVID_DEBUG=0x0003,
   X3XX_PCIDEVID_HD80=0x0004,
   X3XX_PCIDEVID_3G40=0x0005,
   X3XX_PCIDEVID_HD44=0x0006,
   X3XX_PCIDEVID_3G22=0x0007,
   X3XX_PCIDEVID_3G04=0x0008,
   X3XX_PCIDEVID_HD62=0x0009,
   X3XX_PCIDEVID_HDMI_FAILSAFE=0x000A,
   X3XX_PCIDEVID_HDMI=0x000B,
   X3XX_PCIDEVID_KEY=0x000C,
   X3XX_PCIDEVID_SFP22=0x0010,
   X3XX_PCIDEVID_3G40_HD40=0x000D,
   X3XX_PCIDEVID_K7_FAILSAFE=0x011,
   X3XX_PCIDEVID_3G4C=0x012,
   X3XX_PCIDEVID_3G8C=0x013,
   X3XX_PCIDEVID_3GKEY=0x014,
}X3XX_PCIDEVID;

typedef enum _X3XX_PRODUCT_FAMILY
{
   X3XX_PRODUCT_FAMILY_HD,
   X3XX_PRODUCT_FAMILY_3G,
   X3XX_PRODUCT_FAMILY_SD,
   X3XX_PRODUCT_FAMILY_HDMI,
   X3XX_PRODUCT_FAMILY_KEY,
   X3XX_PRODUCT_FAMILY_FLEX,
   NBOF_X3XX_PRODUCT_FAMILY
}X3XX_PRODUCT_FAMILY;

typedef enum _X3XX_COMPANION_CARD_TYPE
{
   X3XX_LTC_COMPANION_CARD=0,          /*! A-LTC companion card*/
   NB_X3XX_COMPANION_CARD_TYPE
}X3XX_COMPANION_CARD_TYPE;

typedef enum _X3XX_TIMECODE_SOURCE
{
   X3XX_TC_SRC_LTC_COMPANION_CARD,     /*! A-LTC companion card timecode source*/
   NB_X3XX_TC_SRC
}X3XX_TIMECODE_SOURCE;

typedef enum _X3XX_TIMECODE_FRAMERATE
{
   X3XX_TC_30_FPS=0x01,     /*! A-LTC companion card framerate 30 FPS*/
   X3XX_TC_25_FPS,          /*! A-LTC companion card framerate 25 FPS*/
   X3XX_TC_24_FPS,          /*! A-LTC companion card framerate 24 FPS*/
   NB_X3XX_TC_FRAMERATE
}X3XX_TIMECODE_FRAMERATE;

#define DELTACAST_VENDID   0x1B66
#define FW_START_ADDRESS   0

#define X3XXBRD_MAILBOX   0
#define X3XXBRD_PCB_VER_BATCH_SIGNATURE 14165482

#endif // _X3XXBOARDTYPES_H_
