/********************************************************************************************************************//**
 * @file   	StreamCallBacks.h
 * @date   	2010/06/10
 * @author 	cs/gt
 * @version v00.01.0000 
 * @brief  
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/10   v00.01.0000     cs/gt    Creation of this file

 **********************************************************************************************************************/

#ifndef _STREAMCALLBACKS_H_
#define _STREAMCALLBACKS_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "SkelCallBacks.h"
#include "CommonBufMngrTypes.h"
#include "DMAMngr.h"
#include "BufMngr.h"
//#include "MailboxMngr.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
typedef enum
{
   INT_SRC_DMA_CHN0,
   INT_SRC_DMA_CHN1,
   INT_SRC_DMA_CHN2,
   INT_SRC_DMA_CHN3,
   INT_SRC_RX0,                 
   INT_SRC_RX1,                
   INT_SRC_RX2,                
   INT_SRC_RX3,                 
   INT_SRC_RX4,                 
   INT_SRC_RX5,                
   INT_SRC_RX6,                
   INT_SRC_RX7, 
  
   INT_SRC_OVERRUN_RX0,      
   INT_SRC_OVERRUN_RX1,      
   INT_SRC_OVERRUN_RX2,      
   INT_SRC_OVERRUN_RX3,      
   INT_SRC_OVERRUN_RX4,      
   INT_SRC_OVERRUN_RX5,      
   INT_SRC_OVERRUN_RX6,      
   INT_SRC_OVERRUN_RX7,      

   INT_SRC_STS_CHANGE_RX0,      
   INT_SRC_STS_CHANGE_RX1,      
   INT_SRC_STS_CHANGE_RX2,      
   INT_SRC_STS_CHANGE_RX3,      
   INT_SRC_STS_CHANGE_RX4,      
   INT_SRC_STS_CHANGE_RX5,      
   INT_SRC_STS_CHANGE_RX6,      
   INT_SRC_STS_CHANGE_RX7,  

   INT_SRC_TX0,                 
   INT_SRC_TX1,                
   INT_SRC_TX2,                
   INT_SRC_TX3,
   INT_SRC_TX4,
   INT_SRC_TX5,
   INT_SRC_TX6,
   INT_SRC_TX7,

   INT_SRC_UNDERRUN_TX0,
   INT_SRC_UNDERRUN_TX1,
   INT_SRC_UNDERRUN_TX2,
   INT_SRC_UNDERRUN_TX3,
   INT_SRC_UNDERRUN_TX4,
   INT_SRC_UNDERRUN_TX5,
   INT_SRC_UNDERRUN_TX6,
   INT_SRC_UNDERRUN_TX7,

   NBOF_INT_SRC
}INT_SRC;


typedef struct 
{
   void              (*CallBack_PreInit_fct)(PDEVICE_EXTENSION pdx);   ///< This callback is called before the CallBack_Init_fct() allowing setting some stuffs related to a detected board
   BOARD_INIT_STATUS (*CallBack_DeviceAdd_fct)(PDEVICE_EXTENSION pdx);
   BOARD_INIT_STATUS (*CallBack_Wakeup_fct)(PDEVICE_EXTENSION pdx);
   void              (*CallBack_Sleep_fct)(PDEVICE_EXTENSION pdx, BOOL32 PowerOff_B);
	void              (*CallBack_DeviceRemove_fct)(PDEVICE_EXTENSION pdx);
	BOOL32            (*CallBack_ISR_fct)(PDEVICE_EXTENSION pdx,ULONG *pIntSources_UL);
	void              (*CallBack_DPCforISR_fct)(PDEVICE_EXTENSION pdx, ULONG DPCIntSource_UL);
	BOOL32            (*CallBack_Open_fct)(PDEVICE_EXTENSION pdx, ULONG RefCount_UL,PID_TYPE PID_UL);
	BOOL32            (*CallBack_Close_fct)(PDEVICE_EXTENSION pdx, ULONG RefCount_UL,PID_TYPE PID_UL);
	IOCTL_STATUS      (*CallBack_IoCtl_fct)(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL, PID_TYPE PID_UL);
   void              (*CallBack_CleanUp_fct)(PDEVICE_EXTENSION pdx, PID_TYPE PID_UL, BOOL32 DevicePresent_B); ///< Callback called during cleanup (pre/process and general)
   BOOL32            (*CallBack_CheckResources_fct)(PDEVICE_EXTENSION pdx);
   void              (*CallBack_ReleasePermanentResources_fct)(PDEVICE_EXTENSION pdx);


	void              (*CallBack_PropMngrPropertiesInit)(ULONG *pDefaultProperties_UL);

	BOOL32            (*CallBack_DMAMngrDMAWakeup_fct)  (PDEVICE_EXTENSION pdx , ULONG Channel_UL, COMMON_BUFFER_DESCR *);  ///< Defines a pointer to a device specific DMA initialization function
	void              (*CallBack_DMAMngrDMADone_fct)  (PDEVICE_EXTENSION, ULONG Channel_UL, BOOL32 *pSplitPart_B, TRANSLATED_DMA_REQUEST *);               ///< Defines a pointer to a device specific DMA done handler function
	BOOL32            (*CallBack_DMAMngrDMAStart_fct) (PDEVICE_EXTENSION, ULONG Channel_UL, TRANSLATED_DMA_REQUEST *);               ///< Defines a pointer to a device specific DMA Start function
   void              (*CallBack_DMAMngrDMAAbort_fct) (PDEVICE_EXTENSION, ULONG Channel_UL);                              ///< Defines a pointer to a device specific DMA Start function
   BOOL32            (*CallBack_DMAMngrAddRequest_fct) (PDEVICE_EXTENSION, ULONG *pChannel_UL,  DMA_REQUEST *pDMARequ_X);  ///< Defines a pointer to a device specific DMA Request processing

	BOOL32            (*CallBack_BufMngrTxIntHandler_fct)(PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL);
	BOOL32            (*CallBack_BufMngrRxIntHandler_fct)(PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL);
	BOOL32            (*CallBack_BufMngrIsTransferAllowed_fct)(PDEVICE_EXTENSION pdx, ULONG BufQueueIdx_UL, BOOL32 Early_B);
   void              (*CallBack_BufMngrOnGrpProcessing_fct)(PDEVICE_EXTENSION pdx,ULONG GrpID_UL, ULONG RefCount_UL, BOOL32 FromDmaDone_B, ULONG *pBQUserParam_UL);
   BOOL32            (*CallBack_SetDrvPowerState_fct)(PDEVICE_EXTENSION pdx, DRV_POWER_STATE GrpID_UL);


   /* Called when the DrvIF acks the wake up */
   void              (*CallBack_AckWakeup_fct)(PDEVICE_EXTENSION pdx, PID_TYPE Process_pid);
   /* Called when the OS detects a device removal */
   void              (*CallBack_DeviceRemoval_fct)(PDEVICE_EXTENSION pdx);
   /* Board must provide this function to inform skel about the device's presence */
   BOOL32            (*CallBack_IsDevicePresent_fct)(PDEVICE_EXTENSION pdx);

   ULONG SleepContextSize_UL;    /* Size of the memory required to retain a board working state (active channels etc.), 0 = unused */
   ULONG pPreallocPoolSize_UL[COMMON_BUFFER_MAX_NBPREALLOCATEDPOOLS]; ///< Sizes of the preallocated buffer pools
   ULONG DriverVersion_UL;
   ULONG DriverSubtype_UL;
   ULONG DefaultDbgMask_UL;
   BOOL32 EnableScatterGatherSupport_B;
   ULONG ValidateAccessCode_UL;
   ULONG pIntSrcBit_UL[NBOF_INT_SRC];
   ULONG BufQUserParamIdx_ChnIdx_UL;
   ULONG BufQUserParamIdx_SwIdx_UL;
   ULONG DmaChannelCount_UL;
#ifdef WIN32
   const GUID *pDriverGUID_X; 
#endif
   const char *pDriverName_c;
   int LocalBoardNb_i;
   ULONG BoardDataSize_UL;

} STREAM_CUSTOMIZATION;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void StreamCallBack_EntryPoint(void);
void StreamCallBack_CustomizeDrv(PDEVICE_EXTENSION pdx, STREAM_CUSTOMIZATION * pCallbacksList_X);

#endif // _STREAMCALLBACKS_H_
