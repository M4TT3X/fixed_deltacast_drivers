#!/bin/bash

drivername="delta-asi"
echo "Unloading the ${drivername} module"
/sbin/rmmod ${drivername}

echo "Deleting device nodes..."
rm /dev/${drivername}*