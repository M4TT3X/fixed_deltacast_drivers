/********************************************************************************************************************//**
 * @file   DMAMngr.h
 * @date   2008/02/05
 * @Author cs
 * @brief  This module handles all DMA operations.
 *
 * It implements a queue of DMA requests to allows to share a unique hardware DMA channel between several requestors.
 * The DMA manager is a generic and board independent module.  For specific behavior associated to a given board, it 
 * calls a set of callback functions that the driver must implement :
 *  - DMA_Init() : Called by the DMAMngr_Init() to initialize the hardware DMA controller.
 *  - DMA_Start() : Called by the DMAMngr_Start() to start a DMA when it needed.
 *  - DMADoneHandler() : Called be the DMAMngr_DMADoneHandler() to manager a End Of DMA interruption
 *  - DMA_Abort() : Called by the DMAMngr_Abort() to abort a DMA transfers.
 *
 * User is able to add DMA requests to the queue by calling the DMAMngr_AddRequest() function. If no DMA are pending, the
 * DMA will automatically be started, otherwise, it will just be added to the queue.
 *
 * To be able to handle multiple DMA channels, an additional layer is added on top of a list of DMAMngr. It dispatches
 * the requested to the target channels. Note that a specific callback allows target driver to modify incoming DMA requests
 * before their processing (for example to automatically route the TX request to one channel and the RX to another).
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2008/02/05  v00.01.0000    cs       Creation of this file
   2009/04/24  v00.02.0000    gt       In DMAMngr_Abort, wait for the pending DMA.    
   2011/01/27  v00.03.0000    cs       Do some change for scatter/gather support. The Request DMA translation is no more done
                                       during the add request but during the DMA start. The translatation in the case of USBuf
                                       requires access to the common buffer used to store pages list. So it could be unaccessible
                                       if a DMA is already in progress.
   2011/03/28  v00.03.0001    cs       Fix a bug in DMAMngr_TranslateToPhysicalAddr() : In SG mode, the offset was ignored.
   2011/03/29  v00.03.0002    cs       Fix a bug the SG mode when auto split request feature is used (by setup a max size in 
                                       a DMA queue). In SG, the size of the descriptor (and the small BM transfer) was wrong.
   2011/04/05  v00.03.0003    cs       Add a way to detect number of active DMA channels.
   2011/04/27  v00.03.0004    cs       Add a way to discard invalid DMA request In the DMA_Start() function, when the TranslateToPhysical fails, 
                                       the function discards the request and attempts to pop the next.
   2013/03/29  v00.03.0005    bc       Replace KOBJ_SetSyncEvent by KOBJ_SetSyncEventByDPC
   2013/11/08  v00.03.0006    ja       Add error management in DMAMngr_Abort function in case of timeout on DMA done event. Previously was stuck in the loop!

   ====================================================================================================================
   =  TODO and remarks                                                                                                =
   ====================================================================================================================
   - The size of preallocated memory used in the case of scatter/gather (to store descriptors) is fixed to 4096*8
     This size could be settable during the init step.

  **********************************************************************************************************************/

#ifndef _DMAMNGR_H_
#define _DMAMNGR_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvTypes.h"
#include "DMAMngrTypes.h"
#include "KernelObjects.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define MAX_DMA_REQUEST 128    ///< Defines the maximum numbers of request per queue
#define MAX_DMA_QUEUE   3      ///< Defines the maximum number of queues

/* Note that these flags are stored the the reserved range (the 8 MSBits of the flags field) */
#define DMA_REQUEST_FLAGS_SPLIT_RQST         0x4000000   // specifies that the request is a sub request
#define DMA_REQUEST_FLAGS_SPLIT_RQST_FIRST   0x1000000   // Specifies that the sub request is the first one
#define DMA_REQUEST_FLAGS_SPLIT_RQST_LAST    0x2000000   // Specifies that the sub request is the last one
#define DMA_REQUEST_FLAGS_SPLIT_RQST_MASK    0x7000000   // Mask to make easier the clear of split request information

#define MAX_DMA_CHANNELS 2

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/


/***** MACROS DEFINITIONS *********************************************************************************************/


/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
   DMA_MODE_BLOCKMODE,                 ///< DMA mode defined by a target physical address and a block size
   DMA_MODE_SCATTERGATHER_PAGES_LIST,  ///< DMA defines by a list of physcal address of pages, the number of pages a,d the physical address of a buffer that contains the list.
   DMA_MODE_UNKNOWN=0xFFFFFFFF
} DMA_MODE;

typedef struct  
{
   ULONGLONG PCIAddr_ULL;
} TDMARQST_INFO_BLOCKMODE;

typedef struct  
{
   ULONGLONG DescrPCIAddr_ULL;
   ULONG     DescrSize_UL;
} TDMARQST_INFO_SCATTERGATHER;

typedef struct  
{
   DMA_REQUEST DMARequest_X;
   DMA_MODE    DMAMode_e;
   union 
   {
      TDMARQST_INFO_BLOCKMODE       BlockModeInfo_X;
      TDMARQST_INFO_SCATTERGATHER   ScatterGatherInfox_X;
   };
} TRANSLATED_DMA_REQUEST;

typedef struct 
{
   TRANSLATED_DMA_REQUEST pDMARequestQueue_X[MAX_DMA_REQUEST];   ///< Request queue
   ULONG RequQueue_NbInQueue_UL;                      ///< Number of request currently in queue
   ULONG RequQueue_RdIdx_UL;                          ///< Reading out index
   ULONG RequQueue_WrIdx_UL;                          ///< Writing in index
   ULONG MaxDMASize_UL;                               ///< Maximum size of a DMA transfer request before splitting it
   ULONG Priority_UL;                                 ///< Defines the queue priority. The higher the value is, the higher the priority is.
}DMA_QUEUE;

typedef struct
{
   BOOL32  InProgress_B;
   BOOL32  AbortInProgress_B;

   DMA_QUEUE pDMAQueues_X[MAX_DMA_QUEUE];
   DMA_QUEUE *pCurrentDMAQueue_X;            ///< Keeps a pointer to the DMA queue involved in the current DMA transfer.

   KOBJ_SPINLOCK SpinLock_KO;                ///< Synchronization object for queue accessing
   KOBJ_SYNC_EVENT DMADone_KO;               ///< Driver event to notify a DMA Done event (used mainly to synchronize properly the end of DMA with driver stopping).   

   COMMON_BUFFER_DESCR *pDMADescrBuffer_X;   ///< Can be used to keep a common buffer for scatter-gather descriptor storing
   ULONG DMADescrBufID_UL;

   /** Function Pointer list for device specific function to adapt the DMA processing behaviour */

   BOOL32 UseScatterGatherDMA_B;
   BOOL32 (*pDevDMAWakeup_fct)(struct _DEVICE_EXTENSION *, ULONG Channel_UL, COMMON_BUFFER_DESCR *);  ///< Defines a pointer to a device specific DMA initialization function
   void   (*pDevDMADone_fct)  (struct _DEVICE_EXTENSION *, ULONG Channel_UL, BOOL32 *pSplitPart_B, TRANSLATED_DMA_REQUEST*);          ///< Defines a pointer to a device specific DMA done handler function
   BOOL32 (*pDevDMAStart_fct) (struct _DEVICE_EXTENSION *, ULONG Channel_UL, TRANSLATED_DMA_REQUEST*);          ///< Defines a pointer to a device specific DMA Start function
   void   (*pDevDMAAbort_fct) (struct _DEVICE_EXTENSION *, ULONG Channel_UL);                         ///< Defines a pointer to a device specific DMA Start function
   
   ULONG DMAChannel_UL; ///< This value is just forwarded to the device specific function
   BOOL32 DisableDMASplit_B;

} DMA_MNGR_DATA;


typedef struct
{
   BOOL32 (*pDevDMAAddRequest_fct) (struct _DEVICE_EXTENSION *, ULONG *pChannel_UL,  DMA_REQUEST *pDMARequ_X); ///< Defines a pointer to a device specific pre-processing DMA request
   DMA_MNGR_DATA pDMAChannel_X[MAX_DMA_CHANNELS];
   ULONG         pAssocatedDMADoneInterrupt_UL[MAX_DMA_CHANNELS];
   ULONG         NbActiveChannels_UL;
} MULTI_CHN_DMA_MNGR_DATA;



// BOOL32 DMAMngr_CheckIoCtl(PDEVICE_EXTENSION pdx, DMA_MNGR_DATA *pDMAMngrData_X, ULONG Cmd_UL);
// BOOL32 DMAMngr_IoCtl(PDEVICE_EXTENSION pdx, DMA_MNGR_DATA *pDMAMngrData_X, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL, ULONG PID_UL);
// BOOL32 DMAMngr_DMAQueueSetup(PDEVICE_EXTENSION pdx, DMA_MNGR_DATA *pDMAMngrData_X,DMA_QUEUE_SETUP *pSetup_X);

BOOL32   MultiChnDMAMngr_InProgress(PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL);
BOOL32   MultiChnDMAMngr_DeviceAdd(PDEVICE_EXTENSION pdx);
BOOL32   MultiChnDMAMngr_Wakeup(PDEVICE_EXTENSION pdx);
void     MultiChnDMAMngr_AbortAll(PDEVICE_EXTENSION pdx);
void     MultiChnDMAMngr_Abort(PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL);
BOOL32   MultiChnDMAMngr_AddRequest(PDEVICE_EXTENSION pdx,ULONG DMAChannel_UL, DMA_REQUEST *pDMARequ_X);
BOOL32   MultiChnDMAMngr_Start(PDEVICE_EXTENSION pdx,ULONG DMAChannel_UL);
ULONG    MultiChnDMAMngr_DMADoneHandler(PDEVICE_EXTENSION pdx,ULONG DMAChannel_UL, BOOL32 *pSplitPArt_B, DMA_REQUEST_USER_DATA * pUserData_X);
BOOL32   MultiChnDMAMngr_CheckDMADoneInt(PDEVICE_EXTENSION pdx,ULONG DMAChannel_UL,ULONG IntSrc_UL);
BOOL32   MultiChnDMAMngr_DMAQueueSetup( PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL ,DMA_QUEUE_SETUP *pSetup_X );
DMA_MNGR_DATA * MultiChnDMAMngr_GetDMAMngrData(PDEVICE_EXTENSION pdx, ULONG DMAChannel_UL);

ULONGLONG MultiChnDMAMngr_InitPhysicalStartAddr(PDEVICE_EXTENSION pdx,DMABUFID DMABufID);

/*BOOL32 MultiChnDMAMngr_CheckIoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL);*/
IOCTL_STATUS MultiChnDMAMngr_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL, PID_TYPE PID_UL);




#endif // _DMAMNGR_H_
