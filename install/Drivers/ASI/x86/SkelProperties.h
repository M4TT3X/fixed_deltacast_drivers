/********************************************************************************************************************//**
 * @internal
 * @file   	skelproperties.h
 * @date   	2011/03/29
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2011/03/29  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _SKELPROPERTIES_H_
#define _SKELPROPERTIES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

enum SKEL_PROPERTIES
{
   SKEL_PROPERTIES_DRIVER_FLAGS=0,       // Internal DEBUG Purpose Flags
   SKEL_PROPERTIES_DRIVER_FEATURES,
   SKEL_PROPERTIES_DRIVER_VERSION,       // Driver Version
   SKEL_PROPERTIES_BOARD_REFCOUNT,       // Open board handles counter

   SKEL_PROPERTIES_INTSRC0_CNT,
   SKEL_PROPERTIES_INTSRC1_CNT,
   SKEL_PROPERTIES_INTSRC2_CNT,
   
   SKEL_PROPERTIES_DRIVER_SUBTYPE,       // Driver subtype

   SKEL_PROPERTIES_BOARD_REFCOUNT_NO_AWAKE,  // Open board handles that doesn't recover from wakeup counter
   /* ...*/
   NB_SKEL_PROPERTIES=SKEL_PROPERTIES_INTSRC2_CNT+32   //Keep 32 free space for future skel properties

};


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _SKELPROPERTIES_H_
