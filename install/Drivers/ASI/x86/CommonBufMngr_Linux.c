/**********************************************************************************************************************
 
   Module   : CommonBufMngr
   File     : CommonBufMngr.c
   Created  : 2006/11/21
   Author   : cs

 **********************************************************************************************************************/



/***** INCLUDES *******************************************************************************************************/
#include "CTypes.h"
#include "LinuxAllocator.h"
#include "CommonBufMngr.h"
#include "LinuxDrvHlp.h"
#define MODID MID_BOARD
#include "DrvDbg.h"
#include "MMapCommandId.h"
#if (MEM_ALLOC_METHOD == MEM_ALLOC_BIGPHYSAREA)
#include <linux/bigphysarea.h>
#endif

/***** LOCAL SYMBOLS DEFINITIONS **************************************************************************************/

#define INVALID_BUFFER_ID 0xFFFFFFFF

/***** EXTERNAL VARIABLES *********************************************************************************************/

/***** GLOBAL VARIABLES ***********************************************************************************************/


/***** CommonBufMngr_MMap *********************************************************************************************

  Description :

	This function is called by system during an MMAP operation focusing on common buffer. The common buffer index is
   given through command (Cmd_UL)

  Parameters :

	PDEVICE_EXTENSION pdx	   : Pointer to device extension object
	struct vm_area_struct *vma	: Pointer to vma structure given by system during MMAP operation
	ULONG Cmd_UL	            : MMAP User command (given through offset parameter. Refer to PCI skeleton MMAP function 
                                for more information)
 
  Returned Value : int
  Remark : None
 
 **********************************************************************************************************************/
int CommonBufMngr_MMap(PDEVICE_EXTENSION pdx, struct vm_area_struct *vma, ULONG Cmd_UL)
{
int Sts_i=0;
ULONG BufIdx_UL = Cmd_UL & MMAP_CMD_PARAM_MASK;
LONGLONG PhysicalAddr_LL;
ULONG Size_UL;

   EnterOutput( "MMap BufIdx:%d\n",BufIdx_UL);
      
   if (BufIdx_UL<COMMON_BUFFER_MAX_NB)
   {
      PhysicalAddr_LL = pdx->CommonBufMngrData_X.pCommonBufferDescr_X[BufIdx_UL].PhysicalAddr_PA.QuadPart;
      Size_UL = pdx->CommonBufMngrData_X.pCommonBufferDescr_X[BufIdx_UL].Size_UL;

      Sts_i=LinuxDrvHlp_MMap(pdx, vma,PhysicalAddr_LL,Size_UL);
   }
   else ErrOutput( "Invalid BufId (%08xh)\n",BufIdx_UL);
         
   ExitOutput("\n");

   return (Sts_i);
}



BOOL32 CommonBufMngr_OSAllocateBuf(PDEVICE_EXTENSION pdx,COMMON_BUFFER_DESCR * pCommonBufferDescr_X)
{
ULONG Size_UL = pCommonBufferDescr_X->Size_UL;
BOOL32 Sts_B = FALSE;

   EnterOutput("(Size_UL=%08lxh\n",Size_UL);
#if (MEM_ALLOC_METHOD == MEM_ALLOC_MEMEQUAL)   
   pCommonBufferDescr_X->PhysicalAddr_PA.QuadPart = allocator_allocate_dma(Size_UL,GFP_KERNEL);
   if (pCommonBufferDescr_X->PhysicalAddr_PA.QuadPart)
   {
      pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v = ioremap((unsigned long)pCommonBufferDescr_X->PhysicalAddr_PA.QuadPart, Size_UL);
      if (!pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v)
      {
         ErrOutput( "Unable to map buffer area to kernel space (ioremap)\n");
         allocator_free_dma((unsigned long)pCommonBufferDescr_X->PhysicalAddr_PA.QuadPart);
      }	
      else Sts_B = TRUE;
   }
#else
#if (MEM_ALLOC_METHOD == MEM_ALLOC_BIGPHYSAREA) 
   pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v = bigphysarea_alloc_pages((Size_UL+PAGE_SIZE-1)/PAGE_SIZE, 0, GFP_KERNEL);
#else
  pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v = (UBYTE *)__get_free_pages(GFP_KERNEL|__GFP_DMA32,get_order(Size_UL));    
#endif
   if (pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v)
   {
      pCommonBufferDescr_X->PhysicalAddr_PA.QuadPart = virt_to_phys(pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v);
      Sts_B = TRUE;
   }
#endif    


   ExitOutput("Sts:%02x\n",Sts_B);

   return Sts_B;

}

void CommonBufMngr_OSFreeBuf(PDEVICE_EXTENSION pdx,COMMON_BUFFER_DESCR * pCommonBufferDescr_X)
{

#if (MEM_ALLOC_METHOD == MEM_ALLOC_MEMEQUAL)             
   iounmap(pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v);	
   if (pCommonBufferDescr_X->PhysicalAddr_PA.QuadPart)
   {
      allocator_free_dma((unsigned long)pCommonBufferDescr_X->PhysicalAddr_PA.QuadPart);
   }
#else
#if (MEM_ALLOC_METHOD == MEM_ALLOC_BIGPHYSAREA)
   bigphysarea_free_pages(pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v);	
#else
   free_pages((unsigned long)pCommonBufferDescr_X->pAddr_UB.Address_U.pAddress_v, get_order(pCommonBufferDescr_X->Size_UL));
#endif
#endif   
}

/***** CommonBufMngr_GetBuf *********************************************************************************************

  Description :

	

  Parameters :

	PDEVICE_EXTENSION pdx	:
	COMMON_BUFFER_DESCR *pDescr_X	:
 
  Returned Value : BOOL32
  Remark : None
 
 **********************************************************************************************************************/
BOOL32 CommonBufMngr_GetBuf( PDEVICE_EXTENSION pdx, COMMON_BUFFER_DESCR *pDescr_X )
{
BOOL32 Sts_B=TRUE;

   EnterOutput("\n");
   ExitOutput( "(Sts_B:%02xh)\n",Sts_B);

   return (Sts_B);
}

/***** CommonBufMngr_ReleaseBuf *********************************************************************************************

  Description :

	

  Parameters :

	PDEVICE_EXTENSION pdx	:
	COMMON_BUFFER_DESCR *pDescr_X	:
 
  Returned Value : BOOL32
  Remark : None
 
 **********************************************************************************************************************/
BOOL32 CommonBufMngr_ReleaseBuf( PDEVICE_EXTENSION pdx, COMMON_BUFFER_DESCR *pDescr_X )
{
   BOOL32 Sts_B=FALSE;

   EnterOutput("\n");
   ExitOutput( "(Sts_B:%02xh)\n",Sts_B);

   return (Sts_B);
}


void CommonBufMngr_DbgDump(PDEVICE_EXTENSION pdx)
{
ULONG i;

   for (i=0; i<10; i++)
   {
      DbgOutput("BUFID:%08lxh - Flags:%08lxh\n",i,pdx->CommonBufMngrData_X.pBufferFlags_UL[i]);
   }
}
