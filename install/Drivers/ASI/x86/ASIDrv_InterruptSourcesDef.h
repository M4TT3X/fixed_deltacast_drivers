#ifndef _INTERRUPT_SOURCES_DEF_H_
#define _INTERRUPT_SOURCES_DEF_H_


#define INT_SOURCE_RXi_INT(chn_idx)                (0x00000001<<(chn_idx))
#define INT_SOURCE_OVERRUN_RXi_INT(chn_idx)        (0x00000100<<(chn_idx))
#define INT_SOURCE_STS_CHANGE_RXi_INT(chn_idx)     (0x00010000<<(chn_idx))


#define INT_SOURCE_TXi_INT(chn_idx)          (0x00000001<<(7-(chn_idx)))
#define INT_SOURCE_UNDERRUN_TXi_INT(chn_idx) (0x00000100<<(7-(chn_idx)))

#define INT_SOURCE_DMA0_DONE                0x01000000
#define INT_SOURCE_DMA1_DONE                0x02000000
#define INT_SOURCE_LTC_INT                 0x04000000
#define INT_SOURCE_WATCHDOG_TIMEOUT        0x08000000

#define INT_SOURCE_USR_INT                 0x80000000


#endif


