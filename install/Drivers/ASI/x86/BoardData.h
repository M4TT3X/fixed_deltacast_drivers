/********************************************************************************************************************//**
 * @internal
 * @file   	BoardData.h
 * @date   	2010/07/15
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/15  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _BOARDDATA_H_
#define _BOARDDATA_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "KernelObjects.h"
#include "ASIDRV_RegistersMap.h"
#include "KernelSysCall.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
#define MAX_NBOF_RX_CHANNEL         8

typedef struct
{
   ULONG           BoardTypeFlags_UL;
   BOOL32            FPGADownloaded_B;

   ULONG           pLastRxSts_UL[MAX_NBOF_RX_CHANNEL];     // This table stores the last buffer id given during interrupt
   ULONG           pLastRXTimeStamp_UL[MAX_NBOF_RX_CHANNEL];
   SYSTEM_TIME     pLastRxSystemTime_X[MAX_NBOF_RX_CHANNEL];  
   ULONG           NbOfDmaChn_UL;

   PMM32_REGISTER  pBoardTC_Reg;
} BOARD_DATA;

#endif // _BOARDDATA_H_

