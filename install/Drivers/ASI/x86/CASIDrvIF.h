/********************************************************************************************************************//**
 * @internal
 * @file   	CSDDrvIF.h
 * @date   	2010/07/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/16  v00.01.0000    cs       Creation of this file
   2011/03/07  v00.01.0001    cs       Add a using CStreamDrvIF::AddDMARequest; in the class definition to avoid compilation
                                       error of the test application. Thisis liked to the hidden name C++ concept.

 **********************************************************************************************************************/

#ifndef _CASIDrvIF_H_
#define _CASIDrvIF_H_

/***** INCLUDES SECTION ***********************************************************************************************/


#include "CStreamDrvIF.h"
#include "ASIDrv_Properties.h"
#include "ASIDrv_Types.h"


namespace CASIDrvIF_NS
{

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define TXCHANNEL_ADDITIONAL_FEATURES_DISABLE_IO_PROCESSING 0x000000001


#define RXCHANNEL_ADDITIONAL_FEATURES_VBI10BITS 0x000000010

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
 
   typedef struct  
   {
      ASIDRV_BOARD_TYPE BoardType_e;
      ULONG FirmwareVersion_UL;
      ASIDRV_BOARD_FIRMWAREID FirmwareID_e;
      STREAM_BOARD_STATE FirmwareState_e;
      ULONG BoardFeatures_UL;
      UBYTE NbRxChannels_UB;
      UBYTE NbTxChannels_UB;
      ULONG CPLDVersion_UL;
   } BOARD_INFO;

   enum DATA_TYPE
   {
      DATA_TYPE_VIDEO_TS=ASIBRD_BUFQ_DATA_TYPE_TS,
   };

   enum CHANNEL
   {
      CHANNEL_RX,
      CHANNEL_TX
   };

   enum ASI_FMT
   {
      ASI_FMT_188,
      ASI_FMT_188_TO_204,
      ASI_FMT_204,
      ASI_FMT_204_TO_188,
      ASI_FMT_RAW,
   };

   enum ASI_INTSEL
   {
      ASI_INTSEL_1,
      ASI_INTSEL_10,
      ASI_INTSEL_100,
      ASI_INTSEL_1000,
   };

   enum ASI_BRSRC
   {
      ASI_BRSRC_LOCAL,
      ASI_BRSRC_RX0,
      ASI_BRSRC_RX1,
      ASI_BRSRC_EXT0,
   };

   struct ASI_TX_CHANNEL_SETUP_PARAM  
   {  
      ASI_FMT           Fmt_e;
      ULONG             BufferSize_UL;
      ULONG             BitRate_UL;
      ASI_INTSEL        IntPer_e;
      ULONG             MaxDev_UL;
      ASI_BRSRC         BitrateSrc_E;
      BOOL32            BkpBR_B;
      BOOL32            SynchronousSlaving_B;
   };

   struct ASI_RX_CHANNEL_SETUP_PARAM 
   {  
      ASI_FMT           Fmt_e;
      ULONG             BufferSize_UL;
      ULONG             BitRate_UL;
      ASI_INTSEL        IntPer_e;
      ULONG             MaxDev_UL;
      BOOL32              EnableTS_B;
      int               *pPIDFilters_i;
      int               NbOfPIDFilters_i;
      BOOL32              DontSetIMR_B;
   };

   enum ASI_REF_CLOCK
   {
      ASI_REFCLK_LOCAL,
      ASI_REFCLK_RX0,
      ASI_REFCLK_RX1,
      ASI_REFCLK_EXT,
      NBOF_ASI_REF_CLOCK
   };

   enum ASI_EXT_OUT
   {
      ASI_EXTOUT_Z,
      ASI_EXTOUT_LOCAL,
      ASI_EXTOUT_RX0_BR,
      ASI_EXTOUT_RX1_BR,
      ASI_EXTOUT_RX2_BR,
      ASI_EXTOUT_RX3_BR,
      ASI_EXTOUT_RX4_BR,
      ASI_EXTOUT_RX5_BR,
      ASI_EXTOUT_RX6_BR,
      ASI_EXTOUT_RX7_BR,
      NBOF_ASI_EXT_OUT
   };

/***** CLASSES DECLARATIONS *******************************************************************************************/

class CASIDrvIF : public CStreamDrvIF
{
public:

   static ULONG GetNbBoard();

   CASIDrvIF(ULONG BoardIdx_UL);
   

   virtual CBufferQueue * CreateBufferQueue();
   virtual CDRVIF_STATUS Board_IsOK();
  
   ULONG GetFirmwareVersion();
   ULONG GetNbOfLane();
   
   const BOARD_INFO * Board_GetInfo() {return &m_BoardInfo_X;}
   

   void Board_WatchdogDisable();
   void Board_WatchdogArm();
   void Board_WatchdogEnable( ULONG TimeInMS_UL );

   CDRVIF_STATUS Board_DownloadFPGA(UBYTE *pBuffer_UB, ULONG Size_UL);
   CDRVIF_STATUS Board_GetSSN(LONGLONG *pSerialNumber_LL);
   CDRVIF_STATUS Board_SetBypassRelay(int RelayID_i, BOOL32 Enable_B);
   CDRVIF_STATUS Board_GetBypassRelay(int RelayID_i, BOOL32 *pEnable_B);
   CDRVIF_STATUS Board_GetSTC(ULONG *pMSW_UL, ULONG *pLSW_UL);
   CDRVIF_STATUS Board_SetRefClock(ASI_REF_CLOCK RefClkSrc_E);
   CDRVIF_STATUS Board_SetExtOut(ASI_EXT_OUT ExtOutSrc_E);
   CDRVIF_STATUS Board_GetClkSts(BOOL32 *pValid_B);
   
   void Board_GpioEnableOutput(ULONG Mask_UL);  //Output not available on bit 0 and 1, available on bit 2,3,4 and 5 
   void Board_GpioWriteOutput(ULONG Mask_UL);
   ULONG Board_GpioReadInput();


   CDRVIF_STATUS Channel_AsiTx_Start( UBYTE Channel_UB, ASI_TX_CHANNEL_SETUP_PARAM *pParam_X );
   CDRVIF_STATUS Channel_AsiTx_Stop( UBYTE Channel_UB );
   CDRVIF_STATUS Channel_AsiTx_GetStatus(UBYTE Channel_UB, BOOL32 *pFramingErr_B/*=NULL*/, BOOL32 *pBitrateErr_B/*=NULL*/);
   CDRVIF_STATUS Channel_AsiTx_GetBytesDiff(UBYTE Channel_UB, int *pBytesDiff_i);
   CDRVIF_STATUS Channel_AsiTx_ArmPreload(UBYTE Channel_UB);
   CDRVIF_STATUS Channel_AsiRx_Start( UBYTE Channel_UB, ASI_RX_CHANNEL_SETUP_PARAM *pParam_X );
   CDRVIF_STATUS Channel_AsiRx_Stop( UBYTE Channel_UB );
   CDRVIF_STATUS Channel_AsiRx_GetStatus( UBYTE Channel_UB, BOOL32 *pFramingErr_B/*=NULL*/, BOOL32 *pBitrateErr_B/*=NULL*/, BOOL32 *pUnlocked_B/*=NULL*/, BOOL32 *pBadFmt_B/*=NULL*/ );
   CDRVIF_STATUS Channel_AsiRx_GetBytesDiff( UBYTE Channel_UB, int *pBytesDiff_i );


   /*static*/ ULONG Helper_GetOnBoardAddress(CHANNEL Channel_e,UBYTE ChnIdx_i,ASIBRD_BUFQ_DATA_TYPE DataType_e, ULONG BufIdx_UL);
   /*static*/ void Helper_FillOnBoardAddresses(CHANNEL Channel_e,UBYTE ChannelIdx_UB, BUFFERQUEUE_PARAM *pBQParam_X);


   using CStreamDrvIF::AddDMARequest;
   virtual CDRVIF_STATUS AddDMARequest( DMA_REQUEST *pDMARequest_X, ULONG DMAChannel_UL);
   CDRVIF_STATUS Board_LoadDriverProperties();
   CDRVIF_STATUS FillTXDriverProperties(UBYTE ChannelIdx_UB, BOOL32 SWModifyStream_B, ULONG Preload_UL);
   ULONG GetDmaChn(CHANNEL Channel_e, UBYTE LogicalChnIdx_UB);

   CDRVIF_STATUS Board_WatchdogTimerRegister(ULONG *pRegister_UL);


private:
   ULONG m_DriverVersion_UL;

 

protected:
   BOARD_INFO m_BoardInfo_X;

   virtual BOOL32 CheckValidateAccessCode(ULONG ValidateAccessCode_UL);
   UBYTE GetPhysicalChnIdx(CHANNEL Channel_e, UBYTE LogicalChnIdx_UB);



   ULONG GetTxStsRegVal( UBYTE Channel_UB );
   ULONG GetTxRateRegVal( UBYTE Channel_UB );
   PMM32_REGISTER GetTxSizeReg( UBYTE Channel_UB );
   PMM32_REGISTER GetTxBaudReg( UBYTE Channel_UB );
   PMM32_REGISTER GetTxDevReg( UBYTE Channel_UB );
   PMM32_REGISTER GetTxCmdReg( UBYTE Channel_UB );


   ULONG GetRxStsRegVal( UBYTE Channel_UB );
   ULONG GetRxRateRegVal( UBYTE Channel_UB );
   PMM32_REGISTER GetRxSizeReg( UBYTE Channel_UB );
   PMM32_REGISTER GetRxBaudReg( UBYTE Channel_UB );
   PMM32_REGISTER GetRxDevReg( UBYTE Channel_UB );
   PMM32_REGISTER GetRxCmdReg( UBYTE Channel_UB );
   PMM32_REGISTER GetRxPidLutEntryReg( UBYTE Channel_UB, int Entry_i );
};

class CASIDrvBufferQueue : public CBufferQueue
{

   friend class CASIDrvIF;

protected:

   CASIDrvBufferQueue(CASIDrvIF * pOwner_O);

private:

   CASIDrvIF * m_pOwner_O;


};

};
#endif // _CASIDrvIF_H_
