/********************************************************************************************************************//**
 * @internal
 * @file   	ASIDRV_Version.h
 * @date   	2010/08/26
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2013/09/12  v01.00.0000    gt       Creation of DELTA-asi driver
	2014/04/25  v01.00.0001    gt       New FPGA (0x14051401)
	2014/08/28	v01.00.0002		ja			Bug fix:  Call the entire remove() function execpt free_irq if request_irq() linux function fails in DriverEntre::probe
	2014/10/07	v01.00.0002		ja			Bug fix: Linux process id (PID) is saved in structure because it can be different in MainDevice_Release when the process is killed (see MainDevice_Open function). 
   2015/12/21  v01.00.0003    gt       SkelDrv_Sleep was not called in removeEx (Linux)
   2016/01/26  v01.00.0004    gt       New : compatibility with Linux kernel >= 4.1 (remove IRQF_DISABLED usage)
   2016/02/16  v01.00.0005    ja       Bug fix: some IOCTL used user space memory without KSYSCALL_CopyToUserSpace. That caused crash on linux with Z170 chipset.
   2016/03/23  v01.00.0006    bc       Bug fix : check device presence before calling SkelDrv_SetDrvPowerState to avoid blue screen if device is removed

 **********************************************************************************************************************/

#ifndef _ASIDRV_VERSION_H_
#define _ASIDRV_VERSION_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define ASIBRD_VERMAJOR 1    
#define ASIBRD_VERMINOR 0
#define ASIBRD_BUILD 6

/** @def VALIDATE_ACCESS_CODE
 * The validate access code allows to check if the interface between driver and interface object are compatible.
 * If the object driver interface is linked to a greater minor code, it could inform that a newer driver is available, but, it
 * can work with the current one.
 * If the major access code mismatches, the interface object is not able to work  with the current driver.
 **/

#define ASIDRV_VALIDATE_ACCESS_CODE_MAJOR 0x0001 ///< Gives a version number of the interface. This number must MATCH !
#define ASIDRV_VALIDATE_ACCESS_CODE_MINOR 0x0001
#define ASIDRV_VALIDATE_ACCESS_CODE ((ASIDRV_VALIDATE_ACCESS_CODE_MAJOR<<16) | ASIDRV_VALIDATE_ACCESS_CODE_MINOR)


#define ASISDI_PPA_FW_VERSION         0x14052201
#define ASISDI_ASI8_FW_VERSION        0x14061601

/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _ASIDRV_VERSION_H_
