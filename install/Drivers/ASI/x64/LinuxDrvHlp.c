/**********************************************************************************************************************
 
   Module   : LinuxDrvHlp
   File     : LinuxDrvHlp.c
   Created  : 2006/11/22
   Author   : cs

 **********************************************************************************************************************/

/***** INCLUDES *******************************************************************************************************/

#define MODID MID_BOARD

#include "LinuxDrvHlp.h"
#include "DrvDbg.h"

/***** DEFINE  *******************************************************************************************************/

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,10))
#define remap_pfn_range(a,b,c,d,e) remap_page_range(a,b,(c)<<PAGE_SHIFT,d,e)
#else
int remap_pfn_range(struct vm_area_struct *, unsigned long,unsigned long, unsigned long, pgprot_t);
#endif

/* jj edit: VM_RESERVED is removed from linux kernel 3.7 */
#ifndef VM_RESERVED
#define VM_RESERVED (VM_DONTEXPAND | VM_DONTDUMP)
#endif


/***** EXTERNAL VARIABLES *********************************************************************************************/

/***** GLOBAL VARIABLES ***********************************************************************************************/

/***** LinuxDrvHlp_MMap *********************************************************************************************

  Description :

	

  Parameters :

	PDEVICE_EXTENSION pdx 	:
	struct vm_area_struct *vma	:
	ULONG PhysicalAddr_UL	:
	ULONG Size_UL	:
 
  Returned Value : int
  Remark : None
 
 **********************************************************************************************************************/
int LinuxDrvHlp_MMap(PDEVICE_EXTENSION pdx , struct vm_area_struct *vma, LONGLONG PhysicalAddr_LL, ULONG Size_UL)
{
unsigned long pfn;
int Sts_i = 0;

   EnterOutput("\n");

   vma->vm_flags |= VM_RESERVED;
   vma->vm_private_data = NULL;

   pfn = PhysicalAddr_LL >> PAGE_SHIFT;

   if (!pfn_valid(pfn) || PageReserved(pfn_to_page(pfn)))
      InfoOutput( "MMAP will work\n");
   else
      WrnOutput( "MMAP will not work\n");

   if (remap_pfn_range(vma,vma->vm_start,	pfn, Size_UL, vma->vm_page_prot))
      Sts_i = -EAGAIN;

   ExitOutput("\n");
   return Sts_i;
}

/***** LinuxDrvHlp_MMap_PCIResource *********************************************************************************************

  Description :

	This function helps to map (through MMap interface) a specified PCI BAR region.

  Parameters :

	PDEVICE_EXTENSION pdx	   : Pointer to device extension object.
	struct vm_area_struct *vma	: Forwards VM information given by mmap().
	UBYTE PCIBar_UB	         : Specifies the PCI BAR.
 
  Returned Value : int
  Remark : None
 
 **********************************************************************************************************************/
int LinuxDrvHlp_MMap_PCIResource(PDEVICE_EXTENSION pdx,struct vm_area_struct *vma, UBYTE PCIBar_UB )
{
int Sts_i;
LONGLONG PhysicalAddr_LL,Size_UL;
   EnterOutput( "(PCI BAR : %d)\n",PCIBar_UB);

   if ((PCIBar_UB >= NB_PCI_BARS) || (pdx->pPCIResources_X[PCIBar_UB].Type_E != PCI_RESOURCE_TYPE_MEMORY)) Sts_i = -EINVAL;
   else
   {
      PhysicalAddr_LL=pdx->pPCIResources_X[PCIBar_UB].PhysicalAddr_PA.QuadPart;
      Size_UL=pdx->pPCIResources_X[PCIBar_UB].Size_UL;

      Sts_i=LinuxDrvHlp_MMap(pdx,vma,PhysicalAddr_LL,Size_UL);
   }

   ExitOutput( "(Sts:%d)\n",Sts_i);

   return (Sts_i);

}

UCHAR IO_Read8(ULONG Offset_UL)
{
   return inb_p(Offset_UL);
}

USHORT IO_Read16(ULONG Offset_UL)
{
   return inw(Offset_UL);
}

ULONG IO_Read32(ULONG Offset_UL)
{
   return inl(Offset_UL);
}

void IO_Write8(ULONG Offset_UL, ULONG Value_UL)
{
   outb(Offset_UL,Value_UL);
}

void IO_Write16(ULONG Offset_UL, ULONG Value_UL)
{
   outw(Offset_UL,Value_UL);
}

void IO_Write32(ULONG Offset_UL, ULONG Value_UL)
{
   outl(Offset_UL,Value_UL);
}


