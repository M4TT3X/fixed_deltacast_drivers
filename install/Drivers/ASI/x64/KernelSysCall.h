/********************************************************************************************************************//**
 * @internal
 * @file   	KernelSysCall.h
 * @date   	2011/03/21
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
    2006/11/21  v00.01.0000    cs       Creation of this file
    2011/03/21  v00.02.0000    cs/bl/qr Change the KSYSCALL_CopyFromUserSpace() and KSYSCALL_CopyToUserSpace() implementation
                                        The 'Macro way' is incompatible with the pre-compiled approach. 
    2011/03/22  v00.02.0001    cs       Comments update in header and c files.
    2013/02/04  v00.03.0000    bc       Add Apple support

 **********************************************************************************************************************/

#ifndef _KERNELSYSCALL_H_
#define _KERNELSYSCALL_H_

/***** INCLUDES SECTION ***********************************************************************************************/
#include "CTypes.h"
#include "DrvTypes.h"
#include "FirmwareRequest.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/***** MACROS DEFINITIONS *********************************************************************************************/

#if defined(__linux__) || defined(__APPLE__)
#define KSYSCALL_Memset(pAddr_v,value,size_UL )       memset(pAddr_v,value,size_UL)
#define KSYSCALL_Memcpy(pDest_v,  pSrc_v, size_UL )   memcpy(pDest_v, pSrc_v, size_UL)
#define KSYSCALL_ZeroMemory(pAddr_v,size_UL)          memset(pAddr_v,0,size_UL)
#define KSYSCALL_Memcmp(pDest_v,  pSrc_v, size_UL )   memcmp(pDest_v, pSrc_v, size_UL)
#else
#define KSYSCALL_Memset(pAddr_v,value,size_UL )       RtlFillMemory(pAddr_v,size_UL,value)
#define KSYSCALL_Memcpy(pDest_v,  pSrc_v, size_UL )   RtlCopyMemory(pDest_v, pSrc_v, size_UL)
#define KSYSCALL_ZeroMemory(pAddr_v,size_UL)          RtlZeroMemory(pAddr_v,size_UL)
#define KSYSCALL_Memcmp(pDest_v,  pSrc_v, size_UL )   (RtlCompareMemory(pDest_v, pSrc_v, size_UL)!=size_UL)
#endif

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef struct
{
#ifdef __linux__ 
   UBYTE *pData_UB;
   ULONG Size_UL;
   struct firmware *pFw_X;
#else
   UBYTE *pData_UB;
   ULONG Size_UL;
   PVOID DataSectionHandle_v;
#endif

} FIRMWARE_DATA;

#ifdef __linux__
//typedef struct timeval SYSTEM_TIME;
typedef struct timespec SYSTEM_TIME;
#elif defined(__APPLE__)
typedef struct
{
    clock_sec_t Sec_UL;
    clock_nsec_t Nano_ULL;
} SYSTEM_TIME;
#else
typedef struct  
{
   LARGE_INTEGER NewTime_LI;
   LARGE_INTEGER Freq_LI;
} SYSTEM_TIME;
#endif


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

// BOOL32 KSYSCALL_NVParameterRead(PDEVICE_EXTENSION pdx, char *pParamName_c, ULONG *pParamValue_UL)

BOOL32   KSYSCALL_RequestFirmware(PDEVICE_EXTENSION pdx, FIRMWARE_DATA *pFWInfo_X, FIRMWARE_REQUEST *pRequest_X);
BOOL32   KSYSCALL_ReleaseFirmware(PDEVICE_EXTENSION pdx, FIRMWARE_DATA *pFWInfo_X);
void     KSYSCALL_Sleep(ULONG MilliSec_UL);
void     KSYSCALL_GetSystemTime(SYSTEM_TIME *pCT_X);
LONGLONG KSYSCALL_SystemTimeToMicroSec(SYSTEM_TIME *pCT_X);
void     KSYSCALL_GetPCIEVendorID(PDEVICE_EXTENSION pdx, UWORD *pPCIEVendorID_UW, UWORD *pPCIEDeviceID_UW);

int      KSYSCALL_CopyFromUserSpace(void* pDst_v, void* pSrc_v, ULONG Size_UL);
int      KSYSCALL_CopyToUserSpace(void* pDst_v, void* pSrc_v, ULONG Size_UL);           

/* Map / Unmap physical memory to user space virtual addressing. 
 * For historical reasons, memory mapping in Linux is achieved by calling mmap from user space.
 * => Those functions have no effect on Linux 
 * On other systems, PCI bars are mapped using those functions from kernel space.
 *
 * This should be unified by calling do_mmap on linux
 */
BOOL32   KSYSCALL_MapMemory(ULONGLONG PhysicalAddress_ULL, ULONG Length_UL, void** ppUserSpace_v);
void     KSYSCALL_UnmapMemory(ULONGLONG PhysicalAddress_ULL, ULONG Length_UL, void* pUserSpace_v);

#endif // _KERNELSYSCALL_H_

