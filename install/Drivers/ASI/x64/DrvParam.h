/********************************************************************************************************************//**
 * @internal
 * @file   	DrvParam.h
 * @date   	2009/01/22
 * @author 	gt
 * @brief   
 **********************************************************************************************************************/
#ifndef DrvParam_h__
#define DrvParam_h__

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"
#include "Driver.h"
#ifdef __linux__
#include <linux/stat.h>
#endif

/***** LOCAL SYMBOLS DEFINITIONS **************************************************************************************/

/* Define driver parameter name (without quotation marks !)*/
#define DPN_RELAY0_DISABLE    Relay0Disable
#define DPN_RELAY0_ENABLE     Relay0Enable
#define DPN_RELAY1_DISABLE    Relay1Disable
#define DPN_RELAY1_ENABLE     Relay1Enable

#define DPN_PREALLOC_POOL0_SIZE PreallocPool0Size
#define DPN_PREALLOC_POOL1_SIZE PreallocPool1Size
#define DPN_PREALLOC_POOL2_SIZE PreallocPool2Size
#define DPN_PREALLOC_POOL3_SIZE PreallocPool3Size

#define DPN_COLORBARS_OUTPUT    ColorBarsOutput

#define DPN_DBG_MASK          DbgMask


/***** LOCAL MACROS DEFINITIONS ***************************************************************************************/

/***** LOCAL TYPES AND STRUCTURES DEFINITIONS *************************************************************************/

typedef struct _DRV_PARAM
{
   /* Put here the driver parameters */
   ULONG Relay0Disable_UL;
   ULONG Relay0Enable_UL;
   ULONG Relay1Disable_UL;
   ULONG Relay1Enable_UL;

   ULONG PreallocPool0Size_UL;
   ULONG PreallocPool1Size_UL;
   ULONG PreallocPool2Size_UL;
   ULONG PreallocPool3Size_UL;

   ULONG ColorBarsOutput_UL;

   ULONG DbgMask_UL;
}DRV_PARAM;

/***** EXTERNAL VARIABLES *********************************************************************************************/
extern DRV_PARAM GL_DrvParam_X;

#ifdef ALLOCATE_EXTERN
DRV_PARAM GL_DrvParam_X = {0,0,0,0,0,0,0,0,0,0}; /* Set the default value */
#else
extern DRV_PARAM GL_DrvParam_X; 
#endif



/***** FUNCTIONS DEFINITION *******************************************************************************************/
void DrvParam_Init(PDEVICE_EXTENSION pdx);












#endif // DrvParam_h__
