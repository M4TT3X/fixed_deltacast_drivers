/********************************************************************************************************************//**
 * @file   PLX9056_RegistersMap.h
 * @date   2007/11/30
 * @Author cs
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/11/30  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _PLX9056_REGISTERSMAP_H_
#define _PLX9056_REGISTERSMAP_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "Register.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/** @addtogroup PLX9056_PCIBAR_DEF These symbols defines all PLX 9056 available PCI BAR */
//@{
#define PLX9056_PCIBAR_PLXMEM	   0	/*!<PCI BAR 0 for PLX memory-mapped registers */
#define PLX9056_PCIBAR_PLXIO	   1	/*!<PCI BAR 1 for PLX I/O mapped registers */
#define PLX9056_PCIBAR_PLXUSER0  2	/*!<PCI BAR 2 (for user purpose) */
#define PLX9056_PCIBAR_PLXUSER1	3	/*!<PCI BAR 3 (for user purpose) */
#define PLX9056_PCIBAR_PLXUSER2	4	/*!<PCI BAR 4 (for user purpose) */
#define PLX9056_PCIBAR_PLXUSER3  5	/*!<PCI BAR 5 (for user purpose) */
//@}


/** @addtogroup PLX9056_REGISTERS_MAP PLX9056 PCI Bridge Register mapping*/
//@{
#define PLX9056_LAS0RR_MM32         DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0000)   ///< Direct Slave Local Address Space 0 Range*
#define PLX9056_LAS0BA_MM32         DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0004)   ///< Direct Slave Local Address Space 0 Local Base Address (Remap)*
#define PLX9056_PROTAREA_MM8        DEFINE_MMREG8 (PLX9056_PCIBAR_PLXMEM, 0x000E)   ///< Serial E2PROM Write-Protected Address Boundary*
#define PLX9056_LBRD0_MM32          DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0018)   ///< Local Address Space 0
#define PLX9056_INTCSR_MM32         DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0068)   ///< Interrupt control register
#define PLX9056_CNTRL_MM32          DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x006C)   ///< Control register
#define PLX9056_REV_MM32            DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0074)   ///< Silicon revision
#define PLX9056_DMAMODE0_MM32       DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0080)   ///< DMA channel 0 mode
#define PLX9056_DMAPADR0_MM32       DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0084)   ///< DMA channel 0 PCI address low
#define PLX9056_DMAPADR0UP_MM32     DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00B4)   ///< DMA channel 0 PCI address up
#define PLX9056_DMALADR0_MM32       DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0088)   ///< DMA channel 0 local address
#define PLX9056_DMASIZ0_MM32        DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x008C)   ///< DMA channel 0 transfer count
#define PLX9056_DMADPR0_MM32        DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0090)   ///< DMA channel 0 descriptor pointer
#define PLX9056_DMAMODE1_MM32       DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0094)   ///< DMA channel 1 mode
#define PLX9056_DMAPADR1_MM32       DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x0098)   ///< DMA channel 1 PCI address
#define PLX9056_DMALADR1_MM32       DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x009C)   ///< DMA channel 1 local address
#define PLX9056_DMASIZ1_MM32        DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00A0)   ///< DMA channel 1 transfer count
#define PLX9056_DMADPR1_MM32        DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00A4)   ///< DMA channel 1 descriptor pointer
#define PLX9056_DMACSR0_MM8         DEFINE_MMREG8  (PLX9056_PCIBAR_PLXMEM, 0x00A8)   ///< DMA channel 0 Control/Status register 
#define PLX9056_DMACSR1_MM8         DEFINE_MMREG8 (PLX9056_PCIBAR_PLXMEM, 0x00A9)   ///< DMA channel 0 Control/Status register 
#define PLX9056_DMAAARB_MM32        DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00AC)   ///< DMA Arbitration
#define PLX9056_DMATHR_MM32         DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00B0)   ///  
#define PLX9056_DMADAC0_MM32        DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00B4)   /// DMA channel 0 PCI Dual Address Cycles Upper Address 
#define PLX9056_DMADAC1_MM32        DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00B8)   /// DMA channel 1 PCI Dual Address Cycles Upper Address 
#define PLX9056_LAS1RR_MM32         DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00F0)   ///< Direct Slave Local Address Space 1 Range*
#define PLX9056_LAS1BA_MM32         DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00F4)   ///< Direct Slave Local Address Space 1 Local Base Address (Remap)*
#define PLX9056_LBRD1_MM32          DEFINE_MMREG32 (PLX9056_PCIBAR_PLXMEM, 0x00F8)   ///< Local Address Space 1
//@}

/** @addtogroup PLX9056_DMAAARB PLX9056_DMAAARB register bit layout */
//@{
#define PLX9056_DMAAARB_BIT_PCICOMPLIANCE 		   0x01000000   /*!<PCI Compliance Enable.*/
#define PLX9056_DMAAARB_BIT_RELEASELOCALBUS 		   0x00200000   /*!<When set to 1, the PCI 9656 de-asserts LHOLD and releases the Local Bus when either of the following occurs:..*/
#define PLX9056_DMAAARB_BIT_DMACHANNEL0PRIORITY    0x00080000   /*!<DMA Channel Priority.*/
//@}

/*!\addtogroup PLX9056_DMAMODE0_LOCALBUSWIDTH32 PLX9056_DMAMODE0 register
PLX9056_DMAMODE0 register bit layout
@{*/
#define PLX9056_DMAMODE0_LOCALBUSWIDTH32 		   0x00000003    /*!<Local bus width is 32 bits*/
#define PLX9056_DMAMODE0_DUALCYCLE					0x00040000  /*! DMA Chain Mode Enable Dual Cycle Fetch */
#define PLX9056_DMAMODE0_TAREADYENABLE          0x00000040    /*!<TA# and READY# Input Enable*/
#define PLX9056_DMAMODE0_BTERM                  0x00000080    /*!<Enable BTERM*/               
#define PLX9056_DMAMODE0_LOCALBURSTMODE         0x00000100    /*!<Local burst mode*/
#define PLX9056_DMAMODE0_INTERNALWAITSTATE  	   0x00000140    /*!<Internal wait state(Local Burst)*/
#define PLX9056_DMAMODE0_SCATTERGATHERMODE      0x00000200    /*!<Scatter/Gather mode*/
#define PLX9056_DMAMODE0_INTERNALWAITSTATEDMA	0x00000240    /*!<Internal wait state(Scatter/Gather)*/
#define PLX9056_DMAMODE0_DONEINTENABLE   		   0x00000400    /*!<Enable DoneInterrupt*/
#define PLX9056_DMAMODE0_LADDRCONST       		0x00000800    /*!<Enable Local add constant*/
#define PLX9056_DMAMODE0_DEMANDMODE      		   0x00001000    /*!<Enable DMA controller running in DemandMode*/
#define PLX9056_DMAMODE0_FASTTERMINATE   		   0x00008000    /*!<Fast terminate*/
#define PLX9056_DMAMODE0_DMACHANNEL0INTSELECT   0x00020000    /*!<DMA channel0 interrupt select*/
/*!@}*/

/** @addtogroup PLX9056_DMACSR0 PLX9056_DMACSR0 register bit layout*/
//@{
#define PLX9056_DMACSRi_BIT_ENABLE        0x00000001     /*!<Enable data transfer on channel 0*/  
#define PLX9056_DMACSRi_BIT_START         0x00000002     /*!<Start data transfer on channel 0*/  
#define PLX9056_DMACSRi_BIT_ABORT         0x00000004     /*!<Abort data transfer on channel 0*/  
#define PLX9056_DMACSRi_BIT_CLEARINT      0x00000008     /*!<Clear data transfer interrupt on channel 0*/  
#define PLX9056_DMACSRi_BIT_DONE          0x00000010     /*!<DMA finished on channel 0*/
//@}

/*!\addtogroup PLX9056_INTCNTRL PLX9056_INTCNTRL register bit layout */
//@{
#define PLX9056_INTCNTRL_DMAREADMULTIPLE 		0x0000000C   /*!<DMA Memory read multiple*/
#define PLX9056_INTCNTRL_DMAWRITECYCLE       0x00000070   /*!<DMA Memory write cycle*/
#define PLX9056_INTCNTRL_PCIREADMULTIPLE 		0x00000C00   /*!<PCI Memory read multiple*/
#define PLX9056_INTCNTRL_PCIWRITECYCLE       0x00007000   /*!<PCI Memory write cycle*/
#define PLX9056_INTCNTRL_GPO      	         0x00010000   /*!<General purpose output*/
#define PLX9056_INTCNTRL_USERiI     	      0x00040000   /*!<Useri is input*/
#define PLX9056_INTCNTRL_USER0O     	      0x00080000   /*!<User0 is output*/
#define PLX9056_INTCNTRL_RELOADCONFIG        0x20000000   /*!<Reload config register*/
#define PLX9056_INTCNTRL_RESET           	   0x40000000   /*!<PCI adapter soft reset*/
#define PLX9056_INTCNTRL_ENABLEEEDO          0x80000000   /*!<Enable E2Prom Data output*/
//@}

/** @addtogroup PLX9056_INTCSR 
PLX9056_INTCSR register bit layout. */ 
//@{
#define PLX9056_INTCSR_BIT_PARITYERROR   		         0x00000080   /*!<PCI local data parity error clear*/
#define PLX9056_INTCSR_BIT_PCI_INT_ENABLE			      0x00000100   /*!<PCI interrupt enable*/
#define PLX9056_INTCSR_BIT_LOCALBUS_INT_ENABLE		   0x00000800   /*!<Local interrupt input enable*/
#define PLX9056_INTCSR_BIT_LOCALBUS_INT_ACTIVE		   0x00008000   /*!<Local interrupt input active*/
#define PLX9056_INTCSR_BIT_LOCALBUS_OUTPUT_ENABLE		0x00010000   /*!<Local interrupt output pin enable*/
#define PLX9056_INTCSR_BIT_DMA0_INT_ENABLE		      0x00040000   /*!<DMA Channel 0 Interrupt Enable*/
#define PLX9056_INTCSR_BIT_DMA1_INT_ENABLE		      0x00080000   /*!<Local DMA channel 1 interrupt enable*/
#define PLX9056_INTCSR_BIT_DMA0_INT_ACTIVE            0x00200000   /*!<Channel 0 DMA interrupt active*/
#define PLX9056_INTCSR_BIT_DMA1_INT_ACTIVE            0x00400000   /*!<Channel 1 DMA interrupt active*/
//@}

/* PLX9056_DMADPR0_MM32 */
#define PLX9056_DMADPR0_BIT_DIR                       0x00000008     /*!<Setup DMA transfer direction*/


#define PLX9056_CNTRL_BIT_EEPROMCLOCK              0x01000000   /*@Toggling this bit asserts the serial EEPROM clock*/
#define PLX9056_CNTRL_BIT_EEPROMCHIPSEL            0x02000000   /*@Serial EEPROM Chip Select*/
#define PLX9056_CNTRL_BIT_EEPROMWRITEBIT           0x04000000   /*@Output bit to input to the serial EEPROM*/
#define PLX9056_CNTRL_BIT_EEPROMREADBIT            0x08000000   /*@Serial EEPROM output to input bit*/
#define PLX9056_CNTRL_BIT_PROGEEPROMPRESENT        0x10000000   /*@Programmed serial EEPROM presence bit*/

#endif // _PLX9056_REGISTERSMAP_H_
