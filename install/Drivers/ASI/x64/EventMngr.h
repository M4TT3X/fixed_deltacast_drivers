/********************************************************************************************************************//**
 * @internal
 * @file   	EventMngr.h
 * @date   	2011/09/14
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2011/09/14  v00.01.0000    cs       Creation of this file
   2013/03/29  v00.01.0001    bc       Replace KOBJ_SetSyncEvent by KOBJ_SetSyncEventByDPC

 **********************************************************************************************************************/

#ifndef _EVENTMNGR_H_
#define _EVENTMNGR_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "DrvTypes.h"
#include "KernelObjects.h"
#include "EventMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define MAX_EVENT_ID 32

/***** MACROS DEFINITIONS *********************************************************************************************/

#define GetEventMngrData() (&pdx->EventMngrData_X) ///< Macro to retrieve Event Manager data from the device context.

/** @name Event Manager safe area
 The Event manager shares data between different execution context (multi-user, DPC, ...). The access to these shared data
 can be safe using a local safe area that can be used through this set of macros.
*///@{

#define EVENTMNGR_INIT_SAFEAREA()            KOBJ_InitializeSpinLock(pdx,&GetEventMngrData()->SpinLock_KO);
#define EVENTMNGR_ENTER_SAFEAREA()           KOBJ_AcquireSpinLock(pdx,&GetEventMngrData()->SpinLock_KO)
#define EVENTMNGR_LEAVE_SAFEAREA()           KOBJ_ReleaseSpinLock(pdx,&GetEventMngrData()->SpinLock_KO);
#define EVENTMNGR_ENTER_SAFEAREA_DPC()       KOBJ_AcquireSpinLockAtDPCLevel(pdx,&GetEventMngrData()->SpinLock_KO)
#define EVENTMNGR_LEAVE_SAFEAREA_DPC()       KOBJ_ReleaseSpinLockAtDPCLevel(pdx,&GetEventMngrData()->SpinLock_KO);

//@}


/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
typedef struct
{
   KOBJ_SYNC_EVENT pEventTable_KO[MAX_EVENT_ID];//PKEVENT        pKEventTable_X[MAX_EVENT_ID];
   PID_TYPE        pPID_UL[MAX_EVENT_ID];
   ULONG           pAssociatedIntSource_UL[MAX_EVENT_ID];
   ULONG           EventUsed_UL;
   ULONG           EventToSetByDPCHandler_UL;
   KOBJ_SPINLOCK   SpinLock_KO;	
} EVENT_MNGR_DATA;

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void           EventMngr_DPCHandler(PDEVICE_EXTENSION pdx, ULONG IntSource_UL);
EVENT_ID       EventMngr_CreateEvent(PDEVICE_EXTENSION pdx);
void           EventMngr_DeviceAdd(PDEVICE_EXTENSION pdx);
void           EventMngr_OSDeviceAdd(PDEVICE_EXTENSION pdx);
void           EventMngr_Close(PDEVICE_EXTENSION pdx, EVENT_ID EventId_e);
void           EventMngr_SetEvent(PDEVICE_EXTENSION pdx, EVENT_ID EventId_e);
void           EventMngr_SetByDPCHandler(PDEVICE_EXTENSION pdx, EVENT_ID EventId_e);
BOOL32         EventMngr_RegisterIntEvent(PDEVICE_EXTENSION pdx, EVENT_ID EventId_e, ULONG IntSource_UL);
IOCTL_STATUS   EventMngr_IoCtl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,PID_TYPE PID_UL, void * pSysParam_v);
BOOL32         EventMngr_WaitOnEvent(PDEVICE_EXTENSION pdx,EVENT_ID Event_e);
BOOL32         EventMngr_WaitOnEvent_Timeout(PDEVICE_EXTENSION pdx,EVENT_ID Event_e, ULONG Timeout_UL);
void           EventMngr_CleanUp(PDEVICE_EXTENSION pdx, PID_TYPE PID_UL, BOOL32 DevicePresent_B);
void           EventMngr_ResetEvent(PDEVICE_EXTENSION pdx, EVENT_ID EventId_e);

#if defined(__linux__) || defined(__APPLE__)
EVENT_ID EventMngr_Open(PDEVICE_EXTENSION pdx, PID_TYPE PID_UL);
#else
EVENT_ID EventMngr_OpenByHandle(PDEVICE_EXTENSION pdx,HANDLE Event_H, KPROCESSOR_MODE AccessMode,PID_TYPE PID_UL);
void EventMngr_CloseByHandle(PDEVICE_EXTENSION pdx, EVENT_ID EventId_e);
#endif
#endif // _EVENTMNGR_H_
