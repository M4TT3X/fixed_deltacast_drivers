/********************************************************************************************************************//**
 * @internal
 * @file   	ASIDRV_Properties.h
 * @date   	2010/08/26
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/08/26  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _ASIDRV_PROPERTIES_H_
#define _ASIDRV_PROPERTIES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "StreamProperties.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define ASIDRV_BOARD_FEATURES_LOW_PROFILE 0x000000001


#define ASIDRV_RELAYx_DISABLE_VAL_BYUSER       0
#define ASIDRV_RELAYx_DISABLE_VAL_FGPALOADED   1
#define ASIDRV_RELAYx_DISABLE_VAL_BOARDOPEN    2
#define ASIDRV_RELAYx_ENABLE_VAL_BYUSER        0
#define ASIDRV_RELAYx_ENABLE_VAL_DRIVERUNLOAD  1
#define ASIDRV_RELAYx_ENABLE_VAL_BOARDCLOSED   2


/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef enum
{
   ASIDRV_BOARD_TYPE_PPA,
   ASIDRV_BOARD_TYPE_ASI8
} ASIDRV_BOARD_TYPE;


typedef enum
{
   ASIDRV_BOARD_FIRMWAREID_UNKOWN,    
   ASIDRV_BOARD_FIRMWAREID_PPA,
   ASIDRV_BOARD_FIRMWAREID_ASI8,
}ASIDRV_BOARD_FIRMWAREID;

typedef enum
{
   ASIDRV_PROPERTIES_BOARD_TYPE=NB_STREAM_PROPERTIES,
   ASIDRV_PROPERTIES_BOARD_FEATURES,
   ASIDRV_PROPERTIES_BOARD_FIRMWAREID,
   ASIDRV_PROPERTIES_BOARD_FIRMWARE_VERSION,

   ASIDRV_PROPERTIES_RELAY0_DISABLE,
   ASIDRV_PROPERTIES_RELAY0_ENABLE,
   ASIDRV_PROPERTIES_RELAY1_DISABLE,
   ASIDRV_PROPERTIES_RELAY1_ENABLE,


   NB_ASIDRV_PROPERTIES
} ASIDRV_PROPERTIES;


#define ASIDRV_BOARD_FEATURES_RELAY_0            0x00001000
#define ASIDRV_BOARD_FEATURES_RELAY_1            0x00002000


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _ASIDRV_PROPERTIES_H_
