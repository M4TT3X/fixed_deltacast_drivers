/********************************************************************************************************************//**
 * @file   	DMAMngrTypes.h
 * @date   	2007/07/18
 * @author 	cs
 * @version v00.01.0000 
 * @brief   This file contains needed type for DMA manager. This file is shared between driver and user space.
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/07/18  v00.01.0000    cs       Creation of this file
   2009/06/03  v00.02.0000    cs       Add new DMA request flag to be able to specify the target DMA queue
 **********************************************************************************************************************/

#ifndef _DMAMNGRTYPES_H_
#define _DMAMNGRTYPES_H_   

/***** INCLUDES SECTION ***********************************************************************************************/

#include "EventMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define DMA_REQUEST_FLAGS_DIR             0x00000001
#define DMA_REQUEST_FLAGS_DIR_RCV         0x00000000  // Set DMA request as Receive  DMA (LB->PCI)
#define DMA_REQUEST_FLAGS_DIR_XMT         0x00000001  // Set DMA request as Transmit DMA (PCI->LB)
#define DMA_REQUEST_FLAGS_STATIC_PCI_ADDR 0x00000002  // Disable Auto-Increment address on PCI side (if available)
#define DMA_REQUEST_FLAGS_STATIC_LB_ADDR  0x00000004  // Disable Auto-Increment address on Local Bus side (if available)
#define DMA_REQUEST_FLAGS_DMAQUEUE_MASK   0x0000FF00  // Defines the target DMA queue
#define DMA_REQUEST_FLAGS_RESERVED_MASK   0xFF000000  // Reserved

#define INVALID_REQUEST_DONE_ID           0xFFFFFFFF
#define MAX_DMA_REQUEST_USER_DATA         4           // Defines the number of "user purpose ULONG Values attached to a DMA request 

/***** MACROS DEFINITIONS *********************************************************************************************/

#define DMA_REQUEST_FLAGS_DMAQUEUE(dmaqueue)     ((dmaqueue<<8)&DMA_REQUEST_FLAGS_DMAQUEUE_MASK)  // Marco to help to fill correctly the dma queue field in the DMA flags.
#define GET_DMA_REQUEST_FLAGS_DMAQUEUE(dmaflags) ((dmaflags&DMA_REQUEST_FLAGS_DMAQUEUE_MASK)>>8)  // Marco to help to retrieve the dma queue out of the DMA flags.

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

#pragma pack(push, 4)

typedef struct
{
   ULONG pUserData_UL[MAX_DMA_REQUEST_USER_DATA];
}DMA_REQUEST_USER_DATA;

typedef enum
{
   DMA_REQUEST_CONFIG_TYPE_ADDR_0_AND_START = 0,
   DMA_REQUEST_CONFIG_TYPE_ADDR_0,
   DMA_REQUEST_CONFIG_TYPE_ADDR_1,
   DMA_REQUEST_CONFIG_TYPE_ADDR_2,
   DMA_REQUEST_CONFIG_TYPE_ADDR_3,
   DMA_REQUEST_CONFIG_TYPE_ADDR_4,
   DMA_REQUEST_CONFIG_TYPE_ADDR_5,
   DMA_REQUEST_CONFIG_TYPE_ADDR_6,
   DMA_REQUEST_CONFIG_TYPE_ADDR_7,
   DMA_REQUEST_CONFIG_TYPE_DDR_ACCESS_LIMIT, ///< Nbr of DDR accesses (64B) before jump 
   DMA_REQUEST_CONFIG_TYPE_PADDING_LIMIT,
   DMA_REQUEST_CONFIG_TYPE_POS_PADDING_EN,
   DMA_REQUEST_CONFIG_TYPE_NEG_PADDING_EN,
   DMA_REQUEST_CONFIG_TYPE_NEG_PADDING_SIZE,
   DMA_REQUEST_CONFIG_TYPE_SET_LOCAL_ADDR,   ///< nbr of local addresses
   DMA_REQUEST_CONFIG_TYPE_CHANGE_SET_LIMIT, ///< Nbr of A before change of set
   DMA_REQUEST_CONFIG_TYPE_START
}DMA_REQUEST_CONFIG_TYPE;

typedef struct
{
   ULONG DMABufID_UL;                                                                  ///< Gives the DMA buffer ID for the source or the destination.
   ULONG Offset_UL;                                                                    ///< Specifies an offset inside the given buffer ID. The offset has to be a multiple of 4096 bytes.
   ULONG LBAddr_UL;                                                                    ///< Target address
   ULONG Count_UL;                                                                     ///< DMA transfer size (in bytes)
   ULONG Flags_UL;                                                                     ///< Flags that define the request
   EVENT_ID EventID;                                                                   ///< An event ID attached to the request that be set once the request will be done.
   ULONG RequestDoneID_UL;                                                             ///< This ID is returned by the DMA Done Handler once a request has finished
   DMA_REQUEST_USER_DATA UserData_X;                                                   ///< USer data attached to the request. These data follow the request from its in-queuing unitl the request is done.
   ULONG LBCount_UL;
   DELTA_VOID pADVLPAddr_UL;
   DELTA_VOID pADVLPParam_UL;
} DMA_REQUEST;

typedef struct  
{
   ULONG DMAQueueIdx_UL;   ///< Specifies the target DMA queue to apply the setup
   ULONG MaxDMASize_UL;    ///< Specifies the maximum DMA transfer size before splitting the request
   ULONG Priority_UL;      ///< Specifies the DMA queue priority, the higher the value is, the higher the priority is.
   ULONG pReserved_UL[4];  ///< For Future use (priority definition , selection algorithm rules, ...)
} DMA_QUEUE_SETUP;

typedef struct 
{
   ULONG       StructSize_UL;
   ULONG       DMAChannel_UL;
   DMA_REQUEST DMARequest_X;
} IOCTL_MULTICHANNEL_ADD_DMA_REQUEST_PARAM;

typedef struct 
{
   ULONG           StructSize_UL;
   ULONG           DMAChannel_UL;
   DMA_QUEUE_SETUP DMAQueueSetup_X;
} IOCTL_MULTICHANNEL_DMA_QUEUE_SETUP_PARAM;

typedef struct 
{
   ULONG           StructSize_UL;
} IOCTL_MULTICHANNEL_ABORT_DMA_PARAM;


#pragma pack(pop)

#endif // _DMAMNGRTYPES_H_

