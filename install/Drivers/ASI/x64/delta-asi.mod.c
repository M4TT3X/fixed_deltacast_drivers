#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x59101cf6, "module_layout" },
	{ 0x1fedf0f4, "__request_region" },
	{ 0xb3c84ca9, "cdev_del" },
	{ 0xf5d6b4d, "kmalloc_caches" },
	{ 0xa5ad4eb1, "cdev_init" },
	{ 0xf9a482f9, "msleep" },
	{ 0xd6147ae2, "up_read" },
	{ 0x4c4fef19, "kernel_stack" },
	{ 0xd6ee688f, "vmalloc" },
	{ 0xb85f3bbe, "pv_lock_ops" },
	{ 0x69a358a6, "iomem_resource" },
	{ 0x25ec1b28, "strlen" },
	{ 0x9db6a5ed, "dev_set_drvdata" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0x4aabc7c4, "__tracepoint_kmalloc" },
	{ 0x670c0597, "down_interruptible" },
	{ 0x2df59cec, "device_destroy" },
	{ 0x7485e15e, "unregister_chrdev_region" },
	{ 0x999e8297, "vfree" },
	{ 0x592b9cd7, "down_read" },
	{ 0xe174aa7, "__init_waitqueue_head" },
	{ 0x4f8b5ddb, "_copy_to_user" },
	{ 0x8c55139d, "pci_set_master" },
	{ 0xde0bdcff, "memset" },
	{ 0x480bd6bb, "kmem_cache_alloc_notrace" },
	{ 0x88941a06, "_raw_spin_unlock_irqrestore" },
	{ 0xd92efe9f, "current_task" },
	{ 0xea147363, "printk" },
	{ 0x8fa768f4, "kthread_stop" },
	{ 0xfaef0ed, "__tasklet_schedule" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0x9f6e19ab, "mem_section" },
	{ 0x39e27f81, "device_create" },
	{ 0x9545af6d, "tasklet_init" },
	{ 0xfda85a7d, "request_threaded_irq" },
	{ 0xb8c21336, "cdev_add" },
	{ 0x42c8de35, "ioremap_nocache" },
	{ 0x1ee2b5c, "pci_bus_read_config_word" },
	{ 0x93fca811, "__get_free_pages" },
	{ 0x30612cd3, "get_user_pages" },
	{ 0x108e8985, "param_get_uint" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x1000e51, "schedule" },
	{ 0xf38f2ef9, "pv_cpu_ops" },
	{ 0xbfb2b667, "wake_up_process" },
	{ 0x7c61340c, "__release_region" },
	{ 0xb429978e, "pci_unregister_driver" },
	{ 0x6443d74d, "_raw_spin_lock" },
	{ 0x587c70d8, "_raw_spin_lock_irqsave" },
	{ 0xe52947e7, "__phys_addr" },
	{ 0x4302d0eb, "free_pages" },
	{ 0xf09c7f68, "__wake_up" },
	{ 0xd2965f6f, "kthread_should_stop" },
	{ 0x506746b6, "getrawmonotonic" },
	{ 0x37a0cba, "kfree" },
	{ 0x3ae00ded, "kthread_create" },
	{ 0x79e0434e, "remap_pfn_range" },
	{ 0x236c8c64, "memcpy" },
	{ 0xe75663a, "prepare_to_wait" },
	{ 0x3285cc48, "param_set_uint" },
	{ 0xedc03953, "iounmap" },
	{ 0x57b09822, "up" },
	{ 0x78d5d0f, "__pci_register_driver" },
	{ 0x737c1563, "put_page" },
	{ 0x867f2d8a, "class_destroy" },
	{ 0x47a56ac7, "request_firmware" },
	{ 0xb00ccc33, "finish_wait" },
	{ 0xdad99aa1, "pci_enable_device" },
	{ 0x4f6b400b, "_copy_from_user" },
	{ 0x6a807896, "__class_create" },
	{ 0x956ae612, "dev_get_drvdata" },
	{ 0xfeb36d66, "release_firmware" },
	{ 0x9e7d6bd0, "__udelay" },
	{ 0x29537c9e, "alloc_chrdev_region" },
	{ 0xf20dabd8, "free_irq" },
	{ 0xe914e41e, "strcpy" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v000010B5d00002840sv*sd*bc*sc*i*");
