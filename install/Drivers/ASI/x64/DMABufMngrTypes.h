/********************************************************************************************************************//**
 * @internal
 * @file   	DMABufMngrTypes.h
 * @date   	2010/07/08
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/08  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _DMABUFMNGRTYPES_H_
#define _DMABUFMNGRTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "CTypes.h"
//#include "CommonBufMngrTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

#define INVALID_BUFID            (BUFID)0xFFFFFFFF
#define INVALID_DMABUFID ((DMABUFID)0xFFFFFFFF)


#define DMABUF_MAX_NB_SLAVES           27
#define MAX_DMABUF        256*(1+DMABUF_MAX_NB_SLAVES)  ///< Defines the maximum number of DMABUF handled by the DMABUF manager at a time

/** @defgroup DMABUF_TYPE_GROUP Available DMA buffer types
 * These symbols defines the different buffer type handled by the DMA Buffer manager. The type information is embedded in the DMA buffer ID (DMABUFID).
 ****///@{
#define DMABUF_TYPE_COMMONBUF 1  ///< Common buffer type
#define DMABUF_TYPE_USBUF     2  ///< Userspace buffer type
//@}


/** @name DMABUF_MACROS Set of macros related to the DMA Buffer ID
 *
 * The DMA buffer ID is a 32-bit word using only 24-bit of data (the 3 LSBytes) that embeds some information about the buffer itself. For more details about DMA buffer management, 
 * refer to @ref DMABUFMNGR_PAGE "this chapter".
 ***///@{
#define DMABUFID_Create(type, id)  ((((type)&0xF)<<20)|((id)&0xFFFF))  ///< Creates a DMABUFID from an ID and a type (chossen among @ref DMABUF_TYPE_GROUP "these values").    
#define DMABUFID_GetType(DMABufID) ((DMABufID>>20)&0xF)
#define DMABUFID_GetId(DMABufID)   ((DMABufID)&0xFFFF)
//@}



#define DMABUF_FLAGS_FREE                    0x00000000  ///< The corresponding DMA buffer entry is empty
#define DMABUF_FLAGS_USED                    0x00000001  ///< The DMA buffer contains information related to an allocated buffer
#define DMABUF_FLAGS_LOCK_BY_SYSTEM_CNT_MASK 0x0000FF00  ///< The buffer is marked as "used by system", it can't be released
#define DMABUF_FLAGS_LOCK_BY_SYSTEM          0x00000010  ///< The buffer is marked as "used by system", it can't be released
#define DMABUF_FLAGS_MARKED_AS_TO_FREE       0x00000020  ///< The buffer is marked as "to free" after an attempt to release when it was locked by the system
#define DMABUF_FLAGS_READY_TO_FREE           0x00000040  ///< The buffer marked as "to free" if now ready to be freed.
#define DMABUF_FLAGS_USER(idx)               (0x00010000<<(idx))


/***** MACROS DEFINITIONS *********************************************************************************************/

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

typedef ULONG BUFID;
typedef ULONG DMABUFID;



typedef enum
{
   DMABUF_ACCESS_TYPE_LINK_BUFFER,
   DMABUF_ACCESS_TYPE_GET_FLAGS,
   DMABUF_ACCESS_TYPE_SETBITS_FLAGS,
   DMABUF_ACCESS_TYPE_CLRBITS_FLAGS,
} DMABUF_ACCESS_TYPE;


#pragma pack(push, 4)

typedef struct  
{
   ULONG              StructSize_UL;
   DMABUF_ACCESS_TYPE Type_e;

   union
   {
      struct DMABUF_LINK_PARAM
      {
         DMABUFID SlaveBufID_UL;
         DMABUFID MasterBufID_UL;
      } LinkParam_X;

      struct DMABUF_FLAGS_PARAM
      {
         DMABUFID BufID_UL;
         ULONG Flags_UL;
      } FlagsParam_X;
   } Params;
} IOCTL_DMABUF_ACCESS_PARAM;

#pragma pack(pop)

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/




#endif // _DMABUFMNGRTYPES_H_
