#ifndef OS_H_HH__H
#define OS_H_HH__H

#if defined(__linux__)

#  include <semaphore.h>
#  include <pthread.h>
#  include "CTypes.h"
   typedef char * LPCTSTR;
   typedef void * LPVOID;
#ifndef TCHAR_DEFINED
   typedef char TCHAR;
#define TCHAR_DEFINED
#endif

   typedef pthread_t THREAD_ID;
   
  // typedef unsigned long DWORD;

void MilliSleep(unsigned long dwMiliseconds);

unsigned long GetMilliCount();

#elif defined(__APPLE__)

#include "CTypes.h"
#include <semaphore.h>
#include <sys/shm.h>

typedef char * LPCTSTR;
typedef void * LPVOID;
#ifndef TCHAR_DEFINED
typedef char TCHAR;
#define TCHAR_DEFINED
#endif

void MilliSleep(ULONG Miliseconds_UL);

ULONG GetMilliCount();

#else
#  include <windows.h>

#  define GetMilliCount GetTickCount
#  define MilliSleep    Sleep

   typedef DWORD THREAD_ID;
   typedef BOOL BOOL32;



#endif

/* Addendum 2011-15-12, thread identification support, used by the GPU extension */
#ifdef __linux__  

   THREAD_ID GetCurrentThreadId();
#else
   
   /* The GetCurrentThreadId() exists in Windows.h, nothing to do ! */
#endif

/*** MACROS ******************************************************************/

#ifndef SAFE_FREE
#define SAFE_FREE(Pointer_X) if (Pointer_X) { free(Pointer_X); Pointer_X = NULL; }
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(Object_O) if (Object_O) { Object_O->Release(); Object_O = NULL; }
#endif
#ifndef SAFE_DELETE
#define SAFE_DELETE(Pointer_X) if (Pointer_X) { delete Pointer_X; Pointer_X = NULL; }
#endif
#ifndef SAFE_DELETA
#define SAFE_DELETA(Object_O) if (Object_O) { delete[] Object_O; Object_O = NULL; }
#endif
#ifndef SAFE_CLOSE
#define SAFE_CLOSE(Handle_h) if (Handle_h && (Handle_h != INVALID_HANDLE_VALUE)) { CloseHandle(Handle_h); Handle_h = NULL; }
#endif
#ifndef SAFE_CLOSEF
#define SAFE_CLOSEF(Handle_h) if (Handle_h && (Handle_h != INVALID_HANDLE_VALUE)) { CloseHandle(Handle_h); Handle_h = INVALID_HANDLE_VALUE; }
#endif




/*** CONSTANTS ***************************************************************/
#ifndef INVALID_HANDLE
#define INVALID_HANDLE  0xFFFFFFFF
#endif








class DTMutex
{
private:

#if defined(__linux__)
   BOOL32 mHandleCreated_B;
   sem_t *mpSemaphore_X;
#elif defined(__APPLE__)
   sem_t *mpSemaphore_X;
   BOOL32 mHandleCreated_B;
#else
   HANDLE mTheWindowsHandle;
   PSID mpEveryoneSID;
   PACL mpACL;
   PSECURITY_DESCRIPTOR mpSD;
   SECURITY_ATTRIBUTES msa;
#endif



public:
   DTMutex();
   ~DTMutex();
   DWORD Init(const TCHAR *lpName,BOOL32 DestroyExisting_B=FALSE);
   DWORD ReleaseMutex();
   DWORD WaitOn(DWORD dwMiliSecondsTimeOut);
   BOOL32 IsValid();
   void Close();
   
};










class DTFileMap
{
private:

#if defined(__linux__)
   BOOL32 mHandleCreated_B;
   int mFileDescriptor_i;
#elif defined(__APPLE__)
   int mShMem_i;
#else
   HANDLE mTheWindowsHandle;
   PSID mpEveryoneSID;
   PACL mpACL;
   PSECURITY_DESCRIPTOR mpSD;
   SECURITY_ATTRIBUTES msa;
#endif



public:
   DTFileMap();
   ~DTFileMap();
   DWORD Init(DWORD Size_UL,const TCHAR * lpName, BOOL32 *pWasFirstCreated_B=NULL);
   DWORD UnmapPointer(LPVOID pMapViewOfFile);
   DWORD MapPointerOn(LPVOID *ppMapViewOfFile);
   void Close();

};















#define CheckPtr(a,b) ((!(a)))


typedef class DTFileMap FILEMAP;
typedef class DTMutex MUTEX;




#endif
