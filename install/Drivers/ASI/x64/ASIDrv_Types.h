/********************************************************************************************************************//**
 * @internal
 * @file   	BoardTypes.h
 * @date   	2010/07/16
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/07/16  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _ASIDRVTYPES_H_
#define _ASIDRVTYPES_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/

/** @defgroup ASIDRV_BRD_TYPES_GRP Types and symbols for ASI driver (ASIDrv driver) */

/** @addtogroup ASIDRV_BRD_TYPES_GRP *///@{

#define ASIBRD_NB_ONBRD_BUF 2 ///< Defines the number of on board buffers per filed type.

/** @name BRD_DMARQST_USRDATA_IDX Definition of the index for DMA request user data table
    *///@{
//#define ASIBRD_DMARQST_USRDATA_IDX_FLAGS       0   ///< This user data attached to a DMA request gives some flags specific to the board.
//@}


/** @name ASIDRV_BUFQUEUE_DATA_TYPE_INDEX_GRP BUffer queue data type indexes
    These symbols defines the indexes of the buffer queue data types for all supported data types. */
//@{
typedef enum
{
   ASIBRD_BUFQ_DATA_TYPE_TS,     ///< Data type index corresponding to the odd field video data
}ASIBRD_BUFQ_DATA_TYPE;
//@}


/** @name SDDDRV_BUFQUEUE_USER_PARAM_IDX_GRP Buffer Queue Parameters Index list
    These symbols defines the indexes for the buffer queue user parameters. *///@{
#define ASIBRD_BUFQUEUE_USRPARAM_IDX_DATACONTENT                           0           ///< Defines the data handled by the buffer queue (Refers to the @ref ASIDRV_BUFQUEUE_DATA_TYPE_GRP "available values")
#define ASIBRD_BUFQUEUE_USRPARAM_IDX_CHANNEL_INDEX                         1           ///< Stores the channel index
#define ASIBRD_BUFQUEUE_USRPARAM_IDX_CURRENT_ONBRD_BUF_IDX                 2          ///< Keeps the current on board buffer index
#define ASIBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE                   3           
#define ASIBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_TS_BASE             (ASIBRD_BUFQUEUE_USRPARAM_IDX_ONBRD_BUF_ADDR_BASE)
//@}




/** @name ASIDRV_BUFQUEUE_DATA_TYPE_GRP Available values for SDDrv buffer queue data type 
 
 @remark : To simplify internal processing, the data content value must match with the buffer queue data type. In this case, the video data content
           embedded 2 buffer queue data types (BUFQ_DATA_TYPE_VIDEO_ODD and BUFQ_DATA_TYPE_VIDEO_EVEN) as well as the ANC data content.
           So, these symbols are defined by a value with 2 bits set to 1.
    
 *///@{
#define ASIBRD_BUFQUEUE_DATACONTENT_TS 0x1   
//@}



#define ASIBRD_BUFHDR_USRIDX_TIMESTAMP        0x0
#define ASIBRD_BUFHDR_STS_FRAMING_ERROR       0x1


#define ASIBRD_DMA_PADDING_SIZE 64
#define ASIBRD_DMA_ADDR_ALIGN   4

//@}



/***** MACROS DEFINITIONS *********************************************************************************************/

// Move to CTypes.h #define ROUND_TO_x(Size, x)  (((ULONG)(Size) + x - 1) & ~(x - 1))
#define ASIBRD_PAD_DMA_SIZE(Size)    ROUND_TO_x(Size, ASIBRD_DMA_PADDING_SIZE)
#define ASIBRD_ALIGN_DMA_ADDR(Size)  ROUND_TO_x(Size, ASIBRD_DMA_ADDR_ALIGN)


/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _SDDRVTYPES_H_
