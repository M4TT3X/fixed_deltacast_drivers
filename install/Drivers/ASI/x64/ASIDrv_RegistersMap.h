#ifndef _REGISTERS_MAP_H_
#define _REGISTERS_MAP_H_

#include "Register.h"
#include "PLX9056_RegistersMap.h"


#define NBOF_CHANNEL_BY_FPGA        4

/* This enumeration contains symbols that defines all needed shadow registers */
typedef enum
{
   IDX_FPGA_IMR_SHRMM32,
   IDX_FPGA_GCMD_SHRMM32,
   IDX_FPGA1_IMR_SHRMM32,
   IDX_FPGA1_GCMD_SHRMM32,
   IDX_FPGA_GPIO_CTRL_SHRMM32,
   IDX_FPGA_WDT_SHRMM32,
   IDX_NB
} ASIDRV_SHREG32;

/***** SYMBOLS DEFINITIONS *******************************************************************************************/

#define USER_PCIBAR_IOMAP  PLX9056_PCIBAR_PLXUSER0
#define USER_PCIBAR_MEMMAP PLX9056_PCIBAR_PLXUSER1

#define FPGA_DMA_DATA_MM32_OFFSET      0x800c
#define FPGA1_DMA_DATA_MM32_OFFSET      0x1800c

/** @defgroup REGISTER_MAP_GROUP FPGA and PLD Registers Map */

/** @ingroup REGISTER_MAP_GROUP 
@{ */
#define CFGPLD_CMD_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0000)    ///<FPGA downloader command register
#define CFGPLD_STATUS_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0000)    ///<FPGA downloader status register
#define CFGPLD_DATA_MM32         DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0004)    ///<FPGA downloader data register
#define CFGPLD_TST_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0008)    ///<PLD test register
#define CFGPLD_REV0_MM8          DEFINE_MMREG8 (USER_PCIBAR_MEMMAP, 0x0010)    ///<PLD revision id: Version
#define CFGPLD_REV1_MM8          DEFINE_MMREG8 (USER_PCIBAR_MEMMAP, 0x0014)    ///<PLD revision id: Year
#define CFGPLD_REV2_MM8          DEFINE_MMREG8 (USER_PCIBAR_MEMMAP, 0x0018)    ///<PLD revision id: Month
#define CFGPLD_REV3_MM8          DEFINE_MMREG8 (USER_PCIBAR_MEMMAP, 0x001C)    ///<PLD revision id: Day


#define FPGA_GCMD_SHRMM32        DEFINE_SHRMMREG32 (USER_PCIBAR_MEMMAP, 0x0020)     ///<General Command Register
#define FPGA_GSTS_MM32           DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0020)        ///<General Status Register
#define FPGA_REVID_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0024)        ///<Fpga revision id
#define FPGA_IMR_SHRMM32         DEFINE_SHRMMREG32 (USER_PCIBAR_MEMMAP, 0x0028)     ///<Interrupt Mask Register
#define FPGA_SR_MM32             DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0028)        ///<Status Register
#define FPGA_ICR_MM32            DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x002C)        ///<Interrupt Clear Register
#define FPGA_ISR_MM32            DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x002C)        ///<Interrupt Status Register
#define FPGA_TAP_A_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x8000)        ///<SDRAM Test Access Port Address
#define FPGA_TAP_D_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x8004)        ///<SDRAM Test Access Port Data
#define FPGA_STC_LSW_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0030)        ///<System Time Clock Least Significant 23 bits (33 bits timer from 90 Khz reference) and 9 bit prescaler value (0 ..299)
#define FPGA_STC_MSW_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0034)        ///<System Time Clock 10 Most Significant Bits
#define FPGA_SSN_LSW_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0038)        ///<Silicon Serial Number LSB
#define FPGA_SSN_MSW_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x003C)        ///<Silicon Serial Number MSB
#define FPGA_DMA_A_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x8008)        ///<DMA Start Address for transfers
#define FPGA_DMA_DATA_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x800C)        ///<DMA DATA access port for channel transfers


#define FPGA_RX0_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0040)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA_RX0_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0044)		   ///<Channel Bit Rate Deviation
#define FPGA_RX0_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x004C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA_RX0_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00C0)		   ///<Channel Command Register
#define FPGA_RX0_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00C0)		   ///<Channel Status Register
#define FPGA_RX0_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0040)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA_RX1_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0050)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA_RX1_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0054)		   ///<Channel Bit Rate Deviation
#define FPGA_RX1_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x005C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA_RX1_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00C4)		   ///<Channel Command Register
#define FPGA_RX1_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00C4)		   ///<Channel Status Register
#define FPGA_RX1_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0050)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA_RX2_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0060)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA_RX2_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0064)		   ///<Channel Bit Rate Deviation
#define FPGA_RX2_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x006C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA_RX2_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00C8)		   ///<Channel Command Register
#define FPGA_RX2_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00C8)		   ///<Channel Status Register
#define FPGA_RX2_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0060)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA_RX3_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0070)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA_RX3_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0074)		   ///<Channel Bit Rate Deviation
#define FPGA_RX3_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x007C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA_RX3_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00CC)		   ///<Channel Command Register
#define FPGA_RX3_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00CC)		   ///<Channel Status Register
#define FPGA_RX3_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0070)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA_TX0_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00A0)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA_TX0_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00A4)		   ///<Channel Bit Rate Deviation
#define FPGA_TX0_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00AC)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA_TX0_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00D8)		   ///<Channel Command Register
#define FPGA_TX0_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00D8)		   ///<Channel Status Register
#define FPGA_TX0_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00A0)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA_TX1_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00B0)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA_TX1_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00b4)		   ///<Channel Bit Rate Deviation
#define FPGA_TX1_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00BC)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA_TX1_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00DC)		   ///<Channel Command Register
#define FPGA_TX1_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00DC)		   ///<Channel Status Register
#define FPGA_TX1_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00B0)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA_TX2_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0080)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA_TX2_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0084)		   ///<Channel Bit Rate Deviation
#define FPGA_TX2_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x008C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA_TX2_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00D0)		   ///<Channel Command Register
#define FPGA_TX2_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00D0)		   ///<Channel Status Register
#define FPGA_TX2_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0080)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA_TX3_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0090)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA_TX3_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0094)		   ///<Channel Bit Rate Deviation
#define FPGA_TX3_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x009C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA_TX3_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00D4)		   ///<Channel Command Register
#define FPGA_TX3_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00D4)		   ///<Channel Status Register
#define FPGA_TX3_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x0090)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA_WDT_SHRMM32	      DEFINE_SHRMMREG32 (USER_PCIBAR_MEMMAP, 0x00F0)	   ///<Watchdog timer register :  time-out value  (50MHz clock)
#define FPGA_WDC_MM32	         DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00F4)	      ///<Watchdog control register :�	WACT : 0x57414354�	WARM : 0x5741524D�	WDIS : 0x57444953Set bit 31 of  WARM to �1� when you want watchdog to trigger relay command instead of active loopback.=> 0xD741524D to arm watchdog to trigger relays
#define FPGA_VCXO_MM32	         DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00F8)	      ///<[7:0] : 27 Mhz VCXO Control	   
#define FPGA_GPIO_CTRL_SHRMM32   DEFINE_SHRMMREG32 (USER_PCIBAR_MEMMAP, 0x00FC)     ///<GPIO Control and Read Back Register
#define FPGA_GPIO_STS_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x00FC)     ///<GPIO Control and Read Back Register

#define FPGA_RX0_PID_LUT_MM32    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0xA000)
#define FPGA_RX1_PID_LUT_MM32    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0xA400)
#define FPGA_RX2_PID_LUT_MM32    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0xA800)
#define FPGA_RX3_PID_LUT_MM32    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0xAC00)

#define FPGA_RX0_PID_LUT_ENTRY_MM32(i)    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0xA000+(i)*4)
#define FPGA_RX1_PID_LUT_ENTRY_MM32(i)    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0xA400+(i)*4)
#define FPGA_RX2_PID_LUT_ENTRY_MM32(i)    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0xA800+(i)*4)
#define FPGA_RX3_PID_LUT_ENTRY_MM32(i)    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0xAC00+(i)*4)


#define FPGA_SERDES_PLL_MM32     DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x4C4C)

#define FPGA_SERDES_CH0_MM32     DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x4800)

#define FPGA1_GCMD_SHRMM32        DEFINE_SHRMMREG32 (USER_PCIBAR_MEMMAP, 0x10020)     ///<General Command Register
#define FPGA1_GSTS_MM32           DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10020)        ///<General Status Register
#define FPGA1_REVID_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10024)        ///<Fpga revision id
#define FPGA1_IMR_SHRMM32         DEFINE_SHRMMREG32 (USER_PCIBAR_MEMMAP, 0x10028)     ///<Interrupt Mask Register
#define FPGA1_SR_MM32             DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10028)        ///<Status Register
#define FPGA1_ICR_MM32            DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1002C)        ///<Interrupt Clear Register
#define FPGA1_ISR_MM32            DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1002C)        ///<Interrupt Status Register
#define FPGA1_TAP_A_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x18000)        ///<SDRAM Test Access Port Address
#define FPGA1_TAP_D_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x18004)        ///<SDRAM Test Access Port Data
#define FPGA1_STC_LSW_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10030)        ///<System Time Clock Least Significant 23 bits (33 bits timer from 90 Khz reference) and 9 bit prescaler value (0 ..299)
#define FPGA1_STC_MSW_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10034)        ///<System Time Clock 10 Most Significant Bits
#define FPGA1_SSN_LSW_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10038)        ///<Silicon Serial Number LSB
#define FPGA1_SSN_MSW_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1003C)        ///<Silicon Serial Number MSB
#define FPGA1_DMA_A_MM32          DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x18008)        ///<DMA Start Address for transfers
#define FPGA1_DMA_DATA_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1800C)        ///<DMA DATA access port for channel transfers


#define FPGA1_RX0_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10040)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA1_RX0_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10044)		   ///<Channel Bit Rate Deviation
#define FPGA1_RX0_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1004C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA1_RX0_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100C0)		   ///<Channel Command Register
#define FPGA1_RX0_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100C0)		   ///<Channel Status Register
#define FPGA1_RX0_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10040)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA1_RX1_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10050)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA1_RX1_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10054)		   ///<Channel Bit Rate Deviation
#define FPGA1_RX1_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1005C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA1_RX1_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100C4)		   ///<Channel Command Register
#define FPGA1_RX1_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100C4)		   ///<Channel Status Register
#define FPGA1_RX1_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10050)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA1_RX2_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10060)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA1_RX2_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10064)		   ///<Channel Bit Rate Deviation
#define FPGA1_RX2_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1006C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA1_RX2_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100C8)		   ///<Channel Command Register
#define FPGA1_RX2_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100C8)		   ///<Channel Status Register
#define FPGA1_RX2_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10060)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA1_RX3_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10070)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA1_RX3_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10074)		   ///<Channel Bit Rate Deviation
#define FPGA1_RX3_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1007C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA1_RX3_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100CC)		   ///<Channel Command Register
#define FPGA1_RX3_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100CC)		   ///<Channel Status Register
#define FPGA1_RX3_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10070)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA1_TX0_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100A0)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA1_TX0_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100A4)		   ///<Channel Bit Rate Deviation
#define FPGA1_TX0_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100AC)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA1_TX0_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100D8)		   ///<Channel Command Register
#define FPGA1_TX0_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100D8)		   ///<Channel Status Register
#define FPGA1_TX0_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100A0)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA1_TX1_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100B0)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA1_TX1_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100b4)		   ///<Channel Bit Rate Deviation
#define FPGA1_TX1_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100BC)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA1_TX1_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100DC)		   ///<Channel Command Register
#define FPGA1_TX1_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100DC)		   ///<Channel Status Register
#define FPGA1_TX1_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100B0)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA1_TX2_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10080)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA1_TX2_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10084)		   ///<Channel Bit Rate Deviation
#define FPGA1_TX2_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1008C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA1_TX2_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100D0)		   ///<Channel Command Register
#define FPGA1_TX2_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100D0)		   ///<Channel Status Register
#define FPGA1_TX2_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10080)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA1_TX3_BAUD_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10090)		   ///<Reference Bit Rate Generator : 32 bit phase increment.
#define FPGA1_TX3_DEV_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10094)		   ///<Channel Bit Rate Deviation
#define FPGA1_TX3_SIZE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1009C)		   ///<Channel Buffer Size in bytes : multiple of packet size 
#define FPGA1_TX3_CMD_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100D4)		   ///<Channel Command Register
#define FPGA1_TX3_STS_MM32        DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100D4)		   ///<Channel Status Register
#define FPGA1_TX3_RATE_MM32       DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x10090)		   ///<Incoming - Reference Bit Rate difference.

#define FPGA1_WDT_MM32	         DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100F0)	      ///<Watchdog timer register :  time-out value  (50MHz clock)
#define FPGA1_WDC_MM32	         DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100F4)	      ///<Watchdog control register :�	WACT : 0x57414354�	WARM : 0x5741524D�	WDIS : 0x57444953Set bit 31 of  WARM to �1� when you want watchdog to trigger relay command instead of active loopback.=> 0xD741524D to arm watchdog to trigger relays
#define FPGA1_VCXO_MM32	         DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x100F8)	      ///<[7:0] : 27 Mhz VCXO Control	   
#define FPGA1_GPIO_CTRL_SHRMM32   DEFINE_SHRMMREG32 (USER_PCIBAR_MEMMAP, 0x100FC)     ///<GPIO Control and Read Back Register
            
#define FPGA1_RX0_PID_LUT_MM32    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1A000)
#define FPGA1_RX1_PID_LUT_MM32    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1A400)
#define FPGA1_RX2_PID_LUT_MM32    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1A800)
#define FPGA1_RX3_PID_LUT_MM32    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1AC00)
            
#define FPGA1_RX0_PID_LUT_ENTRY_MM32(i)    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1A000+(i)*4)
#define FPGA1_RX1_PID_LUT_ENTRY_MM32(i)    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1A400+(i)*4)
#define FPGA1_RX2_PID_LUT_ENTRY_MM32(i)    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1A800+(i)*4)
#define FPGA1_RX3_PID_LUT_ENTRY_MM32(i)    DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x1AC00+(i)*4)

#define FPGA1_SERDES_PLL_MM32     DEFINE_MMREG32 (USER_PCIBAR_MEMMAP, 0x14C4C)
            

//FPGA_GCMD_SHRMM32
#define CFGPLD_CMD_BIT_PROG  	0x00000001   /*!<Reset FPGA*/

//CFGPLD_STATUS_MM32
#define CFGPLD_STATUS_BIT_INITDETECT    0x00000001  /*!<Problem during FPGA download*/
#define CFGPLD_STATUS_BIT_INIT    	   0x00000002  /*!<FPGA init signal*/
#define CFGPLD_STATUS_BIT_DONE0    	   0x00000004  /*!<FPGA 0 done signal*/
#define CFGPLD_STATUS_BIT_DONE1    	   0x00000008  /*!<FPGA 1 done signal*/

//FPGA_GCMD_SHRMM32
#define FPGA_GCMD_BIT_RESET	               0x80000000	         //SDRAM setup control
#define FPGA_GCMD_BIT_SSN_READ	            0x40000000           //SSN read command pulse (write 1 then 0)
#define FPGA_GCMD_BIT_FPGA0_XCLK_IN          0x20000000           //Select external clock and external bitrate reference for FPGA0/1:�	�0�: XCLK[1] (external clock/bitrate)�	�1�: FCLKin (FPGA1/0 clock/bitrate)
#define FPGA_GCMD_BIT_RX2TX3_LOOP	         0x08000000           //RX2/TX3 Active loopback control  (**)
#define FPGA_GCMD_BIT_RX0TX1_LOOP	         0x04000000           //RX0/TX1 Active loopback control  (**) 
#define FPGA_GCMD_BIT_END_OF_DMA	            0x02000000           //Set to �1� by the Host to signal the end of DMA transfer. This bit is auto-cleared.
#define FPGA_GCMD_BIT_SERDES_RST	            0x01000000           //Serdes reset
#define FPGA_GCMD_BIT_CH3_RELAY	            0x00800000           //RX3/TX3 channel relay control (**)
#define FPGA_GCMD_BIT_CH2_RELAY	            0x00400000           //RX2/TX2 channel relay control (**)
#define FPGA_GCMD_BIT_CH1_RELAY	            0x00200000           //RX1/TX1 channel relay control (**)
#define FPGA_GCMD_BIT_CH0_RELAY	            0x00100000           //RX0/TX0 channel relay control (**)
#define FPGA_GCMD_MSK_FCLKOUT_MUX            0x000E0000           //Inter-FPGA clock/bitrate selection
#define FPGA_GCMD_MSK_XCLK_MUX	            0x0001E000           //Extern output selection:�	0000 : �Z� �	0001 : loc_clk �	0010: RX0_br�	0011: RX1_br�	0100: RX2_br�	0101: RX3_br
#define FPGA_GCMD_BIT_TX23_LOOP_EN	         0x00001000           //Copy TX2 to TX3
#define FPGA_GCMD_BIT_TX01_LOOP_EN	         0x00000800           //Copy TX0 to TX1
#define FPGA_GCMD_MSK_RDEV	                  0x000007F8           //Reference clock max deviationAllowed deviation for reference : the difference between internal 27 Mhz & external reference clock rates is integrated over 1 msec; whenever it exceeds RDEV, all Txi_rcf status are set.
#define FPGA_GCMD_BIT_RSE	                  0x00000004	         //Reference Clocks switch enable0 => keep the selected reference clock even if Tx_rcf status is set1 => if the reference clock fails (Tx_rcf status set), then switch on the local 27 Mhz oscillator. Switch back to the selected source when Tx_rcf is cleared.
#define FPGA_GCMD_MSK_RCS	                  0x00000003           //Reference Clock selection:�	00:  loc_clk�	01 : rx0_clk�	10 : rx1_clk�	11 : xclk

#define FPGA_GCMD_BIT_FCLKOUT_MUX(fclkout_mux)  ((fclkout_mux<<17) & FPGA_GCMD_MSK_FCLKOUT_MUX)
#define FPGA_GCMD_GET_BIT_FCLKOUT_MUX(reg)      ((reg&FPGA_GCMD_MSK_FCLKOUT_MUX) >> 17)

#define FPGA_GCMD_BIT_XCLK_MUX(xclk_mux)     ((xclk_mux<<13) & FPGA_GCMD_MSK_XCLK_MUX)
#define FPGA_GCMD_GET_BIT_XCLK_MUX(reg)      ((reg&FPGA_GCMD_MSK_XCLK_MUX) >> 13)

#define FPGA_GCMD_BIT_RDEV(rdev)         ((rdev<<3) & FPGA_GCMD_MSK_RDEV)
#define FPGA_GCMD_GET_BIT_RDEV(reg)      ((reg&FPGA_GCMD_MSK_RDEV) >> 3)

#define FPGA_GCMD_BIT_RCS(rcs)          (rcs & FPGA_GCMD_MSK_RCS)
#define FPGA_GCMD_GET_BIT_RCS(reg)      (reg & FPGA_GCMD_MSK_RCS)


#define FCLKOUT_MUX_Z                        0
#define FCLKOUT_MUX_REFCLK27                 1
#define FCLKOUT_MUX_RX0_BR                   2
#define FCLKOUT_MUX_RX1_BR                   3
#define FCLKOUT_MUX_RX2_BR                   4
#define FCLKOUT_MUX_RX3_BR                   5
#define FCLKOUT_MUX_XCLKIN                   6

#define XCLK_MUX_Z                        0
#define XCLK_MUX_REFCLK27                 1
#define XCLK_MUX_RX0_BR                   2
#define XCLK_MUX_RX1_BR                   3
#define XCLK_MUX_RX2_BR                   4
#define XCLK_MUX_RX3_BR                   5
#define XCLK_MUX_FCLKIN                   6

#define RCS_LOCAL        0
#define RCS_RX0          1
#define RCS_RX1          2
#define RCS_EXT          3



//FPGA_GSTS_SHRMM32

#define FPGA_GSTS_BIT_SSN_RDY	               0x01000000           //SSN interface ready
#define FPGA_GSTS_BIT_SSN_NO_SLAVE	         0x02000000	         //No slave response detected
#define FPGA_GSTS_BIT_RELAY4_STS	            0x00800000           //Readback Relay control Command
#define FPGA_GSTS_BIT_RELAY3_STS	            0x00400000           //Readback Relay control Command
#define FPGA_GSTS_BIT_RELAY2_STS	            0x00200000           //Readback Relay control Command
#define FPGA_GSTS_BIT_RELAY1_STS	            0x00100000           //Readback Relay control Command
#define FPGA_GSTS_BIT_LOC_INTI	            0x00040000           //Interrupt request pending
#define FPGA_GSTS_BIT_DLL1_LOCKED	         0x00020000           //FPGA DLL1 status (1 = ready)
#define FPGA_GSTS_BIT_DLL0_LOCKED	         0x00010000           //FPGA DLL0 status (1 = ready)
#define FPGA_GSTS_BIT_WDT_RUN	               0x00000800           //Watchdog status (1 = running)
#define FPGA_GSTS_BIT_WDT_TIMEOUT	         0x00000400           //Watchdog timeout (1 = timeout)
#define FPGA_GSTS_BIT_RCF                    0x00000100           //reference clock fail
#define FPGA_GSTS_BIT_LFI3                   0x00000008           //RX3 line fault indicator (0=fault)
#define FPGA_GSTS_BIT_LFI2                   0x00000004           //RX2 line fault indicator (0=fault)
#define FPGA_GSTS_BIT_LFI1                   0x00000002           //RX1 line fault indicator (0=fault)
#define FPGA_GSTS_BIT_LFI0                   0x00000001           //RX0 line fault indicator (0=fault)


//-- FPGA_ISR_MM32
#define FPGA_ISR_BIT_RXi_BR(RxIdx)            (1<<((RxIdx)*4))         //Rxi Buffer Ready Interrupt
#define FPGA_ISR_BIT_RXi_SC(RxIdx)            (2<<((RxIdx)*4))         //Rxi Status Change Interrupt (RXi_STS[4..0])       
#define FPGA_ISR_BIT_RXi_OR(RxIdx)            (4<<((RxIdx)*4))         //Rxi Overrun Interrupt

#define FPGA_ISR_BIT_TXi_BE(TxIdx)            (0x10000<<((((TxIdx)+2)%4)*4))    //Txi Buffer Empty Interrupt 
#define FPGA_ISR_BIT_TXi_SC(TxIdx)            (0x20000<<((((TxIdx)+2)%4)*4))    //Txi Status Change Interrupt (TX2_STS[4..0])     
#define FPGA_ISR_BIT_TXi_UR(TxIdx)            (0x40000<<((((TxIdx)+2)%4)*4))    //Txi Underrun Interrupt

//-- FPGA_ICR_MM32
#define FPGA_ICR_BIT_RXi_BR        FPGA_ISR_BIT_RXi_BR               
#define FPGA_ICR_BIT_RXi_SC        FPGA_ISR_BIT_RXi_SC    
#define FPGA_ICR_BIT_RXi_OR        FPGA_ISR_BIT_RXi_OR    
                                   
#define FPGA_ICR_BIT_TXi_BE        FPGA_ISR_BIT_TXi_BE    
#define FPGA_ICR_BIT_TXi_SC        FPGA_ISR_BIT_TXi_SC        
#define FPGA_ICR_BIT_TXi_UR        FPGA_ISR_BIT_TXi_UR    

//-- FPGA_SR_MM32
#define FPGA_SR_BIT_RXi_BR        FPGA_ISR_BIT_RXi_BR               
#define FPGA_SR_BIT_RXi_SC        FPGA_ISR_BIT_RXi_SC    
#define FPGA_SR_BIT_RXi_OR        FPGA_ISR_BIT_RXi_OR    

#define FPGA_SR_BIT_TXi_BE        FPGA_ISR_BIT_TXi_BE    
#define FPGA_SR_BIT_TXi_SC        FPGA_ISR_BIT_TXi_SC        
#define FPGA_SR_BIT_TXi_UR        FPGA_ISR_BIT_TXi_UR    

//-- FPGA_IMR_SHRMM32
#define FPGA_IMR_BIT_RXi_BR        FPGA_ISR_BIT_RXi_BR               
#define FPGA_IMR_BIT_RXi_SC        FPGA_ISR_BIT_RXi_SC    
#define FPGA_IMR_BIT_RXi_OR        FPGA_ISR_BIT_RXi_OR    

#define FPGA_IMR_BIT_TXi_BE        FPGA_ISR_BIT_TXi_BE    
#define FPGA_IMR_BIT_TXi_SC        FPGA_ISR_BIT_TXi_SC        
#define FPGA_IMR_BIT_TXi_UR        FPGA_ISR_BIT_TXi_UR    


// -- FPGA_TXx_ASI_CMD_MM32 : ASI TXx command register -----------------------------------------------------------------------------/
#define FPGA_TXx_ASI_CMD_BIT_EN              0x00000001  /* Channel enable (soft reset) */
#define FPGA_TXx_ASI_CMD_MSK_FMT             0x0000000E  /* Packet Format */
#define FPGA_TXx_ASI_CMD_MSK_BRSS            0x00000700  /* Channel Bit Rate Source Select */
#define FPGA_TXx_ASI_CMD_BIT_SXBR            0x00000800  /* Synchronous external bit rate source (can forward bursts) */
#define FPGA_TXx_ASI_CMD_BIT_BSE             0x00001000  /* Automatic bit clock switch enable.
                                                            0 => keep the selected bit clock even if a bit clock failure is detected (bit TXi_BCF of Txi_STS)
                                                            1 => whenever a bit clock failure is detected, automatically switch to the internal phase accumulator reference.
                                                                 When Txi_BCF is deasserted, switch back to selected external reference. */
#define FPGA_TXx_ASI_CMD_BIT_TX_PRELOAD      0x00100000  /* Preload enable */
#define FPGA_TXx_ASI_CMD_MSK_INTSEL          0x00600000  /* Integration period selection for bit rate check */


#define FPGA_TXx_ASI_CMD_BIT_FMT(fmt)           ((fmt<<1) & FPGA_TXx_ASI_CMD_MSK_FMT)
#define FPGA_TXx_ASI_CMD_GET_BIT_FMT(reg)       ((reg&FPGA_TXx_ASI_CMD_MSK_FMT) >> 1)
#define xXx_ASI_FMT_188                             0x0 /* 188 bytes per packet */
#define xXx_ASI_FMT_188_TO_204                      0x1 /* 188 bytes per packet extended to 204 */
#define xXx_ASI_FMT_204                             0x2 /* 204 bytes per packet */
#define xXx_ASI_FMT_204_TO_188                      0x3 /* 204 bytes stripped down to 188 */
#define xXx_ASI_FMT_RAW                             0x4 /* raw data mode */

#define FPGA_TXx_ASI_CMD_BIT_BRSS(brss)         ((brss<<8) & FPGA_TXx_ASI_CMD_MSK_BRSS)
#define FPGA_TXx_ASI_CMD_GET_BIT_BRSS(reg)      ((reg&FPGA_TXx_ASI_CMD_MSK_BRSS) >> 8)
#define TXx_ASI_BRSS_RX0                        0x0 /* RX Channel 0 (not yet implemented) */
#define TXx_ASI_BRSS_RX2                        0x1 /* RX Channel 2 (not yet implemented) */
#define TXx_ASI_BRSS_EXT_CLK                    0x2 /* External clock 0 (not yet implemented) */
#define TXx_ASI_BRSS_LOCAL                      0x4 /* Phase accumulator */

#define FPGA_TXx_ASI_CMD_BIT_INTSEL(int_sel)   ((int_sel<<21) & FPGA_TXx_ASI_CMD_MSK_INTSEL)
#define FPGA_TXx_ASI_CMD_GET_BIT_INTSEL(reg)   ((reg&FPGA_TXx_ASI_CMD_MSK_INTSEL) >> 21)
#define xXx_ASI_INTSEL_1                       0x0 /* 1 ms */
#define xXx_ASI_INTSEL_10                      0x1 /* 10 ms */
#define xXx_ASI_INTSEL_100                     0x2 /* 100 ms */
#define xXx_ASI_INTSEL_1000                    0x3 /* 1000 ms */


// -- FPGA_TXx_ASI_STS_MM32 : ASI TXx status register -----------------------------------------------------------------------------/
#define FPGA_TXx_ASI_STS_BIT_LFI                0x00000001  /* RX Link fault indicator */
#define FPGA_TXx_ASI_STS_BIT_RCF                0x00000002  /* Reference Clock FailSee Txi_rdev.This status is automatically cleared at the end of the 1 msec integration period, if the external reference clock is valid again. */
#define FPGA_TXx_ASI_STS_BIT_BCF                0x00000004  /* Bit clock failure */
#define FPGA_TXx_ASI_STS_BIT_UDR                0x00000008  /* TX Channel Underrun */
#define FPGA_TXx_ASI_STS_BIT_FER                0x00000010  /* Framing error */
#define FPGA_TXx_ASI_STS_BIT_PLK                0x00000020  /* RX packet lock indicator */
#define FPGA_TXx_ASI_STS_BIT_BRST               0x00000040  /* Burst mode indicator; TX: 0 */
#define FPGA_TXx_ASI_STS_BIT_LAST_EMPTIED_BUF   0x00000080  /* Index of the last buffer emptied by the Tx channel */
#define FPGA_TXx_ASI_STS_BIT_RDY                0x00008000  /* TX channel ready(TBD) */




// -- FPGA_RXx_ASI_CMD_MM32 : ASI RXx command register -----------------------------------------------------------------------------/
#define FPGA_RXx_ASI_CMD_BIT_EN              0x00000001  /* Channel enable (soft reset) */
#define FPGA_RXx_ASI_CMD_MSK_FMT             0x0000000E  /* Packet Format */
#define FPGA_RXx_ASI_CMD_BIT_FLUSH           0x00000010  /* Flush channel buffers by pushing padding 0s. Channel MUST be disabled after a flush operation */
#define FPGA_RXx_ASI_CMD_BIT_TS_EN           0x00000020  /* Enable Time Stamp insertion after each valid DVB packet. Time stamp is STC + prescaler value zero-extended and packed in eight bytes. */
#define FPGA_RXx_ASI_CMD_MSK_CMS             0x00007000  /* Clocks monitor & auto-switch configuration */
#define FPGA_RXx_ASI_CMD_BIT_ESC             0x00008000  /* Error Status Clear (TBD) */
#define FPGA_RXx_ASI_CMD_MSK_INTSEL          0x00600000  /* Integration period selection for bit rate check */
#define FPGA_RXx_ASI_CMD_BIT_PID_EN          0x00800000  /* PID filter enable */
#define FPGA_RXx_ASI_CMD_MSK_MAX_PID         0xFF000000  /* Last valid PID table entry (number of valid PIDs - 1) */


#define FPGA_RXx_ASI_CMD_BIT_FMT(fmt)           ((fmt<<1) & FPGA_RXx_ASI_CMD_MSK_FMT)
#define FPGA_RXx_ASI_CMD_GET_BIT_FMT(reg)       ((reg&FPGA_RXx_ASI_CMD_MSK_FMT) >> 1)

#define FPGA_RXx_ASI_CMD_BIT_CMS(cms)           ((cms<<12) & FPGA_RXx_ASI_CMD_MSK_CMS)
#define FPGA_RXx_ASI_CMD_GET_BIT_CMS(reg)       ((reg&FPGA_RXx_ASI_CMD_MSK_CMS) >> 12)

#define FPGA_RXx_ASI_CMD_BIT_INTSEL(int_sel)    ((int_sel<<21) & FPGA_RXx_ASI_CMD_MSK_INTSEL)
#define FPGA_RXx_ASI_CMD_GET_BIT_INTSEL(reg)    ((reg&FPGA_RXx_ASI_CMD_MSK_INTSEL) >> 21)

#define FPGA_RXx_ASI_CMD_BIT_MAX_PID(max_pid)   ((max_pid<<24) & FPGA_RXx_ASI_CMD_MSK_MAX_PID)
#define FPGA_RXx_ASI_CMD_GET_BIT_MAX_PID(reg)   ((reg&FPGA_RXx_ASI_CMD_MSK_MAX_PID) >> 24)

// -- FPGA_RXx_ASI_STS_MM32 : ASI RXx status register -----------------------------------------------------------------------------/
#define FPGA_RXx_ASI_STS_BIT_LFI            0x00000001  /* RX Link fault indicator */
#define FPGA_RXx_ASI_STS_BIT_RCF            0x00000002  /* Reference Clock Fail */
#define FPGA_RXx_ASI_STS_BIT_BCF            0x00000004  /* Bit rate failure */
#define FPGA_RXx_ASI_STS_BIT_OVR            0x00000008  /* RX Channel Overrun */
#define FPGA_RXx_ASI_STS_BIT_FER            0x00000010  /* Framing error */
#define FPGA_RXx_ASI_STS_BIT_PLK            0x00000020  /* RX packet lock indicator */
#define FPGA_RXx_ASI_STS_BIT_BRST           0x00000040  /* Burst mode indicator*/
#define FPGA_RXx_ASI_STS_BIT_RX_BUFIDX      0x00000080  /* Index of the last buffer filled by the Rx channel */
#define FPGA_RXx_ASI_STS_BIT_FER_LTCH       0x00000100  /* Framing error latched (updated for each new buffer) */
#define FPGA_RXx_ASI_STS_BIT_RDY            0x00008000  /* RX channel ready(TBD) */
#define FPGA_RXx_ASI_STS_MSK_FMT_CTRL       0x00030000  /* Format control. The format control information is significant when there is no framing error only. */


#define FPGA_RXx_ASI_STS_BIT_FMT_CTRL(fmt_ctrl)   ((fmt_ctrl<<16) & FPGA_RXx_ASI_STS_MSK_FMT_CTRL)
#define FPGA_RXx_ASI_STS_GET_BIT_FMT_CTRL(reg)    ((reg&FPGA_RXx_ASI_STS_MSK_FMT_CTRL) >> 16)
#define ASI_FMTCTRL_OK                            0x0 /* Received stream has the expected format */
#define ASI_FMTCTRL_188                           0x1 /* Expected packet length is 204 and received packets are 188 byte long */
#define ASI_FMTCTRL_204                           0x2 /* Expected packet length is 188 and received packets are 204 byte long */

#define FPGA_RXx_ASI_STS_GET_BIT_RX_BUFIDX(reg)    ((reg&FPGA_RXx_ASI_STS_BIT_RX_BUFIDX) >> 7)


// -- FPGA_WDC_MM32
#define FPGA_WDC_VAL_ENABLE                  0xD7414354
#define FPGA_WDC_VAL_ARM                     0xD741524D
#define FPGA_WDC_VAL_DISABLE                 0x57444953

// -- FPGA_WATCHDOG_CTRL_SHRMM32
#define FPGA_WATCHDOG_MSK_VALUE     0xFFFFFFFF//??

//-- FPGA_GPIO_CTRL_SHRMM32
#define FPGA_GPIO_CTRL_MSK_GPIO_RD           0x0000003F
#define FPGA_GPIO_CTRL_MSK_GPIO_ENO          0x00000FC0
#define FPGA_GPIO_CTRL_MSK_GPIO_WR           0x0003F000

#define FPGA_GPIO_CTRL_GET_BIT_GPIO_RD(reg)     (reg&FPGA_GPIO_CTRL_MSK_GPIO_RD)
#define FPGA_GPIO_CTRL_BIT_GPIO_ENO(gpio_eno)   ((gpio_eno<<6) & FPGA_GPIO_CTRL_MSK_GPIO_ENO)
#define FPGA_GPIO_CTRL_BIT_GPIO_WR(gpio_wr)     ((gpio_wr<<12) & FPGA_GPIO_CTRL_MSK_GPIO_WR)


#endif

