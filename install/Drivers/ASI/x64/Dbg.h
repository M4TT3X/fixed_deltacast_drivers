/********************************************************************************************************************//**
 * @internal
 * @file   	DrvDbg.h
 * @date   	2010/06/17
 * @author 	cs
 * @version v00.01.0000 
 * @brief  
 *
 * 
 *
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2010/06/17  v00.01.0000    cs       Creation of this file

 **********************************************************************************************************************/

#ifndef _DRVDBG_H_
#define _DRVDBG_H_

/***** INCLUDES SECTION ***********************************************************************************************/

/***** SYMBOLS DEFINITIONS ********************************************************************************************/


#define LIBNAME "VHD"

#if defined(__linux__) || defined(__APPLE__)
#include <stdio.h>
#define OutputPrint(...) fprintf(stderr, ##__VA_ARGS__)
#else
void DbgPrint(const char *pFmt_c,...);
#define OutputPrint DbgPrint 
#endif

extern ULONG GL_DbgMask_UL;
extern ULONG GL_ANCErrTolerance_UL;


#define DBG_OUTPUT_ALL        0xFFFFFFFF
#define DBG_OUTPUT_NONE       0x00000000

#define DBG_OUTPUT_ENTER      0x00000010
#define DBG_OUTPUT_EXIT       0x00000020
#define DBG_OUTPUT_DEBUG      0x00000040
#define DBG_OUTPUT_INFO       0x00000080
#define DBG_OUTPUT_WARNING    0x00000100
#define DBG_OUTPUT_ERROR      0x00000200
#define DBG_OUTPUT_RETAIL     0x00000400
#define DBG_OUTPUT_TRACE      0x00000800

#define DBG_OUTPUT_ENTER_PREFIX           "--> Entering "
#define DBG_OUTPUT_EXIT_PREFIX            "<-- Exiting "
#define DBG_OUTPUT_DEBUG_PREFIX           ""
#define DBG_OUTPUT_INFO_PREFIX            "==INFO: " 
#define DBG_OUTPUT_WARNING_PREFIX         "**WRN: " 
#define DBG_OUTPUT_ERROR_PREFIX           "**ERR: "
#define DBG_OUTPUT_RETAIL_PREFIX          ""
#define DBG_OUTPUT_TRACE_PREFIX           "==INFO: " 

#define TIME_GET                      
#define TIME_FMT                       
#define TIME_USE                       


/***** MACROS DEFINITIONS *********************************************************************************************/

#define EnterOutput(pFmt_c,...)        xxxOutput(DBG_OUTPUT_ENTER, DBG_OUTPUT_ENTER_PREFIX, pFmt_c, ##__VA_ARGS__)
#define ExitOutput(pFmt_c,...)         xxxOutput(DBG_OUTPUT_EXIT, DBG_OUTPUT_EXIT_PREFIX, pFmt_c, ##__VA_ARGS__)
#define DbgOutput(pFmt_c,...)          xxxOutput(DBG_OUTPUT_DEBUG, DBG_OUTPUT_DEBUG_PREFIX, pFmt_c, ##__VA_ARGS__)
#define InfoOutput(pFmt_c,...)         xxxOutput(DBG_OUTPUT_INFO, DBG_OUTPUT_INFO_PREFIX, pFmt_c, ##__VA_ARGS__)
#define WrnOutput(pFmt_c,...)          xxxOutput(DBG_OUTPUT_WARNING, DBG_OUTPUT_WARNING_PREFIX, pFmt_c, ##__VA_ARGS__)
#define ErrOutput(pFmt_c,...)          xxxOutput(DBG_OUTPUT_ERROR, DBG_OUTPUT_ERROR_PREFIX, pFmt_c, ##__VA_ARGS__)
#define RetailOutput(pFmt_c,...)       xxxOutput(DBG_OUTPUT_RETAIL, DBG_OUTPUT_RETAIL_PREFIX, pFmt_c, ##__VA_ARGS__)
#define TraceOutput(pFmt_c,...)        xxxOutput(DBG_OUTPUT_TRACE, DBG_OUTPUT_TRACE_PREFIX, pFmt_c, ##__VA_ARGS__)


#define xxxOutput(Level_UL,pPrefix_c,pFmt_c,...)  do{TIME_GET  if(Level_UL&GL_DbgMask_UL) \
   OutputPrint( TIME_FMT LIBNAME " [%s] " pPrefix_c pFmt_c"\n" TIME_USE,__FUNCTION__, ##__VA_ARGS__);}while(0)

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/
void SetDbgMask( ULONG DbgMask_UL );
void SetANCErrTolerance( ULONG SetANCErrTolerance_UL );

/***** CLASSES DECLARATIONS *******************************************************************************************/

#endif // _DRVDBG_H_



