/********************************************************************************************************************//**
 * @file   UserShadowRegisters.h
 * @date   2007/12/05
 * @Author cs
 * @brief  
 *
 * This file describes the functions and data involved in the User defined shadow registers accesses feature. It allows
 * to user to define from the register map definition a shadow register that multi-CPU/Process coherency is kept at
 * driver level.
 *
 * Note : This module needs to handle these I/O controls (handled by the UserShadowReg_Ioctl() function)
 * Then please add these following IOControl in the IOCtl.h file ...
 *
 * #define IOCTL_USRSHADOWREG_DEFINE         DEFINE_IOCTL_WR   (0x090, USER_SHADOWREG_PARAM)
 * #define IOCTL_USRSHADOWREG_SETVALUE       DEFINE_IOCTL_WR   (0x091, USER_SHADOWREG_PARAM)
 * #define IOCTL_USRSHADOWREG_GETVALUE       DEFINE_IOCTL_RDWR (0x092, USER_SHADOWREG_PARAM)
 * #define IOCTL_USRSHADOWREG_SETBIT         DEFINE_IOCTL_WR   (0x093, USER_SHADOWREG_PARAM)
 * #define IOCTL_USRSHADOWREG_CLRBIT         DEFINE_IOCTL_WR   (0x094, USER_SHADOWREG_PARAM)
 * #define IOCTL_USRSHADOWREG_RESET          DEFINE_IOCTL      (0x095)
 *
 * ... and add the IOCtl handling in the Board_Ioctl() function :
 *
 * @code
 *
 * ...
 * case IOCTL_USRSHADOWREG_DEFINE  :
 * case IOCTL_USRSHADOWREG_SETVALUE:
 * case IOCTL_USRSHADOWREG_GETVALUE:
 * case IOCTL_USRSHADOWREG_SETBIT  :
 * case IOCTL_USRSHADOWREG_CLRBIT  :
 * case IOCTL_USRSHADOWREG_RESET   :
 * Sts_B = UserShadowReg_Ioctl(pdx,Cmd_UL,pBuffer_UB,BufferLength_UL, PID_UL);
 * break;
 * ...
 *
 * @endcode
 *
 * Furthermore, this module also needs internal data to work. These data are grouped in the USER_SHADOWREG_DATA 
 * structure. Then, in the BoardDrvTypes.h file, insert :
 *
 * @code
 *
 * ...
 * #include "UserShadowRegistersDrvTypes.h"
 * ...
 *
 * typedef struct
 * {
 *   ...
 *   USER_SHADOWREG_DATA     UserShadowRegDatra_X;   // Needed data for User Shadow Registers
 *   ...
 *  } BOARD_DATA;
 *  
 * @endcode
 **********************************************************************************************************************//*
  
   ====================================================================================================================
   =  Version History                                                                                                 =
   ====================================================================================================================
   Date     	Version     	Author   Description
   ====================================================================================================================
   2007/12/05  v00.01.0000    cs       Creation of this file
   2007/12/06  v00.02.0000    cs       Change function name to add ioctl prefix
                                       Add function to allow driver to define and modify user shadow registers
 **********************************************************************************************************************/

#ifndef _SHADOWREGISTERS_H_
#define _SHADOWREGISTERS_H_

/***** INCLUDES SECTION ***********************************************************************************************/

#include "SkelDrv.h"
#include "ShadowRegistersTypes.h"

/***** SYMBOLS DEFINITIONS ********************************************************************************************/
#define MAX_SHADOW_REGISTERS 256 /*!< Definies the maximum allowed number of user shadow registers */

/***** MACROS DEFINITIONS *********************************************************************************************/

#define GetShadowRegData() (&pdx->ShadowRegData_X)

#define SHADOWREG_INIT_SAFEAREA()  KOBJ_InitializeSpinLock(pdx,&GetShadowRegData()->SpinLock_KO)
#define SHADOWREG_ENTER_SAFEAREA() KOBJ_AcquireSpinLock(pdx,&GetShadowRegData()->SpinLock_KO)
#define SHADOWREG_LEAVE_SAFEAREA() KOBJ_ReleaseSpinLock(pdx,&GetShadowRegData()->SpinLock_KO)


#ifdef DRIVER

#define REGISTER_ACCESS_PCI_RESOURCES(bar,offset) REGISTER_ACCESS_PCI_RESOURCES_PTR[bar][offset]
#define SHREG32_SET(idx,val)        ShadowReg32_Set(pdx,idx,val,TRUE)
#define SHREG32_SETBIT(idx,val)        ShadowReg32_SetBit(pdx,idx,val,TRUE)
#define SHREG32_CLRBIT(idx,val)        ShadowReg32_ClrBit(pdx,idx,val,TRUE)
#define SHREG32_SET_AUTOCLR_BIT(idx,val)        ShadowReg32_SetAutoClrBit(pdx,idx,val,TRUE)
#define SHREG32_SETMSK(idx,val,msk)        ShadowReg32_SetMsk(pdx,idx,val,msk,TRUE)
#define UNSAFE_SHREG32_SET(idx,val) ShadowReg32_Set(pdx,idx,val,FALSE)
#define SHREG32_GET(idx,pval)              ShadowReg32_Get(pdx,idx,pval,TRUE)

#define SHREG32_DEFINE(reg,val)  ShadowReg32_Define(pdx,IDX_##reg,reg,val,TRUE)
#define UNSAFE_SHREG32_DEFINE(reg,val)  ShadowReg32_Define(pdx,IDX_##reg,reg,val,FALSE)

#endif

/***** TYPES AND STRUCTURES DEFINITIONS *******************************************************************************/
typedef struct  
{
   KOBJ_SPINLOCK          SpinLock_KO;
   ULONG                  pInitialValue_UL[MAX_SHADOW_REGISTERS];
   ULONG                  pValue_UL[MAX_SHADOW_REGISTERS];
   ULONG                  pOffset_UL[MAX_SHADOW_REGISTERS];
   UBYTE                  pPCIBar_UB[MAX_SHADOW_REGISTERS];
} SHADOWREG_DATA;


/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

void ShadowReg_DeviceAdd(PDEVICE_EXTENSION pdx);
void ShadowReg_Reset( PDEVICE_EXTENSION pdx );
IOCTL_STATUS ShadowReg_Ioctl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,PID_TYPE PID_UL);
BOOL32 ShadowReg32_Define(PDEVICE_EXTENSION pdx, ULONG IDx_UL, UBYTE PCIBar_UL, ULONG Offset_UL, ULONG InitValue_UL, BOOL32 Safe_B);
void ShadowReg32_ResetAll(PDEVICE_EXTENSION pdx, BOOL32 Safe_B);

BOOL32 ShadowReg32_Reset(PDEVICE_EXTENSION pdx, ULONG IDx_UL,  BOOL32 Safe_B);
BOOL32 ShadowReg32_Set(PDEVICE_EXTENSION pdx, ULONG IDx_UL, ULONG Val_UL, BOOL32 Safe_B);
BOOL32 ShadowReg32_SetBit(PDEVICE_EXTENSION pdx, ULONG IDx_UL, ULONG Val_UL, BOOL32 Safe_B);
BOOL32 ShadowReg32_SetAutoClrBit(PDEVICE_EXTENSION pdx, ULONG IDx_UL, ULONG Val_UL, BOOL32 Safe_B);
BOOL32 ShadowReg32_ClrBit(PDEVICE_EXTENSION pdx, ULONG IDx_UL, ULONG Val_UL, BOOL32 Safe_B);
BOOL32 ShadowReg32_Get( PDEVICE_EXTENSION pdx, ULONG IDx_UL, ULONG *pValue_UL, BOOL32 Safe_B );
BOOL32 ShadowReg32_SetMsk(PDEVICE_EXTENSION pdx, ULONG IDx_UL, ULONG Val_UL, ULONG Msk_UL, BOOL32 Safe_B);
//BOOL32 ShadowReg32_TestAndSetBit(PDEVICE_EXTENSION pdx, ULONG IDx_UL, UBYTE PCIBar_UL, ULONG Offset_UL, ULONG Val_UL, BOOL32 Safe_B);


/*
BOOL32 UserShadowReg_Ioctl(PDEVICE_EXTENSION pdx, ULONG Cmd_UL, UBYTE *pBuffer_UB, ULONG BufferLength_UL,ULONG PID_UL);
BOOL32 UserShadowReg_ioctlDefine(PDEVICE_EXTENSION pdx,USER_SHADOWREG_PARAM * pParam_X);
BOOL32 UserShadowReg_ioctlSetValue(PDEVICE_EXTENSION pdx,USER_SHADOWREG_PARAM * pParam_X);
BOOL32 UserShadowReg_ioctlGetValue(PDEVICE_EXTENSION pdx,USER_SHADOWREG_PARAM * pParam_X);
BOOL32 UserShadowReg_ioctlSetBit(PDEVICE_EXTENSION pdx,USER_SHADOWREG_PARAM * pParam_X);
BOOL32 UserShadowReg_ioctlSetAutoClearBit( PDEVICE_EXTENSION pdx,USER_SHADOWREG_PARAM * pParam_X );
BOOL32 UserShadowReg_ioctlClrBit(PDEVICE_EXTENSION pdx,USER_SHADOWREG_PARAM * pParam_X);
BOOL32 UserShadowReg_ioctlResetToInitialValue(PDEVICE_EXTENSION pdx,USER_SHADOWREG_PARAM * pParam_X);

BOOL32 UserShadowReg_Define(PDEVICE_EXTENSION pdx,PREGISTER pRegister, ULONG InitialValue_UL);
BOOL32 UserShadowReg_SetValue(PDEVICE_EXTENSION pdx,PREGISTER pRegister, ULONG Value_UL);
BOOL32 UserShadowReg_GetValue(PDEVICE_EXTENSION pdx,PREGISTER pRegister, ULONG *pValue_UL);
BOOL32 UserShadowReg_SetBit(PDEVICE_EXTENSION pdx,PREGISTER pRegister, ULONG Bit_UL);
BOOL32 UserShadowReg_SetAutoClearBit(PDEVICE_EXTENSION pdx,PREGISTER pRegister, ULONG Bit_UL);
BOOL32 UserShadowReg_ClrBit(PDEVICE_EXTENSION pdx,PREGISTER pRegister, ULONG Bit_UL);
BOOL32 UserShadowReg_ResetToInitialValue(PDEVICE_EXTENSION pdx,PREGISTER pRegister);*/

#endif // _USERSHADOWREGISTERS_H_

