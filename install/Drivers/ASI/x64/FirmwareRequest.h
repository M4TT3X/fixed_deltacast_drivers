
#ifndef _FIRMWAREREQUEST_H_
#define _FIRMWAREREQUEST_H_

#include "CTypes.h"

typedef struct
{
#ifdef __linux__ 
   char *pFilename_c;
#else
   UBYTE *pData_UB;
   ULONG Size_UL;
#endif
} FIRMWARE_REQUEST;

#endif
