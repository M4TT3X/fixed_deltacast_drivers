/**********************************************************************************************************************

Module   : LinuxPCI_skel
File     : LinuxPCI_skel.c
Created  : 2006/11/20
Author   : cs

**********************************************************************************************************************/

/***** INCLUDES *******************************************************************************************************/

#define MODID MID_BOARD

#include "Driver.h"
#include "EventMngr.h"
#include "CommonBufMngr.h"
#include "LinuxAllocator.h"
#include "DrvDbg.h"
#include "LinuxDrvInfo.h"
#include "DriverAccess.h"
#include "InterlockedOp.h"
#include "OSDrvData.h"
#include "DrvParam.h"


#if (MEM_ALLOC_METHOD == MEM_ALLOC_BIGPHYSAREA)
#include <linux/bigphysarea.h>
#endif

MODULE_LICENSE("GPL");
MODULE_AUTHOR("CS");


/***** SYMBOLS DEFINITION *********************************************************************************************/

#define TraceISROutput(pFmt_c,...)       xxxOutput(SKELDRV_TRACE_MSG_ISR, "TRACEISR: ", pFmt_c, ##__VA_ARGS__)


#define NB_MAX_BOARD 4

/* Add compatibility with old kernel */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
   #ifndef IRQF_DISABLED
   #define IRQF_DISABLED SA_INTERRUPT
   #endif
#else
   #define IRQF_DISABLED    0              //IRQF_DISABLED is a NOOP since 2.6.35 and removed since 4.1    
#endif

#ifndef IRQF_SHARED
#define IRQF_SHARED SA_SHIRQ
#endif



/* Add compatibility with the old class_simple interface */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,12))
struct class_simple *pDeltaClass_X;
#define class_create class_simple_create
#define class_destroy class_simple_destroy
#elif (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27))
struct class *pDeltaClass_X;
#else
struct class *pDeltaClass_X;
#endif

/***** EXTERNAL VARIABLES *********************************************************************************************/

/***** FUNCTIONS PROTOTYPES *******************************************************************************************/

static int probe(struct pci_dev *dev, const struct pci_device_id *id);
static void remove(struct pci_dev *dev);
static void removeEx(struct pci_dev *pPCIDevice_X, BOOL32 NoFreeIRQ_B);
static void isr_tasklet(unsigned long data);

/* PM Functions */
static int Power_Suspend(struct pci_dev* pDev_X, pm_message_t state);
static int Power_Resume(struct pci_dev* pDev_X);
static int Power_Shutdown(struct pci_dev* pDev_X);

//DECLARE_TASKLET(signal_tasklet, isr_tasklet, 0);

/***** GLOBAL VARIABLES ***********************************************************************************************/

DEVICE_EXTENSION GL_pDeviceExtension_X[NB_MAX_BOARD];
ULONG    GL_NbBoards_UL = 0;

static int    GL_major_i; // Major number associated to this driver
static struct pci_device_id GL_idtable_X[]= BOARD_PCI_INFO;

static struct pci_driver GL_pci_driver_X = 
{
   .name=DRIVERNAME, /*name that will be shown under /sys/bus/pci/drivers when the driver is loaded in kernel*/
   .id_table=GL_idtable_X,
   .probe=probe,
   .remove=remove,
   .suspend=Power_Suspend,
   .resume=Power_Resume,
   .shutdown=Power_Shutdown
};

MODULE_DEVICE_TABLE(pci, GL_idtable_X);

void PCIResources_CleanUp(PDEVICE_EXTENSION pdx)
{
   int i;  

   EnterOutput("\n");

   for (i = 0 ; i < 6 ; i++)
   {
      switch(pdx->pPCIResources_X[i].Type_E)
      {
      case PCI_RESOURCE_TYPE_IO:
      case PCI_RESOURCE_TYPE_NOT_USED: break; // Do nothing
      case PCI_RESOURCE_TYPE_MEMORY: 
         if(pdx->pPCIResources_X[i].pKernelAddr_UB)
         {
            iounmap(pdx->pPCIResources_X[i].pKernelAddr_UB);
            pdx->pPCIResources_X[i].pKernelAddr_UB=NULL;
         }
         release_mem_region(pci_resource_start(pdx->pOsDrvData_X->pPCIDevice_X, i), pci_resource_len(pdx->pOsDrvData_X->pPCIDevice_X, i));
         break;
      }
   }

   ExitOutput("\n");
}

void SkelDrv_DPCforISR( PDEVICE_EXTENSION pdx, ULONG DPCIntSource_UL );

/***** isr_tasklet *********************************************************************************************

Description :

This function is a defered one called by the ISR if needed.

Parameters :

unsigned long data	:

Returned Value : void
Remark : None

**********************************************************************************************************************/
static void isr_tasklet(unsigned long data)
{
   DEVICE_EXTENSION *pdx = (DEVICE_EXTENSION *) data;  
   ULONG DPCIntSource_UL;

   //pdx->BoardData_X.IntSource_UL&=~DPCIntSource_UL;
   DPCIntSource_UL = InterlockedExchange(&pdx->IntSource_UL,0);
   //DPCIntSource_UL=xchg(&pdx->IntSource_UL,0);
   DPCIntSource_UL &= pdx->DPCIntMask_UL;
   
   //printk ("<0> Tasklet %x\n",DPCIntSource_UL);
   SkelDrv_DPCforISR(pdx, DPCIntSource_UL);
}


/***** OnInterrupt *********************************************************************************************

Description :



Parameters :

int irq	:
void *dev_id	:
struct pt_regs *regs	:

Returned Value : irqreturn_t
Remark : None

**********************************************************************************************************************/

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
static irqreturn_t OnInterrupt(int irq, void *dev_id, struct pt_regs *regs)
#else
static irqreturn_t OnInterrupt(int irq, void *dev_id)
#endif
{
   DEVICE_EXTENSION  *pdx = (DEVICE_EXTENSION*)dev_id;
   irqreturn_t rc= IRQ_NONE;
   ULONG IntSources_UL=0,tmp;

   if (!pdx->DeviceRemoved_B)
   {
      spin_lock(&pdx->pOsDrvData_X->InterruptSpinlock_X);

      if (SkelDrv_GetCustomizationTable()->CallBack_ISR_fct)
      {
         if (SkelDrv_GetCustomizationTable()->CallBack_ISR_fct(pdx,&IntSources_UL))
         {
            rc = IRQ_HANDLED;


            tmp=InterlockedOr(&pdx->IntSource_UL,IntSources_UL);

            TraceISROutput("INTSRC=%08lx/%08lx DPCMSK=%08lx\n",IntSources_UL,tmp|IntSources_UL,pdx->DPCIntMask_UL);

            //printk ("<0> TrigTasklet (%x %x)\n",pdx->IntSource_UL,pdx->DPCIntMask_UL);
            if (pdx->IntSource_UL&pdx->DPCIntMask_UL) 
            {

               //signal_tasklet.data = (unsigned long)pdx;
               tasklet_schedule(&pdx->pOsDrvData_X->Tasklet_X);
            }
         }
      }

      spin_unlock(&pdx->pOsDrvData_X->InterruptSpinlock_X);
   }

   return rc;
}


/***** probe *********************************************************************************************

Description :



Parameters :

struct pci_dev *pPCIDevice_X	:
const struct pci_device_id *pDeviceID_X	:

Returned Value : int
Remark : None

**********************************************************************************************************************/
static int probe(struct pci_dev *pPCIDevice_X, const struct pci_device_id *pDeviceID_X)
{
   int sts=0,i;
   UBYTE PCIBar_UB;
   ULONG Flags_UL;
   DEVICE_EXTENSION *pdx;
   int index = GL_NbBoards_UL;   

   InitDbgOutput("[probe]:Sizeof(DEVICE_EXTENSION):%d\n",sizeof(DEVICE_EXTENSION));

   pdx = &GL_pDeviceExtension_X[index];
   pdx->BoardIdx_i = index;

   

   pdx->pOsDrvData_X = kmalloc(sizeof(OS_DRV_DATA),GFP_KERNEL);

   if (!pdx->pOsDrvData_X) return -ENOMEM;
   memset(pdx->pOsDrvData_X,0,sizeof(OS_DRV_DATA));

   /* Initialize OSDrvData members */
   tasklet_init(&pdx->pOsDrvData_X->Tasklet_X,isr_tasklet,(unsigned long) pdx);

   SKELDRV_INIT_SAFEAREA();

   if (pci_enable_device(pPCIDevice_X))
   {
      ErrOutput( "Error on pci_enable_device\n");
      return -ENODEV;
   }


   pci_set_drvdata(pPCIDevice_X, pdx); // Let's establish cross relations between pPCIDevice_X ... 
   pdx->pOsDrvData_X->pPCIDevice_X = pPCIDevice_X;   // .. and GL_pDevices_X[index]


   SKELDRV_INIT_SAFEAREA(); /* Warning, this call require OSDrvData */


   EnterOutput("\n"); // Requires PDX !!

   pdx->CustomizationTable_X.DefaultDbgMask_UL = DEFAULT_DBG_MASK;
   SkelCallBack_CustomizeDrv(pdx, &pdx->CustomizationTable_X);

   /* Initialization the DBG levels to default values */
   for (i=0;i<MAX_MODULES_ID; i++)
      pdx->pDbgMsk_UL[i] = pdx->CustomizationTable_X.DefaultDbgMask_UL;

   InitDbgOutput("Major: *****************\n");

   DbgOutput( "Creation of Character device(s)\n");
   if (CreateCharDevices(pdx, GL_major_i,index)) sts= -ENODEV;

   SkelDrv_AllocDrvData(pdx);

   for(i=0; i<6; i++) pdx->pPCIResources_X[i].Type_E =PCI_RESOURCE_TYPE_NOT_USED;
   pdx->BoardInitStatus_E = BOARD_INIT_KO;

InitDbgOutput("Browsing to retrieve PCI BAR information\n");
   DbgOutput( "Browsing to retrieve PCI BAR information\n");
   for (PCIBar_UB = 0; (!sts) && (PCIBar_UB < 6); PCIBar_UB++)
   {
      Flags_UL = pci_resource_flags(pPCIDevice_X, PCIBar_UB);
      
      InitDbgOutput("PCI%d , %08lx\n",PCIBar_UB, Flags_UL);

      if (Flags_UL & IORESOURCE_MEM)
      {  
         if (!request_mem_region(pci_resource_start(pPCIDevice_X, PCIBar_UB),pci_resource_len(pPCIDevice_X, PCIBar_UB), DRIVERNAME))
         {
            InitErrOutput( "Cannot reserve PCI memory range for PCI BAR %d\n",PCIBar_UB);
            remove(pPCIDevice_X);
            sts = -ENODEV;
         }
         else
         {
            pdx->pPCIResources_X[PCIBar_UB].Type_E =PCI_RESOURCE_TYPE_MEMORY;
            pdx->pPCIResources_X[PCIBar_UB].Size_UL=pci_resource_len(pPCIDevice_X, PCIBar_UB);
            pdx->pPCIResources_X[PCIBar_UB].PhysicalAddr_PA.QuadPart = pci_resource_start(pPCIDevice_X, PCIBar_UB);
            pdx->pPCIResources_X[PCIBar_UB].pKernelAddr_UB = (UBYTE*)ioremap(pci_resource_start(pPCIDevice_X, PCIBar_UB), pci_resource_len(pPCIDevice_X, PCIBar_UB));
         }


         if ((!sts) && (!pdx->pPCIResources_X[PCIBar_UB].pKernelAddr_UB))
         {
            ErrOutput( "Unable to map memory block %016llxh, length %08xh\n", pdx->pPCIResources_X[PCIBar_UB].PhysicalAddr_PA.QuadPart, pdx->pPCIResources_X[PCIBar_UB].Size_UL);
            remove(pPCIDevice_X);
            sts =  -ENOMEM;
         }			   
         InitDbgOutput( "PCI Bar %d : MEM (Addr:%016llxh - Size:%08xh - MapAddr:%016ph)\n",PCIBar_UB,pdx->pPCIResources_X[PCIBar_UB].PhysicalAddr_PA.QuadPart,pdx->pPCIResources_X[PCIBar_UB].Size_UL,pdx->pPCIResources_X[PCIBar_UB].pKernelAddr_UB);

      }
      else if (Flags_UL & IORESOURCE_IO)
      {
         //DBG("[IORESOURCE_IO]");
         pdx->pPCIResources_X[PCIBar_UB].Type_E = PCI_RESOURCE_TYPE_IO;
         pdx->pPCIResources_X[PCIBar_UB].Size_UL = pci_resource_len(pPCIDevice_X, PCIBar_UB);
         pdx->pPCIResources_X[PCIBar_UB].PhysicalAddr_PA.QuadPart = pci_resource_start(pPCIDevice_X, PCIBar_UB);
         pdx->pPCIResources_X[PCIBar_UB].IOAddr_UL = pci_resource_start(pPCIDevice_X, PCIBar_UB);
         DbgOutput( "PCI Bar %d : I/O (Addr:%016llxh - Size:%08xh)\n",PCIBar_UB,pdx->pPCIResources_X[PCIBar_UB].PhysicalAddr_PA.QuadPart,pdx->pPCIResources_X[PCIBar_UB].Size_UL);

      }
      else 
      {
         DbgOutput( "PCI BAR %d : Not used\n",	PCIBar_UB);
      }
   } //end for

   // Verify that we got all the resources we were expecting
   if(pdx->CustomizationTable_X.CallBack_CheckResources_fct != NULL && !pdx->CustomizationTable_X.CallBack_CheckResources_fct(pdx))
   {
      remove(pPCIDevice_X);
      sts=-ENODEV;
   }

   //SkelDrv_AllocDrvData(pdx);

   // Requesting IRQ for this device
   if ((!sts) && (request_irq(pPCIDevice_X->irq, OnInterrupt, IRQF_DISABLED|IRQF_SHARED/*SA_INTERRUPT | SA_SHIRQ*/, DRIVERNAME,pdx) != 0))
   {
      InitErrOutput( "Cannot request interrupt for this device (irq:%d)\n", pPCIDevice_X->irq);
      removeEx(pPCIDevice_X,TRUE); 
		
      sts =  -ENODEV;
   }
   else
   {
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,38))
      pdx->pOsDrvData_X->InterruptSpinlock_X = SPIN_LOCK_UNLOCKED;  // Initialize Interrupt spinlock 
#else
      pdx->pOsDrvData_X->InterruptSpinlock_X = __SPIN_LOCK_UNLOCKED(pdx->pOsDrvData_X->InterruptSpinlock_X);
#endif
      InitDbgOutput( "IRQ ok on %d\n",pPCIDevice_X->irq);
   }

   if (!sts)
   {
      atomic_set(&pdx->pOsDrvData_X->RefCount, 0);
      atomic_set(&pdx->pOsDrvData_X->MainDev_RefCount,0);

      pci_set_master(pPCIDevice_X);

      for (i=0; i<NB_AUX_CHAR_DEVICES; i++) atomic_set(&pdx->pOsDrvData_X->pAuxDev_RefCount[i],0);   

		SkelDrv_DeviceAdd(pdx);

      if (pdx->BoardInitStatus_E == BOARD_INIT_OK)
         SkelDrv_Wakeup(pdx);


 		switch(pdx->BoardInitStatus_E)
      {
      case BOARD_INIT_NEED_RESTART :
      case BOARD_INIT_NEED_SHUTDOWN : 
      	sts = -ENODEV; 
         remove(pPCIDevice_X);
      	printk(DRIVERNAME " System needs a restart\n");
      	break;
      case BOARD_INIT_OK : sts = 0; break;
      case BOARD_INIT_KO :
      default : sts = -ENODEV; break;
      }

      GL_NbBoards_UL++;

      /* TODO : Set a FLAG here to notice that the PROBE setup had succeeded. This flag will be used in the remove function */
   }

   RetailOutput( "(sts:%d)\n",sts);

   return sts;	
}


/***** remove *********************************************************************************************

Description :



Parameters :

struct pci_dev *pPCIDevice_X	:

Returned Value : void
Remark : None

**********************************************************************************************************************/
static void remove(struct pci_dev *pPCIDevice_X)
{
   removeEx(pPCIDevice_X,FALSE);
}

static void removeEx(struct pci_dev *pPCIDevice_X, BOOL32 NoFreeIRQ_B)
{
   DEVICE_EXTENSION *pdx = pci_get_drvdata(pPCIDevice_X);

   EnterOutput("\n");

   SkelDrv_Sleep(pdx, TRUE);

   if (!SkelDrv_CheckDevicePresence(pdx))
      SkelDrv_DeviceSurpriseRemoval(pdx);

   if (pdx->BoardInitStatus_E != BOARD_INIT_KO)
      SkelDrv_DeviceRemove(pdx);

   SkelDrv_Cleanup(pdx, 0, FALSE);
   SkelDrv_ReleasePermanentResources(pdx);


   if(!NoFreeIRQ_B)
   {
      DbgOutput( "Freeing IRQ\n");
      free_irq(pPCIDevice_X->irq, pdx);  
   } 


   DbgOutput( "Un-mapping all PCI memory-mapped regions\n");
   PCIResources_CleanUp(pdx);

   DbgOutput( "Removing Character device(s)\n");
   RemoveCharDevices(pdx, GL_major_i, pdx->BoardIdx_i);

   SkelDrv_FreeDrvData(pdx);

   kfree(pdx->pOsDrvData_X);
   ExitOutput("\n");
}

/***** pci_skel_init *********************************************************************************************

Description :

This is the main entry point of the driver called during INSMOD process.

Parameters :

void	:

Returned Value : int 
Remark : DRIVERNAME will be shown in /proc/devices, so a cat on it will help us to get back the major number 
cfr Bash Loading Script

**********************************************************************************************************************/
static int __init pci_skel_init(void)
{
   int             result_i;
   dev_t           dev_X;

   memset(GL_pDeviceExtension_X,0,NB_MAX_BOARD*sizeof(DEVICE_EXTENSION));

   SkelCallBack_EntryPoint();
   SetInitDbgMask(GL_DrvParam_X.DbgMask_UL);



#if (MEM_ALLOC_METHOD == MEM_ALLOC_MEMEQUAL)
   InitRetailOutput( "Using Mem=xxx\n");
   if ((result_i = allocator_init()) != 0) // Initialize Allocator module 
   {
      InitErrOutput( "Cannot initialize Allocator\n"); 
      return result_i; 
   }
#elif (MEM_ALLOC_METHOD == MEM_ALLOC_BIGPHYSAREA)
   //InitRetailOutput( "Using the bigphys area patch with %ld bytes available...\n",bigphysarea_get_size());
   InitRetailOutput( "Using the bigphys area patch\n");
#else
   InitRetailOutput( "Using __get_free_pages (MAX_ORDER=%d)\n",MAX_ORDER);
#endif    


   result_i = alloc_chrdev_region(&dev_X, 0 , NB_MAX_BOARD*NBMINORS, DRIVERNAME);

   GL_major_i = MAJOR(dev_X);

   if (result_i < 0)
   {
      InitErrOutput( "Can't get major %d\n",GL_major_i);
#if (MEM_ALLOC_METHOD == MEM_ALLOC_MEMEQUAL)
      allocator_cleanup();
#endif
      return result_i;
   }
   else InitDbgOutput("Major: %d\n",GL_major_i);

   /* Create a class in sysfs for use with udev */

   pDeltaClass_X = class_create(THIS_MODULE, DRIVERNAME);
   if (IS_ERR((void*) pDeltaClass_X)){
      InitErrOutput("Error creating %s class.  You will have to create nodes manually.\n", DRIVERNAME);
   }

   result_i = pci_register_driver(&GL_pci_driver_X);
   

   if (result_i < 0)
   {
      InitErrOutput( "Can't get pci_register_driver result_i : %d\n", result_i);
      unregister_chrdev_region(MKDEV(GL_major_i,0), NB_MAX_BOARD*NBMINORS);
#if (MEM_ALLOC_METHOD == MEM_ALLOC_MEMEQUAL)
      allocator_cleanup();
#endif
      return result_i;
   }

   InitDbgOutput( "Exiting\n");
   return 0;
}



/***** pci_skel_exit *********************************************************************************************

Description :



Parameters :

void	:

Returned Value : void 
Remark : None

**********************************************************************************************************************/
static void __exit pci_skel_exit(void)
{
   InitDbgOutput( "Entering\n");
   pci_unregister_driver(&GL_pci_driver_X);
   unregister_chrdev_region(MKDEV(GL_major_i, 0), NB_MAX_BOARD*NBMINORS);
   class_destroy(pDeltaClass_X);
#if (MEM_ALLOC_METHOD == MEM_ALLOC_MEMEQUAL)  
   allocator_cleanup();
#endif
   InitDbgOutput( "Exiting\n");
}


module_init(pci_skel_init);
module_exit(pci_skel_exit);


int Power_Suspend(struct pci_dev* pDev_X, pm_message_t state)
{
   DEVICE_EXTENSION *pdx = pci_get_drvdata(pDev_X);
   EnterOutput("State: %d\n", state.event);

   SkelDrv_Sleep(pdx, FALSE);

   ExitOutput("\n");
   return 0;
}

int Power_Resume(struct pci_dev* pDev_X)
{
   DEVICE_EXTENSION *pdx = pci_get_drvdata(pDev_X);
   int Return_i = 0;
   EnterOutput("\n");

   SkelDrv_Wakeup(pdx);

   switch (pdx->BoardInitStatus_E)
   {
   case BOARD_INIT_KO:
   case BOARD_INIT_NEED_RESTART:
   case BOARD_INIT_NEED_SHUTDOWN:
      Return_i = -1;
      break;
   }

   ExitOutput("Status: %d\n", Return_i);
   return Return_i;
}

int Power_Shutdown(struct pci_dev* pDev_X)
{
   DEVICE_EXTENSION *pdx = pci_get_drvdata(pDev_X);
   EnterOutput("\n");

   SkelDrv_Sleep(pdx, TRUE);

   ExitOutput("\n");
   return 0;
}
